<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use mdm\admin\models\form\Login;
//use mdm\admin\models\form\PasswordResetRequest;
use mdm\admin\models\form\ResetPassword;
//use mdm\admin\models\form\Signup;
use mdm\admin\models\form\ChangePassword;
use mdm\admin\models\User;
use mdm\admin\models\searchs\User as UserSearch;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\base\UserException;
use yii\mail\BaseMailer;
//use \app\models\User;
use app\models\Student;
use app\models\ForgotPassword;
use app\models\CreditSession;
use app\libraries\General;
use yii\helpers\Url;
//session
use yii\web\Session;
use app\models\SytemCapabilityAutoLog;
use app\libraries\MailChimpApi;

class SiteController extends Controller {

    private $_oldMailPath;

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['reset-password', 'forgot-password', 'signup', 'account-activation', 'student-account-activation', 'test'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['login', 'test-mail', 'test-default-notification', 'daylight', 'check-device', 'test-check-device'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'change-password', 'index', 'account-activation'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['tutor-unavailability'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'layout' => 'error',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    /*  public static function beforeAction($action) {
      if (parent::beforeAction($action)) {
      if (Yii::$app->has('mailer') && ($mailer = Yii::$app->getMailer()) instanceof BaseMailer) {
      /* @var $mailer BaseMailer */
    /*      $this->_oldMailPath = $mailer->getViewPath();
      $mailer->setViewPath('@mdm/admin/mail');
      }
      return true;
      }
      return false;
      } */

    /**
     * @inheritdoc
     */
    /*  public static function afterAction($action, $result) {
      if ($this->_oldMailPath !== null) {
      Yii::$app->getMailer()->setViewPath($this->_oldMailPath);
      }
      return parent::afterAction($action, $result);
      } */

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        if (Yii::$app->user->identity->role == 'STUDENT') {

            return $this->redirect(['/student/home']);
        } elseif (Yii::$app->user->identity->role == 'TUTOR') {
            return $this->redirect(['/booked-session/upcoming']);
//            $model = \app\models\Tutor::find()->where(['uuid' => Yii::$app->user->identity->tutor_uuid])->one();
//            return $this->render('tutor_dashboard', [
//                        'model' => $model,
//            ]);
        } elseif (Yii::$app->user->identity->role == 'OWNER') {
            return $this->render('owner_dashboard', []);
        } elseif (Yii::$app->user->identity->role == 'ADMIN') {
            return $this->render('admin_dashboard', []);
        } elseif (Yii::$app->user->identity->role == 'SUBADMIN') {
            return $this->render('subadmin_dashboard', []);
        }
        return $this->render('index');
    }
    

    public static function emailIsForVerification () {
        $res = false;
        if (Yii::$app->getRequest()->isPost) {
            $data = Yii::$app->getRequest()->post();
            $user = User::find()
            ->where(["username"=>$data["Login"]["username"]])
            ->one();
            if ($user) {
                if($user->status == 0) {
                    $res = true;
                }
            }
        }
        return $res;
    }


    public function actionLogin() {
        $this->layout = "student_login";

        if (!Yii::$app->getUser()->isGuest) {
            return $this->goHome();
        }

        $model = new Login();

        if(self::emailIsForVerification()) {
            $model->load(Yii::$app->getRequest()->post());
            return $this->render('login', [
                'model' => $model,
                'verifyEmail' => true
            ]);
        }

        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {

            // STUDENT LOGIN
            if (Yii::$app->user->identity->role == 'STUDENT') {
                $student_uuid = Yii::$app->user->identity->student_uuid;
                $isMonthlySubscription = Student::find()->where(['student_uuid' => $student_uuid])->one();
                Yii::$app->session->set('isMonthlySubscription', $isMonthlySubscription->isMonthlySubscription);
           
                $device_id = General::GetMAC();
                $ua = General::getBrowser();
                $browser = $ua['name'];
                $browser_version = $ua['version'];
                
                $system_log_count = SytemCapabilityAutoLog::find()
                        ->where(['student_uuid'=>Yii::$app->user->identity->student_uuid,
                        'browser'=>$browser,'browser_version'=>$browser_version])
                        ->orderBy([
                            'datetime' => SORT_DESC
                          ])->count();
                
                if($system_log_count > 0) {
                    $system_log = SytemCapabilityAutoLog::find()
                        ->where(['student_uuid'=>Yii::$app->user->identity->student_uuid,
                        'browser'=>$browser,'browser_version'=>$browser_version])
                        ->orderBy([
                            'datetime' => SORT_DESC
                          ])->one();
                    
                    $previous_date = $system_log->datetime;
                    $current_date = date('Y-m-d H:i:s');

                    $diff = abs(strtotime($current_date) - strtotime($previous_date));

                    $years = floor($diff / (365*60*60*24));
                    $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                    $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

                    if($months >= 1){
                        $check_log = General::JitsiCheckDevice();
                        $system_log = new SytemCapabilityAutoLog();
                        $system_log->student_uuid = Yii::$app->user->identity->student_uuid;
                        $system_log->datetime = date('Y-m-d H:i:s');
                        $system_log->status = $check_log['success'];
                        $system_log->message=$check_log['msg'];
                        $system_log->browser = $check_log['browser'];
                        $system_log->browser_version = $check_log['browser_version'];
                        $system_log->device = $check_log['device'];
                        $system_log->device_id = General::GetMAC();
                        $system_log->save(false);  
                        
                        if($check_log['success'] == 'true') {
                            Yii::$app->getSession()->setFlash('success', $check_log['msg']);
                        } else {
                            Yii::$app->getSession()->setFlash('danger', $check_log['msg']);
                        }
                    }

                } else {
                    $check_log = General::JitsiCheckDevice();
                    $system_log = new SytemCapabilityAutoLog();
                    $system_log->student_uuid = Yii::$app->user->identity->student_uuid;
                    $system_log->datetime = date('Y-m-d H:i:s');
                    $system_log->status = $check_log['success'];
                    $system_log->message = $check_log['msg'];
                    $system_log->browser = $check_log['browser'];
                    $system_log->browser_version = $check_log['browser_version'];
                    $system_log->device = $check_log['device'];
                    $system_log->device_id = General::GetMAC();
                    $system_log->save(false);     
                                 
                    if($check_log['success'] == 'true')
                    {
                        Yii::$app->getSession()->setFlash('success', $check_log['msg']);
                    } else {
                        Yii::$app->getSession()->setFlash('danger', $check_log['msg']);
                    }
                    
                }

            }
            
            
            // TUTOR LOGIN
            if (Yii::$app->user->identity->role == 'TUTOR') {
                $device_id = General::GetMAC();
                $ua = General::getBrowser();
                $browser = $ua['name'];
                $browser_version = $ua['version'];
            
                
                $system_log_count = SytemCapabilityAutoLog::find()
                        ->where(['tutor_uuid'=>Yii::$app->user->identity->tutor_uuid,
                        'browser'=>$browser,'browser_version'=>$browser_version])
                        ->orderBy([
                            'datetime' => SORT_DESC
                          ])->count();
                

                if($system_log_count > 0) {
                        
                    $system_log = SytemCapabilityAutoLog::find()
                        ->where(['tutor_uuid'=>Yii::$app->user->identity->tutor_uuid,
                        'browser'=>$browser,'browser_version'=>$browser_version])
                        ->orderBy([
                            'datetime' => SORT_DESC
                          ])->one();
                    
                    $previous_date = $system_log->datetime;
                    $current_date = date('Y-m-d H:i:s');

                    $diff = abs(strtotime($current_date) - strtotime($previous_date));

                    $years = floor($diff / (365*60*60*24));
                    $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                    $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

                    

                    if($months >= 1){
                        
                        $check_log = General::JitsiCheckDevice();
                        $system_log = new SytemCapabilityAutoLog();
                        $system_log->tutor_uuid = Yii::$app->user->identity->tutor_uuid;
                        $system_log->datetime = date('Y-m-d H:i:s');
                        $system_log->status = $check_log['success'];
                        $system_log->message=$check_log['msg'];
                        $system_log->browser = $check_log['browser'];
                        $system_log->browser_version = $check_log['browser_version'];
                        $system_log->device = $check_log['device'];
                        $system_log->device_id = General::GetMAC();
                        $system_log->save(false);  
                        
                        if($check_log['success'] == 'true')
                        {
                            Yii::$app->getSession()->setFlash('success', $check_log['msg']);
                        } else {
                            Yii::$app->getSession()->setFlash('danger', $check_log['msg']);
                        }
                    
                    }
                } else {
                    
                    $check_log = General::JitsiCheckDevice();
                    $system_log = new SytemCapabilityAutoLog();
                    $system_log->tutor_uuid = Yii::$app->user->identity->tutor_uuid;
                    $system_log->datetime = date('Y-m-d H:i:s');
                    $system_log->status = $check_log['success'];
                    $system_log->message = $check_log['msg'];
                    $system_log->browser = $check_log['browser'];
                    $system_log->browser_version = $check_log['browser_version'];
                    $system_log->device = $check_log['device'];
                    $system_log->device_id = General::GetMAC();
                    $system_log->save(false);                  
                    if($check_log['success'] == 'true')
                    {
                        Yii::$app->getSession()->setFlash('success', $check_log['msg']);
                    } else {
                        Yii::$app->getSession()->setFlash('danger', $check_log['msg']);
                    }
                    
                }
            }

            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Signup new user
     * @return string
     */
    public function actionSignup() {

//        $this->layout = "signup";
        $this->layout = "student_login";

        $model = new \app\models\User();
        $model->scenario = 'signup';
        $modelStudent = new Student();
        $modelStudent->state_text = $modelStudent->state;

        $modelStudent->attributes = Yii::$app->getRequest()->post('Student');
        $model->attributes = Yii::$app->getRequest()->post('User');

        $model->username = $model->email;
        $model->created_at = $model->updated_at = time();
        $modelStudent->created_at = $modelStudent->updated_at = time();
        $model->role = 'STUDENT';
        $model->auth_key = Yii::$app->security->generateRandomString();
        $modelStudent->email = $model->email;
        $modelStudent->enrollment_id = General::generateEnrollmentid();
        $modelStudent->birthyear = date('Y', strtotime($modelStudent->birthdate));
        $model->referer_source = $modelStudent->referer_source;

        if ($modelStudent->load(Yii::$app->getRequest()->post()) && $model->load(Yii::$app->getRequest()->post()) && $model->validate() && $modelStudent->validate()) {

            $modelStudent->state = ($modelStudent->country == 'other') ? $modelStudent->state_text : $modelStudent->state;
            $modelStudent->save(false);

            $model->password_hash = Yii::$app->security->generatePasswordHash($model->password_hash);
            $model->student_uuid = $modelStudent->student_uuid;
            $model->first_name = $modelStudent->first_name;
            $model->last_name = $modelStudent->last_name;
            $model->phone = $modelStudent->phone_number;
            $model->status = 0;
            $model->referer_source = $modelStudent->referer_source;
            $model->save(false);
            

            /*
                Developer: Akash Shrimali
                Task: Add data in Mailchimp when student Signup
                Date: 12-03-2020
                Start
            */
            Yii::$app->authManager->assign(Yii::$app->authManager->getRole($model->role), $model->getPrimaryKey());

            // DARYL: uncomment logs ON SANDBOX AND PROD
            // $start_text = "******************* start ".$model->student_uuid." date ". date('Y-m-d H:i:s'). " *************\n";
            // $path = '/var/www/html/musiconn/';
            // file_put_contents($path . 'mailchimp_log.txt', $start_text, FILE_APPEND);
            // $mailchimp_user = MailChimpApi::insert($model->email, $model->first_name, $model->last_name);
            // file_put_contents($path . 'mailchimp_log.txt', print_r($mailchimp_user, true), FILE_APPEND);
            // $end_text = "\n******************* end date ". date('Y-m-d H:i:s'). " *************\n";
            // file_put_contents($path . 'mailchimp_log.txt', $end_text, FILE_APPEND);
            self::SendActivationLink($model->student_uuid);

            \app\models\StudentNotification::addDefaultNotification($modelStudent->student_uuid);

            Yii::$app->session->set('registration_success', 'registration_success');
            //Yii::$app->getSession()->setFlash('success', 'You have successfully created your account. Please check your email to verify account.');
            return $this->redirect(['login']);
            /*
                Developer: Akash Shrimali
                Task: Add data in Mailchimp when student Signup
                Date: 12-03-2020
                End
            */
        }

        return $this->render('signup', [
                    'model' => $model,
                    'modelStudent' => $modelStudent,
        ]);
    }

    /**
     * Request reset password
     * @return string
     */
    public function actionForgotPassword() {
        $this->layout = "student_login";

        $model = new ForgotPassword();
        if ($model->load(Yii::$app->getRequest()->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
                    'model' => $model,
        ]);
    }

    /**
     * Reset password
     * @return string
     */
    public function actionResetPassword($token) {
        $this->layout = "student_login";
        try {
            $model = new ResetPassword($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->getRequest()->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
                    'model' => $model,
        ]);
    }

    /**
     * Reset password
     * @return string
     */
    public function actionAccountActivation() {

        $this->layout = "error";
        $return = ['code' => 421, 'message' => 'Sorry, something wrong'];

        try {

            $t = urldecode(Yii::$app->getRequest()->get('t'));
            $u = urldecode(Yii::$app->getRequest()->get('u'));
            $uuid_denc = General::decrypt_str($t);
            $email_denc = General::decrypt_str($u);

            if (($admin = \app\models\Admin::find()->where(['admin_uuid' => $uuid_denc])->andWhere(" admin.status != 'DELETED' ")->one()) !== null) {

                if ($admin->status == 'INACTIVE') {
                    $user = \app\models\User::find()->where(['admin_uuid' => $uuid_denc])->one();
                    if (!empty($user)) {

                        $user->status = 10;
                        $user->save(false);

                        $admin->status = 'ENABLED';
                        $admin->save(false);

                        $return = ['code' => 200, 'message' => 'Your account actived successfully.'];
                    } else {
                        $return = ['code' => 421, 'message' => 'User record does not exist'];
                    }
                } else {
                    $return = ['code' => 421, 'message' => 'Your account not activated.'];
                }
            } else {
                $return = ['code' => 421, 'message' => 'Sorry no record found for your account activation request.'];
            }
        } catch (InvalidParamException $e) {

            $return = ['code' => 421, 'message' => $e->getMessage()];
        }

        return $this->render('account_activation', [
                    'return' => $return,
        ]);
    }

    public static function SendActivationLink($id) {
        //
        if (($student = Student::findOne($id)) !== null) {
            if ($student->status == "INACTIVE") {

                $uuid_enc = General::encrypt_str($student->student_uuid);
                $token = Student::generateActivationToken();

                $student->activation_link_token = $token;
                $student->save(false);

                $content['name'] = $student->first_name;
                $content['link'] = Url::to(['site/student-account-activation', 't' => $uuid_enc, 'token' => $token], true);

                $ownersEmail = General::getOwnersEmails(['OWNER','ADMIN']);
                Yii::$app->mailer->compose('account_enabled', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                        ->setFrom(Yii::$app->params['supportEmail'])
                        ->setTo($student->email)
                        ->setBcc($ownersEmail)
                        ->setSubject('Account Activation mail - ' . ucfirst(strtolower(Yii::$app->name)))
                        ->send();

                $return = ['code' => 200, 'message' => 'Activation link sent successfully.'];
            } else {
                $return = ['code' => 421, 'message' => 'Student account already enabled.'];
            }
        } else {
            $return = ['code' => 404, 'message' => 'Record does not exist.'];
        }

        return $return;
    }

    public function actionStudentAccountActivation() {

        $this->layout = "error";
        $return = ['code' => 421, 'message' => 'Sorry, something wrong'];

        try {

            $t = urldecode(Yii::$app->getRequest()->get('t'));
            $get_token = urldecode(Yii::$app->getRequest()->get('token'));
            $uuid_denc = General::decrypt_str($t);

            if (($student = \app\models\Student::find()->where(['student_uuid' => $uuid_denc])->andWhere(" student.status != 'DELETED' ")->one()) !== null) {
                if ($student->status == 'INACTIVE') {
                    $user = \app\models\User::find()->where(['student_uuid' => $uuid_denc])->one();
                    $tokenFlag = Student::isActivationTokenValid($get_token, $uuid_denc);
                    if ($tokenFlag) {
                        if (!empty($user)) {

                            $user->status = 10;
                            $user->save(false);
                            $uuid_enc = General::encrypt_str($student->student_uuid);
                            $token = Student::generateActivationToken();

                            $student->status = 'ENABLED';
                            $student->save(false);

                            $logger_identity = \mdm\admin\models\User::findIdentity($user->id);
                            Yii::$app->user->switchIdentity($logger_identity, 0);

                            $content['name'] = $student->first_name;
                            $content['link'] = Url::to(['site/student-account-activation', 't' => $uuid_enc, 'token' => $token], true);

                            Yii::$app->mailer->compose('registration_confirm', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                                    ->setFrom(Yii::$app->params['supportEmail'])
                                    ->setTo($student->email)
                                    ->setSubject(ucfirst(strtolower(Yii::$app->name)) . ' - Your Registration is Complete')
                                    ->send();

                            $content_admin['student_name'] = $student->first_name;
                            $content_admin['student_last_name'] = $student->last_name;
                            $content_admin['student_id'] = $student->enrollment_id;
                            $content_admin['username'] = $user->username;
                            $content_admin['date'] = date('Y-m-d H:i:s T', $student->created_at);
                            Yii::$app->mailer->compose('registration_confirm_admin', ['content' => $content_admin], ['htmlLayout' => 'layouts/html'])
                                    ->setFrom(Yii::$app->params['supportEmail'])
                                    ->setTo(Yii::$app->params['adminEmail'])
                                    ->setSubject(ucfirst(strtolower(Yii::$app->name)) . ' Notification: New Student Registration')
                                    ->send();

                            $return = ['code' => 200, 'message' => 'Your account activated successfully.'];
                        } else {
                            $return = ['code' => 421, 'message' => 'User record does not exist'];
                        }
                    } else {
                        $return = ['code' => 421, 'message' => 'Account activation token expired'];
                        return $this->render('inactive_account_activation', [
                                    'return' => $return,
                                    'uuid' => $student->student_uuid,
                        ]);
                    }
                } elseif ($student->status == 'ENABLED') {
                    $return = ['code' => 200, 'message' => 'Your account already activated.'];
                } else {
                    $return = ['code' => 421, 'message' => 'Your account not activated.'];
                }
            } else {
                $return = ['code' => 421, 'message' => 'Sorry no record found for your account activation request.'];
            }
        } catch (\Exception $e) {

            $return = ['code' => 421, 'message' => $e->getMessage()];
        }

        if ($return['code'] == 200) {
            Yii::$app->getSession()->setFlash('success', $return['message']);
        } else {
            Yii::$app->getSession()->setFlash('danger', $return['message']);
        }
        return $this->goBack();
    }

    public function actionTutorUnavailability() {

        $tutorModel = \app\models\Tutor::find()->where(['uuid' => Yii::$app->user->identity->tutor_uuid])->one();
        $model = new \app\models\TutorUnavailability();
        $model->scenario = 'unavailability';
        if (Yii::$app->request->isAjax) {

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $start_datetime = date('Y-m-d H:i:s', strtotime($model->start_datetime));
                $end_datetime = date('Y-m-d H:i:s', strtotime($model->end_datetime));
                $model->tutor_uuid = Yii::$app->user->identity->tutor_uuid;
                $model->start_datetime = General::convertUserToSystemTimezone($start_datetime);
                $model->end_datetime = General::convertUserToSystemTimezone($end_datetime);
                $model->save(false);
                $return = ['code' => 200, 'message' => 'Your unavailability set successfully.'];
                Yii::$app->response->format = 'json';
                return $return;
            } else {
                if (!$model->load(Yii::$app->request->post())) {
                    $nextDate = date('d-m-Y H:i', strtotime('+1 day', time()));
                    $model->start_datetime = $nextDate;
                    $model->end_datetime = date('d-m-Y H:i', strtotime('+30 minutes', strtotime($nextDate)));
                }
            }

            //\app\libraries\General::displayDate();
            return $this->renderAjax('_tutor_unavailability', [
                        'model' => $model,
            ]);
        }
    }

    public function actionTestMail() {
        exit;
        $content['student_first_name'] = 'Alpesh';
        $content['student_last_name'] = 'Patel';
        $content['start_datetime'] = date('d F Y') . ' at ' . date('h:ia T');
        $content['session_pin'] = '0000';
        $content['session_url'] = '0000';


        $mailer = Yii::$app->getMailer();
        $mailer->htmlLayout = 'layouts/notification_layout';
        $mail = Yii::$app->mailer->compose('test', ['content' => $content])
                ->setFrom(Yii::$app->params['supportEmail'])
                ->setTo('alpesh@vindaloovoip.com')

                //->setBcc($owner)
                ->setSubject("Musiconn Notification:  Lesson with your tutor is scheduled to start soon")
                ->send();
        if ($mail) {
            echo "sent";
        } else {
            echo "fail";
        }
    }

    public function actionTestDefaultNotification() {
        echo "yes please";
        $modelStudent = Student::find()->all();
        if (!empty($modelStudent)) {
            foreach ($modelStudent as $key => $student) {
                $modelNotification = \app\models\StudentNotification::find()->where(['student_uuid' => $student->student_uuid])->all();
                if (empty($modelNotification)) {
                    \app\models\StudentNotification::addDefaultNotification($student->student_uuid);
                    echo "Student => " . $student->email . "</br>";
                }
            }
        }

        $modelTutor = $model = \app\models\Tutor::find()->all();
        if (!empty($modelTutor)) {
            foreach ($modelTutor as $key => $tutor) {
                $modelNotificationTutor = \app\models\TutorNotification::find()->where(['tutor_uuid' => $tutor->uuid])->all();
                if (empty($modelNotificationTutor)) {
                    \app\models\TutorNotification::addDefaultNotification($tutor->uuid);
                    echo "Tutor => " . $tutor->email . "</br>";
                }
            }
        }
    }

    public function actionDaylight() {

        $date = '2019-01-27 15:00';
        $f = 'Australia/Sydney';
        $t = 'Australia/Perth';
        $converted = General::convertTimezone($date, $f, $t, 'Y-m-d H:i T');
        echo $converted . "<br>";
        if (date('I', strtotime($date))) {
            echo 'We are in DST!';
        } else {
            echo 'We are not in DST!';
        }
    }
    
    public function actionCheckDevice() {
        
        //$response = General::CheckDevice();
        $response = General::JitsiCheckDevice();
        
        //Yii::$app->getSession()->setFlash( ($response['success'] == 'true')?'success':'danger', $response['msg']);
        
        Yii::$app->session['custom_alert'] = ['type' => ($response['success'] == 'true')?'success':'danger', 'message' => $response['msg']];
        if (Yii::$app->user->identity->role == 'STUDENT') {
            return $this->redirect(['student/profile']);
        } elseif (Yii::$app->user->identity->role == 'TUTOR') {
            return $this->redirect(['tutor/profile']);
        }
    }
    
    public function actionTestCheckDevice() {
        
        $data = [
            'server_data' => $_SERVER,
            'get_browser' => General::getBrowser(),
            ];
        $response = General::JitsiCheckDevice();
        echo "<pre>";
        print_r(($data));
        print_r($response);
        echo "</pre>";
        exit;
    }

}
