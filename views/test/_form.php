<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Settings */
/* @var $form yii\widgets\ActiveForm */
// echo "<pre>";
// print_r($model->getErrors());
// echo "</pre>";
?>

<div class="test-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'name')->textInput() ?>

    <?= $form->field($model, 'file')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <a href="<?= Yii::$app->getUrlManager()->createUrl(['/test/index/'])?>" class="btn btn-danger ">
            back
        </a>
    </div>

    <?php ActiveForm::end(); ?>

</div>
