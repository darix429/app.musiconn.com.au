<?php

use yii\helpers\Html;
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear Musiconn Admin,</p>

            <p><b><?= $content['student_id'].' '.$content['student_name']; ?>(<?= $content['student_email']; ?>)</b> has deleted their student account with <?= Yii::$app->name; ?>. Any booked lessons have also been removed from the system.</p>

            <p>Regards,<br>
            System Administrators</p>
        </td>
    </tr>
</table>

