<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\libraries\General;

$this->registerJS(
  '
  $(document).on("click",".student_approve_button",function(){
        if(confirm("Are you sure, you want to approve this request?")){
            var curr_uuid = $(this).data("bookedsessionuuid");
            $.ajax({
                type: "GET",
                url: "' . Url::to(['/booked-session/cancel-lesson-approval/', ]) .'",
                data: { id : curr_uuid, st : "yes"},
                beforeSend: function() {
                    blockBody();
                },
                success:function(result){
                  if(result.code == 200) {
                    window.location.reload();
                    $("#studentLessonCancelApproval").modal("toggle");
                    unblockBody();
                  } else {
                    unblockBody();
                    showErrorMessage(result.message);
                  }
                },
                error:function(e){
                    unblockBody();
                    showErrorMessage(e.responseText);
                }
            });
        } else {
            unblockBody();
            return false;
        }
  });

  $(document).on("click",".student_cancel_button",function(){
    if(confirm("Are you sure, you want to reject this request?")){
        var curr_uuid = $(this).data("bookedsessionuuid");
        var status;
        $.ajax({
            type: "GET",
            url: "' . Url::to(['/booked-session/cancel-lesson-approval/', ]) .'",
            data: { id : curr_uuid, st : "no"},
            beforeSend: function() {
                blockBody();
            },
            success:function(result){
                if(result.code == 200) {
                    window.location.reload();
                    $("#studentLessonCancelApproval").modal("toggle");
                    unblockBody();
                } else {
                    unblockBody();
                    showErrorMessage(result.message);
                }
            },
            error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
    } else {
        unblockBody();
        return false;
    }
  });
 '
)
?>
<div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">
    <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Date</th>
                <th>Time</th>
                <th>Student Name</th>
                <th>Tutor Name</th>
		<th>Request By</th>
                <th>Request Date</th>
                <th>Note</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php

            if (!empty($cancelSessionList)) {

                foreach ($cancelSessionList as $key => $value) {
                    ?>
                    <tr>
                        <td><?= $key + 1; ?></td>
                        <td><?= General::displayDate($value->bookedSessionUu->start_datetime); ?></td>
                        <td><?= General::displayTime($value->bookedSessionUu->start_datetime) . '-' . General::displayTime($value->bookedSessionUu->end_datetime); ?></td>
                        <td><?= $value->studentUu->first_name . ' ' . $value->studentUu->last_name; ?></td>
                        <td><?= $value->tutorUu->first_name . ' ' . $value->tutorUu->last_name; ?></td>
			<td><?= $value->cancel_by; ?></td>
                        <td><?= General::displayDate($value->created_at); ?></td>
                        <td><?= $value->note; ?></td>
                        <td align="left">
                            <?php
                            if ($value['status'] == 'AWAITING') {
                                $e_class = "badge-primary";
                                $pointer = 'style="cursor:pointer;color:#fff"';
                            } elseif ($value['status'] == 'APPROVED') {
                                $e_class = "badge-secondary";
                                $pointer = 'style="cursor:pointer;color:#fff"';
                            } else {
                                $e_class = "badge-danger";
                                $pointer = '';
                            }
                            ?>
                            <span class="badge badge-pill <?= $e_class ?>"  data-uuid="<?= $value['student_uuid']; ?>" <?= $pointer ?>><?= $value['status']; ?></span>
                        </td>
                        <td>
                            <button class="btn btn-xs btn-success student_approve_button" title="Approve" data-uuid="<?= $value['uuid']; ?>" data-bookedsessionuuid="<?= $value['booked_session_uuid']; ?>" data-url="<?= Url::to(['/booked-session/cancel-lesson-approval','id' => $value['uuid'] ]) ?>" >Approve</button>
                            <button class="btn btn-xs btn-danger student_cancel_button" title="Cancel" data-uuid="<?= $value['uuid']; ?>" data-bookedsessionuuid="<?= $value['booked_session_uuid']; ?>" data-url="<?= Url::to(['/booked-session/cancel-lesson-approval','id' => $value['uuid'] ]) ?>" >Reject</button>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>

</div>
