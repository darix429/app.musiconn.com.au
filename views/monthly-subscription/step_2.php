<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Url;
//rupal
use app\models\MonthlySubscriptionFee;

$this->title = Yii::t('app', 'MONTHLY SUBSCRIPTION');
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_icon'] = 'my-lesson-tag.png';

//echo "<pre>";
//print_r($step_data);
//print_r($model->getErrors());
//echo "</pre>";
//exit;

$studentSubDetail = \app\models\StudentSubscription::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid])->one();

if (!empty($studentSubDetail)) {
    $monthlySubscriptionDetail = \app\models\StudentMonthlySubscription::find()->where(['uuid' => $studentSubDetail->subscription_uuid])->one();
} elseif (!empty($step_data['step_1']['monthlysubscriptionfee_uuid'])) {
    $monthlySubscriptionDetail = MonthlySubscriptionFee::find()->where(['uuid' => $step_data['step_1']['monthlysubscriptionfee_uuid']])->one();
} else {
    return \yii\web\Controller::redirect(['index']);
}

$this->registerJsFile('https://js.stripe.com/v2/');
$this->registerJs(
        '
            $("#gostep1").submit(function(event) { 
                    blockBody();
            });  

            //card number validation
            $(document).on("input","#studentcreditcard-number", function(){
                    match = (/(\d{0,16})[^]*((?:\.\d{0,0})?)/g).exec(this.value.replace(/[^\d.]/g, ""));
                    this.value = match[1] + match[2];
            });
            
            //card cvv validation
            $(document).on("input","#studentcreditcard-cvv", function(){
                    match = (/(\d{0,4})[^]*((?:\.\d{0,0})?)/g).exec(this.value.replace(/[^\d.]/g, ""));
                    this.value = match[1] + match[2];
            });
            
            /*********************************/
            
            //set your publishable key
            Stripe.setPublishableKey("' . Yii::$app->params['stripe']['publishable_key'] . '");

            //callback to handle the response from stripe
                function stripeResponseHandler(status, response) {
                    if (response.error) {

                        $("#payBtn").removeAttr("disabled");
                        unblockBody();
                        $(".payment-errors").html(response.error.message);
                        showSuccess(response.error.message);
                    } else {
                        var form$ = $("#frm_step_2");

                        var token = response["id"];

                        form$.append("<input type=\"hidden\" name=\"stripeToken\" value=\""+token+"\" />");

                        form$.get(0).submit();
                    }
                }

             function submitform()
                {
                    $("#payBtn").attr("disabled", "disabled");
                    blockBody();
                    var dateText = $("#studentcreditcard-cc_month_year").val();
                    var pieces = dateText.split("-");
                    var year = (pieces[0] != undefined && pieces[0] > 0) ? pieces[0] : "1970";
                    var month = (pieces[1] != undefined && pieces[1] > 0) ? pieces[1] : "00";

                    var token = Stripe.createToken({
                        number: $("#studentcreditcard-number").val(),
                        cvc: $("#studentcreditcard-cvv").val(),
                        exp_month: month,
                        exp_year: year,
                    }, stripeResponseHandler);
                }
                $("#frm_step_2").submit(function (event) {  
                    //submit from callback
                    blockBody();
                    return false;
                });
                
                $(document).bind("cut copy paste","#studentcreditcard-number",function(e){
                    e.preventDefault();
                }); 
    $(".custom_expire_month_year").datepicker( {
        format: "yyyy-mm",
        viewMode: "months", 
        minViewMode: "months",
        startDate:new Date(),
        startView:2
    });

', View::POS_END
);
?>

<section class="content">
    <div class="row mar-bottom-10">
        <div class="col-xs-12">
            <div class="box">
                <div class="col-md-12 box-panel-header ">
                    <h4 class="col-md-6">Select Monthly Subscription &nbsp;<i class="fa fa-check"></i></h4>
                    <div class="col-md-6 ">
                        <?php echo Html::beginForm(['index'], 'post', ['id' => 'gostep1', 'class' => 'form-inline d-block']) ?>
                        <input type="hidden" name="step" value="1">
                        <input type="hidden" name="prev" value="1">
                        <a href="javascript:$('#gostep1').submit();" class="box-panel-header-right-btn pull-right">
                            Change
                        </a>
                        <?php echo Html::endForm() ?>
                        <p class="box-panel-header-right-p text-white pull-right"> <?= $monthlySubscriptionDetail->name; ?>&nbsp;</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row mar-bottom-10">
        <div class="col-xs-12">
            <div class="box">
                <div class="col-md-12 box-panel-header border-bottom-radious-none">
                    <h4 class="col-md-6">Payment</h4>
                    <div class="col-md-6 ">

                    </div>
                </div>
                <div class="box-body box-panel-body">


                    <div class="row vertical-center-box vertical-center-box-tablet" >
                        <?php
                        $form = ActiveForm::begin([
                                    'action' => ['index'],
                                    'id' => 'frm_step_2',
                                    'enableClientValidation' => true,
//                                'enableAjaxValidation' => true,
                                    'validateOnChange' => true,
                                    'options' => ['class' => 'form-horizontal']]);
                        ?>
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <div class="row" style="margin-top:10px;">

                                    <input type="hidden" name="step" value="2">
                                    <div class="payment-errors text-danger"></div><br>

                                </div>

                                <?=
                                        $form->field($model, 'name', ['template' => '<span>{label}</span>{input}{error}'])
                                        ->textInput(['class' => 'form-control demo', 'maxlength' => true,])
                                        ->label($model->getAttributeLabel('name') . '*')
                                ?>

                                <?=
                                        $form->field($model, 'number', ['template' => '<span>{label}</span>{input}{error}', 'inputOptions' => ['autocomplete' => 'new-password']])
                                        ->textInput(['class' => 'credit-card-number form-control', 'maxlength' => true,])
                                        ->label($model->getAttributeLabel('number') . '*')
                                ?>
                                <div class="padd_zero col-md-5">
                                    <?=
                                            $form->field($model, 'cvv', ['template' => '<span>{label}</span>{input}{error}', 'inputOptions' => ['autocomplete' => 'new-password']])
                                            ->passwordInput(['class' => 'form-control', 'maxlength' => true,])
                                            ->label($model->getAttributeLabel('cvv') . '*')
                                    ?>
                                </div>
                                <div class="padd_zero col-md-5 pull-right">
                                    <?=
                                            $form->field($model, 'cc_month_year', ['template' => '<span>{label}</span>{input}{error}'])
                                            ->textInput(['class' => 'form-control custom_expire_month_year', 'data-min-view-mode' => 'months', 'data-start-view' => '2', 'data-format' => 'yyyy-mm', 'maxlength' => true,])
                                            ->label($model->getAttributeLabel('cc_month_year') . '*')
                                    ?>
                                </div>


                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="checkbox icheck">
                                            <?php
                                            $model->save_future = 1;
                                            echo $form->field($model, 'save_future', ['template' => ' <label>{input} Save for future use</label>{error} '])->checkbox([
                                                'class' => 'skin-square-red float-left save_future'], false);
                                            ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="checkbox icheck">
                                            <?php
                                            echo $form->field($model, 'term_condition_checkbox', ['template' => ' <label>{input} I agree to Musiconns <a data-toggle="modal" data-target="#terms_condition_model"><b>Terms and Conditions</b></a></label>{error} '])->checkbox([
                                                'class' => 'skin-square-red float-left '], false);
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-md-4 pull-right">
                                        <?= Html::submitButton(Yii::t('app', 'Proceed Securely'), ['class' => 'btn btn-primary', 'id' => 'payBtn', 'onclick' => 'submitform()', 'style' => '']) ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <a class="btn btn-primary btn-sm black_button_in" href="<?= Yii::$app->getUrlManager()->createUrl('/student/profile'); ?>">RETURN TO PROFILE</a>
</section>

<style>

    .demo{
        border: 1px solid #bbb8b8;
    }
    .term_condition_checkbox_parent_col{
        margin-top: -17px;
    }
</style>

<!-- modal start -->
<div class="modal fade" id="terms_condition_model" tabindex="-1" role="dialog" aria-labelledby="ultraModal-Label" aria-hidden="true">
    <div class="modal-dialog animated zoomIn">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Terms and Conditions</h4>
            </div>

            <div class="modal-body">
                <?= \app\models\Settings::getSettingsByName('payment_terms_and_conditions'); ?>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-primary" type="button">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- modal end -->



