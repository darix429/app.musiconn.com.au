<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\models\StudentNotification;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Student;
use app\models\Modelmulti;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use \yii\base\Model;


/**
 * StudentNotificationController implements the CRUD actions for StudentNotification model.
 */
class StudentNotificationController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
        	'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => Yii::$app->user->can('/student-notification/index'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['validations'],
                        'allow' => Yii::$app->user->can('/student-notification/validations'),
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all StudentNotification models.
     * @return mixed
     */
    public function actionIndex()
    {
        $id = Yii::$app->user->identity->student_uuid;
        $model = Student::findOne($id);
        $model->scenario = 'student_notification';
        
        $allNoti = StudentNotification::find()->where(['student_uuid' => $model->student_uuid])->all();
        if(!empty($allNoti)){
            foreach ($allNoti as $k => $v) {
                $modelsNotifications[] = $v;
            }
        } else {
            $modelsNotifications = [new StudentNotification];
        }

        if ($model->load(Yii::$app->request->post())) {
            
            $oldIDs = ArrayHelper::map($modelsNotifications, 'uuid', 'uuid');
            $modelsNotifications = Modelmulti::createMultiple(StudentNotification::classname(), $modelsNotifications);
            Model::loadMultiple($modelsNotifications, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsNotifications, 'uuid', 'uuid')));
            
            // ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
            }

            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($modelsNotifications) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        if (! empty($deletedIDs)) {
                            StudentNotification::deleteAll(['uuid' => $deletedIDs]);
                        }
                        foreach ($modelsNotifications as $modelAddress) {
                            $modelAddress->student_uuid = $model->student_uuid;
                            $modelAddress->type = $modelAddress->type;
                            $modelAddress->time_unit = $modelAddress->time_unit;
                            $modelAddress->unit_value = $modelAddress->unit_value;
                            if (! ($flag = $modelAddress->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        Yii::$app->response->format = 'json';
                        $return = ['code' => 200, 'message' => 'Notifications updated successfully.'];
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            } else {
                Yii::$app->response->format = 'json';
                $validatemulti = ArrayHelper::merge(
                    ActiveForm::validateMultiple($modelsNotifications), ActiveForm::validate($model)
                );
                return ['code' => 500, 'message' => $validatemulti];
            }
            Yii::$app->response->format = 'json';
            return $return;
        }
        return $this->renderAjax('index', [
            'model' => $model,
            'modelsNotifications' => (empty($modelsNotifications)) ? [new StudentNotification] : $modelsNotifications
        ]);
    }

    public function actionValidations()
    {
        $id = Yii::$app->user->identity->student_uuid;
        $validateMulti = [];
        $model = Student::findOne($id);
        $model->scenario = 'student_notification';
        
        $allNoti = StudentNotification::find()->where(['student_uuid' => $model->student_uuid])->all();
        if(!empty($allNoti)){
            foreach ($allNoti as $k => $v) {
                $modelsNotifications[] = $v;
            }
        } else {
            $modelsNotifications = [new StudentNotification];
        }

        $model->attributes = Yii::$app->getRequest()->post('StudentNotification');

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
            $oldIDs = ArrayHelper::map($modelsNotifications, 'uuid', 'uuid');
            $modelsNotifications = Modelmulti::createMultiple(StudentNotification::classname(), $modelsNotifications);
            Model::loadMultiple($modelsNotifications, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsNotifications, 'uuid', 'uuid')));
            
            // ajax validation
            $validateMulti = ArrayHelper::merge(
                ActiveForm::validateMultiple($modelsNotifications), ActiveForm::validate($model)
            );
        }
        Yii::$app->response->format = 'json';
        return $validateMulti;
    }

    /**
     * Finds the StudentNotification model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return StudentNotification the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = StudentNotification::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
