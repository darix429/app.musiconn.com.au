<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Instrument */

$this->title = Yii::t('app', 'Create Instrument');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Instruments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?=

$this->render('_form', [
    'model' => $model,
    'new' => true,
])
?>


