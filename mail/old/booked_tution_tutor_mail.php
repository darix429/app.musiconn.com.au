<?php

use yii\helpers\Html;
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['tutor_name']; ?>,</p>
            
            <p>Please be advised details of tutorials booked through the Musiconn Booking System:</p>
            
            <p><strong>Scheduled tutorials</strong></p>
            <p><strong>Booking Reference :</strong> <?= "#".$content['booking_id']; ?></p>
            <p><strong>Tutor Name :</strong> <?= $content['tutor_name']; ?></p>
            <p><strong>Student Name :</strong> <?= $content['student_name']; ?></p>
            <p><strong>Instrument :</strong> <?= $content['instrument']; ?></p>
            <p><strong>Scheduled Lessons :</strong> </p>
                <?php if(is_array($content['session_dates'])){ ?>
                <ul>
                    <?php foreach ($content['session_dates'] as $key => $value) {
                        echo "<li>".$value." ".$content['time']."</li>";
                    } ?>
                </ul>
                <?php } ?>
            <p><strong>Subscription to online contents :</strong> <?= (!empty($content['student_monthly_subscription_uuid']) ) ? "Yes" : "No"; ?></p>
                       
            <p>Please do not reply to this unattended mailbox. Any queries or feedback can be directed to admin@musiconn.com.au.</p>

            <p>Regards,<br>
            Musiconn Booking System</p>
        </td>
    </tr>
</table>

