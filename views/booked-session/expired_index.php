<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;
use app\models\Tutor;
use app\models\BookedSession;
use app\libraries\General;


$this->title = Yii::t('app', 'Missed Lessons');
$this->params['breadcrumbs'][] = $this->title;


$this->registerJs(
   '
$(document).on("click","#search_btn", function(){        
    $.ajax({
        type: "GET",
        url: "'.Url::to(['/booked-session/expired/']).'",
        data: $("#search_frm").serialize(),
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            $("#expired_tution").html(result);
            window.ULTRA_SETTINGS.dataTablesInit();
        },
        error:function(e){
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});

$(document).on("click","#search_reset_btn", function(){        
    $.ajax({
        type: "GET",
        url: "'.Url::to(['/booked-session/expired/']).'",
        data: {},
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            $("#expired_tution").html(result);
            window.ULTRA_SETTINGS.dataTablesInit();
        },
        error:function(e){
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});

$(".datefilter").daterangepicker({
      autoUpdateInput: false,
      locale: {
          cancelLabel: "Clear",
          format: "DD-MM-YYYY"
      },ranges : {
                    "Today": [moment(), moment()],
                    "Yesterday": [moment().subtract("days", 1), moment().subtract("days", 1)],
                    "Last 7 Days": [moment().subtract("days", 6), moment()],
                    "Last 30 Days": [moment().subtract("days", 29), moment()],
                    "This Month": [moment().startOf("month"), moment().endOf("month")],
                    "Last Month": [moment().subtract("month", 1).startOf("month"), moment().subtract("month", 1).endOf("month")]
                }
  });

  $(".datefilter").on("apply.daterangepicker", function(ev, picker) {
      $(this).val(picker.startDate.format("DD-MM-YYYY") + " - " + picker.endDate.format("DD-MM-YYYY"));
  });

  $(".datefilter").on("cancel.daterangepicker", function(ev, picker) {
      $(this).val("");
  });

',View::POS_END
);
?>

<div class="col-xl-12">
    <section class="box">
        <header class="panel_header">
            <h2 class="title float-left">Missed Lessons History</h2>
            <div class="actions panel_actions float-right">
                <i class="fa fa-search icon-primary icon-xs icon-orange mail_head_icon custom-toggle" data-togglediv="datatable-search-div" title="Advance Search"></i>

            </div>
        </header>
        <div class="content-body">    
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12">
                    <div class="datatable-search-div " style="display: none;">
                    
                        <?php $form = ActiveForm::begin([
                            'method' => 'get',
                            'options' => [
                                'id' => 'search_frm',
                                'class' => 'form-inline'
                            ],
                        ]); ?>
                    
                        <div class="col-md-4 mb-0">
                            <?php
                            (Yii::$app->user->identity->role == 'TUTOR') ? $placeholder = 'Student Name' : $placeholder = 'Tutor Name';
                            (Yii::$app->user->identity->role == 'TUTOR') ? $field = 'student_name' : $field = 'tutor_name';
                            ?>
                            <?= $form->field($searchModel, $field)->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $placeholder])->label(false) ?>
                        </div>
                        <div class="col-md-4 mb-0">
                            <?= $form->field($searchModel, 'start_daterange')->textInput(['value' => '','class' => 'form-control datefilter', 'maxlength' => true, 'placeholder' => 'Select Date', 'data-format' => 'DD-MM-YYYY' ])->label(false) ?>
                        </div>
                        <br><br>
                                   
                        <?= Html::Button(Yii::t('app', 'Search'), ['class' => 'btn btn-primary mb-0', 'id'=>'search_btn','style'=>'margin-left: 17px;']) ?>
                        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-purple mb-0', 'id'=>'search_reset_btn']) ?>
                
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
            <br>
            <div class=""></div>
            <div class="clearfix"></div>
                <div class="row" id="expired_tution">
                    <?php
                    (Yii::$app->user->identity->role == 'STUDENT') ? $renderPartial = 'student_expired' : $renderPartial = 'tutor_expired';
                    echo Yii::$app->controller->renderPartial($renderPartial, ['expiredList' => $expiredList]);
                    ?>
                </div>

            <!-- </div> -->

        </div>
    <!-- </section> -->
</div>
