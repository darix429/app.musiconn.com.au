<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;

//echo "<pre>";
//print_r($model->getErrors());
//echo "</pre>";
?>
<style>
    #reschedule_approve_form div.required label.control-label:after{
        content: "";
    }
</style>
<div class=" border border-top-2 border-info pb-2" style="border-width: 2px !important; ">
    <h5 class="list-group-item text-center text-white mt-0 mb-3" style="background-color: #17a2b8;" >Status</h5>
    <div class="form-row align-items-center1">
        <div class="col-sm-12">
            <input type="hidden" id="reschedule_approve_form_action" name="reschedule_approve_form_action" value="<?= Url::to(['/book-music-tution/reschedule-request-approval/','id' => $model->uuid]) ?>" />
            <div class="text-center">
                <button type="button" class="btn btn-success" onclick="save_reschedule_approve_form();">Approve</button>
                <button type="button" class="btn btn-danger" onclick="save_reschedule_reject_form();">Reject</button>
            </div>
        </div>
        
    </div>
</div>
