<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\Admin */

$this->title = Yii::t('app', 'Promo Code : ' . $model->code, [
    'nameAttribute' => '' . $model->code,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Promo Codes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->uuid, 'url' => ['view', 'id' => $model->uuid]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');


?>

<?=

$this->render('_form', [
    'model' => $model,
    'new' => false,
])
?>
