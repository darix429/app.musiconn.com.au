<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;
//rupal
use yii\helpers\ArrayHelper;
use app\models\InstrumentCategory;

$parentIdArray = ArrayHelper::map(InstrumentCategory::find()->all(), 'uuid', 'name');

$this->title = Yii::t('app', 'Instrument Categories');
$this->params['breadcrumbs'][] = $this->title;


$this->registerJs(
        '
    
//delete code 
    $(document).on("click",".delete_instrument_category_link", function(){
        if(confirm("Are you sure, you want to delete this instrument category?")){
        var element = $(this);
            $.ajax({
                type: "POST",
                url: "' . Url::to(['/instrument-category/delete/']) . '",
                data: { "id": $(this).data("uuid")},
                beforeSend:function(){ blockBody(); },
                success:function(result){
                    if(result.code == 200){
                        unblockBody();
                        $(element).closest("tr").remove();
                        
                        showSuccess(result.message);
                    }else{
                        unblockBody();
                        showErrorMessage(result.message);
                    }
                },
                error:function(e){
                    unblockBody();
                    showErrorMessage(e.responseText);
                }
            });
        }
    });
    
$(document).on("click","#search_btn", function(){        
    $.ajax({
        type: "GET",
        url: "' . Url::to(['/instrument-category/index/']) . '",
        data: $("#search_frm").serialize(),
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            $("#instrument_category_table_parent_div").html(result);
            window.ULTRA_SETTINGS.dataTablesInit();
        },
        error:function(e){ 
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});
    
$(document).on("click","#search_reset_btn", function(){        
    $.ajax({
        type: "GET",
        url: "' . Url::to(['/instrument-category/index/']) . '",
        data: {},
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            $("#instrument_category_table_parent_div").html(result);
            window.ULTRA_SETTINGS.dataTablesInit();
        },
        error:function(e){ 
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});

    
', View::POS_END
);
?>

<div class="col-xl-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Instrument Categories List</h2>
            <div class="actions panel_actions float-right">
                <i class="fa fa-search icon-primary icon-xs icon-orange mail_head_icon custom-toggle" data-togglediv="datatable-search-div" title="Advance Search"></i>
                <a href="<?= Yii::$app->getUrlManager()->createUrl(['/instrument-category/create/']) ?>" class="btn btn-primary "><i class="fa fa-plus text-white"></i></a>
            </div>
        </header>
        <div class="content-body"> 
            <div class="row ">
                <div class="col-lg-12 col-md-12 col-12 ">
                    <div class="datatable-search-div " style="display: none;">

                        <?php
                        $form = ActiveForm::begin([
                                    'method' => 'get',
                                    'options' => [
                                        'id' => 'search_frm',
                                        'class' => 'form-inline'
                                    ],
                        ]);
                        ?>

                        <div class="col-md-3 mb-0">
<?= $form->field($searchModel, 'name')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Name'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
<?= $form->field($searchModel, 'level')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'level', 'type' => 'number'])->label(false) ?>
                        </div>

                        <?= Html::Button(Yii::t('app', 'Search'), ['class' => 'btn btn-primary mb-0', 'id' => 'search_btn', 'style' => 'margin-left: 17px;']) ?>
                        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-purple mb-0', 'id'=>'search_reset_btn']) ?>
                        
<?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
            <br>
            <div class=""></div>
            <div class="clearfix"></div>
            <div class="row" id="instrument_category_table_parent_div">
<?php echo Yii::$app->controller->renderPartial('_instrument_categories_list', ['instrumentCategoryList' => $instrumentCategoryList]); ?>
            </div>
        </div>
    </section> 
</div>

