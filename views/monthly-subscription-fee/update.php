<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
//rupal
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\Admin */

$this->title = Yii::t('app', 'Monthly Subscription Fee : ' . $model->name, [
    'nameAttribute' => '' . $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Monthly Subscription Fees'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->uuid, 'url' => ['view', 'id' => $model->uuid]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<style>
.input-group .form-control{
	width: 287px;
}
</style>

<div class="col-xl-12 col-lg-12 col-12 col-md-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Please Fill Information</h2>
            <div class="actions panel_actions float-right">
                <!--<i class="box_toggle fa fa-chevron-down"></i>-->
                <a href="<?= Yii::$app->getUrlManager()->createUrl(['/monthly-subscription-fee/index/'])?>" class="btn btn-info btn-icon "><i class="fa fa-arrow-left text-white"></i> <span>Back</span></a>
            </div>
        </header>
        <div class="content-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12">
                    <?php $form = ActiveForm::begin(); ?>
                    <div class="form-row">
                        <div class="col-md-6 mb-0">
                            <?= $form->field($model, 'name')->textInput(['class' => 'form-control code', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('name')]) ?>
                        </div>
                        <div class="col-md-6 mb-0">
                            <?= $form->field($model, 'status')->dropDownList(['ENABLED' => 'ENABLED', 'DISABLED' => 'DISABLED'], ['class' => 'form-control']) ?>
                        </div>
			<!--<div class="col-md-4 mb-0">
			    <label class='control-label' for='monthlysubscriptionfee-with_tutorial'>Is Tutorial</label>-->
                            
                            
                            <?= $form->field($model, 'with_tutorial')->hiddenInput(['class' => 'form-control', 'maxlength' => true,'value'=>'0'])->label(false) ?>
                            
                            <?php //echo  $form->field($model, 'with_tutorial')->checkbox(['class' => 'form-control iswitch iswitch-lg iswitch-primary','label' => null]) ?>
                        <!--</div>-->
			                         
                    </div>
                    <div class="form-row">
                        <div class="col-md-4 mb-0">
				 <?= $form->field($model, 'price',['template'=>'{label}<div class="input-group"><div class="input-group-prepend" ><span class="input-group-text">$</span></div>{input}{error}</div>']
				)->textInput(['class' => 'form-control common','id'=>'price', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('price'),'onchange'=>'myFunction()','onkeypress'=>'myFunction()', 'aria-label'=>'Amount (to the nearest dollar)','style'=>'text-align:right;']) ?>
			</div>
			<div class="col-md-4 mb-0">
				 <?= $form->field($model, 'tax',['template'=>'{label}<div class="input-group"><div class="input-group-prepend" ><span class="input-group-text">$</span></div>{input}{error}</div>']
				)->textInput(['class' => 'form-control common','id'=>'tax', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('tax'),'onchange'=>'myFunction()','onkeypress'=>'myFunction()', 'aria-label'=>'Amount (to the nearest dollar)','style'=>'text-align:right;']) ?>
			</div>
                        <div class="col-md-4 mb-0">
				 <?= $form->field($model, 'total_price',['template'=>'{label}<div class="input-group"><div class="input-group-prepend" ><span class="input-group-text">$</span></div>{input}{error}</div>']
				)->textInput(['class' => 'form-control common','id'=>'total_price', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('total_price'), 'aria-label'=>'Amount (to the nearest dollar)','style'=>'text-align:right;', 'readonly'=>'true']) ?>
			</div>
			
		    </div>
		    <div class="form-row">
                        <div class="col-md-12 mb-0">
                            <?= $form->field($model, 'description')->textarea(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('description')]) ?>
                        </div>
                                                                              
                    </div>

                    <?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-primary', 'style' => 'float: right;']) ?>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>

        </div>
    </section>
</div>

<?php 
//$this->registerJsFile('http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js');
$this->registerJs(
   '
	
   	$(function() {
	  $(".common").on("input", function() {
	    match = (/(\d{0,10})[^.]*((?:\.\d{0,2})?)/g).exec(this.value.replace(/[^\d.]/g, ""));
	    this.value = match[1] + match[2];
	  });
	});

	function myFunction() { 
		var price = parseFloat("0"+$("#price").val());
		var tax = parseFloat("0"+$("#tax").val());
		var price_str = price.toString();
		var price_array = price_str.split(".");
		var price_1 = price_array[0];
		var price_2 = (price_array[1] > 0)?price_array[1]:"";
		var tax_str = tax.toString();
		var tax_array = tax_str.split(".");
		var tax_1 = tax_array[0];
		var tax_2 = (tax_array[1] > 0)?tax_array[1]:"";
		//alert(price_1.length);

		if(price_1.length <= 10 && price_2.length <= 2 && tax_1.length <= 10 && tax_2.length <= 2){
			$("#total_price").val((price + tax).toFixed(2));
		}
	}  
    
',View::POS_END
);
?>




