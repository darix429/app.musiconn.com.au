<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "musicoin_coupon".
 *
 * @property string $uuid
 * @property string $code
 * @property string $description
 * @property string $coins
 * @property string $start_date
 * @property string $end_date
 * @property string $status
 * @property string $min_amount
 * @property string $created_at
 * @property string $updated_at
 */
class MusicoinCoupon extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'musicoin_coupon';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //html tag filter
            [['code',], 'match', 'pattern' => \app\libraries\General::validTextPattern(), 'message' => 'Invalid characters used.'], 
            [['description',], 'match', 'pattern' => \app\libraries\General::validTextPattern(['@','$','(',')','\[','\]','\/',]), 'message' => 'Invalid characters used.'], 
            
            [['code', 'coins', 'description', 'start_date', 'end_date', 'created_at', 'status'], 'required', ],
            //, 'min_amount'
            [['uuid', 'code', 'description', 'status'], 'string'],
            [['coins', ], 'integer'],
            [['coins', 'min_amount'], 'number'],
            [['coins', 'min_amount'], 'default', 'value' => 0],
            [['coins',], 'match', 'pattern' => '/^\d{0,10}?$/', 'message' => 'Only allow digit not allow decimal digit.'],
            [['start_date', 'end_date', 'created_at', 'updated_at'], 'safe'],
            [['uuid'], 'unique'],
            ['start_date',function ($attribute, $params) {
                $start_date = date("Y-m-d H:i:00",strtotime($this->start_date));
                $end_date = date("Y-m-d H:i:00",strtotime($this->end_date));
                if ($start_date > $end_date) {
                    $this->addError($attribute, 'Start date must be less than End date.');
                }
            }],
            ['end_date',function ($attribute, $params) {
                $start_date = date("Y-m-d H:i:00",strtotime($this->start_date));
                $end_date = date("Y-m-d H:i:00",strtotime($this->end_date));
                if ($end_date < $start_date) {
                    $this->addError($attribute, 'End date must be greater than Start date.');
                }
            }],
            ['start_date','compare','compareValue'=> date('d-m-Y'),'operator'=>'>', 'on' => 'coupon_create','message' => 'Start Date must be greater than today date.'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'code' => Yii::t('app', 'Code'),
            'description' => Yii::t('app', 'Description'),
            'coins' => Yii::t('app', 'Coins'),
            'start_date' => Yii::t('app', 'Start Date'),
            'end_date' => Yii::t('app', 'End Date'),
            'status' => Yii::t('app', 'Status'),
            'min_amount' => Yii::t('app', 'Min Amount'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
