<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Url;
use app\libraries\General;
use app\models\StudentTuitionBooking;
use app\models\Instrument;
use app\models\Tutor;

$this->title = Yii::t('app', 'BOOK A MUSIC LESSON');
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_icon'] = 'my-lesson-tag.png';

$instrumentList = StudentTuitionBooking::getInstrumentList();


if (!empty($step_data['step_1']['instrument_uuid'])) {
    $instrumentDetail = Instrument::find()->where(['uuid' => $step_data['step_1']['instrument_uuid']])->one();
} else {
    return \yii\web\Controller::redirect(['index']);
}
//print_r($step_data['step_2']);
if (isset($step_data['step_2']['tutor_uuid']) && $step_data['step_2']['tutor_uuid'] != "") {
    $tutorDetail = Tutor::findOne($step_data['step_2']['tutor_uuid']);
    $model->tutor_uuid = $step_data['step_2']['tutor_uuid'];
    $model->tutorcheckbox = $model->tutor_uuid;
} else {
    return \yii\web\Controller::redirect(['index']);
}

if (empty($step_data['step_3']['lesson_length']) || empty($step_data['step_3']['lesson_total'])) {
    return \yii\web\Controller::redirect(['index']);
}

$this->registerJs(
        '
    $( "#frm_step_5" ).submit(function( event ) {
        blockBody();
    });
function cart_refresh(loader_hide = true){

    var monthly_sub_check = ($("#book-monthly_sub_checkbox").is(":checked")) ? "yes" : "no";
    var monthly_sub_uuid = $("#book-monthly_sub_uuid").val();
    var coupon_code = $("#book-coupon_code").val();
    $.ajax({
        type: "GET",
        url: "' . Url::to(['/book/refresh-cart/']) . '",
        data: { "monthly_sub_check": monthly_sub_check,"monthly_sub_uuid":monthly_sub_uuid,"coupon_code":coupon_code},
        beforeSend: function() {
            blockBody();
        },
        success:function(result){

            if(result.code == 200){

                $("#book-cart_total").val(result.result.cart_total);
               $("#cart_total_textd").html(result.result.cart_total);
 
                $("#book-coupon_uuid").val(result.result.coupon.coupon_uuid);
                $("#book-discount").val(result.result.coupon.discount);
                $("#discount_textd").html(result.result.coupon.discount);

                if(result.result.coupon.coupon_code != ""){
                    $(".promo-code-msg").text(result.result.coupon.message);
                }
                if(result.result.coupon.is_available){
                    $(".promo-code-msg").removeClass("text-danger");
                    $(".promo-code-msg").addClass("text-success");
                }else{
                    $(".promo-code-msg").removeClass("text-success");
                    $(".promo-code-msg").addClass("text-danger");
                }

               /* if(result.result.monthly_sub_check != "yes"){
                    $("#monthly_sub_price_text_parent").hide();
                }else{
                    $("#monthly_sub_price_text_parent").show();
                }*/
                if(loader_hide){
                    unblockBody();
                }
            }else{
                unblockBody();
                showErrorMessage(result.message);
            }
        },
        error:function(e){
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
}
cart_refresh();
        ', View::POS_END
);
?>

<?= Yii::$app->controller->renderPartial('step_prev_booking', ['model' => $model]); ?>

<!--<div class="new_when_startdate"></div>-->

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-12"><strong>BOOK A NEW MUSIC LESSON</strong></div><br>
            <div class="panel-group box-header with-border" id="accordion" role="tablist" aria-multiselectable="true">

                <!-- SELECT INSTRUMENT -->
                <div class="panel panel-default">

                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseInstrument" aria-expanded="false" aria-controls="collapseTwo">
                                SELECT INSTRUMENT 
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </a>
                            <span class="pull-right"><?= $instrumentDetail->name; ?> <a href="javascript:$('#gostep1').submit();"><div class="label label-warning hm-1 mr-2 siba">CHANGE</div></a></span>
                        </h4>
                    </div>
                </div>
                <!-- END SELECT INSTRUMENT -->

                <!-- SELECT TUTOR -->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseLength" aria-expanded="false" aria-controls="collapseThree">
                                SELECT TUTOR
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </a>
                            <span class="pull-right"><?= $tutorDetail->first_name . ' ' . $tutorDetail->last_name; ?> <a href="javascript:$('#gostep2').submit();"><div class="label label-warning hm-1 mr-2 siba">CHANGE</div></a></span>
                        </h4>
                    </div>

                </div>
                <!-- END SELECT TUTOR -->

                <!-- SELECT LENGTH OF LESSON -->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingfour">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseLength" aria-expanded="false" aria-controls="collapsefour">
                                SELECT LENGTH OF LESSON
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </a>
                            <span class="pull-right"><?= $step_data['step_3']['lesson_length'] . ' Mins - ' . $step_data['step_3']['lesson_total'] . ($step_data['step_3']['lesson_total'] > 1? ' Lessons' : ' Lesson'); ?> <a href="javascript:$('#gostep3').submit();"><div class="label label-warning hm-1 mr-2 siba">CHANGE</div></a></span>
                        </h4>
                    </div>
                </div>
                <!--END SELECT LENGTH OF LESSON -->

                <!-- SELECT DATE AND TIME -->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingfive">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseDate" aria-expanded="false" aria-controls="collapsefive">
                                SELECT DATE AND TIME
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </a>
                            <span class="pull-right"> 
                                <?php
                                $count = json_decode($step_data['step_4']['all_lesson_data'], true);
                                $available = 0;
                                $unavailable = 0;
                                foreach ($count as $value) {
                                    if ($value['is_available'] == "YES") {
                                        $available++;
                                    } else {
                                        $unavailable++;
                                    }
                                }

                                //echo "<pre>";print_r(array_count_values(json_decode($step_data['step_4']['all_lesson_data'])));echo "</pre>";
//                               array_keys(
                                //$count = array_count_values(json_decode($step_data['step_4']['all_lesson_data'],true));
                                ?>
                                <?php echo 'Total Selected '.($available > 1 ? 'Lessons' : 'Lesson') . ' - ' . $available ; ?>
                                <?php // "Total Lesson-" . $available; ?>
                                <a href="javascript:$('#gostep4').submit();"><div class="label label-warning hm-1 mr-2 siba">CHANGE</div></a></span>
                        </h4>
                    </div>
                </div>
                <!--END SELECT DATE AND TIME -->

                <!-- CONFIRM BOOKING AND PAYMENT -->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingsix">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsePayment" aria-expanded="false" aria-controls="collapsesix">
                                CONFIRM BOOKING AND PAYMENT
                            </a>
                        </h4>
                    </div>
                    <div class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingFive">
                        <?php
                        $form = ActiveForm::begin([
                                    'action' => ['index'],
                                    'id' => 'frm_step_5',
                                    'enableClientValidation' => true,
                                    'validateOnChange' => true,
                                    'options' => ['class' => 'form-horizontal']]);
                        ?>
                        <div class="panel-body">
                            <ul class="products-list product-list-in-box">
                                <li>
                                    <div class="col-md-4">
                                        <div class="sst_btn sst_btn2">
                                            <h4>BOOKING INFORMATION</h4>
                                            <div><strong>Instrument:</strong> <?= $instrumentDetail->name ?></div>
                                            <div><strong>Lesson Length:</strong> <?= $step_data['step_3']['lesson_length'] . ' Minutes' ?></div>
                                            <div><strong>Total Lesson Occurrence:</strong> <?= $available ?></div>
                                            <?php
                                            $count = json_decode($step_data['step_4']['all_lesson_data'], true);

                                            $tutorwiselessonDetails = [];

                                            foreach ($count as $value) {
                                                $tutorwiselessonDetails[$value['tutor_uuid']][] = $value;
                                            }
                                            //echo "<pre>";print_r($tutorwiselessonDetails);echo "</pre>";
                                            foreach ($tutorwiselessonDetails as $uuid => $value) {
                                                //echo $uuid;
                                                $tutorDetails = Tutor::findOne($uuid);
                                                if (!empty($value)) {
                                                    echo "<br><div><strong>Tutor:</strong> " . $tutorDetails->first_name . " " . $tutorDetails->last_name . "</div>";
                                                    echo "<div><strong>Sessions:</strong></div>";
                                                    foreach ($value as $sessonDetails) {
                                                        if ($sessonDetails['is_available'] == "YES") {
                                                            echo "<div>" . date('d M Y - h:i A', strtotime($sessonDetails['lesson_time'])) . "</div>";
                                                        }
                                                    }
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="sst_btn sst_btn2" style="display: inline-block;">
                                            <div class="col-md-8">
                                                <h4>BOOKING INFORMATION</h4>
                                                <?php 
                                                    $isHourly = ($step_data['step_3']['lesson_length'] == 60) ? true: false;
                                                    $coinRate = $isHourly? $step_data['step_2']['tutor_hour_price_in_coin'] : $step_data['step_2']['tutor_half_hour_price_in_coin'] ;
                                                
                                                ?>
                                                <div><strong>Tutor Rate</strong> <?php echo $tutorCoinRate = ($step_data['step_3']['lesson_length'] == 60) ? $step_data['step_2']['tutor_hour_price_in_coin'] . ' Musicoins/' . $step_data['step_3']['lesson_length'] . " Mins" : $step_data['step_2']['tutor_half_hour_price_in_coin'] . ' Musicoins/' . $step_data['step_3']['lesson_length'] . " Mins"; ?></div>
                                                <div><strong>Total</strong> <span id="plan_total_text"><?php
                                                        $total = $coinRate * $available;
                                                        echo "$total Musicoins";
                                                        ?></span></div>
                                                <!--<div><strong>Discount</strong> <span id="discount_textd"> 0 </span> Musicoins</div>-->
                                                <div><strong>Amount Payable</strong> <span id="cart_total_textd"><?= $model->cart_total; ?></span><?php echo ' Musicoins'; ?></div>
                                                <?php
                                                $studentHasCoin = \app\libraries\General::getStudentMusicoin(Yii::$app->user->identity->student_uuid);
                                                $addonCoin = ($total > $studentHasCoin) ? true : false;
                                                if ($addonCoin) {
                                                    echo "<div><small><strong style='color:red'>You have insufficient musicoins. Please reload your wallet.</strong> </small></div>";
                                                }
                                                ?>
                                                <div class="col-md-12">
                                                    <ul class="products-list product-list-in-box">
                                                        <li class="st-p">
                                                            <div class="checkbox contai_r" style="padding-left:0">
                                                                <?php
                                                                echo $form->field($model, 'term_condition_checkbox', ['template' => ' {input} I agree to Musiconns <br><a style="margin-left: 26px;" data-toggle="modal" data-target="#terms_condition_model"><b>Terms and Conditions</b></a>{error} '])->radio([
                                                                    'class' => 'skin-square-red float-left '], false);
                                                                ?>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-12" style="visibility: hidden; display: none;">
                                                    <div class="col-md-6" style="padding-left: 0;">
                                                        <div class="input-form-group">
                                                            <?=
                                                                    $form->field($model, 'coupon_code', ['inputOptions' => ['autocomplete' => 'off']])
                                                                    ->textInput(['class' => 'form-control my_input_border pull-left', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('coupon_code')])->label(false);
                                                            ?>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-6" style="padding-right: 0;">
                                                        <div class="input-form-group">
                                                            <button onclick="cart_refresh();" type="button" class="btn btn-primary">APPLY</button>
                                                        </div>
                                                    </div><div class="col-md-12" style="padding-left: 0;"><p class="promo-code-msg "></p></div>
                                                </div>

                                                <div class="hidden_inputs">

                                                    <?= $form->field($model, 'discount', ['template' => '{input}{error}'])->hiddenInput()->label(false); ?>
                                                    <?= $form->field($model, 'cart_total', ['template' => '{input}{error}'])->hiddenInput()->label(false); ?>
                                                    <?= $form->field($model, 'coupon_uuid', ['template' => '{input}{error}'])->hiddenInput()->label(false); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <p class="">My Musicoins <?= $studentHasCoin; ?></p>
                                                <?php
                                                echo '<button style="display:' . ($addonCoin ? 'block' : 'none') . '" class="btn btn-primary btn-sm black_button_in3" type="button" onclick="buy_musicoin();">ADD MUSICOINS</button>';

                                                echo '<button ' . ($addonCoin ? 'disabled' : '') . ' class="btn btn-primary btn-sm black_button_in2" type="submit">CONFIRM PAYMENT</button>';
                                                ?>
                                                <!--<button style="display:'.($addonCoin?'block':'none').'"class="btn btn-primary btn-sm black_button_in2" type="submit">CONFIRM PAYMENT</button>-->
                                            </div>
                                        </div>
                                        <div><br></div>
                                        <div class="sst_btn sst_btn2" style="visibility: hidden; display: none;">
                                            <div class="col-md-12">
                                                <h3>GOVERNMENT VOUCHERS</h3>
                                                <p>Many States and Territories offer discount and voucher programs designed to support kids getting into creative arts such as music.</p>

                                                <p style="border: 1px #0c9dca solid; border-left: 3px #0c9dca solid; padding: 5px;background-color: #c1e6fd;color: #066684;">
                                                    <strong>Info:</strong> Please enter the participant's name, voucher code, date of birth, and postcode exactly as shown on the voucher.
                                                </p>

                                                <div class="col-md-12" style="padding-left: 0;">


                                                    <div class="col-md-6" style="padding-left1: 0;">
                                                        <div class="input-form-group">
                                                            <?=
                                                                    $form->field($model, 'kids_name', ['inputOptions' => ['autocomplete' => 'off', ['class' => 'form-group invisible']]
                                                                    ])
                                                                    ->textInput(['class' => 'form-control ', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('kids_name')])
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6" style="padding-right1: 0;">
                                                        <div class="input-form-group">
                                                            <?=
                                                                    $form->field($model, 'creative_kids_voucher_code', ['inputOptions' => ['autocomplete' => 'off']])
                                                                    ->textInput(['class' => 'form-control my_input_border pull-left', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('creative_kids_voucher_code')])
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12" style="padding-left: 0;">
                                                    <div class="col-md-6" style="padding-left1: 0;">
                                                        <div class="input-form-group">
                                                            <?=
                                                            $form->field($model, 'kids_postal_code')->textInput(['class' => 'form-control ', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('kids_postal_code')]);
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6" style="padding-right1: 0;">
                                                        <div class="input-form-group">
                                                            <?=
                                                            $form->field($model, 'kids_dob')->textInput(['readonly' => true, 'class' => 'form-control custom_date_picker', 'maxlength' => true, 'placeholder' => 'DD-MM-YYYY']);
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <input type="hidden" name="step" value="5">
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
                <!--END CONFIRM BOOKING AND PAYMENT -->

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <a class="btn btn-primary btn-sm black_button_in" href="<?= Yii::$app->getUrlManager()->createUrl('/student/home'); ?>">RETURN TO HOME</a>
        </div>
    </div>
</section>

<div class="other_forms">
    <?php echo Html::beginForm(['index'], 'post', ['id' => 'gostep1', 'class' => 'form-inline d-block']) ?>
    <input type="hidden" name="step" value="1">
    <input type="hidden" name="prev" value="1">
    <?php echo Html::endForm() ?>
    <?php echo Html::beginForm(['index'], 'post', ['id' => 'gostep2', 'class' => 'form-inline d-block']) ?>
    <input type="hidden" name="step" value="2">
    <input type="hidden" name="prev" value="2">
    <?php echo Html::endForm() ?>
    <?php echo Html::beginForm(['index'], 'post', ['id' => 'gostep3', 'class' => 'form-inline d-block']) ?>
    <input type="hidden" name="step" value="3">
    <input type="hidden" name="prev" value="3">
    <?php echo Html::endForm() ?>
    <?php echo Html::beginForm(['index'], 'post', ['id' => 'gostep4', 'class' => 'form-inline d-block']) ?>
    <input type="hidden" name="step" value="4">
    <input type="hidden" name="prev" value="4">
    <?php echo Html::endForm() ?>
</div>
