<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\View;

$this->registerJs(
   '
       var doller_value = '.Yii::$app->params['musicoin']['doller_value'].';

       function bonusCoinsToPrice() {
            var bonus_coins = $("#musicoinpackage-bonus_coins").val();
            bonus_coins = (bonus_coins === "")? 0 : bonus_coins;
            var bonus_coins_amount = ( parseFloat(bonus_coins) * parseFloat(doller_value) ).toFixed(2);
            $("#musicoinpackage-bonus_coins").val(bonus_coins);
            $("#musicoinpackage-bonus_coins_amount").val(bonus_coins_amount);
        }

	function calculatePrice() { 
		var price = parseFloat("0"+$("#musicoinpackage-price").val());
		var gst = parseFloat("0"+$("#musicoinpackage-gst").val());
		var price_str = price.toString();
		var price_array = price_str.split(".");
		var price_1 = price_array[0];
		var price_2 = (price_array[1] > 0)?price_array[1]:"";
		var gst_str = gst.toString();
		var gst_array = gst_str.split(".");
		var gst_1 = gst_array[0];
		var gst_2 = (gst_array[1] > 0)?gst_array[1]:"";
		//alert(price_1.length);

		if(price_1.length <= 10 && price_2.length <= 2 && gst_1.length <= 10 && gst_2.length <= 2){
                        var total_price = (price + gst).toFixed(2);
			$("#musicoinpackage-total_price").val(total_price);
                        
                        // Set total price into 
                        var musicoins =  ( (1 / parseFloat(doller_value)) * parseFloat(total_price) );
                        var final_musicoins = Math.ceil( musicoins );   
                        $("#musicoinpackage-coins").val(final_musicoins);
		}
	} 
	function calculateCoins() { 
		var price = parseFloat("0"+$("#musicoinpackage-coins").val());
		var gst = parseFloat("0"+$("#musicoinpackage-bonus_coins").val());
		var price_str = price.toString();
		var price_array = price_str.split(".");
		var price_1 = price_array[0];
		var price_2 = (price_array[1] > 0)?price_array[1]:"";
		var gst_str = gst.toString();
		var gst_array = gst_str.split(".");
		var gst_1 = gst_array[0];
		var gst_2 = (gst_array[1] > 0)?gst_array[1]:"";
		//alert(price_1.length);

		if(price_1.length <= 10 && price_2.length <= 2 && gst_1.length <= 10 && gst_2.length <= 2){
			$("#musicoinpackage-total_coins").val((price + gst).toFixed(0));
		}
                
                bonusCoinsToPrice();
	} 
        
        function change_anyvalue(){
            calculatePrice();
            calculateCoins();
        }
        
    
',View::POS_END
);
?>
<style>
/*.input-group .form-control{
	width: 285px;
}*/
 .semi-bold {
        font-weight: 900;
    }
</style>

<div class="col-xl-12 col-lg-12 col-12 col-md-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Please Fill Information</h2>
            <div class="actions panel_actions float-right">
                <!--<i class="box_toggle fa fa-chevron-down"></i>-->
                <a href="<?= Yii::$app->getUrlManager()->createUrl(['/musicoin-package/index/'])?>" class="btn btn-info btn-icon "><i class="fa fa-arrow-left text-white"></i> <span>Back</span></a>
            </div>
        </header>
        <div class="content-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12">
                    <?php $form = ActiveForm::begin(); ?>
                    <div class="form-row">
                        <div class="col-md-8 mb-0">
                            <?= $form->field($model, 'name')->textInput(['class' => 'form-control code', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('name'),'id'=>'name']) ?>
                        </div>
                        <div class="col-md-4 mb-0">
                            <?= $form->field($model, 'status')->dropDownList(['ENABLED' => 'ENABLED', 'DISABLED' => 'DISABLED'], ['class' => 'form-control']) ?>
                        </div> 
                    </div>
                    <div class="form-row">
                        <div class="col-md-4 mb-0">
                            <?=
                                $form->field($model, 'price', ['template' => '{label}<div class="input-group"><div class="input-group-prepend" ><span class="input-group-text">$</span></div>{input}{error}</div>']
                                )->textInput([ 'class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('price'), 'onchange' => 'change_anyvalue()', 'onkeyup' => 'change_anyvalue()', 'style' => 'text-align:right;width:80%;'])
                            ?>
                        </div>
                        <div class="col-md-4 mb-0">
                            <?=
                                $form->field($model, 'gst', ['template' => '{label}<div class="input-group"><div class="input-group-prepend" ><span class="input-group-text">$</span></div>{input}{error}</div>']
                                )->textInput([ 'class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('gst'), 'onchange' => 'change_anyvalue()', 'onkeyup' => 'change_anyvalue()', 'style' => 'text-align:right;width:80%;'])
                            ?>
                        </div>
                        <div class="col-md-4 mb-0">
                            <?=
                                $form->field($model, 'total_price', ['template' => '{label}<div class="input-group"><div class="input-group-prepend" ><span class="input-group-text">$</span></div>{input}{error}</div>']
                                )->textInput([ 'class' => 'form-control', 'maxlength' => true, 'readonly' => true, 'placeholder' => $model->getAttributeLabel('total_price'), 'style' => 'text-align:right;width:80%;'])
                            ?>
                        </div>
                        
                        <div class="col-md-4 mb-0">
                            <?=
                                $form->field($model, 'coins', ['template' => '{label}<div class="input-group"><div class="input-group-prepend" ><span class="input-group-text">O</span></div>{input}{error}</div>']
                                )->textInput([ 'class' => 'form-control', 'maxlength' => true, 'readonly' => true, 'placeholder' => $model->getAttributeLabel('coins'), 'onchange' => 'change_anyvalue()', 'onkeyup' => 'change_anyvalue()', 'style' => 'text-align:right;width:80%;'])
                            ?>
                        </div>
                        <div class="col-md-2 mb-0">
                            <?=
                                $form->field($model, 'bonus_coins', ['template' => '{label}<div class="input-group">{input}{error}</div>']
                                )->textInput([ 'class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('bonus_coins'), 'onchange' => 'change_anyvalue();', 'onkeyup' => 'change_anyvalue()', 'style' => 'text-align:right;width:80%;'])
                            ?>
                        </div>
                        <div class="col-md-2 mb-0 ml-0">
                            <?=
                                $form->field($model, 'bonus_coins_amount', ['template' => '{label}<div class="input-group">{input}{error}</div>']
                                )->textInput([ 'class' => 'form-control', 'maxlength' => true, 'readonly' => true, 'placeholder' => $model->getAttributeLabel('bonus_coins_amount'), 'style' => 'text-align:right;width:80%;'])
                            ?>
                        </div>
                        <div class="col-md-4 mb-0">
                            <?=
                                $form->field($model, 'total_coins', ['template' => '{label}<div class="input-group"><div class="input-group-prepend" ><span class="input-group-text">O</span></div>{input}{error}</div>']
                                )->textInput([ 'class' => 'form-control', 'maxlength' => true, 'readonly' => true, 'placeholder' => $model->getAttributeLabel('total_coins'), 'style' => 'text-align:right;width:80%;'])
                            ?>
                        </div>
                                    
                    </div>
      
                    <div class="form-row">
                        <div class="col-md-6 mb-0">
                            <?= $form->field($model, 'description')->textarea(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('description')]) ?>
                        </div>
                        <div class="col-md-6 mb-0">
                            <?= $form->field($model, 'gst_description')->textarea(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('gst_description')]) ?>
                        </div>
                        
                    </div>

                    <?= Html::submitButton(Yii::t('app', ($model->isNewRecord)?'Create':'Update'), ['class' => 'btn btn-primary', 'style' => 'float: right;']) ?>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>

        </div>
    </section>
</div>
