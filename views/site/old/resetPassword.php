<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \mdm\admin\models\form\ResetPassword */

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>



<div class="login-wrapper">
    <div id="login" class="login loginpage offset-xl-4 col-xl-4 offset-lg-3 col-lg-6 offset-md-3 col-md-6 col-offset-0 col-12">
        <!-- <h2 class="login-logo-text"><a href="#" title="Login Page" tabindex="-1"><?= Yii::$app->name; ?></a></h2> -->
        <h2 class="login-logo-text"><a href="<?= Yii::$app->params['base_url']; ?>" title="Login Page" tabindex="-1"><img src="/theme_assets/images/musiconn_logo.png"></a></h2>
        <!--<h1><a href="#" title="Login Page" tabindex="-1" style="background-image:url('theme_assets/images/login-logo.png');"><?= Yii::$app->name; ?></a></h1>-->
        <p>Please choose your new password:</p>
        
        <?php
        $form = ActiveForm::begin([
                    'id' => 'reset-password-form',
                    'options' => ['name' => 'resetpasswordform'],
        ]);
        ?>

        <?= $form->field($model, 'password')->passwordInput(['autofocus' => false, 'class' => 'input']) ?>
        
	<p class="submit">
            <?= Html::submitButton('Save', ['class' => 'btn btn-orange btn-block', 'name' => 'save-button']) ?>
        </p>

        <?php ActiveForm::end(); ?>
        <p id="nav">            
            <?= Html::a('Go to SignIn', ['site/login'],['class'=>'float-left']) ?>
        </p>


    </div>
</div>