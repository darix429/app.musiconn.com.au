<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\ArrayHelper;
use app\models\OnlineTutorialPackageTutorwise;
use app\models\Instrument;
use app\models\StanadardPlanOptions;

$instrument = Instrument::find()->where(['uuid' => $instrument_uuid])->one();

$optionlist = StanadardPlanOptions::find()->orderBy([
  'option_value'=>SORT_ASC
])->all();
$optionlistData = ArrayHelper::map($optionlist, 'option_value', 'option_name');

$selected_option_detail = OnlineTutorialPackageTutorwise::find()->where(['tutor_uuid' => $model['uuid'], 'instrument_uuid' => $instrument_uuid])->one();


$this->title = $instrument->name . Yii::t('app', '\'s Tutorial Packages');
$this->params['breadcrumbs'][] = $this->title;


$this->registerJs(
        '            
function change_option(e){

                //alert(payment_term);
                $.ajax({
                    url: "' . Url::to(['/tutor/get-option-rate/']) . '",
                    method: "post",
                    data: {value: e.value,id : "' . $model['uuid'] . '",instrument_uuid:"' . $instrument_uuid . '"},
                    //dataType: "json",
                    success: function(result){
                       
                            $("#optionrate").html(result);
                       
                      
                    }
                 });
                 


}
    
    
', View::POS_END
);
?>

<style>
    .form-control-label
    {
        border : 0;
    }
    .semi-bold {
        font-weight: 900;
    }
    #tutor_table_parent_div{
        padding-right: 15px;
        padding-left: 15px;
    }
    .form-group
    {
        margin-bottom: 0;
    }

</style>

<div class="col-xl-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left"><?= $model['first_name'] . ' ' . $model['last_name']; ?>'s Online Tutorial Package List</h2>
            <div class="actions panel_actions float-right">
                <a href="<?= Yii::$app->getUrlManager()->createUrl(['/tutor/assign-plan/', 'id' => $model['uuid']]) ?>" class="btn btn-info btn-icon "><i class="fa fa-arrow-left text-white"></i> <span>Back</span></a>
            </div>
        </header>
        <div class="content-body"> 
            <div class=""></div>
            <div class="clearfix"></div>
            <div class="row" id="tutor_table_parent_div">
                <div class="col-lg-12 col-md-12 col-12">
                    <?php $form = ActiveForm::begin(); ?>
                    <?= Html::hiddenInput('tutor_uuid', $model['uuid']); ?>
                    <?= Html::hiddenInput('instrument_uuid', $instrument_uuid); ?>
                    <?= Html::hiddenInput('online_tutorial_package_type_uuid', $model['online_tutorial_package_type_uuid']); ?>
                    <div class="form-row">
                        <div class="col-md-6 mb-0"><div class="form-group field-user-timezone">
                                <label class="control-label" for="onlinetutorialpackage-name">Standard Plans</label>
                                <?= Html::dropDownList('standard_plan_rate_option', $selected_option_detail->standard_plan_rate_option, $optionlistData, ['class' => 'form-control common', 'onchange' => 'change_option(this)']);
                                ?><div class="help-block"></div>                                    </div>
                        </div>
                        <div class="col-md-3 mb-0">   
                            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary', 'style' => 'margin-top:13%;']) ?>
                        </div>
                    </div>
                    <div class="form-row"><bt></div>
                    <br>
                    <div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable" id="optionrate">
                        <table  class="table table-striped dt-responsive display" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Plan Name</th>
                                    <th style="text-align:right">Price</th>
                                    <th style="text-align:right">GST</th>
                                    <th style="text-align:right">Total Price</th>
                                </tr>
                               </thead>
                            <tbody>
                                <?php
                                if (!empty($option_array)) {
                                    foreach ($option_array as $key => $value) {
                                        $tutor_plan = OnlineTutorialPackageTutorwise::find()->where(['tutor_uuid' => $model['uuid'], 'premium_standard_plan_rate_uuid' => $value['uuid']])->asArray()->one();
                                        //echo "<pre>";
                                        //print_r($value);exit;
                                        ?>
                                        <tr>
                                            <td><?= $key + 1; ?></td>
                                            <td><?= $value['name']; ?></td>
                                            <td align="right"><?= $value['price']; ?></td>
                                            <td align="right"><?= $value['gst']; ?></td>
                                            <td align="right"><?= $value['total_price']; ?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>

                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </section>
</div>


