<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Url;
use app\libraries\General;
use app\models\StudentTuitionBooking;
use app\models\Instrument;
use app\models\StandardPlanOptionRate;

$this->title = Yii::t('app', 'BOOK A MUSIC LESSON');
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_icon'] = 'my-lesson-tag.png';

if (!empty($step_data['step_1']['instrument_uuid'])) {
    $instrumentDetail = Instrument::find()->where(['uuid' => $step_data['step_1']['instrument_uuid']])->one();
} else {
    return \yii\web\Controller::redirect(['index']);
}

$tutorList = StudentTuitionBooking::getInstrumentWiseTutorList($step_data['step_1']['instrument_uuid']);
//echo "<pre>";
//print_r($tutorList);
//echo "</pre>";
if (isset($step_data['step_2']['tutor_uuid']) && $step_data['step_2']['tutor_uuid'] != "") {
    $model->tutor_uuid = $step_data['step_2']['tutor_uuid'];
    $model->tutor_half_hour_price = $step_data['step_2']['tutor_half_hour_price'];
    $model->tutor_half_hour_price_in_coin = $step_data['step_2']['tutor_half_hour_price_in_coin'];
    $model->tutor_hour_price = $step_data['step_2']['tutor_hour_price'];
    $model->tutor_hour_price_in_coin = $step_data['step_2']['tutor_hour_price_in_coin'];
    $model->tutor_further_details = $step_data['step_2']['tutor_further_details'];
    $model->tutorcheckbox = $model->tutor_uuid;
}
if (is_array($model->tutor_further_details)) {
    $model->tutor_further_details = json_encode($model->tutor_further_details);
}
//print_r($model->tutor_further_details);exit;
$this->registerJs(
        '
    $("input[name=\"Book[tutorcheckbox]\"]").on("click", function (event) {
//    alert($("#"+this.value+"_tutor_further_details").val());
        $("#book-tutor_uuid").val(this.value);
        $("#book-tutor_half_hour_price").val($("#"+this.value+"_tutor_half_hour_price").val());
        $("#book-tutor_half_hour_price_in_coin").val($("#"+this.value+"_tutor_half_hour_price_in_coin").val());
        $("#book-tutor_hour_price").val($("#"+this.value+"_tutor_hour_price").val());
        $("#book-tutor_hour_price_in_coin").val($("#"+this.value+"_tutor_hour_price_in_coin").val());
        $("#book-tutor_further_details").val($("#"+this.value+"_tutor_further_details").val());
        $(".tutor_select_btn").addClass("custom-d-none");
        $(this).closest("li.tutor_tr").find(".tutor_select_btn").removeClass("custom-d-none");
        
        $(".tutor_deselect").removeClass("custom-d-none");
        $(this).closest("li.tutor_tr").find(".tutor_deselect").addClass("custom-d-none");
        $(".tutor_select_btn").removeClass("custom-d-visible");
        $(this).closest("li.tutor_tr").find(".tutor_select_btn").addClass("custom-d-visible");
    });
    $(".tutor_select_btn").on("click", function (event) {
        $("#frm_step_2").submit();
    });
    $( "#frm_step_2" ).submit(function( event ) {
        blockBody();
    });


        ', View::POS_END
);
?>

<?= Yii::$app->controller->renderPartial('step_prev_booking', ['model' => $model]); ?>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-12"><strong>BOOK A NEW MUSIC LESSON</strong></div><br>
            <div class="panel-group box-header with-border" id="accordion" role="tablist" aria-multiselectable="true">

                <!-- SELECT INSTRUMENT -->
                <div class="panel panel-default">

                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseInstrument" aria-expanded="false" aria-controls="collapseTwo">
                                SELECT INSTRUMENT 
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </a>
                            <span class="pull-right"><?= $instrumentDetail->name; ?> <a href="javascript:$('#gostep1').submit();"><div class="label label-warning hm-1 mr-2 siba">CHANGE</div></a></span>
                        </h4>
                    </div>
                </div>
                <!-- END SELECT INSTRUMENT -->

                <!-- SELECT TUTOR -->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTutor" aria-expanded="false" aria-controls="collapseThree">
                                SELECT TUTOR
                            </a>
                        </h4>
                    </div>
                    <div class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
                        <?php
//                        echo "<pre>";
//                        print_r($model->getErrors());
//                        echo "</pre>";

                        $form = ActiveForm::begin([
                                    'action' => ['index'],
                                    'id' => 'frm_step_2',
                                    'enableClientValidation' => true,
                                    'validateOnChange' => true,
                                    'options' => ['class' => 'form-horizontal']]);
                        ?>
                        <div class="panel-body">

                            <?php
                            $tutor_array = \yii\helpers\ArrayHelper::map($tutorList, 'uuid', function($e) {
                                        $TutorInstrumentPlanOptions = StudentTuitionBooking::getTutorInstrumentPlanOptions($e['online_tutorial_package_type_uuid'], $e['uuid'], $e['instrument_uuid']);
                                        $InstrumentWiseTutorPriceList = StudentTuitionBooking::getInstrumentWiseTutorPriceList($e['online_tutorial_package_type_uuid'], $TutorInstrumentPlanOptions['standard_plan_rate_option']);
                                        //echo "<pre>";print_r($InstrumentWiseTutorPriceList);echo "</pre>";
                                        $t_cat_class = ($e['tutor_cat_name'] == 'STANDARD') ? "musicon_play_plan_" . $TutorInstrumentPlanOptions['standard_plan_rate_option'] : "musicon_plus";
					$t_plan_title_text = ($e['tutor_cat_name'] == 'STANDARD') ? "MUSICONN PLAY - TIER " . $TutorInstrumentPlanOptions['standard_plan_rate_option'] . " TUTOR" : "MUSICONN+ TUTOR";
                                        $tutor_img_dataUri = \app\libraries\General::getTutorImage($e['uuid']);
                                        $tutor30minPriceInCoin = \app\libraries\General::priceToMusicoins($InstrumentWiseTutorPriceList['half']);
                                        $tutor60minPriceInCoin = \app\libraries\General::priceToMusicoins($InstrumentWiseTutorPriceList['full']);
                                        //$h = '<img class="sel_i tutor_booking_profile" src="' . $tutor_img_dataUri . '"><span id="tutor_detail_group"><span style="position: relative;display: inline-block;"><b>' . $e['first_name'] . ' ' . $e['last_name'] . '</b><br><span class="' . $t_cat_class . '">' . '$' . $InstrumentWiseTutorPriceList['half'] . ' / &#189;hr OR ' . '$' . $InstrumentWiseTutorPriceList['full'] . ' / hr' . "</span>"
                                        //      . "<input type=hidden name='Book[tutor_half_hour_price]' value='" . $InstrumentWiseTutorPriceList['half'] . "'>"
                                        //       . "<input type=hidden name='Book[tutor_hour_price]' value='" . $InstrumentWiseTutorPriceList['full'] . "'>";
                                        $h = '<img class="sel_i tutor_booking_profile" src="' . $tutor_img_dataUri . '"><span id="tutor_detail_group"><span style="position: relative;display: inline-block;text-align:left;"><b class="title_tutor_name">' . $e['first_name'] . ' ' . $e['last_name'] . '</b><br><span class="' . $t_cat_class . '" title="'.$t_plan_title_text.'">' . $tutor30minPriceInCoin . ' coins / &#189;hr OR ' . $tutor60minPriceInCoin . ' coins / hr' . "</span>"
                                                . "<input type=hidden name='tutor_half_hour_price' id='" . $e['uuid'] . "_tutor_half_hour_price' value='" . $InstrumentWiseTutorPriceList['half'] . "'><input type=hidden name='tutor_half_hour_price_in_coin' id='" . $e['uuid'] . "_tutor_half_hour_price_in_coin' value='" . $tutor30minPriceInCoin . "'>"
                                                . "<input type=hidden name='tutor_hour_price' id='" . $e['uuid'] . "_tutor_hour_price' value='" . $InstrumentWiseTutorPriceList['full'] . "'><input type=hidden name='tutor_hour_price_in_coin' id='" . $e['uuid'] . "_tutor_hour_price_in_coin' value='" . $tutor60minPriceInCoin . "'>";
                                        $h .= "<input type=hidden name='tutor_further_details' id='" . $e['uuid'] . "_tutor_further_details' value='" . json_encode($InstrumentWiseTutorPriceList) . "'>";
                                        return $h;
                                    });

                            $model->tutorcheckbox = $model->tutor_uuid;
                            $model_tutor_uuid = $model->tutorcheckbox;

                            echo $form->field($model, 'tutorcheckbox', ['template' => '{input}', 'options' => ['tag' => false]])
                                    ->radioList(
                                            $tutor_array, [
                                        'item' => function($index, $label, $name, $checked, $value) use($model_tutor_uuid) {

                                            $displayClass = ($model_tutor_uuid == $value) ? "" : "custom-d-none";
                                            $checked_val = ($model_tutor_uuid == $value) ? 'checked="checked"' : "";
                                            $btn = '<span style="cursor: pointer;" class="label label-warning custom-hm-1 tutor_select_btn ' . $displayClass . '">NEXT</span>';
                                            $btn1 = '';
                                            if ($displayClass == 'custom-d-none') {
                                                $btn1 = '<span class="tutor_deselect">&nbsp;<br></span>';
                                            }
                                            if ($model_tutor_uuid == $value) {
                                                $btn1 = '<span class="tutor_deselect custom-d-none">&nbsp;<br></span>';
                                            }

                                            $html = '<div class="col-md-4 ">';
                                            $html .= '<ul class="products-list product-list-in-box">';
                                            $html .= '<li class="item item_tb booking_tutor_listing tutor_tr">';
                                            $html .= '<label class="contai_r booking">';
                                            $html .= '<input type="radio" value="' . $value . '" name="' . $name . '" ' . $checked_val . '>';
                                            $html .= '<span class="checkmark"></span>';
                                            $html .= '</label>';
                                            //echo "<pre>";print_r($label);echo "</pre>";
                                            //$html .= $label[0].$label[1][0]['total_price']."<br/>". $btn.'</label>';
                                            $html .= $label;
                                            $html .= "<br/>" . $btn1 . $btn . '</span>';
                                            $html .= '</li>';
                                            $html .= '</ul>';
                                            $html .= '</div>';
                                            return $html;
                                        }
                                            ]
                                    )
                                    ->label(false);

                            //------------ Empty Tutor list----------
                            if (empty($tutor_array)) {
                                $html = '<div class="col-md-4 ">';
                                $html .= '<ul class="products-list product-list-in-box">';
                                $html .= '<li class="">';
                                $html .= 'No tutor found.';
                                $html .= '</li>';
                                $html .= '</ul>';
                                $html .= '</div>';
                                echo $html;
                            }
                            ?>

                            <div class="col-md-12" style="">
                                <input type="hidden" name="step" value="2">
                                <?= $form->field($model, 'tutor_uuid')->hiddenInput(['class' => 'form-control', 'maxlength' => true,])->label(false) ?>
                                <input value="<?= $model->tutor_half_hour_price ?>" type="hidden" id="book-tutor_half_hour_price" class="form-control" name="Book[tutor_half_hour_price]">
                                <input value="<?= $model->tutor_half_hour_price_in_coin ?>" type="hidden" id="book-tutor_half_hour_price_in_coin" class="form-control" name="Book[tutor_half_hour_price_in_coin]">
                                <input value="<?= $model->tutor_hour_price ?>" type="hidden" id="book-tutor_hour_price" class="form-control" name="Book[tutor_hour_price]">
                                <input value="<?= $model->tutor_hour_price_in_coin ?>" type="hidden" id="book-tutor_hour_price_in_coin" class="form-control" name="Book[tutor_hour_price_in_coin]">
                                <input value='<?= $model->tutor_further_details ?>' type="hidden" id="book-tutor_further_details" class="form-control" name="Book[tutor_further_details]">
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
                <!-- END SELECT TUTOR -->

                <!-- SELECT LENGTH OF LESSON -->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingfour">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseLength" aria-expanded="false" aria-controls="collapsefour">
                                SELECT LENGTH OF LESSON
                            </a>
                        </h4>
                    </div>
                </div>
                <!--END SELECT LENGTH OF LESSON -->

                <!-- SELECT DATE AND TIME -->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingfive">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseDate" aria-expanded="false" aria-controls="collapsefive">
                                SELECT DATE AND TIME
                            </a>
                        </h4>
                    </div>
                </div>
                <!--END SELECT DATE AND TIME -->

                <!-- CONFIRM BOOKING AND PAYMENT -->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingsix">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsePayment" aria-expanded="false" aria-controls="collapsesix">
                                CONFIRM BOOKING AND PAYMENT
                            </a>
                        </h4>
                    </div>
                </div>
                <!--END CONFIRM BOOKING AND PAYMENT -->

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <a class="btn btn-primary btn-sm black_button_in" href="<?= Yii::$app->getUrlManager()->createUrl('/student/home'); ?>">RETURN TO HOME</a>
        </div>
    </div>
</section>

<div class="other_forms">
    <?php echo Html::beginForm(['index'], 'post', ['id' => 'gostep1', 'class' => 'form-inline d-block']) ?>
    <input type="hidden" name="step" value="1">
    <input type="hidden" name="prev" value="1">
    <?php echo Html::endForm() ?>

</div>
