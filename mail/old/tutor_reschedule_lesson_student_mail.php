<?php

use yii\helpers\Html;

?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['student_name']; ?>,</p>
            
            <p>Your lesson is rescheduled by Tutor.</p>
            
            <p>The details of your lesson change request #<?= $content['reschedule_number']; ?> are as follows:</p>
            
            <p><strong>From:</strong></p>
            
            <p><strong>Reschedule Reference :</strong> <?= "#".$content['reschedule_number']; ?></p>
            <p><strong>Tutor Name :</strong> <?= $content['tutor_name']; ?></p>
            <p><strong>Previous Tutorial Date :</strong> <?= $content['from_date']; ?></p>
            <p><strong>Previous Tutorial Time :</strong> <?= $content['from_time']; ?></p>
            
            <p><strong>To:</strong></p>
            
            <p><strong>Tutor Name :</strong> <?= $content['tutor_name']; ?></p>
            <p><strong>Change Tutorial Date :</strong> <?= $content['to_date']; ?></p>
            <p><strong>Change Tutorial Time :</strong> <?= $content['to_time']; ?></p>
            
            <p>Please do not reply to this unattended mailbox.  Any queries can be directed to admin@musiconn.com.au.</p>
            
            <p>Regards,<br>
            Admin Team – Musiconn Pty Ltd</p>
        </td>
    </tr>
</table>

