<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Test');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-xl-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Test List</h2>
            <div class="actions panel_actions float-right">
                <a href="<?= Yii::$app->getUrlManager()->createUrl(['/test/create/'])?>" class="btn btn-primary "><i class="fa fa-plus text-white"></i></a>
                <i class="fa fa-search icon-primary icon-xs icon-orange mail_head_icon custom-toggle" data-togglediv="datatable-search-div" title="Advance Search"></i>
            </div>
        </header>
        <div class="content-body">   

            <div class="clearfix"></div>
            <div class="row" id="">
                <div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">
                    <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Name</th>
                                <th>File</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (!empty($model)) {
                                foreach ($model as $key => $value) {
                                    ?>
                                    <tr>
                                        <td><?= $key + 1; ?></td>
                                        <td><?= $value['name']; ?></td>
                                        <td><?= $value['file']; ?></td>

                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </section>
</div>






















