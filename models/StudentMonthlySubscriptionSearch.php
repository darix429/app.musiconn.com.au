<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\StudentMonthlySubscription;

/**
 * StudentMonthlySubscriptionSearch represents the model behind the search form of `app\models\StudentMonthlySubscription`.
 */
class StudentMonthlySubscriptionSearch extends StudentMonthlySubscription {

    public $student_name, $start_daterange;

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['uuid', 'student_uuid', 'name', 'description', 'status', 'start_datetime', 'end_datetime', 'created_at', 'updated_at', 'student_name', 'start_daterange'], 'safe'],
            [['price', 'tax', 'total_price'], 'number'],
            [['with_tutorial'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $modeDatatable = false, $statusnot = '') {
        $query = StudentMonthlySubscription::find();
        $query->joinWith(['orderUu']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($statusnot)) {
            if (is_array($statusnot)) {

                $query->andWhere(['not in', 'status', $statusnot]);
            } else {

                $query->andWhere(['<>', 'status', $statusnot]);
            }
        }
        $query->andFilterWhere(['status' => $this->status]);

        // grid filtering conditions
        $query->andFilterWhere([
            'price' => $this->price,
            'tax' => $this->tax,
            'total_price' => $this->total_price,
            'with_tutorial' => $this->with_tutorial,
            "to_char(start_datetime, 'DD-MM-YYYY')" => $this->start_datetime,
            "to_char(end_datetime, 'DD-MM-YYYY')" => $this->end_datetime,
            //'start_datetime' => $this->start_datetime,
            //'end_datetime' => $this->end_datetime,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        if (Yii::$app->user->identity->role == 'STUDENT') {
            $query->andFilterWhere(['student_monthly_subscription.student_uuid' => Yii::$app->user->identity->student_uuid]);
            //$query->andFilterWhere(['status' => 'ENABLED']);
        }

        $query->andFilterWhere(['ilike', 'uuid', $this->uuid])
                //->andFilterWhere(['ilike', 'student_monthly_subscription.student_uuid', $this->student_uuid])
                ->andFilterWhere(['ilike', 'name', $this->name])
                ->andFilterWhere(['ilike', 'description', $this->description]);
        //->andFilterWhere(['ilike', 'status', $this->status]);
//echo $query->createCommand()->getRawSql(); exit;
        if ($modeDatatable) {
            $result = $query->asArray()->orderBy('created_at')->all();
            return $result;
        }
        return $dataProvider;
    }

    public function searchReport($params, $modeDatatable = false, $statusnot = '') {
        $query = StudentMonthlySubscription::find();
        $query->joinWith(['orderUu', 'studentUu']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($statusnot)) {
            if (is_array($statusnot)) {

                $query->andWhere(['not in', 'status', $statusnot]);
            } else {

                $query->andWhere(['<>', 'status', $statusnot]);
            }
        }
        $query->andFilterWhere(['status' => $this->status]);

        // grid filtering conditions
        $query->andFilterWhere([
            'price' => $this->price,
            'tax' => $this->tax,
            'total_price' => $this->total_price,
            'with_tutorial' => $this->with_tutorial,
            "to_char(start_datetime, 'DD-MM-YYYY')" => $this->start_datetime,
            "to_char(end_datetime, 'DD-MM-YYYY')" => $this->end_datetime,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        if (Yii::$app->user->identity->role == 'STUDENT') {
            $query->andFilterWhere(['student_monthly_subscription.student_uuid' => Yii::$app->user->identity->student_uuid]);
            $query->andFilterWhere(['status' => 'ENABLED']);
        }
        //echo $this->start_daterange;exit;
        if (!is_null($this->start_daterange) && strpos($this->start_daterange, ' - ') !== false) {
            list($start_date, $end_date) = explode(' - ', $this->start_daterange);
            $query->andFilterWhere(['>=', 'student_monthly_subscription.created_at', date('Y-m-d 00:00:00', strtotime($start_date))]);
            $query->andFilterWhere(['<=', 'student_monthly_subscription.created_at', date('Y-m-d 23:59:59', strtotime($end_date))]);
        }
        $query->andFilterWhere(['ilike', "CONCAT(student.first_name,' ',student.last_name)", $this->student_name]);

        $query->andFilterWhere(['ilike', 'uuid', $this->uuid])
                ->andFilterWhere(['ilike', 'name', $this->name])
                ->andFilterWhere(['ilike', 'description', $this->description]);
//echo "<pre>"; print_r($query); exit;
        if ($modeDatatable) {
            $result = $query->asArray()->orderBy('created_at')->all();
            return $result;
        }
        return $dataProvider;
    }

}
