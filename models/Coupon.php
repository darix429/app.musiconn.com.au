<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%coupon}}".
 *
 * @property string $uuid
 * @property string $code
 * @property string $description
 * @property string $type
 * @property string $amount
 * @property string $code_type
 * @property string $start_date
 * @property string $end_date
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 */
class Coupon extends \yii\db\ActiveRecord
{

    public $percentage;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%coupon}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //html tag filter
            [['code',], 'match', 'pattern' => \app\libraries\General::validTextPattern(), 'message' => 'Invalid charecters used.'], 
            [['description',], 'match', 'pattern' => \app\libraries\General::validTextPattern(['@','$','(',')','\[','\]','\/',]), 'message' => 'Invalid charecters used.'], 
            
            [[ 'code', 'code_type', 'start_date', 'end_date'], 'required'],
            [['uuid', 'code', 'description', 'type', 'code_type', 'status'], 'string'],
            [['amount','percentage'], 'number'],
            [['start_date', 'end_date', 'created_at', 'updated_at'], 'safe'],
            [['code'], 'unique'],
            [['uuid'], 'unique'],

            [['amount'], 'required','when' => function($model) {
                    return ($model->type == "Flat");
                }, 'whenClient' => "function (attribute, value) {
                    return ($('#coupon-type').val() == 'Flat')?true:false;
                }"],

                [['percentage'], 'required','when' => function($model) {
                    return ($model->type == "Percentage");
                }, 'whenClient' => "function (attribute, value) {
                    return ($('#coupon-type').val() == 'Percentage')?true:false;
                }"],


            ['start_date',function ($attribute, $params) {
                $start_date = date("Y-m-d H:i:00",strtotime($this->start_date));
                $end_date = date("Y-m-d H:i:00",strtotime($this->end_date));
                if ($start_date > $end_date) {
                    $this->addError($attribute, 'Start date must be less than End date.');
                }
            }],
            ['end_date',function ($attribute, $params) {
                $start_date = date("Y-m-d H:i:00",strtotime($this->start_date));
                $end_date = date("Y-m-d H:i:00",strtotime($this->end_date));
                if ($end_date < $start_date) {
                    $this->addError($attribute, 'End date must be greter than Start date.');
                }
            }],

           
	    //[['amount'],'match','pattern' => '/(\d{0,10})[^.]*((?:\.\d{0,2})?)/'/*,'message'=>'Only allow decimal 5 digit.'*/],
//            [['start_date',], 'date'],
            ['start_date','compare','compareValue'=> date('d-m-Y'),'operator'=>'>', 'on' => 'coupon_create','message' => 'Start Date must be greater than today date.'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'code' => Yii::t('app', 'Code'),
            'description' => Yii::t('app', 'Description'),
            'type' => Yii::t('app', 'Type'),
            'amount' => Yii::t('app', 'Amount'),
            'code_type' => Yii::t('app', 'Code Type'),
            'start_date' => Yii::t('app', 'Start Date'),
            'end_date' => Yii::t('app', 'End Date'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            //'flat' => Yii::t('app', 'Amount'),
            'percentage' => Yii::t('app', 'Percentage'),
        ];
    }
}
