<?php

namespace app\controllers;

use app\models\PreRecordedVideo;
use app\models\PreRecordedVideoSearch;
use app\models\SessionRecordedVideo;
use app\models\SessionRecordedVideoSearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\FileHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use app\models\User;
use app\models\InstrumentCategory;
use app\models\Instrument;
use app\models\BookedSession;
use app\models\Admin;
use app\models\BookedSessionSearch;
use app\models\Student;
use yii\helpers\Url;
use app\libraries\General;
use app\models\StudentViewRecordedLesson;
use app\models\StudentViewPreRecordedLesson;

/**
 * PreRecordedVideoController implements the CRUD actions for PreRecordedVideo model.
 */
class VideoController extends Controller {
    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['upload'],
                        'allow' => Yii::$app->user->can('/video/upload'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['update'],
                        'allow' => Yii::$app->user->can('/video/update'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['review'],
                        'allow' => Yii::$app->user->can('/video/review'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => Yii::$app->user->can('/video/delete'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['uploaded'],
                        'allow' => Yii::$app->user->can('/video/uploaded'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['published'],
                        'allow' => Yii::$app->user->can('/video/published'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['video-more-details'],
                        'allow' => Yii::$app->user->can('/video/video-more-details'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['published-enabled'],
                        'allow' => Yii::$app->user->can('/video/published-enabled'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['is-free-enabled'],
                        'allow' => Yii::$app->user->can('/video/is-free-enabled'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['is-free-disabled'],
                        'allow' => Yii::$app->user->can('/video/is-free-disabled'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['student-video-list'],
                        'allow' => Yii::$app->user->can('/video/student-video-list'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['recorded'],
                        'allow' => Yii::$app->user->can('/video/recorded'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['tution-view'],
                        'allow' => Yii::$app->user->can('/video/tution-view'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['session-publish'],
                        'allow' => Yii::$app->user->can('/video/session-publish'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['tution-more-details'],
                        'allow' => Yii::$app->user->can('/video/tution-more-details'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['session-view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['categoryupdate'],
                        'allow' => Yii::$app->user->can('/video/categoryupdate'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['report'],
                        'allow' => Yii::$app->user->can('/video/report'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['play','test','download','playtution','categoryupdate','student-video-list-old'],
                        'allow' => true,
//                        'allow' => Yii::$app->user->can('/video/play'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['video-category-update'],
                        'allow' => Yii::$app->user->can('/video/video-category-update'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['published-video'],
                        'allow' => Yii::$app->user->can('/video/published-video'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['delete-video'],
                        'allow' => Yii::$app->user->can('/video/delete-video'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['student_view_session_count'],
                        'allow' => Yii::$app->user->can('/video/student_view_session_count'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['student_view_pre_recorded_count'],
                        'allow' => Yii::$app->user->can('/video/student_view_pre_recorded_count'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['download-recording'],
                        'allow' => Yii::$app->user->can('/video/download-recording'),
                        'roles' => ['@'],
                    ],
                    
                    
                ],
                
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
        if ($action->id == 'video-category-update') {
            $this->enableCsrfValidation = false;
        }
        if ($action->id == 'delete-video') {
            $this->enableCsrfValidation = false;
        }
        if ($action->id == 'student-video-list') {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function actionUpload() {

        Yii::$app->session['status'] = 'uploaded';
        $model = new PreRecordedVideo();
        $model->scenario = 'create';
        if (Yii::$app->user->identity->role == 'TUTOR') {
            $tutor_uuid = Yii::$app->user->identity->tutor_uuid;
            $user = User::find()->where(['tutor_uuid' => $tutor_uuid])->one();
            $model->user_uuid = $user->id;
            //rupal
            $model->tutor_uuid = Yii::$app->user->identity->tutor_uuid;
        } else {
            $admin_uuid = Yii::$app->user->identity->admin_uuid;
            $user = User::find()->where(['admin_uuid' => $admin_uuid])->one();
            $model->user_uuid = $user->id;
            //rupal
            $model->admin_uuid = Yii::$app->user->identity->admin_uuid;
        }

        $model->attributes = Yii::$app->request->post('PreRecordedVideo');

        $model->created_at = $model->updated_at = date('Y-m-d H:i:s', time());
        $model->publish_date = $model->expire_date = '';

        if (Yii::$app->request->isPost) {

            $file = UploadedFile::getInstance($model, 'file');
            $model->file = $file;

            $model->title = (empty($model->title)) ? substr($file['name'], 0, strpos($file['name'], '.')) : $model->title;

            if ($model->status == 'UPLOADED') {
                $model->uploaded_date = $model->updated_at;
            }
            if ($model->instrument_uuid == '') {
                $model->instrument_uuid = NULL;
            }
            if ($model->instrument_category_uuid == '') {
                $model->instrument_category_uuid = NULL;
            }


            if ($model->validate()) {

                if ($model->status != 'REJECTED') {
                    $model->rejected_reason = NULL;
                }

                $model->save(false);
                $path = Yii::$app->params['media']['pre_recorded_video']['path'];
                if (!is_dir($path)) {
                    FileHelper::createDirectory($path);
                }

                if ($file) {
                    $fname = $model->uuid . '.' . $file->extension;
                    $fileFullPath = $path . $fname;
                    $model->file = $fname;
                    $model->save(false);
                }


                $content['name'] = $user->first_name . ' ' . $user->last_name;
                $content['title'] = $model->title;
                $content['link'] = Url::to(['/video/update/','id' => $model->uuid], true);
                //$content['link'] = Yii::$app->params['media']['pre_recorded_video']['url'] . $model->file;
                //$content['email'] = $user->email;

                Yii::$app->mailer->compose('tutor_video_upload', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                        ->setFrom(Yii::$app->params['supportEmail'])
                        ->setTo($user->email)//$admin->email
                        ->setSubject('Tutor Upload Video - ' . Yii::$app->name)
                        ->send();

                //mail to subadmin
                $subadmin = Admin::find()->joinWith(['user'])->where(['user.role'=> 'SUBADMIN'])->andWhere(['admin.status' => 'ENABLED'])->asArray()->one();

                $content2['subadmin_name'] = $subadmin['first_name'].' '.$subadmin['last_name'];
                $content2['video_title'] = $model->title;
                $content2['tutor_name'] = $user->first_name . ' ' . $user->last_name;

                Yii::$app->mailer->compose('subadmin_video_upload', ['content' => $content2], ['htmlLayout' => 'layouts/html'])
                    ->setFrom(Yii::$app->params['supportEmail'])
                    ->setTo($subadmin['email'])//$admin->email
                    ->setSubject('Musiconn Notification: A new video has been uploaded to the Video Library')
                    ->send();        

                if ($file) {
                    //save file in folder
                    if ($file->saveAs($fileFullPath)) {

                        //thumbnail generate
                        PreRecordedVideo::videoThumbnailGenerate($fileFullPath, $model->file);

                        Yii::$app->getSession()->setFlash('success', 'Video updated successfully.');
                        Yii::$app->response->format = 'json';
                        return [
                            'code' => 200,
                            'message' => 'Video updated successfully.',
                            'files' => [
                                [
                                    'name' => $fname,
                                    'size' => $file->size,
                                    'url' => $fileFullPath,
                                    'thumbnailUrl' => $fileFullPath,
                                ],
                            ],
                        ];
                    }
                } else {
                    Yii::$app->getSession()->setFlash('success', 'Video created successfully.');
                    Yii::$app->response->format = 'json';
                    return [
                        'code' => 200,
                        'message' => 'Video created successfully.',
                    ];
                }
            }
        }
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_form_inner', [
                        'model' => $model,
            ]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    public function actionUploaded() {

        $searchModel = new PreRecordedVideoSearch();
        $searchModel->status = 'UPLOADED';

        $videoList = $searchModel->search(Yii::$app->request->queryParams, true);

        if (Yii::$app->user->identity->role == 'TUTOR') {

            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('_tutor_uploaded_list', [
                            'recodedVideoList' => $videoList,
                ]);
            } else {
                return $this->render('tutor_uploaded_video', [
                            'recodedVideoList' => $videoList,
                            'searchModel' => $searchModel,
                ]);
            }
        } else {
            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('_uploaded_list', [
                            'videoList' => $videoList,
                            'mode' => 'UPLOADED',
                ]);
            } else {
                return $this->render('uploaded_video', [
                            'videoList' => $videoList,
                            'searchModel' => $searchModel,
                ]);
            }
        }
    }

    public function actionPublished() {
        Yii::$app->session['status'] = 'published';
        $searchModel = new PreRecordedVideoSearch();
        $searchModel->status = 'PUBLISHED';
        $videoList = $searchModel->search(Yii::$app->request->queryParams, true);

        if (Yii::$app->user->identity->role == 'TUTOR') {
            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('_tutor_published_list', [
                            'recodedVideoList' => $videoList,
                ]);
            } else {
                return $this->render('tutor_published_video', [
                            'recodedVideoList' => $videoList,
                            'searchModel' => $searchModel,
                ]);
            }
        } else {
            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('_published_list', [
                            'videoList' => $videoList,
                            'mode' => 'UPLOADED',
                ]);
            } else {
                return $this->render('published_video', [
                            'videoList' => $videoList,
                            'searchModel' => $searchModel,
                ]);
            }
        }
    }

    public function actionUpdate($id) {
        $access = PreRecordedVideo::loginUserAuthorizeUpdateVideo($id);
        if (!$access) {
            throw new \yii\web\HttpException(505, 'You are not authorised to access this page');
        }
        try {
            $model = $this->findModel($id);
            //$model->scenario = 'UPDATE_VIDEO';
            $old_status = $model->status;
            $oldfilename = $model->file;
            $old_category = $model->instrument_category_uuid;
            $oldfilenamePath = Yii::$app->params['media']['pre_recorded_video']['path'] . $oldfilename;
            $model->publish_date = $model->expire_date = '';

            if (Yii::$app->request->isPost) {

                $model->attributes = Yii::$app->request->post('PreRecordedVideo');
                $file = UploadedFile::getInstance($model, 'file');

                $model->update_file = $file;
                $model->updated_at = date('Y-m-d H:i:s', time());

                if ($model->status == 'UPLOADED') {
                    $model->uploaded_date = $model->updated_at;
                }
                if ($model->instrument_uuid == '') {
                    $model->instrument_uuid = NULL;
                }
                if ($model->instrument_category_uuid == '') {
                    $model->instrument_category_uuid = NULL;
                }

                if ($model->validate()) {

                    $path = Yii::$app->params['media']['pre_recorded_video']['path'];
                    if (!is_dir($path)) {
                        FileHelper::createDirectory($path);
                    }

                    if ($file) {
                        $fname = $model->uuid . '.' . $file->extension;
                        $fileFullPath = $path . $fname;
                        $model->file = $fname;
                    } else {
                        $model->file = $oldfilename;
                    }

                    if ($model->status != 'REJECTED') {
                        $model->rejected_reason = NULL;
                    }

                    $model->save(false);
                    //echo "<pre>"; print_r($file); exit;
                    if ($file) {
                        //delete old file
                        if (is_file($oldfilenamePath)) {
                            unlink($oldfilenamePath);
                        }

                        if ($file->saveAs($fileFullPath)) {

                            Yii::$app->getSession()->setFlash('success', 'Video updated successfully.');
                            Yii::$app->response->format = 'json';
                            return [
                                'code' => 200,
                                'message' => 'Video updated successfully.',
                                'files' => [
                                    [
                                        'name' => $fname,
                                        'size' => $file->size,
                                        'url' => $fileFullPath,
                                        'thumbnailUrl' => $fileFullPath,
                                    ],
                                ],
                            ];
                        }
                    } else {
                        Yii::$app->getSession()->setFlash('success', 'Video updated successfully.');
                        Yii::$app->response->format = 'json';
                        return [
                            'code' => 200,
                            'message' => 'Video updated successfully.',

                        ];
                    }
                }
            }

            if (Yii::$app->request->isAjax) {

                return $this->renderAjax('_tutor_update_form', [
                            'model' => $model,
                ]);
            } else {
                return $this->render('tutor_update', [
                            'model' => $model,
                                //'back_url' => ['all-video'],
                ]);
            }
        } catch (\yii\base\Exception $exception) {
            throw new \yii\web\HttpException(505, $exception->getMessage());
        }
    }

    public function actionDelete() {
        $id = Yii::$app->getRequest()->post('id');
        if ((($model = PreRecordedVideo::findOne($id)) !== null)) {
            $model->delete();
            //remove video in folder
            $file = Yii::$app->params['media']['pre_recorded_video']['path'] . $model->file;
            if (file_exists($file)) {
                unlink($file);
            }
            $return = ['code' => 200, 'message' => 'Video deleted successfully.'];
        } else {

            $return = ['code' => 404, 'message' => 'The requested page does not exist.'];
        }
        Yii::$app->response->format = 'json';
        return $return;
    }

    protected function findModel($id) {
        if (($model = PreRecordedVideo::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionReview($id) {
        $access = PreRecordedVideo::loginUserAuthorizeReviewVideo($id);
        if (!$access) {
            throw new \yii\web\HttpException(505, 'You are not authorised to access this page');
        }
        try {
            $model = $this->findModel($id);
            $model->scenario = 'UPDATE_VIDEO';
            $old_status = $model->status;
            $oldfilename = $model->file;
            $old_category = $model->instrument_category_uuid;
            $oldfilenamePath = Yii::$app->params['media']['pre_recorded_video']['path'] . $oldfilename;
            $model->publish_date = (empty($model->publish_date)) ? date('Y-m-d') : date('Y-m-d', strtotime($model->publish_date));
            if ($model->status == "PUBLISHED") {
                Yii::$app->session['status'] = 'published';
            } else {
                Yii::$app->session['status'] = 'uploaded';
            }

            if (Yii::$app->request->isPost) {

                $model->attributes = Yii::$app->request->post('PreRecordedVideo');
                //echo "<pre>"; print_r($model->attributes); exit;
                $file = UploadedFile::getInstance($model, 'update_file');
                $model->is_free = $_POST['PreRecordedVideo']['is_free'];
                $model->update_file = $file;
                $model->updated_at = date('Y-m-d H:i:s', time());
                if ($model->status == 'PUBLISH') {
                    if ($old_status != 'PUBLISH') {
                        $model->publish_date = date('Y-m-d', strtotime($model->updated_at));
                    }
                } else if ($model->status == 'UPLOADED') {
                    $model->uploaded_date = $model->updated_at;
                }

                if ($model->instrument_uuid == '') {
                    $model->instrument_uuid = NULL;
                }
                if ($model->instrument_category_uuid == '') {
                    $model->instrument_category_uuid = NULL;
                }

                if ($model->validate()) {

                    $path = Yii::$app->params['media']['pre_recorded_video']['path'];
                    if (!is_dir($path)) {
                        FileHelper::createDirectory($path);
                    }

                    if ($file) {
                        $fname = $model->uuid . '.' . $file->extension;
                        $fileFullPath = $path . $fname;
                        $model->file = $fname;
                    }

                    if ($model->status != 'REJECTED') {
                        $model->rejected_reason = NULL;
                    }

                    if ($model->status != 'PUBLISH') {
                        $model->publish_date = NULL;
                    }

                    $model->save(false);
                    if ($model->status == "PUBLISHED") {
                        Yii::$app->session['status'] = 'published';
                    } else {
                        Yii::$app->session['status'] = 'uploaded';
                    }

                    //rupal
                    $instrument = Instrument::find()->where(['uuid' => $model->attributes['instrument_uuid']])->one();
                    $instrumentCategory = InstrumentCategory::find()->where(['uuid' => $model->attributes['instrument_category_uuid']])->one();
                    //echo "<pre>"; print_r($fname); print_r($fileFullPath); exit;


                    if ($file) {
                        //delete old file
                        $url = Yii::$app->params['media']['pre_recorded_video']['url'];
                        $fileFullUrl = $url . $fname;

                        if (is_file($oldfilenamePath)) {
                            unlink($oldfilenamePath);
                        }


                        if ($file->saveAs($fileFullPath)) {

			    //thumbnail generate
                            PreRecordedVideo::videoThumbnailGenerate($fileFullPath, $model->file);

                            Yii::$app->getSession()->setFlash('success', 'Video updated successfully.');
                            Yii::$app->response->format = 'json';
                            return [
                                'code' => 200,
                                'message' => 'Video updated successfully.',
                                //rupal
                                'files' => [
                                        'name' => $fname,
                                        'size' => $file->size,
                                        'url' => $fileFullUrl,
                                        'thumbnailUrl' => $fileFullPath,
                                    ],

                            ];
                        }
                    } else {
                        Yii::$app->getSession()->setFlash('success', 'Video updated successfully.');
                        Yii::$app->response->format = 'json';
                        return [
                            'code' => 200,
                            'message' => 'Video updated successfully.',
                        ];
                    }
                }
            }
            if (Yii::$app->user->identity->role == 'STUDENT') {

                $this->layout = "student_layout";
                return $this->render('student_video_view', [
                            'model' => $model,
                            'back_url' => ['all-video'],
                ]);
            } else {
                if (Yii::$app->request->isAjax) {

                    return $this->renderAjax('_admin_update_form', [
                                'model' => $model,
                    ]);
                } else {
                    return $this->render('admin_update', [
                                'model' => $model,
                                'back_url' => ['all-video'],
                    ]);
                }
            }
        } catch (\yii\base\Exception $exception) {
            throw new \yii\web\HttpException(505, $exception->getMessage());
        }
    }
    
    public function actionReviewOld($id) {
        $access = PreRecordedVideo::loginUserAuthorizeReviewVideo($id);
        if (!$access) {
            throw new \yii\web\HttpException(505, 'You are not authorised to access this page');
        }
        try {
            $model = $this->findModel($id);
            $model->scenario = 'UPDATE_VIDEO';
            $old_status = $model->status;
            $oldfilename = $model->file;
            $old_category = $model->instrument_category_uuid;
            $oldfilenamePath = Yii::$app->params['media']['pre_recorded_video']['path'] . $oldfilename;
            $model->publish_date = (empty($model->publish_date)) ? date('Y-m-d') : date('Y-m-d', strtotime($model->publish_date));
            if ($model->status == "PUBLISHED") {
                Yii::$app->session['status'] = 'published';
            } else {
                Yii::$app->session['status'] = 'uploaded';
            }

            if (Yii::$app->request->isPost) {

                $model->attributes = Yii::$app->request->post('PreRecordedVideo');
                //echo "<pre>"; print_r($model->attributes); exit;
                $file = UploadedFile::getInstance($model, 'update_file');
                $model->is_free = $_POST['PreRecordedVideo']['is_free'];
                $model->update_file = $file;
                $model->updated_at = date('Y-m-d H:i:s', time());
                if ($model->status == 'PUBLISH') {
                    if ($old_status != 'PUBLISH') {
                        $model->publish_date = date('Y-m-d', strtotime($model->updated_at));
                    }
                } else if ($model->status == 'UPLOADED') {
                    $model->uploaded_date = $model->updated_at;
                }

                if ($model->instrument_uuid == '') {
                    $model->instrument_uuid = NULL;
                }
                if ($model->instrument_category_uuid == '') {
                    $model->instrument_category_uuid = NULL;
                }

                if ($model->validate()) {

                    $path = Yii::$app->params['media']['pre_recorded_video']['path'];
                    if (!is_dir($path)) {
                        FileHelper::createDirectory($path);
                    }

                    if ($file) {
                        $fname = $model->uuid . '.' . $file->extension;
                        $fileFullPath = $path . $fname;
                        $model->file = $fname;
                    }

                    if ($model->status != 'REJECTED') {
                        $model->rejected_reason = NULL;
                    }

                    if ($model->status != 'PUBLISH') {
                        $model->publish_date = NULL;
                    }

                    $model->save(false);
                    if ($model->status == "PUBLISHED") {
                        Yii::$app->session['status'] = 'published';
                    } else {
                        Yii::$app->session['status'] = 'uploaded';
                    }

                    //rupal
                    $instrument = Instrument::find()->where(['uuid' => $model->attributes['instrument_uuid']])->one();
                    $instrumentCategory = InstrumentCategory::find()->where(['uuid' => $model->attributes['instrument_category_uuid']])->one();
                    //echo "<pre>"; print_r($fname); print_r($fileFullPath); exit;


                    if ($file) {
                        //delete old file
                        $url = Yii::$app->params['media']['pre_recorded_video']['url'];
                        $fileFullUrl = $url . $fname;

                        if (is_file($oldfilenamePath)) {
                            unlink($oldfilenamePath);
                        }


                        if ($file->saveAs($fileFullPath)) {

			    //thumbnail generate
                            PreRecordedVideo::videoThumbnailGenerate($fileFullPath, $model->file);

                            Yii::$app->getSession()->setFlash('success', 'Video updated successfully.');
                            Yii::$app->response->format = 'json';
                            return [
                                'code' => 200,
                                'message' => 'Video updated successfully.',
                                //rupal
                                'files' => [
                                        'name' => $fname,
                                        'size' => $file->size,
                                        'url' => $fileFullUrl,
                                        'thumbnailUrl' => $fileFullPath,
                                    ],

                            ];
                        }
                    } else {
                        Yii::$app->getSession()->setFlash('success', 'Video updated successfully.');
                        Yii::$app->response->format = 'json';
                        return [
                            'code' => 200,
                            'message' => 'Video updated successfully.',
                        ];
                    }
                }
            }
            if (Yii::$app->user->identity->role == 'STUDENT') {

                $this->layout = "student_layout";
                return $this->render('student_video_view', [
                            'model' => $model,
                            'back_url' => ['all-video'],
                ]);
            } else {
                if (Yii::$app->request->isAjax) {

                    return $this->renderAjax('_admin_update_form', [
                                'model' => $model,
                    ]);
                } else {
                    return $this->render('admin_update', [
                                'model' => $model,
                                'back_url' => ['all-video'],
                    ]);
                }
            }
        } catch (\yii\base\Exception $exception) {
            throw new \yii\web\HttpException(505, $exception->getMessage());
        }
    }

    public function actionVideoMoreDetails($id) {

        if (($query = PreRecordedVideo::find()->where(['uuid' => $id])->one()) !== null) {

            $instrument = \app\models\Instrument::find()->where(['uuid' => $query->instrument_uuid])->one();
            $category = \app\models\InstrumentCategory::find()->where(['uuid' => $query->instrument_category_uuid])->one();

            if ($query->tutor_uuid != NULL) {
                $user = \app\models\Tutor::find()->where(['uuid' => $query->tutor_uuid])->one();
            } else {
                $user = \app\models\Admin::find()->where(['admin_uuid' => $query->admin_uuid])->one();
            }

            $return['title'] = $query->title;
            if (!empty($instrument->name)) {
                $return['instrument'] = $instrument->name;
            }
            if (!empty($category->name)) {
                $return['category'] = $category->name;
            }
            if (!empty($query->description)) {
                $return['description'] = $query->description;
            }
            if ($query->status == 'PUBLISHED') {
		     $return['publish_date'] = \app\libraries\General::displayDateTime($query->publish_date);
		}
            $return['user'] = $user->first_name . ' ' . $user->last_name;
            $return['updated_at'] = \app\libraries\General::displayDateTime($query->updated_at);
        }

        Yii::$app->response->format = 'json';
        return $return;
    }

    public function actionPublishedEnabled($id) {

        if (($published = PreRecordedVideo::findOne($id)) !== null) {

            if ($published->status == 'UPLOADED') {
                $published->status = 'PUBLISHED';
                $published->publish_date = date('Y-m-d H:i:s', time());
				$published->published_by = Yii::$app->user->identity->role;
				if(Yii::$app->user->identity->role == 'OWNER' || Yii::$app->user->identity->role == 'ADMIN' || Yii::$app->user->identity->role == 'SUBADMIN') {
					$published->published_by_uuid = Yii::$app->user->identity->admin_uuid;
				} else if(Yii::$app->user->identity->role == 'TUTOR') {
					$published->published_by_uuid = Yii::$app->user->identity->tutor_uuid;
				} else {
					$published->published_by_uuid = Yii::$app->user->identity->student_uuid;
				}
                $published->save(false);

                if ($published->tutor_uuid != NULL) {
                    $user = \app\models\Tutor::find()->where(['uuid' => $published->tutor_uuid])->one();
                } else {
                    $user = \app\models\Admin::find()->where(['admin_uuid' => $published->admin_uuid])->one();
                }

                $content['name'] = $user->first_name . ' ' . $user->last_name;
                $content['title'] = $published->title;
                $content['link'] = Url::to(['/video/review/','id' => $published->uuid], true);
                //$content['link'] = Yii::$app->params['media']['pre_recorded_video']['url'] . $published->file;
                //$content['email'] = $user->email;
                Yii::$app->session['status'] = 'published';

                $owner = \app\libraries\General::getOwnersEmails(['OWNER', 'ADMIN', 'SUBADMIN']);

                Yii::$app->mailer->compose('video_published', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                        ->setFrom(Yii::$app->params['supportEmail'])
                        ->setTo($user->email)                           //$user->email
                        //->setBcc($owner)//pooja comment : ticket:1284
                        //->setSubject('Video Published Successfully - ' . Yii::$app->name)
                        ->setSubject(Yii::$app->name." Notification: Your video has been published successfully")                        
                        
                        ->send();

                $return = ['code' => 200, 'message' => 'Video is published successfully.'];
            } else {
                $return = ['code' => 421, 'message' => 'Video not published successfully.'];
            }
        } else {
            $return = ['code' => 404, 'message' => 'Record does not exist.'];
        }

        Yii::$app->response->format = 'json';
        return $return;
    }

    public function actionIsFreeEnabled($id) {

        if (($free = PreRecordedVideo::findOne($id)) !== null) {

            if ($free->is_free == '0') {

                $free->is_free = '1';
                $free->save(false);

                $return = ['code' => 200, 'message' => 'Video is free enabled successfully.'];
            } else {
                $return = ['code' => 421, 'message' => 'Video is free disabled successfully.'];
            }
        } else {
            $return = ['code' => 404, 'message' => 'Record does not exist.'];
        }

        Yii::$app->response->format = 'json';
        return $return;
    }

    public function actionIsFreeDisabled($id) {

        if (($free = PreRecordedVideo::findOne($id)) !== null) {

            if ($free->is_free == '1') {

                $free->is_free = '0';
                $free->save(false);

                $return = ['code' => 200, 'message' => 'Video is free disabled successfully.'];
            } else {
                $return = ['code' => 421, 'message' => 'Video is free enabled successfully.'];
            }
        } else {
            $return = ['code' => 404, 'message' => 'Record does not exist.'];
        }

        Yii::$app->response->format = 'json';
        return $return;
    }

    public function actionStudentVideoListOld() {

        $searchModel = new PreRecordedVideoSearch();

        $student = Student::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid])->one();

        if ($student->isMonthlySubscription == '1') {
            $where = " (is_free = 1 AND status = 'PUBLISHED') OR status = 'PUBLISHED' ";
        } else {
            $where = " (is_free = 1 AND status = 'PUBLISHED')";
        }
        $videoList = $searchModel->search(Yii::$app->request->queryParams, true, '', $where);

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('old/_student_video_list', [
                        'recodedVideoList' => $videoList,
            ]);
        } else {
            return $this->render('old/student_video', [
                        'recodedVideoList' => $videoList,
                        'searchModel' => $searchModel,
            ]);
        }
    }
    public function actionStudentVideoList() {

        $this->layout = "student_layout";
        
        if (Yii::$app->request->isAjax) {
            $page_no = (!empty(Yii::$app->request->get('page_no'))) ? Yii::$app->request->get('page_no') : 0;
            $instrument_uuid = (!empty(Yii::$app->request->get('instrument_uuid'))) ? Yii::$app->request->get('instrument_uuid') : "";
            $search_input = (!empty(Yii::$app->request->get('search_input'))) ? Yii::$app->request->get('search_input') : "";
            
            $videoList =  PreRecordedVideo::getInstrumentPreRecordedVideo($instrument_uuid, $page_no, $search_input);
            /*$videoListWithoutJson = array_map(function($e){
                 //if (isset($e['tutorUu']['working_plan'])) { unset($e['tutorUu']['working_plan']); }
                 if (isset($e['tutorUu']['profile_image'])) { unset($e['tutorUu']['profile_image']); }
                 if (isset($e['instrumentUu']['image'])) { unset($e['instrumentUu']['image']); }
                 return $e;
            }, $videoList);*/
            $return = [
                'code' => 200,
                'result' => (!empty($videoList['data']))?true:false,
                'dataHtml' => $videoList['html'],
                'message' => 'success'
            ];
            
            Yii::$app->response->format = 'json';
            return $return;
        } else {
            
            if (Yii::$app->request->isPost) {
                
                $page_no = (!empty(Yii::$app->request->post('page_no'))) ? Yii::$app->request->post('page_no') : 0;
                $instrument_uuid = (!empty(Yii::$app->request->post('instrument_uuid'))) ? Yii::$app->request->post('instrument_uuid') : "";
                $search_input = (!empty(Yii::$app->request->post('search_input'))) ? Yii::$app->request->post('search_input') : "";
                
                $videoList =  PreRecordedVideo::getInstrumentPreRecordedVideo($instrument_uuid, $page_no, $search_input);
                
            } else{
                $page_no = 0;
                $instrument_uuid = "";
                $search_input = "";
                $videoList =  PreRecordedVideo::getInstrumentPreRecordedVideo();
            }
            
            return $this->render('student_video', [
                        'recodedVideoList' => $videoList['data'],
                        'recodedVideoListHtml' => $videoList['html'],
                        'page_no' => $page_no,
                        'instrument_uuid' => $instrument_uuid,
                        'search_input' => $search_input,                        
            ]);
        }

        // $collection = PreRecordedVideo::find()
        // ->joinWith(['tutorUu','instrumentUu','adminUploadUu'])
        // ->where(["status"=>"PUBLISHED"])
        // ->all();
        // return $this->render('student_video', [
        //     'recodedVideoList' => [],
        //     'recodedVideoListHtml' => "",
        //     'page_no' => "",
        //     'instrument_uuid' => "",
        //     'search_input' => "",  

        //     "collection"  => $collection                    
        // ]);
    }

    public function actionRecorded() {
        $searchModel = new BookedSessionSearch();
        $searchModel->status = 'PUBLISHED';

        $videoList = $searchModel->search(Yii::$app->request->queryParams, true);

	//echo "<pre>";print_r($videoList);exit;
	if (Yii::$app->request->isAjax) {
		return $this->renderAjax('_recorded_list', [
		    'videoList' => $videoList,
		    'mode' => 'PUBLISHED',
		]);
	} else {
		return $this->render('recorded_video', [
		    'videoList' => $videoList,
		    'searchModel' => $searchModel,
		]);
	}
    }

    public function actionTutionView($id) {
      ob_clean();
        $p = isset($_GET['p']) ? $_GET['p'] : 1;
        try {
            if (Yii::$app->user->identity->role == 'STUDENT') {
		$model = \app\models\BookedSession::find()->joinWith(['studentUu','tutorUu','studentBookingUu'])->where(['booked_session.uuid' => $id,'booked_session.student_uuid'=> Yii::$app->user->identity->student_uuid])->one();
		if(!empty($model) && !$model->studentUu['isMonthlySubscription']){
			throw new \yii\web\HttpException(505, 'You do not have any monthly subscriptions.');
		}
           }elseif(Yii::$app->user->identity->role == 'TUTOR'){
		$model = \app\models\BookedSession::find()->joinWith(['studentUu','tutorUu','studentBookingUu'])->where(['booked_session.uuid' => $id,'booked_session.tutor_uuid'=> Yii::$app->user->identity->tutor_uuid])->one();
           }else{
   		$model = \app\models\BookedSession::find()->joinWith(['studentUu','tutorUu','studentBookingUu'])->where(['booked_session.uuid' => $id])->one();
           }
           if(empty($model)){
           	throw new \yii\web\HttpException(403, 'You are not authorised to access this page');
           }
            $model->scenario = 'UPDATE_VIDEO';
            $old_status = $model->status;
            $oldfilename = $model->filename;
            //$old_category = $model->instrument_category_uuid;
            $oldfilenamePath = Yii::$app->params['media']['session_recorded_video']['path'] . $oldfilename;
            $model->publish_date = (empty($model->publish_date)) ? date('Y-m-d H:i:s') : date('Y-m-d H:i:s', strtotime($model->publish_date));

            if ($model->status == "PUBLISHED") {
                Yii::$app->session['status'] = 'published';
            } else {
                Yii::$app->session['status'] = 'completed';
            }

            if (Yii::$app->request->isPost) {

                $model->attributes = Yii::$app->request->post('BookedSession');
                $file = UploadedFile::getInstance($model, 'update_file');
                $model->update_file = $file;
                $model->updated_at = date('Y-m-d H:i:s', time());
                if ($model->status == 'PUBLISH') {
                    if ($old_status != 'PUBLISH') {
                        $model->publish_date = date('Y-m-d H:i:s', strtotime($model->updated_at));
                    }
                }

                if ($model->validate()) {

                    $path = Yii::$app->params['media']['session_recorded_video']['path'];
                    if (!is_dir($path)) {
                        FileHelper::createDirectory($path);
                    }

                    if ($file) {
                        $fname = (!empty($model->filename)) ? $model->filename : $model->uuid . '.' . $file->extension;
                        $fileFullPath = $path . $fname;
                        $model->filename = $fname;
                    }

                    if ($model->status != 'PUBLISHED') {
                        $model->publish_date = NULL;
                    }

                    $model->save(false);
                    if ($model->status == "PUBLISHED") {
                        Yii::$app->session['status'] = 'published';
                    } else {
                        Yii::$app->session['status'] = 'completed';
                    }

                    if ($file) {
                        //delete old file
                        if (is_file($oldfilenamePath)) {
                            unlink($oldfilenamePath);
                        }

                        if ($file->saveAs($fileFullPath)) {

                            Yii::$app->getSession()->setFlash('success', 'Session video updated successfully.');
                            Yii::$app->response->format = 'json';
                            return [
                                'code' => 200,
                                'message' => 'Session video updated successfully.',
                            ];
                        }
                    } else {
                        Yii::$app->getSession()->setFlash('success', 'Session video updated successfully.');
                        Yii::$app->response->format = 'json';
                        return [
                            'code' => 200,
                            'message' => 'Session video updated successfully.',
                        ];
                    }
                }
            }



            if (Yii::$app->user->identity->role == 'STUDENT') {

                $this->layout = "student_layout";
                return $this->render('student_session_video_view', [
                            'model' => $model,
                            'back_url' => ['all-video'],
                ]);
            } else {

                if (Yii::$app->request->isAjax) {

                    return $this->renderAjax('_admin_session_update_form', [
                                'model' => $model,
                                'back_url' => ['recorded'],
                    ]);
                } else {
                    return $this->render('admin_session_update', [
                                'model' => $model,
                                'back_url' => ['recorded'],
                    ]);
                }
            }
        } catch (\yii\base\Exception $exception) {
            throw new \yii\web\HttpException(505, $exception->getMessage());
        }
    }
    
    public function actionTutionViewOld($id) {
      ob_clean();
        $p = isset($_GET['p']) ? $_GET['p'] : 1;
        try {
            if (Yii::$app->user->identity->role == 'STUDENT') {
		$model = \app\models\BookedSession::find()->joinWith(['studentUu','tutorUu','studentBookingUu'])->where(['booked_session.uuid' => $id,'booked_session.student_uuid'=> Yii::$app->user->identity->student_uuid])->one();
		if(!empty($model) && !$model->studentUu['isMonthlySubscription']){
			throw new \yii\web\HttpException(505, 'You do not have any monthly subscriptions.');
		}
           }elseif(Yii::$app->user->identity->role == 'TUTOR'){
		$model = \app\models\BookedSession::find()->joinWith(['studentUu','tutorUu','studentBookingUu'])->where(['booked_session.uuid' => $id,'booked_session.tutor_uuid'=> Yii::$app->user->identity->tutor_uuid])->one();
           }else{
   		$model = \app\models\BookedSession::find()->joinWith(['studentUu','tutorUu','studentBookingUu'])->where(['booked_session.uuid' => $id])->one();
           }
           if(empty($model)){
           	throw new \yii\web\HttpException(403, 'You are not authorised to access this page');
           }
            $model->scenario = 'UPDATE_VIDEO';
            $old_status = $model->status;
            $oldfilename = $model->filename;
            //$old_category = $model->instrument_category_uuid;
            $oldfilenamePath = Yii::$app->params['media']['session_recorded_video']['path'] . $oldfilename;
            $model->publish_date = (empty($model->publish_date)) ? date('Y-m-d H:i:s') : date('Y-m-d H:i:s', strtotime($model->publish_date));

            if ($model->status == "PUBLISHED") {
                Yii::$app->session['status'] = 'published';
            } else {
                Yii::$app->session['status'] = 'completed';
            }

            if (Yii::$app->request->isPost) {

                $model->attributes = Yii::$app->request->post('BookedSession');
                $file = UploadedFile::getInstance($model, 'update_file');
                $model->update_file = $file;
                $model->updated_at = date('Y-m-d H:i:s', time());
                if ($model->status == 'PUBLISH') {
                    if ($old_status != 'PUBLISH') {
                        $model->publish_date = date('Y-m-d H:i:s', strtotime($model->updated_at));
                    }
                }

                if ($model->validate()) {

                    $path = Yii::$app->params['media']['session_recorded_video']['path'];
                    if (!is_dir($path)) {
                        FileHelper::createDirectory($path);
                    }

                    if ($file) {
                        $fname = (!empty($model->filename)) ? $model->filename : $model->uuid . '.' . $file->extension;
                        $fileFullPath = $path . $fname;
                        $model->filename = $fname;
                    }

                    if ($model->status != 'PUBLISHED') {
                        $model->publish_date = NULL;
                    }

                    $model->save(false);
                    if ($model->status == "PUBLISHED") {
                        Yii::$app->session['status'] = 'published';
                    } else {
                        Yii::$app->session['status'] = 'completed';
                    }

                    if ($file) {
                        //delete old file
                        if (is_file($oldfilenamePath)) {
                            unlink($oldfilenamePath);
                        }

                        if ($file->saveAs($fileFullPath)) {

                            Yii::$app->getSession()->setFlash('success', 'Session video updated successfully.');
                            Yii::$app->response->format = 'json';
                            return [
                                'code' => 200,
                                'message' => 'Session video updated successfully.',
                            ];
                        }
                    } else {
                        Yii::$app->getSession()->setFlash('success', 'Session video updated successfully.');
                        Yii::$app->response->format = 'json';
                        return [
                            'code' => 200,
                            'message' => 'Session video updated successfully.',
                        ];
                    }
                }
            }



            if (Yii::$app->user->identity->role == 'STUDENT') {

                return $this->render('student_session_video_view', [
                            'model' => $model,
                            'back_url' => ['all-video'],
                ]);
            } else {

                if (Yii::$app->request->isAjax) {

                    return $this->renderAjax('_admin_session_update_form', [
                                'model' => $model,
                                'back_url' => ['recorded'],
                    ]);
                } else {
                    return $this->render('admin_session_update', [
                                'model' => $model,
                                'back_url' => ['recorded'],
                    ]);
                }
            }
        } catch (\yii\base\Exception $exception) {
            throw new \yii\web\HttpException(505, $exception->getMessage());
        }
    }

    public function actionSessionView($id) {

        try {

           if (!Yii::$app->user->identity->role == 'STUDENT') {
               throw new \yii\web\HttpException(403, 'You are not authorised to access this page');
           }

           $model = \app\models\BookedSession::find()->joinWith(['studentUu','tutorUu','studentBookingUu'])->where(['booked_session.uuid' => $id])->one();

            $model->scenario = 'UPDATE_VIDEO';
            $old_status = $model->status;
            $oldfilename = $model->filename;

            $oldfilenamePath = Yii::$app->params['media']['session_recorded_video']['path'] . $oldfilename;
            $model->publish_date = (empty($model->publish_date)) ? date('Y-m-d') : date('Y-m-d', strtotime($model->publish_date));

            if ($model->status == "PUBLISHED") {
                Yii::$app->session['status'] = 'published';
            } else {
                Yii::$app->session['status'] = 'completed';
            }



            return $this->render('student_session_video_view', [
                            'model' => $model,
                ]);

        } catch (\yii\base\Exception $exception) {
            throw new \yii\web\HttpException(505, $exception->getMessage());
        }
    }

    public function actionSessionPublish($id) {

        if (($published = BookedSession::find()->joinWith(['studentUu','tutorUu','studentBookingUu'])->where(['booked_session.uuid' => $id])->one() ) !== null) {

            if ($published->status == 'COMPLETED') {

                $published->status = 'PUBLISHED';
                $published->publish_date = date('Y-m-d H:i:s', time());
                $published->save(false);


                $content['tutor_name'] = $published->tutorUu->first_name . ' ' . $published->tutorUu->last_name;
                $content['student_name'] = $published->studentUu->first_name . ' ' . $published->studentUu->last_name;
                $content['title'] = $published->studentUu->first_name . ' ' . $published->studentUu->last_name. ' - '. date('Y-m-d h:i A', strtotime($published->start_datetime));
                $content['link'] = Url::to(['video/tution-view','id' => $id],true);

                Yii::$app->session['status'] = 'published';
                $owner = \app\libraries\General::getOwnersEmails(['OWNER', 'ADMIN', 'SUBADMIN']);

                Yii::$app->mailer->compose('recorded_video_published_tutor', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                        ->setFrom(Yii::$app->params['supportEmail'])
                        ->setTo($published->tutorUu->email)
                        ->setBcc($owner)
                        ->setSubject('Session video Published Successfully - ' . Yii::$app->name)
                        ->send();

                Yii::$app->mailer->compose('recorded_video_published_tutor', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                        ->setFrom(Yii::$app->params['supportEmail'])
                        ->setTo($published->studentUu->email)                //$published->studentUu->email
                        ->setBcc($owner)
                        ->setSubject('Session video Published Successfully - ' . Yii::$app->name)
                        ->send();

                $return = ['code' => 200, 'message' => 'Session video is published successfully.'];
            } else {
                $return = ['code' => 421, 'message' => 'Sorry session video does not published successfully.'];
            }
        } else {
            $return = ['code' => 404, 'message' => 'Record does not exist.'];
        }

        Yii::$app->response->format = 'json';
        return $return;
    }

    public function actionTutionMoreDetails($id) {

        if (($published =  \app\models\BookedSession::find()->joinWith(['studentUu','tutorUu','studentBookingUu'])->where(['booked_session.uuid' => $id])->one() ) !== null) {
      //  $model = \app\models\BookedSession::find()->joinWith(['studentUu','tutorUu','studentBookingUu'])->where(['booked_session.uuid' => $id])->one();

            $instrument = \app\models\Instrument::find()->where(['uuid' => $published['studentBookingUu']['instrument_uuid']])->one();
            //print_r($instrument);exit;


            $return['student_name'] = $published['studentUu']['first_name'].' '.$published['studentUu']['last_name'];
            $return['tutor_name'] = $published['tutorUu']['first_name'].' '.$published['tutorUu']['last_name'];
	    $return['instrument'] = $instrument['name'];
            $return['session_date'] = \app\libraries\General::displayDate($published['start_datetime']);
	    $return['session_time'] = \app\libraries\General::displayTime($published['start_datetime']).' - '.\app\libraries\General::displayTime($published['end_datetime']);
        }

        Yii::$app->response->format = 'json';
        return $return;
    }

    public function actionPlay($id) {
        ob_clean();
        $access = PreRecordedVideo::loginUserAuthorizePlayVideo($id);
        if (!$access) {
            throw new \yii\web\HttpException(505, 'You are not authorised to access this page');
        }

        //cache limiter
        session_cache_limiter('public');
        //get call recording from database
        $uuid = $id;
        $record_file = '';
        if ($uuid != '') {

            if (($model = PreRecordedVideo::findOne($uuid)) !== null) {
                ob_clean();
                $record_file = Yii::$app->params['media']['pre_recorded_video']['path'] . '/' . $model->file;
                //header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
                return \Yii::$app->response->sendFile($record_file);
                ob_clean();
            }
        }
    }

    public function actionDownload($id) {
        ob_clean();
        $access = PreRecordedVideo::loginUserAuthorizePlayVideo($id);
        if (!$access) {
            throw new \yii\web\HttpException(505, 'You are not authorised to access this page');
        }

        //cache limiter
        //session_cache_limiter('public');
        //get call recording from database
        $uuid = $id;
        $record_file = '';
        if ($uuid != '') {

            if (($model = PreRecordedVideo::findOne($uuid)) !== null) {

                $record_file = Yii::$app->params['media']['pre_recorded_video']['path'] . '/' . $model->file;

                return \Yii::$app->response->sendFile($record_file);
            }
        }
        throw new \yii\web\HttpException(404, 'Page not found');
    }

    public function actionPlaytution($id, $p) {
        ob_clean();
        $access = PreRecordedVideo::loginUserAuthorizePlayTutionVideo($id);
        if (!$access) {
            throw new \yii\web\HttpException(505, 'You are not authorised to access this page');
        }

        //cache limiter
        session_cache_limiter('public');
        //get call recording from database

        $record_file = '';
        if ($id != '' && $p != '') {
            if (($model = BookedSession::findOne($id)) !== null) {
                ob_clean();
                $video_list = General::getLessonVideoListUsingUuid($id);
                $filename = (!empty($video_list)) ? $video_list[$p]: $id.".mp4";
                $record_file = Yii::$app->params['media']['session_recorded_video']['path'] . $id . '/'. $filename;
                //header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
                return \Yii::$app->response->sendFile($record_file);
                ob_clean();
            }
        }
    }

    public function actionCategoryupdate($id) {

        $model = PreRecordedVideo::findOne($id);

        $model->attributes = Yii::$app->getRequest()->post('Video');

        if ($model->load(Yii::$app->request->post()) && $model->validate() ) {

            $model->instrument_category_uuid = (!empty($model->instrument_category_uuid)) ? $model->instrument_category_uuid : NULL;
            $model->save(false);
            Yii::$app->response->format = 'json';
            $return = ['code' => 200, 'message' => "Category updated successfully"];
        }
        return $this->renderAjax('_admin_category_edit_form', [
            'model' => $model,
        ]);
    }

    public function actionReport() {

        $searchModel = new PreRecordedVideoSearch();
        $report = $searchModel->searchReport(Yii::$app->request->queryParams, true, '', '', '', '');

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_report_list', [
                        'report' => $report,
                        'searchModel' => $searchModel,
            ]);
        } else {
            return $this->render('report', [
                        'report' => $report,
                        'searchModel' => $searchModel,
            ]);
        }
    }

    public function actionVideoCategoryUpdate($id) {
        if (!Yii::$app->request->isAjax) {
            Yii::$app->response->format = 'json';
            $return = ['code' => 505, 'message' => 'You are not authorised to access this page'];
        }
        $model = $this->findModel($id);
        $old_status = $model->status;
        $oldfilename = $model->file;
        $old_category = $model->instrument_category_uuid;
        $oldfilenamePath = Yii::$app->params['media']['pre_recorded_video']['path'] . $oldfilename;

        if (Yii::$app->request->isAjax) {
            $model->attributes = Yii::$app->request->post('PreRecordedVideo');
            $id = $_GET['id'];
            $file = $id . ".mp4";

            if ($file) {
                $title = $_POST['title'];
                $instrument_category_uuid = $_POST['instrument_category_uuid'];
                $instrument_uuid = $_POST['instrument_uuid'];
                $path = Yii::$app->params['media']['pre_recorded_video']['path'];
                $model->uuid = $id;
                $model->title = $title;
                if($instrument_uuid == '') {
                    $model->instrument_uuid = null;
                } else {
                    $model->instrument_uuid = $instrument_uuid;
                }
                if($instrument_category_uuid == '') {
                    $model->instrument_category_uuid = null;
                } else {
                    $model->instrument_category_uuid = $instrument_category_uuid;
                }
                $model->updated_at = date('Y-m-d H:i:s', time());
                $model->file = $file;
                $fileFullPath = $path . $file;

                if ($model->save(false)) {
                    $return = ['code' => 200, 'message' => 'Video updated successfully.'];
                } else {
                    $return = ['code' => 504, 'message' => 'Video not updated successfully.'];
                }
            }
            Yii::$app->response->format = 'json';
            return $return;
        }
    }

    public function actionPublishedVideo() {
        $id = Yii::$app->getRequest()->get('id');
        $searchModel = new PreRecordedVideoSearch();
        $searchModel->status = 'PUBLISHED';
        $searchModel->instrument_category_uuid = $id;
        $videoList = $searchModel->searchCategoryList(Yii::$app->request->queryParams, true);

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_get_video_list', [
                        'videoList' => $videoList,
                        'mode' => 'UPLOADED',
            ]);
        } else {
            return $this->render('get_video', [
                        'videoList' => $videoList,
                        'searchModel' => $searchModel,
            ]);
        }
    }

    public function actionDeleteVideo($id) {
        if (!Yii::$app->request->isAjax) {
            Yii::$app->response->format = 'json';
            $return = ['code' => 505, 'message' => 'You are not authorised to access this page'];
        }

        $model = PreRecordedVideo::findOne($id);

        $file = $id . ".mp4";
        $file = Yii::$app->params['media']['pre_recorded_video']['path'] . $file;
        if (file_exists($file)) {
            unlink($file);
        }
        $image = $id . ".jpg";
        $image = Yii::$app->params['media']['pre_recorded_video']['thumbnail']['path'] . $image;
        if (file_exists($image)) {
            unlink($image);
        }
        $model->delete();
        if($model->status == "PUBLISHED") {
            $return = ['code' => 200, 'message' => "Published video deleted successfully", 'status' => 'published'];
        } else {
            $return = ['code' => 200, 'message' => "Uploaded video deleted successfully", 'status' => 'uploaded'];
        }
        Yii::$app->response->format = 'json';
        return $return;
    }
    
    public function actionStudent_view_session_count($id,$part) {
        $model = BookedSession::find()->where(['uuid' => $id])->one();
        
        $modelinsert = new StudentViewRecordedLesson();
        $modelinsert->booked_session_uuid = $id;
        $modelinsert->part = $part;
        $modelinsert->student_uuid = Yii::$app->user->identity->student_uuid;
        $modelinsert->tutor_uuid = $model->tutor_uuid;
        $modelinsert->instrument_uuid = $model->instrument_uuid;
        $modelinsert->datetime = General::convertUserToSystemTimezone(date('Y-m-d H:i:s'));
        if($modelinsert->save(false)) {
            $return = ['code' => 200, 'message' => "Data saved successfully."];
        } else {
            $return = ['code' => 201, 'message' => "Error in Video Count"];
        }
        Yii::$app->response->format = 'json';
        return $return;
        
    }
    
    
    public function actionStudent_view_pre_recorded_count($id) {
        $model = PreRecordedVideo::find()->where(['uuid' => $id])->one();
        
        $modelinsert = new StudentViewPreRecordedLesson();
        $modelinsert->pre_recorded_video_uuid = $id;
        $modelinsert->title = $model->title;
        $modelinsert->student_uuid = Yii::$app->user->identity->student_uuid;
        $modelinsert->tutor_uuid = $model->tutor_uuid;
        $modelinsert->instrument_uuid = $model->instrument_uuid;
        $modelinsert->datetime = General::convertUserToSystemTimezone(date('Y-m-d H:i:s'));
        if($modelinsert->save(false)) {
            $return = ['code' => 200, 'message' => "Data saved successfully."];
        } else {
            $return = ['code' => 201, 'message' => "Error in Video Count"];
        }
        Yii::$app->response->format = 'json';
        return $return;
        
    }
    
    public function actionDownloadRecording($id) {
            
             if ($id != '') {
$p = isset($_GET['p']) ? $_GET['p'] : 1;
            $videos = General::getLessonVideoListUsingUuid($id);
$filename = (!empty($videos)) ? $videos[$p] : $id . ".mp4";
$file_path = Yii::$app->params['media']['session_recorded_video']['path'] . $id . '/' . $filename;
$file_url = Yii::$app->params['media']['session_recorded_video']['url'] . $id . '/' . $filename;

                return \Yii::$app->response->sendFile($file_path);
           
        }
        
        
        /*header ('Content-type: octet/stream');
    header ('Content-disposition: attachment; filename='.$file.';');
    header('Content-Length: '.filesize($file));
    readfile($file);
    exit;*/
        
    }
    

}
