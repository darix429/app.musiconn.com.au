<?php

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use app\models\BookedSession;
use app\models\Student;
use app\models\Tutor;
use app\models\RescheduleSession;
use app\models\StudentNotification;
use app\models\TutorNotification;
use app\libraries\General;
use Yii;

class StartBookedSessionController extends Controller {

    public function actionIndex($message = 'StartBookedSession cron Index') {
        echo $message . "\n";

        return ExitCode::OK;
    }

    public function actionNotification() {

        $currDate = date("Y-m-d H:i:s");
        $start_date_before_10 = date('Y-m-d H:i', strtotime('+10 minutes', strtotime($currDate)));
        $start_date_before_30 = date('Y-m-d H:i', strtotime('+30 minutes', strtotime($currDate)));
        $before_minute_time_array = [$start_date_before_10, $start_date_before_30];
        $bookedSessionlist = BookedSession::find()->where(['in', "to_char(start_datetime, 'YYYY-MM-DD HH24:MI')", $before_minute_time_array])->andwhere(['status' => 'SCHEDULE'])->all();

        if (!empty($bookedSessionlist)) {
            foreach ($bookedSessionlist as $key => $value) {
                $student = \app\models\User::find()->joinWith('student')->where(['user.student_uuid' => $value->student_uuid, 'student.student_uuid' => $value->student_uuid, 'student.status' => 'ENABLED'])->one();
                $tutor = Tutor::find()->where(['uuid' => $value->tutor_uuid, 'status' => 'ENABLED'])->one();
                $tutor_tz = General::getTutorTimezone($value->tutor_uuid);
                
                $bookedSessionDetails = array(
                    'tutor_first_name' => $tutor->first_name,
                    'tutor_last_name' => $tutor->last_name,
                    'tutor_email' => $tutor->email,
                    'student_first_name' => $student->first_name,
                    'student_last_name' => $student->last_name,
                    'student_email' => $student->email,
                    'start_datetime' => General::convertTimezone($value->start_datetime, Yii::$app->params['timezone'], $student->timezone, 'd-m-Y H:i T'),
                    'tutor_start_datetime' => General::convertTimezone($value->start_datetime, Yii::$app->params['timezone'], $tutor_tz, 'd-m-Y H:i T'),
                    'session_pin' => $value->session_pin,
                    'uuid' => $value->uuid,
                );

                //php /var/www/html/musiconn/yii start-booked-session/notification
                BookedSession::startBookedSessionNotification($bookedSessionDetails);
            }
        }
    }

    public function actionSessionExpire() {

//        $currDate = date("Y-m-d H:i T");
        $currDate = date("Y-m-d H:i");
        $modelBS = BookedSession::find()->andwhere(['<', "to_char(end_datetime, 'YYYY-MM-DD HH24:MI')", $currDate])->andwhere(['status' => 'SCHEDULE'])->asArray()->all();

        if (!empty($modelBS)) {
            foreach ($modelBS as $key => $value) {

                $end_datetime = date('Y-m-d H:i', strtotime($value['end_datetime']));
                if ($currDate >= $end_datetime) {
                    $modelBS1 = BookedSession::find()->where(['uuid' => $value['uuid'], 'status' => 'SCHEDULE'])->one();

                    $modelBS1->status = 'EXPIRED';
                    $modelBS1->updated_at = date('Y-m-d H:i:s', time());
                    $modelBS1->save(false);
                }
            }
        }
    }

    public function actionSessionComplete() {
        $currDate = date("Y-m-d H:i");
	echo "\n-------Start time--".date("Y-m-d H:i:s T")."-------";
        $modelBS = BookedSession::find()->andwhere(['<=', "to_char(end_datetime, 'YYYY-MM-DD HH24:MI')", $currDate])->andwhere(['status' => 'INPROCESS'])->asArray()->all();

        if (!empty($modelBS)) {
            foreach ($modelBS as $key => $value) {

                /*                 * ***check file exist or not** */
                /*$file_list = [];
                $glob_path = Yii::$app->params['media']['session_recorded_video']['path'] . $value['uuid'] . '/' . $value['uuid'] . '*.*';
                $file_glob_array = glob($glob_path);
                if (is_array($file_glob_array) && count($file_glob_array) > 0) {
                    foreach ($file_glob_array as $k => $v) {
                        $file_list[] = basename($v);
                    }
                }*/

                $end_datetime = date('Y-m-d H:i', strtotime($value['end_datetime']));
                //Jitsi changes
                $end_datetime_after_10min = date('Y-m-d H:i', strtotime($value['end_datetime']." +20 minute"));
echo "\n-------lesson end time--".$end_datetime_after_10min."-------";
                if ($end_datetime_after_10min <= $currDate) {

                    $modelBS1 = BookedSession::find()->where(['uuid' => $value['uuid'], 'status' => 'INPROCESS'])->one();

		    //move file into media folder - start
                    $lesson_uuid = $value['uuid'];
		    $file_uuid = strtolower(base64_encode($lesson_uuid));

                    $file_list = [];
                    $jitsi_path = Yii::$app->params['jitsi']['video_path'];
                    $glob_path = $jitsi_path . $file_uuid . '*.*';
                    $file_glob_array = glob($glob_path);
		    
                    if (is_array($file_glob_array) && count($file_glob_array) > 0) {
                        foreach ($file_glob_array as $k => $v) {

                            $source_filename = basename($v);
			    $file_arr = explode('_',$source_filename);
			    $newFileName = $lesson_uuid.'_'.$file_arr[1];
                            
                            $file_media_path = Yii::$app->params['media']['session_recorded_video']['path']. $lesson_uuid;
                            $path = \app\libraries\General::CreateDir($file_media_path);

                            if(is_file($v) && is_dir($file_media_path)){
                                if( copy($v, $file_media_path.'/'.$newFileName) ){

                                    $file_list[] = $newFileName;
                                    //delete source file
                                    unlink($v);
                                }
                            }
                            
                        }
                    }
		    echo "\n-------UUID=".$lesson_uuid."-------File glob array count=".count($file_glob_array)."---";
                    //move file into media folder - end	


                    if (!empty($modelBS1)) {

                        //$memberCount = shell_exec('/usr/local/freeswitch/bin/fs_cli -x "conference ' . $value['uuid'] . ' list count"');
                        //$memberCount = (in_array($memberCount, [1, 2])) ? $memberCount : 0;
                        
                        //Jitsi update
                        $memberCount = 0;
                        
                        //self::filelog([date('Y-m-d H:i:s T'),$value['uuid'],$memberCount]);

                        if ($memberCount < 2) {

                            //$status_set = "PUBLISHED";
                            $status_set = (count($file_list) > 0 ) ? "PUBLISHED" : "EXPIRED";
				if($status_set == "EXPIRED"){
					 $modelBS2 = BookedSession::find()->where(['uuid' => $value['uuid'], 'status' => 'INPROCESS','student_login_flag'=>1,'tutor_login_flag'=>1])->one();
					if(!empty($modelBS2))
					{
						$status_set = 'COMPLETED';
					}
			  	}
                            if (count($file_list) > 0) {
				$modelBS1->filename = $file_list[0];
                                General::lessonVideoThumbnailGenerate($file_list[0], $value['uuid']);
                            }
                            $modelBS1->status = $status_set;
                            $modelBS1->updated_at = date('Y-m-d H:i:s', time());
                            $modelBS1->save(false);
                            if ($status_set == "PUBLISHED") {
                                BookedSession::lessonCompletedMail($value['uuid'], "cron");
                            }
                        }
                    }
                }
            }
        }
	echo "\n-------End time--".date("Y-m-d H:i:s")."-------\n";
    }


    public function actionSessionRescheduleRequestExpire() {
        $currDate = date("Y-m-d H:i");
        
	$where = "(to_char(from_start_datetime, 'YYYY-MM-DD HH24:MI') <= '".$currDate."' AND \"status\"='AWAITING') OR (to_char(to_start_datetime, 'YYYY-MM-DD HH24:MI') <= '".$currDate."' AND \"status\"='AWAITING')";
	$modelRS = RescheduleSession::find()->where($where)->asArray()->all();
        
        if (!empty($modelRS)) {
            
            foreach ($modelRS as $key => $RSvalue) {
                
                $modelRS1 = RescheduleSession::find()->where(['uuid' => $RSvalue['uuid']])->one();
                if(!empty($modelRS1)){
                    $modelRS1->status = 'EXPIRED';
                    $modelRS1->save(false);
                }
                
                $modelBS = BookedSession::find()->where(['uuid' => $RSvalue['booked_session_uuid']])->one();
                if(!empty($modelBS)){
                    $modelBS->is_reschedule_request = 0;
                    $modelBS->save(false);
                }
            }
        }        
    }

//     public function filelog($log = '', $filename = 'cron_test.txt') {
//        
//        $file =  "/var/www/html/" . $filename;
//        if (!is_file($file)) {
//            $fp = fopen($file, "wb");
//        } else {
//            $fp = fopen($file, "a+");
//        }
//        fwrite($fp, print_r($log, true));
//        //file_put_contents($file, print_r($log, true));
//        fclose($fp);
//        return 1;
//    }

    //This cron execute when student session start before minutes for each lessons
    public function actionStudentNotificationMinute() {
        $currDate = date("Y-m-d H:i:s");
        $student_before_minute_old = StudentNotification::find()->where(['time_unit' => 'MINUTES'])->asArray()->all();
        if (!empty($student_before_minute_old)) {
            foreach ($student_before_minute_old as $key => $student_minute_value) {
                $student_before_minute_time_array[] = date('Y-m-d H:i', strtotime('+'.$student_minute_value['unit_value'].' minutes', strtotime($currDate)));
                $student_uuid_array[] = $student_minute_value['student_uuid'];
            }
            $studentMinuteBookedSessionlist = BookedSession::find()->joinWith(['studentUu'])->where(['booked_session.student_uuid' => $student_uuid_array, 'student.student_uuid' => $student_uuid_array])->andWhere(['in', "to_char(start_datetime, 'YYYY-MM-DD HH24:MI')", $student_before_minute_time_array])->andWhere(['booked_session.status' => 'SCHEDULE'])->asArray()->all();
            if (!empty($studentMinuteBookedSessionlist)) {
                foreach ($studentMinuteBookedSessionlist as $key => $result) {
                    $student_before_minute = StudentNotification::find()->where(['time_unit' => 'MINUTES', 'student_uuid' => $result['student_uuid']])->asArray()->all();
                    if (!empty($student_before_minute)) {
                        foreach ($student_before_minute as $key => $student_minute) {
                            $before_array[] = date('Y-m-d H:i', strtotime('+'.$student_minute['unit_value'].' minutes', strtotime($currDate)));
                            $uuid_array[] = $student_minute['student_uuid'];
                        }
                        $studentMinutebookedLessionlist = BookedSession::find()->joinWith(['studentUu','tutorUu'])->where(['booked_session.student_uuid' => $uuid_array, 'student.student_uuid' => $uuid_array])->andWhere(['in', "to_char(start_datetime, 'YYYY-MM-DD HH24:MI')", $before_array])->andWhere(['booked_session.status' => 'SCHEDULE'])->asArray()->all();
                        if (!empty($studentMinutebookedLessionlist)) {
                            foreach ($studentMinutebookedLessionlist as $key => $value) {
                                $student = \app\models\User::find()->joinWith('student')->where(['user.student_uuid' => $value['student_uuid'], 'student.student_uuid' => $value['student_uuid']])->asArray()->one();
                                //$owner = General::getOwnersEmails(['OWNER', 'ADMIN', 'SUBADMIN']);

                                $content['student_first_name'] = $student['first_name'];
                                $content['student_last_name'] = $student['last_name'];
                                $content['tutor_name'] = $value['tutorUu']['first_name'].' '.$value['tutorUu']['last_name'];
                                $content['start_datetime'] = General::convertTimezone($value['start_datetime'], Yii::$app->params['timezone'], $student['timezone'], 'Y-m-d H:i:s T');
                                $content['session_pin'] = $value['session_pin'];
                                $content['uuid'] = $value['uuid'];

                                
                                Yii::$app->mailer->compose('booked_session_start_notification_student_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                                    ->setFrom(Yii::$app->params['supportEmail'])
                                    ->setTo($student['email'])
                                    ->setSubject("Musiconn Reminder: A Scheduled Lesson is due to Start.")
                                    ->send();
                            }
                        }
                    }
                }
            }
        }
    }

    //This cron execute when student session start before hours for each lessons
    public function actionStudentNotificationHour() {
        $currDate = date("Y-m-d H:i:s");
        $student_before_hour_old = StudentNotification::find()->where(['time_unit' => 'HOURS'])->asArray()->all();
        if (!empty($student_before_hour_old)) {
            foreach ($student_before_hour_old as $key => $student_hour_value) {
                $student_before_hour_time_array[] = date('Y-m-d H:i', strtotime('+'.$student_hour_value['unit_value'].' hours', strtotime($currDate)));
                $student_uuid_array[] = $student_hour_value['student_uuid'];
            }
            $studentHourBookedSessionlist = BookedSession::find()->joinWith(['studentUu'])->where(['booked_session.student_uuid' => $student_uuid_array, 'student.student_uuid' => $student_uuid_array])->andWhere(['in', "to_char(start_datetime, 'YYYY-MM-DD HH24:MI')", $student_before_hour_time_array])->andWhere(['booked_session.status' => 'SCHEDULE'])->asArray()->all();
            if (!empty($studentHourBookedSessionlist)) {
                foreach ($studentHourBookedSessionlist as $key => $result) {
                    $student_before_hour = StudentNotification::find()->where(['time_unit' => 'HOURS', 'student_uuid' => $result['student_uuid']])->asArray()->all();
                    if (!empty($student_before_hour)) {
                        foreach ($student_before_hour as $key => $student_hour) {
                            $before_array[] = date('Y-m-d H:i', strtotime('+'.$student_hour['unit_value'].' hours', strtotime($currDate)));
                            $uuid_array[] = $student_hour['student_uuid'];
                        }
                        $studentHourbookedLessionlist = BookedSession::find()->joinWith(['studentUu','tutorUu'])->where(['booked_session.student_uuid' => $uuid_array, 'student.student_uuid' => $uuid_array])->andWhere(['in', "to_char(start_datetime, 'YYYY-MM-DD HH24:MI')", $before_array])->andWhere(['booked_session.status' => 'SCHEDULE'])->asArray()->all();
                        if (!empty($studentHourbookedLessionlist)) {
                            foreach ($studentHourbookedLessionlist as $key => $value) {
                                $student = \app\models\User::find()->joinWith('student')->where(['user.student_uuid' => $value['student_uuid'], 'student.student_uuid' => $value['student_uuid']])->asArray()->one();
                                //$owner = General::getOwnersEmails(['OWNER', 'ADMIN', 'SUBADMIN']);

                                $content['student_first_name'] = $student['first_name'];
                                $content['student_last_name'] = $student['last_name'];
                                $content['tutor_name'] = $value['tutorUu']['first_name'].' '.$value['tutorUu']['last_name'];
                                $content['start_datetime'] = General::convertTimezone($value['start_datetime'], Yii::$app->params['timezone'], $student['timezone'], 'Y-m-d H:i:s T');
                                $content['session_pin'] = $value['session_pin'];
                                $content['uuid'] = $value['uuid'];

                                
                                Yii::$app->mailer->compose('booked_session_start_notification_student_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                                    ->setFrom(Yii::$app->params['supportEmail'])
                                    ->setTo($student['email'])
                                    //->setBcc($owner)
                                    ->setSubject("Musiconn Reminder: A Scheduled Lesson is due to Start.")
                                    ->send();
                            }
                        }
                    }
                }
            }
        }
    }

    //This cron execute when student session start before days for each lessons
    public function actionStudentNotificationDay() {
        $currDate = date("Y-m-d H:i:s");
        $student_before_day_old = StudentNotification::find()->where(['time_unit' => 'DAYS'])->asArray()->all();
        if (!empty($student_before_day_old)) {
            foreach ($student_before_day_old as $key => $student_day_value) {
                $student_before_day_time_array[] = date('Y-m-d H:i', strtotime('+'.$student_day_value['unit_value'].' days', strtotime($currDate)));
                $student_uuid_array[] = $student_day_value['student_uuid'];
            }
            $studentDayBookedSessionlist = BookedSession::find()->joinWith(['studentUu'])->where(['booked_session.student_uuid' => $student_uuid_array, 'student.student_uuid' => $student_uuid_array])->andWhere(['in', "to_char(start_datetime, 'YYYY-MM-DD HH24:MI')", $student_before_day_time_array])->andWhere(['booked_session.status' => 'SCHEDULE'])->asArray()->all();
            if (!empty($studentDayBookedSessionlist)) {
                foreach ($studentDayBookedSessionlist as $key => $result) {
                    $student_before_day = StudentNotification::find()->where(['time_unit' => 'DAYS', 'student_uuid' => $result['student_uuid']])->asArray()->all();
                    if (!empty($student_before_day)) {
                        foreach ($student_before_day as $key => $student_day) {
                            $before_array[] = date('Y-m-d H:i', strtotime('+'.$student_day['unit_value'].' days', strtotime($currDate)));
                            $uuid_array[] = $student_day['student_uuid'];
                        }
                        $studentDaybookedLessionlist = BookedSession::find()->joinWith(['studentUu','tutorUu'])->where(['booked_session.student_uuid' => $uuid_array, 'student.student_uuid' => $uuid_array])->andWhere(['in', "to_char(start_datetime, 'YYYY-MM-DD HH24:MI')", $before_array])->andWhere(['booked_session.status' => 'SCHEDULE'])->asArray()->all();
                        if (!empty($studentDaybookedLessionlist)) {
                            foreach ($studentDaybookedLessionlist as $key => $value) {
                                $student = \app\models\User::find()->joinWith('student')->where(['user.student_uuid' => $value['student_uuid'], 'student.student_uuid' => $value['student_uuid']])->asArray()->one();
                                //$owner = General::getOwnersEmails(['OWNER', 'ADMIN', 'SUBADMIN']);

                                $content['student_first_name'] = $student['first_name'];
                                $content['student_last_name'] = $student['last_name'];
                                $content['tutor_name'] = $value['tutorUu']['first_name'].' '.$value['tutorUu']['last_name'];
                                $content['start_datetime'] = General::convertTimezone($value['start_datetime'], Yii::$app->params['timezone'], $student['timezone'], 'Y-m-d H:i:s T');
                                $content['session_pin'] = $value['session_pin'];
                                $content['uuid'] = $value['uuid'];

                                
                                Yii::$app->mailer->compose('booked_session_start_notification_student_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                                    ->setFrom(Yii::$app->params['supportEmail'])
                                    ->setTo($student['email'])
                                    //->setBcc($owner)
                                    ->setSubject("Musiconn Reminder: A Scheduled Lesson is due to Start.")
                                    ->send();
                            }
                        }
                    }
                }
            }
        }
    }

    //This cron execute when student session start before weeks for each lessons
    public function actionStudentNotificationWeek() {
        $currDate = date("Y-m-d H:i:s");
        $student_before_week_old = StudentNotification::find()->where(['time_unit' => 'WEEKS'])->asArray()->all();
        if (!empty($student_before_week_old)) {
            foreach ($student_before_week_old as $key => $student_week_value) {
                $student_before_week_time_array[] = date('Y-m-d H:i', strtotime('+'.$student_week_value['unit_value'].' weeks', strtotime($currDate)));
                $student_uuid_array[] = $student_week_value['student_uuid'];
            }
            $studentWeekBookedSessionlist = BookedSession::find()->joinWith(['studentUu'])->where(['booked_session.student_uuid' => $student_uuid_array, 'student.student_uuid' => $student_uuid_array])->andWhere(['in', "to_char(start_datetime, 'YYYY-MM-DD HH24:MI')", $student_before_week_time_array])->andWhere(['booked_session.status' => 'SCHEDULE'])->asArray()->all();
            if (!empty($studentWeekBookedSessionlist)) {
                foreach ($studentWeekBookedSessionlist as $key => $result) {
                    $student_before_week = StudentNotification::find()->where(['time_unit' => 'WEEKS', 'student_uuid' => $result['student_uuid']])->asArray()->all();
                    if (!empty($student_before_week)) {
                        foreach ($student_before_week as $key => $student_week) {
                            $before_array[] = date('Y-m-d H:i', strtotime('+'.$student_week['unit_value'].' weeks', strtotime($currDate)));
                            $uuid_array[] = $student_week['student_uuid'];
                        }
                        $studentWeekbookedLessionlist = BookedSession::find()->joinWith(['studentUu','tutorUu'])->where(['booked_session.student_uuid' => $uuid_array, 'student.student_uuid' => $uuid_array])->andWhere(['in', "to_char(start_datetime, 'YYYY-MM-DD HH24:MI')", $before_array])->andWhere(['booked_session.status' => 'SCHEDULE'])->asArray()->all();
                        if (!empty($studentWeekbookedLessionlist)) {
                            foreach ($studentWeekbookedLessionlist as $key => $value) {
                                $student = \app\models\User::find()->joinWith('student')->where(['user.student_uuid' => $value['student_uuid'], 'student.student_uuid' => $value['student_uuid']])->asArray()->one();
                                //$owner = General::getOwnersEmails(['OWNER', 'ADMIN', 'SUBADMIN']);

                                $content['student_first_name'] = $student['first_name'];
                                $content['student_last_name'] = $student['last_name'];
                                $content['tutor_name'] = $value['tutorUu']['first_name'].' '.$value['tutorUu']['last_name'];
                                $content['start_datetime'] = General::convertTimezone($value['start_datetime'], Yii::$app->params['timezone'], $student['timezone'], 'Y-m-d H:i:s T');
                                $content['session_pin'] = $value['session_pin'];
                                $content['uuid'] = $value['uuid'];

                                
                                Yii::$app->mailer->compose('booked_session_start_notification_student_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                                    ->setFrom(Yii::$app->params['supportEmail'])
                                    ->setTo($student['email'])
                                    //->setBcc($owner)
                                    ->setSubject("Musiconn Reminder: A Scheduled Lesson is due to Start.")
                                    ->send();
                            }
                        }
                    }
                }
            }
        }
    }

    //This cron execute when tutor session start before minutes for each lessons
    public function actionTutorNotificationMinute() {
        $currDate = date("Y-m-d H:i:s");
        $tutor_before_minute_old = TutorNotification::find()->where(['time_unit' => 'MINUTES'])->asArray()->all();
        if (!empty($tutor_before_minute_old)) {
            foreach ($tutor_before_minute_old as $key => $tutor_minute_value) {
                $tutor_before_minute_time_array[] = date('Y-m-d H:i', strtotime('+'.$tutor_minute_value['unit_value'].' minutes', strtotime($currDate)));
                $tutor_uuid_array[] = $tutor_minute_value['tutor_uuid'];
            }
            $tutorMinuteBookedSessionlist = BookedSession::find()->joinWith(['tutorUu'])->where(['booked_session.tutor_uuid' => $tutor_uuid_array, 'tutor.uuid' => $tutor_uuid_array])->andWhere(['in', "to_char(start_datetime, 'YYYY-MM-DD HH24:MI')", $tutor_before_minute_time_array])->andWhere(['booked_session.status' => 'SCHEDULE'])->asArray()->all();
            if (!empty($tutorMinuteBookedSessionlist)) {
                foreach ($tutorMinuteBookedSessionlist as $key => $result) {
                    $tutor_before_minute = TutorNotification::find()->where(['time_unit' => 'MINUTES', 'tutor_uuid' => $result['tutor_uuid']])->asArray()->all();
                    if (!empty($tutor_before_minute)) {
                        foreach ($tutor_before_minute as $key => $tutor_minute) {
                            $before_array[] = date('Y-m-d H:i', strtotime('+'.$tutor_minute['unit_value'].' minutes', strtotime($currDate)));
                            $uuid_array[] = $tutor_minute['tutor_uuid'];
                        }
                        $tutorMinutebookedLessionlist = BookedSession::find()->joinWith(['studentUu','tutorUu'])->where(['booked_session.tutor_uuid' => $uuid_array, 'tutor.uuid' => $uuid_array])->andWhere(['in', "to_char(start_datetime, 'YYYY-MM-DD HH24:MI')", $before_array])->andWhere(['booked_session.status' => 'SCHEDULE'])->asArray()->all();
                        if (!empty($tutorMinutebookedLessionlist)) {
                            foreach ($tutorMinutebookedLessionlist as $key => $value) {
                                $tutor = \app\models\User::find()->joinWith('tutor')->where(['user.tutor_uuid' => $value['tutor_uuid'], 'tutor.uuid' => $value['tutor_uuid']])->asArray()->one();
                                //$owner = General::getOwnersEmails(['OWNER', 'ADMIN', 'SUBADMIN']);

                                $content['tutor_first_name'] = $tutor['first_name'];
                                $content['tutor_last_name'] = $tutor['last_name'];
                                $content['student_name'] = $value['studentUu']['first_name'].' '.$value['studentUu']['last_name'];
                                $content['start_datetime'] = General::convertTimezone($value['start_datetime'], Yii::$app->params['timezone'], $tutor['timezone'], 'Y-m-d H:i:s T');
                                $content['session_pin'] = $value['session_pin'];
                                $content['uuid'] = $value['uuid'];

                                
                                Yii::$app->mailer->compose('booked_session_start_notification_tutor_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                                    ->setFrom(Yii::$app->params['supportEmail'])
                                    ->setTo($tutor['email'])
                                    //->setBcc($owner)
                                    ->setSubject("Musiconn Reminder: A Scheduled Lesson is due to Start")
                                    ->send();
                            }
                        }
                    }
                }
            }
        }
    }

    //This cron execute when tutor session start before hours for each lessons
    public function actionTutorNotificationHour() {
        $currDate = date("Y-m-d H:i:s");
        $tutor_before_hour_old = TutorNotification::find()->where(['time_unit' => 'HOURS'])->asArray()->all();
        if (!empty($tutor_before_hour_old)) {
            foreach ($tutor_before_hour_old as $key => $tutor_hour_value) {
                $tutor_before_hour_time_array[] = date('Y-m-d H:i', strtotime('+'.$tutor_hour_value['unit_value'].' hours', strtotime($currDate)));
                $tutor_uuid_array[] = $tutor_hour_value['tutor_uuid'];
            }
            $tutorHourBookedSessionlist = BookedSession::find()->joinWith(['tutorUu'])->where(['booked_session.tutor_uuid' => $tutor_uuid_array, 'tutor.uuid' => $tutor_uuid_array])->andWhere(['in', "to_char(start_datetime, 'YYYY-MM-DD HH24:MI')", $tutor_before_hour_time_array])->andWhere(['booked_session.status' => 'SCHEDULE'])->asArray()->all();
            if (!empty($tutorHourBookedSessionlist)) {
                foreach ($tutorHourBookedSessionlist as $key => $result) {
                    $tutor_before_hour = TutorNotification::find()->where(['time_unit' => 'HOURS', 'tutor_uuid' => $result['tutor_uuid']])->asArray()->all();
                    if (!empty($tutor_before_hour)) {
                        foreach ($tutor_before_hour as $key => $tutor_hour) {
                            $before_array[] = date('Y-m-d H:i', strtotime('+'.$tutor_hour['unit_value'].' hours', strtotime($currDate)));
                            $uuid_array[] = $tutor_hour['tutor_uuid'];
                        }
                        $tutorHourbookedLessionlist = BookedSession::find()->joinWith(['studentUu','tutorUu'])->where(['booked_session.tutor_uuid' => $uuid_array, 'tutor.uuid' => $uuid_array])->andWhere(['in', "to_char(start_datetime, 'YYYY-MM-DD HH24:MI')", $before_array])->andWhere(['booked_session.status' => 'SCHEDULE'])->asArray()->all();
                        if (!empty($tutorHourbookedLessionlist)) {
                            foreach ($tutorHourbookedLessionlist as $key => $value) {
                                $tutor = \app\models\User::find()->joinWith('tutor')->where(['user.tutor_uuid' => $value['tutor_uuid'], 'tutor.uuid' => $value['tutor_uuid']])->asArray()->one();
                                //$owner = General::getOwnersEmails(['OWNER', 'ADMIN', 'SUBADMIN']);

                                $content['tutor_first_name'] = $tutor['first_name'];
                                $content['tutor_last_name'] = $tutor['last_name'];
                                $content['student_name'] = $value['studentUu']['first_name'].' '.$value['studentUu']['last_name'];
                                $content['start_datetime'] = General::convertTimezone($value['start_datetime'], Yii::$app->params['timezone'], $tutor['timezone'], 'Y-m-d H:i:s T');
                                $content['session_pin'] = $value['session_pin'];
                                $content['uuid'] = $value['uuid'];

                                
                                Yii::$app->mailer->compose('booked_session_start_notification_tutor_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                                    ->setFrom(Yii::$app->params['supportEmail'])
                                    ->setTo($tutor['email'])
                                    //->setBcc($owner)
                                    ->setSubject("Musiconn Reminder: A Scheduled Lesson is due to Start")
                                    ->send();
                            }
                        }
                    }
                }
            }
        }
    }

    //This cron execute when tutor session start before days for each lessons
    public function actionTutorNotificationDay() {
        $currDate = date("Y-m-d H:i:s");
        $tutor_before_day_old = TutorNotification::find()->where(['time_unit' => 'DAYS'])->asArray()->all();
        if (!empty($tutor_before_day_old)) {
            foreach ($tutor_before_day_old as $key => $tutor_day_value) {
                $tutor_before_day_time_array[] = date('Y-m-d H:i', strtotime('+'.$tutor_day_value['unit_value'].' days', strtotime($currDate)));
                $tutor_uuid_array[] = $tutor_day_value['tutor_uuid'];
            }
            $tutorDayBookedSessionlist = BookedSession::find()->joinWith(['tutorUu'])->where(['booked_session.tutor_uuid' => $tutor_uuid_array, 'tutor.uuid' => $tutor_uuid_array])->andWhere(['in', "to_char(start_datetime, 'YYYY-MM-DD HH24:MI')", $tutor_before_day_time_array])->andWhere(['booked_session.status' => 'SCHEDULE'])->asArray()->all();
            if (!empty($tutorDayBookedSessionlist)) {
                foreach ($tutorDayBookedSessionlist as $key => $result) {
                    $tutor_before_day = TutorNotification::find()->where(['time_unit' => 'DAYS', 'tutor_uuid' => $result['tutor_uuid']])->asArray()->all();
                    if (!empty($tutor_before_day)) {
                        foreach ($tutor_before_day as $key => $tutor_day) {
                            $before_array[] = date('Y-m-d H:i', strtotime('+'.$tutor_day['unit_value'].' days', strtotime($currDate)));
                            $uuid_array[] = $tutor_day['tutor_uuid'];
                        }
                        $tutorDaybookedLessionlist = BookedSession::find()->joinWith(['studentUu','tutorUu'])->where(['booked_session.tutor_uuid' => $uuid_array, 'tutor.uuid' => $uuid_array])->andWhere(['in', "to_char(start_datetime, 'YYYY-MM-DD HH24:MI')", $before_array])->andWhere(['booked_session.status' => 'SCHEDULE'])->asArray()->all();
                        if (!empty($tutorDaybookedLessionlist)) {
                            foreach ($tutorDaybookedLessionlist as $key => $value) {
                                $tutor = \app\models\User::find()->joinWith('tutor')->where(['user.tutor_uuid' => $value['tutor_uuid'], 'tutor.uuid' => $value['tutor_uuid']])->asArray()->one();
                                //$owner = General::getOwnersEmails(['OWNER', 'ADMIN', 'SUBADMIN']);

                                $content['tutor_first_name'] = $tutor['first_name'];
                                $content['tutor_last_name'] = $tutor['last_name'];
                                $content['student_name'] = $value['studentUu']['first_name'].' '.$value['studentUu']['last_name'];
                                $content['start_datetime'] = General::convertTimezone($value['start_datetime'], Yii::$app->params['timezone'], $tutor['timezone'], 'Y-m-d H:i:s T');
                                $content['session_pin'] = $value['session_pin'];
                                $content['uuid'] = $value['uuid'];

                                
                                Yii::$app->mailer->compose('booked_session_start_notification_tutor_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                                    ->setFrom(Yii::$app->params['supportEmail'])
                                    ->setTo($tutor['email'])
                                    //->setBcc($owner)
                                    ->setSubject("Musiconn Reminder: A Scheduled Lesson is due to Start")
                                    ->send();
                            }
                        }
                    }
                }
            }
        }
    }

    // //This cron execute when tutor session start before weeks for each lessons
    public function actionTutorNotificationWeek() {
        $currDate = date("Y-m-d H:i:s");
        $tutor_before_week_old = TutorNotification::find()->where(['time_unit' => 'WEEKS'])->asArray()->all();
        if (!empty($tutor_before_week_old)) {
            foreach ($tutor_before_week_old as $key => $tutor_week_value) {
                $tutor_before_week_time_array[] = date('Y-m-d H:i', strtotime('+'.$tutor_week_value['unit_value'].' weeks', strtotime($currDate)));
                $tutor_uuid_array[] = $tutor_week_value['tutor_uuid'];
            }
            $tutorWeekBookedSessionlist = BookedSession::find()->joinWith(['tutorUu'])->where(['booked_session.tutor_uuid' => $tutor_uuid_array, 'tutor.uuid' => $tutor_uuid_array])->andWhere(['in', "to_char(start_datetime, 'YYYY-MM-DD HH24:MI')", $tutor_before_week_time_array])->andWhere(['booked_session.status' => 'SCHEDULE'])->asArray()->all();
            if (!empty($tutorWeekBookedSessionlist)) {
                foreach ($tutorWeekBookedSessionlist as $key => $result) {
                    $tutor_before_week = TutorNotification::find()->where(['time_unit' => 'WEEKS', 'tutor_uuid' => $result['tutor_uuid']])->asArray()->all();
                    if (!empty($tutor_before_week)) {
                        foreach ($tutor_before_week as $key => $tutor_week) {
                            $before_array[] = date('Y-m-d H:i', strtotime('+'.$tutor_week['unit_value'].' weeks', strtotime($currDate)));
                            $uuid_array[] = $tutor_week['tutor_uuid'];
                        }
                        $tutorWeekbookedLessionlist = BookedSession::find()->joinWith(['studentUu','tutorUu'])->where(['booked_session.tutor_uuid' => $uuid_array, 'tutor.uuid' => $uuid_array])->andWhere(['in', "to_char(start_datetime, 'YYYY-MM-DD HH24:MI')", $before_array])->andWhere(['booked_session.status' => 'SCHEDULE'])->asArray()->all();
                        if (!empty($tutorWeekbookedLessionlist)) {
                            foreach ($tutorWeekbookedLessionlist as $key => $value) {
                                $tutor = \app\models\User::find()->joinWith('tutor')->where(['user.tutor_uuid' => $value['tutor_uuid'], 'tutor.uuid' => $value['tutor_uuid']])->asArray()->one();
                                //$owner = General::getOwnersEmails(['OWNER', 'ADMIN', 'SUBADMIN']);

                                $content['tutor_first_name'] = $tutor['first_name'];
                                $content['tutor_last_name'] = $tutor['last_name'];
                                $content['student_name'] = $value['studentUu']['first_name'].' '.$value['studentUu']['last_name'];
                                $content['start_datetime'] = General::convertTimezone($value['start_datetime'], Yii::$app->params['timezone'], $tutor['timezone'], 'Y-m-d H:i:s T');
                                $content['session_pin'] = $value['session_pin'];
                                $content['uuid'] = $value['uuid'];

                                
                                Yii::$app->mailer->compose('booked_session_start_notification_tutor_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                                    ->setFrom(Yii::$app->params['supportEmail'])
                                    ->setTo($tutor['email'])
                                    //->setBcc($owner)
                                    ->setSubject("Musiconn Reminder: A Scheduled Lesson is due to Start")
                                    ->send();
                            }
                        }
                    }
                }
            }
        }
    }
    
    public function actionTestMail() {
        exit;
        $content['student_first_name'] = 'Alpesh';
        $content['student_last_name'] = 'Patel';
        $content['start_datetime'] = date('Y-m-d H:i:s T');
        $content['session_pin'] = '0000';
        


        $mail = Yii::$app->mailer->compose('test', ['content' => $content], ['htmlLayout' => 'layouts/html'])
            ->setFrom(Yii::$app->params['supportEmail'])
            ->setTo('alpesh@vindaloovoip.com')
            //->setBcc($owner)
            ->setSubject("Lesson Start Notification -" . Yii::$app->name)
            ->send();
        if($mail){
            echo "sent";
        }else{
            echo "fail";
        }
    }
    
    //Booked lesson package expired mail notification before 1 week - cron run every day 1 time
    public function actionBookedPackageExpire() {

        $currDate = date("Y-m-d H:i:s");
        
        $start_date_before_day = date('Y-m-d', strtotime('+7 days', strtotime($currDate)));
        $before_day_array = [$start_date_before_day];
        $bookedSessionlist = BookedSession::find()
                        ->joinWith(['studentUu','tutorUu'])
                        ->where(['in', "to_char(booked_session.start_datetime, 'YYYY-MM-DD')", $before_day_array])
                        //->andwhere(['booked_session.booking_uuid' => 'e3ef9ecb-3cd8-44b3-8905-00ec41763d0b'])
                        ->andwhere(['booked_session.status' => 'SCHEDULE'])
                        ->orderBy('booked_session.start_datetime desc')
                        ->asArray()->all();

        $result = self::getBookedPackageLastLesson($bookedSessionlist);
        if (!empty($result)) {
            foreach ($result as $key => $value) {
                
                $content['student_first_name'] = $value['studentUu']['first_name'];
                $content['tutor_first_name'] = $value['tutorUu']['first_name'];
                $content['tutor_last_name'] = $value['tutorUu']['first_name'];
                
                //echo $value['studentUu']['email']."\n<br>";
                Yii::$app->mailer->compose('booked_package_expire_notification_student_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                        ->setFrom(Yii::$app->params['supportEmail'])
                        ->setTo($value['studentUu']['email'])
                        ->setSubject("Musiconnn Notification:  Your purchased package will finish soon!")
                        ->send();
            }
        }
    }

    private function getBookedPackageLastLesson($bookedSessionlist=[]) {
         $result = $return = array();
        foreach ($bookedSessionlist as $val) {
            if (array_key_exists('booking_uuid', $val)) {
                $result[$val['booking_uuid']][] = $val;
            } else {
                $result[""][] = $val;
            }
        }
        
        if(!empty($result)){
            foreach ($result as $key => $value) {
                if(!empty($value[0])) {
                    $return[] = $value[0];
                }
            }
        }
        return $return;
    }

}
