<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\libraries\General;

/* @var $this yii\web\View */
/* @var $model app\models\Admin */

$this->title = Yii::t('app', 'Profile');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$timezone = General::getAllTimeZones();
if($userModel->timezone == '') {
 $userModel->timezone = Yii::$app->params['timezone'];
}

$this->registerJs('
    function refreshCPForm(){
        $.ajax({
            type: "GET",
            url:  "' . Url::to(['tutor/change-password']) . '",
            success:function(result){
                $("#changepassword_div").html(result);
            },
            error:function(e){
                showErrorMessage(e.responseText);
            }
        });
    }
    $(document).on("click","#password-change-btn",function(){
        $.ajax({
            type: "POST",
            url:  "' . Url::to(['tutor/change-password']) . '",
            data: $("#form-change").serialize(),
            success:function(result){
                if(result.code == 200){
                    showSuccess(result.message);
                    refreshCPForm();
                }else{
                    $("#changepassword_div").html(result);
                }
            },
            error:function(e){
                showErrorMessage(e.responseText);
            }
        });
    })
    $(".birthdate").datepicker({
        minViewMode:  0,
        format: "yyyy/mm/dd",
        /* startDate: getValue($this, "startDate", ""),
        endDate: getValue($this, "endDate", ""),*/
        /* daysOfWeekDisabled: getValue($this, "disabledDays", ""),*/
        startView: 2,
        autoclose:true
    });
    $(".profile_edit").click(function() {
        $("input[id=\'tutor-profile_image\']").click();
    });
    $("input[type=file]").on("change", prepareUpload);

    // Grab the files and set them to our variable
    function prepareUpload(event)
    {

        var pro_image = $("#tutor-profile_image").prop("files")[0];
        var formData = new FormData();
        formData.append("Tutor[profile_image]", pro_image);
        //formData.append("Tutor[first_name]", "Alpesh");

        if (typeof(pro_image) != "undefined"){
            $.ajax({
                url: "' . Url::to(['tutor/profile-image']) . '",
                type: "POST",
                data: formData,
                cache: false,
                dataType: "json",
                processData: false,
                contentType: false,
                success: function(result)
                {
                    if(result.code == 200){
                        document.getElementById("refresProfile").innerHTML = result.image_profile;
                        document.getElementById("profileImage").innerHTML = result.sidebar_profile;
                        $("#menuImage").attr("src", result.navbar_src);
                                 // alert("dasdsad");
                                 showSuccess(result.message);
                                }else{
                            showErrorMessage(result.message);
                        }
                },
                error: function(jqXHR, textStatus, errorThrown)
                {

                }
            });

        }
    }

    $("#tutor-instrumentlist").multiSelect({
        selectableHeader: "<input type=\'text\' class=\'form-control search-input\' autocomplete=\'off\' placeholder=\'search...\'>",
        selectionHeader: "<input type=\'text\' class=\'form-control search-input\' autocomplete=\'off\' placeholder=\'search...\'>",
        afterInit: function(ms) {
            var that = this,
                $selectableSearch = that.$selectableUl.prev(),
                $selectionSearch = that.$selectionUl.prev(),
                selectableSearchString = "#" + that.$container.attr("id") + " .ms-elem-selectable:not(.ms-selected)",
                selectionSearchString = "#" + that.$container.attr("id") + " .ms-elem-selection.ms-selected";

            that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                .on("keydown", function(e) {
                if (e.which === 40) {
                    that.$selectableUl.focus();
                    return false;
                }
            });

            that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                .on("keydown", function(e) {
                if (e.which == 40) {
                    that.$selectionUl.focus();
                    return false;
                }
            });
        },
        afterSelect: function() {
            this.qs1.cache();
            this.qs2.cache();
        },
        afterDeselect: function() {
            this.qs1.cache();
            this.qs2.cache();
        }
    });



    $("#tutor-country").change(function(){
        if($(this).val() == "other"){
            $(".state_dd").hide();
            $(".state_text").show();
        }else{
            $(".state_text").hide();
            $(".state_dd").show();
        }
    });

    function refreshTutorNotificationForm(){
        $.ajax({
            type: "GET",
            url:  "' . Url::to(['tutor-notification/index']) . '",
            success:function(result){
                $("#tutor_notification_div").html(result);
                $(".tutor_uuid").val("'. Yii::$app->user->identity->tutor_uuid .'");
            },
            error:function(e){
                showErrorMessage(e.responseText);
            }
        });
    }

    //Get Tutor Notification
    $("#tutorNotification").click(function() {
        refreshTutorNotificationForm();
    });

    //Add Tutor Notification
    $(document).on("click", "#TutorAddNotification", function() {
        $.ajax({
            type: "POST",
            url:  "' . Url::to(['tutor-notification/index']) . '",
            beforeSend: function() { blockBody(); },
            data: $("#dynamic-form").serialize(),
            success:function(result){
                unblockBody();
                if(result.code == 200) {
                    showSuccess(result.message);
                    refreshTutorNotificationForm();
                } else {
                    $.each(result.message, function( key, val ){
                      $("#"+key).next().html(val);
                      $("#"+key).parent().addClass("has-error");
                    });
                }
            },
            error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
    });
');
?>

<!--?= Html::errorSummary($model)?-->
<!--?= print_r($model->getErrors());?-->
<div class="col-lg-12">
    <div class="row">
        <div class="col-md-3 col-sm-4 col-xs-12 ">
            <section class="box">
                <div class="content-body">
                    <div class="uprofile-image ">
                        <!--<img src="/data/profile/profile.png" class="img-fluid user-profile-status-active">-->
                        <div class="music_genre" style="display: inline-block;">
                            <div class="thumb">
                                <span id='refresProfile'>
                                <!--<img class="img-fluid user-profile-status-active" src="/data/profile/profile.png">-->
                                    <?php
                                    ob_start();
                                    fpassthru($model->profile_image);
                                    $contents = ob_get_contents();
                                    ob_end_clean();
                                    //print_r($contents);
                                    $dataUri = "data:image/jpg;base64," . base64_encode($contents);
                                    echo "<img src='$dataUri' class='img-fluid user-profile-status-active'/>";
                                    ?>
                                </span>
                                <div class="overlay profile_edit" style="border-radius:50%"><a href="javascript:;"><i class="fa fa-edit"></i></a></div>
                                <input type="file" name="Totur[profile_image]" id="tutor-profile_image" style="display: none;" accept="image/*"/>
                            </div>
                        </div>
                    </div>
                    <div class="uprofile-name">
                        <h3>
                            <a href="#"><?= $model->first_name . ' ' . $model->last_name; ?></a>
                            <!-- Available statuses: online, idle, busy, away and offline -->
                            <span class="uprofile-status online"></span>
                        </h3>
                        <p class="uprofile-title"><?= Yii::$app->user->identity->role; ?></p>
                    <?= Html::a('Test Systems Compatibility', ['/site/check-device'], ['class'=>'btn btn-purple']) ?>

                    </div>

                    <ul class="list-group list-group-unbordered" style="font-size: 13px;">
                        <li class="list-group-item bg-gray-field" style="padding: 4px 10px;">
                            <i class="fa fa-music margin-r-5"></i> Instruments Known
                        </li>
                        <?php
                        foreach ($tutorInstrumentModel as $key => $value) {
                            echo '<li class="list-group-item" style="padding: 4px 10px;">';
                            echo $value['instrument']->name;
                            echo '</li>';
                        }
                        ?>

                    </ul>
                </div>
            </section>
            <section class="box ">
                <header class="panel_header">
                    <h2 class="title float-left">About Me</h2>
                    <div class="actions panel_actions float-right">
                        <i class="box_toggle fa fa-chevron-down"></i>
                    </div>
                </header>
                <div class="content-body">
                    <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>
                    <p class="text-muted">
                        <?= $model->street; ?><br>
                        <?= $model->city; ?><br>
                        <?= $model->state; ?> <?= $model->postal_code; ?><br>
                        <?= $model->country; ?>
                    </p>

                </div>
            </section>
        </div>

        <div class="col-md-9 col-sm-8 col-xs-12">
            <section class="box">
                <!-- Horizontal - start -->
                <div class="content-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="nav nav-tabs primary"  id="myTab1" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" role="tab" href="#profile-1" data-toggle="tab">
                                        <i class="fa fa-user"></i> Profile
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="#notification" role="tab" data-toggle="tab" id="tutorNotification">
                                        <i class="fa fa-bell-o"></i> Notification
                                    </a>
                                </li>
                                <!--<li class="nav-item">
                                    <a class="nav-link " href="#change-password" role="tab" data-toggle="tab">
                                        <i class="fa fa-lock"></i> Change Password
                                    </a>
                                </li>-->
                            </ul>

                            

                            <div class="tab-content primary" id="myTabContent1">
                                <div class="tab-pane fade show active" id="profile-1">
                                    <div>
                                        <?php $form = ActiveForm::begin(); ?>
                                        <div class="form-row">
                                            <div class="col-md-8 mb-0">
                                                <?= $form->field($userModel, 'email')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $userModel->getAttributeLabel('email')]) ?>
                                            </div>
                                            <div class="col-md-4 mb-0">
                                                <div class="form-group ">
                                                    <label class="control-label" ><?= $model->getAttributeLabel('join_date'); ?></label>
                                                    <span  class="form-control bg-gray-field" aria-required="true" aria-invalid="true"><?= \app\libraries\General::displayDate(date("Y-m-d", strtotime($model->join_date))); ?></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="col-md-6 mb-0">
                                                <strong>
                                                    <h5 class="title float-left page-title-custom">Personal Information</h5>
                                                </strong>
                                            </div>
                                        </div>

                                        <hr>

                                        <div class="form-row">
                                            <div class="col-md-6 mb-0">
                                                <?= $form->field($model, 'first_name')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('first_name')]) ?>
                                            </div>
                                            <div class="col-md-6 mb-0">
                                                <?= $form->field($model, 'last_name')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('last_name')]) ?>
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="col-md-12 mb-0">
                                                <?= $form->field($userModel, 'timezone')->dropDownList($timezone, ['class' => 'form-control myselect2', 'prompt' => 'Select Timezone', 'maxlength' => true]) ?>
                                            </div>

                                        </div>

                                        <div class="form-row">
                                            <div class="col-md-6 mb-0">
                                                <strong>
                                                    <h5 class="title float-left page-title-custom">Address</h5>
                                                </strong>
                                            </div>
                                        </div>

                                        <hr>

                                        <div class="form-row">

                                            <?php if($model->country == 'other'){
                                                $display_state_dd= 'none';
                                                $display_state_text= 'block';
                                            }else{
                                                $display_state_dd= 'block';
                                                $display_state_text= 'none';

                                            } ?>
                                            <div class="col-md-4 mb-0">
                                                <?= $form->field($model, 'country')->dropDownList(['AUS' => 'Australia','other' => 'Other'], ['class' => 'form-control', 'maxlength' => true]) ?>
                                            </div>
                                            <div class="col-md-4 mb-0 state_dd" style="display:<?= $display_state_dd;?>">
                                                <?= $form->field($model, 'state')->dropDownList(['New South Wales' => ' New South Wales', 'Queensland' => 'Queensland', 'South Australia' => 'South Australia', 'Tasmania' => 'Tasmania', 'Victoria' => 'Victoria', 'Western Australia' => 'Western Australia', 'Australian Capital Territory' => 'Australian Capital Territory', 'Northern Territory' => 'Northern Territory'], ['class' => 'form-control', 'maxlength' => true, 'prompt' => "Select " . $model->getAttributeLabel('state')]) ?>
                                            </div>
                                            <div class="col-md-4 mb-0 state_text" style="display:<?= $display_state_text;?>">
                                                <?= $form->field($model, 'state_text')->textInput(['autofocus' => false, 'class' => 'input form-control state_text', 'placeholder' => "State"]); ?>
                                            </div>


                                            <div class="col-md-4 mb-0">
                                                <?= $form->field($model, 'city')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('city')]) ?>
                                            </div>
                                        </div>

                                        <div class="form-row">
                                              <div class="col-md-4 mb-0">
                                                  <?= $form->field($model, 'street')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('street')]) ?>
                                              </div>
                                            <div class="col-md-4 mb-0">
                                                <?= $form->field($model, 'postal_code')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('postal_code')]) ?>
                                                <?php //echo $form->field($model, 'postal_code')->widget(\yii\widgets\MaskedInput::className(['plceholder'=>'']), ['mask' => '9999', 'clientOptions' => ['removeMaskOnSubmit' => true],'options' => ['placeholder' => 'xxxx', 'class' => 'form-control']]) ?>
                                            </div>
                                            <div class="col-md-4 mb-0">
                                                <!--?= $form->field($model, 'phone')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('phone')]) ?-->
                                                <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), ['mask' => '9999 999 999', 'clientOptions' => ['removeMaskOnSubmit' => true],'options' => ['placeholder' => 'xxxx xxx xxx', 'class' => 'form-control']]) ?>
                                                
                                            </div>
                                        </div>


                                        <div class="form-row">
                                            <div class="col-md-6 mb-0">
                                                <strong>
                                                    <h5 class="title float-left page-title-custom">Instruments Known</h5>
                                                </strong>
                                            </div>
                                        </div>

                                        <hr>

                                        <div class="form-row">
                                            <div class="col-md-12 mb-0">
                                                <?php
                                                $items = array();
                                                foreach ($instrumentModel as $key => $value) {
                                                    $items[$value->uuid] = $value->name;
                                                }
                                                ?>
                                                <?= $form->field($model, 'instrumentList')->dropDownList($items, ['multiple' => true])->label('Instruments'); ?>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-12 mb-0">
                                                <?= $form->field($model, 'description')->textarea(['rows' => 2]) ?>
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="col-md-6 mb-0">
                                                <strong>
                                                    <h5 class="title float-left page-title-custom">Certification Inforamtion</h5>
                                                </strong>
                                            </div>
                                        </div>

                                        <hr>

                                        <div class="form-row">
                                            <div class="col-md-6 mb-0">
                                                <?php $model->contract_expiry = \app\libraries\General::displayDate($model->contract_expiry);
                                                echo $form->field($model, 'contract_expiry')->textInput(['readonly' => true, 'class' => 'form-control custom_date_picker', 'maxlength' => true, 'placeholder' => 'DD-MM-YYYY']) ?>
                                            </div>
                                            <div class="col-md-6 mb-0">
                                                <?= $form->field($model, 'child_certi_number')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('child_certi_number')]) ?>
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="col-md-6 mb-0">
                                            <?php $model->child_certi_expiry = \app\libraries\General::displayDate($model->child_certi_expiry);
                                                echo $form->field($model, 'child_certi_expiry')->textInput(['readonly' => true, 'class' => 'form-control custom_date_picker', 'maxlength' => true, 'placeholder' => 'DD-MM-YYYY']) ?>
                                            </div>
                                            <div class="col-md-6 mb-0">
                                                <?= $form->field($model, 'abn')->widget(\yii\widgets\MaskedInput::className(), ['mask' => '99 999 999 999', 'clientOptions' => ['removeMaskOnSubmit' => true],'options' => ['placeholder' => 'xx xxx xxx xxx', 'class' => 'form-control']]) ?>
                                            </div>
                                        </div>



                                        <div class="form-row">
                                            <div class="col-md-12 mb-0">
                                                <?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-primary float-right']) ?>
                                            </div>
                                        </div>
                                        <?php ActiveForm::end(); ?>
                                    </div>
                                    <hr>
                                    <div class="form-row">
                                        <div class="col-md-12 mb-0">
                                            <ul class="nav nav-tabs primary" id="myTab1" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link " style='background-color: rgba(31, 181, 172, 1); border: 1px solid rgba(31, 181, 172, 1); color: #fafafa;' role="tab" data-toggle="tab">
                                                        <i class="fa fa-lock" style='color: #fafafa;'></i> Change Password
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div><br>
                                    <div id="changepassword_div">
                                        <?php echo Yii::$app->controller->renderPartial('change-password', ['modelCP' => $modelCP]); ?>
                                    </div>

                                </div>
                                <div class="tab-pane fade" id="notification">

                                    <div id="tutor_notification_div">
                                    
                                    </div>
                                </div>
                                <!--                                <div class="tab-pane fade" id="change-password">

                                                                    <div id="changepassword_div">
                                <?php echo Yii::$app->controller->renderPartial('change-password', ['modelCP' => $modelCP]); ?>
                                                                    </div>

                                                                </div>-->
                            </div>

                        </div>
                        <!--<br><div class="spacer"></div><div class="spacer"></div>-->
                    </div>
                    <!-- Horizontal - end -->
                </div>
            </section>

        </div>
    </div>
</div>
