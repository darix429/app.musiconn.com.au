<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\libraries\General;
?>
<div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">
    <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Student ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Birth Year</th>
                <th>Join Date</th>
                <th>Musicoins</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (!empty($studentList)) {
                foreach ($studentList as $key => $value) {
                    ?>
                    <tr>
                        <td><?= $key + 1; ?></td>
                        <td><?= $value['enrollment_id']; ?></td>
                        <td><?= $value['first_name'] . ' ' . $value['last_name']; ?></td>
                        <td><?= $value['email']; ?></td>
                        <td><?= $value['phone_number']; ?></td>
                        <td><?= $value['birthyear']; ?></td>
                        <td><?= General::displayDate(date('Y-m-d', $value['created_at'])); ?></td>
                        <td class="text-right"><?= $value['musicoin_balance']; ?></td>
                        <td align="center">
                            <?php
                            if ($value['status'] == 'ENABLED') {
                                $e_class = "badge-primary";
                                $pointer = 'style="cursor:pointer;color:#fff"';
                                $e_link_class = "student_disabled_link";
                            } elseif ($value['status'] == 'DISABLED') {
                                $e_class = "badge-secondary";
                                $pointer = 'style="cursor:pointer;color:#fff"';
                                $e_link_class = "student_enabled_link";
                            } elseif ($value['status'] == 'INACTIVE') {
                                $e_class = "badge-orange";
                                $pointer = '';
                                $e_link_class = "";
                            } else {
                                $e_class = "badge-danger";
                                $pointer = '';
                                $e_link_class = "";
                            }
                            ?>
                            <span class="badge badge-pill <?= $e_class ?> <?= $e_link_class ?>"  data-uuid="<?= $value['student_uuid']; ?>" <?= $pointer ?>><?= $value['status']; ?></span>
                        </td>
                        <td class="list-unstyled list-inline">
                            <?php if ($value['status'] == 'ENABLED') { ?>
                                <a href="<?= Url::to(['/admins/switch-to-student/', 'id' => $value['student_uuid']]) ?>" class="badge badge-orange badge-md" title="Go To Account"><i class="fa fa-exchange"></i></a>
                            <?php } ?>
                            <?php if (in_array(Yii::$app->user->identity->role, ['OWNER', 'ADMIN']) && $value['isMonthlySubscription'] && $value['isCancelMonthlySubscriptionRequest'] && $value['status'] == 'ENABLED') { ?>
                                <a href="javascript:void(0)" class="badge badge-success badge-md cancel_monthly_subscription_paln_student" data-uuid="<?= $value['student_uuid'] ?>" title="Approve Cancel Monthly Subscription"><i class="fa fa-check"></i></a>
                            <?php } ?>
                            <?php if ($value['status'] == 'INACTIVE') { ?>
                                <a href="javascript:void(0)" class="text-primary student_active_link" data-uuid="<?= $value['student_uuid'] ?>" title="Send Activation Link"><i class="fa fa-paper-plane-o icon-rounded icon-primary icon-xs"></i></a>
                                <a href="javascript:void(0)" data-uuid="<?= $value['student_uuid']; ?>" class="delete_student_link badge badge-danger badge-md" title="Delete"><i class="fa fa-trash"></i></a>
                            <?php } ?>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>

</div>
