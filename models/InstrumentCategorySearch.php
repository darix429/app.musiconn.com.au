<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\InstrumentCategory;

/**
 * InstrumentCategorySearch represents the model behind the search form of `app\models\InstrumentCategory`.
 */
class InstrumentCategorySearch extends InstrumentCategory
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'name', 'parent_id'], 'safe'],
            [['level'], 'integer'],
	    
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $modeDatatable = false)
    {
        $query = InstrumentCategory::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'level' => $this->level,
        ]);

        $query->andFilterWhere(['ilike', 'uuid', $this->uuid])
            ->andFilterWhere(['ilike', 'name', $this->name])
	    ->andFilterWhere(['ilike', 'parent_id', $this->parent_id]);

	if ($modeDatatable) {
            $result = $query->asArray()->orderBy('name')->all();
	    return $result;
        }
        return $dataProvider;
    }
}
