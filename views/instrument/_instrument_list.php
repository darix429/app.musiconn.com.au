<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<style>
    .thumb{
        width: 221px;
        height: 221px;
    }
</style>

<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="row">
        <?php
        if (!empty($instrumentList)) {

            foreach ($instrumentList as $key => $value) {
                ?>
                <div class="col-lg-3 col-sm-6 col-md-4 music_genre">
                    <div class="team-member gallery-parent-div">
                        <div class="thumb">
                            <?php
                            ob_start();
                            fpassthru($value['image']);
                            $contents = ob_get_contents();
                            ob_end_clean();
                            $dataUri = "data:image/jpg;base64," . base64_encode($contents);
                            echo "<img src='$dataUri'/>";
                            ?>
                            <div class="overlay"><a href="<?= Url::to(['/instrument/update/', 'id' => $value['uuid']]) ?>"><i class="fa fa-pencil"></i></a></div>
                        </div>
                        <div class="team-info">
                            <h4>
                                <a href="<?= Url::to(['/instrument/update/', 'id' => $value['uuid']]) ?>"><?= $value['name']; ?></a>
                            </h4>
                            <span class='team-member-edit' style="top:auto">
                                
                                <a href="<?= Url::to(['/instrument/update/', 'id' => $value['uuid']]) ?>" class="badge badge-info badge-md" title="Update"><i class="fa fa-pencil"></i></a>
                                <a href="javascript:void(0)" data-uuid="<?= $value['uuid']; ?>" class="delete_admin_link badge badge-danger badge-md" title="Delete"><i class="fa fa-trash"></i></a>
                            </span>

                            <?php
                            if ($value['status'] == 'ACTIVE') {
                                $e_class = "badge-primary";
                                $pointer = 'style="cursor:pointer;color:#fff"';
                                $e_link_class = "instrument_inactive_link";
                            } elseif ($value['status'] == 'INACTIVE') {
                                $e_class = "badge-secondary";
                                $pointer = 'style="cursor:pointer;color:#fff"';
                                $e_link_class = "instrument_active_link";
                            } else {
                                $e_class = "badge-danger";
                                $pointer = '';
                                $e_link_class = "";
                            }
                            ?>
                            <span class="badge badge-pill <?= $e_class ?> <?= $e_link_class ?>"  data-uuid="<?= $value['uuid']; ?>" <?= $pointer ?>><?= $value['status']; ?></span>
                            
                        </div>
                    </div>
                </div>
                
                <?php
            }
        } else { echo "<div class='col-lg-3 col-sm-6 col-md-4 music_genre'>No record found.</div>";}
        ?>


    </div>
</div>
