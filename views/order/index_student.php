<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;

$this->title = Yii::t('app', 'ORDER HISTORY');
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_icon'] = 'my-profiles.png';


$this->registerJs(
        '

var PAGE_CUSTOM = function () {
	if ($.isFunction($.fn.dataTable)) {
	$(".my-datatable-student-order").dataTable({
                responsive: true,
               "pageLength": dt_page_length,
                "dom": "<\"dataTables_wrapper\" <\"row\" <\"col-sm-12 col-md-4\"l><\"col-sm-12 col-md-4\"B><\"col-sm-12 col-md-4\"f>>  <\"row\" <\"col-sm-12\"t>> <\"row\" <\"col-sm-12 col-md-5\"i> <\"col-sm-12 col-md-7\"p>> >",
                buttons: [
                    "copy", {
			   extend: "csv",
			   footer: false,
			   exportOptions: {
                		columns: [0,1,2,3,4,5,6]
            		   }			  
		       }, 
		       {
			   extend: "excel",
			   footer: false,
			   exportOptions: {
                		columns: [0,1,2,3,4,5,6]
            		   }			  
		       },
			{
			   extend: "pdf",
			   footer: false,
			   exportOptions: {
                		columns: [0,1,2,3,4,5,6]
            		   }			  
		       },
			{
			   extend: "print",
			   footer: false,
			   exportOptions: {
                		columns: [0,1,2,3,4,5,6]
            		   }			  
		       }
                ],
                aLengthMenu: [
                    [10, 25, 50, 100, -1],
                    [10, 25, 50, 100, "All"]
                ]
            });
}
};
$(document).ready(function() {
        PAGE_CUSTOM();
    });
//invoice check    
$(document).on("click",".invoice_check", function(){ 
        var id = $(this).data("uuid");
        //alert(id); 
            $.ajax({
                type: "GET",
                url: "' . Url::to(['/order/invoice-check/']) . '",
                data: { "id": id},
                success:function(result){ 
                    if(result.code == 200){ 
                        //window.location.reload();
                        showSuccess(result.message);
                    }else{
                        showErrorMessage(result.message);
                    }
                },
                error:function(e){ 
                    showErrorMessage(e.responseText);
                }
            });
        
    });

$(document).on("click","#search_btn", function(){        
    $.ajax({
        type: "GET",
        url: "' . Url::to(['/order/history/']) . '",
        data: $("#search_frm").serialize(),
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            $("#coupon_table_parent_div").html(result);
            //window.NEW_CUSTOM.dataTablesInit();
	    PAGE_CUSTOM();
        },
        error:function(e){ 
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});
    
$(document).on("click","#search_reset_btn", function(){        
    $.ajax({
        type: "GET",
        url: "' . Url::to(['/order/history/']) . '",
        data: {},
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            $("#coupon_table_parent_div").html(result);
            //window.NEW_CUSTOM.dataTablesInit();
	    PAGE_CUSTOM();
        },
        error:function(e){
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});
    
    
', View::POS_END
);
?>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row center-block box-search-area">
                        <div class="col-lg-12 col-md-12 col-12 ">
                            <div class="actions panel_actions pull-right custom-toggle" data-togglediv="datatable-search-div" title="Advance Search" style="margin-bottom: 5px; cursor: pointer">
                                <span>
                                    <i class="fa fa-search " ></i>
                                    Advance Search
                                </span>
                            </div>
                            <div class="datatable-search-div " style="display: none;">
                                <?php
                                $form = ActiveForm::begin([
                                            'method' => 'get',
                                            'options' => [
                                                'id' => 'search_frm',
                                                'class' => 'form-inline'
                                            ],
                                ]);
                                ?>

                                <?= $form->field($searchModel, 'order_id')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Order ID'])->label(false) ?>
                                <?= $form->field($searchModel, 'order_datetime')->textInput(['class' => 'form-control custom_date_picker', 'maxlength' => true, 'placeholder' => 'Order Date'])->label(false) ?>
                                

                                <?= Html::Button(Yii::t('app', 'Search'), ['class' => 'btn btn-primary mb-0', 'id' => 'search_btn', 'style' => 'margin-left: 17px;']) ?>
                                <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-primary mb-0', 'id' => 'search_reset_btn']) ?>

                                <?php ActiveForm::end(); ?>

                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row" id="coupon_table_parent_div">
                        <?php echo Yii::$app->controller->renderPartial('_student_order_list', ['orderList' => $orderList]); ?>
                    </div>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <a class="btn btn-primary btn-sm black_button_in" href="<?= Yii::$app->getUrlManager()->createUrl('/student/profile'); ?>">RETURN TO PROFILE</a>

</section>
