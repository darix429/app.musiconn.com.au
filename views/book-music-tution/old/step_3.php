<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Url;
use app\models\Instrument;
use app\models\OnlineTutorialPackage;
use app\models\StudentTuitionBooking;

$this->title = Yii::t('app', 'Book Music Lessons');
$this->params['breadcrumbs'][] = $this->title;

if (!empty($step_data['step_1']['instrument_uuid'])) {
    $instrumentDetail = Instrument::find()->where(['uuid' => $step_data['step_1']['instrument_uuid']])->one();
} else {
    return \yii\web\Controller::redirect(['index']);
}

$planDetail = OnlineTutorialPackage::find()->where(['uuid' => $step_data['step_2']['onlinetutorial_uuid']])->one();

$tutorList = StudentTuitionBooking::getInstrumentWiseTutorList($step_data['step_1']['instrument_uuid']);

if(isset($step_data['step_3']['tutor_uuid']) && $step_data['step_3']['tutor_uuid'] != ""){
    $model->tutor_uuid = $step_data['step_3']['tutor_uuid'];
    $model->tutorcheckbox = $model->tutor_uuid;
}

//echo "<pre>";
//print_r($step_data);
//echo $model->tutor_uuid;
//echo "</pre>";
$this->registerJs(
        '
$(document).on("ifClicked","input[name=\"StudentTuitionBooking[tutorcheckbox]\"]",function (event) {

    $("#studenttuitionbooking-tutor_uuid").val(this.value);
    $(".tutor_select_btn").addClass("d-none");
    $(this).closest("tr.tutor_tr").find(".tutor_select_btn").removeClass("d-none");

});
$( "#frm_step_3,#gostep1,#gostep2" ).submit(function( event ) {
        blockBody();
});
        ', View::POS_END
);
?>
<div class="col-xl-12">
    <p class="bg-warning text-dark">For issues concerning tutor availability, please contact <a href="mailto:admin@musiconn.com.au">admin@muscionn.com.au</a> for assistance.</label>
    <nav class="navbar navbar-expand-lg navbar-light bg-primary text-light" style="">
        <h2 class="booking-nav-title float-left text-white" style="">Select Instrument &nbsp;<i class="fa fa-check"></i> </h2>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#change-step1" aria-controls="change-step1" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="change-step1" >
            <?php echo Html::beginForm(['index'], 'post', ['id' => 'gostep1', 'class' => 'form-inline d-block']) ?>
            <input type="hidden" name="step" value="1">
            <input type="hidden" name="prev" value="1">
            <a href="javascript:$('#gostep1').submit();" class="btn btn-outline-success pull-right">
                Change
            </a>
            <p class="pull-right"> <?= $instrumentDetail->name; ?>&nbsp;</p>
            <?php echo Html::endForm() ?>

        </div>
    </nav>
    <div class="spacer"></div>

    <nav class="navbar navbar-expand-lg navbar-light bg-primary text-light" style="">
        <h2 class="booking-nav-title float-left text-white" style="">Select Plan &nbsp;<i class="fa fa-check"></i> </h2>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#change-step2" aria-controls="change-step2" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="change-step2">
            <?php echo Html::beginForm(['index'], 'post', ['id' => 'gostep2', 'class' => 'form-inline d-block']) ?>
            <input type="hidden" name="step" value="2">
            <input type="hidden" name="prev" value="2">
            <a href="javascript:$('#gostep2').submit();" class="btn btn-outline-success pull-right">
                Change
            </a>
            <p class="pull-right"> <?= $planDetail->name." Total : $".$planDetail->total_price; ?>&nbsp;</p>
            <?php echo Html::endForm() ?>

        </div>
    </nav>

    <section class="box ">
        <header class="panel_header bg-primary ">
            <h2 class="title float-left text-white">Select Tutor</h2>
            <div class="actions panel_actions float-right">

            </div>
        </header>
        <div class="content-body padding-imp-30">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12">
                    <?php
                    $form = ActiveForm::begin([
                                'action' => ['index'],
                                'id' => 'frm_step_3',
                                'enableClientValidation' => true,
//                                'enableAjaxValidation' => true,
                                'validateOnChange' => true,
                                'options' => ['class' => 'form-horizontal']]);
                    ?>

                    <div class="col-lg-12 col-md-12 col-12 booking_search_data">
                        <div class="tab-pane fade show active " >
                            <input type="hidden" name="step" value="3">
                            <?= $form->field($model, 'tutor_uuid')->hiddenInput(['class' => 'form-control', 'maxlength' => true,])->label(false) ?>

                            <div id="tutor_list_parent">
                                <table class="table table-striped table-hover">
                                    <?php
                                    if (!empty($tutorList)) {
                                        foreach ($tutorList as $key => $value) {

                                            ob_start();
                                            fpassthru($value['profile_image']);
                                            $contents = ob_get_contents();
                                            ob_end_clean();
                                            $dataUri = "data:image/jpg;base64," . base64_encode($contents);

                                            $displayClass = ($model->tutor_uuid == $value['uuid']) ? "" : "d-none";
                                            ?>
                                            <tr class="unread tutor_tr" >
                                                <td class="" style="width: 10%">

                                                <?php
                                                echo $form->field($model, 'tutorcheckbox')->radio([
                                                    'label' => '',
                                                    'template' => '{input}{error} ',
                                                    'class' => 'skin-square-red float-left tutor_checkbox', 'value' => $value['uuid']])->label('')
                                                ?>
                                                </td>
                                                <td class=""style="width: 5%">
                                                    <span title="Availability" class="badge badge-md badge-secondary tutor_availability_calender_btn" data-id="<?= $value['uuid']?>" data-tutorname="<?= $value['first_name']. ' '.$value['last_name'] ?>"><i class="fa fa-calendar "></i></span>
                                                </td>
                                                <td class=""style="width: 10%">
                                                    <img class="" style="width: 30px;" src="<?= $dataUri; ?>">
                                                </td>
                                                <td class="open-view"><?= $value['first_name'].' '.$value['last_name']; ?></td>

                                                <td class="open-view">
                                                    <?= Html::submitButton('Select', ['class' => "btn btn-orange btn-md tutor_select_btn $displayClass"]) ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        echo "No Tutor available for this instrument.";
                                    }
                                    ?>
                                </table>
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </section>


    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Select Date</h2>
            <div class="actions panel_actions float-right">

            </div>
        </header>
    </section>
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Confirm and Payment</h2>
            <div class="actions panel_actions float-right">

            </div>
        </header>
    </section>
</div>
