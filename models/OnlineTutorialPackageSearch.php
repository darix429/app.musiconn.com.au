<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\OnlineTutorialPackage;

/**
 * OnlineTutorialPackageSearch represents the model behind the search form of `app\models\OnlineTutorialPackage`.
 */
class OnlineTutorialPackageSearch extends OnlineTutorialPackage {

    /**
     * {@inheritdoc}
     */
    public $online_tutorial_package_type_uuid;

    public function rules() {
        return [
            [['uuid', 'name', 'description', 'tutorial_type_uuid', 'payment_term_uuid', 'gst_description', 'status', 'created_at', 'updated_at','online_tutorial_package_type_uuid'], 'safe'],
            [['price', 'gst', 'total_price'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $modeDatatable = false, $statusnot = '') {
        $query = new \yii\db\Query;
      
             $query->select(['o.*','p.uuid','p.name','p.price','p.gst','p.total_price','p.status','p.default_option','p.online_tutorial_package_type_uuid']
                    )
                    ->from('online_tutorial_package as o')
                    ->join('RIGHT JOIN', 'premium_standard_plan_rate as p', 'o.uuid = p.online_tutorial_package_uuid');
            
        


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            //'price' => $this->price,
            //'gst' => $this->gst,
            //'total_price' => $this->total_price,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'p.uuid', $this->uuid])
                ->andFilterWhere(['ilike', 'p.name', $this->name])
                ->andFilterWhere(['ilike', 'CAST(p.price AS TEXT)', $this->price])
                ->andFilterWhere(['ilike', 'CAST(p.gst AS TEXT)', $this->gst])
                ->andFilterWhere(['ilike', 'CAST(p.total_price AS TEXT)', $this->total_price])
                ->andFilterWhere(['ilike', 'p.description', $this->description])
                ->andFilterWhere(['ilike', 'p.tutorial_type_uuid', $this->tutorial_type_uuid])
                ->andFilterWhere(['ilike', 'p.payment_term_uuid', $this->payment_term_uuid])
                ->andFilterWhere(['ilike', 'p.gst_description', $this->gst_description])
                ->andFilterWhere(['ilike', 'p.status', $this->status])
                ->andFilterWhere(['=', 'p.online_tutorial_package_type_uuid', $this->online_tutorial_package_type_uuid]);

        if ($modeDatatable) {
            $result = $query->orderBy('o.name')->all();
            return $result;
        }
        return $dataProvider;
    }

}
