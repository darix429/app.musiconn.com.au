<?php

namespace app\controllers;

use Yii;
use app\models\StudentMonthlySubscription;
use app\models\StudentMonthlySubscriptionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\MonthlySubscriptionFee;
use app\models\StudentSubscription;
use yii\web\Session;
use app\models\StudentCreditCard;
use yii\filters\AccessControl;
use app\libraries\General;

/**
 * StudentMonthlySubscriptionController implements the CRUD actions for StudentMonthlySubscription model.
 */
class MonthlySubscriptionController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => Yii::$app->user->can('/monthly-subscription/index'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => Yii::$app->user->can('/monthly-subscription/delete'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['cancel'],
                        'allow' => Yii::$app->user->can('/monthly-subscription/cancel'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['history'],
                        'allow' => Yii::$app->user->can('/monthly-subscription/history'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['report'],
                        'allow' => Yii::$app->user->can('/monthly-subscription/report'),
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex() {

        $model = new StudentMonthlySubscription();

        $subscriptionFeeList = StudentMonthlySubscription::getsubscriptionFeeList(Yii::$app->user->identity->student_uuid);
        $step_data = StudentMonthlySubscription::getSession("MS_" . Yii::$app->user->identity->student_uuid);

        //student
        $student_details = \app\models\Student::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid, 'status' => 'ENABLED'])->one();
        
        $this->layout = "student_layout";
        
        if ($student_details->isMonthlySubscription == '0') {
            
            if (!empty($step_data['step_1']['monthlysubscriptionfee_uuid'])) {
                $monthlySubscriptionFeeDetail = MonthlySubscriptionFee::find()->where(['uuid' => $step_data['step_1']['monthlysubscriptionfee_uuid']])->one();
                $monthlySubscriptionFeeDetail->price = $subscriptionFeeList->price;
                $monthlySubscriptionFeeDetail->tax = $subscriptionFeeList->tax;
                $monthlySubscriptionFeeDetail->total_price = $subscriptionFeeList->total_price;
                Yii::$app->session->set('monthlySubscriptionFee', $monthlySubscriptionFeeDetail);
            }

            if (Yii::$app->getRequest()->post('step') == '1') {

                $model->scenario = 'step_1';

                if ($model->load(Yii::$app->request->post()) && $model->validate()) {     //first check monthly subscription

                    $modelcard = new StudentCreditCard();
                    StudentMonthlySubscription::setSession("MS_" . Yii::$app->user->identity->student_uuid, 'step_1', Yii::$app->getRequest()->post());
                    $step_data = StudentMonthlySubscription::getSession("MS_" . Yii::$app->user->identity->student_uuid);

                    $creditCard = StudentCreditCard::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid, 'status' => 'ENABLED'])->asArray()->orderBy('updated_at DESC')->one();
                    Yii::$app->session->set('creditCard', $creditCard);

                    if (!empty($creditCard)) {
                        return $this->render('step_2_else', [
                                    'model' => $modelcard,
                                    'studentMonthlysubDetail' => $subscriptionFeeList,
                                    'creditCard' => $creditCard,
                                    'step_data' => $step_data,
                                    'student_details' => $student_details,
                        ]);
                    } else {
                        return $this->render('step_2', [
                                    'model' => $modelcard,
                                    'subscriptionFeeList' => $subscriptionFeeList,
                                    'step_data' => $step_data,
                                    'student_details' => $student_details,
                        ]);
                    }
                }

                return $this->render('step_1', [
                            'model' => $model,
                            'subscriptionFeeList' => $subscriptionFeeList,
                            'step_data' => $step_data,
                            'student_details' => $student_details,
                ]);
            } elseif (Yii::$app->getRequest()->post('step') == '2' && Yii::$app->getRequest()->post('remove_card') == 'yes') {    //remove credit card
                $modelcard = new StudentCreditCard();

                return $this->render('step_2', [
                            'model' => $modelcard,
                            'step_data' => $step_data,
                            'student_details' => $student_details,
                ]);
                
            } elseif (Yii::$app->getRequest()->post('step') == '2') {   //add credit card details

                $modelcard = new StudentCreditCard();
                $modelcard->scenario = 'step_term_condition';
                $model->scenario = 'step_2';

                StudentMonthlySubscription::setSession("MS_" . Yii::$app->user->identity->student_uuid, 'step_2', Yii::$app->getRequest()->post());
                $step_data = StudentMonthlySubscription::getSession("MS_" . Yii::$app->user->identity->student_uuid);

                if ($modelcard->load(Yii::$app->request->post()) && $modelcard->validate()) {

                    //session
                    $monthlySubscriptionFee = Yii::$app->session->get('monthlySubscriptionFee');
                    $selectCreditCard = Yii::$app->getRequest()->post();
                    
                    if (!empty($selectCreditCard)) {
                        $payment_response = StudentMonthlySubscription::SuccessPayment($monthlySubscriptionFee, $selectCreditCard);

                        if (isset($payment_response['code']) && $payment_response['code'] == 200) {

                            Yii::$app->getSession()->setFlash('success', 'You have buy  monthly subscription successfully.');
                        } else {

                            $message = (isset($payment_response['message'])) ? $payment_response['message'] : 'Sorry, something went wrong.';
                            Yii::$app->getSession()->setFlash('danger', $message);
                        }
                        return $this->redirect(['monthly-subscription/index']);
                    }
                } else {
                    return $this->render('step_2', [
                                'model' => $modelcard,
                                'step_data' => $step_data,
                                'student_details' => $student_details,
                    ]);
                }
                
            } elseif (Yii::$app->getRequest()->post('step_else') == '2') {    //select credit card only cvv add
                
                $model->scenario = 'step_2_else';
                $modelcard = new StudentCreditCard();
                $modelcard->scenario = 'step_term_condition_cvv';
                
                StudentMonthlySubscription::setSession("MS_" . Yii::$app->user->identity->student_uuid, 'step_2_else', Yii::$app->getRequest()->post());
                $step_data = StudentMonthlySubscription::getSession("MS_" . Yii::$app->user->identity->student_uuid);
                
                if ($modelcard->load(Yii::$app->request->post()) && $modelcard->validate()) {  

                    $monthlySubscriptionFee = Yii::$app->session->get('monthlySubscriptionFee');
                    $selectCreditCard = Yii::$app->getRequest()->post();

                    if (!empty($selectCreditCard)) {
                        $payment_response = StudentMonthlySubscription::SuccessPayment($monthlySubscriptionFee, $selectCreditCard);

                        if (isset($payment_response['code']) && $payment_response['code'] == 200) {

                            Yii::$app->getSession()->setFlash('success', 'You have buy  monthly subscription successfully.');
                        } else {

                            $message = (isset($payment_response['message'])) ? $payment_response['message'] : 'Sorry, something went wrong.';
                            Yii::$app->getSession()->setFlash('danger', $message);
                        }
                        return $this->redirect(['monthly-subscription/index']);
                    }
                }

                $creditCard = Yii::$app->session->get('creditCard');
                return $this->render('step_2_else', [
                            'model' => $modelcard,
                            'studentMonthlysubDetail' => $subscriptionFeeList,
                            'creditCard' => $creditCard,
                            'step_data' => $step_data,
                            'student_details' => $student_details,
                ]);
            } else {
                $model->scenario = 'step_1';
                return $this->render('step_1', [
                            'model' => $model,
                            'subscriptionFeeList' => $subscriptionFeeList,
                            'step_data' => $step_data,
                            'student_details' => $student_details,
                ]);
            }
        } else {
            
            $studentSubDetail = StudentSubscription::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid])->one();
            $studentMonthlySubDetail = StudentMonthlySubscription::find()->where(['uuid' => $studentSubDetail->subscription_uuid])->one();
            $step_data = StudentMonthlySubscription::getSession("MS_" . Yii::$app->user->identity->student_uuid);

            //check student monthly sub
            if (!empty($step_data['step_1_else']['studentmonthlysub_uuid'])) {
                $studentMonthlySubDetail_session = StudentMonthlySubscription::find()->where(['uuid' => $step_data['step_1_else']['studentmonthlysub_uuid']])->one();
                $studentMonthlySubDetail_session->price = $subscriptionFeeList->price;
                $studentMonthlySubDetail_session->tax = $subscriptionFeeList->tax;
                $studentMonthlySubDetail_session->total_price = $subscriptionFeeList->total_price;
                $studentMonthlySubDetail_session->name = $subscriptionFeeList->name;
                $studentMonthlySubDetail_session->with_tutorial = $subscriptionFeeList->with_tutorial;
                $studentMonthlySubDetail_session->description = $subscriptionFeeList->description;
                Yii::$app->session->set('studentMonthlySub', $studentMonthlySubDetail_session);
            }

            if (Yii::$app->getRequest()->post('step_else') == '1') {

                $model->scenario = 'step_1_else';
                //first step
                if (($model->load(Yii::$app->request->post()) && $model->validate()) || (!empty(Yii::$app->getRequest()->post('studentmonthlysub_uuid')))) {
                    $modelcard = new StudentCreditCard();
                    StudentMonthlySubscription::setSession("MS_" . Yii::$app->user->identity->student_uuid, 'step_1_else', Yii::$app->getRequest()->post());
                    $step_data = StudentMonthlySubscription::getSession("MS_" . Yii::$app->user->identity->student_uuid);
                    
                    $creditCard = StudentCreditCard::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid, 'status' => 'ENABLED'])->asArray()->orderBy('updated_at DESC')->one();
                    Yii::$app->session->set('creditCard', $creditCard);

                    if (!empty($creditCard)) { //DONE
                        return $this->render('step_2_else', [
                                    'model' => $modelcard,
                                    'studentMonthlysubDetail' => $studentMonthlySubDetail,
                                    'creditCard' => $creditCard,
                                    'step_data' => $step_data,
                                    'student_details' => $student_details,
                        ]);
                    } else { //DONE
                        return $this->render('step_2', [
                                    'model' => $modelcard,
                                    'subscriptionFeeList' => $studentMonthlySubDetail,
                                    'step_data' => $step_data,
                                    'student_details' => $student_details,
                        ]);
                    }
                }

                return $this->render('step_1_else', [
                            'model' => $model,
                            'studentMonthlysubDetail' => $studentMonthlySubDetail,
                            //'step_data' => $step_data,
                            'student_details' => $student_details,
                ]);
                
            } elseif (Yii::$app->getRequest()->post('step') == '2' && Yii::$app->getRequest()->post('remove_card') == 'yes') {    //remove credit card
                
                $modelcard = new StudentCreditCard();

                return $this->render('step_2', [
                            'model' => $modelcard,
                            'step_data' => $step_data,
                            'student_details' => $student_details,
                ]);
                
            } elseif (Yii::$app->getRequest()->post('step') == '2') {    //remove card than add credit card
                
                $modelcard = new StudentCreditCard();
                $modelcard->scenario = 'step_term_condition';
                $model->scenario = 'step_2';

                StudentMonthlySubscription::setSession("MS_" . Yii::$app->user->identity->student_uuid, 'step_2', Yii::$app->getRequest()->post());
                $step_data = StudentMonthlySubscription::getSession("MS_" . Yii::$app->user->identity->student_uuid);
                
                if ($modelcard->load(Yii::$app->request->post()) && $modelcard->validate()) { 
                    
                    //session
                    $studentMonthlySub = Yii::$app->session->get('studentMonthlySub'); 
                   // echo "<pre>"; print_r($studentMonthlySub); exit();
                    $selectCreditCard = Yii::$app->getRequest()->post();
                    
                    if (!empty($selectCreditCard)) { 
                        $payment_response = StudentMonthlySubscription::SuccessPayment($studentMonthlySub, $selectCreditCard);

                        if (isset($payment_response['code']) && $payment_response['code'] == 200) {

                            Yii::$app->getSession()->setFlash('success', 'You have buy  monthly subscription successfully.');
                        } else {

                            $message = (isset($payment_response['message'])) ? $payment_response['message'] : 'Sorry, something went wrong.';
                            Yii::$app->getSession()->setFlash('danger', $message);
                        }
                        return $this->redirect(['monthly-subscription/index']);
                    }
                }else {
                    return $this->render('step_2', [
                                'model' => $modelcard,
                                'step_data' => $step_data,
                                'student_details' => $student_details,
                    ]);
                }
                
            } elseif (Yii::$app->getRequest()->post('step_else') == '2') {    //select credit card
                $model->scenario = 'step_2_else';
                //session
                $modelcard = new StudentCreditCard();
                $modelcard->scenario = 'step_term_condition_cvv';
                
                StudentMonthlySubscription::setSession("MS_" . Yii::$app->user->identity->student_uuid, 'step_2_else', Yii::$app->getRequest()->post());
                $step_data = StudentMonthlySubscription::getSession("MS_" . Yii::$app->user->identity->student_uuid);

                if ($modelcard->load(Yii::$app->request->post()) && $modelcard->validate()) {

                    $studentMonthlySub = Yii::$app->session->get('studentMonthlySub');
                    //echo "<pre>"; print_r($studentMonthlySub); exit();
                    $selectCreditCard = Yii::$app->getRequest()->post();

                    if (!empty($selectCreditCard)) {
                        $payment_response = StudentMonthlySubscription::SuccessPayment($studentMonthlySub, $selectCreditCard);

                        if (isset($payment_response['code']) && $payment_response['code'] == 200) {

                            Yii::$app->getSession()->setFlash('success', 'You have buy  monthly subscription successfully.');
                        } else {

                            $message = (isset($payment_response['message'])) ? $payment_response['message'] : 'Sorry, something went wrong.';
                            Yii::$app->getSession()->setFlash('danger', $message);
                        }
                        return $this->redirect(['monthly-subscription/index']);
                    }
                }

                $creditCard = Yii::$app->session->get('creditCard');
                return $this->render('step_2_else', [
                            'model' => $modelcard,
                            'studentMonthlysubDetail' => $subscriptionFeeList,
                            'creditCard' => $creditCard,
                            'step_data' => $step_data,
                            'student_details' => $student_details,
                ]);
            } else { //DONE
                $model->scenario = 'step_1_else';
                return $this->render('step_1_else', [
                            'model' => $model,
                            'studentMonthlysubDetail' => $studentMonthlySubDetail,
                            //'step_data' => $step_data,
                            'student_details' => $student_details,
                ]);
            }
        }
    }

    public function actionDelete() {
        $id = Yii::$app->getRequest()->post('id');
        if ((($model = StudentCreditCard::findOne($id)) !== null)) {
            $model->delete();
            $return = ['code' => 200, 'message' => 'Credit Card deleted successfully.'];
        } else {
            $return = ['code' => 404, 'message' => 'The requested page does not exist.'];
        }
        Yii::$app->response->format = 'json';
        return $return;
        return $this->redirect(['index']);
    }

    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate() {

//        $model = new StudentMonthlySubscription();
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->uuid]);
//        }
//
//        return $this->render('create', [
//                    'model' => $model,
//        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->uuid]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    protected function findModel($id) {
        if (($model = StudentMonthlySubscription::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionCancel($id) {
        if (!Yii::$app->request->isAjax) {
            Yii::$app->response->format = 'json';
            return ['code' => 505, 'message' => 'You are not authorised to access this page'];
        }

        if (($model = \app\models\Student::findOne($id)) !== null) {
            $model->isCancelMonthlySubscriptionRequest = 1;
            if ($model->save(false)) {
                self::MonthlySubscriptionCancelMail($model);
                self::MonthlySubscriptionCancelMailAdmin($model);
                $return = ['code' => 200, 'message' => 'Your monthly subscription plan cancel request is successfully done. After administrator approval your plan has been cancel.'];
            } else {
                $return = ['code' => 421, 'message' => 'Something went worng. Please try after sometime or contact your administrator.'];
            }
        } else {
            $return = ['code' => 404, 'message' => 'Record does not exist.'];
        }
        Yii::$app->response->format = 'json';
        return $return;
    }

    public function MonthlySubscriptionCancelMail($student) {
        $content['student_name'] = $student->first_name . ' ' . $student->last_name;
        return Yii::$app->mailer->compose('monthly_subscription_cancel', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                        ->setFrom(Yii::$app->params['supportEmail'])
                        ->setTo($student->email)
                        ->setSubject('Cancel Monthly Subscription - ' . Yii::$app->name)
                        ->send();
    }

    public function MonthlySubscriptionCancelMailAdmin($student, $is_admin = false) {
        $content['student_name'] = $student->first_name . ' ' . $student->last_name;
        $content['student_email'] = $student->email;
        $owner = General::getOwnersEmails(['OWNER']);
        $admin = General::getOwnersEmails(['ADMIN']);

        return Yii::$app->mailer->compose('monthly_subscription_cancel_admin', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                        ->setFrom(Yii::$app->params['supportEmail'])
                        ->setTo($admin)
                        ->setBcc($owner)
                        ->setSubject('Cancel Monthly Subscription by ' . $content['student_name'] . ' - ' . Yii::$app->name)
                        ->send();
    }

    public function actionHistory() {
        
        $this->layout = "student_layout";
        $searchModel = new StudentMonthlySubscriptionSearch();
        $monthlySubList = $searchModel->search(Yii::$app->request->queryParams, true);

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_monthly_sub_history_list', [
                        'monthlySubList' => $monthlySubList,
            ]);
        } else {
            return $this->render('index', [
                        'monthlySubList' => $monthlySubList,
                        'searchModel' => $searchModel,
            ]);
        }
    }

    public function actionReport() {
        
        $searchModel = new StudentMonthlySubscriptionSearch();
        $monthlySubList = $searchModel->searchReport(Yii::$app->request->queryParams, true);

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_report_list', [
                        'monthlySubList' => $monthlySubList,
            ]);
        } else {
            return $this->render('report', [
                        'monthlySubList' => $monthlySubList,
                        'searchModel' => $searchModel,
            ]);
        }
    }

}
