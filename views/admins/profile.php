<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\libraries\General;

/* @var $this yii\web\View */
/* @var $model app\models\Admin */

$timezone = General::getAllTimeZones();
if($userModel->timezone == '') {
 $userModel->timezone = Yii::$app->params['timezone'];
}
$this->title = Yii::t('app', 'Profile');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs('

    function refreshCPForm(){
        $.ajax({
            type: "GET",
            url:  "' . Url::to(['admins/change-password']) . '",
            success:function(result){
                $("#changepassword_div").html(result);
            },
            error:function(e){
                showErrorMessage(e.responseText);
            }
        });
    }
    $(document).on("click","#password-change-btn",function(){
        $.ajax({
            type: "POST",
            url:  "' . Url::to(['admins/change-password']) . '",
            data: $("#form-change").serialize(),
            success:function(result){
                if(result.code == 200){
                    showSuccess(result.message);
                    refreshCPForm();
                }else{
                    $("#changepassword_div").html(result);
                }
            },
            error:function(e){
                showErrorMessage(e.responseText);
            }
        });
    })
        ');
?>
<div class="col-lg-12">
    <div class="row">
        <div class="col-md-3 col-sm-4 col-xs-12 ">
            <section class="box">
                <div class="content-body">
                    <div class="uprofile-image ">
                        <img src="/data/profile/profile.png" class="img-fluid user-profile-status-active">
                    </div>
                    <div class="uprofile-name">
                        <h3>
                            <a href="#"><?= $model->first_name . ' ' . $model->last_name; ?></a>
                            <!-- Available statuses: online, idle, busy, away and offline -->
                            <span class="uprofile-status online"></span>
                        </h3>
                        <p class="uprofile-title"><?= Yii::$app->user->identity->role; ?></p>
                    </div>
                </div>
            </section>
        </div>

        <div class="col-md-9 col-sm-8 col-xs-12">
            <section class="box">
                <!-- Horizontal - start -->
                <div class="content-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="nav nav-tabs primary"  id="myTab1" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" role="tab" href="#profile-1" data-toggle="tab">
                                        <i class="fa fa-user"></i> Profile
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="#change-password" role="tab" data-toggle="tab">
                                        <i class="fa fa-lock"></i> Change Password
                                    </a>
                                </li>
                            </ul>

                            <div class="tab-content primary" id="myTabContent1">
                                <div class="tab-pane fade show active" id="profile-1">
                                    <div>
                                        <?php $form = ActiveForm::begin(); ?>
                                        <div class="form-row">
                                            <div class="col-md-6 mb-0">
                                                <?= $form->field($userModel, 'email')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $userModel->getAttributeLabel('email')]) ?>
                                            </div>
                                            <div class="col-md-3 mb-0">
                                                <div class="form-group ">
                                                    <label class="control-label" ><?= $model->getAttributeLabel('admin_type'); ?></label>
                                                    <span  class="form-control bg-gray-field" aria-required="true" aria-invalid="true"><?= $model->admin_type; ?></span>
                                                </div>
                                            </div>
                                            <div class="col-md-3 mb-0">
                                                <div class="form-group ">
                                                    <label class="control-label" ><?= $model->getAttributeLabel('status'); ?></label>
                                                    <span  class="form-control bg-gray-field" aria-required="true" aria-invalid="true"><?= $model->status; ?></span>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="form-row">
                                            <div class="col-md-6 mb-0">
                                                <?= $form->field($model, 'first_name')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('first_name')]) ?>
                                            </div>
                                            <div class="col-md-6 mb-0">
                                                <?= $form->field($model, 'last_name')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('last_name')]) ?>
                                            </div>

                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-4 mb-0">
                                                <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), ['mask' => '9999 999 999', 'clientOptions' => ['removeMaskOnSubmit' => true],'options' => ['placeholder' => 'XXXX XXX XXX', 'class' => 'form-control']]) ?>
                                            </div>
                                            <div class="col-md-4 mb-0">
                                                <?= $form->field($model, 'company_position')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('company_position')]) ?>
                                            </div>
                                            <div class="col-md-4 mb-0">
                                                <?= $form->field($userModel, 'timezone')->dropDownList($timezone, ['class' => 'form-control myselect2', 'prompt' => 'Select Timezone', 'maxlength' => true]) ?>
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="col-md-6 mb-0">
                                                <?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
                                            </div>
                                        </div>
                                        <?php ActiveForm::end(); ?>
                                    </div>

                                </div>
                                <div class="tab-pane fade" id="change-password">

                                    <div id="changepassword_div">
                                        <?php echo Yii::$app->controller->renderPartial('change-password', ['modelCP' => $modelCP]); ?>
                                    </div>

                                </div>
                            </div>

                        </div>
                        <br><div class="spacer"></div><div class="spacer"></div>
                    </div>
                    <!-- Horizontal - end -->
                </div>
            </section>

        </div>
    </div>
</div>
