<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Url;
use app\models\Instrument;
use app\models\OnlineTutorialPackage;
use app\models\StudentTuitionBooking;

$this->title = Yii::t('app', 'BOOK MUSIC LESSONS');
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_icon'] = 'my-lesson-tag.png';

if (!empty($step_data['step_1']['instrument_uuid'])) {
    $instrumentDetail = Instrument::find()->where(['uuid' => $step_data['step_1']['instrument_uuid']])->one();
} else {
    return \yii\web\Controller::redirect(['index']);
}

//$planDetail = OnlineTutorialPackage::find()->where(['uuid' => $step_data['step_1']['onlinetutorial_uuid']])->one();

$tutorList = StudentTuitionBooking::getInstrumentWiseTutorList($step_data['step_1']['instrument_uuid']);

if (isset($step_data['step_3']['tutor_uuid']) && $step_data['step_3']['tutor_uuid'] != "") {
    $model->tutor_uuid = $step_data['step_3']['tutor_uuid'];
    $model->tutorcheckbox = $model->tutor_uuid;
}

//echo "<pre>";
//print_r($step_data);
//echo $model->tutor_uuid;
//echo "</pre>";
$this->registerJs(
        '
$(document).on("ifClicked","input[name=\"StudentTuitionBooking[tutorcheckbox]\"]",function (event) {

    $("#studenttuitionbooking-tutor_uuid").val(this.value);
    $(".tutor_select_btn").addClass("d-none");
    $(this).closest("div.tutor_tr").find(".tutor_select_btn").removeClass("d-none");

});
$( "#frm_step_3,#gostep1,#gostep2" ).submit(function( event ) {
        blockBody();
});
        ', View::POS_END
);
?>

<section class="content">
    <label class="warning-info mar-bottom-10">For issues concerning tutor availability, please contact <a href="mailto:<?= Yii::$app->params['adminEmail']; ?>"><?= Yii::$app->params['adminEmail']; ?></a> for assistance.</label>
    <div class="row mar-bottom-10">
        <div class="col-xs-12">
            <div class="box">
                <div class="col-md-12 box-panel-header ">
                    <h4 class="col-md-6">Select Instrument &nbsp;<i class="fa fa-check"></i></h4>
                    <div class="col-md-6 ">
                        <?php echo Html::beginForm(['index'], 'post', ['id' => 'gostep1', 'class' => 'form-inline d-block']) ?>
                        <input type="hidden" name="step" value="1">
                        <input type="hidden" name="prev" value="1">
                        <a href="javascript:$('#gostep1').submit();" class="box-panel-header-right-btn pull-right">
                            Change
                        </a>
                        <p class="box-panel-header-right-p text-white pull-right"> <?= $instrumentDetail->name; ?>&nbsp;</p>

                        <?php echo Html::endForm() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

     <div class="row mar-bottom-10">
        <div class="col-xs-12">
            <div class="box">
                <div class="col-md-12 box-panel-header border-bottom-radious-none">
                    <h4 class="col-md-6">Select Tutor</h4>
                    <div class="col-md-6 "> </div>
                </div>
                <div class="box-body box-panel-body" style="max-height: 500px;overflow-x: hidden;overflow-y: scroll;border-top-left-radius: 0px;border-top-right-radius: 0px;">
                    <div class="row vertical-center-box vertical-center-box-tablet"  style="">
                        <?php
                        $form = ActiveForm::begin([
                                    'action' => ['index'],
                                    'id' => 'frm_step_2',
                                    'enableClientValidation' => true,
//                                'enableAjaxValidation' => true,
                                    'validateOnChange' => true,
                                    'options' => ['class' => 'form-horizontal']]);
                        ?>

                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <div class="row" style="">
                                    <input type="hidden" name="step" value="2">
                                    <?= $form->field($model, 'tutor_uuid')->hiddenInput(['class' => 'form-control', 'maxlength' => true,])->label(false) ?>
                                </div>
                                <div class="tutor_list_parent">
                                    
                                <?php
                                if (!empty($tutorList)) {
                                    foreach ($tutorList as $key => $value) {

                                        ob_start();
                                        fpassthru($value['profile_image']);
                                        $contents = ob_get_contents();
                                        ob_end_clean();
                                        $dataUri = "data:image/jpg;base64," . base64_encode($contents);

                                        $displayClass = ($model->tutor_uuid == $value['uuid']) ? "" : "d-none";
                                        ?>
                                        <div class="row pakage-list tutor_tr " >
                                            <div class="col-md-2 text-center">
                                                <?php
                                                echo $form->field($model, 'tutorcheckbox')->radio([
                                                    'label' => '',
                                                    'template' => '{input}{error} ',
                                                    'class' => 'skin-square-red float-left tutor_checkbox', 'value' => $value['uuid']])->label('')
                                                ?>
                                            </div>
                                            <div class="col-md-2">
                                                <!--<span title="Availability" class="badge badge-md badge-secondary tutor_availability_calender_btn" data-id="<?= $value['uuid'] ?>" data-tutorname="<?= $value['first_name'] . ' ' . $value['last_name'] ?>"><i class="fa fa-calendar "></i></span>-->
                                            </div>
                                            <div class="col-md-2">

                                                <img class="" style="width: 30px;" src="<?= $dataUri; ?>">
                                            </div>
                                            <div class="col-md-4">
                                                <?= $value['first_name'] . ' ' . $value['last_name']; ?>
                                            </div>
                                            <div class="col-md-2">
                                                <?= Html::submitButton('Select', ['class' => "btn btn-primary btn-md tutor_select_btn $displayClass"]) ?>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                } else {
                                    echo "No Tutor available for this instrument.";
                                }
                                ?>
                                </div>

                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
    
    <div class="row mar-bottom-10">
        <div class="col-xs-12">
            <div class="box">
                <div class="col-md-12 box-panel-header ">
                    <h4 class="col-md-6">Select Plan &nbsp;<i class="fa fa-check"></i></h4>
                    <div class="col-md-6 ">                        
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

   
    
    <div class="row mar-bottom-10">
        <div class="col-xs-12">
            <div class="box">
                <div class="col-md-12 box-panel-header ">
                    <h4 class="col-md-6">Select Date</h4>
                    <div class="col-md-6 "></div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row mar-bottom-10">
        <div class="col-xs-12">
            <div class="box">
                <div class="col-md-12 box-panel-header ">
                    <h4 class="col-md-6">Confirm and Payment</h4>
                    <div class="col-md-6 "></div>
                </div>
            </div>
        </div>
    </div>
    
    <a class="btn btn-primary btn-sm black_button_in" href="<?= Yii::$app->getUrlManager()->createUrl('/booked-session/my-lessons'); ?>">RETURN TO MY LESSONS</a>

</section>
