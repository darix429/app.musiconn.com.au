<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%payment_Transaction}}".
 *
 * @property string $uuid
 * @property string $student_uuid
 * @property string $transaction_number
 * @property string $book_uuid
 * @property string $type
 * @property string $payment_datetime
 * @property string $amount
 * @property string $payment_method
 * @property string $reference_number
 * @property string $status
 * @property string $subscription_uuid
 * @property string $response
 *
 * @property Student $studentUu
 */
class PaymentTransaction extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%payment_Transaction}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'student_uuid', 'transaction_number', 'payment_method', 'reference_number', 'status'], 'required'],
            [['uuid', 'student_uuid', 'transaction_number', 'book_uuid', 'type', 'payment_method', 'reference_number', 'status', 'subscription_uuid', 'response'], 'string'],
            [['payment_datetime'], 'safe'],
            [['amount'], 'number'],
            [['uuid'], 'unique'],
            [['student_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['student_uuid' => 'student_uuid']],
            [['book_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => StudentTuitionBooking::className(), 'targetAttribute' => ['book_uuid' => 'uuid']],
            [['subscription_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => StudentMonthlySubscription::className(), 'targetAttribute' => ['subscription_uuid' => 'uuid']],
            //[['subscription_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['student_uuid' => 'student_uuid']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'student_uuid' => Yii::t('app', 'Student Uuid'),
            'transaction_number' => Yii::t('app', 'Transaction Number'),
            'book_uuid' => Yii::t('app', 'Book Uuid'),
            'type' => Yii::t('app', 'Type'),
            'payment_datetime' => Yii::t('app', 'Payment Datetime'),
            'amount' => Yii::t('app', 'Amount'),
            'payment_method' => Yii::t('app', 'Payment Method'),
            'reference_number' => Yii::t('app', 'Reference Number'),
            'status' => Yii::t('app', 'Status'),
            'subscription_uuid' => Yii::t('app', 'Subscription Uuid'),
            'response' => Yii::t('app', 'Response'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentUu()
    {
        return $this->hasOne(Student::className(), ['student_uuid' => 'student_uuid']);
    }
    
    public function getStudentTuitionBookingUu()
    {
        return $this->hasOne(StudentTuitionBooking::className(), ['uuid' => 'book_uuid']);
    }
    
    public function getStudentMonthlySubscriptionUu()
    {
        return $this->hasOne(StudentMonthlySubscription::className(), ['uuid' => 'subscription_uuid']);
    }
    
    public function getOrderUu()
    {
        return $this->hasOne(Order::className(), ['payment_uuid' => 'uuid']);
    }
    
    public function getStudentMusicoinPackageUu()
    {
        return $this->hasOne(StudentMusicoinPackage::className(), ['uuid' => 'student_musicoin_package_uuid']);
    }
}
