<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
//rupal
use yii\helpers\ArrayHelper;
use app\models\InstrumentCategory;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Admin */
/* @var $form yii\widgets\ActiveForm */
//echo "<pre>";print_r(Yii::$app->controller);
$parentIdArray = ArrayHelper::map(InstrumentCategory::find()->all(), 'uuid', 'name');

?>



<div class="col-xl-12 col-lg-12 col-12 col-md-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Please Fill Information</h2>
            <div class="actions panel_actions float-right">
                <!--<i class="box_toggle fa fa-chevron-down"></i>-->
                <a href="<?= Yii::$app->getUrlManager()->createUrl(['/instrument-category/index/']) ?>" class="btn btn-info btn-icon "><i class="fa fa-arrow-left text-white"></i> <span>Back</span></a>
            </div>
        </header>
        <div class="content-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12">
                    <?php
//                    echo "<pre>";
//                    print_r($model->getErrors());
//                    print_r($userModel->getErrors());
//                    echo "</pre>";
                    ?>
                    <?php $form = ActiveForm::begin(['id' => 'category_create_form']); ?>
                    <div class="form-row">
                        <div class="col-md-6 mb-0">
                            <?= $form->field($model, 'name')->textInput(['class' => 'form-control code', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('name')]) ?>
                        </div>
                        <div class="col-md-6 mb-0">
                            <?= $form->field($model, 'parent_id')->dropDownList($parentIdArray, ['class' => 'form-control', 'maxlength' => true, 'prompt' => "Select Parent ID"]) ?>
                        </div> 

                    </div>

                    <?= Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn btn-primary', 'style' => 'float: right;']) ?>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>

        </div>
    </section>
</div>






