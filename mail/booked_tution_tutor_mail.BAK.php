<?php

use yii\helpers\Html;
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['tutor_name']; ?>,</p>
            
            <p>A student has just purchased a package of lessons with you.  Below are details of the lessons booked:</p>
            
            <p style="text-decoration: underline;"><strong>Scheduled tutorials</strong></p>
            
            <p><strong>Booking Reference :</strong> <?= "#".$content['booking_id']; ?></p>
            <p><strong>Student Name :</strong> <?= $content['student_name']." ".$content['student_last_name']; ?></p>
            <p><strong>Instrument :</strong> <?= $content['instrument']; ?></p>
            
            <p><strong>Scheduled Lessons :</strong> </p>
                <?php if(is_array($content['session_dates'])){ ?>
                <ul>
                    <?php foreach ($content['session_dates'] as $key => $value) {
                        echo "<li>".$value." ".$content['time']."</li>";
                    } ?>
                </ul>
                <?php } ?>
            <p><strong>Subscription to online contents :</strong> <?= (!empty($content['student_monthly_subscription_uuid']) ) ? "Yes" : "No"; ?></p>
                       
            <p>You can view details of lessons booked with you by logging onto the <a target="_blank" href="https://app.musiconn.com.au/">booking system</a>.</p>
            
            <p>If you need to re-schedule a lesson with a student, please refer to the tutor’s manual for instructions.</p>
            
            <p>Please do not reply to this unattended mailbox.  Any queries can be directed to admin@musiconn.com.au, or call us on 1300 068 742.</p>

            
            <p>Regards,<br>
            Admin Team – Musiconn Pty Ltd</p>
        </td>
    </tr>
</table>

