<?php

namespace app\models;

use Yii;
use app\models\PaymentTransaction;
use app\models\StudentCreditCard;
use app\models\MonthlySubscriptionFee;
use app\models\Student;
use app\models\Order;
use app\libraries\General;
use app\models\StudentSubscription;
use kartik\mpdf\Pdf;
use yii\helpers\FileHelper;


/**
 * This is the model class for table "{{%student_monthly_subscription}}".
 *
 * @property string $uuid
 * @property string $student_uuid
 * @property string $name
 * @property string $price
 * @property string $tax
 * @property string $total_price
 * @property string $description
 * @property int $with_tutorial
 * @property string $status
 * @property string $start_datetime
 * @property string $end_datetime
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Student $studentUu
 */
class StudentMonthlySubscription extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public $monthlysubscriptionfeecheckbox;
    public $cc_cvv;
     //public $term_condition_checkbox;


    public static function tableName() {
        return '{{%student_monthly_subscription}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['uuid', 'student_uuid', 'name', 'start_datetime', 'end_datetime'], 'required'],
            [['uuid', 'student_uuid', 'name', 'description', 'status'], 'string'],
            [['price', 'tax', 'total_price'], 'number'],
            [['with_tutorial'], 'default', 'value' => null],
            [['with_tutorial'], 'integer'],
            [['cc_cvv'], 'integer', 'min' => 3],
            [['start_datetime', 'end_datetime', 'created_at', 'updated_at'], 'safe'],
            [['uuid'], 'unique'],
            [['student_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['student_uuid' => 'student_uuid']],
            [['order_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_uuid' => 'order_id']],
            //[['term_condition_checkbox'], 'required', 'on' => 'step_2','requiredValue' => 1,'message' => 'Accept Terms and Condition'],
            //[['monthlysubscriptionfee_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => MonthlySubscriptionFee::className(), 'targetAttribute' => ['monthlysubscriptionfee_uuid' => 'uuid']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'student_uuid' => Yii::t('app', 'Student Uuid'),
            'name' => Yii::t('app', 'Name'),
            'price' => Yii::t('app', 'Price'),
            'tax' => Yii::t('app', 'Tax'),
            'total_price' => Yii::t('app', 'Total Price'),
            'description' => Yii::t('app', 'Description'),
            'with_tutorial' => Yii::t('app', 'With Tutorial'),
            'status' => Yii::t('app', 'Status'),
            'start_datetime' => Yii::t('app', 'Start Datetime'),
            'end_datetime' => Yii::t('app', 'End Datetime'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            //'term_condition_checkbox' => 'Term and condition',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getStudentUu() {
        return $this->hasOne(Student::className(), ['student_uuid' => 'student_uuid']);
    }

    public static function getOrderUu(){
        return $this->hasOne(Order::className(), ['uuid' => 'order_uuid']);
    }

    public static function getOrderID(){
        return StudentMonthlySubscription::find()->joinWith('orderUu')->orderBy('student_monthly_subscription.order_uuid, order.order_id')->one();
    }

    public static function setSession($_id, $key, $value) {
        if (!isset(Yii::$app->session[$_id])) {
            Yii::$app->session[$_id] = serialize(array($key => $value));
        } else {
            $session = unserialize(Yii::$app->session[$_id]);
            $session[$key] = $value;
            Yii::$app->session[$_id] = serialize($session);
        }
    }

    public static function getSession($_id, $key = NULL) {
        $session = unserialize(Yii::$app->session[$_id]);

        if ($key === NULL) {
            return $session;
        } else {
            return isset($session[$key]) ? $session[$key] : array();
        }
    }

    public static function getsubscriptionFeeList($student_uuid) {

        /*$s_c_count = \app\models\CreditSession::getStudentCreditSessionCount($student_uuid);

        $statusIn = ['SCHEDULE','INPROCESS'];
        $upcomingList = BookedSession::find()
                ->where(['booked_session.student_uuid' => $student_uuid])
                ->andWhere(['in', 'booked_session.status', $statusIn])
                ->asArray()->orderBy('booked_session.start_datetime asc')->limit(3)->all();

        if (!empty($upcomingList) || $s_c_count > 0) {
            return MonthlySubscriptionFee::find()->where(['with_tutorial' => '1', 'status' => 'ENABLED'])->one();
        } else{
            return MonthlySubscriptionFee::find()->where(['with_tutorial' => '0', 'status' => 'ENABLED'])->one();
        }*/
                    return MonthlySubscriptionFee::find()->where(['status' => 'ENABLED'])->one();

        
    }


    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['step_1'] = ['monthlysubscriptionfee_uuid', 'monthlysubscriptionfeecheckbox'];
        $scenarios['step_2'] = ['payment', 'term_condition_checkbox'];
        $scenarios['step_1_else'] = ['monthlysubscriptionfee_uuid', 'monthly_sub_fee_select_btn'];
        return $scenarios;
    }

    public static function SuccessPayment($studentMonthlySub, $availableCard) {

        $return = ['code' => 421, 'message' => 'Something went wrong.'];
        $student = Student::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid, 'status'=>'ENABLED'])->one();

        if (!empty($_POST['stripeToken'])) {

            $token = $_POST['stripeToken'];
            $email = $student->email;

//            $dollar_amount = (int) $studentMonthlySub['total_price'];
            $dollar_amount = (float) $studentMonthlySub['total_price'];
            $cents_amount = $dollar_amount * 100;       // 1 AUD = 100 cents
            //include Stripe PHP library
            require Yii::$app->basePath . '/vendor/stripe/stripe-php/init.php';

            //set api key
            $stripe = array(
                "secret_key" => Yii::$app->params['stripe']['secret_key'],
                "publishable_key" => Yii::$app->params['stripe']['publishable_key']
            );

            try {
                \Stripe\Stripe::setApiKey($stripe['secret_key']);
                $customer = \Stripe\Customer::create(array(
                            'email' => $email,
                            'source' => $token
                ));

                $student_name = $student->first_name.' '.$student->last_name;
                //item information
                $orderID = General::generateTransactionID();

                //$itemName = $studentMonthlySub['name'];
                $itemName = $orderID.' - '.$student->enrollment_id.' - '.$student_name.' - Monthly Subscription';
                $itemPrice = $cents_amount;
                $currency = Yii::$app->params['stripe']['currency'];
                

                //charge a credit or a debit card
                $charge = \Stripe\Charge::create(array(
                            'customer' => $customer->id,
                            'amount' => $itemPrice,
                            'currency' => $currency,
                            'description' => $itemName,
                            'metadata' => array(
                                'order_id' => $orderID
                            )
                ));
            } catch (\Stripe\Error\ApiConnection $e) {
                return $return = ['code' => 421, 'message' => 'Network problem, perhaps try again.'];
                //Yii::$app->session->setFlash('danger', Yii::t('app', 'Network problem, perhaps try again.'));
                //return \yii\web\Controller::redirect(['index']);
            } catch (\Stripe\Error\InvalidRequest $e) {
                return $return = ['code' => 421, 'message' => 'Your request is invalid.'];
                //Yii::$app->session->setFlash('danger', Yii::t('app', 'Your request is invalid.'));
                //return \yii\web\Controller::redirect(['index']);
            } catch (\Stripe\Error\Api $e) {
                return $return = ['code' => 421, 'message' => 'Stripe servers are down!'];
                //Yii::$app->session->setFlash('danger', Yii::t('app', 'Stripe servers are down!'));
                //return \yii\web\Controller::redirect(['index']);
            } catch (\Stripe\Error\Card $e) {
                return $return = ['code' => 421, 'message' => 'This Card is Declined.'];
                //Yii::$app->session->setFlash('danger', Yii::t('app', 'This Card is Declined.'));
                //return \yii\web\Controller::redirect(['index']);
            }

            $chargeJson = $charge->jsonSerialize();

            if ($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1) {

                $subscription_uuid = NULL;

                //Payment Transaction
                $modelPT = new PaymentTransaction();
                $modelPT->student_uuid = Yii::$app->user->identity->student_uuid;
                $modelPT->transaction_number = $orderID;
                $modelPT->payment_datetime = date("Y-m-d H:i:s");
                $modelPT->amount = $chargeJson['amount'] / 100;
                $modelPT->payment_method = 'Stripe';
                $modelPT->reference_number = $chargeJson['balance_transaction'];
                $modelPT->status = $chargeJson['status'];
                $modelPT->subscription_uuid = $subscription_uuid;
                $modelPT->response = json_encode($chargeJson);
                $modelPT->save(false);

                //cc save or not
                $creditCard = Yii::$app->session->get('creditCard');

                if (empty($creditCard)) {
                    if (Yii::$app->getRequest()->post()['StudentCreditCard']['save_future'] == '1') {

                        $modelCC = new StudentCreditCard();
                        $modelCC->attributes = Yii::$app->getRequest()->post('StudentCreditCard');

                        $modelCC->student_uuid = Yii::$app->user->identity->student_uuid;
                        $modelCC->number = General::encrypt_str($modelCC->number);
                        $modelCC->cvv = General::encrypt_str($modelCC->cvv);
                        $modelCC->expire_date = date('Y-m-t', strtotime(Yii::$app->request->post('StudentCreditCard')['cc_month_year']));
                        $modelCC->name = $modelCC->name;
                        $modelCC->status = 'ENABLED';
                        $modelCC->stripe_customer_id = $chargeJson['customer'];
                        $modelCC->save(false);
                    }
                } else {

                    $post_studentCC = Yii::$app->request->post('StudentCreditCard');

                    $modelCC = StudentCreditCard::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid])->one();
                    if(empty($modelCC)){

                        $modelCC = new StudentCreditCard;
                        $modelCC->student_uuid = Yii::$app->user->identity->student_uuid;
                        $modelCC->number = General::encrypt_str($post_studentCC['number']);
                        $modelCC->expire_date = date('Y-m-t', strtotime($post_studentCC['cc_month_year']));
                        $modelCC->name = $post_studentCC['name'];
                        $modelCC->status = 'ENABLED';
                    }
                    $modelCC->stripe_customer_id = $chargeJson['customer'];
                    $modelCC->cvv = General::encrypt_str($post_studentCC['cvv']);
                    $modelCC->updated_at = date('Y-m-d H:i:s', time());
                    $modelCC->save(false);
                }

                //student monthly subscription
                $modelStudent = Student::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid])->one();
                $modelStudentSubscription = StudentSubscription::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid])->one();

                $start_date = date('Y-m-d H:i:s');
                if($modelStudent->isMonthlySubscription == 1 ){
                    if(!empty($modelStudentSubscription)){
                        $start_date = date('Y-m-d H:i:s', strtotime($modelStudentSubscription->next_renewal_date));
                    }
                }
                //$start_date = (!empty($modelStudentSubscription) && !empty($modelStudent)) ? date('Y-m-d H:i:s', strtotime($modelStudentSubscription->next_renewal_date)) : date('Y-m-d H:i:s');
                $end_date = date('Y-m-d H:i:s', strtotime($start_date . " +30 days"));
                $next_renewal_date = date('Y-m-d', strtotime($end_date . " +1 days"));

                $modelMS = new StudentMonthlySubscription();
                $modelMS->student_uuid = Yii::$app->user->identity->student_uuid;
                $modelMS->name = $studentMonthlySub['name'];
                $modelMS->price = $studentMonthlySub['price'];
                $modelMS->tax = $studentMonthlySub['tax'];
                $modelMS->total_price = $studentMonthlySub['total_price'];
                $modelMS->description = $studentMonthlySub['description'];
                $modelMS->with_tutorial = $studentMonthlySub['with_tutorial'];
                $modelMS->status = $studentMonthlySub['status'];
                $modelMS->start_datetime = $start_date;
                $modelMS->end_datetime = $end_date;
                $modelMS->created_at = $modelMS->updated_at = date("Y-m-d H:i:s");
                if ($modelMS->save(false)) {

                    //update Transaction Subscription id
                    $modelPaymentUpdate = PaymentTransaction::find()->where(['uuid' => $modelPT->getPrimaryKey()])->one();
                    $modelPaymentUpdate->subscription_uuid = $modelMS->getPrimaryKey();
                    $modelPaymentUpdate->save(false);

                    $save_m_s_flag = true;

                    //student_subscription
                    if (empty($modelStudentSubscription)) {
                        $modelStudentSubscription = new StudentSubscription();
                    }
                    $modelStudentSubscription->student_uuid = $modelMS->student_uuid;
                    $modelStudentSubscription->subscription_uuid = $modelMS->getPrimaryKey();
                    $modelStudentSubscription->next_renewal_date = $next_renewal_date;
                    $modelStudentSubscription->save(false);

                    //update student monthly sub flag
                    $student->isMonthlySubscription = 1;
                    $student->isCancelMonthlySubscriptionRequest = 0;
                    $student->save(false);

                    $item_array[0] = ['item' => $modelMS->name, 'booking_uuid' => NULL, 'student_monthly_subscription_uuid' => $modelMS->getPrimaryKey(), 'tax' => $modelMS->tax, 'price' => $modelMS->price, 'total' => $modelMS->total_price];

                    $order_array['student_uuid'] = $modelMS->student_uuid;
                    $order_array['payment_uuid'] = $modelPT->getPrimaryKey();
                    $order_array['discount'] = 0;
                    $order_array['item'] = $item_array;
                    $order_array['total'] = $modelMS->total_price;
                    $order_array['grand_total'] = $modelMS->total_price;

                    $order_result = self::generate_order($order_array);

                    $isRecuring = (isset($_POST['StudentCreditCard']['save_future']) && $_POST['StudentCreditCard']['save_future'] == '1') ? "Yes" : "No";

                    if ($save_m_s_flag) {
                        self::MonthlySubscriptionBuyMail($studentMonthlySub, $modelMS->student_uuid,$order_result, $isRecuring, $orderID);

                    }
                }

                return $return = ['code' => 200, 'message' => 'success'];

            } else {
                return $return = ['code' => 421, 'message' => 'Transaction has been failed.'];
            }
        } else {
            return $return = ['code' => 421, 'message' => 'Form submission error.'];
        }
        return $return;
    }
    
        public static function generate_order($order_array) {

        if (!empty($order_array)) {

            $filename = '';
            $modelOrder = new Order();
            $modelOrder->order_id = General::generateOrderID();
            $modelOrder->student_uuid = $order_array['student_uuid'];
            $modelOrder->payment_uuid = $order_array['payment_uuid'];
            $modelOrder->order_datetime = date('Y-m-d H:i:s');
            $modelOrder->total = $order_array['total'];
            $modelOrder->discount = $order_array['discount'];
            $modelOrder->grand_total = $order_array['grand_total'];
            if ($modelOrder->save(false)) {

                if (!empty($order_array['item'])) {
                    foreach ($order_array['item'] as $key => $item) {

                        $modelOrderItem = new OrderItem();
                        $modelOrderItem->order_uuid = $modelOrder->getPrimaryKey();
                        $modelOrderItem->item = $item['item'];
                        $modelOrderItem->booking_uuid = $item['booking_uuid'];
                        $modelOrderItem->student_monthly_subscription_uuid = $item['student_monthly_subscription_uuid'];
                        $modelOrderItem->student_musicoin_package_uuid = (!empty($item['student_musicoin_package_uuid']))?$item['student_musicoin_package_uuid']:NULL;
                        $modelOrderItem->price = $item['price'];
                        $modelOrderItem->tax = $item['tax'];
                        $modelOrderItem->total = $item['total'];
                        $modelOrderItem->save(false);

                        if (!empty($item['booking_uuid'])) {
                            $modelSTBooking = StudentTuitionBooking::find()->where(['uuid' => $item['booking_uuid']])->one();
                            if (!empty($modelSTBooking)) {
                                $modelSTBooking->order_uuid = $modelOrder->getPrimaryKey();
                                $modelSTBooking->save(false);
                            }
                        }

                        if (!empty($item['student_monthly_subscription_uuid'])) {
                            $modelStudentMonthlySub = StudentMonthlySubscription::find()->where(['uuid' => $item['student_monthly_subscription_uuid']])->one();
                            if (!empty($modelStudentMonthlySub)) {
                                $modelStudentMonthlySub->order_uuid = $modelOrder->getPrimaryKey();
                                $modelStudentMonthlySub->save(false);
                            }
                        }
                        if (!empty($item['student_musicoin_package_uuid'])) {
                            $modelStudentMusicoinPackage = StudentMusicoinPackage::find()->where(['uuid' => $item['student_musicoin_package_uuid']])->one();
                            if (!empty($modelStudentMusicoinPackage)) {
                                $modelStudentMusicoinPackage->order_uuid = $modelOrder->getPrimaryKey();
                                $modelStudentMusicoinPackage->save(false);
                            }
                        }
                    }
                }
                $transaction_model = \app\models\PaymentTransaction::find()->where(['uuid' => $modelOrder->payment_uuid])->asArray()->one();
                if (!empty($transaction_model['transaction_number'])) {
                    $filename = 'Invoice_' . $transaction_model['transaction_number'] . '.pdf';
                } else {
                    $filename = 'Invoice_' . $modelOrder->order_id . '.pdf';
                }
            }
            
            $pdf_res = self::create_pdf($filename, $modelOrder->getPrimaryKey());

            $modelStudent = Student::find()->where(['student_uuid' => $order_array['student_uuid']])->one();
            $student_tz = General::getStudentTimezone($order_array['student_uuid']);

            $content['student_name'] = $modelStudent->first_name;
            $content['order_uuid'] = $modelOrder->getPrimaryKey();
            $content['order_id'] = $modelOrder->order_id;
            $content['order_array'] = $order_array;
            //$content['order_date'] = General::displayDate($modelOrder->order_datetime);
            $content['order_date'] = General::convertTimezone($modelOrder->order_datetime, Yii::$app->params['timezone'], $student_tz,'d-m-Y');
            $content['order_file'] = $pdf_res['file'];


            return $content;
        }
        return [];
    }

    /**
     * This function use generate PDF file for Invoice.
     *
     */
    public static function create_pdf($filename = '', $order_uuid) {

        $file = Yii::$app->params['media']['invoice']['path'] . $filename;

        $model = \app\models\Order::findOne($order_uuid);

        if (!empty($model)) {

            $order_item = \app\models\OrderItem::find()->where(['order_uuid' => $order_uuid])->asArray()->all();
            $transaction_model = \app\models\PaymentTransaction::find()->where(['uuid' => $model->payment_uuid])->asArray()->one();

            $content = Yii::$app->controller->renderPartial('//order/buy_ms_invoice_pdf', ['model' => $model, 'order_item' => $order_item,'transaction_model' => $transaction_model]);

            if ($filename == '') {
                if (!empty($transaction_model['transaction_number'])) {
                    $filename = 'Invoice_' . $transaction_model['transaction_number'] . '.pdf';
                } else {
                    $filename = 'Invoice_' . $model->order_id . '.pdf';
                }
            }
            $file = Yii::$app->params['media']['invoice']['path'] . $filename;

            if (!is_dir(Yii::$app->params['media']['invoice']['path'])) {
                FileHelper::createDirectory(Yii::$app->params['media']['invoice']['path']);
            }

            $css_content = file_get_contents(Yii::$app->params['theme_assets']['url'] . 'plugins/bootstrap/css/bootstrap.min.css');
            $css_content .= file_get_contents(Yii::$app->params['theme_assets']['url'] . 'css/new_pdf.css');
            $css_content .= 'body {background-image: url(\'' . Yii::$app->params['theme_assets']['url'] . 'images/pdf_bg.png' . '\'); background-position: center bottom;background-repeat: no-repeat;}';
            
            $footer_html = '<table class="footer_tbl"><tr><td>ADMIN@MUSICONN.COM.AU</td><td>1300 068 742</td><td>MUSICONN.COM.AU</td></tr></table>';

            $params = [
                'mode' => Pdf::MODE_CORE,
                'format' => Pdf::FORMAT_A4,
                'orientation' => Pdf::ORIENT_PORTRAIT,
                'destination' => Pdf::DEST_FILE,
                'content' => $content,
                'filename' => Yii::$app->params['media']['invoice']['path'] . $filename,
                'tempPath' => 'mpdf/',
                'cssInline' => '.kv-heading-1{font-size:12px;border:none;}',
                'cssInline' => $css_content,
                'options' => ['title' => 'Invoice'],
                'methods' => [
                    'SetHeader' => [''],
                    'SetFooter' => [$footer_html],
                ]
            ];
            $pdf = new Pdf($params);
            $pdf->render();

            if (is_file($file)) {

                $model->invoice_file = $filename;
                $model->save(false);

                return ['status' => '1', 'file' => $file, 'filename' => $filename];
            } else {
                return ['status' => '0', 'file' => $file, 'filename' => $filename];
            }
        } else {
            return ['status' => '0', 'file' => $file, 'filename' => $filename];
        }
    }

    public static function MonthlySubscriptionBuyMail($subscriptionPlan, $student_uuid,$order_result = [],$isRecuring = "No", $transaction_id = '') {

        $student = Student::find()->where(['student_uuid' => $student_uuid, 'status'=>'ENABLED'])->one();
        $content['student_name'] = $student->first_name;
        $content['title'] = $subscriptionPlan->name;
        $content['price'] = $subscriptionPlan->price;
        $content['tax'] = $subscriptionPlan->tax;
        $content['transaction_id'] = $transaction_id;
        $content['recurring_payment'] = $isRecuring;
        $content['total_price'] = $subscriptionPlan->total_price;
        $content['order_id'] = (isset($order_result['order_id'])) ? "#".$order_result['order_id'] : "";
        $content['order_date'] = (isset($order_result['order_date'])) ? General::convertSystemToUserTimezone($order_result['order_date']) : "";
        $content['student_id'] = "#".$student->enrollment_id;
        $isInvoiceFile = (isset($order_result['order_file'])) ? $order_result['order_file'] : "";

        $content_admin['recurring_payment'] = $isRecuring;
        $content_admin['student_name'] = $student->first_name;
        $content_admin['student_last_name'] = $student->last_name;
        $content_admin['student_id'] = "#".$student->enrollment_id;
        $content_admin['order_id'] = (isset($order_result['order_id'])) ? "#".$order_result['order_id'] : "";
        $content_admin['order_date'] = (isset($order_result['order_date'])) ? $order_result['order_date'] : "";
        $content_admin['title'] = $subscriptionPlan->name;
        $content_admin['price'] = $subscriptionPlan->price;
        $content_admin['tax'] = $subscriptionPlan->tax;
        $content_admin['total_price'] = $subscriptionPlan->total_price;
        $content_admin['transaction_id'] = $transaction_id;

        $ownersEmail = General::getOwnersEmails(['OWNER', 'ADMIN']);

        $mail = Yii::$app->mailer->compose('monthly_subscription_buy', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                        ->setFrom(Yii::$app->params['supportEmail'])
                        ->setTo($student->email)
                        //->setBcc($ownersEmail)
                        ->setSubject('Online Content Subscription Purchased:  Transaction ID #' . $transaction_id);
                        if (is_file($isInvoiceFile)) {
                            $mail->attach($isInvoiceFile);
                        }
                        $mail->send();

        $mail_1 = Yii::$app->mailer->compose('monthly_subscription_buy_admin', ['content' => $content_admin], ['htmlLayout' => 'layouts/html'])
                        ->setFrom(Yii::$app->params['supportEmail'])
                        ->setTo($ownersEmail)
                        ->setSubject('Online Content Subscription Purchased: Transaction ID #'.$content_admin['transaction_id']);
                        if (is_file($isInvoiceFile)) {
                            $mail_1->attach($isInvoiceFile);
                        }
                        $mail_1->send();
         return true;
    }

    //cron job mail
    public static function ExpireSubscriptionNotification($studentsubDetails) {

        $content['student_name'] = $studentsubDetails['student_name'];
        $content['name'] = $studentsubDetails['name'];
        $content['price'] = $studentsubDetails['price'];
        $content['tax'] = $studentsubDetails['tax'];
        $content['total_price'] = $studentsubDetails['total_price'];
        $content['end_datetime'] = $studentsubDetails['end_datetime'];
        $content['renew_link'] = Yii::$app->params['base_url']."monthly-subscription/index";

        // $owner = General::getOwnersEmails(['OWNER', 'ADMIN', 'SUBADMIN']);

        $mail = Yii::$app->mailer->compose('expire_date_ago_student_sub_notification', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                ->setFrom(Yii::$app->params['supportEmail'])
                ->setTo($studentsubDetails['email'])            //$student->email
                //->setBcc($owner)
                ->setSubject("Musiconn Notification: Your monthly subscription for online contents is due to expire!");

        $mail->send();

        return true;
    }


}
