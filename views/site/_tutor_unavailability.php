<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TutorUnavailability */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([            
            'id' => 'unavailability',
]);
?>
<div class="modal-body">

    <div class="row">
        <div class="col-md-6 mb-0">
            <?php
            // $model->start_date = \app\libraries\General::displayDate($model->start_datetime);
            echo $form->field($model, 'start_datetime')->textInput(['readonly' => true, 'class' => 'form-control custom_datetime_picker', 'maxlength' => true, 'placeholder' => 'DD-MM-YYYY HH:II'])
            ?>
        </div>   
        <div class="col-md-6 mb-0">
            <?php
            //  $model->end_date = \app\libraries\General::displayDate($model->end_datetime);
            echo $form->field($model, 'end_datetime')->textInput(['readonly' => true, 'class' => 'form-control custom_datetime_picker', 'maxlength' => true, 'placeholder' => 'DD-MM-YYYY HH:II'])
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 mb-0">
            <?= $form->field($model, 'reason')->textarea(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('reason')]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 mb-0">
            <?= $form->field($model, 'dateError')->hiddenInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('dateError')])->label(false); ?>
        </div>
    </div>

</div>
<div class="modal-footer">
    <button type="button" id="tutor_unavailability" class="btn btn-primary" style="float: right;">Submit</button>
    <!--<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>-->
    <!--<button class="btn btn-success" type="button">Save changes</button>-->
</div>
<?php ActiveForm::end(); ?>