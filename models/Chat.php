<?php

namespace app\models;


class Chat extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'chat';
    }

    public function rules() {
        return [];
    }

    public function attributeLabels() {
        return [
        ];
    }

    public function getStudent() {
        return $this->hasOne(Student::className(), ['student_uuid' => 'student_uuid']);
    }
    public function getTutor() {
        return $this->hasOne(Tutor::className(), ['uuid' => 'tutor_uuid']);
    }
}