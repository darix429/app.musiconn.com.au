<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
//rupal
use yii\helpers\ArrayHelper;
use app\models\InstrumentCategory;
use app\models\Instrument;
use yii\helpers\Url;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\Admin */
/* @var $form yii\widgets\ActiveForm */
$instrumentCategoryArray = ArrayHelper::map(InstrumentCategory::find()->all(),'uuid','name');
$instrumentArray = ArrayHelper::map(Instrument::find()->all(),'uuid','name');

?>

<?php

$this->registerJs(
   '
        $(document).on("click","#savebutton",function(){ 
            
            var file_data = $("#prerecordedvideo-file").prop("files")[0];  
            var form_data = new FormData($("form")[2]);              
            form_data.append("file", file_data);
            $(".msg").text("Uploading in progress..."); 
            $.ajax({
                    type: "POST",
                    url: $( "#upload_form" ).attr("action"),
                    data:  form_data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    beforeSubmit: function() {
                        blockBody();
                        $("#progress-bar").width("0%");
                    },
                    xhr: function () {
                            blockBody();
                            var xhr = new window.XMLHttpRequest();
                            xhr.upload.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                percentComplete = parseInt(percentComplete * 100);                       

                                $("#progress-bar").width(percentComplete + "%");
                                $("#progress-status").html(percentComplete + "%");
                            }
                        }, false);
                        return xhr;
                    }, 
                    success: function (result) { 

                        if(result.code == 200){ 
                            $(".msg").text(result.message);

                            if($("#progress-status").html() == "100%"){
                                setTimeout(function(){ 
                                    location.href = "' . Url::to(['video/uploaded']) . '";
                                }, 1000);
                            }
                        } else {
                            $("#uploadFormDiv").html(result);
                            $("#progress-bar").width("0%");
                        }
                        unblockBody();
                    }, error:function(e){
                        unblockBody();
                        $("#progress-bar").width("0%");
                        showErrorMessage(e.responseText);
                    }
                });
        });

        $(document).on("click",".upload-form",function(){  
        
            $( ".form-create" ).show();
            $("html,body").animate({
                 scrollTop: $(".form-open").offset().top},
                 "slow");
        });
        
        $(document).on("change",".upload",function(){ 
            var file_url = $("#prerecordedvideo-file").val().replace("fakepath","").replace("C:",""); 
            var file_name =  file_url.substr(2);
            document.getElementById("uploaded-video").innerHTML = file_name;
            
            var file_str = file_name.toString(); 
            var file_array = file_str.split("."); 
            var title = file_array[0];
            //var title = (empty(file_array[0])) ?  file_array[0] : "";
            $("#prerecordedvideo-title").val(title);
        });
    
',View::POS_END
);
?>

<div class="col-xl-12 col-lg-12 col-12 col-md-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Please Fill Information</h2>
            <div class="actions panel_actions float-right">
                <a href="<?= Yii::$app->getUrlManager()->createUrl(['/video/uploaded/'])?>" class="btn btn-info btn-icon "><i class="fa fa-arrow-left text-white"></i> <span>Back</span></a>
            </div>
        </header>
        <div class="content-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12">
                    <div id="uploadFormDiv">
                        <?=
                        $this->render('_form_inner', [
                            'model' => $model,
                        ])
                        ?>
                    </div>
			
                </div>
            </div>

        </div>
    </section>
</div>

