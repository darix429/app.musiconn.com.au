<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LoginAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'theme_assets/plugins/pace/pace-theme-flash.css',        
        'theme_assets/plugins/bootstrap/css/bootstrap.min.css',        
        'theme_assets/fonts/font-awesome/css/font-awesome.css',
        'theme_assets/css/animate.min.css',
        'theme_assets/plugins/perfect-scrollbar/perfect-scrollbar.css',
        'theme_assets/plugins/messenger/css/messenger.css',
        'theme_assets/plugins/messenger/css/messenger-theme-future.css',
        'theme_assets/plugins/messenger/css/messenger-theme-flat.css',
        
        
//        'theme_assets/plugins/icheck/skins/square/orange.css',
        'theme_assets/plugins/icheck/skins/all.css',
        'theme_assets/css/style.css',
        'theme_assets/css/responsive.css',
        'theme_assets/css/custom.css',
    ];
    public $js = [
//        'theme_assets/js/jquery-3.2.1.min.js',
        'theme_assets/js/popper.min.js',
        'theme_assets/plugins/bootstrap/js/bootstrap.min.js',
        'theme_assets/plugins/pace/pace.min.js',
        'theme_assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js',
        'theme_assets/plugins/viewport/viewportchecker.js',
        'theme_assets/plugins/icheck/icheck.min.js',
//        'theme_assets/plugins/messenger/js/messenger.min.js',
//        'theme_assets/plugins/messenger/js/messenger-theme-future.js',
//        'theme_assets/plugins/messenger/js/messenger-theme-flat.js',
//        'theme_assets/js/messenger.js',
//        'theme_assets/js/scripts.js',
        'theme_assets/plugins/sparkline-chart/jquery.sparkline.min.js',
        'theme_assets/js/chart-sparkline.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
