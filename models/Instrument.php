<?php

namespace app\models;
use Yii;
//rupal
use app\models\PreRecordedVideo;
/**
 * This is the model class for table "instrument".
 *
 * @property string $uuid
 * @property string $name
 * @property string $description
 * @property resource $image
 */
class Instrument extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'instrument';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
                //html tag filter
                [['name', 'description',], 'match', 'pattern' => \app\libraries\General::validTextPattern(), 'message' => 'Invalid charecters used.'], 
            
                [['uuid', 'name', 'description', 'status'], 'string'],
                [['name', 'description', 'image'], 'required', 'on' => 'create'],
                [['name', 'description'], 'required', 'on' => 'update'],
                [['uuid'], 'unique'],
                [['image'], 'file'],
            //['image', 'image', 'minWidth' => 250, 'maxWidth' => 300,'minHeight' => 200, 'maxHeight' => 250],
                ['image', 'image', 'maxWidth' => 550, 'maxHeight' => 550],
                [['created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'image' => Yii::t('app', 'Image'),
        ];
    }
    
    public function InstrumentDelete($id)
    {
        //print_r($id); exit;
        $uuid = PreRecordedVideo::find()->where(['instrument_uuid' => $id])->one();
        $instrumentId = $uuid['instrument_uuid'];
        
        if($instrumentId !== null){
            return $id;
        } 
        else{
            return false;
        }
    }

    public function getInstrumentUsingUuid($uuid)
    {
        return Instrument::find()->where(['uuid' => $uuid])->one();
    }
}
