<?php

use yii\helpers\Html;
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear Musiconn Admin,</p>
            
            <p>This message is to confirm <?= $content['student_id']; ?> <?= $content['student_name'].' '.$content['student_last_name']; ?>'s purchase of online content subscription with Musiconn Pty Ltd.  Below are details of your purchase:</p>
            
            <p><strong>Transaction ID:</strong> #<?= $content['transaction_id']; ?></p>
            <p><strong>Transaction Date:</strong> <?= $content['order_date']; ?></p>
            <p><strong>Recurring Payment:</strong> <?= $content['recurring_payment']; ?></p>
            <p><strong>Type of Purchase:</strong> Online Contents Subscription</p>
            <p><strong>Discount Allowed:</strong> $0</p>
            <p><strong>Amount Paid (Includes GST):</strong> $<?= $content['total_price']; ?></p>
            <p><strong>GST paid on purchase:</strong> $<?= $content['tax']; ?></p>
            
            <p>Please do not reply to this unattended mailbox.  Any queries or feedback can be directed to admin@musiconn.com.au, or call us on 1300 068 742.</p>

            <p>Regards,<br>
            Musiconn Booking System</p>
        </td>
    </tr>
</table>

