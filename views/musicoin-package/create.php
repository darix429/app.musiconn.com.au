<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MusicoinPackage */

$this->title = Yii::t('app', 'Create Musicoin Package');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Musicoin Packages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?= $this->render('_form', [
        'model' => $model,
    ]) ?>