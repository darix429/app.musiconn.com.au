<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\PremiumStandardPlanRate;
?>

<div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">

    <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Booking ID</th>
                <th>Student Name</th>
                <th>Tutor Name</th>
                <th>Instrument</th>
                <th>Order ID</th>
<!--                <th>Subscription Name</th>
                <th>Tutorial Type Code</th>
                <th>Tutorial Min</th>
                <th>Payment Term Code</th>
                <th>Total Session</th>
                <th>Session Length Day</th>
                <th>Price</th>
                <th>GST</th>
                <th>Total Price</th>
                <th>Subscription Price</th>-->
                <th>Tutorial Name</th>
                <th>Discount</th>
                <th>Grand Total</th>
                <th>Booking Date</th>
                <th>NSW Creative Kids Voucher Code</th>
                <th>Plan</th>
                <th>Action</th>
            </tr>
        </thead>

        <tbody>
            <?php
            if (!empty($tuitionBookingList)) {
                foreach ($tuitionBookingList as $key => $value) {  //echo "<pre>"; print_r($value); exit;
                $student_name = $value['studentUu']['first_name']. ' ' . $value['studentUu']['last_name'];
                $tutor_name = $value['tutorUu']['first_name']. ' ' .$value['tutorUu']['last_name']; 
                
                $plan_model = PremiumStandardPlanRate::find()->joinWith(['onlineTutorialPackageTypeUu'])->where(['premium_standard_plan_rate.uuid'=>$value['premium_standard_plan_rate_uuid']])->asArray()->one();

                    ?>
                    <tr>
                        <td><?= $key + 1; ?></td>
                        <td><?= "#".$value['booking_id'] ?></td>
                        <td><?= (!empty($student_name)) ? $student_name : ''; ?></td>
                        <td><?= (!empty($tutor_name)) ? $tutor_name : ''; ?></td>
                        <td><?= (!empty($value['instrumentUu']['name'])) ? $value['instrumentUu']['name'] : ''; ?></td>
                        <td><?= (!empty($value['orderUu']['order_id'])) ? "#".$value['orderUu']['order_id'] : ''; ?></td>
<!--                        <td><?= (!empty($value['studentMonthlySubscriptionUu']['name'])) ? $value['studentMonthlySubscriptionUu']['name'] : ''; ?></td>
                        <td><?= $value['tutorial_type_code'] ?></td>
                        <td><?= $value['tutorial_min'] ?></td>
                        <td><?= $value['payment_term_code'] ?></td>
                        <td><?= $value['total_session'] ?></td>
                        <td><?= $value['session_length_day'] ?></td>
                        <td><?= $value['price'] ?></td>
                        <td><?= $value['gst'] ?></td>
                        <td><?= $value['total_price'] ?></td>
                        <td><?= $value['monthly_subscription_price'] ?></td>-->
                        <td><?= $value['tutorial_name'] ?></td>
                        <td><?= $value['discount'] ?></td>
                        <td><?= $value['grand_total'] ?></td>
                        <td><?= \app\libraries\General::displayDate($value['booking_datetime']); ?></td>
                        <td><?= (!empty($value['creative_kids_voucher_code'])) ? "#".trim($value['creative_kids_voucher_code'],"#") : ""; ?></td>
                        <td><?= strtoupper($plan_model['onlineTutorialPackageTypeUu']['display_name']) ?></td>
                        <td>
                            <a href="<?= Url::to(['/book-music-tution/view/', 'id' => $value['uuid']]) ?>" class="badge badge-info badge-md" title="View"><i class="fa fa-eye"></i></a>
                        </td>
                        
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>

</div>
