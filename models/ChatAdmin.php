<?php

namespace app\models;


class ChatAdmin extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'chat_admin';
    }

    public function rules() {
        return [];
    }

    public function attributeLabels() {
        return [
        ];
    }

    public function getSender($class, $col) {
        return $this->hasOne($class::className(), [$col=>'sender_uuid']);
    }
}