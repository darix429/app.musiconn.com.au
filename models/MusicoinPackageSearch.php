<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MusicoinPackage;

/**
 * MusicoinPackageSearch represents the model behind the search form of `app\models\MusicoinPackage`.
 */
class MusicoinPackageSearch extends MusicoinPackage
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'name', 'description', 'gst_description', 'status', 'created_at', 'updated_at'], 'safe'],
            [['price', 'gst', 'total_price', 'coins', 'bonus_coins', 'total_coins'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $modeDatatable = false, $statusnot = '')
    {
        $query = MusicoinPackage::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        if (!empty($statusnot)) {
            if (is_array($statusnot)) {

                $query->andWhere(['not in', 'status', $statusnot]);
            } else {

                $query->andWhere(['<>', 'status', $statusnot]);
            }
        }
	$query->andFilterWhere(['status' => $this->status]);

        // grid filtering conditions
        $query->andFilterWhere([
//            'price' => $this->price,
//            'gst' => $this->gst,
//            'total_price' => $this->total_price,
            'coins' => $this->coins,
            'bonus_coins' => $this->bonus_coins,
            'total_coins' => $this->total_coins,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'uuid', $this->uuid])
            ->andFilterWhere(['ilike', 'name', $this->name])
            ->andFilterWhere(['ilike', 'CAST(p.price AS TEXT)', $this->price])
            ->andFilterWhere(['ilike', 'CAST(p.gst AS TEXT)', $this->gst])
            ->andFilterWhere(['ilike', 'CAST(p.total_price AS TEXT)', $this->total_price])
            ->andFilterWhere(['ilike', 'description', $this->description])
            ->andFilterWhere(['ilike', 'gst_description', $this->gst_description])
            ->andFilterWhere(['ilike', 'status', $this->status]);

        if ($modeDatatable) {
            $result = $query->orderBy('created_at desc')->all();
            return $result;
        }
        return $dataProvider;
    }
}
