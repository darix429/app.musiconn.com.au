<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\StudentTuitionBooking;

/**
 * StudentTuitionBookingSearch represents the model behind the search form of `app\models\StudentTuitionBooking`.
 */
class StudentTuitionBookingSearch extends StudentTuitionBooking
{
    public $student_name, $tutor_name, $order_id, $coupon_code, $coupon_code_type;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'booking_id', 'booking_datetime', 'student_uuid', 'tutor_uuid', 'instrument_uuid', 'tutorial_name', 'tutorial_description', 'tutorial_type_code', 'payment_term_code', 'gst_description', 'status', 'created_at', 'student_monthly_subscription_uuid', 'order_uuid', 'student_name', 'tutor_name', 'order_id', 'coupon_uuid', 'coupon_code', 'coupon_code_type'], 'safe'],
            [['tutorial_min', 'total_session', 'session_length_day'], 'integer'],
            [['price', 'gst', 'total_price', 'monthly_subscription_price', 'discount', 'grand_total'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $modeDatatable = false, $statusnot = '',$booking_by_promocode = '')
    {
        $query = StudentTuitionBooking::find()->joinWith(['studentUu', 'tutorUu', 'instrumentUu', 'studentMonthlySubscriptionUu', 'orderUu','couponUu'])->andWhere(['student_tuition_booking.status' => 'BOOKED']);
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        if (!empty($statusnot)) {
            if (is_array($statusnot)) {

                $query->andWhere(['not in', 'status', $statusnot]);
            } else {

                $query->andWhere(['<>', 'status', $statusnot]);
            }
        }
	$query->andFilterWhere(['status' => $this->status]);
        
        if (Yii::$app->user->identity->role == 'STUDENT') { 
            $query->andFilterWhere(['student_tuition_booking.student_uuid' => Yii::$app->user->identity->student_uuid]);
            //$query->andFilterWhere(['order.student_uuid' => Yii::$app->user->identity->student_uuid]);
        }else{
            $query->andFilterWhere(['ilike', "CONCAT(student.first_name,' ',student.last_name)", $this->student_name]);
        }
        
        $query->andFilterWhere(['ilike', "CONCAT(tutor.first_name,' ',tutor.last_name)", $this->tutor_name]);

        if($booking_by_promocode == 'booking_by_promocode'){
            $query->andWhere('coupon_uuid IS NOT NULL');
        }
        // grid filtering conditions
        $query->andFilterWhere([
            "to_char(booking_datetime, 'DD-MM-YYYY')" => $this->booking_datetime,
            'tutorial_min' => $this->tutorial_min,
            'total_session' => $this->total_session,
            'session_length_day' => $this->session_length_day,
            'price' => $this->price,
            'gst' => $this->gst,
            'total_price' => $this->total_price,
            'created_at' => $this->created_at,
            'monthly_subscription_price' => $this->monthly_subscription_price,
            'coupon_uuid' => $this->coupon_uuid,
            'order.order_id' => trim($this->order_id,"#"),
            'booking_id' => trim($this->booking_id,"#"),
            'coupon.code' => $this->coupon_code,
            'coupon.code_type' => $this->coupon_code_type,
            
        ]);

        $query->andFilterWhere(['ilike', 'uuid', $this->uuid])
            //->andFilterWhere(['ilike', 'booking_id', $this->booking_id])
            //->andFilterWhere(['ilike', 'student_uuid', $this->student_uuid])
            //->andFilterWhere(['ilike', 'tutor_uuid', $this->tutor_uuid])
            ->andFilterWhere(['ilike', 'CAST(instrument_uuid AS TEXT)', $this->instrument_uuid])
            ->andFilterWhere(['ilike', 'tutorial_name', $this->tutorial_name])
            ->andFilterWhere(['ilike', 'tutorial_description', $this->tutorial_description])
            ->andFilterWhere(['ilike', 'tutorial_type_code', $this->tutorial_type_code])
            ->andFilterWhere(['ilike', 'payment_term_code', $this->payment_term_code])
            ->andFilterWhere(['ilike', 'gst_description', $this->gst_description])
            ->andFilterWhere(['ilike', 'status', $this->status])
            ->andFilterWhere(['ilike', 'student_monthly_subscription_uuid', $this->student_monthly_subscription_uuid])
            //->andFilterWhere(['ilike', 'order_uuid', $this->order_uuid]);
            //->andFilterWhere(['ilike', 'order.order_id', $this->order_id])
            ->andFilterWhere(['ilike', 'CAST(student_tuition_booking.discount AS TEXT)', $this->discount])
            ->andFilterWhere(['ilike', 'CAST(student_tuition_booking.grand_total AS TEXT)', $this->grand_total]);
        
//echo $query->createCommand()->getRawSql(); exit;
        if ($modeDatatable) {
            $result = $query->asArray()->orderBy('booking_datetime DESC')->all();
                        
           // echo "<pre>"; print_r($result); 
            return $result;
        }
        return $dataProvider;
    }
    
    
    public function searchSalesCommissions($params, $modeDatatable = false, $statusnot = '',$booking_by_promocode = '')
    {
        $query = StudentTuitionBooking::find()->joinWith(['studentUu', 'tutorUu', 'instrumentUu', 'studentMonthlySubscriptionUu', 'orderUu','couponUu'])->andWhere(['student_tuition_booking.status' => 'BOOKED']);
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        if (!empty($statusnot)) {
            if (is_array($statusnot)) {

                $query->andWhere(['not in', 'status', $statusnot]);
            } else {

                $query->andWhere(['<>', 'status', $statusnot]);
            }
        }
	$query->andFilterWhere(['status' => $this->status]);
        
        if (Yii::$app->user->identity->role == 'STUDENT') { 
            $query->andFilterWhere(['student_tuition_booking.student_uuid' => Yii::$app->user->identity->student_uuid]);
        }else{
            $query->andFilterWhere(['ilike', "CONCAT(student.first_name,' ',student.last_name)", $this->student_name]);
        }
        
        $query->andFilterWhere(['ilike', "CONCAT(tutor.first_name,' ',tutor.last_name)", $this->tutor_name]);

        if($booking_by_promocode == 'booking_by_promocode'){
            $query->andWhere(' "coupon_uuid" IS NOT NULL ');
        }
        
        if (!is_null($this->booking_datetime) && 
            strpos($this->booking_datetime, ' - ') !== false ) {
            list($start_date, $end_date) = explode(' - ', $this->booking_datetime);
            $query->andFilterWhere(['>=', '"student_tuition_booking"."booking_datetime"', date('Y-m-d 00:00:00', strtotime($start_date))]);
            $query->andFilterWhere(['<=', '"student_tuition_booking"."booking_datetime"', date('Y-m-d 23:59:59', strtotime($end_date))]);
//            $query->andFilterWhere(['>=', "to_char(\"student_tuition_booking"."booking_datetime\", 'DD-MM-YYYY')", $start_date]);
//            $query->andFilterWhere(['<=', "to_char(\"student_tuition_booking"."booking_datetime\", 'DD-MM-YYYY')", $end_date]);
        }
        
        // grid filtering conditions
        $query->andFilterWhere([
            //"to_char(booking_datetime, 'DD-MM-YYYY')" => $this->booking_datetime,
            'tutorial_min' => $this->tutorial_min,
            'total_session' => $this->total_session,
            'session_length_day' => $this->session_length_day,
            'price' => $this->price,
            'gst' => $this->gst,
            'total_price' => $this->total_price,
            'created_at' => $this->created_at,
            'monthly_subscription_price' => $this->monthly_subscription_price,
            'coupon_uuid' => $this->coupon_uuid,
            'order.order_id' => trim($this->order_id,"#"),
            'booking_id' => trim($this->booking_id,"#"),
            'coupon.code' => $this->coupon_code,
            'coupon.code_type' => $this->coupon_code_type,
            
        ]);

        $query->andFilterWhere(['ilike', 'uuid', $this->uuid])
            ->andFilterWhere(['ilike', 'CAST(instrument_uuid AS TEXT)', $this->instrument_uuid])
            ->andFilterWhere(['ilike', 'tutorial_name', $this->tutorial_name])
            ->andFilterWhere(['ilike', 'tutorial_description', $this->tutorial_description])
            ->andFilterWhere(['ilike', 'tutorial_type_code', $this->tutorial_type_code])
            ->andFilterWhere(['ilike', 'payment_term_code', $this->payment_term_code])
            ->andFilterWhere(['ilike', 'gst_description', $this->gst_description])
            ->andFilterWhere(['ilike', 'status', $this->status])
            ->andFilterWhere(['ilike', 'student_monthly_subscription_uuid', $this->student_monthly_subscription_uuid])
            ->andFilterWhere(['ilike', 'CAST(student_tuition_booking.discount AS TEXT)', $this->discount])
            ->andFilterWhere(['ilike', 'CAST(student_tuition_booking.grand_total AS TEXT)', $this->grand_total]);
        
//echo $query->createCommand()->getRawSql(); exit;
        if ($modeDatatable) {
            $result = $query->asArray()->orderBy('booking_datetime DESC')->all();
                        
           // echo "<pre>"; print_r($result); 
            return $result;
        }
        return $dataProvider;
    }
}
