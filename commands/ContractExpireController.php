<?php

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
//rupal
use app\models\Tutor;
use app\models\User;
use app\libraries\General;
use Yii;

class ContractExpireController extends Controller
{

    public function actionIndex($message = 'ContractExpire cron Index')
    {
        echo $message . "\n";

        return ExitCode::OK;
    }

    /******contract expire notification ******/
    public function actionNotification() {

        $currDate = date("Y-m-d H:i:s");
        $expire_date_after = date('Y-m-d', strtotime('+10 days', strtotime($currDate)));
        $tutorlist = Tutor::find()->where(["to_char(contract_expiry, 'YYYY-MM-DD')" => $expire_date_after, 'status' => 'ENABLED'])->all();
        if(!empty($tutorlist)){
		foreach($tutorlist as $key => $value){

        $student = User::find()->where(['user.tutor_uuid' => $value->uuid])->one();
		    $tutorDetails = array(
		        'first_name' => $value->first_name,
		        'last_name' => $value->last_name,
		        'contract_expiry' => General::convertTimezone($value->contract_expiry, Yii::$app->params['timezone'], $student->timezone,'Y-m-d H:i:s'),
		        'tutor_email' => $value->email,
		    );
        //php /var/www/html/musiconn/yii contract-expire/notification
		    Tutor::contractExpireMail($tutorDetails);
		}
	}
    }

    /******child certificate expire notification ******/
    public function actionChildCertificateNotification() {

        $currDate = date("Y-m-d H:i:s");
        $expire_date_after = date('Y-m-d', strtotime('+10 days', strtotime($currDate)));
        $tutorlist = Tutor::find()->where(["to_char(child_certi_expiry, 'YYYY-MM-DD')" => $expire_date_after, 'status' => 'ENABLED'])->all();
        if(!empty($tutorlist)){
            foreach($tutorlist as $key => $value){

                $student = User::find()->where(['user.tutor_uuid' => $value->uuid])->one();
                $tutorDetails = array(
                    'first_name' => $value->first_name,
                    'last_name' => $value->last_name,
                    'child_certificate_expiry' => General::convertTimezone($value->child_certi_expiry, Yii::$app->params['timezone'], $student->timezone,'Y-m-d H:i:s'),
                    'tutor_email' => $value->email,
                );

                Tutor::childCertificateExpireMail($tutorDetails);
            }
        }
    }


}
