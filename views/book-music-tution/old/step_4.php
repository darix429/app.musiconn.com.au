<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Url;
use app\models\Instrument;
use app\models\OnlineTutorialPackage;
use app\models\StudentTuitionBooking;
use app\models\Tutor;
use app\models\User;
use app\libraries\General;

$this->title = Yii::t('app', 'Book Music Lessons');
$this->params['breadcrumbs'][] = $this->title;

if (!empty($step_data['step_1']['instrument_uuid'])) {
    $instrumentDetail = Instrument::find()->where(['uuid' => $step_data['step_1']['instrument_uuid']])->one();
} else {
    return \yii\web\Controller::redirect(['index']);
}

$planDetail = OnlineTutorialPackage::find()->where(['uuid' => $step_data['step_2']['onlinetutorial_uuid']])->one();
$package = OnlineTutorialPackage::find()->joinWith(['paymentTermUu', 'tutorialTypeUu'])->where(['online_tutorial_package.uuid' => $step_data['step_2']['onlinetutorial_uuid']])->asArray()->one();
$session_minute = $package['tutorialTypeUu']['min'];
//$start_hour_array = StudentTuitionBooking::start_hour_array($step_data['step_2']['onlinetutorial_uuid']);
// $start_hour_array = StudentTuitionBooking::hoursRange();
$start_hour_array = [];

$tutor_uuid = $step_data['step_3']['tutor_uuid'];
$tutorDetail = Tutor::findOne($tutor_uuid);
$tutor_working_plan = json_decode($tutorDetail->working_plan, true);
$tutor_user_model = User::find()->where(['tutor_uuid' => $tutor_uuid])->one();
$student_user_model = User::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid])->one();
$tutor_timezone = (!empty($tutor_user_model)) ? $tutor_user_model->timezone : Yii::$app->params['timezone'];
$user_timezone = (!empty($student_user_model)) ? $student_user_model->timezone : Yii::$app->params['timezone'];
$this->registerJs(
        '
$( "#frm_step_4,#gostep1,#gostep2,#gostep3" ).submit(function( event ) {
        blockBody();
});

$("document").ready(function(){
    $(".btn-secondary").removeAttr("onclick");
    $("#studenttuitionbooking-start_hour").css({"display":"none"});
});

function load_daywise_session(dy){

    if(dy !== undefined){
    $.ajax({
            type: "GET",
            url: "' . Url::to(['book-music-tution/get-daywise-session']) . '",
            data: {"day":dy},
            beforeSend: function() {
                blockBody();
            },
            success:function(result){
                //showSuccess(result.message);
                $("#session_list_test").html(result);
                ULTRA_SETTINGS.iCheck();
                unblockBody();
            },error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
    }
}

function load_daywise_changes(dy,tm = "00:00",whendate = ""){
    //if(dy !== undefined){
    if(whendate !== ""){
    $.ajax({
            type: "GET",
            url: "' . Url::to(['book-music-tution/get-day-changes']) . '",
            data: {"day":dy,"selected_time":tm,"selected_when_date":whendate},
            beforeSend: function() {
                blockBody();
            },
            success:function(result){

                $("#studenttuitionbooking-session_when_start").datepicker("remove");


                if(result.code == 200){
                    $("#studenttuitionbooking-start_hour").css({"display":"inline"});
                    $("#studenttuitionbooking-session_when_start").val(result.session.session_when_start);

                    if(result.message != "success"){
                        showErrorMessage(result.message);
                    }

                }else{
                    $("#studenttuitionbooking-session_when_start").val(result.session_when_start);
                    // showErrorMessage(result.message);
                }
                if(result.all_dates != ""){
                    $(".next_btn").show();
                }else{
                    $(".next_btn").hide();
                }


                $("#session_text_info").html(result.session_text_info);
                $("#studenttuitionbooking-session_text_info").val(result.session_text_info);
                $("#studenttuitionbooking-all_dates").val(result.all_dates);

                $("#studenttuitionbooking-session_when_start").datepicker({startDate:"'.General::displayDate(date('Y-m-d')).'",daysOfWeekDisabled: result.disabled_day });
                ULTRA_SETTINGS.iCheck();
                unblockBody();
            },error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
    }
}

function get_day_start_time(dy){
    if(dy !== undefined){
    $.ajax({
            type: "GET",
            url: "' . Url::to(['book-music-tution/get-day-start-time']) . '",
            data: {"day":dy},
            async:false,
            success:function(result){
                $("#studenttuitionbooking-session_when_start").datepicker("remove");
               $("#studenttuitionbooking-start_hour").val(result.time);
               $("#studenttuitionbooking-session_when_start").val(result.session_when_start);
               $("#studenttuitionbooking-session_when_start").datepicker({startDate:result.session_when_start,daysOfWeekDisabled: result.disabled_day,format:"dd-mm-yyyy" });
               ULTRA_SETTINGS.iCheck();
            },error:function(e){
                showErrorMessage(e.responseText);
            }
        });
    }
}


function get_daywise_time(dy,date,typ){
    if(dy !== undefined){
    // $("#studenttuitionbooking-start_hour").css({"display":"inline"});
    $.ajax({
            type: "GET",
            url: "' . Url::to(['book-music-tution/get-tutor-available-hours']) . '?tutor_uuid='.$tutor_uuid.'&session_minute='.$session_minute.'",
            data: {"day":dy,"date":date,"typ":typ},
            async:false,
            success:function(result){
                if(result.time == ""){
                    $("#studenttuitionbooking-start_hour").css({"display":"none"});
                }
                $("#studenttuitionbooking-session_when_start").datepicker("remove");
                $("#studenttuitionbooking-start_hour").html("");
                $.each(result.time, function (key, value) {

                    $("#studenttuitionbooking-start_hour").append("<option value=\'"+key+"\'>"+value+"</option>");
                });
                $("#studenttuitionbooking-session_when_start").val(result.session_when_start);
                // $("#studenttuitionbooking-session_when_start").datepicker({startDate:result.session_when_start,daysOfWeekDisabled: result.disabled_day,datesOfWeekDisabled: result.disabled_date,format:"dd-mm-yyyy" });
                $("#studenttuitionbooking-session_when_start").datepicker({startDate:date,daysOfWeekDisabled: result.disabled_day,datesOfWeekDisabled: result.disabled_date,format:"dd-mm-yyyy" });
                ULTRA_SETTINGS.iCheck();
            },error:function(e){
                showErrorMessage(e.responseText);
            }
        });
    }
}

function week_day_change(dy){
    // get_day_start_time(dy);
    var date = $("#studenttuitionbooking-session_when_start").val();
    get_daywise_time(dy,date,"day");
    var tm = $("#studenttuitionbooking-start_hour").val();
    var whendate = $("#studenttuitionbooking-session_when_start").val();
    load_daywise_changes(dy,tm,whendate);
}

function hour_change(){
    var dayvalue = $("input[name=\"StudentTuitionBooking[session_week_day]\"]:checked").val();
    var tm = $("#studenttuitionbooking-start_hour").val();
    var whendate = $("#studenttuitionbooking-session_when_start").val();

    load_daywise_changes(dayvalue,tm,whendate);
}

var send=true;
function date_change(obj){
    var weekday=new Array(7);
    weekday[0]="monday";
    weekday[1]="tuesday";
    weekday[2]="wednesday";
    weekday[3]="thursday";
    weekday[4]="friday";
    weekday[5]="saturday";
    weekday[6]="sunday";

    if(send){

    var date = $(obj).datepicker("getDate");
    var dayOfWeek = weekday[date.getUTCDay()];

    var w_str = "week_day_"+dayOfWeek;
    $("."+w_str).prop("checked", true);
    $("."+w_str).parent("label").addClass("active");

    var dayvalue = $("input[name=\"StudentTuitionBooking[session_week_day]\"]:checked").val();

    var tm = $("#studenttuitionbooking-start_hour").val();
    var date_new = convert(date);
    var whendate = $("#studenttuitionbooking-session_when_start").val();

    send=false;
    get_daywise_time(dayOfWeek,date_new,"date");
    load_daywise_changes(dayOfWeek,tm,whendate);

    }
    setTimeout(function(){send=true;},200);
}

function convert(str) {
    var date = new Date(str),
        mnth = ("0" + (date.getMonth()+1)).slice(-2),
        day  = ("0" + date.getDate()).slice(-2);
    return [day, mnth, date.getFullYear()].join("-");
}


        ', View::POS_END
);
?>

<style>
   .datepicker td.day.disabled {
    color: #bababa;
}
</style>
<div class="col-xl-12">
    <p class="bg-warning text-dark">For issues concerning tutor availability, please contact <a href="mailto:admin@musiconn.com.au">admin@muscionn.com.au</a> for assistance.</label>
    <nav class="navbar navbar-expand-lg navbar-light bg-primary text-light" style="">
        <h2 class="booking-nav-title float-left text-white" style="">Select Instrument &nbsp;<i class="fa fa-check"></i> </h2>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#change-step1" aria-controls="change-step1" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="change-step1">
            <?php echo Html::beginForm(['index'], 'post', ['id' => 'gostep1', 'class' => 'form-inline d-block']) ?>
            <input type="hidden" name="step" value="1">
            <input type="hidden" name="prev" value="1">
            <a href="javascript:$('#gostep1').submit();" class="btn btn-outline-success pull-right">
                Change
            </a>
            <p class="pull-right"> <?= $instrumentDetail->name; ?>&nbsp;</p>
            <?php echo Html::endForm() ?>

        </div>
    </nav>

    <div class="spacer"></div>

    <nav class="navbar navbar-expand-lg navbar-light bg-primary text-light" style="">
        <h2 class="booking-nav-title float-left text-white" style="">Select Plan &nbsp;<i class="fa fa-check"></i> </h2>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#change-step2" aria-controls="change-step2" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="change-step2">
            <?php echo Html::beginForm(['index'], 'post', ['id' => 'gostep2', 'class' => 'form-inline d-block']) ?>
            <input type="hidden" name="step" value="2">
            <input type="hidden" name="prev" value="2">
            <a href="javascript:$('#gostep2').submit();" class="btn btn-outline-success pull-right">
                Change
            </a>
            <p class="pull-right"> <?= $planDetail->name . " Total : $" . $planDetail->total_price; ?>&nbsp;</p>
            <?php echo Html::endForm() ?>

        </div>
    </nav>

    <div class="spacer"></div>

    <nav class="navbar navbar-expand-lg navbar-light bg-primary text-light" style="">
        <h2 class="booking-nav-title float-left text-white" style="">Select Tutor &nbsp;<i class="fa fa-check"></i> </h2>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#change-step3" aria-controls="change-step3" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="change-step3">
            <?php echo Html::beginForm(['index'], 'post', ['id' => 'gostep3', 'class' => 'form-inline d-block']) ?>
            <input type="hidden" name="step" value="3">
            <input type="hidden" name="prev" value="3">
            <a href="javascript:$('#gostep3').submit();" class="btn btn-outline-success pull-right">
                Change
            </a>
            <p class="pull-right"> <?= $tutorDetail->first_name . ' ' . $tutorDetail->last_name; ?>&nbsp;</p>
            <?php echo Html::endForm() ?>

        </div>
    </nav>

    <section class="box ">
        <header class="panel_header bg-primary ">
            <h2 class="title float-left text-white">Select Date</h2>
            <div class="actions panel_actions float-right">
                <a class="btn tutor_availability_calender_btn" style="color: #ffffff;border: 1px solid #ffffff; background: transparent; padding: 5px 8px;" data-id="<?= $tutorDetail->uuid; ?>" data-tutorname="<?= $tutorDetail->first_name . ' ' . $tutorDetail->last_name; ?>" title="Avaibility"><i class="fa fa-calendar text-white"></i></a>
            </div>
        </header>
        <div class="content-body padding-imp-30">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12">
                    <?php
                    $form = ActiveForm::begin([
                                'action' => ['index'],
                                'id' => 'frm_step_4',
                                'enableClientValidation' => true,
                                //'enableAjaxValidation' => true,
                                'validateOnChange' => true,
                                'options' => ['class' => 'form-horizontal']]);
                    ?>

                    <input type="hidden" name="step" value="4">
                    <?= $form->field($model, 'all_dates')->hiddenInput(['class' => 'form-control', 'maxlength' => true,])->label(false) ?>
                    <?= $form->field($model, 'session_text_info')->hiddenInput(['class' => 'form-control', ])->label(false) ?>

                    <div class="my-inline-button" >
                        <div class=" text-center" >
                            <ul class="my-inline-button-filter list-inline btn-group btn-group-toggle " data-toggle="buttons">
                                <li>
                                    <label class="btn <?= ($tutor_working_plan['sunday'] == '') ? "btn-secondary" : "btn-primary" ?> <?= ($model->session_week_day == 'sunday') ? "active" : "" ?>" onclick="week_day_change('sunday')" >
                                        <input type="radio" name="StudentTuitionBooking[session_week_day]" class="session_week_day week_day_sunday" autocomplete="off" value="sunday" <?= ($model->session_week_day == 'sunday') ? "checked" : "" ?> >Sun
                                    </label>
                                </li>
                                <li>
                                    <label class="btn <?= ($tutor_working_plan['monday'] == '') ? "btn-secondary" : "btn-primary" ?> <?= ($model->session_week_day == 'monday') ? "active" : "" ?>" onclick="week_day_change('monday')">
                                        <input type="radio" name="StudentTuitionBooking[session_week_day]" class="session_week_day week_day_monday" autocomplete="off" value="monday" <?= ($model->session_week_day == 'monday') ? "checked" : "" ?> >Mon
                                    </label>
                                </li>
                                <li>
                                    <label class="btn <?= ($tutor_working_plan['tuesday'] == '') ? "btn-secondary" : "btn-primary" ?> <?= ($model->session_week_day == 'tuesday') ? "active" : "" ?>" onclick="week_day_change('tuesday')">
                                        <input type="radio" name="StudentTuitionBooking[session_week_day]" class="session_week_day week_day_tuesday" autocomplete="off" value="tuesday" <?= ($model->session_week_day == 'tuesday') ? "checked" : "" ?> >Tue
                                    </label>
                                </li>
                                <li>
                                    <label class="btn <?= ($tutor_working_plan['wednesday'] == '') ? "btn-secondary" : "btn-primary" ?> <?= ($model->session_week_day == 'wednesday') ? "active" : "" ?>" onclick="week_day_change('wednesday')">
                                        <input type="radio" name="StudentTuitionBooking[session_week_day]" class="session_week_day week_day_wednesday" autocomplete="off" value="wednesday" <?= ($model->session_week_day == 'wednesday') ? "checked" : "" ?> >Wed
                                    </label>
                                </li>
                                <li>
                                    <label class="btn <?= ($tutor_working_plan['thursday'] == '') ? "btn-secondary" : "btn-primary" ?> <?= ($model->session_week_day == 'thursday') ? "active" : "" ?>" onclick="week_day_change('thursday')">
                                        <input type="radio" name="StudentTuitionBooking[session_week_day]"  class="session_week_day week_day_thursday" autocomplete="off" value="thursday" <?= ($model->session_week_day == 'thursday') ? "checked" : "" ?> >Thu
                                    </label>
                                </li>
                                <li>
                                    <label class="btn <?= ($tutor_working_plan['friday'] == '') ? "btn-secondary" : "btn-primary" ?> <?= ($model->session_week_day == 'friday') ? "active" : "" ?>" onclick="week_day_change('friday')">
                                        <input type="radio" name="StudentTuitionBooking[session_week_day]" class="session_week_day week_day_friday" autocomplete="off" value="friday" <?= ($model->session_week_day == 'friday') ? "checked" : "" ?> >Fri
                                    </label>
                                </li>
                                <li>
                                    <label class="btn <?= ($tutor_working_plan['saturday'] == '') ? "btn-secondary" : "btn-primary" ?> <?= ($model->session_week_day == 'saturday') ? "active" : "" ?>" onclick="week_day_change('saturday')">
                                        <input type="radio" name="StudentTuitionBooking[session_week_day]" class="session_week_day week_day_saturday" autocomplete="off" value="saturday" <?= ($model->session_week_day == 'saturday') ? "checked" : "" ?> >Sat
                                    </label>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-2"></div>
                        <div class="col-lg-8 col-md-8 col-8 booking_search_data">
                            <div class="tab-pane fade show active " >

                                <div id="session_list_parent" class="text-centre">
                                    <div class="card " >
                                        <div class="card-body">

                                            <div class="form-row align-items-center">
                                                <div class="col-lg-2 col-md-2 col-2"></div>
                                                <div class="col-auto">
                                                    <label class="" style="margin-top: -10px;vertical-align: top;" for="inlineFormInput">When</label>
                                                </div>
                                                <div class="col-auto">
                                                    <?php $model->session_when_start = General::displayDate($model->session_when_start); ?>
                                                    <?=
                                                    $form->field($model, 'session_when_start', ['template' => "{label}\n{input}\n{hint}\n{error}",
                                                        'labelOptions' => ['class' => 'control-label']])->textInput(
                                                            ['class' => 'form-control when-datepicker', 'data-date-format' =>'dd-mm-yyyy','data-disabled-days' => '','data-start-date' => '0d','onchange' => 'date_change(this);','readonly' => true])->label(false)
                                                    ?>
                                                </div>
                                                <div class="col-auto">
                                                    <?=
                                                    $form->field($model, 'start_hour', ['template' => "{input}\n{error}",
                                                        'labelOptions' => ['class' => 'control-label1']])->dropDownList(
                                                            $start_hour_array, ['class' => 'form-control select2','onchange' => 'hour_change();'])->label(false)
                                                    ?>
                                                </div>
                                                <div id="session_text_info" style="width: 100%;text-align: center;"><?= $model->session_text_info; ?></div>

                                                <?php $displayStyle = ($model->all_dates != "") ? "display:block;" : "display:none;"; ?>
                                                <div class="col-lg-12 col-md-12 col-12 text-center">
                                                    <?= Html::submitButton('Next', ['class' => "btn btn-orange btn-md next_btn pull-right", "style" => "$displayStyle"]) ?>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </section>


    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Confirm and Payment</h2>
            <div class="actions panel_actions float-right">

            </div>
        </header>
    </section>
</div>
