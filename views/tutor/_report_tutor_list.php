<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Tutor;
?>

<div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">

    <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>ABN</th>
                <th>Instrument</th>
                <th>Join Date</th>
                <th>Plan Category</th>
                <th>Plan Option</th>
                <th>State</th>
                <th>Time Zone</th>
                <th>Contract Expiry</th>
                <th>Certification Expiry Date</th>
                <th>Phone Number</th>
            </tr>
        </thead>

        <tbody>
            <?php
            if (!empty($tutorList)) {
                foreach ($tutorList as $key => $value) { 
                    $package_type_data = Tutor::getPlanCategoryByPlanId($value['tutor']['online_tutorial_package_type_uuid']);
                    $plan_cat = $plan_cat_display_name = $plan_option = '';
                    if(!empty($package_type_data)){
                        $plan_cat = $package_type_data['name'];
                        $plan_display_name = $package_type_data['display_name'];
                        
                        if($plan_cat == 'STANDARD'){
                            $getOption = Tutor::getPlanOption($value['tutor']['uuid'], $value['tutor']['online_tutorial_package_type_uuid'], $value['instrument_uuid']);
                        
                            $plan_option = "Option ".$getOption;
                            
                            $plan_cat_display_name = '<span class="badge badge-pill badge-warning tutor_disabled_link" >'.$plan_display_name.'</span>';
                        } else{
                            $plan_option = $plan_display_name;
                            $plan_cat_display_name = '<span class="badge badge-pill badge-success tutor_disabled_link" >'.$plan_display_name.'</span>';
                        }
                    }
                    
                    ?>
                    <tr>
                        <td><?= $key + 1; ?></td>
                        <td><?= $value['tutor']['first_name']; ?></td>
                        <td><?= $value['tutor']['last_name']; ?></td>
                        <td><?= $value['tutor']['email']; ?></td>
                        <td><?= $value['tutor']['abn']; ?></td>
                        <td><?= $value['instrument']['name']; ?></td>
                        <td><?= \app\libraries\General::displayDate($value['tutor']['join_date']); ?></td>
                        <td><?= $plan_cat_display_name; ?></td>
                        <td><?= $plan_option; ?></td>
                        <td><?= $value['tutor']['state']; ?></td>
                        <td><?= Tutor::getTimezone($value['tutor']['uuid']); ?></td>
                        <td><?= \app\libraries\General::displayDate($value['tutor']['contract_expiry']); ?></td>
                        <td><?= \app\libraries\General::displayDate($value['tutor']['child_certi_expiry']); ?></td>
                        <td><?= $value['tutor']['phone']; ?></td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>

</div>
