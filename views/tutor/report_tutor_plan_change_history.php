<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\web\View;
use app\models\OnlineTutorialPackageType;

$this->title = Yii::t('app', 'Tutor Plan Changes History');
$this->params['breadcrumbs'][] = $this->title;
$package_type_array = ArrayHelper::map(OnlineTutorialPackageType::find()->all(), 'uuid', 'display_name');

$this->registerJs(
        '
        $(document).on("click","#search_btn", function(){        
            $.ajax({
                type: "GET",
                url: "' . Url::to(['/tutor/tutor-plan-change-history/']) . '",
                data: $("#search_frm").serialize(),
                beforeSend:function(){ blockBody(); },
                success:function(result){
                    unblockBody();
                    $("#tutorial_package_table_parent_div").html(result);
                    window.ULTRA_SETTINGS.dataTablesInit();
                },
                error:function(e){ 
                    unblockBody();
                    showErrorMessage(e.responseText);
                }
            });
        });
$(document).on("click","#search_reset_btn", function(){        
    $.ajax({
        type: "GET",
        url: "' . Url::to(['/tutor/tutor-plan-change-history/']) . '",
        data: {},
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            $("#tutorstandardratehistorysearch-old_online_tutorial_package_type_uuid,#tutorstandardratehistorysearch-old_option,#tutorstandardratehistorysearch-option,#tutorstandardratehistorysearch-online_tutorial_package_type_uuid").select2({
                placeholder: "Select",
                allowClear: true
            })
            $("#tutorial_package_table_parent_div").html(result);
            window.ULTRA_SETTINGS.dataTablesInit();
        },
        error:function(e){ 
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});
    
    
', View::POS_END
);
?>

<div class="col-xl-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Plan Changes History</h2>
            <div class="actions panel_actions float-right">
                <i class="fa fa-search icon-primary icon-xs icon-orange mail_head_icon custom-toggle" data-togglediv="datatable-search-div" title="Advance Search"></i>

            </div>
        </header>
        <div class="content-body"> 
            <div class="row ">
                <div class="col-lg-12 col-md-12 col-12 ">
                    <div class="datatable-search-div " style="display: none;">

                        <?php
                        $form = ActiveForm::begin([
                                    'method' => 'get',
                                    'options' => [
                                        'id' => 'search_frm',
                                        'class' => 'form-inline'
                                    ],
                        ]);
                        ?>

                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'datetime')->textInput(['class' => 'form-control daterange  add-date-ranges', 'maxlength' => true, 'placeholder' => 'Date', 'data-format' => 'DD-MM-YYYY'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'firstName')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'First Name', 'style' => 'width:100%'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'lastName')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Last Name', 'style' => 'width:100%'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'instrumentName')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Instrument', 'style' => 'width:100%'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'old_online_tutorial_package_type_uuid')->dropDownList($package_type_array, ['class' => 'form-control myselect2', 'prompt' => 'Select Prev. Category', 'maxlength' => true])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'old_option')->dropDownList(['1' => 1,'2' => 2, '3' => 3, '4' => 4, '5' => 5, '6' => 6], ['class' => 'form-control myselect2', 'prompt' => 'Select Prev. Option', 'maxlength' => true])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'online_tutorial_package_type_uuid')->dropDownList($package_type_array, ['class' => 'form-control myselect2', 'prompt' => 'Select New Category', 'maxlength' => true])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'option')->dropDownList(['1' => 1,'2' => 2, '3' => 3, '4' => 4, '5' => 5, '6' => 6], ['class' => 'form-control myselect2', 'prompt' => 'Select New Option', 'maxlength' => true])->label(false) ?>
                        </div>


                        <?= Html::Button(Yii::t('app', 'Search'), ['class' => 'btn btn-primary mb-0', 'id' => 'search_btn', 'style' => 'margin-left: 17px;']) ?>
                        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-purple mb-0', 'id' => 'search_reset_btn']) ?>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
            <br>
            <div class=""></div>
            <div class="clearfix"></div>
            <div class="row" id="tutorial_package_table_parent_div">
                <?php echo Yii::$app->controller->renderPartial('_report_tutor_plan_change_history', ['historyList' => $historyList]); ?>
            </div>
        </div>
    </section> 
</div>

