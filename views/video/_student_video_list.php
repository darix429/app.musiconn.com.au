<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

if (!empty($recodedVideoList)) {
    foreach ($recodedVideoList as $key => $value) {

        $thumb_url = is_file(Yii::$app->params['media']['pre_recorded_video']['thumbnail']['path'] . $value['uuid'] . '.jpg') ?
                Yii::$app->params['media']['pre_recorded_video']['thumbnail']['url'] . $value['uuid'] . '.jpg' :
                Yii::$app->params['theme_assets']['url'] . "images/thumbnail.jpeg";
        $r_url = Url::to(['/video/review/', 'id' => $value['uuid']]);
        ?>
        <div class="col-md-15 col-sm-3">
            <div class="box_video">
                <img class="video_library_img img-reponsive" onclick="window.location.href='<?= $r_url?>'" src="<?= $thumb_url; ?>">
                <p class="orange video_content_side_bar"><?= $value['instrumentUu']['name']; ?></p>
                <p class="video_content_side_bar_sub"><?= $value['title']; ?></p>
                <p class="video_name" >
                        <?= (!empty($value['tutorUu']['first_name'])) ? $value['tutorUu']['first_name'].' '.$value['tutorUu']['last_name'] : ''; ?>
                        <?= (!empty($value['adminUploadUu']['first_name'])) ? $value['adminUploadUu']['first_name'].' '.$value['adminUploadUu']['last_name'] : ''; ?>
                </p>
            </div>
        </div>
        <?php
    }
} else {
    ?>
    <!--<h5 style="margin-left: 25px;"><b>No video found</b></h5>-->
<?php }
?>