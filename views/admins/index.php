<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;


$this->registerJs(
   '
    $(".admin_active_link").click(function(){
        
        $.ajax({
            type: "GET",
            url: "'.Url::to(['/admins/send-activation-link/']).'",
            data: { "id": $(this).data("uuid")},
            //beforeSend:function(){ $(".local-spinner").show(); },
            success:function(result){
                if(result.code == 200){
                    showSuccess(result.message);
                }else{
                    showErrorMessage(result.message);
                }
            },
            error:function(e){ 
                showErrorMessage(e.responseText);
            }
        });
    });
    

    $(document).on("click",".admin_disabled_link", function(){
        if(confirm("Are you sure you want to disable this account?")){
        var element = $(this);
            $.ajax({
                type: "GET",
                url: "'.Url::to(['/admins/account-disabled/']).'",
                data: { "id": $(this).data("uuid")},
                //beforeSend:function(){ $(".local-spinner").show(); },
                success:function(result){
                    if(result.code == 200){
                        $(element).removeClass("badge-primary");
                        $(element).addClass("badge-secondary");
                        
                        $(element).removeClass("admin_disabled_link");
                        $(element).addClass("admin_enabled_link");
                        $(element).text("DISABLED");
                        showSuccess(result.message);
                    }else{
                        showErrorMessage(result.message);
                    }
                },
                error:function(e){ 
                    showErrorMessage(e.responseText);
                }
            });
        }
    });
    
    $(document).on("click",".admin_enabled_link", function(){
        if(confirm("Are you sure you want to enable this account?")){
        var element = $(this);
            $.ajax({
                type: "GET",
                url: "'.Url::to(['/admins/account-enabled/']).'",
                data: { "id": $(this).data("uuid")},
                //beforeSend:function(){ $(".local-spinner").show(); },
                success:function(result){
                    if(result.code == 200){
                        $(element).removeClass("badge-secondary");
                        $(element).addClass("badge-primary");
                        
                        $(element).removeClass("admin_enabled_link");
                        $(element).addClass("admin_disabled_link");
                        
                        $(element).text("ENABLED");
                        showSuccess(result.message);
                    }else{
                        showErrorMessage(result.message);
                    }
                },
                error:function(e){ 
                    showErrorMessage(e.responseText);
                }
            });
        }
    });
    
//delete code 
    $(document).on("click",".delete_admin_link", function(){
        if(confirm("Are you sure you want to delete this account?")){
        var element = $(this);
            $.ajax({
                type: "POST",
                url: "'.Url::to(['/admins/delete/']).'",
                data: { "id": $(this).data("uuid")},
                success:function(result){
                    if(result.code == 200){
                    
                        $(element).closest("tr").remove();
                        
                        showSuccess(result.message);
                    }else{
                        showErrorMessage(result.message);
                    }
                },
                error:function(e){ 
                    showErrorMessage(e.responseText);
                }
            });
        }
    });
    
$(document).on("click","#search_btn", function(){        
    $.ajax({
        type: "GET",
        url: "'.Url::to(['/admins/index/']).'",
        data: $("#search_frm").serialize(),
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            $("#admin_table_parent_div").html(result);
            window.ULTRA_SETTINGS.dataTablesInit();
        },
        error:function(e){ 
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});

$(document).on("click","#search_reset_btn", function(){

    $.ajax({
        type: "GET",
        url: "'.Url::to(['/admins/index/']).'",
        data: {},
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            $("#admin_table_parent_div").html(result);
            window.ULTRA_SETTINGS.dataTablesInit();
        },
        error:function(e){ 
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});
    
    
',View::POS_END
);
?>

<div class="col-xl-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Users List</h2>
            <div class="actions panel_actions float-right">
                <i class="fa fa-search icon-primary icon-xs icon-orange mail_head_icon custom-toggle" data-togglediv="datatable-search-div" title="Advance Search"></i>
                <a href="<?= Yii::$app->getUrlManager()->createUrl(['/admins/create/'])?>" class="btn btn-primary "><i class="fa fa-plus text-white"></i></a>
            </div>
        </header>
        <div class="content-body"> 
            <div class="row ">
                <div class="col-lg-12 col-md-12 col-12 ">
                    <div class="datatable-search-div " style="display: none;">
                    
                    <?php $form = ActiveForm::begin([
                                'method' => 'get',
                                'options' => [
                                    'id' => 'search_frm',
                                    'class' => 'form-inline'
                                ],
                            ]); ?>
                   
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'fullname')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Name'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'email')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $searchModel->getAttributeLabel('email')])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'status')->dropDownList(['ENABLED' => 'ENABLED', 'DISABLED' => 'DISABLED', 'DELETED' => 'DELETED'], ['class' => 'form-control', 'style' => 'width:100%'])->label(false) ?>
                        </div>                            
                        <?= Html::Button(Yii::t('app', 'Search'), ['class' => 'btn btn-primary mb-0', 'id'=>'search_btn']) ?>
                        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-purple mb-0', 'id'=>'search_reset_btn']) ?>
                    
                    <?php ActiveForm::end(); ?>
                </div>
                </div>
            </div>
            <br>
            <div class=""></div>
            <div class="clearfix"></div>
            <div class="row" id="admin_table_parent_div">
                <?php echo Yii::$app->controller->renderPartial('_admin_list', ['adminList' => $adminList]); ?>
            </div>
        </div>
    </section>
  
    <!--
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Inactive Admin List</h2>
            <div class="actions panel_actions float-right">
                <i class="box_toggle fa fa-chevron-down"></i>
            </div>
        </header>
        <div class="content-body">  
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">

                    <table id="example-1" class="table table-striped dt-responsive display " cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php
                           /* if (!empty($inactiveAdmin)) {
                                foreach ($inactiveAdmin as $key => $value) {
                                    ?>
                                    <tr>
                                        <td><?= $key + 1; ?></td>
                                        <td><?= $value['first_name'] . ' ' . $value['last_name']; ?></td>
                                        <td><?= $value['email']; ?></td>
                                        <td><?= $value['admin_type']; ?></td>
                                        <td>
                                            <span class="bg-danger"><?= $value['status']; ?></span>
                                        </td>
                                        <td class="list-unstyled list-inline">
                                           <?php 
                                           if($value['status'] == 'INACTIVE'){ ?>
                                            <a href="javascript:void(0)" class="text-primary admin_active_link" data-uuid="<?= $value['admin_uuid']?>" title="Send Enabled Link"><i class="fa fa-paper-plane-o icon-rounded icon-primary icon-xs icon-bordered"></i></a>
                                                <!--<button class="btn btn-primary" ><i class="fa fa-paper-plane-o icon-xs"></i></button>-->
                                           <!--<?php } ?> 
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }*/
                            ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </section> -->
</div>


