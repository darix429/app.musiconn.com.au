<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Student */

$this->title = Yii::t('app', 'Apps');
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_icon'] = 'apps_main.png';
$this->params['page_icon'] = 'Apps-gradient.png';
?>
<div class="student-view">
	<section class="content">
		<div class="container-fluid video_categry-bg-white">
			<div class="row">
				<div class="container-fluid video_sub_manu">
				    	<div class="col-sm-12 col-md-12 col-lg-12">
						<div class="col-sm-3 col-md-3 col-lg-3" style='margin-bottom:25px;'>
							<a href="https://musescore.org/en" target="_blank">
						    <img src="/theme_assets/new/img/compose.png"/>
						    Compose
							</a>
						</div>

						<div class="col-sm-3 col-md-3 col-lg-3" style='margin-bottom:25px;'>
							<a href="https://www.drumbot.com/projects/tuned/" target="_blank">
						    <img src="/theme_assets/new/img/tuner.png">
						    Simple Tuner
							</a>
						</div>
						<div class="col-sm-3 col-md-3 col-lg-3" style='margin-bottom:25px;'>
							<a href="http://simple.bestmetronome.com/" target="_blank">
						    <img src="/theme_assets/new/img/metronome.png">Metronome X
							</a>
						</div>
						<div class="col-sm-3 col-md-3 col-lg-3" style='margin-bottom:25px;'>
							<a href="https://www.youtube.com/channel/UCoZUex0PQGOrTa9VdGxHaNg/playlists" target="_blank">
						    <img src="/theme_assets/new/img/drumless.png">Drumless Backing Tracks
							</a>
						</div>
				        
				    	</div>
				</div>
			</div>	
			<div class="row">
		            	<div class="container-fluid video_sub_manu">
				        <div class="col-sm-3 col-md-3 col-lg-3" style='margin-bottom:25px;'>
						<a href="https://www.youtube.com/playlist?list=PLEk1V5QgcGrPLxlnIV_NPFyIh8mfAvPCF" target="_blank">
				            <img src="/theme_assets/new/img/jazz.png">Jazz Standards
						</a>
				        </div>
				        <div class="col-sm-3 col-md-3 col-lg-3" style='margin-bottom:25px;'>
						<a href="https://www.musictheory.net/lessons" target="_blank">
				            <img src="/theme_assets/new/img/theory.png">Theory
				            </span>
						</a>
				        </div>
					<div class="col-sm-3 col-md-3 col-lg-3" style='margin-bottom:25px;'>
						<a href="https://www.musictheory.net/exercises" target="_blank">
				            		<img src="/theme_assets/new/img/aural.png">Aural training
				            	</a>
				        </div>
		   		</div>
	     		</div>
		</div>
     </section>	
</div>
