<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CancelSession */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cancel-session-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'uuid')->textInput() ?>

    <?= $form->field($model, 'booked_session_uuid')->textInput() ?>

    <?= $form->field($model, 'student_uuid')->textInput() ?>

    <?= $form->field($model, 'tutor_uuid')->textInput() ?>

    <?= $form->field($model, 'cancel_by')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'canceler_uuid')->textInput() ?>

    <?= $form->field($model, 'approved_by')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'approver_uuid')->textInput() ?>

    <?= $form->field($model, 'approve_datetime')->textInput() ?>

    <?= $form->field($model, 'rejected_by')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'rejecter_uuid')->textInput() ?>

    <?= $form->field($model, 'reject_datetime')->textInput() ?>

    <?= $form->field($model, 'status')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'cancel_note')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
