<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\OnlineTutorialPackage */

$this->title = Yii::t('app', 'Create Online Tutorial Package');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Online Tutorial Packages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<!--<div class="online-tutorial-package-create">

    <h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
        'packegetypeModel' => $packegetypeModel,
        'premiumstandardplanmodel' => $premiumstandardplanmodel,
    ]) ?>

<!--/div>-->
