<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;

$this->title = Yii::t('app', 'Monthly Subscription Fees');
$this->params['breadcrumbs'][] = $this->title;


$this->registerJs(
   '
    
    $(document).on("click",".fee_disabled_link", function(){
        if(confirm("Are you sure, you want to disabled this monthly subscription fee?")){
        var element = $(this);
            $.ajax({
                type: "GET",
                url: "'.Url::to(['/monthly-subscription-fee/fee-disabled/']).'",
                data: { "id": $(this).data("uuid")},
                //beforeSend:function(){ $(".local-spinner").show(); },
                success:function(result){
                    if(result.code == 200){
                        $(element).removeClass("badge-primary");
                        $(element).addClass("badge-secondary");
                        
                        $(element).removeClass("fee_disabled_link");
                        $(element).addClass("fee_enabled_link");
                        $(element).text("DISABLED");
                        showSuccess(result.message);
                    }else{
                        showErrorMessage(result.message);
                    }
                },
                error:function(e){ 
                    showErrorMessage(e.responseText);
                }
            });
        }
    });
    
    $(document).on("click",".fee_enabled_link", function(){
        if(confirm("Are you sure, you want to enabled this monthly subscription fee?")){
        var element = $(this);
            $.ajax({
                type: "GET",
                url: "'.Url::to(['/monthly-subscription-fee/fee-enabled/']).'",
                data: { "id": $(this).data("uuid")},
                //beforeSend:function(){ $(".local-spinner").show(); },
                success:function(result){
                    if(result.code == 200){
                        $(element).removeClass("badge-secondary");
                        $(element).addClass("badge-primary");
                        
                        $(element).removeClass("fee_enabled_link");
                        $(element).addClass("fee_disabled_link");
                        
                        $(element).text("ENABLED");
                        showSuccess(result.message);
                    }else{
                        showErrorMessage(result.message);
                    }
                },
                error:function(e){ 
                    showErrorMessage(e.responseText);
                }
            });
        }
    });
    
//delete code 
    $(document).on("click",".delete_fee_link", function(){
        if(confirm("Are you sure, you want to delete this monthly subscription fee?")){
        var element = $(this);
            $.ajax({
                type: "POST",
                url: "'.Url::to(['/monthly-subscription-fee/delete/']).'",
                data: { "id": $(this).data("uuid")},
                beforeSend:function(){ blockBody(); },
                success:function(result){
                    if(result.code == 200){
                        unblockBody();
                        $(element).closest("tr").remove();
                        showSuccess(result.message);
                    }else{
                        unblockBody();
                        showErrorMessage(result.message);
                    }
                },
                error:function(e){ 
                    unblockBody();
                    showErrorMessage(e.responseText);
                }
            });
        }
    });
    
$(document).on("click","#search_btn", function(){        
    $.ajax({
        type: "GET",
        url: "'.Url::to(['/monthly-subscription-fee/index/']).'",
        data: $("#search_frm").serialize(),
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            $("#fee_table_parent_div").html(result);
            window.ULTRA_SETTINGS.dataTablesInit();
        },
        error:function(e){ 
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});
$(document).on("click","#search_reset_btn", function(){        
    $.ajax({
        type: "GET",
        url: "'.Url::to(['/monthly-subscription-fee/index/']).'",
        data: {},
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            $("#fee_table_parent_div").html(result);
            window.ULTRA_SETTINGS.dataTablesInit();
        },
        error:function(e){ 
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});
    
    
',View::POS_END
);
?>

<div class="col-xl-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Monthly Subscription Fees List</h2>
            <div class="actions panel_actions float-right">
                <i class="fa fa-search icon-primary icon-xs icon-orange mail_head_icon custom-toggle" data-togglediv="datatable-search-div" title="Advance Search"></i>
                <a href="<?= Yii::$app->getUrlManager()->createUrl(['/monthly-subscription-fee/create/'])?>" class="btn btn-primary "><i class="fa fa-plus text-white"></i></a>
            </div>
        </header>
        <div class="content-body"> 
            <div class="row ">
                <div class="col-lg-12 col-md-12 col-12 ">
                    <div class="datatable-search-div " style="display: none;">
                    
                    <?php $form = ActiveForm::begin([
                                'method' => 'get',
                                'options' => [
                                    'id' => 'search_frm',
                                    'class' => 'form-inline'
                                ],
                            ]); ?>
                   	    
		                <div class="col-md-3 mb-0">
		                    <?= $form->field($searchModel, 'name')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Name'])->label(false) ?>
		                </div>
				<div class="col-md-3 mb-0">
		                    <?= $form->field($searchModel, 'price')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Price', 'type' => 'number',])->label(false) ?>
		                </div>
				<div class="col-md-3 mb-0">
		                    <?= $form->field($searchModel, 'tax')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Tax', 'type' => 'number',])->label(false) ?>
		                </div>
				<div class="col-md-3 mb-0">
		                    <?= $form->field($searchModel, 'total_price')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Total Price', 'type' => 'number',])->label(false) ?>
		                </div><br><br>
				<div class="col-md-3 mb-0">
		                    <?= $form->field($searchModel, 'with_tutorial')->dropDownList(['1' => 'YES', '0' => 'NO'], ['prompt' => 'Is Tutorial','class' => 'form-control','style'=>'width: 100%;'])->label(false) ?>
		                </div> 
				<div class="col-md-3 mb-0">
		                    <?= $form->field($searchModel, 'status')->dropDownList(['ENABLED' => 'ENABLED', 'DISABLED' => 'DISABLED'], ['class' => 'form-control', 'style' => 'width:100%'])->label(false) ?>
		                </div>  
			                       
                        <?= Html::Button(Yii::t('app', 'Search'), ['class' => 'btn btn-primary mb-0', 'id'=>'search_btn', 'style'=>'margin-left: 17px;']) ?>
                                <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-purple mb-0', 'id'=>'search_reset_btn']) ?>
                    
                    <?php ActiveForm::end(); ?>
                </div>
                </div>
            </div>
            <br>
            <div class=""></div>
            <div class="clearfix"></div>
            <div class="row" id="fee_table_parent_div">
                <?php echo Yii::$app->controller->renderPartial('_monthly_subscription_fees_list', ['monthlyFeeList' => $monthlyFeeList]); ?>
            </div>
        </div>
    </section> 
</div>
