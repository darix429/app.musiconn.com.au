<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Url;
use app\models\Instrument;
use app\models\StudentTuitionBooking;

$step_data = StudentTuitionBooking::getSession(Yii::$app->user->identity->student_uuid);
?>



<?php
if (!empty($session)) {
    foreach ($session as $key => $value) {
        ?>
        <div class="row">
            <div class="col-md-2 col-lg-2 "><?= $value['date']; ?></div>
            <div class="col-md-10 ">
                <div class="row ">

                    <?php
                    if (!empty($value['time_array'])) {
                        foreach ($value['time_array'] as $k => $v) {
                            ?>
                            <div class="col-md-2 col-lg-2 offset-md-0 ">
                                <input tabindex="5" type="radio" name="session_date_time_radio_<?= $value['date'] ?>"  class="skin-line-red "  >
                                <label class="iradio-label form-label" for="line-radio-1"><?= $v; ?></label>
                            </div>
                        <?php } ?>
                    <?php } ?>

                </div>
            </div>
        </div>
    <?php } ?>
<?php } ?>