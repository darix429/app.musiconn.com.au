<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\web\View;
use yii\helpers\ArrayHelper;
use app\models\MusicoinPackage;
use app\models\StudentCreditCard;
use app\libraries\General;

/* @var $this yii\web\View */
/* @var $model app\models\MusicoinLog */
/* @var $form yii\widgets\ActiveForm */
//Yii::$app->user->identity->student_uuid

$planDetail = MusicoinPackage::findOne($step_data['step_1']['package_uuid']);
$saved_cc_detail = StudentCreditCard::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid, 'status' => 'ENABLED'])->one();

$model->promo_coins = !empty($model->promo_coins) ? $model->promo_coins : 0;
$model->cart_total_coins = ($model->total_coins + $model->promo_coins);

$this->registerJsFile('https://js.stripe.com/v2/');
$this->registerJs(
        ' 
//set your publishable key
Stripe.setPublishableKey("' . Yii::$app->params['stripe']['publishable_key'] . '");

buycoin_cart_refresh();

function delete_credit_card(cc_uuid){

    if(confirm("Are you sure, you want to delete this card ?")){
        $.ajax({
            type: "POST",
            url: "' . Url::to(['/musicoin/remove-card/']) . '",
            data: { "id": cc_uuid},
            beforeSend: function() {
                blockBody();
            },
            success:function(result){
                if(result.code == 200){

                    //$("#gostep-reload-2").submit();
                    refresh_step_2_after_card_remove();
                }else{
                    showErrorMessage(result.message);
                }
                unblockBody();
            },
            error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
    }
}
    
//callback to handle the response from stripe
    function buycoin_stripeResponseHandler(status, response) {

        if (response.error) {
            $("#buycoin_payBtn").removeAttr("disabled");
            $(".payment-errors").html(response.error.message);
            unblockBody();
            showSuccess(response.error.message);
        } else {
            var form$ = $("#frm_step_2");
            var token = response.id;

            form$.append("<input type=\"hidden\" name=\"stripeToken\" value=\""+token+"\" />");
            blockBody("Submiting...");
            
            save_step_2();
	    blockBody("Please waiting...");
        }
    }
    
function buycoin_beforeValidation(){

    $("#frm_step_2" ).find("input").each(
        function(index) {
            idnya = $(this).attr("id");
            $("#frm_step_2").yiiActiveForm("validateAttribute", idnya);
            err = $(this).parent().has("has-error");
            msg = $(this).parent().find(".help-block").html();
           // alert(idnya+"=="+msg);
        }
    );
}

function buycoin_submitform()
    {
        buycoin_cart_refresh(false);
        buycoin_beforeValidation();

        $("#buycoin_payBtn").attr("disabled", "disabled");
        var dateText = $("#studentmusicoinpackage-cc_month_year").val();
        var pieces = dateText.split("-");
        var year = (pieces[0] != undefined && pieces[0] > 0) ? pieces[0] : "1970";
        var month = (pieces[1] != undefined && pieces[1] > 0) ? pieces[1] : "00";

        var token = Stripe.createToken({
            number: $("#studentmusicoinpackage-cc_number").val(),
            cvc: $("#studentmusicoinpackage-cc_cvv").val(),
            exp_month: month,
            exp_year: year,
        }, buycoin_stripeResponseHandler);
    }
    
$(".custom_expire_month_year").datepicker( {
        format: "yyyy-mm",
        viewMode: "months", 
        minViewMode: "months",
        startDate:new Date(),
        startView:2
    });
');
?>
<style>

    /* Important part */
    .modal-dialog{
        overflow-y: initial !important
    }
    .modal-body{
        /*height: 250px;*/
        overflow-y: auto;
    }
    .form-group{
        margin-bottom: 0px;
    }
</style>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">BUY MUSICOINS</h4>
</div>
<div class="modal-body padding-no">

    <?php
//    echo "<pre>";
//    print_r($model->getErrors()['coupon_code'][0]);
//    echo "</pre>";
//    exit;
    $form = ActiveForm::begin([
                'action' => ['buy'],
                'id' => 'frm_step_2',
                'enableClientValidation' => true,
                'validateOnChange' => true,
                'options' => ['class' => 'form-horizontal']]);
    ?>
    <div class="row">
        <div class="col-md-12 ">
            <span class="" style="border-bottom: 2px solid #bbb8b8;margin-bottom: 5px;display: inline-block;width: 100%;">
                <div class="row">
                    <div class="col-md-6" style="border-right: 1px solid #bbb8b8;">
                        <label class=""><b>Plan:</b> <?= $planDetail->name; ?>
                            <br><b>Plan Coins:</b> <span id="plan_coins_text"><?= $planDetail->coins; ?></span>
                            <?= ($planDetail->bonus_coins > 0) ? "<br><b>Bonus Coins:</b> <span id=\"plan_bonus_coins_text\">" . $planDetail->bonus_coins : "</span>"; ?>
                            <br><b>Promo Coins:</b> <span id="promo_coins_text"><?= $model->promo_coins; ?></span>
<!--                            <br>-->
                            <br>
                            <b>Total Coins:</b> <span id="cart_total_coins_text"><?= $model->cart_total_coins; ?></span>
                        </label>
                    </div>

                    <div class="col-md-6" style="bborder-left: 1px solid #bbb8b8;">
                        <div class=" has-static">
                            <label class="form-label">Plan Price: <strong>$<span id="plan_price_text"><?= $planDetail->price; ?></span></strong></label>
                        </div>
                        <div class=" has-static">
                            <label class="form-label">Plan GST: <strong>$<span id="plan_gst_text"><?= $planDetail->gst; ?></span></strong></label>
                        </div>
                        <div class=" ">
                            <label class="form-label">Amount Payable: <strong>$<span id="cart_total_text"><?= $model->cart_total; ?></span></strong></label>
                        </div>
                    </div>
                </div>
            </span>

            <div class="card " >
                <div class="my-card-body">
                    <div class="row ">
                        <div class="col-md-12" >
                            <div class="row mb-0">
                                <div class="col-md-12 creative_kids_voucher_code_div">
                                    <div class="col-md-12">
                                        <?=
                                                $form->field($model, 'creative_kids_voucher_code', ['inputOptions' => ['autocomplete' => 'off']])
                                                ->textInput(['class' => 'form-control my_input_border pull-left', 'maxlength' => true, 'placeholder' => 'NSW Creative Kids Voucher Code'])
                                        ?>
                                    </div>
                                </div> 
                            </div>
                            <div class="row mb-0">
                                <div class="col-md-12 kids_name_div">
                                    <div class="col-md-12">
                                        <?=
                                                $form->field($model, 'kids_name', ['inputOptions' => ['autocomplete' => 'off']
                                                ])
                                                ->textInput(['class' => 'form-control my_input_border pull-left', 'maxlength' => true, 'placeholder' => 'Kid\'s Name'])
                                        ?>
                                        <font class="help-block" style="margin-top: 0%;margin-bottom: 0;margin-left: -3%;font-size: 11px; color: red; weight:bold;"><strong>Please enter the kid's name exactly as shown on the Creative Kids voucher.</strong></font>
                                    </div>
                                </div> 
                            </div>
                            <div class="row mb-0">
                                <div class="col-md-6 kids_dob_div">
                                    <div class="col-md-12">
                                        <?=
                                        $form->field($model, 'kids_dob')->textInput(['readonly' => true, 'class' => 'form-control custom_date_picker', 'maxlength' => true, 'placeholder' => 'DD-MM-YYYY']);
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-6 kids_postal_code_div">
                                    <div class="col-md-12">
                                        <?=
                                        $form->field($model, 'kids_postal_code')->textInput(['class' => 'form-control ', 'maxlength' => true, 'placeholder' => 'Kid\'s Postal Code']);
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-0">
                                <?=
                                        $form->field($model, 'coupon_code', ['inputOptions' => ['autocomplete' => 'off'], 'template' => ''
                                            . '<div class="col-md-12 coupon_parent_div">'
                                            . '<div class="col-md-8">{input}</div>'
                                            . '<div class="">'
                                            . '<button type="button" id="coupon_btn" class="btn btn-md btn-primary" onclick="buycoin_cart_refresh();">Apply</button>'
                                            . '</div>'
                                            . '</div>'
                                                //. '<div class="col-md-12">{error}</div>'
                                        ])
                                        ->textInput(['class' => 'form-control my_input_border pull-left', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('coupon_code')])
                                ?>

                                <div class="col-md-12"><p class="promo-code-msg "><?= (!empty($model->getErrors()['coupon_code'][0])) ? $model->getErrors()['coupon_code'][0] : "" ?></p></div>
                            </div>
                        </div>
                    </div>
                    <div class="hidden_inputs">
                        <?= $form->field($model, 'discount', ['template' => '{input}{error}'])->hiddenInput()->label(false); ?>
                        <?= $form->field($model, 'cart_total', ['template' => '{input}{error}'])->hiddenInput()->label(false); ?>
                        <?= $form->field($model, 'coupon_uuid', ['template' => '{input}{error}'])->hiddenInput()->label(false); ?>
                        <?= $form->field($model, 'promo_coins', ['template' => '{input}{error}'])->hiddenInput()->label(false); ?>
                        <?= $form->field($model, 'cart_total_coins', ['template' => '{input}{error}'])->hiddenInput()->label(false); ?>
                    </div>

                </div>
            </div>

            <div class="card " >
                <?php if (!empty($saved_cc_detail)) { ?>
                    <div class="my-card-body" style="padding-top: 0px">
                        <div class="row " >
                            <div class="col-md-12 " style="padding: 10px;background-color: #000;border: 1px solid; border-radius: 20px 15px 0px 0px;" >
                                <button type="button" class="btn btn-primary btn-sm pull-right buy_delete_credit_card" style="border:1px solid #fff;" data-uuid="<?= $saved_cc_detail['uuid']; ?>" onclick="delete_credit_card('<?= $saved_cc_detail['uuid']; ?>');">Remove Card</button>
                            </div>
                        </div>

                        <div class="hidden_inputs">
                            <?= $form->field($model, 'cc_uuid', ['template' => '{input}{error}'])->hiddenInput(['value' => $saved_cc_detail['uuid']])->label(false); ?>
                            <?= $form->field($model, 'cc_name', ['template' => '{input}{error}'])->hiddenInput(['value' => $saved_cc_detail['name']])->label(false); ?>
                            <?= $form->field($model, 'cc_number', ['template' => '{input}{error}'])->hiddenInput(['value' => General::decrypt_str($saved_cc_detail['number'])])->label(false); ?>
                            <?php
                            $exp_date = date('Y-m', strtotime($saved_cc_detail['expire_date']));
                            echo $form->field($model, 'cc_month_year', ['template' => '{input}{error}'])->hiddenInput(['value' => $exp_date])->label(false);
                            ?>
                        </div>

                        <div class="row">
                            <div class=" col-md-12">
                                <div class=" col-md-12 ">                                                                    
                                    <div class="form-group">
                                        <span><label class="control-label" for="studentcreditcard-name">Card Holder Name*</label></span>
                                        <span type="text" class="form-control" disabled="disabled"><?= $saved_cc_detail['name']; ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class=" col-md-12">
                                <div class=" col-md-12 ">                                                                    
                                    <div class="form-group">
                                        <span><label class="control-label" for="studentcreditcard-number">Card Number*</label></span>
                                        <span type="text" class="form-control bg-gray-field" disabled="disabled">
                                            <?php
                                            $number = General::decrypt_str($saved_cc_detail['number']);
                                            echo General::ccMasking($number);
                                            ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class=" col-md-12">
                                <div class=" col-md-6 ">
                                    <?=
                                            $form->field($model, 'cc_cvv', ['template' => '<span>{label}</span>{input}{error}', 'inputOptions' => ['autocomplete' => 'new-password']])
                                            ->passwordInput(['class' => 'form-control my_input_border', 'maxlength' => true,])
                                            ->label('CVV*')
                                    ?>
                                </div>
                                <div class=" col-md-6 ">
                                    <span><label class="control-label" for="studentcreditcard-cc_month_year">Expiry*</label></span>
                                    <span type="text" class="form-control" disabled="disabled"><?= date("Y-m", strtotime($saved_cc_detail['expire_date'])); ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class=" col-md-12">
                                <div class="col-md-12  term_condition_checkbox_parent_col">
                                    <div class="checkbox icheck">
                                        <?php
                                        echo $form->field($model, 'term_condition_checkbox', ['template' => ' {input} I agree to Musiconns <a data-toggle="modal" data-target="#terms_condition_model"><b>Terms and Conditions</b></a>{error} '])->checkbox([
                                            'class' => 'skin-square-red float-left '], false);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } else { ?>

                    <div class="my-card-body" >

                        <div class="row">
                            <div class="col-md-12">
                                <div class=" col-md-12 ">
                                    <?=
                                            $form->field($model, 'cc_name', ['template' => '<span>{label}</span>{input}{error}'])
                                            ->textInput(['class' => 'form-control demo', 'maxlength' => true,])
                                            ->label($model->getAttributeLabel('cc_name') . '*')
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class=" col-md-12 ">
                                    <?=
                                            $form->field($model, 'cc_number', ['template' => '<span>{label}</span>{input}{error}', 'inputOptions' => ['autocomplete' => 'new-password']])
                                            ->textInput(['class' => 'credit-card-number form-control', 'maxlength' => true,])
                                            ->label($model->getAttributeLabel('cc_number') . '*')
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class=" col-md-12">
                                <div class=" col-md-6 ">
                                    <?=
                                            $form->field($model, 'cc_cvv', ['template' => '<span>{label}</span>{input}{error}', 'inputOptions' => ['autocomplete' => 'new-password']])
                                            ->passwordInput(['class' => 'form-control my_input_border ', 'maxlength' => 5,])
                                            ->label($model->getAttributeLabel('cc_cvv') . '*')
                                    ?>
                                </div>
                                <div class=" col-md-6 ">
                                    <?=
                                            $form->field($model, 'cc_month_year', ['template' => '<span>{label}</span>{input}{error}'])
                                            ->textInput(['class' => 'form-control custom_expire_month_year ', 'data-min-view-mode' => 'months', 'data-start-view' => '2', 'data-format' => 'yyyy-mm', 'maxlength' => true,])
                                            ->label($model->getAttributeLabel('cc_month_year') . '*')
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class=" col-md-12">
                                <div class="col-md-12 cc_ckeckbx_parent_col">
                                    <?php
                                    echo $form->field($model, 'cc_checkbox')->checkbox([
                                        'template' => '<p class="save_future pull-left"><label class="icheck-label form-label" for="save"> {input}  </label> <p>',
                                        'class' => 'skin-square-red float-left save_future'])->label(false);
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class=" col-md-12">
                                <div class="col-md-12 term_condition_checkbox_parent_col">
                                    <?php
                                    echo $form->field($model, 'term_condition_checkbox', ['template' => ' {input} I agree to Musiconns <a data-toggle="modal" data-target="#terms_condition_model"><b>Terms and Conditions</b></a>{error} '])->checkbox([
                                        'class' => 'skin-square-red float-left '], false);
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>

            <div class="payment-errors text-danger"><?= (isset($error)) ? $error : ''; ?></div>

            <div class="row" style="">
                <input type="hidden" name="package_id" id="package_id" value="<?= $step_data['step_1']['package_uuid']; ?>">
                <input type="hidden" name="step" value="2">
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

    <?php echo Html::beginForm(['buy'], 'post', ['id' => 'frm_gotostep', 'class' => 'form-inline d-block']) ?>
    <input type="hidden" name="step" value="1">
    <?php echo Html::endForm() ?>

    <?php echo Html::beginForm(['buy'], 'post', ['id' => 'gostep-reload-2', 'class' => 'form-inline d-block']) ?>
    <input type="hidden" name="step" value="2">
    <input type="hidden" name="remove_card" value="yes">
    <?php echo Html::endForm() ?>
</div>
<div class="modal-footer">
    <?php
    //$next_btn_class = "d-none";
    ?>
    <?= Html::Button('Prev', ['class' => 'btn btn-primary ', 'onclick' => 'goto_step();']) ?>
    <?= Html::Button('Buy', ['class' => 'btn btn-primary ', 'id' => 'buycoin_payBtn', 'onclick' => 'buycoin_submitform();']) ?>

</div>