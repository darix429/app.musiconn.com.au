<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\ArrayHelper;
use app\models\TutorUnavailability;
use app\models\Tutor;

$this->title = Yii::t('app', 'Tutor Availability');
$this->params['breadcrumbs'][] = $this->title;


$tutorList = Tutor::find()->where(['status' => 'ENABLED'])->asArray()->all();

$this->registerJs(
        '
    $(document).on("click","#search_btn",function(){
        if( $("#tutor_select").val() != ""){

            $.ajax({
                type: "GET",
                url: "'.Url::to(['/tutor/booking_availability/']).'",
                data: { "tutor_uuid": $("#tutor_select").val()},
                beforeSend:function(){
                    blockBody();
                },
                success:function(result){

                    if(result.code == 404){
                        showErrorMessage(result.message);
                    }else{
                        $("#tutor_calendar_parent_div").show();
                        $("#tutor_calendar_parent_div").html(result);
                    }
                    unblockBody();
                },
                error:function(e){
                    unblockBody();
                    showErrorMessage(e.responseText);
                }
            });

        }else{
            $("#tutor_calendar_parent_div").hide();
        }
    });

', View::POS_END
);
?>

<div class="col-xl-12">
    <section class="box " >
        <header class="panel_header">

        </header>
        <div class="content-body">
            <div class="row ">
                <div class="col-lg-4 col-md-4 col-4 ">

                    <div class="input-group">
                        <select class="form-control myselect2" id="tutor_select">
                            <option value="" selected>Select Tutor</option>
                            <?php
                            if(count($tutorList)>0){
                                foreach ($tutorList as $key => $value) {
                                    echo '<option value="'.$value['uuid'].'" >'.$value['first_name'].' '.$value['last_name'].'</option>';
                                }
                            }
                            ?>
                        </select>
                        <div class="input-group-append">
                            <button class="btn btn-orange" id="search_btn" type="button">Search</button>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </section>
</div>


<div class="col-xl-12" id="tutor_calendar_parent_div">
    <?php //echo Yii::$app->controller->renderPartial('_availability_calender', []); ?>
</div>
