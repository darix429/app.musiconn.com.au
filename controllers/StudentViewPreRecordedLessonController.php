<?php

namespace app\controllers;

use Yii;
use app\models\StudentViewPreRecordedLesson;
use app\models\StudentViewPreRecordedLessonSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * StudentViewPreRecordedLessonController implements the CRUD actions for StudentViewPreRecordedLesson model.
 */
class StudentViewPreRecordedLessonController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => Yii::$app->user->can('/student-view-pre-recorded-lesson/index'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['view'],
                        'allow' => Yii::$app->user->can('/student-view-pre-recorded-lesson/view'),
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all StudentViewPreRecordedLesson models.
     * @return mixed
     */
    public function actionIndex() {

        $searchModel = new StudentViewPreRecordedLessonSearch();
        $lessonList = $searchModel->search(Yii::$app->request->queryParams, true);
        $searchModel->load(Yii::$app->request->queryParams);
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_pre_recorded_lesson_list', [
                        'lessonList' => $lessonList,
                'searchModel' => $searchModel,
            ]);
        } else {
            return $this->render('index', [
                        //'inactiveAdmin' => $inactiveAdmin,
                        'lessonList' => $lessonList,
                        'searchModel' => $searchModel,
            ]);
        }
    }

    /**
     * Displays a single StudentViewPreRecordedLesson model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id = '') {

        $searchModel = new StudentViewPreRecordedLessonSearch();
        if(isset($id) || !empty($id))
        {
        $searchModel->pre_recorded_video_uuid = $id;
        }
        $lessonList = $searchModel->searchDetail(Yii::$app->request->queryParams, true);

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_lesson_detail_list', [
                        'lessonList' => $lessonList,
            ]);
        } else {
            return $this->render('lesson_detail', [
                        //'inactiveAdmin' => $inactiveAdmin,
                        'lessonList' => $lessonList,
                        'searchModel' => $searchModel,
            ]);
        }
    }

    /**
     * Finds the StudentViewPreRecordedLesson model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return StudentViewPreRecordedLesson the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = StudentViewPreRecordedLesson::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

}
