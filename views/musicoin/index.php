<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Musicoin History');
$this->params['breadcrumbs'][] = $this->title;
$this->registerJs(
        '
    
function refresh_history(){
    $.ajax({
        type: "GET",
        url: "' . Url::to(['/musicoin/index/']) . '",
        data: $("#search_frm").serialize(),
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            $("#lesson_table_parent_div").html(result);
            window.ULTRA_SETTINGS.dataTablesInit();
        },
        error:function(e){ 
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
}

$(document).on("click","#search_btn", function(){        
    refresh_history();
});
$(document).on("click","#search_reset_btn", function(){        
    $.ajax({
        type: "GET",
        url: "' . Url::to(['/musicoin/index/']) . '",
        data: {},
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            $("#lesson_table_parent_div").html(result);
            window.ULTRA_SETTINGS.dataTablesInit();
        },
        error:function(e){ 
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});
    


    
', View::POS_END
);
?>

<div class="col-xl-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left"><?= $this->title; ?></h2>
            <div class="actions panel_actions float-right">
                <i class="fa fa-search icon-primary icon-xs icon-orange mail_head_icon custom-toggle" data-togglediv="datatable-search-div" title="Advance Search"></i>
                <a  class="btn btn-success btn-icon text-white " onclick="openCreditDebitCoinForm()"><span>Credit / Debit Musicoins</span></a>
            </div>
        </header>
        <div class="content-body"> 
            <div class="row ">
                <div class="col-lg-12 col-md-12 col-12 ">
                    <div class="datatable-search-div " style="display: none;">

                        <?php
                        $form = ActiveForm::begin([
                                    'method' => 'get',
                                    'options' => [
                                        'id' => 'search_frm',
                                        'class' => 'form-inline'
                                    ],
                        ]);
                        ?>
                        
                        <div class="col-md-3 mb-0">
                            <?php echo $form->field($searchModel, 'created_at')->textInput(['class' => 'form-control daterange  add-date-ranges', 'maxlength' => true, 'placeholder' => 'Date', 'data-format' => 'DD-MM-YYYY'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?php echo $form->field($searchModel, 'studentName')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Student', 'style' => 'width:100%'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?php echo $form->field($searchModel, 'summary')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Summary', 'style' => 'width:100%'])->label(false) ?>
                        </div>

                        <?= Html::Button(Yii::t('app', 'Search'), ['class' => 'btn btn-primary mb-0', 'id' => 'search_btn', 'style' => 'margin-left: 17px;']) ?>
                        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-purple mb-0', 'id' => 'search_reset_btn']) ?>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
            <br>
            <div class=""></div>
            <div class="clearfix"></div>
            <div class="row" id="lesson_table_parent_div">
                <?php echo Yii::$app->controller->renderPartial('_recorded_list', ['recordList' => $recordList,'searchModel'=>$searchModel]); ?>
            </div>
        </div>
    </section>
</div>
