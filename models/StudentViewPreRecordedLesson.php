<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "student_view_pre_recorded_lesson".
 *
 * @property string $uuid
 * @property string $pre_recorded_video_uuid
 * @property string $title
 * @property string $student_uuid
 * @property string $tutor_uuid
 * @property string $instrument_uuid
 * @property string $datetime
 */
class StudentViewPreRecordedLesson extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'student_view_pre_recorded_lesson';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'pre_recorded_video_uuid', 'title', 'student_uuid', 'tutor_uuid', 'instrument_uuid'], 'required'],
            [['uuid', 'pre_recorded_video_uuid', 'title', 'student_uuid', 'tutor_uuid', 'instrument_uuid'], 'string'],
            [['datetime'], 'safe'],
            [['uuid'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'pre_recorded_video_uuid' => Yii::t('app', 'Pre Recorded Video Uuid'),
            'title' => Yii::t('app', 'Title'),
            'student_uuid' => Yii::t('app', 'Student Uuid'),
            'tutor_uuid' => Yii::t('app', 'Tutor Uuid'),
            'instrument_uuid' => Yii::t('app', 'Instrument Uuid'),
            'datetime' => Yii::t('app', 'Datetime'),
        ];
    }
    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstrumentUu() {
        return $this->hasOne(Instrument::className(), ['uuid' => 'instrument_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentUu() {
        return $this->hasOne(Student::className(), ['student_uuid' => 'student_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTutorUu() {
        return $this->hasOne(Tutor::className(), ['uuid' => 'tutor_uuid']);
    }
    
}
