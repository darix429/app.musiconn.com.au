<?php

namespace app\controllers;

use Yii;
use app\models\Instrument;
use app\models\InstrumentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * InstrumentController implements the CRUD actions for Instrument model.
 */
class InstrumentController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                        [
                        'actions' => ['index'],
                        'allow' => Yii::$app->user->can('/instrument/index'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['create'],
                        'allow' => Yii::$app->user->can('/instrument/create'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['update'],
                        'allow' => Yii::$app->user->can('/instrument/update'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['delete'],
                        'allow' => Yii::$app->user->can('/instrument/delete'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['instrument-active'],
                        'allow' => Yii::$app->user->can('/instrument/instrument-active'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['instrument-inactive'],
                        'allow' => Yii::$app->user->can('/instrument/instrument-inactive'),
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                //'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    /*public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }*/

    /**
     * Lists all Instrument models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new InstrumentSearch();
        $searchModel->status = "ACTIVE";
        $instrumentList = $searchModel->search(Yii::$app->request->queryParams, true);

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_instrument_list', [
                        'instrumentList' => $instrumentList,
            ]);
        } else {
            return $this->render('index', [
                        //'inactiveAdmin' => $inactiveAdmin,
                        'instrumentList' => $instrumentList,
                        'searchModel' => $searchModel,
            ]);
        }
    }

    /**
     * Displays a single Instrument model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Instrument model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Instrument();
        $model->scenario = 'create';
        $model->attributes = Yii::$app->getRequest()->post('Instrument');

        if ($model->load(Yii::$app->request->post())) {
            $UploadedFile = UploadedFile::getInstance($model, 'image');
            $model->image = file_get_contents($UploadedFile->tempName);
            $model->created_at = $model->updated_at = time();
            //echo "<pre>";print_r($model);echo "</pre>";
            if ($model->validate()) {
                if ($model->save()) {
                    Yii::$app->getSession()->setFlash('success', 'You have successfully created Instrument category.');
                } else {
                    Yii::$app->getSession()->setFlash('danger', 'Something wrong. Instrument category does not created.');
                }
            }
            //return $this->redirect(['view', 'id' => $model->uuid]);
            return $this->redirect(['index']);
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Instrument model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $model->scenario = 'update';
// && $model->save()
        if ($model->load(Yii::$app->request->post())) {

            $tete = Yii::$app->request->post('Instrument');
            $UploadedFile = UploadedFile::getInstance($model, 'image');

            $model = Instrument::findOne($id);
            $model->name = $tete['name'];
            $model->description = $tete['description'];
            $model->status = $tete['status'];
            $model->updated_at = time();

            if (!empty($UploadedFile)) {
                $model->image = file_get_contents($UploadedFile->tempName);
            }
            if ($model->validate()) {
                if ($model->update()) {
                    Yii::$app->getSession()->setFlash('success', 'You have successfully created Instrument category.');
                } else {
                    Yii::$app->getSession()->setFlash('danger', 'Something wrong. Instrument category does not created.');
                }
            }
            return $this->redirect(['index']);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Instrument model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete() {
        $id = Yii::$app->getRequest()->post('id');
        
        $instrument = Instrument::instrumentDelete($id);
        if($instrument == ''){ 
            if ((($model = Instrument::findOne($id)) !== null))
            { 
                $model->delete();
                $return = ['code' => 200, 'message' => 'Instrument Category deleted successfully.'];
            } else { 
                $return = ['code' => 404, 'message' => 'The requested page does not exist.'];
            }
        }
        else{ 
            $return = ['code' => 404, 'message' => 'This instrument already assign to video so could not delete.'];
        }

        Yii::$app->response->format = 'json';
        return $return;
//        $this->findModel($id)->delete();
//
//        return $this->redirect(['index']);
    }

    /**
     * Finds the Instrument model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Instrument the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Instrument::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionInstrumentInactive($id) {

        if (($instrument = Instrument::findOne($id)) !== null) {

            if ($instrument->status == "ACTIVE") {
                $instrument->status = 'INACTIVE';
                $instrument->updated_at = time();
                $instrument->save(false);

                $return = ['code' => 200, 'message' => 'Instrument inactived successfully.'];
            } else {
                $return = ['code' => 421, 'message' => 'Instrument not actived.'];
            }
        } else {
            $return = ['code' => 404, 'message' => 'Record does not exist.'];
        }

        Yii::$app->response->format = 'json';
        return $return;
    }

    public function actionInstrumentActive($id) {

        if (($instrument = Instrument::findOne($id)) !== null) {

            if ($instrument->status == "INACTIVE") {
                $instrument->status = 'ACTIVE';
                $instrument->updated_at = time();
                $instrument->save(false);

                $return = ['code' => 200, 'message' => 'Instrument actived successfully.'];
            } else {
                $return = ['code' => 421, 'message' => 'Instrument not inactived.'];
            }
        } else {
            $return = ['code' => 404, 'message' => 'Record does not exist.'];
        }

        Yii::$app->response->format = 'json';
        return $return;
    }

}
