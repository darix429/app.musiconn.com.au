<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tutor_notification".
 *
 * @property string $uuid
 * @property string $tutor_uuid
 * @property string $type
 * @property string $time_unit
 * @property int $unit_value
 */
class TutorNotification extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tutor_notification';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'tutor_uuid', 'type', 'time_unit'], 'string'],
            [['tutor_uuid', 'time_unit', 'unit_value'], 'required'],
            [['unit_value'], 'default', 'value' => null],
            [['unit_value'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uuid' => 'Uuid',
            'tutor_uuid' => 'Tutor Uuid',
            'type' => 'Type',
            'time_unit' => 'Time',
            'unit_value' => 'Before',
        ];
    }
    
    public function addDefaultNotification($uuid = '') {
        
        $modelTutor = Tutor::find()->where(['uuid' => $uuid])->one();
        if(!empty($modelTutor)){
            $model = new TutorNotification();
            $model->tutor_uuid = $modelTutor->uuid;
            $model->type = "EMAIL";
            $model->time_unit = "MINUTES";
            $model->unit_value = 10;
            $model->save(false);
            return true;
        }else{
            return false;
        }        
    }
}
