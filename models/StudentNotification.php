<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "student_notification".
 *
 * @property string $uuid
 * @property string $student_uuid
 * @property string $type
 * @property string $time_unit
 * @property int $unit_value
 *
 * @property Student $studentUu
 */
class StudentNotification extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'student_notification';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'student_uuid', 'type', 'time_unit'], 'string'],
            [['student_uuid', 'time_unit', 'unit_value'], 'required'],
            [['unit_value'], 'default', 'value' => null],
            [['unit_value'], 'integer'],
            [['student_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['student_uuid' => 'student_uuid']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uuid' => 'Uuid',
            'student_uuid' => 'Student Uuid',
            'type' => 'Type',
            'time_unit' => 'Time',
            'unit_value' => 'Before',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentUu()
    {
        return $this->hasOne(Student::className(), ['student_uuid' => 'student_uuid']);
    }
    
    public function addDefaultNotification($uuid = '') {
        
        $modelStudent = Student::find()->where(['student_uuid' => $uuid])->one();
        if(!empty($modelStudent)){
            $model = new StudentNotification();
            $model->student_uuid = $modelStudent->student_uuid;
            $model->type = "EMAIL";
            $model->time_unit = "MINUTES";
            $model->unit_value = 10;
            $model->save(false);
            return true;
        }else{
            return false;
        }    
    }
}
