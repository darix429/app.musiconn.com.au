<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use app\libraries\General;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \mdm\admin\models\form\Signup */

$timezone = General::getAllTimeZones();
if ($model->timezone == '') {
    $model->timezone = Yii::$app->params['timezone'];
}
$this->title = 'Registration';
$this->params['breadcrumbs'][] = $this->title;
$this->registerJs('
$(".birthdate_signup").datepicker({
        minViewMode:  0,
        format: "dd-mm-yyyy",
        /* startDate: getValue($this, "startDate", ""),
        endDate: getValue($this, "endDate", ""),*/
        /* daysOfWeekDisabled: getValue($this, "disabledDays", ""),*/
        startView: 2,
        autoclose:true
    });

$("#student-country").change(function(){
                if($(this).val() == "other"){
                    $(".state_dd").hide();
                    $(".state_text").show();
                }else{
                    $(".state_text").hide();
                    $(".state_dd").show();
                }
            })
    ');
?>
<style>
    .col-md-6,.col-md-12{
        padding-right: 5px;
        padding-left: 5px;
    }
    .login-box{
        width: 500px;
        margin: 4% auto;
    }
    .login-logo{
        margin-top: 8%;
    }
</style>
<div class="login-box">
    <!-- /.login-logo -->
    <div class="login-box-body" style="margin-top: 0;">
        <h3 class="text-center"><?= $this->title; ?></h3>
        <?php
        $form = ActiveForm::begin([
                    'id' => 'form-signup',
                    'options' => ['name' => 'signupform'],
        ]);
        ?>

        <div class="row">
            <div class="col-lg-12 col-md-12 col-12">

                <div class="col-lg-12 col-md-12 col-12">
                    <div class="col-md-6">
                        <?= $form->field($modelStudent, 'first_name')->textInput(['autofocus' => false, 'class' => 'form-control', 'placeholder' => "First name"]); ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($modelStudent, 'last_name')->textInput(['autofocus' => false, 'class' => 'form-control', 'placeholder' => "Last name"]); ?>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-12">
                    <div class="col-md-12">
                        <?= $form->field($model, 'email')->textInput(['autofocus' => false, 'class' => 'input form-control', 'placeholder' => "Email"]); ?>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-12">
                    <div class="col-md-6">
                        <?= $form->field($model, 'password_hash')->passwordInput(['autofocus' => false, 'class' => 'input form-control', 'placeholder' => "Password"]); ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'confirmPassword')->passwordInput(['autofocus' => false, 'class' => 'input form-control', 'placeholder' => "Confirm Password"]); ?>
                    </div>
                </div>
                <div class="col-md-12">

                    <div class="col-md-6">
                        <?php
                        $modelStudent->birthdate = \app\libraries\General::displayDate($modelStudent->birthdate);
                        echo $form->field($modelStudent, 'birthdate')->textInput(['autofocus' => false, 'class' => 'form-control birthdate_signup', 'placeholder' => "DD-MM-YYYY",'readonly'=>'true']);
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'timezone')->dropDownList($timezone, ['class' => 'form-control myselect2', 'prompt' => 'Select Timezone', 'maxlength' => true]) ?>
                    </div>
                </div>
                
                
                <?php
                if ($modelStudent->country == 'other') {
                    $display_state_dd = 'none';
                    $display_state_text = 'block';
                } else {
                    $display_state_dd = 'block';
                    $display_state_text = 'none';
                }
                ?>
                <div class="col-lg-12 col-md-12 col-12">
                    <div class="col-md-6">

                        <?= $form->field($modelStudent, 'country')->dropDownList(['AUS' => 'Australia', 'other' => 'Other'], ['class' => 'form-control', 'maxlength' => true]) ?>
                        <!--?= $form->field($modelStudent, 'country')->textInput(['autofocus' => false, 'class' => 'input form-control','placeholder'=>"Country"]); ?-->
                    </div>
                    <div class="col-md-6 state_dd" style="display:<?= $display_state_dd; ?>">
                        <?= $form->field($modelStudent, 'state')->dropDownList(['New South Wales' => '	New South Wales', 'Queensland' => 'Queensland', 'South Australia' => 'South Australia', 'Tasmania' => 'Tasmania', 'Victoria' => 'Victoria', 'Western Australia' => 'Western Australia', 'Australian Capital Territory' => 'Australian Capital Territory', 'Northern Territory' => 'Northern Territory'], ['class' => 'form-control', 'maxlength' => true, 'prompt' => "Select " . $modelStudent->getAttributeLabel('state')]) ?>
                        <!--?= $form->field($modelStudent, 'state')->dropDownList(['NSW'=>'	New South Wales','QLD'=>'Queensland','SA'=>'South Australia','TAS'=>'Tasmania','VIC'=>'Victoria','WA'=>'Western Australia'], ['class' => 'form-control', 'maxlength' => true, 'prompt' => "Select " . $modelStudent->getAttributeLabel('state')]) ?-->
                        <!--?= $form->field($modelStudent, 'state')->textInput(['autofocus' => false, 'class' => 'input form-control','placeholder'=>"State"]); ?-->
                    </div>
                    <div class="col-md-6 state_text" style="display:<?= $display_state_text; ?>">
                        <?= $form->field($modelStudent, 'state_text')->textInput(['autofocus' => false, 'class' => 'input form-control state_text', 'placeholder' => "State"]); ?>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-12">
                    <div class="col-md-6">
                        <?= $form->field($modelStudent, 'street')->textInput(['autofocus' => false, 'class' => 'input form-control', 'placeholder' => "Street"]); ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($modelStudent, 'city')->textInput(['autofocus' => false, 'class' => 'input form-control', 'placeholder' => "City"]); ?>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-12">
                    <div class="col-md-6">
                        <?php // $form->field($modelStudent, 'postal_code')->widget(\yii\widgets\MaskedInput::className(['plceholder' => '']), ['mask' => '9999', 'clientOptions' => ['removeMaskOnSubmit' => true], 'options' => ['placeholder' => 'xxxx', 'class' => 'form-control']]) ?>
                        <?php echo $form->field($modelStudent, 'postal_code')->textInput(['autofocus' => false, 'maxlength' => true,'minlength' => true,'class' => 'input form-control', 'placeholder' => "Postal Code"]);   ?>
                    </div>
                    <div class="col-md-6">
                        <?php // $form->field($modelStudent, 'phone_number')->textInput(['maxlength' => 10,'autofocus' => false, 'class' => 'input form-control', 'placeholder' => "Phone Number"]);  ?>
                        <?= $form->field($modelStudent, 'phone_number')->widget(\yii\widgets\MaskedInput::className(['plceholder' => '']), ['mask' => '9999 999 999', 'clientOptions' => ['removeMaskOnSubmit' => true], 'options' => ['placeholder' => 'XXXX XXX XXX', 'class' => 'form-control']]) ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <center>
                <?= Html::submitButton('Register', ['class' => 'login_btn btn btn-primary btn-block btn-flat', 'name' => 'save-button']) ?>
            </center>
            <!-- /.col -->
        </div>
        <?php ActiveForm::end(); ?>
        <center>
            <?= Html::a('Go to Sign In', ['site/login'], []) ?>
        </center>
        <div class="login-logo">
            <a href="<?= Yii::$app->params['base_url']; ?>"><img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new/' ?>img/login-logo.png"></a>
            <center><p class="login_btm"><?= Yii::$app->params['copyright']; ?></p></center>
        </div>
    </div>
    <!-- /.login-box-body -->
</div>
