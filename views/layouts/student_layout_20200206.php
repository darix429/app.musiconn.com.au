<?php
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/js/jquery-3.2.1.min.js');

$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/datatables/js/datatables.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/datatables/extensions/Buttons/js/dataTables.buttons.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/datatables/extensions/Buttons/js/buttons.flash.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/datatables/extensions/Buttons/js/buttons.html5.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/datatables/extensions/Buttons/js/buttons.print.min.js');


$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/messenger/js/messenger.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/messenger/js/messenger-theme-future.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/messenger/js/messenger-theme-flat.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/js/messenger.js');

//Calender
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/calendar/moment.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/jquery-ui/smoothness/jquery-ui.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/calendar/fullcalendar.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/js/jquery.form.js');

$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/jquery-validation/js/jquery.validate.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/js/form-validation.js');

foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
    if ($key == 'success') {
        $this->registerJs("showSuccess('$message');");
    } else {
        $this->registerJs("showErrorMessage('$message');");
    }
}

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\NewAppAsset;
use yii\helpers\Url;
use yii\web\View;
use app\models\Tutor;
use app\models\Student;

NewAppAsset::register($this);

$page_length = (Yii::$app->params['datatable']['page_length'] > 0 ) ? Yii::$app->params['datatable']['page_length'] : 10;
$this->registerJs("
        var dt_page_length = " . $page_length . ";
        var web_url = '" . Url::to(['/']) . "';
       ", View::POS_HEAD);


$this->registerJs(
        '
// Global form submit loader display code
$("form").on("beforeValidate", function (event, messages, deferreds) {
    blockBody("Loading...");

    // called when the validation is triggered by submitting the form
    // return false if you want to cancel the validation for the whole form

}).on("afterValidate", function (event, messages,errorAttributes) {
    
    if (errorAttributes.length) {
    
        unblockBody();
    }

}).on("beforeSubmit", function () {
    blockBody("Loading...");
   //return false;
    // after all validations have passed
    // you can do ajax form submission here
    // return false if you want to stop form submission
});

', yii\web\View::POS_END
);


$profile_url = Url::to(['student/profile']);

$currentController = Yii::$app->controller->id;
$currentAction = Yii::$app->controller->action->id;
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>

        <link rel="shortcut icon" href="/theme_assets/images/favicon.ico" type="image/x-icon" /><!-- Favicon -->
        <link rel="apple-touch-icon-precomposed" href="/theme_assets/images/apple-touch-icon-57-precomposed.png"><!-- For iPhone -->
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/theme_assets/images/apple-touch-icon-114-precomposed.png">   <!-- For iPhone 4 Retina display -->
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/theme_assets/images/apple-touch-icon-72-precomposed.png"><!-- For iPad -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/theme_assets/images/apple-touch-icon-144-precomposed.png"><!-- For iPad Retina display -->

        <?php $this->head() ?>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <?php $this->beginBody() ?>
        
        <div class="wrapper">
            <?php echo Html::beginForm(['/site/logout'], 'post', ['id' => 'logoutfrm']) ?>
            <?php echo Html::endForm() ?>
            
            <!-- Loader code -->
            <div id="preloader" style="display: block;">
                <div id="loader"></div>
                <p id="loader_message"></p>
            </div>
            
            <header class="main-header">
                <nav class="navbar navbar-static-top">
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button"><img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new/'?>img/toggle.png"></a>
                    <span class="welcome_title"><?= substr(Yii::$app->user->identity->first_name . ' ' . Yii::$app->user->identity->last_name,0,20) ?></span>
                    <div class="navbar-custom-menu">
                        
                        <ul class="nav navbar-nav">
                            
                            <?php if(Yii::$app->session->get('user.idbeforeswitch.admin') && Yii::$app->session->get('user.idbeforeswitch.admin') > 0 && in_array(Yii::$app->user->identity->role, ['STUDENT']) ){ ?>
                            
                            <li>
                                <a href="<?= Url::to(['/admins/back-to-admin/'])?>">
                                    <img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new/'?>img/back_to_admin.png">
                                    Back To <?= ucfirst(strtolower(Yii::$app->session->get('user.idbeforeswitch.admin.role')));?>
                                </a>
                            </li>
                            <?php } ?>
<!--                            <li class="profile list-inline-item">
                                <a href="#" data-toggle="dropdown" class="toggle">
                                    <i class="fa fa-shopping-cart"></i>
                                    <img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new/'?>img/logout.png">
                                </a>
                                <ul class="dropdown-menu profile animated fadeIn">
                                    <li class="dropdown-item">
                                        <a href="">
                                            <i class="fa fa-shopping-cart"></i>
                                            Profile
                                        </a>
                                    </li>
                                </ul>
                            </li>-->
                            <li>
                                <a onclick="buy_musicoin();" style="cursor: pointer;">
                                    <img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new/'?>img/cart.png">
                                    <span class="badge badge-pill badge-primary student_musicoin_balance_text"><?= \app\libraries\General::getStudentMusicoin(Yii::$app->user->identity->student_uuid); ?></span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:$('#logoutfrm').submit();">
                                    <img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new/'?>img/logout.png"> Logout
                                </a>
                            </li>
                        </ul>
                        
                    </div>
                </nav>
            </header>
            
            <aside class="main-sidebar">
                <section class="sidebar">
                    <div class="user-panel">
                        <div class="pull-left image">
                            <span class="profile-charecter" onclick="window.location.href = '<?= Yii::$app->getUrlManager()->createUrl('/student/profile'); ?>'"><?= \app\models\User::loginUserProfileCharecter(); ?></span>
                            <!--<img onclick="window.location.href = '<?= Yii::$app->getUrlManager()->createUrl('/student/profile'); ?>'" src="<?= Yii::$app->request->baseUrl . '/theme_assets/new/'?>img/profile.png" class="img-circle" alt="User Image">-->
                        </div><br>
                        <div class="pull-left info">
                            <p><?= substr(Yii::$app->user->identity->first_name . ' ' . Yii::$app->user->identity->last_name,0,20) ?></p>
                            <a href="#"><?= Yii::$app->user->identity->role; ?></a>
                        </div>
                    </div>

                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="<?= ( 
                                        ($currentController == 'student' && in_array($currentAction ,['profile',]) )
                                    ||  ($currentController == 'monthly-subscription' && in_array($currentAction ,['index','history']) )
                                    ||  ($currentController == 'book-music-tution' && in_array($currentAction ,['history','view']) )
                                    ||  ($currentController == 'order' && in_array($currentAction ,['history']) )
                                    ||  ($currentController == 'booked-session' && in_array($currentAction ,['expired']) )
                                ) ? 'active' : '' ?>">
                            <a href="<?= Yii::$app->getUrlManager()->createUrl('/student/profile'); ?>">
                                <img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new/'?>img/my-profile.png"> <span>MY PROFILE</span>
                            </a>
                        </li>
                        <li class="<?= ( 
                                        ($currentController == 'booked-session' && in_array($currentAction ,['my-lessons','upcoming','completed','cancelled','credited']) )
                                    ||  ($currentController == 'book-music-tution' && in_array($currentAction ,['index','credit-schedule','reschedule-request']) )
                                ) ? 'active' : '' ?>">
                            <a href="<?= Yii::$app->getUrlManager()->createUrl('/booked-session/my-lessons'); ?>">
                                <img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new/'?>img/my-lessons.png"> <span>MY LESSONS</span>
                            </a>
                        </li>
                        <li class="<?= ( 
                                        ($currentController == 'video' && in_array($currentAction ,['student-video-list', 'review']) )
                                ) ? 'active' : '' ?>">
                            <a href="<?= Yii::$app->getUrlManager()->createUrl('/video/student-video-list'); ?>">
                                <img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new/'?>img/video-library.png"> <span>VIDEO LIBRARY</span>
                            </a>
                        </li>
			 <li class="<?= ( 
                                        ($currentController == 'student' && in_array($currentAction ,['apps']) )
                                ) ? 'active' : '' ?>">
                            <a href="<?= Yii::$app->getUrlManager()->createUrl('/student/apps'); ?>">
                                <img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new/'?>img/chat_app.png"> <span>APPS</span>
                            </a>
                        </li>
<!--                        <li class="<?= ( 
                                        ($currentController == 'student' && in_array($currentAction ,['tutor_available']) )
                                ) ? 'active' : '' ?>">
                            <a href="<?= Yii::$app->getUrlManager()->createUrl('/student/tutor_available'); ?>">
                                <img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new/'?>img/calendar.png"> <span>TUTOR AVAILABILITY</span>
                            </a>
                        </li>-->
                    </ul>
                    <span class="side_logo"><img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new/'?>img/logo.png"></span>
                </section>
            </aside>
            
            <!-- START CONTAINER -->
            <div class="content-wrapper">
                <section class="content-header">
                    <?php if(!empty($this->params['page_title_custom'])) {
                        echo $this->params['page_title_custom'];
                    } else {?>
                    <h1>
                        <?php $page_icon = (!empty($this->params['page_icon'])) ? $this->params['page_icon'] : 'my-profile.png'; ?>
                        <img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new/img/'.$page_icon; ?>"><?= strtoupper($this->title); ?>
                    </h1>
                    <?php } ?>
                    
                    
                </section>
                
                <?php if(!empty(Yii::$app->session['custom_alert']) && !empty(Yii::$app->session['custom_alert']['message'])){ ?>
                    <div class="col-md-12 margin-10">
                        <div class="alert alert-<?= Yii::$app->session['custom_alert']['type'];?>" role="alert">
                            <?= Yii::$app->session['custom_alert']['message']; ?>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    </div>
                <?php unset(Yii::$app->session['custom_alert']); } ?>
                
                <?= $content ?>
            </div>
            <!-- END CONTAINER -->
            
            <footer class="main-footer">
                <?= Yii::$app->params['copyright']; ?>
            </footer>
            <div class="control-sidebar-bg"></div>
            
        </div>
        <!-- END .wrapper -->
        

        <!-- modal start -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="ultraModal-Label" aria-hidden="true">
            <div class="modal-dialog animated zoomIn">
                <div class="modal-content">
                    <div class="modal-header">
                        <div id="title-demo"></div>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div id="video-content"></div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <!--<button class="btn btn-success" type="button">Save changes</button>-->
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="tutorUnavailability" tabindex="-1" role="dialog" aria-labelledby="ultraModal-Label" aria-hidden="true">
            <div class="modal-dialog animated zoomIn">
                <div class="modal-content">
                    <div class="modal-header">
                        <div id="title-tutorUnavailability"></div>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div id="unavailability-content"></div>
                </div>
            </div>
        </div>
<!--        <div class="modal fade" id="tutorAvailabilityCalender" tabindex="-1" role="dialog" aria-labelledby="ultraModal-Label" aria-hidden="true">
            <style>#tutorAvailabilityCalender-content > section.box { margin: 0;}</style>
            <div class="modal-dialog animated zoomIn " style="max-width:90%;">
                <div class="modal-content border-0">
                    <div class="modal-header bg-primary">
                        <h4 id="title-tutorAvailabilityCalender" class="modal-title bg-primary"></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div id="tutorAvailabilityCalender-content"></div>
                </div>
            </div>
        </div>-->
        <!-- modal-content- LESSON BOOK SECTION AVAIBILITY START-->
        <div class="modal fade" id="tutorAvailabilityCalender">
            <div class="modal-dialog" style="width:90%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="title-tutorAvailabilityCalender"></h4>
                    </div>
                    <div id="tutorAvailabilityCalender-content" style="padding: 10px"></div>

                </div>
            </div>
        </div>
        <!-- modal-content- LESSON BOOK SECTION AVAIBILITY END-->
        
        
      
        <!-- modal-content- LESSON BOOK OPTION -->
        <div class="modal fade" id="studentBookingOption">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">CHOOSE YOUR BOOKING OPTION</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-6 text-center" style="border-right: 1px solid #bbb8b8;">
                                <p> </p>
                                <a class="btn btn-primary btn-icon redirect_booking_btn" href="javascript:void(0)" data-href="<?= Yii::$app->params['base_url'] . 'book-music-tution'; ?>" >
                                    BOOK A NEW LESSON
                                </a>
                            </div>
                            <div class="col-lg-6 text-center" >
                                <p> </p>
                                <a class="btn btn-primary btn-icon redirect_booking_btn" href="javascript:void(0)" data-href="<?= Yii::$app->params['base_url'] . 'book-music-tution/credit-schedule'; ?>" >
                                    BOOK A CREDIT LESSON
                                </a>
                                <!--<div class="clearfix"></div>-->
                                <p>You have <span id="model_credit_session_count">0</span> credit Lessons.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal -->
        
        <!-- modal-content- Buy Musicoins-->
        <div class="modal fade" id="buy-musicoins-model">
            <div class="modal-dialog">
                <div class="modal-content buy-musicoins-model-body">  
                    
                </div>
            </div>
        </div>
        <!-- /.modal -->
        
        <!-- modal start -->
        <div class="modal fade" id="terms_condition_model" style="z-index: 1051;" tabindex="-1" role="dialog" aria-labelledby="ultraModal-Label" aria-hidden="true">
    <div class="modal-dialog animated zoomIn">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Terms and Conditions</h4>
            </div>

            <div class="modal-body">
                <?= \app\models\Settings::getSettingsByName('payment_terms_and_conditions'); ?>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-primary" type="button">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- modal end -->
        

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
