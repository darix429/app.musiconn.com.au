<?php

use yii\helpers\Html;
use yii\helpers\Url;
//rupal
use app\models\InstrumentCategory;
//use app\models\PaymentTerm;
?>
<div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">

    <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Name</th>
		<th>Parent</th>
		<th>Level</th>
                <th>Action</th>
            </tr>
        </thead>

        <tbody>
            <?php
	    if (!empty($instrumentCategoryList)) {  
                foreach ($instrumentCategoryList as $key => $value) { 
			$parent = InstrumentCategory::find()->where(['uuid' => $value['parent_id']])->one();
			
                    ?>
                    <tr>
                        <td><?= $key + 1; ?></td>
                        <td><?= $value['name']; ?></td>
			<td><?= $parent['name']; ?></td>
			<td><?= $value['level']; ?></td>
                        <td>
                            	<a href="<?= Url::to(['/instrument-category/update/', 'id' => $value['uuid']]) ?>" class="badge badge-info badge-md" title="Update"><i class="fa fa-pencil"></i></a>
                                <a href="javascript:void(0)" data-uuid="<?= $value['uuid']; ?>" class="delete_instrument_category_link badge badge-danger badge-md" title="Delete"><i class="fa fa-trash"></i></a>
                                
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>

</div>
