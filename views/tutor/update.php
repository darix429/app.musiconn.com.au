<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tutor */

$this->title = Yii::t('app', 'Update Tutor: ' . $model->first_name, [
            'nameAttribute' => '' . $model->first_name,
        ]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tutors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->first_name, 'url' => ['view', 'id' => $model->uuid]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<?=

$this->render('_form', [
    'model' => $model,
    'userModel' => $userModel,
    'instrumentModel' => $instrumentModel,
    'tutorInstrumentModel' => $tutorInstrumentModel,
    'packegetypeModel' => $packegetypeModel,/** Added by pooja : Tutor wise plan rate **/
    'new' => $new,
])
?>
