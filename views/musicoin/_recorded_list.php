<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use app\libraries\General;
?>
<div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">
    <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Date</th>
                <th>Student</th>
                <th>Debit</th>
                <th>Credit</th>
                <th>Balance</th>
                <th>Summary</th>

            </tr>
        </thead>
        <tbody>
            <?php
            if (!empty($recordList)) {
                foreach ($recordList as $key => $value) {
                    ?>
                    <tr>
                        <td><?= $key + 1; ?></td>
                        <td ><?= \app\libraries\General::displayDateTime($value['created_at']); ?></td>
                        <td><?= $value['studentUu']['first_name'] . ' ' . $value['studentUu']['last_name']; ?></td>
                        <td> <?= ($value['debit'] == 0)? '-' : $value['debit']; ?></td>          
                        <td> <?= ($value['credit'] == 0)? '-' : $value['credit']; ?></td>          
                        <td> <?= $value['balance'];?></td>          
                        <td><?= $value['summary'];?></td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
</div>
