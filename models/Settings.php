<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%settings}}".
 *
 * @property string $uuid
 * @property string $name
 * @property string $value
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%settings}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'name', 'value'], 'string'],
            [['name', 'value'], 'required'],
	    [['name'], 'unique'],
	    [['value'], 'string', 'max' => 400],
	    
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'name' => Yii::t('app', 'Name'),
            'value' => Yii::t('app', 'Value'),
        ];
    }
    
    public static function getSettingsByName($name=''){
        $value = '';
        if (($model = Settings::find()->where(['name' => $name])->one()) !== null) {
            $value = $model->value;
        }
        return $value;
    }
}
