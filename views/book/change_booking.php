<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\web\View;
use yii\helpers\ArrayHelper;
use app\libraries\General;
use app\models\StudentTuitionBooking;

$tutorList = StudentTuitionBooking::getInstrumentWiseTutorList($step_data['step_1']['instrument_uuid']);
$model->cb_tutor_uuid = $step_data['step_2']['tutor_uuid'];
$tutorLists = array();

for ($i = 0; $i < count($tutorList); $i++) {
    $TutorInstrumentPlanOptions = StudentTuitionBooking::getTutorInstrumentPlanOptions($tutorList[$i]['online_tutorial_package_type_uuid'], $tutorList[$i]['uuid'], $tutorList[$i]['instrument_uuid']);
    $InstrumentWiseTutorPriceList = StudentTuitionBooking::getInstrumentWiseTutorPriceList($tutorList[$i]['online_tutorial_package_type_uuid'], $TutorInstrumentPlanOptions['standard_plan_rate_option']);

    $tutor30minPriceInCoin = \app\libraries\General::priceToMusicoins($InstrumentWiseTutorPriceList['half']);
    $tutor60minPriceInCoin = \app\libraries\General::priceToMusicoins($InstrumentWiseTutorPriceList['full']);
    if ($step_data['step_2']['tutor_half_hour_price_in_coin'] == $tutor30minPriceInCoin && $tutor60minPriceInCoin == $step_data['step_2']['tutor_hour_price_in_coin']) {
        $tutorLists[$i]['uuid'] = $tutorList[$i]['uuid'];
        $tutorLists[$i]['first_name'] = $tutorList[$i]['first_name'];
        $tutorLists[$i]['last_name'] = $tutorList[$i]['last_name'];
        $tutorLists[$i]['tutor_half_hour_price'] = $InstrumentWiseTutorPriceList['half'];
        $tutorLists[$i]['tutor_half_hour_price_in_coin'] = $tutor30minPriceInCoin;
        $tutorLists[$i]['tutor_hour_price'] = $InstrumentWiseTutorPriceList['full'];
        $tutorLists[$i]['tutor_hour_price_in_coin'] = $tutor60minPriceInCoin;
    }
}
$tutor_array = ArrayHelper::map($tutorLists, 'uuid', function($e) {
            // print_r($tutorList);
            // $TutorInstrumentPlanOptions = StudentTuitionBooking::getTutorInstrumentPlanOptions($e['online_tutorial_package_type_uuid'], $e['uuid'], $e['instrument_uuid']);
            // $InstrumentWiseTutorPriceList = StudentTuitionBooking::getInstrumentWiseTutorPriceList($e['online_tutorial_package_type_uuid'], $TutorInstrumentPlanOptions['standard_plan_rate_option']);
//            return $e['first_name'] . ' ' . $e['last_name'] . ' ($' . $InstrumentWiseTutorPriceList['half'] . ' / ½hr OR ' . '$' . $InstrumentWiseTutorPriceList['full'] . ' / hr)';
            return $e['first_name'] . ' ' . $e['last_name'] . ' (' . $e['tutor_half_hour_price_in_coin'] . ' coins / ½hr OR ' . $e['tutor_hour_price_in_coin'] . ' coins / hr)';
        });
$this->registerJs(
        ' 
//Define datepicker and onchange datepicker
//var todaydate = new Date();
var todaydate = "' . date("Y-m-d") . '";
$("#new_cb_when_startdate_div").datepicker({
    inline: true,
    //todayBtn: true,
    format: "yyyy-mm-dd",
    viewSelect: "Default",
    startDate: todaydate,
    todayHighlight:false
}).on("changeDate", function(e) {
    $("#book-cb_session_when_start").val( e.format() );
    cb_clear_selected_data();
    cb_get_timeslots();
});
cb_get_timeslots();
function cb_get_timeslots(){
    var cb_whendate = $("#book-cb_session_when_start").val();
    var cb_tutor_uuid =$("#book-cb_tutor_uuid").val();
    all_lesson_data = ' . $all_lesson_data . ';
    if(cb_whendate !== ""){
        $.ajax({
            type: "GET",
            url: "' . Url::to(['book/cb-get-timeslots']) . '",
            data: {"selected_when_date":cb_whendate,"cb_tutor_uuid":cb_tutor_uuid,"student_uuid":"' . $student_uuid . '","all_lesson_data":all_lesson_data},
            beforeSend: function() {
                blockBody();
            },
            success:function(result){
                $("#book-cb_start_hour").html(result.html);
               // $("#start_hour_array").val(JSON.stringify(result.result));
                NEW_CUSTOM.iCheck();
                unblockBody();
            },error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
    }
}
function cb_select_time(tm){
    if(tm !== ""){
    var cb_tutor_uuid =$("#book-cb_tutor_uuid").val();
        $.ajax({
            type: "POST",
            url: "' . Url::to(['book/cb-select-time']) . '",
            data: {"selected_time":tm,"student_uuid":"' . $student_uuid . '","cb_tutor_uuid":cb_tutor_uuid},
            beforeSend: function() {
                blockBody();
            },
            success:function(result){
                $("#cb_selected_date_data_parent").html(result.html);
                $("#book-cb_lesson_data").val(JSON.stringify(result.all_lesson_data));
                $("#btn_cnf_cb").show();
                unblockBody();
            },error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
    }
}
function cb_clear_selected_data(){
    $("#cb_selected_date_data_parent").html("<div><strong>No Lesson Available</strong></div>");
    $("#book-cb_lesson_data").val("");
    $("#btn_cnf_cb").hide();
}
function changeBooking(){
    cdate = $("#change_date_from").val();
    if(cdate !== ""){
        all_lesson_data = $("#book-all_lesson_data").val();
        cb_lesson_data = $("#book-cb_lesson_data").val();
        //alert(cb_lesson_data);
       // alert(all_lesson_data);
        $.ajax({
            type: "GET",
            url: "' . Url::to(['book/change-booking']) . '",
            data: {"current_datetime":cdate,"student_uuid":"' . $student_uuid . '","all_lesson_data":all_lesson_data,"cb_lesson_data":cb_lesson_data},
            beforeSend: function() {
                blockBody();
            },
            success:function(result){
                $("#selected_date_data_parent").html(result.html);
                $(".selected_sesson_html").val(result.html);
                $("#book-all_lesson_data").val(JSON.stringify(result.all_lesson_data));
                //$(".change-booking-model-body").html(result);
                //NEW_CUSTOM.iCheck();
                $("#change-booking-model").modal("hide");
                unblockBody();
            },error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
    }
}
');
?>
<style>
    .help-block{color: #a94442;}
    #new_cb_when_startdate_div .datepicker{
        /*  border: 1px solid #ccc;
        border-radius: 30px;
        padding: 10px;/*
        }
    </style>
    <?php
//    echo "<pre>";
//    print_r($model->getErrors());
//    echo "</pre>";
//    exit;
    $form = ActiveForm::begin([
                'action' => ['index'],
                'id' => 'frm_change_datetime',
                'enableClientValidation' => true,
                'validateOnChange' => true,
                'options' => ['class' => 'form-horizontal111']]);
    ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Rebook for (<?php echo date("d M Y - h:i A", strtotime($current_datetime)) ?>).</h4>
    </div>
    <div class="modal-body">



        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!--<br>-->
                    <div class="row">
                        <div class="col-md-12">
                            <?php
                            echo $form->field($model, 'cb_tutor_uuid', ['template' => '<span>{label}</span>{input}{error}'])
                                    ->dropDownList($tutor_array, ['class' => 'form-control myselect2', 'maxlength' => true])
                                    ->label('Select Tutor *')
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingfive">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseDate" aria-expanded="false" aria-controls="collapsefive">
                                        SELECT DATE AND TIME
                                    </a>
                                </h4>
                            </div>
                            <div class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingFour">
                                <div class="panel-body">
                                    <div class="col-md-4">
                                        <ul class="products-list product-list-in-box">
                                            <li>
                                                <div id="new_cb_when_startdate_div"></div>
                                                <?php echo $form->field($model, 'cb_session_when_start')->hiddenInput(['class' => 'form-control cb_session_when_start', 'maxlength' => true, 'value' => date("Y-m-d"),])->label(false) ?>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="col-md-4" style="border: 1px solid #ccc;padding: 7px 0px; border-radius: 30px;">
                                        <div class="col-md-12"><div class="sst_btn">Select Start Time</div><br></div>


                                        <?php
                                        $model_start_hour = $model->start_hour;
                                        $start_hour_json = (isset($step_data['step_4']['start_hour_array'])) ? $step_data['step_4']['start_hour_array'] : '{}';
                                        $start_hour_array = json_decode($start_hour_json, true);
                                        $start_hour_array = (!empty($start_hour_array)) ? $start_hour_array : [0 => 'dummy'];
                                        echo $form->field($model, 'cb_start_hour', ['template' => '{input}<div class="col-md-12">{error}</div>', 'options' => ['tag' => false]], ['class' => 'dasdasd'])
                                                ->radioList(
                                                        $start_hour_array, [
                                                    'item' => function($index, $label, $name, $checked, $value) use($model_start_hour, $start_hour_array) {

                                                        if (count($start_hour_array) == 1 && $value == 'dummy') {
                                                            return '<div class="col-md-12 text-center">Please select the date.</div>';
                                                        }
                                                        $checked_val = ($model_start_hour == $value) ? 'checked="checked"' : "";

                                                        $html = '<div class="col-md-6 ">';
                                                        $html .= '<ul class="products-list product-list-in-box">';
                                                        $html .= '<li class="st-p3">';
                                                        $html .= '<label class="contai_r">';
                                                        $html .= '<input type="radio" onclick="cb_select_time(\'' . $value . '\');" value="' . $value . '" name="' . $name . '" ' . $checked_val . '>';
                                                        $html .= '<span class="checkmark"></span>';
                                                        $html .= '</label>';
                                                        $html .= '<span class="mn_3">' . $label . '</span>';
                                                        $html .= '</li>';
                                                        $html .= '</ul>';
                                                        $html .= '</div>';
                                                        return $html;
                                                    }
                                                        ]
                                                )
                                                ->label(false);
                                        ?>


                                    </div>
                                    <div class="col-md-4">
                                        <ul class="products-list product-list-in-box">
                                            <li>
                                                <div class="col-md-12">
                                                    <div class="sst_btn sst_btn2" id="cb_selected_date_data_parent">
                                                        <div><strong>No Lesson Available</strong></div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                        <?php echo $form->field($model, 'cb_lesson_data')->hiddenInput(['class' => 'form-control all_lesson_data', 'maxlength' => true,])->label(false) ?>
                                        <input type='hidden' id='change_date_from' name='change_date_from' value='<?= $current_datetime ?>'>
                                    </div>
                                    <div class="col-md-12" style="">
<!--                                        <input type="hidden" name="step" value="4">-->
                                        <!--<input type="hidden" name="cb_start_hour_array" id="cb_start_hour_array" value="">-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 payment-errors text-danger"><?= (isset($error)) ? $error : ''; ?></div>
                </div>
            </div>
        </section>

        <div class="hidden_inputs ">
            <?php echo $form->field($model, 'student_uuid', ['template' => '{input}{error}'])->hiddenInput()->label(false); ?>
<!--            <input type="hidden" name="step" value="4">-->
        </div>


    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <!--<button type="button" class="btn btn-primary" onclick="save_buy_musicoin();">Save changes</button>-->
        <button  style="display:none;" id="btn_cnf_cb" type="button" onclick='changeBooking()' class="btn btn-primary">Save changes</button>
    </div>
    <?php ActiveForm::end(); ?>