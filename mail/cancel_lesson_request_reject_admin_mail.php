<?php

use yii\helpers\Html;
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['student_name']; ?>,</p>

            <p>Your request for the cancellation of lesson has been rejected by admin.</p>

            <p>The details of your rejected lesson cancellation are as follows:</p>

            <p><strong>Student Name:</strong> <?= $content['student_name']." ".$content['student_last_name']; ?></p>
            <p><strong>Tutor Name:</strong> <?= $content['tutor_name']." ".$content['tutor_last_name']; ?></p>
            <p><strong>Lesson Date:</strong> <?= $content['from_date']; ?></p>
            <p><strong>Lesson Time:</strong> <?= $content['from_time']; ?></p>

            <p>Please contact to Musiconn Admin on admin@musiconn.com.au or 1300 068 742 if you have any queries.</p>

            <p>Sincerely,<br>
            System Administrators</p>
        </td>
    </tr>
</table>
