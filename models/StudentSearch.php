<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Student;

/**
 * StudentSearch represents the model behind the search form of `app\models\Student`.
 */
class StudentSearch extends Student {

    public $fullname;

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
                [['student_uuid', 'first_name', 'last_name', 'email', 'phone_number', 'skype', 'street', 'city', 'state', 'country', 'postal_code', 'status', 'enrollment_id', 'birthdate', 'birthyear', 'fullname','created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $modeDatatable = false, $statusnot = '') {
        $query = Student::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['<>', 'student_uuid', Yii::$app->user->identity->student_uuid]);

        if (!empty($statusnot)) {
            if (is_array($statusnot)) {

                $query->andWhere(['not in', 'status', $statusnot]);
            } else {

                $query->andWhere(['<>', 'status', $statusnot]);
            }
        }

        $query->andFilterWhere(['status' => $this->status]);

        if (!empty($this->fullname)) {
            $query->andFilterWhere(['ilike', "concat(first_name,'',last_name)", $this->fullname]);
        }

        // grid filtering conditions
        $query->andFilterWhere(['ilike', 'student_uuid', $this->student_uuid])
                ->andFilterWhere(['ilike', 'first_name', $this->first_name])
                ->andFilterWhere(['ilike', 'last_name', $this->last_name])
                ->andFilterWhere(['ilike', 'email', $this->email])
                ->andFilterWhere(['ilike', 'phone_number', $this->phone_number])
                ->andFilterWhere(['ilike', 'skype', $this->skype])
                ->andFilterWhere(['ilike', 'street', $this->street])
                ->andFilterWhere(['ilike', 'city', $this->city])
                ->andFilterWhere(['ilike', 'state', $this->state])
                ->andFilterWhere(['ilike', 'country', $this->country])
                ->andFilterWhere(['ilike', 'postal_code', $this->postal_code])
                ->andFilterWhere(['ilike', 'status', $this->status])
                ->andFilterWhere(['ilike', 'enrollment_id', $this->enrollment_id])
                ->andFilterWhere(['ilike', 'birthdate', $this->birthdate])
                ->andFilterWhere(['ilike', 'birthyear', $this->birthyear]);
        if ($modeDatatable) {
            $result = $query->asArray()->orderBy('enrollment_id DESC')->all();
            return $result;
        }
        return $dataProvider;
    }

    public function searchRegistrationReport($params, $modeDatatable = false, $statusnot = '') {
        
        $query = Student::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($statusnot)) {
            if (is_array($statusnot)) {

                $query->andWhere(['not in', 'status', $statusnot]);
            } else {

                $query->andWhere(['<>', 'status', $statusnot]);
            }
        }

        $query->andFilterWhere(['status' => $this->status]);

        if (!empty($this->fullname)) {
            $query->andFilterWhere(['ilike', "concat(first_name,'',last_name)", $this->fullname]);
        }
        
        if (!is_null($this->created_at) && 
            strpos($this->created_at, ' - ') !== false ) {
            list($start_date, $end_date) = explode(' - ', $this->created_at);
            
            $query->andFilterWhere(['>=', '"student"."created_at"', strtotime($start_date)]);
            $query->andFilterWhere(['<=', '"student"."created_at"', strtotime($end_date)]);
        }

        // grid filtering conditions
        $query->andFilterWhere(['ilike', 'student_uuid', $this->student_uuid])
                ->andFilterWhere(['ilike', 'first_name', $this->first_name])
                ->andFilterWhere(['ilike', 'last_name', $this->last_name])
                ->andFilterWhere(['ilike', 'email', $this->email])
                ->andFilterWhere(['ilike', 'phone_number', $this->phone_number])
                ->andFilterWhere(['ilike', 'skype', $this->skype])
                ->andFilterWhere(['ilike', 'street', $this->street])
                ->andFilterWhere(['ilike', 'city', $this->city])
                ->andFilterWhere(['ilike', 'state', $this->state])
                ->andFilterWhere(['ilike', 'country', $this->country])
                ->andFilterWhere(['ilike', 'postal_code', $this->postal_code])
                ->andFilterWhere(['ilike', 'status', $this->status])
                ->andFilterWhere(['ilike', 'enrollment_id', $this->enrollment_id])
                ->andFilterWhere(['ilike', 'birthdate', $this->birthdate])
                ->andFilterWhere(['ilike', 'birthyear', $this->birthyear]);
        
//echo $query->createCommand()->getRawSql(); exit;        
        if ($modeDatatable) {
            $result = $query->asArray()->orderBy('enrollment_id DESC')->all();
            return $result;
        }
        return $dataProvider;
    }

}
