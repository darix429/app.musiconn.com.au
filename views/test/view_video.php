<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Category Demo');
$this->params['breadcrumbs'][] = $this->title;

$this->registerCss('
    ul, #myUL {
  list-style-type: none;
}

#myUL {
  margin: 0;
  padding: 0;
}

.caret {
  cursor: pointer;
  -webkit-user-select: none; /* Safari 3.1+ */
  -moz-user-select: none; /* Firefox 2+ */
  -ms-user-select: none; /* IE 10+ */
  user-select: none;
}

.caret::before {
  content: "\25B6";
  color: black;
  display: inline-block;
  margin-right: 6px;
}

.caret-down::before {
  -ms-transform: rotate(90deg); /* IE 9 */
  -webkit-transform: rotate(90deg); /* Safari */
  transform: rotate(90deg);  
}

.nested {
  display: none;
}

.active {
  display: block;
}
');

$this->registerJs(
        '
    $(document).ready(function(){
        

        
    });

    function videoShow(id){
        var url = "' . Url::to(['test/get-videos-from-category']) . '?id="+id;
        // console.log(toggler);
        $.ajax({
            type: "GET",
            url: url,
            beforeSend: function() {
                blockBody();
            },
            success:function(data){
                var i;
                var toggler = document.getElementsByClassName("caret");
                // console.log(toggler.length);
                console.log($(".hidden").length);
                for (i = 0; i < toggler.length; i++) {
                    toggler[i].addEventListener("click", function() {
                        //console.log(this.parentElement);
                        $(".nested").toggle("active");
                        this.classList.toggle("caret-down");
                        if($(".hidden").length == 1) {
                            console.log("if");
                            var category = $(".hidden").children(".category_name").text();
                            var category_uuid = $(".hidden").children(".category_uuid").text();
                            var category_parent_id = $(".hidden").children(".category_parent_id").text();
                            var html = "<ul class=\"nested\">"+
                                "<li><a href="+category_uuid+" id=\"video_search\" data-uuid="+category_uuid+"  data-parentid="+category_parent_id+"  data-name="+category+" data-toggle=\"tab\" class=\"nav-link\" onclick=\"videoShow("+category_uuid+")\">"+category+"</a></li>"+
                                "</ul>";
                            $("#video_search").after(html);
                        } else {
                            console.log("As");
                            $(".hidden").each(function(cat) {
                                var category = $(".hidden").children(".category_name").text();
                                var category_uuid = $(".hidden").children(".category_uuid").text();
                                var category_parent_id = $(".hidden").children(".category_parent_id").text();
                                console.log("UUID: " + category_uuid);
                                var category_array = [];
                                var category_uuid_array = [];
                                var category_parent_id_array = [];
                                category_array.push(category);
                                category_uuid_array.push(category_uuid);
                                category_parent_id_array.push(category_parent_id);
                                var html_new = "<a href="+category_uuid_array+" id=\"video_search\" data-uuid="+category_uuid_array+"  data-parentid="+category_parent_id_array+"  data-name="+category_array+" data-toggle=\"tab\" class=\"nav-link\"  onclick=videoShow("+category_uuid_array+")>"+category_array+"</a>";
                                // console.log(html_new);
                                html.push(html_new);
                                // console.log(html);
                            });
                            $("#video_search").after(html);
                        }
                        
                    });
                }
            
                // console.log("Hidden: " + $(".hidden").length);
            
                // if($(".hidden").length == "1") {
                //     var html = "<ul class=\"nested\">"+
                //             "<li><a href="+category_uuid+" id=\"video_search\" data-uuid="+category_uuid+"  data-parentid="+category_parent_id+"  data-name="+category+" data-toggle=\"tab\" class=\"nav-link\" onclick=videoShow("+category_uuid+")>"+category+"</a></li>"+
                //             "</ul>";
                //     $(".video_search").after(html);
                // } else {
                //     var html = [];

                //     $(".hidden").each(function(cat) {
                //         var category = $(this).children(".category_name").text();
                //         var category_uuid = $(this).children(".category_uuid").text();
                //         var category_parent_id = $(this).children(".category_parent_id").text();
                //         // console.log("UUID: " + category_uuid);
                //         var category_array = [];
                //         var category_uuid_array = [];
                //         var category_parent_id_array = [];
                //         category_array.push(category);
                //         category_uuid_array.push(category_uuid);
                //         category_parent_id_array.push(category_parent_id);
                //         var html_new = "<a href="+category_uuid_array+" id=\"video_search\" data-uuid="+category_uuid_array+"  data-parentid="+category_parent_id_array+"  data-name="+category_array+" data-toggle=\"tab\" class=\"nav-link\"  onclick=videoShow("+category_uuid_array+")>"+category_array+"</a>";
                //         // console.log(html_new);
                //         html.push(html_new);
                //         // console.log(html);
                //     });
                //     $(".video_search").after(html);
                // }
                
                unblockBody();
                $(".showVideo").html(data);
            },
            error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
    }

    function videoAllShow(){
            var url = "' . Url::to(['test/get-videos-from-category']) . '?id=";
            $.ajax({
                type: "GET",
                url: url,
                beforeSend: function() {
                    blockBody();
                },
                success:function(data){
                    unblockBody();
                    $(".showVideo").html(data);
                },
                error:function(e){
                    unblockBody();
                    showErrorMessage(e.responseText);
                }
            });
        }
  ', View::POS_END
);


?>

<div class="col-xl-12">
    <section class="box">
        <div class="col-lg-12 search_data">
            <ul id="myUL">
                <li>
                    <a href="#" id="all_video" data-name="Allvideo" data-toggle="tab" class="nav-link" onclick="videoAllShow();">
                        <i class="fa fa-home"></i> All Videos
                    </a>
                </li>
                <?php
                if (!empty($model)) {
                    foreach ($model as $key => $value) { ?>
                    <li class="categoy_video">
                        <a href="<?= $value['uuid']; ?>" id="video_search" data-uuid="<?= $value['uuid']; ?>"  data-parentid="<?= $value['parent_id']; ?>"  data-name="<?= $value['name']; ?>" data-toggle="tab" class="caret" onclick="videoShow('<?= $value['uuid']; ?>');">
                            <?= $value['name']; ?>
                        </a>
                    </li>
                    
                    <?php
                    }
                }
                ?>
            </ul>

            <div class="tab-content float-left  vertical col-xl-10 col-lg-10 col-md-3 col-3 left-aligned" id="myTabContent5">
                <?php
                if (!empty($model)) { ?>
                    <div id="<?= $value['uuid']; ?>" class="tab-pane showVideo fade active show">
                    </div>                    
                <?php
                }
                ?>
            </div>

            <?php
            if (!empty($CategoryList)) {
                foreach ($CategoryList as $key => $value) {
                    ?>

                    <div class="hidden" style="margin-right: 30px; display: none;">
                        <h4 class="category_name"><?= $value['name']; ?></h4>
                        <h4 class="category_uuid"><?= $value['uuid']; ?></h4>
                        <h4 class="category_parent_id"><?= $value['parent_id']; ?></h4>
                    </div>
                    <?php
                }
            } ?>

        </div>
    </section>
</div>