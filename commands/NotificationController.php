<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use app\models\BookedSession;
use app\models\User;
use app\models\Chat;
use app\libraries\General;
use Yii;


class NotificationController extends Controller {



    public function actionLessonStarting() {
        date_default_timezone_set('Australia/Sydney');
        $time = time();
        $now = Date("Y-m-d H:i:s", $time);
        $_15mins = Date("Y-m-d H:i:s", $time + (15*60));

        $notified_collection = array();
        $log_file = "/var/www/html/logs/lessonstart_notification";
        if (file_exists($log_file)) {
            $notified_collection = json_decode(file_get_contents($log_file));
        }

        $lessons = BookedSession::find()
            ->joinWith(["tutorUu", "studentUu"])
            ->where(["between", "start_datetime", $now, $_15mins])
            ->andWhere(["not in", "booked_session.uuid", $notified_collection])
            ->all();

        echo "\nNOW: $now\nFROM:$now \nTO: $_15mins\n\n";
        
        foreach($lessons as $lesson) {
            $tutor = $lesson->tutorUu;
            $student = $lesson->studentUu;
            $studentUser = User::find()->where(["student_uuid"=>$student->student_uuid])->one();
            $tutor_tz = General::getTutorTimezone($lesson->tutor_uuid);

            $content['tutor_first_name'] = $tutor->first_name;
            $content['tutor_last_name'] = $tutor->last_name;
            $content['student_first_name'] = $student->first_name;
            $content['student_last_name'] = $student->last_name;
            $content['session_pin'] = $lesson->session_pin;
            $content['uuid'] = $lesson->uuid;
            $content['start_datetime'] =        General::convertTimezone($lesson->start_datetime, Yii::$app->params['timezone'], $studentUser->timezone, 'd-m-Y H:i T');
            $content['tutor_start_datetime'] =  General::convertTimezone($lesson->start_datetime, Yii::$app->params['timezone'], $tutor_tz, 'd-m-Y H:i T');
    
            echo "\n". $lesson->uuid ."\n";
            var_dump($content);
            echo "\n";

            Yii::$app->mailer->compose('booked_session_start_cron_student_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                ->setFrom(Yii::$app->params['supportEmail'])
                ->setTo($student->email)
                ->setSubject("Musiconn Reminder: A Scheduled Lesson is due to Start")
                ->send();
    
            Yii::$app->mailer->compose('booked_session_start_cron_tutor_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                ->setFrom(Yii::$app->params['supportEmail'])
                ->setTo($tutor->email) 
                ->setSubject("Musiconn Reminder: A Scheduled Lesson is due to Start")
                ->send();

            array_push($notified_collection, $lesson->uuid);
        }

        
        file_put_contents($log_file, json_encode($notified_collection, JSON_PRETTY_PRINT));

    }

    public function actionMissedMessages() {
        date_default_timezone_set('Australia/Sydney');
        $time = time();
        $now = Date("Y-m-d H:i:s", $time);

        $notified_collection = array();
        $log_file = "/var/www/html/logs/missedmessage_notification";
        if (file_exists($log_file)) {
            $notified_collection = json_decode(file_get_contents($log_file));
        }

        $collection = Chat::find()
            ->joinWith(["tutor","student"])
            ->where(["NOT", ["messages"=>null]])
            ->andWhere(["not in", "chat.id", $notified_collection])
            ->all();

        $dateFormat = "Y-m-d H:i:s";
        foreach($collection as $data) {
            $tutorLastseen = date($dateFormat, strtotime($data->tutor_lastseen));
            $studentLastseen = date($dateFormat, strtotime($data->student_lastseen));

            $tutorUnseen = 0; $studentUnseen = 0;
            foreach($data["messages"] as $message) {
                $messageTime = date($dateFormat, ($message["timestamp"]));
                $receiverId = $message["to"];
                if ($receiverId == $data->tutor_uuid && $messageTime > $tutorLastseen) {
                    $tutorUnseen+=1;
                }
                if ($receiverId == $data->student_uuid && $messageTime > $studentLastseen) {
                    $studentUnseen+=1;
                }
            }

            if ($tutorUnseen > 0) {
                $content["receiverName"] = $data->tutor->first_name;
                $content["senderName"] = $data->student->first_name . " " . $data->student->last_name;
                $content["missedCount"] = $tutorUnseen;
                
                Yii::$app->mailer->compose('missed_messages', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                ->setFrom(Yii::$app->params['supportEmail'])
                ->setTo($data->tutor->email)
                ->setSubject("Musiconn Notice: You have some unread messages from ". $content["senderName"])
                ->send();
            }
            
            if ($studentUnseen > 0) {
                $content["receiverName"] = $data->student->first_name;
                $content["senderName"] = $data->tutor->first_name . " " . $data->tutor->last_name;
                $content["missedCount"] = $tutorUnseen;
                
                Yii::$app->mailer->compose('missed_messages', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                ->setFrom(Yii::$app->params['supportEmail'])
                ->setTo($data->tutor->email)
                ->setSubject("Musiconn Notice: You have some unread messages from ". $content["senderName"])
                ->send();
            }

            array_push($notified_collection, $data->id);
        }

        file_put_contents($log_file, json_encode($notified_collection, JSON_PRETTY_PRINT));
    }

}
