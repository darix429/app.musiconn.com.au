<?php

use yii\helpers\Html;
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear Musiconn Admin,</p>
            
            <p>A new student has registered in the booking system with the following details:</p>
            
            <ul>
                <li>Customer ID : <?= $content['student_id'] ?></li>
                <li>Name : <?= $content['student_name'].' '.$content['student_last_name'] ?></li>
                <li>Email/Username : <?= $content['username'] ?></li>
                <li>Registration Date : <?= $content['date'] ?></li>
            </ul>
            
            <p>Musiconn Booking System</p>
        </td>
    </tr>
</table>
