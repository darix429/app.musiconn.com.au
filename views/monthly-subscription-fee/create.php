<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MonthlySubscriptionFee */

$this->title = Yii::t('app', 'Create Monthly Subscription Fee');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Monthly Subscription Fees'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<!--<div class="monthly-subscription-fee-create">

    <h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

<!--</div>-->
