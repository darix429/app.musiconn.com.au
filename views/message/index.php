<?php
use richardfan\widget\JSRegister;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

$defaultAvatar = "data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAABC2wAAQtsBUNrbYgAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAABc7SURBVHja7Z15VJX3mcfjkphqzDSLc6bJONOmnjaTmWSm00wSE5tJ58TYSZzanp64ECMBVzR6YoyRWLVsbhi1ghoXgkajYlzqQhRFBQFBCYuAiggooFy8LO7gguI8T/JDQblw33vf/ff943OOiQnc+/ye7+e+931/ywO3b99+AFibdz8IeoR4gfgjMYGYQywkoog1xCZiB7GPSCOyieNEKeEkLgn4nw8TCcRm8f/zz5pMBBADiT7ES0QPoivqb21QBGsEvB3xFNGL8CWCia+JVBHg2wZSKuQSLl7br4nOGDcIAHge+GdEmPgTOI+oMzjkSrlFFBFbiemEj7hC6YTxhQDA/Z/uHI4xRAxRbrGwK6GeOCiuYF4lOqAHIADZAv8Q0ZP4lNhOnLNx4Nvigrg/MYL4KfoDArBr6LuIG2hbLHg5rycFRATxDtcMvQMBWP2Tvh+xjriCcCvmunhqMZZ4HD0FAVgh9B2I3kQ0cR4hVo1r4v7IW0R79BoEYLabeL3Ec3cnwqrLY8dg3DOAAIwO/qPiRl4pQmkIDcQeYhDxMHoSAtAr+DwhZzZxESE0DefEFdiv0KMQgFbBf1ZMzLmOwJmaeOI19CwEoFbwe4rHdw0Il+VE8Cp6GALw9MZeXyIZQbI8uyECCEBJ+P9EHEVwbMcuvppDj0MAroL/b8ReBMX2xBGvoOchgMbgP0ZEEjcRDulE8BwEIG/w2xOjiGqEQVpuEKEyL1OWNfyvi11xEALQuAjpDQjA/sHvLuaUo+lBS0TLtvBIluB3JKYQtWhy0Aa8nsMHArDX9loH0djAg5uEP4MArB3+wWK3WzQ08AS+YpzIV5AQgLWC35VYjQYGKsFXkN0hAGuE/2WiGE0LVKaKN3uBAMz9XH+y2HkWDQu02u6cbya3gwDMFf6nxWk2aFKgB7E8gxQCMEf4eePNGjQl0JmTdtiAxOrhn4R1+sBArhL+EIAxu+8uQgMCkxBl1X0JrRj+zuLMOTQeMBOZxD9CANqGvxtm9QETU8LHpkMA2oSfz6MvRJMBk1PBm8tAAOpP7qlCcwGLwE+lXoIA1HvMhwM1gdW4ZIU9Bswe/tFi9hUaClj1MeHbEIBn4f8IDQRssu1YfwhAWfiHYoIPsNkagqEQgHvh74/LftWb71J/v+CzA4eGlLw3Iix/SMDMnKFjwzNGfTwvjeE/87/jv+P/hv9b8R0W46Ae/IH2EQTQevjfFpdMaBgFUGCrfUfPzJ4wdXHiX5du3Lt9d2pa7vGTxWUVzgsV1TX1xG0PqeefwT+Lfyb/bP4d/Lv4d6L2HjEaAmg5/G+ImyZokta55v9heOb8JRsT96Vk5RSWnqn2IuBewb+bXwO/Fn5N/NowPm5dkfWDAJqH/yVs3eWaQcNCT34Wujxx264D6aUOZ61RgW8Lfm38Gvm18mvG2LmEH2u/DAHcPZILy3nvv6w/FzR7ZUJOPl3JmzTwbcGvnd8DvxeMaYs7DPWQWgBiem8FmuEuPsNCi5atik2i7951Vg3+vfB74ffE7w1j3Aye2t5NSgHwyimxeAKNQPiNmZ317Z6DGXYJvSv4PfJ7xZg323C0s1QC4LXTYvkkLvX9giu/3hCfbPfg38vqDbtT6L1jfccP8PL2DjIJIAqDHtQwYerifcWnHTWyhb+R4rLycx9PWbwfk76+Z5EUAuAtlGQf7AH+ISUJB7K/kzX497I3JfMw1eQMJBA0ydYC4E0UZX/W//7IGTknSs44EPzmFJwsqxw8csYxzBbUd46AnuF/TOykKu0Aj/x4XkpZhfMyAu9yHkHdyPFz07GXQNDTthIAH6Qg9lKXdmAnh0XtcVTV3ETQW8dRVX3rs9DlyZJLgM+4aG8nAUyReUA/mfrFPoRbGR9PWZwquQQm20IA9EZ6y7yqbOi4OQfLK6tvINTKOOOsujl0bHi2xAKo12O6sNbh7y7zXn4+w8PyS8rPXkSgPePkaccVqqHMswf5kNuulhQAvfCOkm/hffFoYclpBNk78gpO8lTxKxL30WqrCmCizN/hlqzcloAAq8PiFVtTJL8fMNhSAqAX/DOiVtYB8w2YmeOoqrmF8Kr2ZKBhSMDMPMl3GH7GSgKIk3iwrmfmFRQhuOqSkVvA34evS75oqKPpBUAv0kfmy7VxgZEpCKw2jAuMkP2rwBRTC4Be4OOEU+ZB2puSlY2wasOe5MwcyQXAX6u7m1kA0ZJv3VWIoGoLthoLijGlAMSmnlKv5opcvnkvQqotEcs27ceqwaDXTSUAekGdiALZN3osPu2oRkg134n4PHYfDspWa62AWgIIld3K4wIjExFQfRg7KSIVVwFBo0whAHohz+Ewj6Db2OBDP+KTMvIggCA+mOUxMwggDoMRdJ4aExN/9JsY1Li3vux9F2moAOgFvIJBCLo9JGBmJoKpL1Tzo+i9oJt8roaRAsCnP/Fp0FLc/deZidOWHEDvfc9eQwRAv7gniv8Dy1bHQgA6s2TlNgjgLn8yQgC7UPgfSDqYg9l/OrMvJSsfvXcH/jrUTjcB0C97FUW/+z2srMKJTT/03ywEJ0k3p6+eAtiNgt852ceJQBoD1f4ievAOyboIAJ/+9x/thTAaJoAL6MFm9NRDAPEoNARgEgHg2PHmbNFUAPQLXkORIQATCaAGPXjfyULPaikAfPpDAGYSQDV68D6iNBGAONcPBYYAzCSASvTg/VvSEU9pIYCFKC4EAAFYgtmqCoB+4MMEbrhAABCARc6kIB5VUwCDUFQIAAKwFJ+qKYA9KCgEAAFYilJ3pge7E/6fiscLKCoEAAFYi15qCCAYhYQAIABLstArAfDGg+JSAsWEACAA68FndHTwRgBvoYgQAARgaXp7I4AYFBACgAAsTbRHAhDHfF1DASEACMDaG9YSD3kigLEoHgQAAdiCfp4IYB8KBwFAALZgnSIB0P/Q5V25z2KHACAAO3GFM61EAO+gaBAABGArBioRQAQKBgFAAPbfLciVAApQMAgAArAVdS09DXA19x8FgwAgAAk2DW1JACNQKAgAApBjiXBLAtiEQkEAEIAt2d6qAHjhAIG91iEACMCenLt3jwAc+gEBQABy8UJrAsDafwgAArA3Y1oTwEEUCAKAAGxNTIsCoL/oRNSjQBAABGBryl0J4AUUxyMB4HRg4wTgRA96xDMtCcAHhVHO8tXf7kcYjWH56thk9KBH+LYkgOkojDLCI9ftQRCNZdaCtZCAcqJaEsBWFMZ9fANmZlMDNiCExuKoquGxOIKeVEReSwIoQmHc5ubBrGPHEUBzkJZ59ASPCfpS0cKgdncEQP/QmbiFwrhHwIT5aQieueAxQW8q4qmmAvg1CuI+MVv2pSJ05oLHBL2p/NSgRgH4oiBuc63U4axF6MwFjwl2sVb+JKBRAOEoiHsMGhZ6EoEzJzw26FG3CW4qgB0oiHv4fxieibCZEx4b9KjbfN1UADj/z03GBUamIGzmhMcGPeo2qd8LgP7QFcVwn48mL4QATAqPDXrUbZyNAuiBYkAAEICUPMICeAmFgAAgADk3B2EB9EEhIAAIQEr+yAIYiEJAABCAlExgAQSgEBAABCAlc1gAk1EICAACkJKFLIA5KAQEAAHIuS8ACyAKhYAAIAApWcMC2IxCQAAQgJRsYgEkoBAQAAQgJTtYAIdRCAgAApCSfVgIBAFAAPKSxgK4hEIoEkAywgYB2IRsFgBOA8YVAAQgJ8dZAA4UAgKAAKSkFNuBK94QJAICMCljJ0VAAAr3BGAB5KIQ7vPB6FmHETZz8v6oGTggRBmXWADYT10BA/xDyhE2c0Ljcxk9qoiLLIC9KIQibpVVOK8icObiyIlT5ehNxZxiAWxHIRQeDPI3HAxiNjbvSD6E3lRMJgtgPQqhdGvw2VkInbnwDZiZg95UzG4WQDQKoZiGLLrmRPDMwZadKenoSY+IYQEsRCGUQ584ueWVVfUIoNHHg1ff8hkeVoie9IjFOBbMC0LmfJWAEBoLjwF60WNCWQBBKITnXwWi1+7E2gCDmDZzRSJ60CvGswAmohDeMXVmdCJfiiKUel321zQEhizbj97zmiEsgDEohAoSmBG9D+HUPvibYpMODR45/Rh6ThX6sgB8UQjvGegfUoqQavqcP+O94WEn0Guq0pMF8BsUQh2yjpwoRFjV51R5RR3Vtw49pjq/ZAF0QyHUIXL5ZjwV0IBV3+w6iP7ShCcf4COC6Q/nUAzvGTZuTjoCqz5Dx4Zj30oNnmARHRoFkIqCqEJdWYWzDqFVj2NFpdW8AAu9pTrVnP1GAWA6sErExqdlILjqMX/JBmzyoQ2JTQUwCQVRhwlTFychuOrx3ogw7FilDQuaCqAfCqIal0sdzlqE13tSM44g/Nrh11QAz6Ig6vHV+l3YN1CdTT5xb0o7/rOpAB4k6lEU7BtoFvKLS6uoltfRT5rAWe90RwBCAsdRGPUesRw+VoSZgV4Q+vkqzPXXjrzG3DcVwFYURs2lwqswKchDSh3Ouv5+wZiboh2rWxLAbBRGPaiBnZgT4BlLVm5LQg9pyictCcAfhVGXiGWbEhFo5Sv+Bg0LPYX+0ZQ3WxLAqyiMJlcBeCSogDWb9hxA72hOt5YE0Jm4huKoy4KlG3EvwE1OOyuvDxwaUoa+0ZTyxsw3E4CQQDwKpPpVQCVdBVxBwNuGvzKhZzRnR2sC+AQFUp/5SzZit6A2OHnacZFkWYN+0ZwZrQngeRRIm6uAUofzMoLumuBw7O6rE39wKQAhAQeKpMFVwBcb9iLorpb8llRgxx9d4Ht8XdoSwEoUClcBejIuMAJLfg34/u9KAINQKG2Yu/ibeAS+Odt3p2agN3QjwB0BPCm2C0LB1L8KqCoqLa9C8O9s9lk7wD8EXzn1o3ubAhAS+A7F0oaxkyKwYYhgUtBSXPrrR3ZLWXclgDAUTDu2xqWkyR7+PcmZR3GlqSvBSgTwOgqmHQP8gx0nz1RckDX8ZRXO64OGhZaiF3TlRSUC4A1CLqFomu4duF9WAfx5+pfY6Ufn6b9EO7cFICSwBYXTdtOQuIT0TNnC/822xEyMve4sdZXz1gQwCoXTFl74UuI4K806gawjJ3ihz2WMve709UQAT2B1oPaM//OiZFl2+fEZji2+DYBnWP5IsQCEBL5GAbVnZUxcsv1n+0Vinb8xbGst420JAE8D9KH2jLPqhl3DH5eQno0xNgw/jwUgJJCPImpPWYXzml0FsCk26RDG2BDO80Y/3gpgPAoJAUAAlmRuW/l2RwC4GQgBQADWg09U/rnXAsDNQAgAArAkse5k210B4GYgBAABWIs+qgkANwMhAAjAUhS4mvrrjQBwMxACgACswTh3c61EALgZCAFAAOaHp1o/qroAcDMQAoAALMEiJZlWKgDcDIQAIABz8y+aCUBIACe3QgAQgDmJV5pnTwTQE4WGACAAU9JPcwFgsxAIAAIwJXlEe70E8BxxE0WHACAA0/A7T7LskQCEBKJRdAgAAjAFuzzNsTcC6E5cRfHV2Q+AgtJgVwFs252Kcya0XfTzgu4CEBL4HAPgPT7DwwrtvBtQRm5BMcZZM6K8ybC3AnicuIBB8I4RH809ZPNzAPhKEYeAqM8V4ieGCUBI4DMMhHdMDotKtPuegAP8Q8ox1qrzF2/zq4YAOhM44NEL1m9NsP1RYf4fzs7CWKsKZ66L4QIQEhiJAfEMPiasvLKq3u4CWBkTh4NA1cVfjeyqJYCOxAkMinLmLIxJkOFcgNPOyhv9/YIrMeaqkOPJpB/NBCAk8A4GRjFXC0vO1MhyMtCsBWuxjkQdequVW9UEICQQhcFxn0XRW6Q6IPREyenz/f1w6KyX7FQzs2oLoCtxCoPUNuMCI1NkPBl4e3xqlpi8gj5QDsvzGdMKQEjgDTzzbZ33Rkw/XlbhrJP1ePAFyzbhq4Bn+KmdV9UFICQwD4PVMu+PmpF3tLDEIWv4G+FDUdEPitioRVa1EsDDxDEMWjMa/jJrRaIMj/zcwVFV00BXAolYT+IWPInqccsIQEjgRaIeg/f9s/6z23enfofgt7xOYPDI6fiwaOWDg3hTq5xqJgAhgWDZd2idtWBtQqnDWYuwu4aviuZ98U1if7/gGgT+PuZpmVGtBcAThDIkHLQbgcHL9p8oOV2NgLtPSfnZK58vWg8R3CWX6GRZATTZPUia73mjP/lr6uFjhSUINETgJXwGx/Na51NzAQgJTLD7gH0wetbhpIM5RxBgiEAlxuuRTb0E0J5ItOVmHsNCi7bsTElHYDUVQa1kIoh/182z/SwhACGBbkSxne7sf7lmR1J5ZfVNhBQiUBF+b0/plUvdBCAk8Cxx3urTMcMjY3Bn33gR7LehCG5o+cjPcAEICfxWvFHLDc5nocsTZVq9BxHojq/eedRdAEICH1hpIsboiXxnv6gUoYMINGSaEVk0RABCAmGmv7M/ZlZ28qHcowgZRKAxXxqVQyMF0I5YZ9ZturfGHcCdfYhAD+J4wpx0AhAS6EQcMNGd/Yov1+5IdlRV30KQIAIdyOY9NIzMoKECEBJ4kigy+s4+780n8xp9iEB3yvR83GdaAQgJ/JI4Z8AgXJ+MO/sQgf7wYTr/aobsmUIATXYSuq7Xnf0xny44kJNfXIZgQAR6f+jwo3Cz5M40AhASGKD1HgJ+Y2ZnJ6fnHkMQgEEiGGymzJlKAEIC/6fF6kG+s79tF+7sA5ciSNJYBLyxx2iz5c10AmgyW/CyWifvRK/diTv7wEgR8MxXHzNmzZQCEBJ42Zsbg/39gi7OWbged/aB0SKoI942a85MKwAhgeeJCsV39sOiEgtLcWcfGC4Cvtvfy8wZM7UAhAR6ECXuFHzE+LmHco/jzj7QTARKrkjPEv9u9nyZXgBCAt2J460Uu3bZqtgkNCvQkqKy8guBwcv4UJObbYSfT8fqYYVsWUIAQgJ/L6ZONiv24BHT8zPzThSjQYFeHMrOL+SFYi7Cf5R42iq5sowAhAR+3GTtQMO0mSsSzjirrqMpgREHm/DuRPdcDRwinrBSpiwlACGBLoNHTt8Yl5CeiUYERrN7f0bOAL9gJ/XlHuIRq+XJcgJgqPAPE8vRgMAM5B4vXqb1/v0QQMsi8CeuogmBQVwjAqycIUsLQEjgV8RJNCPQmVPEi1bPj+UFICTwGBGLpgQ6sZ17zg7ZsYUAhATaEVMIzPkHWsFnQARyr9klN7YRQBMR9Caq0KxAZSqI/7ZbXmwnACGB7kQymhaoRALxD3bMii0F0OQrwRjiEhoYeMhlYizR3q45sa0A7rkawA1CoJRviX+yez5sL4AmIhhEVKKxQRtwj/jIkgtpBCAk8CSxGk0OXLCKeEKmTEglgCYi+B2Bs/5A00k9fWTMgpQCEBJ4hJhLYDWhvNQT84kusuZAWgE0EcE/EysxgUgqeKzXED1k73/pBdBEBM8Rf0M4bM9W4nn0PATgSgSviIkfCIu92EO8jB6HANwVQR8Cm45YnzTif9DTEICnswn7E0cQJMtxmPg9+hgCUEsGvyU2iRVhCJh57+pvIN5Az0IAWk4tno5ZhabiLBFCPI0ehQD0EkEn4n3iEAJoGCliiveD6EkIwEgZ/BfxldgjDsHUllqxIex/oPcgALOJ4MfiqmALNitVlauiplzbv0OvQQBWkEEX4l0iRqwrR5CVwfs4rBM17IKeggCsLAM+x+D3YsrxOYTbJXyi8wqiL99jQe9AAHaUwYPEW2IhUprki5FuEOliQc6bREf0CAQg49XBa8RE8T3XafPNNngu/iTiN8SP0AMQALhfCj2IIcQSIteik49uide+RLyXHhhbCAB4JoSOxM/FV4dRxBxis5juauSmp3VimjR/qs8jPiT+l/gF8RDGDgIA+giim1jF6CMOruCZiguIKPEUIlascPyOyCfKxM3IG4LzxBmigMgSW6rHERvF/IbFQjrTCF+iF/ETOx2QISv/D/94Y2Ny3YIVAAAAAElFTkSuQmCC";

$this->title = Yii::t('app', 'Messages');
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_icon'] = 'message.png';

function timeago($time, $tense='ago') {
    static $periods = array('year', 'month', 'day', 'hour', 'minute', 'second');

    if(!(strtotime($time)>0)) {
        return trigger_error("Wrong time format: '$time'", E_USER_ERROR);
    }

    $now  = new DateTime('now');
    $time = new DateTime($time);
    $diff = $now->diff($time)->format('%y %m %d %h %i %s');
    $diff = explode(' ', $diff);
    $diff = array_combine($periods, $diff);
    $diff = array_filter($diff);
    $period = key($diff);
    $value  = current($diff);

    if(!$value) {
        $period = 'seconds';
        $value  = 0;
    } else {
        if($period=='day' && $value>=7) {
            $period = 'week';
            $value  = floor($value/7);
        }
        if($value>1) {
            $period .= 's';
        }
    }
    return "$value $period $tense";
}

?>
<style>
    .message-container {
        width: 100%; height: 70vh;
    }
    .message-container td {
        vertical-align: top;
    }
    .connection-section {
        width: 280px;
        min-width: 200px;
        background: #f0f0f0;
    }

    .connection-list {
        list-style: none;
        margin: 0px; padding: 0px;
    }
    .connection-list li {
        padding: 10px 0px;
    }
    .connection-list li a {
        color: #32323A;
        font-weight: 1000;
    }
    .connection-list .active, .connection-list .active a{
        color: #FA8564;
        cursor: pointer;
        font-weight: 700;
        text-decoration: none;
    }
    /* .connection-list li:hover a:after, .connection-list .active:after {
        content: ' \227b';
    } */
    .avatar {
        height: 40px; width: 40px;
    }

    .info-section {
        height: 50px; width: 100%;
        background: #eaeaea;
        font-weight: bolder;
        display: flex;
        align-items: center;
        justify-content: center;
    }


    .message-section {
        background: #fff;
    }
    .message-wrapper { 
        width: 100%;
        display: flex;
        padding: 10px 5px;
    }
    .balloon {
        border-radius: 30px;
        padding: 5px 20px 5px 5px;
        background: #1FB5AC;
        color: #fff;
        font-size: 1.2em;
    }
    .balloon a {
        color: inherit;
        font-family: 'courier new';
        font-size: 0.9em;
        text-decoration: underline;

    }
    .self {
        justify-content: flex-end;
    }
    .self .balloon {
        background: #9972B5;
        padding: 15px 20px;
    }
    .attachment {
        padding: 10px 20px 15px 20px;
        margin-top: 10px;
    }
    .attachment, .attachment a {
        color: #000;
    }


    .message-actions {
        background: #fff;
    }
    .flex-container {
        display: flex;
    }
    .message-input {
        width: 100%; height: 100%;
        resize: none;
        outline: none;
        border: none;
        border-top: solid 1px #aaa;
        font-size: 1.2em;
        text-indent: 10px;
    }
    .form-group, .help-block, .help-block-error {
        padding: 0px !important;  margin: 0px !important;
    }
    .btn-attach{
        border: none;
        height: 0; width: 0;
        overflow: hidden;
        padding: 0; margin: 0;
        white-space: nowrap;
        display: none;
    }
    .btn-attach-trigger {
        border: none;
        width: 30px; height: 100%;
        color: #fff;
        background: #333;
        outline: none;
        font-weight: bolder;
        font-size: 2em;
    }

    .msg-timestamp {
        color: #888;
    }
    

</style>

<section class="content mt-3">
    <table class="message-container">
        <tr>
            <td rowspan="3" class="connection-section">
                <?php if (!in_array($user_role, ["TUTOR","STUDENT"])) { ?>
                    <input type="text" class="filter-user form-control" style="width: 92%;" placeholder="search user">
                <?php } ?>
                <ul class="connection-list" style="height: <?php echo $user_role=="STUDENT"?"60vh":"85vh"; ?>; overflow-y: scroll;">
                    <?php if ($user_role == "STUDENT"){ ?>
                        <li class='contact-item' id="admin"><a href='?to=admin'><img src='<?= $defaultAvatar ?>' class='avatar'/> Musiconn Admin</a></li>
                    <?php
                        }
                        foreach($contacts as $contact) {
                            $contact_uuid = empty($contact->student_uuid)? $contact->uuid: $contact->student_uuid;
                            $active = $to == $contact_uuid? 'active':'';
                            echo "<li class='$active contact-item' id='$contact_uuid' search_val='".($contact->first_name." ".$contact->last_name)."'><a href='?to=$contact_uuid'><img src='$defaultAvatar' class='avatar'> ".$contact->first_name." ".substr($contact->last_name,0,1).".</a></li>";
                        }
                    ?>

                </ul>
            </td>
            <td class="info-section">
                <span id="target_name"></span>
            </td>
        </tr>
        <tr>
            <td class="message-section">
                <div class='balloons-container' style="overflow-y: scroll; height: <?php echo $user_role=="STUDENT"?"60vh":"72vh"; ?>;">
                    <?php 
                        if (isset($to)) {
                            if (!empty($chat)) {
                                $messages = ($chat["messages"]);
                                foreach($messages as $message) {
                                    $selfmessage = $from == $message["from"];

                                    if (in_array($user_role,["OWNER","ADMIN"]) && in_array($message["from"], $admin_uuids)) {
                                        $selfmessage = true;
                                    }

                                    echo '
                                    <div class="message-wrapper '.($selfmessage?"self":"").'" >
                                        <div class="balloon">
                                            '.(($from != $message["from"])?'<img src="'.$defaultAvatar.'" class="avatar" alt="">':'').'
                                            '.($message["type"]=="attachment"?"<a target='_blank' href='".$attachment_url.$message["content"]."'>":"").'
                                            '.htmlspecialchars($message["content"]).'
                                            '.($message["type"]=="attachment"?"</a>":"").'
                                        </div>
                                    </div>
                                    <div style="margin-top: -12px;" class="msg-timestamp '.($selfmessage?"text-right mr-4":"ml-4").'"><small>'.(timeago(date("Y-m-d H:i", $message["timestamp"]))).'</small></div>';
                                }
                            }
                        } else {
                            ?>
                            <div style="height: 100%; display: flex; align-items: center; justify-content: center;">
                                <div style="font-size: 2em; color: #ddd; font-weight: bolder; text-align: center;">
                                    <!-- <span>＼(°ロ＼)</span><br> -->
                                    Select a name from the left panel<br>to start a conversation
                                </div>
                            </div>
                            <?php
                        }
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td class="message-actions">
                <?php
                    $form = ActiveForm::begin(["id"=>"message-form", "options"=>array("enctype"=>"multipart/form-data")]);
                ?>
                <div class="flex-container">
                    <div class="flex-item">
                        <?php if (!empty($to)) { ?>
                        <?=
                            $form->field($model, "attachment")
                            ->fileInput([
                                "class"=>"btn-attach", 
                                "title"=>"send file",
                                "disabled"=>empty($to)
                            ])
                            ->label(false)
                        ?>
                        <button type="button" class="btn-attach-trigger" title="Send file">
                            &#128206;
                        </button>
                        <?php } ?>
                    </div>
                    <div class="flex-item" style="flex-grow: 1">
                        <?php 
                        
                        if (!empty($to)) { ?>
                            <?= 
                                $form->field($model, "content")
                                ->textarea(["class"=>"message-input","placeholder"=>"type here...", "autofocus"=>true, "disabled"=>empty($to)])
                                ->label(false)
                            ?>
                            <?= $form->field($model, "role")->hiddenInput(["value"=>$user_role])->label(false); ?>
                            <?= $form->field($model, "type")->hiddenInput(["value"=>"message"])->label(false); ?>
                            <?= $form->field($model, "tutor_uuid")->hiddenInput(["value"=>$tutor_uuid])->label(false); ?>
                            <?= $form->field($model, "student_uuid")->hiddenInput(["value"=>$student_uuid])->label(false); ?>
                            <?= $form->field($model, "status")->hiddenInput(["value"=>""])->label(false); ?>
                            <?= $form->field($model, "timestamp")->hiddenInput(["value"=>time()])->label(false); ?>
                            <?= $form->field($model, "to")->hiddenInput(["value"=>$to])->label(false); ?>
                        <?php } ?>
                    </div>
                </div>
                <?php
                    ActiveForm::end();
                ?>
            </td>
        </tr>
    
    </table>



</section>


<?php JSRegister::begin(); ?>
<script>
<?php if (!empty($to)){ ?>
WS = new WebSocket('<?= $websocket_url ?>?id=<?= $from ?>&to=<?= $to ?>&role=<?= $user_role ?>');
wsStatus = "OFFLINE";
WS.onopen = function(e) {
    wsStatus = "ONLINE";
};
function updateAliveStatus() {
    var data = {
        Message: {
            type: "status",
            role: "<?= $user_role ?>",
            id: "<?= $from ?>",
            to: "<?= $to ?>"
        }
    };
    WS.send($.param(data));
}

setInterval(() => {
    if (wsStatus == "ONLINE") {
    updateAliveStatus();
    }
}, 10000);

// file upload
$(".btn-attach-trigger").on("click", function(e){
    e.preventDefault();
    $(".btn-attach").click();
});

$(".btn-attach").on("change", function(){
    const file = this.files[0];
    const fileType = file["type"];
    var errors = [];

    console.log("attachment selected", file);

    var validTypes = [
        "image/gif", "image/jpeg", "image/png",
        "video/mp4", "video/mov", "video/wmv", "video/x-matroska",
        "audio/mpeg", "audio/wav"
    ];
    
    if (!validTypes.includes(fileType)) {
        errors.push("Invalid file type " + fileType);
    }

    if (file.size > 25000000) {
        errors.push("Max upload size is 25Mb");
    }

    if (errors.length > 0) {
        console.error(errors);
        alert(errors);
    } else {
        console.log("proceed");
        var url = $("#message-form").attr("action");
        $.ajax({
            url: url,
            type: "POST",
            data: new FormData(document.getElementById("message-form")),
            processData: false,
            contentType: false,
            enctype: 'multipart/form-data',
            success: function(data) {

                console.log("ATTACH RESULT", data);
                const filename = data;
                const formData = $("#message-form").serializeArray();
                var i=0;
                formData.forEach(data => {
                    if (data.name == "Message[type]") {
                        formData[i].value = "attachment";
                    }
                    if (data.name == "Message[content]") {
                        formData[i].value = filename;
                    }
                    if (data.name == "Message[to]") {
                        formData[i].value = '<?= $to ?>';
                    }
                    i++;
                })
                
                appendAttachmentBalloon(filename, true);
                WS.send($.param(formData));

                unblockBody();
            },
            error: function() {
                console.error("ERROR");
                unblockBody();
            }
        });
    }
});

$("#target_name").html($("#<?= $to ?>").text());
<?php } ?>

$(".filter-user").on("keyup",function(e){
    var key = $(this).val();
    console.log("search:",key);
    $(".contact-item").each(function(){
        var item = $(this);
        item.hide();
        var searchVal = item.attr("search_val");
        if (searchVal.includes(key)) {
            item.show();
        }
    })
});
</script>
<?php JSRegister::end(); ?>