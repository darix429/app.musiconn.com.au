<?php

namespace app\controllers;

use Yii;
use app\models\Tutor;
use app\models\TutorSearch;
//use app\models\Instrument;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\libraries\General;
use app\models\ChangePassword;
use yii\web\UploadedFile;
use app\models\Settings;
use yii\filters\AccessControl;
use app\models\User;
use app\models\TutorUnavailability;
use app\models\PremiumStandardPlanRate;
use app\models\OnlineTutorialPackageTutorwise;
use app\models\StandardPlanOptionRate;
use app\models\OnlineTutorialPackageType;
use yii\helpers\ArrayHelper;
use app\models\TutorCategoryHistory;
use app\models\TutorStandardRateHistory;
use app\models\TutorInstrumentSearch;

/**
 * TutorController implements the CRUD actions for Tutor model.
 */
class TutorController extends Controller {

    /**
     * {@inheritdoc}
     */
    public $enableCsrfValidation = false;

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => Yii::$app->user->can('/tutor/index'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['create'],
                        'allow' => Yii::$app->user->can('/tutor/create'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['update'],
                        'allow' => Yii::$app->user->can('/tutor/update'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => Yii::$app->user->can('/tutor/delete'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['generate-password'],
                        'allow' => Yii::$app->user->can('/tutor/generate-password'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['profile'],
                        'allow' => Yii::$app->user->can('/tutor/profile'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['profile-image'],
                        'allow' => Yii::$app->user->can('/tutor/profile-image'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['change-password'],
                        'allow' => Yii::$app->user->can('/tutor/change-password'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['account-enabled'],
                        'allow' => Yii::$app->user->can('/tutor/account-enabled'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['account-disabled'],
                        'allow' => Yii::$app->user->can('/tutor/account-disabled'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['working-plan'],
                        'allow' => Yii::$app->user->can('/tutor/working-plan'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['reset-password'],
                        'allow' => Yii::$app->user->can('/tutor/reset-password'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['availability'],
                        'allow' => Yii::$app->user->can('/tutor/availability'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['available'],
                        'allow' => Yii::$app->user->can('/tutor/available'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['get-availability-calender-events'],
                        'allow' => Yii::$app->user->can('/tutor/get-availability-calender-events'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['booking-availability'],
                        'allow' => 'true',
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['get-booking-availability-calender-events'],
                        'allow' => 'true',
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['tutor-delete-unavailability'],
                        'allow' => 'true',
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['tutor-get-unavailability'],
                        'allow' => 'true',
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['apps'],
                        'allow' => Yii::$app->user->can('/tutor/apps'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['assign-plan'],
                        'allow' => Yii::$app->user->can('/tutor/assign-plan'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['assign-instrument-plan'],
                        'allow' => Yii::$app->user->can('/tutor/assign-instrument-plan'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['get-option-rate'],
                        'allow' => Yii::$app->user->can('/tutor/get-option-rate'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['defaultplanentry'],
                        'allow' => 'true',
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

//    public function beforeAction($action)
//    {
//      echo '<script>document.getElementById("preloader").style.property = "display:block";</script>';
//      return parent::beforeAction($action);
//    }

    /**
     * Lists all Tutor models.
     * @return mixed
     */
    public function actionIndex() {

        $searchModel = new TutorSearch();
        $searchModel->status = "ENABLED";
        $tutorList = $searchModel->search(Yii::$app->request->queryParams, true);

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_tutor_list', [
                        'tutorList' => $tutorList,
            ]);
        } else {
            return $this->render('index', [
                        //'inactiveAdmin' => $inactiveAdmin,
                        'tutorList' => $tutorList,
                        'searchModel' => $searchModel,
            ]);
        }
    }

    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate() {

        $model = new Tutor();
        $model->scenario = 'tutor_create';
        $model->state_text = $model->state;
        $instrumentModel = \app\models\Instrument::find()->where(['status' => 'ACTIVE'])->all();

        /**
         * start
         * Devloped By : Pooja Beladiya
         * Task : Tutor wise plan rate
         */
        $packegetypeModel = \app\models\OnlineTutorialPackageType::find()->all();


        /**
         * End
         * Task : Tutor wise plan rate
         */
        $userModel = new \app\models\User();
        $userModel->scenario = 'tutor_create';

        $model->attributes = Yii::$app->getRequest()->post('Tutor');
        $userModel->attributes = Yii::$app->getRequest()->post('User');

        $tutorInstrumentModel = new \app\models\TutorInstrument();
        $tutorInstrumentModel->scenario = 'instrument_selection';
        $tutorInstrumentModel->attributes = Yii::$app->getRequest()->post('TutorInstrument');

        $model->email = $userModel->email = $userModel->username;
        $userModel->role = 'TUTOR';
        $userModel->auth_key = Yii::$app->security->generateRandomString();
        $userModel->created_at = $userModel->updated_at = time();
        $model->join_date = $model->update_at = date('Y-m-d H:i:s', time());
        if ($model->load(Yii::$app->request->post()) && $userModel->load(Yii::$app->request->post())) {

            $UploadedFile = UploadedFile::getInstance($model, 'profile_image');
            if (!empty($UploadedFile)) {
                $model->profile_image = file_get_contents($UploadedFile->tempName);
            } else {
                $model->profile_image = file_get_contents(Yii::$app->params['theme_assets']['path'] . 'images/profile.png');
            }

            if ($model->validate() && $userModel->validate()) {

                $model->state = ($model->country == 'other') ? $model->state_text : $model->state;
                $model->contract_expiry = date('Y-m-d 23:59:59', strtotime($model->attributes['contract_expiry']));
                $model->child_certi_expiry = date('Y-m-d 23:59:59', strtotime($model->attributes['child_certi_expiry']));
                $package_type_model = OnlineTutorialPackageType::find()->where(['uuid' => $model->online_tutorial_package_type_uuid])->asArray()->one();
                $package_type = $package_type_model['name'];
                if ($package_type == "STANDARD") {
                    $model->standard_plan_rate_option = "1";
                }
                if ($model->save(false)) {

                    $password_string = $userModel->password_hash;
                    $userModel->password_hash = Yii::$app->security->generatePasswordHash($userModel->password_hash);
                    $userModel->tutor_uuid = $model->uuid;
                    $userModel->first_name = $model->first_name;
                    $userModel->last_name = $model->last_name;
                    $userModel->phone = $model->phone;
                    $userModel->status = 10;


                    $userModel->save(false);

                    $getInstrument = $model->instrumentList;
                    foreach ($getInstrument as $key => $value) {
                        $tutorInstrumentModelj = new \app\models\TutorInstrument();
                        $tutorInstrumentModelj->tutor_uuid = $model->uuid;
                        $tutorInstrumentModelj->instrument_uuid = $value;
                        $tutorInstrumentModelj->save(false);
                    }


                    $TutorCategoryHistorymodel = new TutorCategoryHistory();
                    $TutorCategoryHistorymodel->tutor_uuid = $model->uuid;
                    $TutorCategoryHistorymodel->online_tutorial_package_type_uuid = $model->online_tutorial_package_type_uuid;
                    if (in_array(Yii::$app->user->identity->role, ["OWNER", "ADMIN", "SUBADMIN"])) {
                        $TutorCategoryHistorymodel->changed_by_uuid = Yii::$app->user->identity->admin_uuid;
                        $TutorCategoryHistorymodel->changed_by_role = Yii::$app->user->identity->role;
                    }

                    $TutorCategoryHistorymodel->action = "ADD";
                    $TutorCategoryHistorymodel->datetime = date('Y-m-d H:i:s', time());
                    $TutorCategoryHistorymodel->save(false);

                    $package_type_model = OnlineTutorialPackageType::find()->where(['uuid' => $model->online_tutorial_package_type_uuid])->asArray()->one();
                    $package_type = $package_type_model['name'];

                    if ($package_type == "STANDARD") {

                        foreach ($model->instrumentList as $key => $value) {

                            $TutorStandardRateHistorymodel = new TutorStandardRateHistory();
                            $TutorStandardRateHistorymodel->tutor_uuid = $model->uuid;
                            $TutorStandardRateHistorymodel->instrument_uuid = $value;


                            if ($package_type == "STANDARD") {
                                $TutorStandardRateHistorymodel->option = "1";
                            } else {
                                $TutorStandardRateHistorymodel->option = "0";
                            }
                            if (in_array(Yii::$app->user->identity->role, ["OWNER", "ADMIN", "SUBADMIN"])) {
                                $TutorStandardRateHistorymodel->changed_by_uuid = Yii::$app->user->identity->admin_uuid;
                            } else if (in_array(Yii::$app->user->identity->role, ["TUTOR"])) {
                                $TutorStandardRateHistorymodel->changed_by_uuid = Yii::$app->user->identity->tutor_uuid;
                            }

                            $TutorStandardRateHistorymodel->changed_by_role = Yii::$app->user->identity->role;
                            $TutorStandardRateHistorymodel->action = "ADD";
                            $TutorStandardRateHistorymodel->change_tutor_category = "NO";

                            $TutorStandardRateHistorymodel->online_tutorial_package_type_uuid = $model->online_tutorial_package_type_uuid;
                            $TutorStandardRateHistorymodel->tutor_category_history_uuid = $TutorCategoryHistorymodel->uuid;
                            $TutorStandardRateHistorymodel->datetime = date('Y-m-d H:i:s', time());
                            $TutorStandardRateHistorymodel->save(false);


                            $planmodel = PremiumStandardPlanRate::find()->where(['online_tutorial_package_type_uuid' => $model->online_tutorial_package_type_uuid])->asArray()->All();

                            foreach ($planmodel as $plan_row) {

                                $online_tutorial_package_uuid = $plan_row['online_tutorial_package_uuid'];
                                $premium_standard_plan_rate_uuid = $plan_row['uuid'];
                                $standard_plan_rate_option = $plan_row['default_option'];

                                $tutorial_plan = new OnlineTutorialPackageTutorwise();
                                $tutorial_plan->tutor_uuid = $model->uuid;
                                $tutorial_plan->instrument_uuid = $value;
                                $tutorial_plan->online_tutorial_package_type_uuid = $model->online_tutorial_package_type_uuid;
                                $tutorial_plan->online_tutorial_package_uuid = $online_tutorial_package_uuid;
                                $tutorial_plan->premium_standard_plan_rate_uuid = $premium_standard_plan_rate_uuid;
                                $tutorial_plan->standard_plan_rate_option = $standard_plan_rate_option;
                                $tutorial_plan->save(false);
                            }
                        }
                    } else {

                        $TutorStandardRateHistorymodel = new TutorStandardRateHistory();
                        $TutorStandardRateHistorymodel->tutor_uuid = $model->uuid;


                        if ($package_type == "STANDARD") {
                            $TutorStandardRateHistorymodel->option = "1";
                        } else {
                            $TutorStandardRateHistorymodel->option = "0";
                        }
                        if (in_array(Yii::$app->user->identity->role, ["OWNER", "ADMIN", "SUBADMIN"])) {
                            $TutorStandardRateHistorymodel->changed_by_uuid = Yii::$app->user->identity->admin_uuid;
                        } else if (in_array(Yii::$app->user->identity->role, ["TUTOR"])) {
                            $TutorStandardRateHistorymodel->changed_by_uuid = Yii::$app->user->identity->tutor_uuid;
                        }

                        $TutorStandardRateHistorymodel->changed_by_role = Yii::$app->user->identity->role;
                        $TutorStandardRateHistorymodel->action = "ADD";
                        $TutorStandardRateHistorymodel->change_tutor_category = "NO";

                        $TutorStandardRateHistorymodel->online_tutorial_package_type_uuid = $model->online_tutorial_package_type_uuid;
                        $TutorStandardRateHistorymodel->tutor_category_history_uuid = $TutorCategoryHistorymodel->uuid;
                        $TutorStandardRateHistorymodel->datetime = date('Y-m-d H:i:s', time());
                        $TutorStandardRateHistorymodel->save(false);


                        $planmodel = PremiumStandardPlanRate::find()->where(['online_tutorial_package_type_uuid' => $model->online_tutorial_package_type_uuid])->asArray()->All();

                        foreach ($planmodel as $plan_row) {

                            $online_tutorial_package_uuid = $plan_row['online_tutorial_package_uuid'];
                            $premium_standard_plan_rate_uuid = $plan_row['uuid'];
                            $standard_plan_rate_option = $plan_row['default_option'];

                            $tutorial_plan = new OnlineTutorialPackageTutorwise();
                            $tutorial_plan->tutor_uuid = $model->uuid;
                            $tutorial_plan->online_tutorial_package_type_uuid = $model->online_tutorial_package_type_uuid;
                            $tutorial_plan->online_tutorial_package_uuid = $online_tutorial_package_uuid;
                            $tutorial_plan->premium_standard_plan_rate_uuid = $premium_standard_plan_rate_uuid;
                            $tutorial_plan->standard_plan_rate_option = $standard_plan_rate_option;
                            $tutorial_plan->save(false);
                        }
                    }





                    Yii::$app->authManager->assign(Yii::$app->authManager->getRole($userModel->role), $userModel->getPrimaryKey());

                    \app\models\TutorNotification::addDefaultNotification($model->uuid);

                    $content = [
                        'name' => $model->first_name . ' ' . $model->last_name,
                        'email' => $userModel->email,
                        'username' => $userModel->username,
                        'password' => $password_string,
                        'login_url' => Yii::$app->params['base_url'],
                    ];
                    $owner = General::getOwnersEmails(['OWNER', 'ADMIN']);
                    Yii::$app->mailer->compose('tutor_account_create', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                            ->setFrom(Yii::$app->params['supportEmail'])
                            ->setTo($userModel->email)
                            ->setSubject('Your Musiconn Tutor Account has been created')
                            ->send();

                    Yii::$app->mailer->compose('tutor_account_create_admin', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                            ->setFrom(Yii::$app->params['supportEmail'])
                            ->setTo($owner)
                            ->setSubject('Your Musiconn Tutor Account has been created ' . $content['name'])
                            ->send();

                    Yii::$app->getSession()->setFlash('success', 'You have successfully created Tutor account.');
                } else {
                    Yii::$app->getSession()->setFlash('danger', 'Something wrong. Tutor account does not created.');
                }
                return $this->redirect(['index']);
            }
        }
//        $model->contract_expiry = Yii::$app->getRequest()->post('Tutor')['contract_expiry'];
//        $model->child_certi_expiry = Yii::$app->getRequest()->post('Tutor')['child_certi_expiry'];

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = 'json';
            return \yii\helpers\ArrayHelper::merge(
                            \yii\widgets\ActiveForm::validate($model), \yii\widgets\ActiveForm::validate($userModel)
            );
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'userModel' => $userModel,
                        'instrumentModel' => $instrumentModel,
                        'tutorInstrumentModel' => $tutorInstrumentModel,
                        'packegetypeModel' => $packegetypeModel, /** Added by pooja : Tutor wise plan rate * */
                        'new' => true,
                            /// 'working_plan'=>$settingModelDecode,
            ]);
        }
    }

    /**
     * Updates an existing Tutor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {//
        try {
            $model = $this->findModel($id);
            $model->scenario = 'tutor_update';
            $model->state_text = $model->state;
            $old_category = $model->online_tutorial_package_type_uuid;
            $instrumentModel = \app\models\Instrument::find()->where(['status' => 'ACTIVE'])->all();

            $tutorInstrumentModel = \app\models\TutorInstrument::find()->where(['tutor_uuid' => $model->uuid])->asArray()->all();
            $old_t_ins = array_column($tutorInstrumentModel, 'instrument_uuid');
            $model->instrumentList = $old_t_ins;

            /**
             * start
             * Devloped By : Pooja Beladiya
             * Task : Tutor wise plan rate
             */
            $packegetypeModel = \app\models\OnlineTutorialPackageType::find()->all();

            //$model->tutorTypeList = $tutorial_type;

            /**
             * End
             * Task : Tutor wise plan rate
             */
            $userModel = \app\models\User::find()->where(['tutor_uuid' => $model->uuid])->one();
            $userModel->scenario = 'tutor_update';

            $model->attributes = Yii::$app->getRequest()->post('Tutor');
            $userModel->attributes = Yii::$app->getRequest()->post('User');



            $userModel->email = $userModel->username;
            $userModel->updated_at = time();
            if ($model->load(Yii::$app->request->post()) && $model->validate() && $userModel->load(Yii::$app->request->post()) && $userModel->validate()) {


                $UploadedFile = UploadedFile::getInstance($model, 'profile_image');
                if (!empty($UploadedFile)) {
                    $model->profile_image = file_get_contents($UploadedFile->tempName);
                } else {
                    $model->profile_image = file_get_contents(Yii::$app->params['theme_assets']['path'] . 'images/profile.png');
                }

                $userModel->first_name = $model->first_name;
                $userModel->last_name = $model->last_name;
                $userModel->phone = $model->phone;
                $userModel->save(false);

                $model->state = ($model->country == 'other') ? $model->state_text : $model->state;
                $model->contract_expiry = date('Y-m-d 23:59:59', strtotime($model->contract_expiry));
                $model->child_certi_expiry = date('Y-m-d 23:59:59', strtotime($model->child_certi_expiry));
                $model->email = $userModel->email;
                $model->update_at = date('Y-m-d H:i:s', time());
                $package_type_model = OnlineTutorialPackageType::find()->where(['uuid' => $model->online_tutorial_package_type_uuid])->asArray()->one();
                $package_type = $package_type_model['name'];
                if ($old_category != $model->online_tutorial_package_type_uuid) {
                    if ($package_type == "STANDARD") {
                        $model->standard_plan_rate_option = "1";
                    } else {
                        $model->standard_plan_rate_option = "0";
                    }
                }

                $model->save(false);


                $delete_ids = array_diff($old_t_ins, $model->instrumentList);
                if (!empty($delete_ids)) {

                    $deleted_array = \app\models\TutorInstrument::find()->where(['in', 'instrument_uuid', $delete_ids])->andWhere(['tutor_uuid' => $model->uuid])->all();
                    if (!empty($deleted_array)) {
                        foreach ($deleted_array as $delete_val) {
                            $delete_val->delete();
                        }
                    }
                }

                
                $add_ids = array_diff($model->instrumentList, $old_t_ins);

                foreach ($add_ids as $key => $value) {
                    $tutorInstrumentModelj = new \app\models\TutorInstrument();
                    $tutorInstrumentModelj->tutor_uuid = $model->uuid;
                    $tutorInstrumentModelj->instrument_uuid = $value;
                    $tutorInstrumentModelj->save();
                }
                
                
               // if ($old_category != $model->online_tutorial_package_type_uuid || count($add_ids)>0) {

                    OnlineTutorialPackageTutorwise::deleteAll(['tutor_uuid' => $model->uuid]);

                    $TutorCategoryHistorymodel = new TutorCategoryHistory();
                    $TutorCategoryHistorymodel->tutor_uuid = $model->uuid;
                    $TutorCategoryHistorymodel->online_tutorial_package_type_uuid = $model->online_tutorial_package_type_uuid;
                    if (in_array(Yii::$app->user->identity->role, ["OWNER", "ADMIN", "SUBADMIN"])) {
                        $TutorCategoryHistorymodel->changed_by_uuid = Yii::$app->user->identity->admin_uuid;
                        $TutorCategoryHistorymodel->changed_by_role = Yii::$app->user->identity->role;
                    }

                    $TutorCategoryHistorymodel->old_online_tutorial_package_type_uuid = $old_category;
                    $TutorCategoryHistorymodel->action = "EDIT";
                    $TutorCategoryHistorymodel->datetime = date('Y-m-d H:i:s', time());
                    $TutorCategoryHistorymodel->save(false);

                    $OnlineTutorialPackageTutorwisemodel = OnlineTutorialPackageTutorwise::find()->where(['tutor_uuid' => $model->uuid])->one();


                    if ($package_type == "STANDARD") {
                        foreach ($model->instrumentList as $key => $value) {

                            $TutorStandardRateHistorymodel = new TutorStandardRateHistory();
                            $TutorStandardRateHistorymodel->tutor_uuid = $model->uuid;
                            $TutorStandardRateHistorymodel->instrument_uuid = $value;

                            $package_type_model = OnlineTutorialPackageType::find()->where(['uuid' => $model->online_tutorial_package_type_uuid])->asArray()->one();
                            $package_type = $package_type_model['name'];
                            if ($package_type == "STANDARD") {
                                $TutorStandardRateHistorymodel->option = "1";
                            } else {
                                $TutorStandardRateHistorymodel->option = "0";
                            }
                            if (in_array(Yii::$app->user->identity->role, ["OWNER", "ADMIN", "SUBADMIN"])) {
                                $TutorStandardRateHistorymodel->changed_by_uuid = Yii::$app->user->identity->admin_uuid;
                            } else if (in_array(Yii::$app->user->identity->role, ["TUTOR"])) {
                                $TutorStandardRateHistorymodel->changed_by_uuid = Yii::$app->user->identity->tutor_uuid;
                            }

                            $TutorStandardRateHistorymodel->changed_by_role = Yii::$app->user->identity->role;
                            $TutorStandardRateHistorymodel->action = "EDIT";
                            $TutorStandardRateHistorymodel->change_tutor_category = "YES";
                            $TutorStandardRateHistorymodel->old_online_tutorial_package_type_uuid = $old_category;
                            $TutorStandardRateHistorymodel->online_tutorial_package_type_uuid = $model->online_tutorial_package_type_uuid;
                            $TutorStandardRateHistorymodel->tutor_category_history_uuid = $TutorCategoryHistorymodel->uuid;
                            $TutorStandardRateHistorymodel->datetime = date('Y-m-d H:i:s', time());
                            $TutorStandardRateHistorymodel->save(false);



                            $planmodel = PremiumStandardPlanRate::find()->where(['online_tutorial_package_type_uuid' => $model->online_tutorial_package_type_uuid])->asArray()->All();

                            foreach ($planmodel as $plan_row) {

                                $online_tutorial_package_uuid = $plan_row['online_tutorial_package_uuid'];
                                $premium_standard_plan_rate_uuid = $plan_row['uuid'];
                                $standard_plan_rate_option = $plan_row['default_option'];

                                $tutorial_plan = new OnlineTutorialPackageTutorwise();
                                $tutorial_plan->tutor_uuid = $model->uuid;
                                $tutorial_plan->instrument_uuid = $value;
                                $tutorial_plan->online_tutorial_package_type_uuid = $model->online_tutorial_package_type_uuid;
                                $tutorial_plan->online_tutorial_package_uuid = $online_tutorial_package_uuid;
                                $tutorial_plan->premium_standard_plan_rate_uuid = $premium_standard_plan_rate_uuid;
                                $tutorial_plan->standard_plan_rate_option = $standard_plan_rate_option;
                                $tutorial_plan->save(false);
                            }
                        }
                    } else {

                        $TutorStandardRateHistorymodel = new TutorStandardRateHistory();
                        $TutorStandardRateHistorymodel->tutor_uuid = $model->uuid;

                        $package_type_model = OnlineTutorialPackageType::find()->where(['uuid' => $model->online_tutorial_package_type_uuid])->asArray()->one();
                        $package_type = $package_type_model['name'];
                        if ($package_type == "STANDARD") {
                            $TutorStandardRateHistorymodel->option = "1";
                        } else {
                            $TutorStandardRateHistorymodel->option = "0";
                        }
                        if (in_array(Yii::$app->user->identity->role, ["OWNER", "ADMIN", "SUBADMIN"])) {
                            $TutorStandardRateHistorymodel->changed_by_uuid = Yii::$app->user->identity->admin_uuid;
                        } else if (in_array(Yii::$app->user->identity->role, ["TUTOR"])) {
                            $TutorStandardRateHistorymodel->changed_by_uuid = Yii::$app->user->identity->tutor_uuid;
                        }

                        $TutorStandardRateHistorymodel->changed_by_role = Yii::$app->user->identity->role;
                        $TutorStandardRateHistorymodel->action = "EDIT";
                        $TutorStandardRateHistorymodel->change_tutor_category = "YES";
                        $TutorStandardRateHistorymodel->old_online_tutorial_package_type_uuid = $old_category;
                        $TutorStandardRateHistorymodel->online_tutorial_package_type_uuid = $model->online_tutorial_package_type_uuid;
                        $TutorStandardRateHistorymodel->tutor_category_history_uuid = $TutorCategoryHistorymodel->uuid;
                        $TutorStandardRateHistorymodel->datetime = date('Y-m-d H:i:s', time());
                        $TutorStandardRateHistorymodel->save(false);



                        $planmodel = PremiumStandardPlanRate::find()->where(['online_tutorial_package_type_uuid' => $model->online_tutorial_package_type_uuid])->asArray()->All();

                        foreach ($planmodel as $plan_row) {

                            $online_tutorial_package_uuid = $plan_row['online_tutorial_package_uuid'];
                            $premium_standard_plan_rate_uuid = $plan_row['uuid'];
                            $standard_plan_rate_option = $plan_row['default_option'];

                            $tutorial_plan = new OnlineTutorialPackageTutorwise();
                            $tutorial_plan->tutor_uuid = $model->uuid;
                            $tutorial_plan->online_tutorial_package_type_uuid = $model->online_tutorial_package_type_uuid;
                            $tutorial_plan->online_tutorial_package_uuid = $online_tutorial_package_uuid;
                            $tutorial_plan->premium_standard_plan_rate_uuid = $premium_standard_plan_rate_uuid;
                            $tutorial_plan->standard_plan_rate_option = $standard_plan_rate_option;
                            $tutorial_plan->save(false);
                        }
                    }
                //}




                Yii::$app->getSession()->setFlash('success', 'You have successfully updated tutor account.');
                return $this->redirect(['index']);
            }

            // }
            $model->contract_expiry = date('Y-m-d', strtotime($model->contract_expiry));
            $model->child_certi_expiry = date('Y-m-d', strtotime($model->child_certi_expiry));

            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = 'json';
                return \yii\helpers\ArrayHelper::merge(
                                \yii\widgets\ActiveForm::validate($model), \yii\widgets\ActiveForm::validate($userModel)
                );
            } else {
                return $this->render('update', [
                            'model' => $model,
                            'userModel' => $userModel,
                            'instrumentModel' => $instrumentModel,
                            'tutorInstrumentModel' => $tutorInstrumentModel,
                            'packegetypeModel' => $packegetypeModel, /** Added by pooja : Tutor wise plan rate * */
                            'new' => false,
                ]);
            }
        } catch (\yii\base\Exception $exception) {
            throw new \yii\web\HttpException(505, $exception->getMessage());
        }
    }

    /**
     * Deletes an existing Tutor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete() {
        exit;
        /* $this->findModel($id)->delete();

          return $this->redirect(['index']); */
        $id = Yii::$app->getRequest()->post('id');

        if ((($model = Tutor::findOne($id)) !== null) && (($modelUser = User::find()->where(['tutor_uuid' => $id])->one()) !== null)) {

            $model->update_at = date('Y-m-d H:i:s', time());
            $model->status = 'DELETED';
            $model->save(false);

            //Remove current role:
            $manager = Yii::$app->authManager;
            $item = $manager->getRole($modelUser->role);
            $item = $item ?: $manager->getPermission($modelUser->role);
            $manager->revoke($item, $modelUser->id);

            $modelUser->delete();

            $return = ['code' => 200, 'message' => 'Tutor account deleted successfully.'];
        } else {

            $return = ['code' => 404, 'message' => 'The requested page does not exist.'];
        }

        Yii::$app->response->format = 'json';
        return $return;
    }

    protected function findModel($id) {
        if (($model = Tutor::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionGeneratePassword() {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            $password = General::GeneratePassword();
            echo $password;
            Yii::$app->end();
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    public function actionAccountDisabled($id) {

        if (($tutor = Tutor::findOne($id)) !== null) {

            if ($tutor->status == "ENABLED") {

                $user = \app\models\User::find()->where(['tutor_uuid' => $tutor->uuid])->one();
                if (!empty($user)) {

                    $user->status = 0;
                    $user->save(false);

                    $tutor->status = 'DISABLED';
                    $tutor->save(false);

                    $return = ['code' => 200, 'message' => 'Totor account disabled successfully.'];
                } else {
                    $return = ['code' => 421, 'message' => 'User record does not exist'];
                }
            } else {
                $return = ['code' => 421, 'message' => 'Totor account not enabled.'];
            }
        } else {
            $return = ['code' => 404, 'message' => 'Record does not exist.'];
        }

        Yii::$app->response->format = 'json';
        return $return;
    }

    public function actionAccountEnabled($id) {

        if (($tutor = Tutor::findOne($id)) !== null) {

            if ($tutor->status == "DISABLED") {

                $user = \app\models\User::find()->where(['tutor_uuid' => $tutor->uuid])->one();
                if (!empty($user)) {

                    $user->status = 10;
                    $user->save(false);

                    $tutor->status = 'ENABLED';
                    $tutor->save(false);

                    $return = ['code' => 200, 'message' => 'Admin account enabled successfully.'];
                } else {
                    $return = ['code' => 421, 'message' => 'User record does not exist'];
                }
            } else {
                $return = ['code' => 421, 'message' => 'Admin account not disabled.'];
            }
        } else {
            $return = ['code' => 404, 'message' => 'Record does not exist.'];
        }

        Yii::$app->response->format = 'json';
        return $return;
    }

    // public function actionProfile() {
    //     $model = $this->findModel(Yii::$app->user->identity->tutor_uuid);
    //     $model->state_text = $model->state;
    //     $modelCP = new ChangePassword();
    //     $userModel = \app\models\User::findOne(Yii::$app->user->identity->id);
    //     //$userModel->scenario = 'tutor_profile';
    //     //$tutorInstrumentModel = \app\models\TutorInstrument::find()->where(['tutor_uuid' => $model->uuid])->all();
    //     $tutorInstrumentModel = \app\models\TutorInstrument::find('instrument')
    //             ->with('instrument')->where(['tutor_uuid' => $model->uuid])
    //             ->all();
    //     $model->attributes = Yii::$app->getRequest()->post('Tutor');
    //     $userModel->attributes = Yii::$app->getRequest()->post('User');
    //     $model->email = $userModel->username = $userModel->email;
    //     $userModel->updated_at = time();
    //     $model->update_at = date("Y-m-d H:i:s", time());
    //     if ($model->load(Yii::$app->request->post()) && $model->validate() && $userModel->load(Yii::$app->request->post()) && $userModel->validate()) {
    //         $userModel->first_name = $model->first_name;
    //         $userModel->last_name = $model->last_name;
    //         $userModel->phone = $model->phone;
    //         $model->state = ($model->country == 'other') ? $model->state_text : $model->state;
    //         $model->save(false);
    //         $userModel->save(false);
    //         Yii::$app->getSession()->setFlash('success', 'You have successfully updated your profile.');
    //         return $this->redirect(['profile']);
    //     }
    //     return $this->render('profile', [
    //                 'model' => $model,
    //                 'userModel' => $userModel,
    //                 'modelCP' => $modelCP,
    //                 'tutorInstrumentModel' => $tutorInstrumentModel,
    //     ]);
    // }

    public function actionProfile() {
        $model = $this->findModel(Yii::$app->user->identity->tutor_uuid);
        $model->state_text = $model->state;
        $instrumentModel = \app\models\Instrument::find()->where(['status' => 'ACTIVE'])->all();
        $tutorInstrumentModel = \app\models\TutorInstrument::find('instrument')
                ->with('instrument')->where(['tutor_uuid' => $model->uuid])
                ->all();

        $tutorInstrument = \app\models\TutorInstrument::find()->where(['tutor_uuid' => $model->uuid])->all();
        $old_t_ins = array_column($tutorInstrument, 'instrument_uuid');
        // print_r($old_t_ins);exit;
        $model->instrumentList = $old_t_ins;
        $modelCP = new ChangePassword();
        $userModel = \app\models\User::findOne(Yii::$app->user->identity->id);
        //$userModel->scenario = 'tutor_profile';
        // $tutorInstrumentModel = \app\models\TutorInstrument::find('instrument')
        //         ->with('instrument')->where(['tutor_uuid' => $model->uuid])
        //         ->all();

        $model->attributes = Yii::$app->getRequest()->post('Tutor');
        $userModel->attributes = Yii::$app->getRequest()->post('User');


        $model->email = $userModel->username = $userModel->email;
        $userModel->updated_at = time();
        $model->update_at = date("Y-m-d H:i:s", time());

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $userModel->load(Yii::$app->request->post()) && $userModel->validate()) {
            $userModel->first_name = $model->first_name;
            $userModel->last_name = $model->last_name;
            $userModel->phone = $model->phone;
            $userModel->save(false);
            $model->state = ($model->country == 'other') ? $model->state_text : $model->state;
            $model->contract_expiry = date('Y-m-d 23:59:59', strtotime($model->contract_expiry));
            $model->child_certi_expiry = date('Y-m-d 23:59:59', strtotime($model->child_certi_expiry));
            $model->email = $userModel->email;
            $model->update_at = date('Y-m-d H:i:s', time());
            $model->save(false);
            // exit;

            $delete_ids = array_diff($old_t_ins, $model->instrumentList);
            if (!empty($delete_ids)) {

                $deleted_array = \app\models\TutorInstrument::find()->where(['in', 'instrument_uuid', $delete_ids])->andWhere(['tutor_uuid' => $model->uuid])->all();
                if (!empty($deleted_array)) {
                    foreach ($deleted_array as $delete_val) {
                        $delete_val->delete();
                    }
                }
            }
            $add_ids = array_diff($model->instrumentList, $old_t_ins);

            foreach ($add_ids as $key => $value) {
                $tutorInstrumentModelj = new \app\models\TutorInstrument();
                $tutorInstrumentModelj->tutor_uuid = $model->uuid;
                $tutorInstrumentModelj->instrument_uuid = $value;
                $tutorInstrumentModelj->save();
            }

            Yii::$app->getSession()->setFlash('success', 'You have successfully updated your profile.');

            return $this->redirect(['profile']);
        }

        $model->contract_expiry = date('Y-m-d', strtotime($model->contract_expiry));
        $model->child_certi_expiry = date('Y-m-d', strtotime($model->child_certi_expiry));

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = 'json';
            return \yii\helpers\ArrayHelper::merge(
                            \yii\widgets\ActiveForm::validate($model), \yii\widgets\ActiveForm::validate($userModel)
            );
        } else {
            return $this->render('profile', [
                        'model' => $model,
                        'userModel' => $userModel,
                        'modelCP' => $modelCP,
                        'instrumentModel' => $instrumentModel,
                        'tutorInstrumentModel' => $tutorInstrumentModel,
            ]);
        }
    }

    public function actionProfileImage() {
        $model = $this->findModel(Yii::$app->user->identity->tutor_uuid);
        $model->state_text = $model->state;
        $model->scenario = 'profile_image_update';
        if (Yii::$app->request->isPost) {
            $model->attributes = Yii::$app->getRequest()->post('Tutor');
            $UploadedFile = UploadedFile::getInstance($model, 'profile_image');
            list($width, $height, $type, $attr) = getimagesize($UploadedFile->tempName);
            $info = getimagesize($UploadedFile->tempName);
            if ($info === FALSE) {

                $return = ['code' => 421, 'message' => 'Please Upload appropriate image file.'];
            }
            // elseif ($width > 500 || $height > 500) {
            //     $return = ['code' => 421, 'message' => 'The image is too large. The width nad height cannot be larger than 550 pixels.'];
            // } 
            else {
                $model->profile_image = file_get_contents($UploadedFile->tempName);
                if ($model->validate()) {
                    $model->save(false);
                    $dataUri = "data:image/jpg;base64," . base64_encode($model->profile_image);
                    $final = "<img src='$dataUri' class='img-fluid user-profile-status-active'/>";
                    $sidebar = "<img src='$dataUri' class='rounded-circle img-inline'/>";
                    $navbar_src = $dataUri;
                    $return = ['code' => 200, 'image_profile' => $final, 'sidebar_profile' => $sidebar, 'navbar_src' => $navbar_src, 'message' => 'Profile image successfully updated.'];
                } else {
                    print_r($model->errors);
                    exit;
                    $return = ['code' => 421, 'message' => 'Somthing wrong.'];
                }
            }

            Yii::$app->response->format = 'json';
            return $return;
            exit;
        }
    }

    public function actionChangePassword() {
        if (!Yii::$app->request->isAjax) {
            Yii::$app->response->format = 'json';
            return ['code' => 505, 'message' => 'You are not authorised to access this page'];
        }

        $modelCP = new ChangePassword();
        if ($modelCP->load(Yii::$app->getRequest()->post()) && $modelCP->validate()) {

            $modelCP->change();
            Yii::$app->response->format = 'json';
            return ['code' => 200, 'message' => 'Password change successfully.'];
        }

        return $this->renderAjax('change-password', [
                    'modelCP' => $modelCP,
        ]);
    }

    public function actionWorkingPlan() {

        if (Yii::$app->user->identity->role == 'TUTOR') {
            $id = Yii::$app->user->identity->tutor_uuid;
            $view_file = 'working_plan_tutor';
            $redirect = ['/tutor/working-plan'];
        } else {
            $id = Yii::$app->request->get('id');
            $view_file = 'working_plan';
            $redirect = ['/tutor/index'];
        }

        if (empty($id)) {
            throw new \yii\web\HttpException(400, 'Missing required parameters: id');
        }

        $plan_error = $break_error = $working_plan = $break_post = [];
        $model = $this->findModel($id);

        if (Yii::$app->request->isPost) {
            $break = array();

            $user_access_arr = array('sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday');

            foreach ($_POST['working_plan'] as $key => $value) {
                if (is_array($_POST['working_plan'][$key]) && isset($_POST['working_plan'][$key]['chk'])) {
                    $working_plan[$key]['start'] = date('H:i', strtotime($_POST['working_plan'][$key]['start']));
                    $working_plan[$key]['end'] = date('H:i', strtotime($_POST['working_plan'][$key]['end']));

                    if ($working_plan[$key]['start'] > $working_plan[$key]['end']) {
                        $plan_error[$key] = "Please enter valid time for " . $key;
                    }

                    //unset($_POST['working_plan'][$key]['chk']);
                } else {
                    $_POST['working_plan'][$key] = null;
                }
            }

            foreach ($user_access_arr as $v) {
                //If the value in the template not exists as a key in the actual array.. (condition)
                if (!array_key_exists($v, $working_plan)) {
                    $working_plan[$v] = null;
                }
            }

            $sorted = array();
            foreach ($user_access_arr as $k) {
                $sorted[$k] = $working_plan[$k];
            }

            if (isset($_POST['breakAdd1']) && !empty($_POST['breakAdd1'])) {
                foreach ($_POST['breakAdd1'] as $key => $value) {
                    foreach ($value['start'] as $key1 => $value1) {
                        $break[$key]['breaks'][$key1]['start'] = date('H:i', strtotime($value1));
                        $break_post[$key][$key1]['start'] = date('H:i', strtotime($value1));
                    }
                    foreach ($value['end'] as $endkey => $endvalue) {
                        $break[$key]['breaks'][$endkey]['end'] = date('H:i', strtotime($endvalue));
                        $break_post[$key][$endkey]['end'] = date('H:i', strtotime($endvalue));
                    }
                }

                /*                 * **find break error**** */
                if (!empty($break)) {
                    foreach ($break as $kb => $vb) {
                        if (!empty($vb)) {
                            foreach ($vb as $kb1 => $vb1) {
                                if (!empty($vb1)) {
                                    foreach ($vb1 as $kb2 => $vb2) {
                                        if ($vb2['start'] > $vb2['end']) {
                                            $break_error[$kb][$kb2] = "Please enter valid time for " . $kb;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                $final = array_intersect_key($break, $sorted);
                foreach ($final as $key => $value) {
                    if (!empty($sorted[$key])) {

                        $a = array_map("unserialize", array_unique(array_map("serialize", $value['breaks'])));
                        $sorted[$key]['breaks'] = $a;
                    }
                }
            }

            if (!empty($plan_error) || !empty($break_error)) {

                $model->working_plan = json_encode($sorted);

                return $this->render($view_file, [
                            'model' => $model,
                            'plan_error' => $plan_error,
                            'break_error' => $break_error,
                            'break_post' => $break_post,
                ]);
            } else {

                $model->working_plan = json_encode($sorted);
                $model->save(false);

                Yii::$app->getSession()->setFlash('success', 'You have successfully updated tutor working plan.');
                return $this->redirect($redirect);
            }
        }
        return $this->render($view_file, [
                    'model' => $model,
                    'plan_error' => $plan_error,
                    'break_error' => $break_error,
                    'break_post' => $break_post,
        ]);
    }

    public function actionResetPassword($id) {   //admin change tutor password
        $model = $this->findModel($id);

        $modelCP = new ChangePassword();

        if ($modelCP->load(Yii::$app->getRequest()->post())) {

            $modelCP->tutorChangePasswordByAdmin($model);
            Yii::$app->getSession()->setFlash('success', 'You have successfully reset tutor password.');
            return $this->redirect(['index']);
        }

        return $this->render('reset_password', [
                    'model' => $model,
                    'modelCP' => $modelCP,
        ]);
    }

    public function actionAvailability() {

        if (Yii::$app->request->isAjax) {
            $tutor_uuid = Yii::$app->getRequest()->get('tutor_uuid');
            $modelTutor = $this->findModel($tutor_uuid);
            if (!empty($tutor_uuid) && !empty($modelTutor)) {

                return $this->renderAjax('_availability_calender', [
                            'tutor_uuid' => $tutor_uuid,
                            'modelTutor' => $modelTutor,
                ]);
            } else {
                Yii::$app->response->format = 'json';
                return ['code' => 404, 'message' => 'Tutor not available'];
            }
        } else {
            return $this->render('availability', [
            ]);
        }
    }

    public function actionGetAvailabilityCalenderEvents() {

        if (!Yii::$app->request->isAjax) {
            throw new \yii\web\HttpException(403, 'The action you have requested is not allowed.');
        }
        $return = [];

        $tutor_uuid = Yii::$app->getRequest()->get('tutor_uuid');
        $modelTutor = $this->findModel($tutor_uuid);

        if (!empty($tutor_uuid) && !empty($modelTutor)) {

//            $start_date = date('Y-m-d', strtotime(Yii::$app->request->get('start')));
//            $end_date = date('Y-m-d', strtotime(Yii::$app->request->get('end') . ' +1 day'));
            $start_date = date('Y-m-d', Yii::$app->request->get('start'));
            $end_date = date('Y-m-d', Yii::$app->request->get('end'));

            $event_1 = Tutor::prepareTutorUnavailabilityEvents($tutor_uuid, $start_date, $end_date);
            $event_2 = Tutor::prepareTutorWeekDayNoneWorkingEvents($tutor_uuid);
            $all_breaks = array_merge($event_1, $event_2);

            $event_3 = Tutor::prepareTutorAppoinmentEvents($tutor_uuid, $start_date, $end_date);
            $return = array_merge($all_breaks, $event_3);
        }

        Yii::$app->response->format = 'json';
        return $return;
    }

    public function actionAvailable() {

        $tutor_uuid = Yii::$app->user->identity->tutor_uuid;
        $modelTutor = $this->findModel($tutor_uuid);
        return $this->render('available_calender', [
                    'tutor_uuid' => $tutor_uuid,
                    'modelTutor' => $modelTutor,
        ]);
    }

    public function actionBookingAvailability() {

        if (Yii::$app->request->isAjax) {
            $tutor_uuid = Yii::$app->getRequest()->get('tutor_uuid');
            $modelBooking = $this->findModel($tutor_uuid);
            if (!empty($tutor_uuid) && !empty($modelBooking)) {

                return $this->renderAjax('_booking_availability_calender', [
                            'tutor_uuid' => $tutor_uuid,
                            'modelBooking' => $modelBooking,
                ]);
            } else {
                Yii::$app->response->format = 'json';
                return ['code' => 404, 'message' => 'Tutor not available'];
            }
        } else {
            return $this->render('booking_availability', [
            ]);
        }
    }

    public function actionGetBookingAvailabilityCalenderEvents() {

        if (!Yii::$app->request->isAjax) {
            throw new \yii\web\HttpException(403, 'The action you have requested is not allowed.');
        }
        $return = [];

        $tutor_uuid = Yii::$app->getRequest()->get('tutor_uuid');
        $modelTutor = $this->findModel($tutor_uuid);

        if (!empty($tutor_uuid) && !empty($modelTutor)) {

            $start_date = date('Y-m-d', Yii::$app->request->get('start'));
            $end_date = date('Y-m-d', Yii::$app->request->get('end'));

            $event_1 = Tutor::prepareTutorUnavailabilityEvents($tutor_uuid, $start_date, $end_date);
            $event_2 = Tutor::prepareTutorWeekDayNoneWorkingEvents_booking($tutor_uuid, $start_date, $end_date);
            $all_1 = array_merge($event_1, $event_2);

            $event_3 = Tutor::prepareTutorScheduleAppoinmentEvents($tutor_uuid, $start_date, $end_date);
            $event_4 = Tutor::prepareStudentScheduleAppoinmentEvents($tutor_uuid, $start_date, $end_date);
            $all_2 = array_merge($event_3, $event_4);

            $all_3 = array_merge($all_1, $all_2);
            $event_5 = Tutor::prepareTutorRescheduleAppoinmentEvents($tutor_uuid, $start_date, $end_date);
            $return = array_merge($all_3, $event_5);
        }

        Yii::$app->response->format = 'json';
        return $return;
    }

    public function actionTutorDeleteUnavailability() {
        $id = Yii::$app->getRequest()->post('id');
        if ((($model = TutorUnavailability::find()->where(['uuid' => $id])->one()) !== null)) {

            $model->delete();

            $return = ['code' => 200, 'message' => 'Tutor unavailability deleted successfully.'];
        } else {

            $return = ['code' => 404, 'message' => 'The requested page does not exist.'];
        }

        Yii::$app->response->format = 'json';
        return $return;
    }

    public function actionTutorGetUnavailability($id) {
        try {
            $model = TutorUnavailability::find()->where(['uuid' => $id])->one();
            $model->scenario = 'unavailability';

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $start_datetime = date('Y-m-d H:i:s', strtotime($model->start_datetime));
                $end_datetime = date('Y-m-d H:i:s', strtotime($model->end_datetime));
                $model->uuid = $id;
                $model->tutor_uuid = $model->tutor_uuid;
                $model->start_datetime = General::convertUserToSystemTimezone($start_datetime);
                $model->end_datetime = General::convertUserToSystemTimezone($end_datetime);
                $model->reason = $model->reason;
                $model->save(false);
                $return = ['code' => 200, 'message' => 'Your unavailability update successfully.'];
                Yii::$app->response->format = 'json';
                return $return;
            } else {
                $model->start_datetime = General::convertSystemToUserTimezone($model->start_datetime);
                ;
                $model->end_datetime = General::convertSystemToUserTimezone($model->end_datetime);
            }

            return $this->renderAjax('_tutor_unavailability', [
                        'model' => $model,
            ]);
        } catch (\yii\base\Exception $exception) {
            throw new \yii\web\HttpException(505, $exception->getMessage());
        }
    }

    public function actionApps() {
        return $this->render('apps_view');
    }

    public function actionAssignInstrumentPlan($tutor_uuid, $instrument_uuid) {
        $model = Tutor::find()->where(['uuid' => $tutor_uuid])->asArray()->one();

        $package_type_model = OnlineTutorialPackageType::find()->where(['uuid' => $model['online_tutorial_package_type_uuid']])->asArray()->one();
        $package_type = $package_type_model['name'];

        $tutor_plans = OnlineTutorialPackageTutorwise::find()->where(['tutor_uuid' => $tutor_uuid, 'instrument_uuid' => $instrument_uuid])->asArray()->one();
        $query = new \yii\db\Query;
        $plans = $query->select(['o.*', 'p.uuid', 'p.name', 'p.price', 'p.gst', 'p.total_price', 'p.status', 'p.default_option', 'p.online_tutorial_package_type_uuid']
                        )
                        ->from('online_tutorial_package as o')
                        ->join('RIGHT JOIN', 'premium_standard_plan_rate as p', 'o.uuid = p.online_tutorial_package_uuid')
                        ->where(['p.online_tutorial_package_type_uuid' => $model['online_tutorial_package_type_uuid'],'p.status' => 'ENABLED'])
                        ->orderBy('p.payment_term_uuid')->all();



        $premium_standard_plan_rate_uuid = array_column($plans, 'uuid');
        $planuuids = implode(",", $premium_standard_plan_rate_uuid);


        $option_array = array();


        $i = 0;

        foreach ($plans as $option_row) {

            $optionmodel = StandardPlanOptionRate::find()->where(['premium_standard_plan_rate_uuid' => $option_row['uuid'], 'option' => $tutor_plans['standard_plan_rate_option']])->asArray()->one();

            //echo "<pre>";
            //print_r($option_row);exit;
            $option_array[$i]['uuid'] = $option_row['uuid'];
            $option_array[$i]['name'] = $option_row['name'];

            $option_array[$i]['price'] = $optionmodel['price'];
            $option_array[$i]['gst'] = $optionmodel['gst'];
            $option_array[$i]['total_price'] = $optionmodel['total_price'];
            $option_array[$i]['option'] = $optionmodel['option'];
            $i++;
        }

        if (Yii::$app->request->post()) {



            $tutor_uuid = Yii::$app->request->post('tutor_uuid');
            $instrument_uuid = Yii::$app->request->post('instrument_uuid');
            //$plan_array = Yii::$app->request->post('plan');
            $standard_plan_rate_option = Yii::$app->request->post('standard_plan_rate_option');
            $online_tutorial_package_type_uuid = Yii::$app->request->post('online_tutorial_package_type_uuid');
            $package_type_model = OnlineTutorialPackageType::find()->where(['uuid' => $model['online_tutorial_package_type_uuid']])->asArray()->one();
            $package_type = $package_type_model['name'];

            if ($package_type == "STANDARD") {

                $old_detail = OnlineTutorialPackageTutorwise::find()->where(['tutor_uuid' => $tutor_uuid, 'instrument_uuid' => $instrument_uuid])->one();
                        $query = new \yii\db\Query;
                 $stdplans = $query->select(['o.*', 'p.uuid', 'p.name', 'p.price', 'p.gst', 'p.total_price', 'p.status', 'p.default_option', 'p.online_tutorial_package_type_uuid']
                        )
                        ->from('online_tutorial_package as o')
                        ->join('RIGHT JOIN', 'premium_standard_plan_rate as p', 'o.uuid = p.online_tutorial_package_uuid')
                        ->where(['p.online_tutorial_package_type_uuid' => $model['online_tutorial_package_type_uuid']])
                        ->orderBy('p.payment_term_uuid')->all();
                
                foreach ($stdplans as $row) {
                    $update_tutor_plans = OnlineTutorialPackageTutorwise::find()->where(['tutor_uuid' => $tutor_uuid, 'premium_standard_plan_rate_uuid' => $row['uuid'], 'instrument_uuid' => $instrument_uuid])->one();

                    $update_tutor_plans->standard_plan_rate_option = $standard_plan_rate_option;
                    $update_tutor_plans->save(false);
                }


                $TutorStandardRateHistorymodel = new TutorStandardRateHistory();
                $TutorStandardRateHistorymodel->tutor_uuid = $tutor_uuid;
                $TutorStandardRateHistorymodel->instrument_uuid = $instrument_uuid;
                $TutorStandardRateHistorymodel->old_option = $old_detail->standard_plan_rate_option;
                $TutorStandardRateHistorymodel->option = Yii::$app->request->post('standard_plan_rate_option');
                if (in_array(Yii::$app->user->identity->role, ["OWNER", "ADMIN", "SUBADMIN"])) {
                    $TutorStandardRateHistorymodel->changed_by_uuid = Yii::$app->user->identity->admin_uuid;
                } else if (in_array(Yii::$app->user->identity->role, ["TUTOR"])) {
                    $TutorStandardRateHistorymodel->changed_by_uuid = Yii::$app->user->identity->tutor_uuid;
                }

                $TutorStandardRateHistorymodel->changed_by_role = Yii::$app->user->identity->role;
                $TutorStandardRateHistorymodel->action = "EDIT";
                $TutorStandardRateHistorymodel->change_tutor_category = "NO";

                $TutorStandardRateHistorymodel->online_tutorial_package_type_uuid = $model['online_tutorial_package_type_uuid'];
                $TutorStandardRateHistorymodel->datetime = date('Y-m-d H:i:s', time());
                $TutorStandardRateHistorymodel->save(false);



                /* if(!empty($plan_array)){

                  foreach ($plan_array as $key => $value){

                  $update_tutor_plans = OnlineTutorialPackageTutorwise::find()->where(['tutor_uuid' => $id , 'premium_standard_plan_rate_uuid'=>$key])->one();
                  $update_tutor_plans->standard_plan_rate_option = $value['price'];
                  $update_tutor_plans->save(false);



                  } */
                $tutor_model = Tutor::find()->where(['uuid' => $tutor_uuid])->one();
                $tutor_model->standard_plan_rate_option = $standard_plan_rate_option;
                $tutor_model->save(false);


                Yii::$app->getSession()->setFlash('success', 'Rate Changed successfully.');
                return $this->redirect(['assign-plan', 'id' => $tutor_uuid]);
            }
        }
        //echo "<pre>";
        //print_r($option_array['8f213ac0-b0c1-4c63-9f80-6359bfe7945d']);exit;
        //echo "<pre>";
        //print_r(ArrayHelper::map($option_array['8f213ac0-b0c1-4c63-9f80-6359bfe7945d'], 'option', 'price'));


        return $this->render('standard_assign_plan', ['plans' => $plans, 'model' => $model, 'tutor_plans' => $tutor_plans, 'option_array' => $option_array, 'instrument_uuid' => $instrument_uuid
        ]);
    }

    public function actionGetOptionRate() {

        $id = Yii::$app->request->post('id');
        $value = Yii::$app->request->post('value');


        $model = Tutor::find()->where(['uuid' => $id])->asArray()->one();

        $package_type_model = OnlineTutorialPackageType::find()->where(['uuid' => $model['online_tutorial_package_type_uuid']])->asArray()->one();
        $package_type = $package_type_model['name'];

        $tutor_plans = OnlineTutorialPackageTutorwise::find()->where(['tutor_uuid' => $id])->asArray()->All();
        $query = new \yii\db\Query;
        $plans = $query->select(['o.*', 'p.uuid', 'p.name', 'p.price', 'p.gst', 'p.total_price', 'p.status', 'p.default_option', 'p.online_tutorial_package_type_uuid']
                        )
                        ->from('online_tutorial_package as o')
                        ->join('RIGHT JOIN', 'premium_standard_plan_rate as p', 'o.uuid = p.online_tutorial_package_uuid')
                        ->where(['p.online_tutorial_package_type_uuid' => $model['online_tutorial_package_type_uuid'],'p.status'=>'ENABLED'])
                        ->orderBy('p.payment_term_uuid')->all();



        $premium_standard_plan_rate_uuid = array_column($plans, 'uuid');
        $planuuids = implode(",", $premium_standard_plan_rate_uuid);


        $option_array = array();


        $i = 0;
        $html = '';
        $html .= '<table  class="table table-striped dt-responsive display" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Plan Name</th>
                                    <th style="text-align:right">Price</th>
                                    <th style="text-align:right">GST</th>

                                    <th style="text-align:right">Total Price</th>

                                </tr>
                            </thead>

                            <tbody>';

        foreach ($plans as $option_row) {

            $optionmodel = StandardPlanOptionRate::find()->where(['premium_standard_plan_rate_uuid' => $option_row['uuid'], 'option' => $value])->asArray()->one();

            //echo "<pre>";
            //print_r($option_row);exit;
            $j = $i + 1;
            $html .= '
            <tr>
                                            <td>' . $j . '</td>
                                            <td>' . $option_row['name'] . '</td>
                                            <td align="right">' . $optionmodel['price'] . '</td>
                                            <td align="right">' . $optionmodel['gst'] . '</td>
                                            <td align="right">' . $optionmodel['total_price'] . '</td>

                                        </tr>';


            $i++;
        }

        $html .= '</tbody>
                        </table>';

        return $html;
    }

    public function actionDefaultplanentry() {
        $model = Tutor::find()->asArray()->All();


        foreach ($model as $tutor_row) {
            $tutor_uuid = $tutor_row['uuid'];
            $online_tutorial_package_type_uuid = $tutor_row['online_tutorial_package_type_uuid'];

            $planmodel = PremiumStandardPlanRate::find()->where(['online_tutorial_package_type_uuid' => $online_tutorial_package_type_uuid])->asArray()->All();

            foreach ($planmodel as $plan_row) {

                $online_tutorial_package_uuid = $plan_row['online_tutorial_package_uuid'];
                $premium_standard_plan_rate_uuid = $plan_row['uuid'];
                $standard_plan_rate_option = $plan_row['default_option'];

                $tutorial_plan = new OnlineTutorialPackageTutorwise();
                $tutorial_plan->tutor_uuid = $tutor_uuid;
                $tutorial_plan->online_tutorial_package_type_uuid = $online_tutorial_package_type_uuid;
                $tutorial_plan->online_tutorial_package_uuid = $online_tutorial_package_uuid;
                $tutorial_plan->premium_standard_plan_rate_uuid = $premium_standard_plan_rate_uuid;
                $tutorial_plan->standard_plan_rate_option = $standard_plan_rate_option;
                $tutorial_plan->save(false);
            }
        }
    }

    public function actionAssignPlan($id) {
        $model = Tutor::find()->where(['uuid' => $id])->asArray()->one();


        $package_type_model = OnlineTutorialPackageType::find()->where(['uuid' => $model['online_tutorial_package_type_uuid']])->asArray()->one();
        $package_type = $package_type_model['name'];

        $tutor_plans = OnlineTutorialPackageTutorwise::find()->where(['tutor_uuid' => $id])->asArray()->All();
        $query = new \yii\db\Query;
        $plans = $query->select(['o.*', 'p.uuid', 'p.name', 'p.price', 'p.gst', 'p.total_price', 'p.status', 'p.default_option', 'p.online_tutorial_package_type_uuid']
                        )
                        ->from('online_tutorial_package as o')
                        ->join('RIGHT JOIN', 'premium_standard_plan_rate as p', 'o.uuid = p.online_tutorial_package_uuid')
                        ->where(['p.online_tutorial_package_type_uuid' => $model['online_tutorial_package_type_uuid'],'p.status' => 'ENABLED'])
                        ->orderBy('p.payment_term_uuid')->all();



        $premium_standard_plan_rate_uuid = array_column($plans, 'uuid');
        $planuuids = implode(",", $premium_standard_plan_rate_uuid);


        $option_array = array();


        $i = 0;

        foreach ($plans as $option_row) {

            $optionmodel = StandardPlanOptionRate::find()->where(['premium_standard_plan_rate_uuid' => $option_row['uuid'], 'option' => $model['standard_plan_rate_option']])->asArray()->one();

            //echo "<pre>";
            //print_r($option_row);exit;
            $option_array[$i]['uuid'] = $option_row['uuid'];
            $option_array[$i]['name'] = $option_row['name'];

            $option_array[$i]['price'] = $optionmodel['price'];
            $option_array[$i]['gst'] = $optionmodel['gst'];
            $option_array[$i]['total_price'] = $optionmodel['total_price'];
            $option_array[$i]['option'] = $optionmodel['option'];
            $i++;
        }


        if ($package_type == "STANDARD") {


            $searchModel = new TutorInstrumentSearch();
            $searchModel->tutor_uuid = $model['uuid'];
            $instrumentList = $searchModel->search(Yii::$app->request->queryParams, true);



            return $this->render('assign_plan', [
                        'model' => $model,
                        'instrumentList' => $instrumentList,
                        'searchModel' => $searchModel,
            ]);
        } else {
            return $this->render('premium_assign_plan', ['plans' => $plans, 'model' => $model, 'tutor_plans' => $tutor_plans, 'option_array' => $option_array
            ]);
        }
    }

}
