<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PaymentTransaction;

/**
 * PaymentTransactionSearch represents the model behind the search form of `app\models\PaymentTransaction`.
 */
class PaymentTransactionSearch extends PaymentTransaction
{
    public $fullname;
    public $order_id;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'student_uuid', 'transaction_number', 'book_uuid', 'type', 'payment_datetime', 'payment_method', 'reference_number', 'status', 'subscription_uuid', 'response', 'fullname', 'order_id'], 'safe'],
            [['amount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $modeDatatable = false, $statusnot = '')
    {
        $query = PaymentTransaction::find();
        $query->joinWith(['studentUu']);
        $query->joinWith(['studentTuitionBookingUu']);
        $query->joinWith(['studentMusicoinPackageUu']);
        $query->joinWith(['orderUu']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        if (!empty($statusnot)) {
            if (is_array($statusnot)) {

                $query->andWhere(['not in', 'status', $statusnot]);
            } else {

                $query->andWhere(['<>', 'status', $statusnot]);
            }
        }
        if($this->status !== 'ALL'){
            $query->andFilterWhere(['payment_Transaction.status' => $this->status]);
        }

        if(!empty($this->fullname)){ 
            $query->andFilterWhere(['ilike', "concat(first_name,' ',last_name)", $this->fullname]); 
        }
        
        // grid filtering conditions
        $query->andFilterWhere([
            "to_char(payment_datetime, 'DD-MM-YYYY')" => $this->payment_datetime,
            'amount' => $this->amount,
        ]);

        $query->andFilterWhere(['ilike', 'uuid', $this->uuid])
            //->andFilterWhere(['ilike', 'CAST(student.first_name AS TEXT)', $this->student_uuid])
            ->andFilterWhere(['ilike', 'student.first_name', $this->student_uuid])
            ->andFilterWhere(['ilike', 'student.last_name', $this->student_uuid])
            ->andFilterWhere(['ilike', 'transaction_number', $this->transaction_number])
            ->andFilterWhere(['ilike', 'CAST(book_uuid AS TEXT)', $this->book_uuid])
            ->andFilterWhere(['ilike', 'type', $this->type])
            ->andFilterWhere(['ilike', 'payment_method', $this->payment_method])
            ->andFilterWhere(['ilike', 'reference_number', $this->reference_number])
            //->andFilterWhere(['ilike', 'status', $this->status])
            ->andFilterWhere(['ilike', 'subscription_uuid', $this->subscription_uuid])
            ->andFilterWhere(['ilike', 'response', $this->response])
            ->andFilterWhere(['ilike', 'order.order_id', $this->order_id]);
        
//echo $query->createCommand()->getRawSql();
        if ($modeDatatable) {
            $result = $query->asArray()->orderBy('payment_datetime DESC')->all();
            return $result;
        }
        
        return $dataProvider;
    }
}
