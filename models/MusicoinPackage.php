<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "musicoin_package".
 *
 * @property string $uuid
 * @property string $name
 * @property string $description
 * @property string $price
 * @property string $gst
 * @property string $total_price
 * @property string $gst_description
 * @property string $coins
 * @property string $bonus_coins
 * @property string $total_coins
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 */
class MusicoinPackage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'musicoin_package';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //html tag filter
            [['name','description','gst_description'], 'match', 'pattern' => \app\libraries\General::validTextPattern(['@','$','(',')','\[','\]','\/',]), 'message' => 'Invalid charecters used.'], 
            
            [['name', 'description','price', 'gst', 'total_price', 'coins', 'bonus_coins', 'total_coins', 'status'], 'required'],
            [['uuid', 'name', 'description', 'gst_description', 'status'], 'string'],
            [['price', 'gst', 'total_price', 'coins', 'bonus_coins', 'total_coins','bonus_coins_amount'], 'number'],
            [['coins', 'bonus_coins', 'total_coins'], 'number','min' =>  0],
            [['coins', 'bonus_coins', 'total_coins'], 'string','max' =>  10],
            [['created_at', 'updated_at'], 'safe'],
            [['uuid'], 'unique'],
            [['price', 'gst',], 'match', 'pattern' => '/^\d{0,10}(\.\d{0,2})?$/', 'message' => 'Only allow decimal 2 digit.'],
            [['coins', 'bonus_coins'], 'match', 'pattern' => '/^\d{0,10}?$/', 'message' => 'Only allow digit not allow decimal digit.'],
            ['gst', 'compare', 'compareAttribute' => 'price', 'operator' => '<', 'enableClientValidation' => false],
            ['price', 'compare', 'compareAttribute' => 'gst', 'operator' => '>', 'enableClientValidation' => false],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'price' => Yii::t('app', 'Price'),
            'gst' => Yii::t('app', 'Gst'),
            'total_price' => Yii::t('app', 'Total Price'),
            'bonus_coins_amount' => Yii::t('app', 'Bonus Coins Price'),
            'gst_description' => Yii::t('app', 'Gst Description'),
            'coins' => Yii::t('app', 'Coins'),
            'bonus_coins' => Yii::t('app', 'Bonus Coins'),
            'total_coins' => Yii::t('app', 'Total Coins'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
