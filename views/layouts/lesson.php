<?php
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/js/jquery-3.2.1.min.js');

$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/datatables/js/datatables.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/datatables/extensions/Buttons/js/dataTables.buttons.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/datatables/extensions/Buttons/js/buttons.flash.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/datatables/extensions/Buttons/js/buttons.html5.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/datatables/extensions/Buttons/js/buttons.print.min.js');


$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/messenger/js/messenger.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/messenger/js/messenger-theme-future.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/messenger/js/messenger-theme-flat.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/js/messenger.js');

//Calender
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/calendar/moment.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/jquery-ui/smoothness/jquery-ui.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/calendar/fullcalendar.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/js/jquery.form.js');

$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/jquery-validation/js/jquery.validate.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/js/form-validation.js');

foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
    if ($key == 'success') {
        $this->registerJs("showSuccess('$message');");
    } else {
        $this->registerJs("showErrorMessage('$message');");
    }
}

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\NewAppAsset;
use yii\helpers\Url;
use yii\web\View;
use app\models\Tutor;
use app\models\Student;

NewAppAsset::register($this);

$page_length = (Yii::$app->params['datatable']['page_length'] > 0 ) ? Yii::$app->params['datatable']['page_length'] : 10;
$this->registerJs("
        var dt_page_length = " . $page_length . ";
        var web_url = '" . Url::to(['/']) . "';
       ", View::POS_HEAD);


$this->registerJs(
        '
// Global form submit loader display code
$("form").on("beforeValidate", function (event, messages, deferreds) {
    blockBody("Loading...");

    // called when the validation is triggered by submitting the form
    // return false if you want to cancel the validation for the whole form

}).on("afterValidate", function (event, messages,errorAttributes) {
    
    if (errorAttributes.length) {
    
        unblockBody();
    }

}).on("beforeSubmit", function () {
    blockBody("Loading...");
   //return false;
    // after all validations have passed
    // you can do ajax form submission here
    // return false if you want to stop form submission
});

', yii\web\View::POS_END
);


$profile_url = Url::to(['student/profile']);

$currentController = Yii::$app->controller->id;
$currentAction = Yii::$app->controller->action->id;
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>

        <link rel="shortcut icon" href="/theme_assets/images/favicon.ico" type="image/x-icon" /><!-- Favicon -->
        <link rel="apple-touch-icon-precomposed" href="/theme_assets/images/apple-touch-icon-57-precomposed.png"><!-- For iPhone -->
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/theme_assets/images/apple-touch-icon-114-precomposed.png">   <!-- For iPhone 4 Retina display -->
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/theme_assets/images/apple-touch-icon-72-precomposed.png"><!-- For iPad -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/theme_assets/images/apple-touch-icon-144-precomposed.png"><!-- For iPad Retina display -->

        <?php $this->head() ?>
    </head>
    
    <?php  /*<body class="lesson_hi hold-transition skin-blue sidebar-mini">
        <?php $this->beginBody() ?>
        <header class="main-header">
                <nav class="black navbar navbar-static-top margin_left_zero">
                    <a href="#" class="" data-toggle="push-menu" role="button"><img class="logo" src="<?= Yii::$app->request->baseUrl . '/theme_assets/new_book/img/logo.png'?>"></a>
                   

                </nav>
            </header>
        <?= $content ?>
        
        <?php $this->endBody() ?>
    </body>
    */ ?>
    
    
 <body class="hold-transition1 lesson_hi">
        <?php $this->beginBody() ?>
     
        <?= $content ?>
        
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
