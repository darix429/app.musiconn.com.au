<?php

namespace app\controllers;

use Yii;
use app\models\Coupon;
use app\models\CouponSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
//use app\models\User;
use app\libraries\General;
use yii\helpers\Url;
use yii\filters\AccessControl;
use app\models\ChangePassword;

/**
 * CouponController implements the CRUD actions for Coupon model.
 */
class CouponController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => Yii::$app->user->can('/coupon/index'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['coupon-enabled'],
                        'allow' => Yii::$app->user->can('/coupon/coupon-enabled'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['coupon-disabled'],
                        'allow' => Yii::$app->user->can('/coupon/coupon-disabled'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['create'],
                        'allow' => Yii::$app->user->can('/coupon/create'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['update'],
                        'allow' => Yii::$app->user->can('/coupon/update'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => Yii::$app->user->can('/coupon/delete'),
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new CouponSearch();
        $searchModel->status = 'ENABLED';
        $couponList = $searchModel->search(Yii::$app->request->queryParams, true);
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_coupon_list', [
                        'couponList' => $couponList,
            ]);
        } else {
            return $this->render('index', [
                        //'inactiveAdmin' => $inactiveAdmin,
                        'couponList' => $couponList,
                        'searchModel' => $searchModel,
            ]);
        }
    }

    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate() {
        $model = new Coupon();
        $model->scenario = 'coupon_create';

        $model->attributes = Yii::$app->getRequest()->post('Coupon');
        $model->category = 'TU';
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if (isset($model->type) && $model->type == 'Percentage') {
                $model->amount = $model->percentage;
            }


            $model->start_date = date('Y-m-d 23:59:59', strtotime($model->start_date));
            $model->end_date = date('Y-m-d 23:59:59', strtotime($model->end_date));
            $model->created_at = $model->updated_at = date('Y-m-d H:i:s', time());
            if ($model->save(false)) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Coupon successfully created.'));
                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    public function actionUpdate($id) {
        try {
            $model = $this->findModel($id);
            if (isset($model->type) && $model->type == 'Percentage') {
                $model->percentage = $model->amount;
                $model->amount = '';
            }

            $model->attributes = Yii::$app->getRequest()->post('Coupon');

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {

                if (isset($model->type) && $model->type == 'Percentage') {
                    $model->amount = $model->percentage;
                }

                $model->start_date = date('Y-m-d 23:59:59', strtotime($model->start_date));
                $model->end_date = date('Y-m-d 23:59:59', strtotime($model->end_date));
                $model->created_at = $model->updated_at = date('Y-m-d H:i:s', time());
                if ($model->save(false)) {
                    Yii::$app->session->setFlash('success', Yii::t('app', 'Coupon successfully updated.'));
                    return $this->redirect(['index']);
                }
            }
            return $this->render('update', [
                        'model' => $model,
            ]);
        } catch (\yii\base\Exception $exception) {
            throw new \yii\web\HttpException(505, $exception->getMessage());
        }
    }

    public function actionDelete() {
        $id = Yii::$app->getRequest()->post('id');
        if ((($model = Coupon::findOne($id)) !== null)) {
            $model->delete();
            $return = ['code' => 200, 'message' => 'Coupon deleted successfully.'];
        } else {

            $return = ['code' => 404, 'message' => 'The requested page does not exist.'];
        }
        Yii::$app->response->format = 'json';
        return $return;
    }

    protected function findModel($id) {
        if (($model = Coupon::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionCouponDisabled($id) {

        if (($coupon = Coupon::findOne($id)) !== null) {

            if ($coupon->status == "ENABLED") {


                $coupon->status = 'DISABLED';
                $coupon->save(false);

                $return = ['code' => 200, 'message' => 'Coupon disabled successfully.'];
            } else {
                $return = ['code' => 421, 'message' => 'Coupon not enabled.'];
            }
        } else {
            $return = ['code' => 404, 'message' => 'Record does not exist.'];
        }

        Yii::$app->response->format = 'json';
        return $return;
    }

    public function actionCouponEnabled($id) {

        if (($coupon = Coupon::findOne($id)) !== null) {

            if ($coupon->status == "DISABLED") {


                $coupon->status = 'ENABLED';
                $coupon->save(false);

                $return = ['code' => 200, 'message' => 'Coupon enabled successfully.'];
            } else {
                $return = ['code' => 421, 'message' => 'Coupon not disabled.'];
            }
        } else {
            $return = ['code' => 404, 'message' => 'Record does not exist.'];
        }

        Yii::$app->response->format = 'json';
        return $return;
    }

}
