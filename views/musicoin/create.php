<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MusicoinLog */

$this->title = Yii::t('app', 'Create Musicoin Log');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Musicoin Logs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="musicoin-log-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
