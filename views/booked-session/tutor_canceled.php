<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;
use app\models\Tutor;
use app\models\BookedSession;
use app\libraries\General;


$this->title = Yii::t('app', 'My Lessons');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-lg-12">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <section class="box"> 
                <!-- Horizontal - start -->
                <div class="content-body">    
                    <div class="row">
                        <div class="col-lg-12">
                            <a class="btn btn-purple btn-icon  right15 pull-right tutor-unavailability" style="cursor:pointer"> Set Unavailability </a>
                            <div class="clearfix"></div>
                            <ul class="nav nav-tabs primary"  id="myTab1" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link " href="<?= Yii::$app->params['base_url']. 'booked-session/upcoming'; ?>" >
                                         Upcoming Lessons
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="<?= Yii::$app->params['base_url']. 'booked-session/completed'; ?>" >
                                         Completed Lessons
                                    </a>
                                </li>
                                <li class="nav-item">                                    
                                        <a class="nav-link active" role="tab" href="#canceled_tution" data-toggle="tab">
                                         Cancelled Lessons
                                    </a>
                                </li>
                            </ul>

                            <div class="tab-content primary" id="myTabContent1">
                                <div class="tab-pane fade " id="upcoming_tution">
                                    
                                    
                                </div>

                                <div class="tab-pane fade show active" id="canceled_tution">
                                    <div>
                                       
                                        <!---start---->
                                        <div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">
                                            <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Student</th>
                                                        <th>Date</th>
                                                        <th>Time</th>
                                                        <th>Cancel Date</th>
                                                        <!--<th>Reason</th>-->
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if (!empty($canceledList)) {
                                                        foreach ($canceledList as $key => $value) {
                                                            $now = date('Y-m-d H:i:s');
                                                                ?>
                                                                <tr>
                                                                    <td><?= $key + 1; ?></td>
                                                                    <td><?= $value['studentUu']['first_name']. ' ' .$value['studentUu']['last_name']; ?></td>
                                                                    <td><?= General::displayDate($value['start_datetime']); ?></td>
                                                                    <td><?= General::displayTime($value['start_datetime']) ." - ".General::displayTime($value['end_datetime']); ?></td>
                                                                    <td><?= General::displayDate($value['cancel_datetime']); ?></td>                                                                    
                                                                    <!--<td><?php //$value['cancel_reason']; ?></td>-->                                                                    
                                                                </tr>
                                                                <?php
                                                        }
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>

                                        </div>
                                        <!---end--->
                                    </div>
                                </div>

                            </div>

                        </div>
                       
                    </div>
                    <!-- Horizontal - end -->
                </div>
            </section>

        </div>
    </div>      
</div>
