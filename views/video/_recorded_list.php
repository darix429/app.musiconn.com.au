<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use app\libraries\General;
?>
<style>
    .custom-box{
        height: 141px;
        width: 211px;
        text-align: center;
        font-size: 20px;
        margin-top:93px;
    }
    .music_genre .thumb {
        height:96%;
    }
    .gallery-parent-div {
        border: 0px solid;
        padding: 0;
    }
</style>

<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="row">
        <?php

        if (!empty($videoList)) {
            foreach ($videoList as $key => $value) {

            if(!empty($value['filename'])):

            	//$instrument = \app\models\Instrument::getInstrumentUsingUuid($value['studentBookingUu']['instrument_uuid']);
				$instrument = \app\models\Instrument::getInstrumentUsingUuid($value['instrument_uuid']);

                $thumb_url = is_file(Yii::$app->params['media']['session_recorded_video']['thumbnail']['path'] . $value['uuid'].'.jpg') ?
                                                        Yii::$app->params['media']['session_recorded_video']['thumbnail']['url'] . $value['uuid'].'.jpg' :
                                                        Yii::$app->params['theme_assets']['url'] . "images/thumbnail.jpeg";
                ?>

                <div class="col-lg-3 col-sm-6 col-md-4 music_genre">
                    <div class="team-member gallery-parent-div">
                        <div class="thumb">
                            <a href="<?= Url::to(['/video/tution-view/', 'id' => $value['uuid']]) ?>">
                                <img class="small-size" src="<?= $thumb_url; ?>">
                            </a>
                            <div class="overlay">
                                <a href="<?= Url::to(['/video/tution-view/', 'id' => $value['uuid']]) ?>" class="overlay">
                                    <i class="fa fa-eye"></i>
                                </a>
                            </div>
                        </div>

                        <div class="team-info ">
                            <h4 style="line-height: 18px !important;">&nbsp;
                                <a href="<?= Url::to(['/video/tution-view/', 'id' => $value['uuid']]) ?>">
                                	<?= $value['studentUu']['first_name']." ".$value['studentUu']['last_name'];?>
                                </a>
                                <br>&nbsp;
                                <a href="<?= Url::to(['/video/tution-view/', 'id' => $value['uuid']]) ?>">
                                	(<b style='font-size:10px'><?= $instrument['name']; ?></b>)
                                </a>
                                <br>&nbsp;
                                <a href="<?= Url::to(['/video/tution-view/', 'id' => $value['uuid']]) ?>">

                                        <b style='font-size:10px'><?= General::displayDate($value['start_datetime']). ' ' .General::displayTime($value['start_datetime']).' - '.General::displayTime($value['end_datetime']); ?></b>
                                </a>


                            </h4>
                        </div>

                        <div class="" align="center">
                            <span><a href="<?= Url::to(['/video/tution-view/', 'id' => $value['uuid']]) ?>" style="color: rgba(147, 149, 150, 1);" title="Review"><i class="box_toggle fa fa-eye" style="font-size:18px;"></i></a></span>
                        </div>

                    </div>
                </div>
                <?php endif;
            }
        } else {
            ?>
            <h5 style="margin-left: 25px;"><b>No video found</b></h5>
        <?php }
        ?>
    </div>
</div>
