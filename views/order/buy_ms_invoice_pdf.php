<?php

use yii\helpers\Html;
use app\libraries\General;
use app\models\Student;

$student_address = '';
$student_name = '';
$student_id = '';
$student_city_state_postcode = '';
$order_date = date("d-m-Y", strtotime($model->order_datetime));
$modelStudent = Student::findOne($model->student_uuid);
if (!empty($modelStudent)) {
    $student_address = $modelStudent->street;
    $student_name = $modelStudent->first_name . ' ' . $modelStudent->last_name;
    $student_id = $modelStudent->enrollment_id;
    $student_city_state_postcode = $modelStudent->city . ', ' . $modelStudent->state . ', ' . $modelStudent->country . ', ' . $modelStudent->postal_code;

    $student_tz = General::getStudentTimezone($model->student_uuid);
    $order_date = General::convertTimezone($model->order_datetime, Yii::$app->params['timezone'], $student_tz,'d-m-Y');
}
?>

<section class="box ">
    <div class="content-body">    
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-12" >
                        <div class="invoice-head"  >
                            <div style="float:left" >
                                <img src="<?= Yii::$app->params['theme_assets']['url'] . 'images/pdf_logo.png' ?>"  width="150px">
                            </div>
                        </div>

                    </div>
                    <div class="clearfix"></div>
                    <div class="spacer"></div>
                </div>

                <div class="row cmp_info_row middle_row">
                    <div class="col-lg-1 col-md-1 col-1" ></div>                        
                    <div class="col-lg-10 col-md-10 col-10">                        
                        <table class="info_tbl">                                
                            <tr >
                                <td class="info_tbl_td_bold">Musiconn Pty Ltd</td>
                            </tr>
                            <tr >
                                <td class="info_tbl_td">ABN:  72 614 882 003</td>
                            </tr>
                            <tr >
                                <td class="info_tbl_td">408/27 Lonsdale Street</td>
                            </tr>
                            <tr >
                                <td class="info_tbl_td">Braddon  ACT  2612</td>
                            </tr>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                    <div class="spacer"></div>
                </div>

                <div class="row cust_row middle_row">
                    <div class="col-lg-1 col-md-1 col-1" ></div>                        
                    <div class="col-lg-10 col-md-10 col-10">                        
                        <table class="cust_tbl">                                
                            <tr >
                                <td class="cust_tbl_td" colspan="2"><?= $student_name; ?></td>
                            </tr>
                            <tr >
                                <td class="cust_tbl_td" colspan="2">Student ID: <?= $student_id; ?></td>
                            </tr>
                            <tr >
                                <td class="cust_tbl_td" colspan="2"><?= $student_address; ?></td>
                            </tr>
                            <tr >
                                <td class="cust_tbl_td"><?= $student_city_state_postcode; ?></td>
                                <td class="cust_tbl_td_big_title">Recipient Created Tax Invoice</td>                                
                            </tr>
                            <tr >
                                <td class="cust_tbl_td_right" colspan="2">Invoice # <?= $model->order_id; ?></td>
                            </tr>
                            <tr >
                                <td class="cust_tbl_td_right" colspan="2">Invoice Date # <?= $order_date; ?></td>
                            </tr>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                    <div class="spacer"></div>
                </div>


                <div class="row item_row middle_row">
                    <div class="col-lg-1 col-md-1 col-1" ></div>                        
                    <div class="col-lg-10 col-md-10 col-10">                        
                        <table class="item_tbl">
                            <thead> 
                                <tr >
                                    <td class="item_tbl_th text-center" width="110px">Transaction ID</td>
                                    <td class="item_tbl_th">Description</td>
                                    <td class="item_tbl_th text-right" width="80px">Total</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $tax = 0;
                                
                                if (!empty($order_item)) {
                                    foreach ($order_item as $key => $item) {
                                        
                                        $item_title = $item['item'];
                                        $tax += $item['tax'];
                                        ?>
                                        <tr class="cls_table_tr">
                                            <td class="item_tbl_td text-center" ><?= $transaction_model['transaction_number']; ?></td>
                                            <td class="item_tbl_td"><?= $item_title; ?></td>
                                            <td class="item_tbl_td text-right" >$ <?= $item['total']; ?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>

                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                    <div class="spacer"></div>
                </div>

                <div class="row total_row middle_row">
                    <div class="col-lg-1 col-md-1 col-1" ></div>                        
                    <div class="col-lg-10 col-md-10 col-10">                        
                        <table class="total_tbl">
                            <tbody>                    
                                <tr class="">
                                    <td width="110px"></td>
                                    <td class="total_tbl_td">Price includes:</td>
                                    <td width="80px"></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td class="total_tbl_td" style="padding-left:50px;">10% GST</td>
                                    <td class="total_tbl_td text-right total_price_td"><?= $tax; ?></td>
                                </tr>
                                
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                    <div class="spacer"></div>
                </div>



            </div>
        </div>
    </div>
</section>