<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Instrument */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="instrument-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <!--?= $form->field($model, 'uuid')->textInput() ?-->

    <?= $form->field($model, 'name')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'image')->fileInput() ?>
    <?php //echo $form->labelEx($model,'image'); ?>
    <?php //echo CHtml::activeFileField($model, 'image'); ?>
    <?php //echo $form->error($model,'image'); ?>	

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
