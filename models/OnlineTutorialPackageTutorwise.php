<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "online_tutorial_package_tutorwise".
 *
 * @property string $uuid
 * @property string $tutor_uuid
 * @property string $online_tutorial_package_type_uuid
 * @property string $online_tutorial_package_uuid
 * @property string $premium_standard_plan_rate_uuid
 * @property string $standard_plan_rate_option
 * @property string $created_date
 * @property string $updated_date
 */
class OnlineTutorialPackageTutorwise extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'online_tutorial_package_tutorwise';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'tutor_uuid', 'online_tutorial_package_type_uuid', 'online_tutorial_package_uuid'], 'required'],
            [['uuid', 'tutor_uuid', 'online_tutorial_package_type_uuid', 'online_tutorial_package_uuid', 'premium_standard_plan_rate_uuid', 'standard_plan_rate_option'], 'string'],
            [['created_date', 'updated_date'], 'safe'],
            [['uuid'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'tutor_uuid' => Yii::t('app', 'Tutor Uuid'),
            'online_tutorial_package_type_uuid' => Yii::t('app', 'Online Tutorial Package Type Uuid'),
            'online_tutorial_package_uuid' => Yii::t('app', 'Online Tutorial Package Uuid'),
            'premium_standard_plan_rate_uuid' => Yii::t('app', 'Premium Standard Plan Rate Uuid'),
            'standard_plan_rate_option' => Yii::t('app', 'Standard Plan Rate Option'),
            'created_date' => Yii::t('app', 'Created Date'),
            'updated_date' => Yii::t('app', 'Updated Date'),
        ];
    }
    
    //Tutor wise onlinetutorial entry when New online plan add (New plan assign to Tutors)
    
    public function tutorWiseNewOnlinePlanEntry_assigntoTutor($data) {
        $premium_standard_plan_rate_uuid = $data['premium_standard_plan_rate_uuid'];
        $modelPSplan = PremiumStandardPlanRate::findOne($premium_standard_plan_rate_uuid);
        if (!empty($modelPSplan)) {

            $tutorArray = Tutor::find()->where(['online_tutorial_package_type_uuid' => $modelPSplan->online_tutorial_package_type_uuid])->all();
            if (!empty($tutorArray)) {

                foreach ($tutorArray as $tutor) {

                    $tutorInst = TutorInstrument::find()->where(['tutor_uuid' => $tutor->uuid])->all();

                    $packegetypeModel = \app\models\OnlineTutorialPackageType::findOne($tutor->online_tutorial_package_type_uuid);


                    if (!empty($packegetypeModel)) {

                        if ($packegetypeModel->name == "STANDARD") {

                            foreach ($tutorInst as $tutor_instrument) {
                                $tutor_online_plan = OnlineTutorialPackageTutorwise::find()->where([
                                            'tutor_uuid' => $tutor->uuid,
                                            'instrument_uuid' => $tutor_instrument->instrument_uuid,
                                            'online_tutorial_package_type_uuid' => $tutor->online_tutorial_package_type_uuid,
                                            'online_tutorial_package_uuid' => $modelPSplan->online_tutorial_package_uuid,
                                            'premium_standard_plan_rate_uuid' => $modelPSplan->uuid,
                                            'standard_plan_rate_option' => $modelPSplan->default_option,
                                        ])->one();
                                if (empty($tutor_online_plan)) {

                                    $tutor_online_plan = new OnlineTutorialPackageTutorwise();
                                    $tutor_online_plan->tutor_uuid = $tutor->uuid;
                                    $tutor_online_plan->instrument_uuid = $tutor_instrument->instrument_uuid;
                                    $tutor_online_plan->online_tutorial_package_type_uuid = $tutor->online_tutorial_package_type_uuid;
                                    $tutor_online_plan->online_tutorial_package_uuid = $modelPSplan->online_tutorial_package_uuid;
                                    $tutor_online_plan->premium_standard_plan_rate_uuid = $modelPSplan->uuid;
                                    $tutor_online_plan->standard_plan_rate_option = $modelPSplan->default_option;
                                    $tutor_online_plan->save(false);
                                }
                            }
                        } else {

                            $tutor_online_plan = OnlineTutorialPackageTutorwise::find()->where([
                                        'tutor_uuid' => $tutor->uuid,
                                        'online_tutorial_package_type_uuid' => $tutor->online_tutorial_package_type_uuid,
                                        'online_tutorial_package_uuid' => $modelPSplan->online_tutorial_package_uuid,
                                        'premium_standard_plan_rate_uuid' => $modelPSplan->uuid,
                                        'standard_plan_rate_option' => $modelPSplan->default_option,
                                    ])->one();
                            if (empty($tutor_online_plan)) {
                                $tutor_online_plan = new OnlineTutorialPackageTutorwise();
                                $tutor_online_plan->tutor_uuid = $tutor->uuid;
                                $tutor_online_plan->online_tutorial_package_type_uuid = $tutor->online_tutorial_package_type_uuid;
                                $tutor_online_plan->online_tutorial_package_uuid = $modelPSplan->online_tutorial_package_uuid;
                                $tutor_online_plan->premium_standard_plan_rate_uuid = $modelPSplan->uuid;
                                $tutor_online_plan->standard_plan_rate_option = $modelPSplan->default_option;
                                $tutor_online_plan->save(false);
                            }
                        }
                    }
                }
            }
        }
    }
}
