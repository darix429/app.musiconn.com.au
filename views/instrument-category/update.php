<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
//rupal
use yii\helpers\ArrayHelper;
use app\models\InstrumentCategory;
use yii\helpers\Url;

if(!$model->isNewRecord){
	$p_query = InstrumentCategory::find()->where(['not', ['uuid' => $model->uuid]])->andWhere(['not', ['parent_id' => $model->uuid]])->all();
}else{
	$p_query = InstrumentCategory::find()->all();
}
$parentIdArray = ArrayHelper::map($p_query,'uuid','name');

/* @var $this yii\web\View */
/* @var $model app\models\Admin */

$this->title = Yii::t('app', 'Instrument Category : ' . $model->name, [
    'nameAttribute' => '' . $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Instrument Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->uuid, 'url' => ['view', 'id' => $model->uuid]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

?>


<div class="col-xl-12 col-lg-12 col-12 col-md-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Please Fill Information</h2>
            <div class="actions panel_actions float-right">
               <a href="<?= Yii::$app->getUrlManager()->createUrl(['/instrument-category/index/'])?>" class="btn btn-info btn-icon "><i class="fa fa-arrow-left text-white"></i> <span>Back</span></a>
            </div>
        </header>
        <div class="content-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12">
                   <?php $form = ActiveForm::begin(['id' => 'category_update_form']); ?>
                    <div class="form-row">
                        <div class="col-md-6 mb-0">
                            <?= $form->field($model, 'name')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('name')]) ?>
                        </div>
                        <div class="col-md-6 mb-0">
                            <?= $form->field($model, 'parent_id')->dropDownList($parentIdArray, ['class' => 'form-control', 'maxlength' => true, 'prompt' => "Select Parent ID"]) ?>
                        </div>                         
                    </div>

                    <?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-primary', 'style' => 'float: right;']) ?>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>

        </div>
    </section>
</div>


