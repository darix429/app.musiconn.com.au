<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;

$this->title = Yii::t('app', 'Tutor Availability');
$this->params['breadcrumbs'][] = $this->title;



$this->registerJs(
"
    $(function() {
        $('.popover').remove(); // Close all open popovers.
	var todayDate = moment().startOf('day');
	var YM = todayDate.format('YYYY-MM');
	var YESTERDAY = todayDate.clone().subtract(1, 'day').format('YYYY-MM-DD');
	var TODAY = todayDate.format('YYYY-MM-DD');
	var TOMORROW = todayDate.clone().add(1, 'day').format('YYYY-MM-DD');

	$('#tutor_calendar').fullCalendar({
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'agendaDay,agendaWeek,listWeek'
			//right: 'agendaDay,agendaWeek,month,listWeek'
		},
                buttonText: {
                    today: 'Today',
                    day: 'Day',
                    week: 'Week',
                    month: 'Month',
                    list: 'List'
                },
                allDayText: 'All Day',
                defaultView: 'agendaWeek',
		editable: true,
		eventLimit: true, // allow more link when too many events
		navLinks: true,
		//events: '".Url::to(['/student/tutor_availability_calender_events/','tutor_uuid' => $tutor_uuid])."',

                        events: function(start, end, timezone, callback){
                            $.ajax({
                                url: '".Url::to(['/student/tutor_availability_calender_events/','tutor_uuid' => $tutor_uuid])."',
                                dataType: 'json',
                                beforeSend:function(){
                                    blockBody();
                                },
                                data: {
                                  // our hypothetical feed requires UNIX timestamps
                                  start: start.unix(),
                                  end: end.unix()
                                },
                                success: function(calendarEvents) {
                                    callback(calendarEvents);
                                    unblockBody();
                                },
                                error:function(e){
                                    unblockBody();
                                    showErrorMessage(e.responseText);
                                }
                              });
                        }
                     ,
                viewRender: function(){
                    $('.close-popover').each(function () {
                        $(this).parents().eq(2).remove();
                    });
                },
                selectable: false,
                eventClick:  _calendarEventClick
	});

        //Event Popup start
        function _calendarEventClick(event, jsEvent, view) {

            $('.popover').remove(); // Close all open popovers.
            var html = '<style type=\"text/css\">.popover-content strong {min-width: 80px; display:inline-block;}.popover-content button {margin-right: 10px;}</style>'
                + '<strong>Start: </strong> '+ event.start.format('DD-MM-YYYY hh:mm A') +'<br>'
                + '<strong>End: </strong> '+ event.end.format('DD-MM-YYYY hh:mm A') +'<br>';
            if(event.type == 'schedule_tution'){
                html += '<strong>Tutor: </strong> '+ event.tutor_name +'<br>';
                html += '<strong>Student: </strong> '+ event.student_name +'<br>';
            }
            html += '<strong>Notes: </strong> '+ event.description +'<br>'
                + '<center><button class=\"close-popover btn btn-default\" data-po=[object HTMLDivElement]>Close</button>'
                + '</center>';

            $(jsEvent.target).popover({
                placement: 'top',
                title: event.title,
                content: html,
                html: true,
                container: '#tutor_calendar',
                trigger: 'manual'
            });
            $(jsEvent.target).popover('toggle');

            // Fix popover position.
            if ($('.popover').length > 0 && $('.popover').position().top < 200) {
                $('.popover').css('top', '200px');
            }
        }

        $('#tutorAvailabilityCalender-content').on('click', '.close-popover', function () {
            $(this).parents().eq(2).remove();
        });
        //Event Popup End


});


", View::POS_END
);
?>

<section class="box " >
    <header class="panel_header">

    </header>
    <div class="content-body">
        <div class="row ">
            <div class="col-lg-12 col-md-12 col-12 ">

                <div id='tutor_calendar' class="col-lg-12 col-md-11 col-11"></div>

            </div>
        </div>

    </div>
</section>
