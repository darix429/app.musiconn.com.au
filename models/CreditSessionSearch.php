<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CreditSession;

/**
 * CreditSessionSearch represents the model behind the search form of `app\models\CreditSession`.
 */
class CreditSessionSearch extends CreditSession
{
    public $rowsCount;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'booked_session_uuid', 'student_uuid', 'instrument_uuid', 'status', 'created_at', 'updated_at', 'tutor_uuid', 'tutorial_type_code','rowsCount','booking_uuid'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $modeDatatable = false, $statusnot = '', $statusIn = '',$orderBy = 'created_at asc',$group_by = '')
    {
        $query = CreditSession::find();
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        if (!empty($statusnot)) {
            if (is_array($statusnot)) { 

                $query->andWhere(['not in', 'status', $statusnot]);
            }else { 

                $query->andWhere(['<>', 'status', $statusnot]);
            }
        }
        
        if (!empty($statusIn)) { 
            if (is_array($statusIn)) {

                $query->andWhere(['in', 'status', $statusIn]);
            } 
        }
        if (Yii::$app->user->identity->role == 'STUDENT') { 
            $query->andFilterWhere(['student_uuid' => Yii::$app->user->identity->student_uuid]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'uuid', $this->uuid])
            ->andFilterWhere(['ilike', 'booked_session_uuid', $this->booked_session_uuid])
//            ->andFilterWhere(['ilike', 'student_uuid', $this->student_uuid])
            ->andFilterWhere(['ilike', 'instrument_uuid', $this->instrument_uuid])
//            ->andFilterWhere(['ilike', 'status', $this->status])
            ->andFilterWhere(['ilike', 'tutor_uuid', $this->tutor_uuid])
            ->andFilterWhere(['ilike', 'tutorial_type_code', $this->tutorial_type_code]);
        if(!empty($group_by)){
            $query->groupBy($group_by);
        }
        if ($modeDatatable) {
            $result = $query->asArray()->orderBy($orderBy)->all();
           //echo $query->createCommand()->getRawSql(); 
//            echo "<pre>"; print_r($result); 
            return $result;
        }
        return $dataProvider;
    }
}
