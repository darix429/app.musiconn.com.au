<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Settings */

$this->title = Yii::t('app', 'Create Test');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tests'), 'url' => ['create']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="settings-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
