<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;

$this->title = Yii::t('app', 'SUBSCRIPTION HISTORY');
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_icon'] = 'my-lesson-tag.png';


$this->registerJs(
        '

    
$(document).on("click","#search_btn", function(){        
    $.ajax({
        type: "GET",
        url: "' . Url::to(['/monthly-subscription/history/']) . '",
        data: $("#search_frm").serialize(),
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            $("#coupon_table_parent_div").html(result);
            window.NEW_CUSTOM.dataTablesInit();
        },
        error:function(e){
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});
    
$(document).on("click","#search_reset_btn", function(){        
    $.ajax({
        type: "GET",
        url: "' . Url::to(['/monthly-subscription/history/']) . '",
        data: {},
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            $("#coupon_table_parent_div").html(result);
            window.NEW_CUSTOM.dataTablesInit();
        },
        error:function(e){
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});
    
    
', View::POS_END
);
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row center-block box-search-area">
                        <div class="col-lg-12 col-md-12 col-12 ">
                            <div class="actions panel_actions pull-right custom-toggle" data-togglediv="datatable-search-div" title="Advance Search" style="margin-bottom: 5px; cursor: pointer">
                                <span>
                                <i class="fa fa-search " ></i>
                                    Advance Search
                                </span>
                            </div>
                            <div class="datatable-search-div " style="display: none;">

                                <?php
                                $form = ActiveForm::begin([
                                            'method' => 'get',
                                            'options' => [
                                                'id' => 'search_frm',
                                                'class' => 'form-inline'
                                            ],
                                ]);
                                ?>

                                <!--<div class="col-md-3 mb-0">-->
                                    <?= $form->field($searchModel, 'name')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Name'])->label(false) ?>
                                <!--</div>-->
                                <!--<div class="col-md-3 mb-0">-->
                                    <?= $form->field($searchModel, 'price')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Price'])->label(false) ?>
                                <!--</div>-->
                                <!--<div class="col-md-3 mb-0">-->
                                    <?= $form->field($searchModel, 'tax')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Tax'])->label(false) ?>
                                <!--</div>-->
                                <!--<div class="col-md-3 mb-0">-->
                                    <?= $form->field($searchModel, 'total_price')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Total Price'])->label(false) ?>
                                <!--</div>-->

                                <!--<div class="col-md-3 mb-0">-->
                                    <?= $form->field($searchModel, 'start_datetime')->textInput(['class' => 'form-control custom_date_picker', 'maxlength' => true, 'placeholder' => 'Start Date'])->label(false) ?>
                                <!--</div>-->
                                <!--<div class="col-md-3 mb-0">-->
                                    <?= $form->field($searchModel, 'end_datetime')->textInput(['class' => 'form-control custom_date_picker', 'maxlength' => true, 'placeholder' => 'End Date'])->label(false) ?>
                                <!--</div>-->
                            <!--<br><br>-->

                                <!--		                <div class="col-md-3 mb-0">
                                <?php // $form->field($searchModel, 'status')->dropDownList(['ENABLED' => 'ENABLED', 'DISABLED' => 'DISABLED'], ['class' => 'form-control', 'style' => 'width:100%'])->label(false) ?>
                                                                </div>  -->

                                <?= Html::Button(Yii::t('app', 'Search'), ['class' => 'btn btn-primary mb-0', 'id' => 'search_btn', 'style' => 'margin-left: 17px;']) ?>
                                <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-primary mb-0', 'id' => 'search_reset_btn']) ?>

                                <?php ActiveForm::end(); ?>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row" id="coupon_table_parent_div">
                        <?php echo Yii::$app->controller->renderPartial('_monthly_sub_history_list', ['monthlySubList' => $monthlySubList]); ?>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <a class="btn btn-primary btn-sm black_button_in" href="<?= Yii::$app->getUrlManager()->createUrl('/student/profile'); ?>">RETURN TO PROFILE</a>
    
</section>