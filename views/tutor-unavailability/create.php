<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TutorUnavailability */

$this->title = Yii::t('app', 'Create Tutor Unavailability');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tutor Unavailabilities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tutor-unavailability-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
