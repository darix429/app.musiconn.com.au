<?php

use yii\helpers\Html;
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['student_name']; ?>,</p>
            
            <p>Your subscription to Musiconn’s online contents has been renewed (transaction reference <?= $content['order_id']; ?>).</p>
            <p>We hope that you will continue to enjoy the wide range of educational contents on our website.</p>
            <p>Attached is an invoice as record of your purchase.</p>
            <p>Please do not reply to this unattended mailbox.  Any queries or feedback can be directed to admin@musiconn.com.au, or call us on 1300 068 742.</p>
            

            <p>Regards,<br>
            Admin Team – Musiconn Pty Ltd</p>
        </td>
    </tr>
</table>

