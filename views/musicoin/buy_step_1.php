<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\web\View;
use yii\helpers\ArrayHelper;
use app\models\MusicoinPackage;

/* @var $this yii\web\View */
/* @var $model app\models\MusicoinLog */
/* @var $form yii\widgets\ActiveForm */
//Yii::$app->user->identity->student_uuid
$planList = MusicoinPackage::find()->where(['status' => 'ENABLED'])->all();
$this->registerJs(
        ' 
            
    $("input[name=\"StudentMusicoinPackage[package_ckeck]\"]").on("ifClicked", function (event) {
        $("#studentmusicoinpackage-package_uuid").val(this.value);

        $("#step_1_next").removeClass("d-none");
    });
');
?>
<style>

    /* Important part */
    .modal-dialog{
        overflow-y: initial !important
    }
    .modal-body{
        height: 250px;
        overflow-y: auto;
        overflow-x: hidden;
        padding: 0px;
    }

    .package_tr{
        border-bottom: 1px solid #d2d5de;
        padding-top: 10px;
        padding-bottom: 0px;
    }
</style>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">BUY MUSICOINS</h4>
</div>
<div class="modal-body padding-no">

    <?php
    $form = ActiveForm::begin([
                'action' => ['index'],
                'id' => 'frm_step_1',
                'enableClientValidation' => true,
                'validateOnChange' => true,
                'options' => ['class' => 'form-horizontal']]);
    ?>
    <div class="row" style="margin:0px">
        <div class="col-md-12 " >

            <div class="row  package_tr pakage-list" style="padding-bottom: 10px;background-color: #d2d5de;">
                <div class="col-md-1 text-center"></div>
                <div class="col-md-6"><b>Name</b></div>
                <div class="col-md-2 text-right"><b>Coins</b></div>
                <div class="col-md-3 text-right"><b>Price</b></div>
            </div>
            <?php
            if (!empty($planList)) {
                foreach ($planList as $key => $value) {

                    $model->package_ckeck = $model->package_uuid;
                    $displayClass = ($model->package_uuid == $value->uuid) ? "" : "d-none";
                    ?>
                    <div class="row pakage-list package_tr" >
                        <div class="col-md-1 text-center">
                            <?php
                            echo $form->field($model, 'package_ckeck')->radio([
                                'label' => '',
                                'template' => '{input}{error} ',
                                'class' => 'skin-square-red float-left package_ckeckbox', 'value' => $value->uuid])->label('')
                            ?>
                        </div>
                        <div class="col-md-6">
                            <p class="">
                                <?= $value->name ?><br>
                                <small>Coins: <?= $value->coins; ?><?= ($value->bonus_coins > 0) ? ", Bonus Coins: " . $value->bonus_coins : ""; ?></small>
                            </p>
                        </div>
                        <div class="col-md-2 text-right"><p><?= $value->total_coins ?></p></div>
                        <div class="col-md-3 text-right"><p>$<?= $value->total_price ?></p></div>

                    </div>

                    <?php
                }
            }
            ?>
            <div class="row" style="">
                <input type="hidden" name="step" value="1">
                <?= $form->field($model, 'package_uuid')->hiddenInput(['class' => 'form-control', 'maxlength' => true,])->label(false) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<div class="modal-footer">
    <?php
    $next_btn_class = empty($model->package_uuid) ? "d-none" : "";
    echo Html::Button('Next', ['class' => 'btn btn-primary ' . $next_btn_class, 'id' => 'step_1_next', 'onclick' => 'save_step_1();'])
    ?>

</div>