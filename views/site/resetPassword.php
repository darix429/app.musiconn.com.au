<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \mdm\admin\models\form\ResetPassword */

$this->title = 'Reset Password';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="login-box">
    <!-- /.login-logo -->
    <div class="login-box-body">
        <h2 class="text-center"><?= $this->title; ?></h2>
        <?php
        $form = ActiveForm::begin([
                    'id' => 'reset-password-form',
                    'options' => ['name' => 'resetpasswordform'],
        ]);
        ?>

        <?= $form->field($model, 'password')->passwordInput(['autofocus' => true, 'class' => 'form-control'])->label('Password*') ?>
        <p>Please choose your new password:</p>

        <div class="row">
            <center>
                <?= Html::submitButton('Save', ['class' => 'login_btn btn btn-primary btn-block btn-flat', 'name' => 'save-button']) ?>
            </center>
            <!-- /.col -->
        </div>
        <?php ActiveForm::end(); ?>
        <center>
            <?= Html::a('Go to Sign In', ['site/login'], []) ?>
        </center>
        <div class="login-logo">
            <a href="<?= Yii::$app->params['base_url']; ?>"><img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new/' ?>img/login-logo.png"></a>
            <center><p class="login_btm"><?= Yii::$app->params['copyright']; ?></p></center>
        </div>
    </div>
    <!-- /.login-box-body -->
</div>
