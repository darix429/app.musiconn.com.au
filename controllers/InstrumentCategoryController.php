<?php

namespace app\controllers;

use Yii;
use app\models\InstrumentCategory;
use app\models\InstrumentCategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
//rupal
use app\libraries\General;
use yii\helpers\Url;
use yii\filters\AccessControl;


/**
 * InstrumentCategoryController implements the CRUD actions for InstrumentCategory model.
 */
class InstrumentCategoryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
	    'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                        [
                        'actions' => ['index'],
                        'allow' => Yii::$app->user->can('/instrument-category/index'),
                        'roles' => ['@'],
                    ],
			[
                        'actions' => ['create'],
                        'allow' => Yii::$app->user->can('/instrument-category/create'),
                        'roles' => ['@'],
                    ],
			[
                        'actions' => ['update'],
                        'allow' => Yii::$app->user->can('/instrument-category/update'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['delete'],
                        'allow' => Yii::$app->user->can('/instrument-category/delete'),
                        'roles' => ['@'],
                    ],
                        
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all InstrumentCategory models.
     * @return mixed
     */
    public function actionIndex()
    { 
        $searchModel = new InstrumentCategorySearch();
        $instrumentCategoryList = $searchModel->search(Yii::$app->request->queryParams,true);
	
	if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_instrument_categories_list', [
                        'instrumentCategoryList' => $instrumentCategoryList,
            ]);
        } else {
            return $this->render('index', [
                        //'inactiveAdmin' => $inactiveAdmin,
                        'instrumentCategoryList' => $instrumentCategoryList,
                        'searchModel' => $searchModel,
            ]);
        }
        
    }

    /**
     * Displays a single InstrumentCategory model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new InstrumentCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new InstrumentCategory();
	$parentid = Yii::$app->request->post('InstrumentCategory')['parent_id'];

	if($model->load(Yii::$app->request->post()) && $model->validate()){  
		
		if($parentid == ''){  
			$model->parent_id = null;
			$model->level = 0;
		} else{
			$data = InstrumentCategory::find()->where(['uuid' => $parentid])->one();
			$model->level = $data['level'] + 1;
		}
		
		if($model->save(false)){ 
			Yii::$app->session->setFlash('success', Yii::t('app', 'Instrument Category successfully created.'));
			return $this->redirect(['index']);
		}
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing InstrumentCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    { 
	try{
		$model = $this->findModel($id);
		$parentid = Yii::$app->request->post('InstrumentCategory')['parent_id'];		

		if ($model->load(Yii::$app->request->post())) {
			if($parentid == ''){  
				$model->parent_id = null;
				$model->level = 0;
			} else{
				$data = InstrumentCategory::find()->where(['uuid' => $parentid])->one();
				$model->level = $data['level'] + 1;
				
			}
		
			if($model->save(false)){ 
				Yii::$app->session->setFlash('success', Yii::t('app', 'Instrument Category successfully updated.'));
				return $this->redirect(['index']);
			}
		}

		return $this->render('update', [
		    'model' => $model,
		]);
	} catch (\yii\base\Exception $exception) {
            throw new \yii\web\HttpException(505, $exception->getMessage());
        }
        
    }

    /**
     * Deletes an existing InstrumentCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete()
    { 
	$id = Yii::$app->getRequest()->post('id');
	$category = InstrumentCategory::categoryDelete($id);
        if($category == ''){ 
            if ((($model = InstrumentCategory::findOne($id)) !== null))
            { 
                $model->delete();
                $return = ['code' => 200, 'message' => 'Instrument Category deleted successfully.'];
            } else {
                $return = ['code' => 404, 'message' => 'The requested page does not exist.'];
            }
        }
        else{ 
            $return = ['code' => 404, 'message' => 'This category already assign to other category.'];
        }
        
	Yii::$app->response->format = 'json';
        return $return;
        
    }

    /**
     * Finds the InstrumentCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return InstrumentCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = InstrumentCategory::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
