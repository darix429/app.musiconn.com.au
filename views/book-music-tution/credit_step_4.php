<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Url;
use app\models\Instrument;
use app\models\TutorialType;
use app\models\StudentTuitionBooking;
use app\models\Tutor;
use app\models\User;
use app\models\OnlineTutorialPackage;
use app\libraries\General;

$this->title = Yii::t('app', 'BOOK CREDITED MUSIC LESSON');
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_icon'] = 'my-lesson-tag.png';

if (!empty($step_data['step_1']['instrument_uuid'])) {
    $instrumentDetail = Instrument::find()->where(['uuid' => $step_data['step_1']['instrument_uuid']])->one();
} else {
    return \yii\web\Controller::redirect(['credit-schedule']);
}

$tutorialTypeDetail = TutorialType::find()->where(['code' => $step_data['step_3']['tutorial_type_code']])->one();
$session_minute = $tutorialTypeDetail['min'];
// $start_hour_array = StudentTuitionBooking::hoursRange();
$start_hour_array = [];

$tutor_uuid = $step_data['step_2']['tutor_uuid'];
$student_uuid = Yii::$app->user->identity->student_uuid;
$tutorDetail = Tutor::findOne($tutor_uuid);
$tutor_working_plan = json_decode($tutorDetail->working_plan, true);
$tutor_user_model = User::find()->where(['tutor_uuid' => $tutor_uuid])->one();
$student_user_model = User::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid])->one();
$tutor_timezone = (!empty($tutor_user_model)) ? $tutor_user_model->timezone : Yii::$app->params['timezone'];
$user_timezone = (!empty($student_user_model)) ? $student_user_model->timezone : Yii::$app->params['timezone'];

$disabled_week_day_in_date = StudentTuitionBooking::get_tutor_disabled_week_day($tutorDetail->working_plan);
$this->registerJs(
        '
$("#studenttuitionbooking-session_when_start").datepicker({startDate:"' . General::displayDate(date('Y-m-d')) . '",daysOfWeekDisabled: "'.$disabled_week_day_in_date.'" });
    
$( "#frm_step_4,#gostep1,#gostep2,#gostep3" ).submit(function( event ) {
        blockBody();
});

$("document").ready(function(){
    $(".btn-secondary").removeAttr("onclick");
    $("#studenttuitionbooking-start_hour").css({"display":"none"});
});

function load_daywise_session(dy){

    if(dy !== undefined){
    $.ajax({
            type: "GET",
            url: "' . Url::to(['book-music-tution/get-daywise-session']) . '",
            data: {"day":dy},
            beforeSend: function() {
                blockBody();
            },
            success:function(result){
                //showSuccess(result.message);
                $("#session_list_test").html(result);
                NEW_CUSTOM.iCheck();
                unblockBody();
            },error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
    }
}

function load_daywise_changes(dy,tm = "00:00",whendate = ""){

    //if(dy !== undefined){
    if(whendate !== ""){
    $.ajax({
            type: "GET",
            url: "' . Url::to(['book-music-tution/credit-get-day-changes']) . '",
            data: {"day":dy,"selected_time":tm,"selected_when_date":whendate},
            beforeSend: function() {
                blockBody();
            },
            success:function(result){
              // console.log(result);

                $("#studenttuitionbooking-session_when_start").datepicker("remove");


                if(result.code == 200){
                    $("#studenttuitionbooking-start_hour").css({"display":"inline"});
                    $("#studenttuitionbooking-start_hour").val(tm);
                    $("#studenttuitionbooking-session_when_start").val(result.session.session_when_start);

                    if(result.message != "success"){
                        showErrorMessage(result.message);
                    }

                }else{
                    $("#studenttuitionbooking-session_when_start").val(result.session_when_start);
                    // showErrorMessage(result.message);
                }
                if(result.all_dates != ""){
                    $(".next_btn").show();
                }else{
                    $(".next_btn").hide();
                }


                $("#session_text_info").html(result.session_text_info);
                $("#studenttuitionbooking-session_text_info").val(result.session_text_info);
                $("#studenttuitionbooking-all_dates").val(result.all_dates);

                $("#studenttuitionbooking-session_when_start").datepicker({startDate:"' . General::displayDate(date('Y-m-d')) . '",daysOfWeekDisabled: result.disabled_day });
                NEW_CUSTOM.iCheck();
                unblockBody();
            },error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
    }
}

function get_day_start_time(dy){
    if(dy !== undefined){
    $.ajax({
            type: "GET",
            url: "' . Url::to(['book-music-tution/credit-get-day-start-time']) . '",
            data: {"day":dy},
            async:false,
            success:function(result){
                $("#studenttuitionbooking-session_when_start").datepicker("remove");
               $("#studenttuitionbooking-start_hour").val(result.time);
               $("#studenttuitionbooking-session_when_start").val(result.session_when_start);
               $("#studenttuitionbooking-session_when_start").datepicker({startDate:result.session_when_start,daysOfWeekDisabled: result.disabled_day,format:"dd-mm-yyyy" });
               NEW_CUSTOM.iCheck();
            },error:function(e){
                showErrorMessage(e.responseText);
            }
        });
    }
}


function get_daywise_time(dy,date,typ){
    if(dy !== undefined){
    // $("#studenttuitionbooking-start_hour").css({"display":"inline"});
    $.ajax({
            type: "GET",
            url: "' . Url::to(['book-music-tution/get-tutor-available-hours']) . '?tutor_uuid=' . $tutor_uuid . '&session_minute=' . $session_minute . '",
            data: {"day":dy,"date":date,"typ":typ,"student_uuid":"'.$student_uuid.'"},
            async:false,
            success:function(result){
                if(result.time == ""){
                    $("#studenttuitionbooking-start_hour").css({"display":"none"});
                }else{
                    $("#studenttuitionbooking-start_hour").css({"display":"block"});
                }
                $("#studenttuitionbooking-session_when_start").datepicker("remove");
                $("#studenttuitionbooking-start_hour").html("");
                $.each(result.time, function (key, value) {

                    $("#studenttuitionbooking-start_hour").append("<option value=\'"+key+"\'>"+value+"</option>");
                });
                $("#studenttuitionbooking-session_when_start").val(result.session_when_start);
                // $("#studenttuitionbooking-session_when_start").datepicker({startDate:result.session_when_start,daysOfWeekDisabled: result.disabled_day,datesOfWeekDisabled: result.disabled_date,format:"dd-mm-yyyy" });
                $("#studenttuitionbooking-session_when_start").datepicker({startDate:date,daysOfWeekDisabled: result.disabled_day,datesOfWeekDisabled: result.disabled_date,format:"dd-mm-yyyy" });
                NEW_CUSTOM.iCheck();
            },error:function(e){
                showErrorMessage(e.responseText);
            }
        });
    }
}

function week_day_change(dy){
    // get_day_start_time(dy);
    var date = $("#studenttuitionbooking-session_when_start").val();
    get_daywise_time(dy,date,"day");
    var tm = $("#studenttuitionbooking-start_hour").val();
    var whendate = $("#studenttuitionbooking-session_when_start").val();
    load_daywise_changes(dy,tm,whendate);
}

function hour_change(){
    var dayvalue = $("input[name=\"StudentTuitionBooking[session_week_day]\"]:checked").val();
    var tm = $("#studenttuitionbooking-start_hour").val();
    var whendate = $("#studenttuitionbooking-session_when_start").val();

    load_daywise_changes(dayvalue,tm,whendate);
}

var send=true;
function date_change(obj){
    var weekday=new Array(7);
    weekday[0]="monday";
    weekday[1]="tuesday";
    weekday[2]="wednesday";
    weekday[3]="thursday";
    weekday[4]="friday";
    weekday[5]="saturday";
    weekday[6]="sunday";

    if(send){

    //var date = $(obj).datepicker("getDate");
    var date = $(obj).val();    
    var curr_date_array = getCurrentUserSelectedTimeInfo(date);

    var w_str = "week_day_" + curr_date_array.week_day;
    $("."+w_str).prop("checked", true);
    $("."+w_str).parent("label").addClass("active");

    var dayvalue = $("input[name=\"StudentTuitionBooking[session_week_day]\"]:checked").val();

    var tm = $("#studenttuitionbooking-start_hour").val();
    var date_new = curr_date_array.date;
    var whendate = $("#studenttuitionbooking-session_when_start").val();

    send=false;
    get_daywise_time(curr_date_array.week_day,date_new,"date");
    var tm = (tm == "" || tm == null) ? $("#studenttuitionbooking-start_hour").val() : tm;
    load_daywise_changes(curr_date_array.week_day,tm,whendate);

    }
    setTimeout(function(){send=true;},200);
}

function convert(str) {
    var date = new Date(str),
        mnth = ("0" + (date.getMonth()+1)).slice(-2),
        day  = ("0" + date.getDate()).slice(-2);
    return [day, mnth, date.getFullYear()].join("-");
}


function getCurrentUserSelectedTimeInfo(sel_date){
    var resDate = "";
    $.ajax({
            type: "GET",
            url: "' . Url::to(['book-music-tution/get-current-user-selected-time-info']) . '",
            data: {"sel_date":sel_date},
            async:false,
            success:function(result){
                resDate = result;
            },error:function(e){
                showErrorMessage(e.responseText);
            }
    });
    
    return resDate;
}
        ', View::POS_END
);
?>

<section class="content">
    <label class="warning-info mar-bottom-10">For issues concerning tutor availability, please contact <a href="mailto:<?= Yii::$app->params['adminEmail']; ?>"><?= Yii::$app->params['adminEmail']; ?></a> for assistance.</label>
    <div class="row mar-bottom-10">
        <div class="col-xs-12">
            <div class="box">
                <div class="col-md-12 box-panel-header ">
                    <h4 class="col-md-6">Select Instrument &nbsp;<i class="fa fa-check"></i></h4>
                    <div class="col-md-6 ">
                        <?php echo Html::beginForm(['credit-schedule'], 'post', ['id' => 'gostep1', 'class' => 'form-inline d-block']) ?>
                        <input type="hidden" name="step" value="1">
                        <input type="hidden" name="prev" value="1">
                        <a href="javascript:$('#gostep1').submit();" class="box-panel-header-right-btn pull-right">
                            Change
                        </a>
                        <p class="box-panel-header-right-p text-white pull-right"> <?= $instrumentDetail->name; ?>&nbsp;</p>
                        <?php echo Html::endForm() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row mar-bottom-10">
        <div class="col-xs-12">
            <div class="box">
                <div class="col-md-12 box-panel-header ">
                    <h4 class="col-md-6">Select Tutor &nbsp;<i class="fa fa-check"></i></h4>
                    <div class="col-md-6 ">
                        <?php echo Html::beginForm(['credit-schedule'], 'post', ['id' => 'gostep2', 'class' => 'form-inline d-block']) ?>
                        <input type="hidden" name="step" value="2">
                        <input type="hidden" name="prev" value="2">
                        <a href="javascript:$('#gostep2').submit();" class="box-panel-header-right-btn pull-right">
                            Change
                        </a>
                        <p class="box-panel-header-right-p text-white pull-right"> <?= $tutorDetail->first_name . ' ' . $tutorDetail->last_name; ?>&nbsp;</p>
                        <?php echo Html::endForm() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row mar-bottom-10">
        <div class="col-xs-12">
            <div class="box">
                <div class="col-md-12 box-panel-header ">
                    <h4 class="col-md-6">Select Tutorial Type &nbsp;<i class="fa fa-check"></i></h4>
                    <div class="col-md-6 ">
                        <?php echo Html::beginForm(['credit-schedule'], 'post', ['id' => 'gostep3', 'class' => 'form-inline d-block']) ?>
                        <input type="hidden" name="step" value="3">
                        <input type="hidden" name="prev" value="3">
                        <a href="javascript:$('#gostep3').submit();" class="box-panel-header-right-btn pull-right">
                            Change
                        </a>
                        <p class="box-panel-header-right-p text-white pull-right"> <?= $tutorialTypeDetail->min . " Minutes"; ?>&nbsp;</p>
                        <?php echo Html::endForm() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    

    <div class="row mar-bottom-10">
        <div class="col-xs-12">
            <div class="box">
                <div class="col-md-12 box-panel-header border-bottom-radious-none">
                    <h4 class="col-md-6">Select Date</h4>
                    <div class="col-md-6 "> 
                        <a class="box-panel-header-right-btn pull-right tutor_availability_calender_btn" data-id="<?= $tutorDetail->uuid; ?>" data-tutorname="<?= $tutorDetail->first_name . ' ' . $tutorDetail->last_name; ?>" title="Avaibility">
                            <i class="fa fa-calendar text-white"></i>
                        </a>
                    </div>
                </div>
                <div class="box-body box-panel-body " style="border-top-left-radius: 0px;border-top-right-radius: 0px;">
                    <div class="row vertical-center-box vertical-center-box-tablet"  style="">
                        <?php
                        $form = ActiveForm::begin([
                                    'action' => ['credit-schedule'],
                                    'id' => 'frm_step_4',
                                    'enableClientValidation' => true,
                                    //'enableAjaxValidation' => true,
                                    'validateOnChange' => true,
                                    'options' => ['class' => 'form-horizontal']]);
                        ?>

                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <div class="row " style="">
                                    <input type="hidden" name="step" value="4">
                                    <?= $form->field($model, 'all_dates')->hiddenInput(['class' => 'form-control', 'maxlength' => true,])->label(false) ?>
                                    <?= $form->field($model, 'session_text_info')->hiddenInput(['class' => 'form-control',])->label(false) ?>
                                </div>
                                <div class="row  " >
                                    <div class="col-md-12 text-center">

                                        <div class="my-inline-button" >
                                            <div class=" text-center" >
                                                <ul class="my-inline-button-filter list-inline btn-group btn-group-toggle " data-toggle="buttons">
                                                    <li>
                                                        <label class="btn <?= ($tutor_working_plan['sunday'] == '') ? "btn-secondary" : "btn-primary" ?> <?= ($model->session_week_day == 'sunday') ? "active" : "" ?>" onclick="week_day_change('sunday')" >
                                                            <input type="radio" name="StudentTuitionBooking[session_week_day]" class="session_week_day week_day_sunday" autocomplete="off" value="sunday" <?= ($model->session_week_day == 'sunday') ? "checked" : "" ?> >Sun
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="btn <?= ($tutor_working_plan['monday'] == '') ? "btn-secondary" : "btn-primary" ?> <?= ($model->session_week_day == 'monday') ? "active" : "" ?>" onclick="week_day_change('monday')" >
                                                            <input type="radio" name="StudentTuitionBooking[session_week_day]" class="session_week_day week_day_monday" autocomplete="off" value="monday" <?= ($model->session_week_day == 'monday') ? "checked" : "" ?> >Mon
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="btn <?= ($tutor_working_plan['tuesday'] == '') ? "btn-secondary" : "btn-primary" ?> <?= ($model->session_week_day == 'tuesday') ? "active" : "" ?>" onclick="week_day_change('tuesday')" >
                                                            <input type="radio" name="StudentTuitionBooking[session_week_day]" class="session_week_day week_day_tuesday" autocomplete="off" value="tuesday" <?= ($model->session_week_day == 'tuesday') ? "checked" : "" ?> >Tue
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="btn <?= ($tutor_working_plan['wednesday'] == '') ? "btn-secondary" : "btn-primary" ?> <?= ($model->session_week_day == 'wednesday') ? "active" : "" ?>" onclick="week_day_change('wednesday')" >
                                                            <input type="radio" name="StudentTuitionBooking[session_week_day]" class="session_week_day week_day_wednesday" autocomplete="off" value="wednesday" <?= ($model->session_week_day == 'wednesday') ? "checked" : "" ?> >Wed
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="btn <?= ($tutor_working_plan['thursday'] == '') ? "btn-secondary" : "btn-primary" ?> <?= ($model->session_week_day == 'thursday') ? "active" : "" ?>" onclick="week_day_change('thursday')" >
                                                            <input type="radio" name="StudentTuitionBooking[session_week_day]"  class="session_week_day week_day_thursday" autocomplete="off" value="thursday" <?= ($model->session_week_day == 'thursday') ? "checked" : "" ?> >Thu
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="btn <?= ($tutor_working_plan['friday'] == '') ? "btn-secondary" : "btn-primary" ?> <?= ($model->session_week_day == 'friday') ? "active" : "" ?>" onclick="week_day_change('friday')" >
                                                            <input type="radio" name="StudentTuitionBooking[session_week_day]" class="session_week_day week_day_friday" autocomplete="off" value="friday" <?= ($model->session_week_day == 'friday') ? "checked" : "" ?> >Fri
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="btn <?= ($tutor_working_plan['saturday'] == '') ? "btn-secondary" : "btn-primary" ?> <?= ($model->session_week_day == 'saturday') ? "active" : "" ?>" onclick="week_day_change('saturday')" >
                                                            <input type="radio" name="StudentTuitionBooking[session_week_day]" class="session_week_day week_day_saturday" autocomplete="off" value="saturday" <?= ($model->session_week_day == 'saturday') ? "checked" : "" ?> >Sat
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-2"></div>
                                            <div class="col-lg-8 col-md-8 col-8 booking_search_data">
                                                <div class="" >

                                                    <div id="session_list_parent" class="text-centre">
                                                        <div class="card " >
                                                            <div class="my-card-body">

                                                                <div class="row ">

                                                                    <div class="col-md-12">
                                                                        <div class="col-md-2">
                                                                            <label class="pull-right"  for="inlineFormInput">When</label>
                                                                        </div>
                                                                        <div class="col-md-5 mar-right-10">
                                                                            <?php $model->session_when_start = General::displayDate($model->session_when_start); ?>
                                                                            <?=
                                                                            $form->field($model, 'session_when_start', ['template' => "{label}\n{input}\n{hint}\n{error}",
                                                                                'labelOptions' => ['class' => 'control-label']])->textInput(
                                                                                    ['class' => 'form-control when-datepicker', 'data-date-format' => 'dd-mm-yyyy', 'data-disabled-days' => '', 'data-start-date' => '0d', 'onchange' => 'date_change(this);', 'readonly' => true])->label(false)
                                                                            ?>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <?=
                                                                            $form->field($model, 'start_hour', ['template' => "{input}\n{error}",
                                                                                'labelOptions' => ['class' => 'control-label1']])->dropDownList(
                                                                                    $start_hour_array, ['class' => 'form-control select2', 'onchange' => 'hour_change();'])->label(false)
                                                                            ?>
                                                                        </div>
                                                                        <div class="col-md-1"></div>
                                                                    </div>


                                                                    <div id="session_text_info" style="width: 100%;text-align: center;"><?= $model->session_text_info; ?></div>

                                                                    <?php $displayStyle = ($model->all_dates != "") ? "display:block;" : "display:none;"; ?>
                                                                    <div class="col-lg-12 col-md-12 col-12 text-center">
                                                                        <?= Html::submitButton('Save Lesson', ['class' => "btn btn-danger btn-md next_btn pull-right", "style" => "$displayStyle"]) ?>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--fdf-->
                                    </div>
                                </div>

                            </div>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a class="btn btn-primary btn-sm black_button_in" href="<?= Yii::$app->getUrlManager()->createUrl('/booked-session/my-lessons'); ?>">RETURN TO MY LESSONS</a>
    
</section>

<style>
    .datepicker td.day.disabled {
        color: #bababa;
    }
</style>