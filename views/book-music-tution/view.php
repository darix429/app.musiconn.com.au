<?php

//echo "<pre>"; print_r($model); exit;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\StudentMonthlySubscription;
use app\models\StudentTuitionBooking;
use app\models\Instrument;
use app\models\Student;
use app\models\Tutor;
use app\models\Order;
use app\models\BookedSession;
use app\models\PremiumStandardPlanRate;

$this->title = Yii::t('app', 'Book Lessons');
$this->params['breadcrumbs'][] = $this->title;

$student = Student::find()->where(['student_uuid' => $model->student_uuid, 'status' => 'ENABLED'])->one();
$tutor = Tutor::find()->where(['uuid' => $model->tutor_uuid, 'status' => 'ENABLED'])->one();
$instrument = Instrument::find()->where(['uuid' => $model->instrument_uuid, 'status' => 'ACTIVE'])->one();
$studentMonthlySubscription = StudentMonthlySubscription::find()->where(['uuid' => $model->student_monthly_subscription_uuid, 'status' => 'ENABLED'])->one();
$order = Order::find()->where(['uuid' => $model->order_uuid])->one();
                $plan_model = PremiumStandardPlanRate::find()->joinWith(['onlineTutorialPackageTypeUu'])->where(['premium_standard_plan_rate.uuid'=>$model->premium_standard_plan_rate_uuid])->asArray()->one();

//Booked Session
$bookedSessionList = BookedSession::find()->where(['booking_uuid' => $model->uuid])->all();
?>


<div class="col-xl-12 col-lg-12 col-12 col-md-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Tuition Details</h2>
            <div class="actions panel_actions float-right">
                <a href="<?= Yii::$app->getUrlManager()->createUrl(['/book-music-tution/history/']) ?>" class="btn btn-info btn-icon "><i class="fa fa-arrow-left text-white"></i> <span>Back</span></a>
            </div>
        </header>
        <div class="content-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12">

                    <?php
                    $form = ActiveForm::begin([
                                'action' => (!$model->isNewRecord) ? Yii::$app->getUrlManager()->createUrl(['/coupon/update/', 'id' => $model->uuid]) : ['create'],
                                'enableClientValidation' => true,
                                //'enableAjaxValidation' => true,
                                'validateOnChange' => true,
                    ]);
                    ?>

                    <div class="form-row">
                        <div class="col-md-4 mb-0">
                            <div class="form-group ">
                                <label class="control-label" >Booking ID</label>
                                <span class="form-control bg-gray-field" aria-required="true" aria-invalid="true"><?= '#' . $model->booking_id; ?></span>
                            </div>
                        </div>
                        <div class="col-md-4 mb-0">
                            <div class="form-group ">
                                <label class="control-label" >Student Name</label>
                                <span class="form-control bg-gray-field" aria-required="true" aria-invalid="true"><?= $student['first_name'] . ' ' . $student['last_name']; ?></span>
                            </div>
                        </div>
                        <div class="col-md-4 mb-0">
                            <div class="form-group ">
                                <label class="control-label" >Tutor Name</label>
                                <span class="form-control bg-gray-field" aria-required="true" aria-invalid="true"><?= $tutor['first_name'] . ' ' . $tutor['last_name']; ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-4 mb-0">
                            <div class="form-group ">
                                <label class="control-label" >Instrument</label>
                                <span class="form-control bg-gray-field" aria-required="true" aria-invalid="true"><?= $instrument['name']; ?></span>
                            </div>
                        </div>
                        <div class="col-md-4 mb-0">
                            <div class="form-group ">
                                <label class="control-label" >Tutorial Name</label>
                                <span class="form-control bg-gray-field" aria-required="true" aria-invalid="true"><?= $model->tutorial_name; ?></span>
                            </div>
                        </div>
                        <?php if(!empty($plan_model['onlineTutorialPackageTypeUu']['display_name'])) { ?>
                        <div class="col-md-4 mb-0">
                            <div class="form-group ">
                                <label class="control-label" >Category</label>
                                <span class="form-control bg-gray-field" aria-required="true" aria-invalid="true"><?= $plan_model['onlineTutorialPackageTypeUu']['display_name']; ?></span>
                            </div>
                        </div>
                        <?php } ?>
                        </div>
                    <div class="form-row">
                        <div class="col-md-4 mb-0">
                            <div class="form-group ">
                                <label class="control-label" >Tutorial Type Code</label>
                                <span class="form-control bg-gray-field" aria-required="true" aria-invalid="true"><?= $model->tutorial_type_code; ?></span>
                            </div>
                        </div>
                    
                        <div class="col-md-4 mb-0">
                            <div class="form-group ">
                                <label class="control-label" >Tutorial Min</label>
                                <span class="form-control bg-gray-field" aria-required="true" aria-invalid="true"><?= $model->tutorial_min; ?></span>
                            </div>
                        </div>
                        <div class="col-md-4 mb-0">
                            <div class="form-group ">
                                <label class="control-label" >Payment Term Code</label>
                                <span class="form-control bg-gray-field" aria-required="true" aria-invalid="true"><?= $model->payment_term_code; ?></span>
                            </div>
                        </div>
                        </div>
                    <div class="form-row">
                        <div class="col-md-4 mb-0">
                            <div class="form-group ">
                                <label class="control-label" >Total Session</label>
                                <span class="form-control bg-gray-field" aria-required="true" aria-invalid="true"><?= $model->total_session; ?></span>
                            </div>
                        </div>
                    
                        <div class="col-md-4 mb-0">
                            <div class="form-group ">
                                <label class="control-label" >Session Length Per Day</label>
                                <span class="form-control bg-gray-field" aria-required="true" aria-invalid="true"><?= $model->session_length_day; ?></span>
                            </div>
                        </div>
                        <div class="col-md-4 mb-0">
                            <div class="form-group ">
                                <label class="control-label" >Price</label>
                                <span class="form-control bg-gray-field" aria-required="true" aria-invalid="true"><?= $model->price; ?></span>
                            </div>
                        </div>
                        </div>
                    <div class="form-row">
                        <div class="col-md-4 mb-0">
                            <div class="form-group ">
                                <label class="control-label" >GST</label>
                                <span class="form-control bg-gray-field" aria-required="true" aria-invalid="true"><?= $model->gst; ?></span>
                            </div>
                        </div>
                    
                        <div class="col-md-4 mb-0">
                            <div class="form-group ">
                                <label class="control-label" >Total Price</label>
                                <span class="form-control bg-gray-field" aria-required="true" aria-invalid="true"><?= $model->total_price; ?></span>
                            </div>
                        </div>
                        <div class="col-md-4 mb-0">
                            <div class="form-group ">
                                <label class="control-label" >Status</label>
                                <span class="form-control bg-gray-field" aria-required="true" aria-invalid="true"><?= $model->status; ?></span>
                            </div>
                        </div>
                        </div>
                    <div class="form-row">
                        <div class="col-md-4 mb-0">
                            <div class="form-group ">
                                <label class="control-label" >Subscription Price</label>
                                <span class="form-control bg-gray-field" aria-required="true" aria-invalid="true"><?= $model->monthly_subscription_price; ?></span>
                            </div>
                        </div>
                    
                        <div class="col-md-4 mb-0">
                            <div class="form-group ">
                                <label class="control-label" >Discount</label>
                                <span class="form-control bg-gray-field" aria-required="true" aria-invalid="true"><?= (!empty($model->discount)) ? $model->discount : '0'; ?></span>
                            </div>
                        </div>
                        <div class="col-md-4 mb-0">
                            <div class="form-group ">
                                <label class="control-label" >Grand Total</label>
                                <span class="form-control bg-gray-field" aria-required="true" aria-invalid="true"><?= $model->grand_total; ?></span>
                            </div>
                        </div>
                        </div>
                    <div class="form-row">
                        <div class="col-md-4 mb-0">
                            <div class="form-group ">
                                <label class="control-label" >Booking Date</label>
                                <span class="form-control bg-gray-field" aria-required="true" aria-invalid="true"><?= \app\libraries\General::displayDate($model->booking_datetime); ?></span>
                            </div>
                        </div>
                    
                        <?php if(!empty($studentMonthlySubscription['name'])){ ?>
                        <div class="col-md-4 mb-0">
                            <div class="form-group ">
                                <label class="control-label" >Subscription Name</label>
                                <span class="form-control bg-gray-field" aria-required="true" aria-invalid="true"><?= $studentMonthlySubscription['name']; ?></span>
                            </div>
                        </div>
                        <?php }
                        if(!empty($order['order_id'])){ ?>
                        <div class="col-md-4 mb-0">
                            <div class="form-group ">
                                <label class="control-label" >Order ID</label>
                                <span class="form-control bg-gray-field" aria-required="true" aria-invalid="true"><?= "#" . $order['order_id']; ?></span>
                            </div>
                        </div>
                        <?php } ?>
                    </div>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>

        </div>
    </section>
</div>

<div class="col-xl-12 col-lg-12 col-12 col-md-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Session Details</h2>
        </header>
        <div class="content-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">

                    <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Student</th>
                                <th>Tutor</th>
                                <th>Session Pin</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Status</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php
                            if (!empty($bookedSessionList)) {
                                foreach ($bookedSessionList as $key => $value) {
                                    $student_name = $value['studentUu']['first_name'] . ' ' . $value['studentUu']['last_name'];
                                    $tutor_name = $value['tutorUu']['first_name'] . ' ' . $value['tutorUu']['last_name'];
                                    ?>
                                    <tr>
                                        <td><?= $key + 1; ?></td>
                                        <td><?= (!empty($student_name)) ? $student_name : ''; ?></td>
                                        <td><?= (!empty($tutor_name)) ? $tutor_name : ''; ?></td>
                                        <td><?= $value['session_pin']; ?></td>
                                        <td><?= \app\libraries\General::displayDate($value['start_datetime']); ?></td>
                                        <td><?= \app\libraries\General::displayTime($value['start_datetime']) ." - ". \app\libraries\General::displayTime($value['end_datetime']) ?></td>
                                        <td>
                                            <?php
                                            if ($value['status'] == 'SCHEDULE') {
                                                $e_class = "badge-info";
                                                //$e_link_class = "coupon_disabled_link";
                                            } elseif ($value['status'] == 'PUBLISHED') {
                                                $e_class = "badge-success";
                                                //$e_link_class = "coupon_enabled_link";
                                            } elseif ($value['status'] == 'EXPIRED') {
                                                $e_class = "badge-secondary";
                                                //$e_link_class = "coupon_enabled_link";
                                            } elseif ($value['status'] == 'INPROCESS') {
                                                $e_class = "badge-warning";
                                                //$e_link_class = "coupon_enabled_link";
                                            } else {
                                                $e_class = "badge-danger";
                                                //$e_link_class = "";
                                            }
                                            ?>
                                            <span class="badge badge-pill <?= $e_class ?> "  ><?= $value['status']; ?></span>
                                        </td>

                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </section>
</div>

<div class="col-xl-12 col-lg-12 col-12 col-md-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Credited Session Details</h2>
        </header>
        <div class="content-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">

                    <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>No.</th>                                
                                <th>Cancelled Session's Schedule Date</th>
                                <th>Time</th>
                                <th>Cancel Date</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php
                            $creditedSessionList = \app\models\CreditSession::find()->joinWith(['bookedSessionUu'])->where(['credit_session.booking_uuid' => $model->uuid, 'credit_session.status' => 'PENDING'])->all();
                            if (!empty($creditedSessionList)) {
                                foreach ($creditedSessionList as $key => $value) {
                                    
                                    ?>
                                    <tr>
                                        <td><?= $key + 1; ?></td>
                                        
                                        <td><?= \app\libraries\General::displayDate($value['bookedSessionUu']['start_datetime']); ?></td>
                                        <td><?= \app\libraries\General::displayTime($value['bookedSessionUu']['start_datetime']) ." - ". \app\libraries\General::displayTime($value['bookedSessionUu']['end_datetime']) ?></td>
                                        <td><?= \app\libraries\General::displayDate($value['created_at']); ?></td>

                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </section>
</div>







