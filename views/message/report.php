<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Messaging Report');
$this->params['breadcrumbs'][] = $this->title;

?>


<div class="col-xl-12">
    <section class="box ">

        <header class="panel_header">
        <h2 class="title float-left">Messaging Report</h2>
        </header>

        <div class="content-body"> 
            <div class="row" id="coupon_table_parent_div">
                <div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">

                    <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>DateTime</th>
                                <th>Sender</th>
                                <th>Recepient</th>
                                <th>Message</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            foreach($collection as $chat) {
                                $messages = $chat["messages"];
                                $tutor = $chat->tutor;
                                $student = $chat->student;
                                $tutor_name = $tutor->first_name . " " . $tutor->last_name;
                                $student_name = $student->first_name . " " . $student->last_name;
                                foreach ($messages as $message) { ?>
                                <tr>
                                    <td><?= date("Y-m-d g:i:s A", ($message["timestamp"])) ?></td>
                                    <td><?= $message["from"]==$tutor->uuid? $tutor_name:$student_name ?></td>
                                    <td><?= $message["to"]==$tutor->uuid? $tutor_name:$student_name ?></td>
                                    <td><?= $message["content"] ?></td>
                                </tr>
                            <?php }} ?>                        
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </section>
</div>