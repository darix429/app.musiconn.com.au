<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\web\View;
use yii\helpers\ArrayHelper;
use app\models\MusicoinPackage;
use app\models\StudentCreditCard;
use app\libraries\General;

$planList = MusicoinPackage::find()->where(['status' => 'ENABLED'])->all();
$saved_cc_detail_all = StudentCreditCard::find()->where(['student_uuid' => $model->student_uuid, 'status' => 'ENABLED'])->all();
$saved_cc_detail = (isset($saved_cc_detail_all[0])) ? $saved_cc_detail_all[0] : [];
$saved_cc_array = \yii\helpers\ArrayHelper::map($saved_cc_detail_all, 'uuid', function($e) {
            $number = General::decrypt_str($e['number']);
            return General::ccMasking($number);
        });
$saved_cc_array['other'] = 'Other Card';




$this->registerJs(
        ' 
//set your publishable key
Stripe.setPublishableKey("' . Yii::$app->params['stripe']['publishable_key'] . '");

buycoin_cart_refresh();

$("input[name=\"StudentMusicoinPackage[package_ckeck]\"]").on("change", function (event) {
        $("#studentmusicoinpackage-package_uuid").val(this.value);
        buycoin_cart_refresh();
    });
    
function changeSavedCCOption(obj){

    if($(obj).val() === "other"){
        $(".other_cc_field_div").show();
    }else{
        $(".other_cc_field_div").hide();
    }
    
    if($(obj).val() == "other"){
        $(".saved_cc_inputs").hide();
    }else if($(obj).val() != "" && $(obj).val() != "other"){
        $(".saved_cc_inputs").show();
    }else{
        $(".saved_cc_inputs").hide();
    }
    
}

//callback to handle the response from stripe
function buycoin_stripeResponseHandler(status, response) {

    if (response.error) {
        $("#buycoin_payBtn").removeAttr("disabled");
        $(".payment-errors").html(response.error.message);
        unblockBody();
        showSuccess(response.error.message);
    } else {
        var form$ = $("#frm_buycoin");
        var token = response.id;

        form$.append("<input type=\"hidden\" name=\"stripeToken\" value=\""+token+"\" />");
        blockBody("Submiting...");

        save_buy_musicoin();
        blockBody("Please waiting...");
    }
}

function buycoin_beforeValidation(){

    $("#frm_buycoin" ).find("input").each(
        function(index) {
            idnya = $(this).attr("id");
            $("#frm_buycoin").yiiActiveForm("validateAttribute", idnya);
            err = $(this).parent().has("has-error");
            msg = $(this).parent().find(".help-block").html();
           // alert(idnya+"=="+msg);
        }
    );
}

function buycoin_submitform()
{
    buycoin_cart_refresh(false);
    buycoin_beforeValidation();

    $("#buycoin_payBtn").attr("disabled", "disabled");
    
    if( $("input[name=\"StudentMusicoinPackage[saved_cc_ckeck]\"]:checked").val() != "other" ){
    
        var dateText = $("#studentmusicoinpackage-s_cc_month_year").val();
        var pieces = dateText.split("-");
        var year = (pieces[0] != undefined && pieces[0] > 0) ? pieces[0] : "1970";
        var month = (pieces[1] != undefined && pieces[1] > 0) ? pieces[1] : "00";
        
        var token = Stripe.createToken({
            number: $("#studentmusicoinpackage-s_cc_number").val(),
            cvc: $("#studentmusicoinpackage-s_cc_cvv").val(),
            exp_month: month,
            exp_year: year,
        }, buycoin_stripeResponseHandler);
    } else {

        var dateText = $("#studentmusicoinpackage-cc_month_year").val();
        var pieces = dateText.split("-");
        var year = (pieces[0] != undefined && pieces[0] > 0) ? pieces[0] : "1970";
        var month = (pieces[1] != undefined && pieces[1] > 0) ? pieces[1] : "00";
        
        var token = Stripe.createToken({
            number: $("#studentmusicoinpackage-cc_number").val(),
            cvc: $("#studentmusicoinpackage-cc_cvv").val(),
            exp_month: month,
            exp_year: year,
        }, buycoin_stripeResponseHandler);
    }
    
}

$(".custom_expire_month_year").datepicker( {
        format: "yyyy-mm",
        viewMode: "months", 
        minViewMode: "months",
        startDate:new Date(),
        startView:2
    });
');
?>
<style>
    .help-block{color: #a94442;}
</style>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel"><img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new_book/img/instrument/Musicoins.png' ?>"> MUSICOINS</h4>
</div>
<div class="modal-body" style="display: unset">
    <div class="col-md-12">
        <p>My Musicoins <span class="student_musicoin_balance_text"><?= \app\libraries\General::getStudentMusicoin(Yii::$app->user->identity->student_uuid); ?></span></p>
        <h3>ADD MUSICOINS</h3>
        <p><strong>Select a Musicoin package below (<?= Yii::$app->params['musicoin']['coin_cent_value']; ?>c per coin package)</strong></p>
    </div>

    <?php
//    echo "<pre>";
//    print_r($model->getErrors());
//    echo "</pre>";
//    exit;
    $form = ActiveForm::begin([
                'action' => ['index'],
                'id' => 'frm_buycoin',
                'enableClientValidation' => true,
                'validateOnChange' => true,
                'options' => ['class' => 'form-horizontal111']]);
    ?>

    <div class="col-md-8">
        <ul class="products-list product-list-in-box">
            <?php
            $plan_array = \yii\helpers\ArrayHelper::map($planList, 'uuid', function($e) {
                        $bonus_text = ($e->bonus_coins > 0) ? ' (Including ' . $e->bonus_coins . ' Bonus Musicoins) ' : ' ';
                        return '<strong>' . $e->name . '-</strong> ' . $e->total_coins . ' Musicoins' . $bonus_text . '($' . $e->total_price . ')';
                    });
            echo $form->field($model, 'package_ckeck', ['template' => '{input}', 'options' => ['tag' => false]])
                    ->radioList(
                            $plan_array, [
                        'item' => function($index, $label, $name, $checked, $value) {

                            $html = '<li class="st-p3">';
                            $html .= '<label class="contai_r">';
                            $html .= '<input type="radio" value="' . $value . '" name="' . $name . '">';
                            $html .= '<span class="checkmark"></span>';
                            $html .= '</label>';
//                                        $html .= '<span class="mn_3"><strong>Package1-</strong>50 Musicoins($25)</span>';
                            //$html .= '<span class="mn_3">' . $label . '</span>';
                            $html .= '<div class="mn_3" style="margin-left:35px;">' . $label . '</div>';
                            $html .= '</li>';
                            return $html;
                        }
                            ]
                    )
                    ->label(false);
            ?>
        </ul>
        <?= $form->field($model, 'package_uuid')->hiddenInput(['class' => 'form-control', 'maxlength' => true,])->label(false) ?>
    </div>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!--<br>-->
                <div class="" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">PAYMENT DETAILS</h4>
                        </div>
                        <div id="" class="">
                            <div class="panel-body">

                                <div class="col-md-12">
                                    <ul class="products-list product-list-in-box">

                                        <?php
                                        if (empty($saved_cc_detail)) {
                                            $model->saved_cc_ckeck = 'other';
                                            $display_radio = "display:none;";
                                        } else {
                                            $model->saved_cc_ckeck = $model->saved_cc_ckeck;
                                            $display_radio = "display:block;";
                                        }
                                        echo $form->field($model, 'saved_cc_ckeck', ['template' => '{input}{error}', 'options' => ['tag' => false]])
                                                ->radioList(
                                                        $saved_cc_array, [
                                                    'item' => function($index, $label, $name, $checked, $value) {

                                                        $rd_label = ($value != "other") ? '<span class="mn_3 mn_33"><strong>Saved Credit Card</strong><br>' . $label . '</span>' : '<span class="mn_3 mn_33"><strong>' . $label . '</strong></span>';
                                                        $chek_ht = ($checked) ? 'checked="checked"' : '';
                                                        $html = '<li class="st-p3">';
                                                        $html .= '<label class="contai_r">';
                                                        $html .= '<input type="radio" value="' . $value . '" name="' . $name . '" ' . $chek_ht . ' onChange="changeSavedCCOption(this);">';
                                                        $html .= '<span class="checkmark"></span>';
                                                        $html .= '</label>';
                                                        //$html .= '<span class="mn_3 mn_33"><strong>Saved Credit Card</strong><br>' . $label . '</span>';
                                                        $html .= $rd_label;
                                                        $html .= '</li>';
                                                        return $html;
                                                    }, 'style' => $display_radio]
                                                )
                                                ->label(false);
                                        ?>
                                    </ul>
                                    <?php
                                    if (!empty($saved_cc_detail)) {
                                        ?>
                                        <div class="hidden_inputs saved_cc_hidden_inputs">
                                            <?= $form->field($model, 's_cc_uuid', ['template' => '{input}{error}'])->hiddenInput(['value' => $saved_cc_detail['uuid']])->label(false); ?>
                                            <?= $form->field($model, 's_cc_name', ['template' => '{input}{error}'])->hiddenInput(['value' => $saved_cc_detail['name']])->label(false); ?>
                                            <?= $form->field($model, 's_cc_number', ['template' => '{input}{error}'])->hiddenInput(['value' => General::decrypt_str($saved_cc_detail['number'])])->label(false); ?>
                                            <?php
                                            $exp_date = date('Y-m', strtotime($saved_cc_detail['expire_date']));
                                            echo $form->field($model, 's_cc_month_year', ['template' => '{input}{error}'])->hiddenInput(['value' => $exp_date])->label(false);
                                            ?>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="col-md-6 saved_cc_inputs" style="display: <?= (!empty($model->saved_cc_ckeck) && $model->saved_cc_ckeck != "other") ? "block" : "none"; ?>">
                                    <div class="col-md-12" style="padding-left: 0;">
                                        <h3>SAVED CARD DETAILS</h3>
                                        <?=
                                                $form->field($model, 's_cc_cvv', ['template' => '<span>{label}</span>{input}{error}', 'inputOptions' => ['autocomplete' => 'new-password']])
                                                ->passwordInput(['class' => 'form-control my_input_border ', 'maxlength' => 5,])
                                                ->label($model->getAttributeLabel('cc_cvv') . '*')
                                        ?>

                                        <?php
                                        echo $form->field($model, 'term_condition_checkbox_1', ['template' => ' {input} I agree to Musiconns <a data-toggle="modal" data-target="#terms_condition_model"><b>Terms and Conditions</b></a>{error} '])->checkbox([
                                            'class' => 'skin-square-red float-left '], false);
                                        ?>
                                    </div>
                                </div>

                                <div class="col-md-6 other_cc_field_div" style="<?= ($model->saved_cc_ckeck == "other") ? "display:block;" : "display:none;"; ?>">
                                    <h3>CREDIT CARD DETAILS</h3>
                                    <p>These credit card details will be used for subscription and lesson payments</p>

                                    <?=
                                            $form->field($model, 'cc_name', ['template' => '<strong>{label}</strong>{input}{error}'])
                                            ->textInput(['class' => 'form-control demo', 'maxlength' => true,])
                                            ->label($model->getAttributeLabel('cc_name') . '*')
                                    ?>

                                    <?=
                                            $form->field($model, 'cc_number', ['template' => '<span>{label}</span>{input}{error}', 'inputOptions' => ['autocomplete' => 'new-password']])
                                            ->textInput(['class' => 'credit-card-number form-control', 'maxlength' => true,])
                                            ->label($model->getAttributeLabel('cc_number') . '*')
                                    ?>
                                    <div class="col-md-6" style="padding-left: 0;">
                                        <?=
                                                $form->field($model, 'cc_cvv', ['template' => '<span>{label}</span>{input}{error}', 'inputOptions' => ['autocomplete' => 'new-password']])
                                                ->passwordInput(['class' => 'form-control my_input_border ', 'maxlength' => 5,])
                                                ->label($model->getAttributeLabel('cc_cvv') . '*')
                                        ?>
                                    </div>
                                    <div class="col-md-6" style="padding-right: 0;">
                                        <?=
                                                $form->field($model, 'cc_month_year', ['template' => '<span>{label}</span>{input}{error}'])
                                                ->textInput(['class' => 'form-control custom_expire_month_year ', 'data-min-view-mode' => 'months', 'data-start-view' => '2', 'data-format' => 'yyyy-mm', 'maxlength' => true,])
                                                ->label($model->getAttributeLabel('cc_month_year') . '*')
                                        ?>
                                    </div>


                                    <?php
                                    echo $form->field($model, 'cc_checkbox')->checkbox([
                                        'template' => '<p class="save_future pull-left"><label class="icheck-label form-label" for="save"> {input}  </label> <p>',
                                        'class' => 'skin-square-red float-left save_future'])->label(false);
                                    ?>

                                    <?php
                                    echo $form->field($model, 'term_condition_checkbox', ['template' => ' {input} I agree to Musiconns <a data-toggle="modal" data-target="#terms_condition_model"><b>Terms and Conditions</b></a>{error} '])->checkbox([
                                        'class' => 'skin-square-red float-left '], false);
                                    ?>
                                </div>
                                <div class="col-md-4">
                                    <br>
                                    <div class="sst_btn sumr_y" style="min-width: auto; padding:10px;width: auto;">
                                        <h3 style="margin-top: 2px;">SUMMARY</h3>
                                        <div><strong>Purchase Price:</strong> <span class="pull-right">$<span id="plan_price_text">0</span></span></div>
                                        <div><strong>Purchase GST:</strong> <span class="pull-right">$<span id="plan_gst_text">0</span></span></div>
                                        <div><strong>Purchase Total:</strong> <span class="pull-right">$<span id="plan_total_price_text">0</span></span></div>
                                        <div><strong>Discount:</strong> <span class="pull-right">$<span id="discount_text">0</span></span></div>
                                        <div><strong>Amount Payable:</strong> <span class="pull-right">$<span id="cart_total_text">0</span></span></div>
                                    </div>
                                </div>


                                <div class="col-md-12"><hr style="width:100%;display: inline-block;margin: 12px 0;"></div>
                                <br><br><br>

                                <div class="col-md-12">
                                    <h3>GOVERNMENT VOUCHERS</h3>
                                    <p>Many States and Territories offer discount and voucher programs designed to support kids getting into creative arts such as music.</p>
                                    <p style="border: 1px #0c9dca solid; border-left: 3px #0c9dca solid; padding: 5px;background-color: #c1e6fd;color: #066684;">
                                        <strong>Info:</strong> Please enter the participant's name, voucher code, date of birth, and postcode exactly as shown on the voucher.
                                    </p>
                                    <p><div><a class="btn_lnk" href="#">Find out more about your state.</a></div></p>
                                    <div class="col-md-7" style="padding: 0;">
                                        <div class="row" style="margin-bottom: 7px;">
                                            <div class="col-md-12" style="padding-left: 0; ">


                                                <div class="col-md-6" style="padding-left1: 0;">
                                                    <?=
                                                            $form->field($model, 'kids_name', ['inputOptions' => ['autocomplete' => 'off']
                                                            ])
                                                            ->textInput(['class' => 'form-control ', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('kids_name'), ])
                                                    ?>
                                                </div>
                                                <div class="col-md-6" style="padding-right1: 0;">
                                                    <?=
                                                            $form->field($model, 'creative_kids_voucher_code', ['inputOptions' => ['autocomplete' => 'off']])
                                                            ->textInput(['class' => 'form-control my_input_border pull-left', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('creative_kids_voucher_code')])
                                                    ?>
                                                    <?php //'title' => 'The name to be entered is the participants name and needs to be exactly as it appears on legal documents' ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12" style="padding-left: 0;">
                                                <div class="col-md-6" style="padding-left1: 0;">
                                                    <?=
                                                    $form->field($model, 'kids_postal_code')->textInput(['class' => 'form-control ', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('kids_postal_code')]);
                                                    ?>
                                                </div>
                                                <div class="col-md-6" style="padding-right1: 0;">
                                                    <?=
                                                    $form->field($model, 'kids_dob')->textInput(['readonly' => true, 'class' => 'form-control custom_date_picker', 'maxlength' => true, 'placeholder' => 'DD-MM-YYYY']);
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12"><hr style="width:100%;display: inline-block;margin: 12px 0;"></div><br><br>
                                <div class="col-md-12">
                                    <div class="col-md-3" style="padding-left: 0;">
                                        <?=
                                                $form->field($model, 'coupon_code', ['template' => '{label}{input}', 'inputOptions' => ['autocomplete' => 'off']
                                                ])
                                                ->textInput(['class' => 'form-control ', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('coupon_code')])
                                        ?>
                                    </div>
                                    <div class="col-md-6" style="padding-right: 0;">
                                        <div class="form-group">
                                            <strong class="ds_nOn">Card Number*</strong><br>
                                            <button type="button" class="btn btn-primary" onclick="buycoin_cart_refresh();">APPLY</button>
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="padding: 0;"><p class="promo-code-msg "><?= (!empty($model->getErrors()['coupon_code'][0])) ? $model->getErrors()['coupon_code'][0] : "" ?></p></div>
                                </div>

                                <div class="col-md-12 payment-errors text-danger"><?= (isset($error)) ? $error : ''; ?></div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="hidden_inputs ">
        <?= $form->field($model, 'discount', ['template' => '{input}{error}'])->hiddenInput()->label(false); ?>
        <?= $form->field($model, 'cart_total', ['template' => '{input}{error}'])->hiddenInput()->label(false); ?>
        <?= $form->field($model, 'coupon_uuid', ['template' => '{input}{error}'])->hiddenInput()->label(false); ?>
        <?= $form->field($model, 'promo_coins', ['template' => '{input}{error}'])->hiddenInput()->label(false); ?>
        <?= $form->field($model, 'cart_total_coins', ['template' => '{input}{error}'])->hiddenInput()->label(false); ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">CLOSE</button>
    <!--<button type="button" class="btn btn-primary" onclick="save_buy_musicoin();">Save changes</button>-->
    <button type="button" class="btn btn-primary" onclick="buycoin_submitform();">CONFIRM PAYMENT</button>
</div>
