<?php

use yii\helpers\Html;
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['name'] ?>,</p>
            
            <p>Musiconn Admin has just created an account for you in the booking system.  You will need to first reset your password before you can login to the system.</p>
            
            <p>Please follow <a href="<?= $content['login_url']; ?>">this link</a> to the login page, and enter the following details:</p>
            
            <p><strong>Username:</strong> <?= $content['username']; ?></p>
            <p><strong>Password:</strong> <?= $content['password']; ?></p>        
                      
            <p>Please do not reply to this unattended mailbox.  Any queries or feedback can be directed to admin@musiconn.com.au.</p>

            <p>Regards,<br>
            Admin Team – Musiconn Pty Ltd</p>
        </td>
    </tr>
</table>

