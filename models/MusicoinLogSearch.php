<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MusicoinLog;

/**
 * MusicoinLogSearch represents the model behind the search form of `app\models\MusicoinLog`.
 */
class MusicoinLogSearch extends MusicoinLog
{
    /**
     * {@inheritdoc}
     */
    public $studentName;
    
    public function rules()
    {
        return [
            [['uuid', 'student_uuid', 'summary', 'referenceno', 'created_at', 'studentName'], 'safe'],
            [['debit', 'credit', 'balance'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $modeDatatable = false, $statusnot = '') {
        
        $query = MusicoinLog::find()->joinWith(['studentUu',]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        if (!empty($this->studentName)) {
            $query->andFilterWhere(['ilike', "concat(student.first_name,'',student.last_name)", $this->studentName]);
        }
        
        if (!is_null($this->created_at) && 
            strpos($this->created_at, ' - ') !== false ) {
            list($start_date, $end_date) = explode(' - ', $this->created_at);
            

            $query->andFilterWhere(['>=', '"musicoin_log"."created_at"', date("Y-m-d", strtotime($start_date))." 00:00:00"]);
            $query->andFilterWhere(['<=', '"musicoin_log"."created_at"', date("Y-m-d", strtotime($end_date))." 23:59:59"]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'debit' => $this->debit,
            'credit' => $this->credit,
            'balance' => $this->balance,
            'uuid' => $this->uuid,
            '"musicoin_log"."student_uuid"' => $this->student_uuid,
        ]);

        $query->andFilterWhere(['ilike', 'summary', $this->summary])
            ->andFilterWhere(['ilike', 'referenceno', $this->referenceno]);
        
        if ($modeDatatable) {
            $result = $query->asArray()->orderBy('created_at DESC')->all();
            //echo "<pre>"; print_r($result); 
            return $result;
        }
        return $dataProvider;
    }
}
