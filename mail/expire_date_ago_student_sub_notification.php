<?php
 
use yii\helpers\Html;
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['student_name']; ?>,</p>

            <p>Our records show that your monthly subscription to access our range of quality online contents is due to expire on <?= $content['end_datetime']; ?>.</p>

	    <p>To renew your subscription, simply login to our booking system, and from your profile page, select “MANAGE SUBSCRIPTION” and proceed to payment.</p>
	    
            <p>Please do not reply to this unattended mailbox.  Any queries or feedback can be directed to admin@musiconn.com.au, or call us on 1300 068 742.</p>

            <p>Regards,<br>
            Musiconn Booking System</p>
        </td>
    </tr>
</table>

