<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\ArrayHelper;
use app\models\InstrumentCategory;
use app\models\Instrument;
use app\models\User;
use app\models\StudentViewRecordedLesson;
use app\libraries\General;
use yii\helpers\Url;

if ($model->status == 'COMPLETED') {
    //$this->title = Yii::t('app', 'Session Video : '.$model->studentUu->first_name.' '.$model->studentUu->last_name);
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Uploaded Videos'), 'url' => (isset($back_url)) ? $back_url : ['index']];
    $this->params['breadcrumbs'][] = '';
} else {
    //$this->title = Yii::t('app', 'Tuition Video : '.$model->studentUu->first_name.' '.$model->studentUu->last_name );
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tuition Videos'), 'url' => (isset($back_url)) ? $back_url : ['index']];
    $this->params['breadcrumbs'][] = '';
}
$this->title = Yii::t('app', 'VIDEO LIBRARY');
$this->params['page_title_custom'] = '<div class="col-md-7">
                        <h1>
                            <img src="' . Yii::$app->request->baseUrl . '/theme_assets/new/img/video-icon.png' . '">VIDEO LIBRARY 
                        </h1>
                    </div>
                    <div class="col-md-5">
                        <a href="' . Url::to(['/booked-session/completed']) . '" class="btn btn-primary btn-sm black_button_in" >MY RECORDED LESSONS</a>
                        <a href="' . Url::to(['/video/student-video-list/']) . '" class="btn btn-primary btn-sm black_button_in" >VIDEO LIBRARY</a>
                    </div>';

$instrumentArray = ArrayHelper::map(Instrument::find()->where(['status' => 'ACTIVE'])->asArray()->all(), 'uuid', 'name');

$videos = General::getLessonVideoListUsingUuid($model['uuid']);
$count = count($videos);


$p = isset($_GET['p']) ? $_GET['p'] : 1;
$videos = General::getLessonVideoListUsingUuid($model['uuid']);
$filename = (!empty($videos)) ? $videos[$p] : $model['uuid'] . ".mp4";
$file_path = Yii::$app->params['media']['session_recorded_video']['path'] . $model['uuid'] . '/' . $filename;
$file_url = Yii::$app->params['media']['session_recorded_video']['url'] . $model['uuid'] . '/' . $filename;
//$file_url = 'http://127.0.0.1:9001/tution_video/' . $model['uuid'] . '/'. $filename;
$view_count = StudentViewRecordedLesson::find()->where(['booked_session_uuid'=> $model['uuid'],'part'=>$p])->count();

$poster = (is_file($file_path)) ? Yii::$app->params['media']['session_recorded_video']['thumbnail']['url'] . $model['uuid'] . ".jpg" : Yii::$app->params['theme_assets']['url'] . "images/novideo.png";



$this->registerJs(
        '
            
var videocount = true;
var vid = document.getElementById("myVideo");
vid.onplay= function() {

      
 var seccount = setInterval(function(){ 
    //code goes here that will be run every 5 seconds. 
    var current_seconds = vid.currentTime;
    if(current_seconds >= 30 && videocount==true)
    {
    student_view_count(current_seconds);
    videocount = false;
    clearInterval(seccount);

    } 
}, 2000);

};

function student_view_count(video_seconds)
{

$.ajax({
                        type: "GET",
                        url: "' . Url::to(['/video/student_view_session_count/', 'id' => $model->uuid, 'part' => $p]) . '",
                       // data: { id: id,part:p},
                        success:function(result){
                          
                        },
                        error:function(e){
                            showErrorMessage(e.responseText);
                        }
                    });
 
}

$(document).on("change","#bookedsession-status", function(){
    if($(this).val() == "REJECTED"){
        $(".rejected-reason-parent").show();
    } else {
        $(".rejected-reason-parent").hide();
    }

    if($(this).val() == "PUBLISHED"){
        $(".expire_date_parent").show();
    } else {
        $(".expire_date_parent").hide();
    }
});


$(document).on("click","#savebutton",function(){

    var file_data = $("#bookedsession-update_file").prop("files")[0];
    var form_data = new FormData($("form")[2]);
    form_data.append("update_file", file_data);
    $(".msg").text("Uploading in progress...");
    $.ajax({
            type: "POST",
            url: $( "#upload_form" ).attr("action"),
            data:  form_data,
            processData: false,
            contentType: false,
            beforeSubmit: function() {
                $("#progress-bar").width("0%");
              },
            xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        percentComplete = parseInt(percentComplete * 100);

                        $("#progress-bar").width(percentComplete + "%");
			$("#progress-status").html(percentComplete + "%");
                    }
                }, false);
                return xhr;
            },
            success: function (result) {

                if(result.code == 200){
                    $(".msg").text(result.message);

                    if($("#progress-status").html() == "100%"){

                        $("#progress-bar").width("0%");
                                $( ".form-open" ).hide();
                                $(".msg").hide();

                                //start rupal
                                //document.getElementById("myVideo").innerHTML = result.files.path;
//                                document.getElementById("title").innerHTML = result.data.title;
//                                document.getElementById("created_at").innerHTML = result.data.uploaded_date;
//                                document.getElementById("instrument").innerHTML = result.data.instrument;
//                                document.getElementById("instrumentCategory").innerHTML = result.data.instrument_category;

                                //$(".video-details").load("' . Url::to(['/video/review?id=db819285-d7fa-4246-a9a8-fbe33dd077a2']) . '", +  " .video-details");
                               //end
                                window.location.reload();
                                showSuccess(result.message);

//                        setTimeout(function(){
//                            location.href = "' . Url::to(['video/recorded']) . '";
//                         }, 1000);
                    }
                } else {
                    $("#uploadFormDiv").html(result);
                    $("#progress-bar").width("0%");
                }
            }, error:function(e){
                $("#progress-bar").width("0%");
                showErrorMessage(e.responseText);
            }
        });

});

//published-video
$(document).on("click",".published-enabled", function(){
        if(confirm("Are you sure, you want to publish this video?")){
        var id = $(this).attr("data-id");

            $.ajax({
                type: "GET",
                url: "' . Url::to(['/video/session-publish/']) . '",
                data: { "id": id},
                success:function(result){
                    if(result.code == 200){
                        window.location.reload();
                        showSuccess(result.message);
                    }else{
                        showErrorMessage(result.message);
                    }
                },
                error:function(e){
                    showErrorMessage(e.responseText);
                }
            });
        }
    });

    $(document).on("click",".edit",function(){
        //$( document ).ready( function() {
           // $( "#edit" ).click( function() {
                $( ".form-open" ).show();
                $("html,body").animate({
                    scrollTop: $(".form-open").offset().top},
                    "slow");

           // });

        });

        //is-free
        $(document).on("click",".isfree_disabled_link", function(e){
                if(confirm("Are you sure, you want to is free disable this video?")){
                var element = $(this);

                    $.ajax({
                        type: "GET",
                        url: "' . Url::to(['/video/is-free-disabled/']) . '",
                        data: { "id": $(this).attr("data-id")},
                        success:function(result){
                            if(result.code == 200){
                                $(element).removeClass("isfree_disabled_link");
                                $(element).addClass("isfree_enabled_link");

                                showSuccess(result.message);
                            }else{
                                showErrorMessage(result.message);
                            }
                        },
                        error:function(e){
                            showErrorMessage(e.responseText);
                        }
                    });
                }
                else{
                    e.preventDefault();
                }
            });

            //not_free
            $(document).on("click",".isfree_enabled_link", function(e){
                if(confirm("Are you sure, you want to is free enable this  video?")){
                var element = $(this);

                    $.ajax({
                        type: "GET",
                        url: "' . Url::to(['/video/is-free-enabled/']) . '",
                        data: { "id": $(this).attr("data-id")},
                        success:function(result){
                            if(result.code == 200){
                                $(element).removeClass("isfree_enabled_link");
                                $(element).addClass("isfree_disabled_link");

                                showSuccess(result.message);
                            }else{
                                showErrorMessage(result.message);
                            }
                        },
                        error:function(e){
                            showErrorMessage(e.responseText);
                        }
                    });
                }else{
                e.preventDefault();
                }
            });

	 $(".video-list").click(function(){
	  $("video").trigger("pause");
	  var id = $(this).attr("data-id");
	  var p = $(this).attr("data-p");
	  var url = web_url + "video/tution-view?id="+id+"&p="+p;
	  location.href = url;
        });

        function endVideo(id, p) {
          var count = ' . $count . ';
          if(p < count) {
            p++;
          } else {
            p = 1;
          }
          var dataurl = web_url + "video/tution-view?id="+id+"&p="+p;
          location.href = dataurl;
        }

', View::POS_END
);
?>
<style>


    .instrument_btn{
        cursor: pointer;
    }
    .search_input_btn{
        cursor: pointer;
    }
    #myVideo{
        width: 100%;
        border-radius: 10px;
        max-height: 340px;
        border: 1px solid #000;
    }

</style>

<section class="content video_display">
    <div class="col-md-2">
        <div class="white_bg">
            <div class="text-center categry-bg-white">
                <?php if (!empty($instrumentArray)) { ?>
                    <?php
                    foreach ($instrumentArray as $key => $value) {
                        $instrument_video_count = \app\models\BookedSession::instrumentCompletedLessonCount($key);
                        ?>

                        <div class=""><a  class="instrument_btn" data-bind="<?= $key ?>"><?= $value . ' (' . $instrument_video_count . ')' ?></a></div>
                    <?php } ?>
                <?php } ?>
            </div>
            <!--            <div class="form-group video_search_btm">
                            <label for="">Search</label>
                            <div class="input-group">
                                <input type="text" name="search_input_1" id="search_input_1" class="form-control">
                                <span class="input-group-addon search_input_btn"><i class="fa fa-search"></i></span>
                            </div>
                        </div>-->
        </div>
    </div>
    <div class="col-md-10">
        <div class="container-fluid video_categry-bg-white">
            <div class="col-md-10 video_content_bar">
                <div class=" mb-0">
                    <video id="myVideo" class="video-js resize" controls preload="none" controlsList="nodownload"  poster="<?= $poster; ?>"  autoplay onended="endVideo('<?= $model->uuid ?>', '<?= $p ?>')" >
                        <source id="myVideo_src" src="<?= $file_url; ?>" type="video/mp4" >
                    </video>
                </div>
                <div class="clearfix"></div>

                <ul class="list-unstyled list-inline">
                    <?php
                    $value = [];
                    $i = 1;
                    if (!empty($videos)) {
                        foreach ($videos as $key => $result) {
                            $current_part_class = ($p == $i) ? "orange" : "blackcolor";
                            ?>

                            <li class="list-inilne-item" style="float:left; margin:1%;">
                                <a href="javascript:void(0)" class="video-list <?= $current_part_class; ?>" data-id="<?= $model->uuid ?>" data-p="<?= $i ?>">

                                    <b>Part <?= $i ?></b> 
                                </a>
                            </li>

                            <?php
                            if ($i < count($videos)) {
                                $i++;
                            }
                        }
                    } else {
                        echo "<h6>No videos found</h6>";
                    }
                    ?>
                </ul>
                <div class="clearfix"></div>
                <h4><?= $model->studentUu->first_name . ' ' . $model->studentUu->last_name; ?></h4>
                <p><?= General::displayDateTime($model->created_at); ?></p>
                <?php if (!empty($view_count) && ($view_count > 0)) { ?>
                    <p><b><?= $view_count."</b> views"; ?></p>
                    <?php } ?>
                <?php
                if (!empty($model->studentBookingUu->instrument_uuid)) {
                    $instrument = Instrument::getInstrumentUsingUuid($model->studentBookingUu->instrument_uuid);
                    ?>
                    <p id="instrument" class="orange"><?= $instrument->name; ?></p>
                    <?php
                }
                ?>
                <p>Tutor : <?= $model->tutorUu->first_name . ' ' . $model->tutorUu->last_name; ?></p>

            </div>
            <div class="col-md-2">
                <h5><b>MORE RECORDED VIDEOS</b></h5>
                <?php
                if ($model->status !== 'PUBLISHED') {
                    $recordData = app\models\BookedSession::find()->joinWith(['studentUu', 'tutorUu'])->where(['<>', 'booked_session.uuid', $model->uuid])->andWhere(['booked_session.status' => 'COMPLETED'])->asArray()->limit(3)->orderBy('booked_session.updated_at')->all();
                } else {
                    if (Yii::$app->user->identity->role == 'STUDENT') {
                        $recordData = app\models\BookedSession::find()->joinWith(['studentUu', 'tutorUu'])->where(['<>', 'booked_session.uuid', $model->uuid])->andWhere(['booked_session.status' => 'PUBLISHED', 'booked_session.student_uuid' => Yii::$app->user->identity->student_uuid])->asArray()->limit(3)->orderBy('booked_session.updated_at')->all();
                    } elseif (Yii::$app->user->identity->role == 'TUTOR') {
                        $recordData = app\models\BookedSession::find()->joinWith(['studentUu', 'tutorUu'])->where(['<>', 'booked_session.uuid', $model->uuid])->andWhere(['booked_session.status' => 'PUBLISHED', 'booked_session.tutor_uuid' => Yii::$app->user->identity->tutor_uuid])->asArray()->limit(3)->orderBy('booked_session.updated_at')->all();
                    } else {
                        $recordData = app\models\BookedSession::find()->joinWith(['studentUu', 'tutorUu'])->where(['<>', 'booked_session.uuid', $model->uuid])->andWhere(['booked_session.status' => 'PUBLISHED'])->asArray()->limit(3)->orderBy('booked_session.updated_at')->all();
                    }
                }

                if (!empty($recordData)) {
                    foreach ($recordData as $key => $value) {
                        $thumb_url = is_file(Yii::$app->params['media']['session_recorded_video']['thumbnail']['path'] . $value['uuid'] . '.jpg') ?
                                Yii::$app->params['media']['session_recorded_video']['thumbnail']['url'] . $value['uuid'] . '.jpg' :
                                Yii::$app->params['theme_assets']['url'] . "images/thumbnail.jpeg";
                        ?>
                        <a href="<?= Url::to(['/video/tution-view/', 'id' => $value['uuid']]) ?>">
                            <img class="video_library_img img-reponsive" src="<?= $thumb_url; ?>">
                        </a>
                        <!--<p class="orange video_content_side_bar"><?php // $value['instrumentUu']['name'];   ?></p>-->
                        <p class="video_content_side_bar_sub"><?= $value['studentUu']['first_name'] . ' ' . $value['studentUu']['last_name']; ?></p>
                        <p class="video_name">
                            <?php
                            echo $value['tutorUu']['first_name'] . ' ' . $value['tutorUu']['last_name'];
                            ?>
                        </p>
                        <br>

                        <?php
                    }
                } else {
                    //echo "<h6>No video found</h6>";
                }
                ?>


            </div>
        </div>
    </div>
</section>
