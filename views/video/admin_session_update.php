<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\ArrayHelper;
use app\models\InstrumentCategory;
use app\models\Instrument;
use app\models\StudentViewRecordedLesson;
use app\models\User;
use yii\helpers\Url;
use app\libraries\General;

if ($model->status == 'COMPLETED') {
    $this->title = Yii::t('app', 'Lesson Video : '.$model->studentUu->first_name.' '.$model->studentUu->last_name);
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Uploaded Videos'), 'url' => (isset($back_url)) ? $back_url : ['index']];
    $this->params['breadcrumbs'][] = '';
} else {
    $this->title = Yii::t('app', 'Lesson Video : '.$model->studentUu->first_name.' '.$model->studentUu->last_name );
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tuition Videos'), 'url' => (isset($back_url)) ? $back_url : ['index']];
    $this->params['breadcrumbs'][] = '';
}
$videos = General::getLessonVideoListUsingUuid($model['uuid']);
$count = count($videos);

$this->registerJs(
        '
$(document).on("change","#bookedsession-status", function(){
    if($(this).val() == "REJECTED"){
        $(".rejected-reason-parent").show();
    } else {
        $(".rejected-reason-parent").hide();
    }

    if($(this).val() == "PUBLISHED"){
        $(".expire_date_parent").show();
    } else {
        $(".expire_date_parent").hide();
    }
});


$(document).on("click","#savebutton",function(){

    var file_data = $("#bookedsession-update_file").prop("files")[0];
    var form_data = new FormData($("form")[2]);
    form_data.append("update_file", file_data);
    $(".msg").text("Uploading in progress...");
    $.ajax({
            type: "POST",
            url: $( "#upload_form" ).attr("action"),
            data:  form_data,
            processData: false,
            contentType: false,
            beforeSubmit: function() {
                $("#progress-bar").width("0%");
              },
            xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        percentComplete = parseInt(percentComplete * 100);

                        $("#progress-bar").width(percentComplete + "%");
			$("#progress-status").html(percentComplete + "%");
                    }
                }, false);
                return xhr;
            },
            success: function (result) {

                if(result.code == 200){
                    $(".msg").text(result.message);

                    if($("#progress-status").html() == "100%"){

                        $("#progress-bar").width("0%");
                                $( ".form-open" ).hide();
                                $(".msg").hide();

                                //start rupal
                                //document.getElementById("myVideo").innerHTML = result.files.path;
//                                document.getElementById("title").innerHTML = result.data.title;
//                                document.getElementById("created_at").innerHTML = result.data.uploaded_date;
//                                document.getElementById("instrument").innerHTML = result.data.instrument;
//                                document.getElementById("instrumentCategory").innerHTML = result.data.instrument_category;

                                //$(".video-details").load("' . Url::to(['/video/review?id=db819285-d7fa-4246-a9a8-fbe33dd077a2']) . '", +  " .video-details");
                               //end
                                window.location.reload();
                                showSuccess(result.message);

//                        setTimeout(function(){
//                            location.href = "' . Url::to(['video/recorded']) . '";
//                         }, 1000);
                    }
                } else {
                    $("#uploadFormDiv").html(result);
                    $("#progress-bar").width("0%");
                }
            }, error:function(e){
                $("#progress-bar").width("0%");
                showErrorMessage(e.responseText);
            }
        });

});

//published-video
$(document).on("click",".published-enabled", function(){
        if(confirm("Are you sure, you want to publish this video?")){
        var id = $(this).attr("data-id");

            $.ajax({
                type: "GET",
                url: "' . Url::to(['/video/session-publish/']) . '",
                data: { "id": id},
                success:function(result){
                    if(result.code == 200){
                        window.location.reload();
                        showSuccess(result.message);
                    }else{
                        showErrorMessage(result.message);
                    }
                },
                error:function(e){
                    showErrorMessage(e.responseText);
                }
            });
        }
    });

    $(document).on("click",".edit",function(){
        //$( document ).ready( function() {
           // $( "#edit" ).click( function() {
                $( ".form-open" ).show();
                $("html,body").animate({
                    scrollTop: $(".form-open").offset().top},
                    "slow");

           // });

        });

        $(".video-list").click(function(){
          $("video").trigger("pause");
          var id = $(this).attr("data-id");
          var p = $(this).attr("data-p");
          var url = web_url + "video/tution-view?id="+id+"&p="+p;
          location.href = url;
        });

        function endVideo(id, p) {
          var count = '.$count.';
          if(p < count) {
            p++;
          } else {
            p = 1;
          }
          var dataurl = web_url + "video/tution-view?id="+id+"&p="+p;
          location.href = dataurl;
        }

', View::POS_END
);
$p = isset($_GET['p']) ? $_GET['p'] : 1;
$videos = General::getLessonVideoListUsingUuid($model['uuid']);
$filename = (!empty($videos)) ? $videos[$p]: $model['uuid'].".mp4";
$file_path = Yii::$app->params['media']['session_recorded_video']['path'] . $model['uuid'] . '/'. $filename;
$file_url = Yii::$app->params['media']['session_recorded_video']['url'] . $model['uuid'] . '/'. $filename;
$view_count = StudentViewRecordedLesson::find()->where(['booked_session_uuid'=> $model['uuid'],'part'=>$p])->count();

$poster = (is_file($file_path)) ? Yii::$app->params['media']['session_recorded_video']['thumbnail']['url'] . $model['uuid'].".jpg" : Yii::$app->params['theme_assets']['url'] . "images/novideo.png";
?>
<style>
    .video-js {
        width: 985px;
        height: 430px;
    }
    .edit{
        margin-top: 11px;
    }
    .download{
        margin-top: 11px;
    }

    .moreSession{
        border-left-style: ridge;
    }

    .title-details{
        margin-top: 16px;
    }
    .published-enabled{
        margin-top: 11px;
    }
    .custom-download{
        margin-top: -9px;
    }

</style>

<div class="col-xl-12 col-lg-12 col-12 col-md-12">
    <section class="box ">
        <header class="panel_header">
            <?php if ($model->status == 'COMPLETED') { ?>
                <h2 class="title float-left">Session Videos Details</h2>
            <?php } else { ?>
                <h2 class="title float-left">Tuition Videos Details</h2>
            <?php } ?>


            <div class="actions panel_actions float-right">
                <?php if ($model->status !== 'PUBLISHED') { ?>
                    <a href="<?= Yii::$app->getUrlManager()->createUrl(['/video/recorded/']) ?>" class="btn btn-info btn-icon "><i class="fa fa-arrow-left text-white"></i> <span>Back</span></a>
                <?php } else {
                if (Yii::$app->user->identity->role == 'TUTOR') { $backURL = '/booked-session/completed/';}
                else {$backURL = '/video/recorded/';}
                ?>

                    <a href="<?= Yii::$app->getUrlManager()->createUrl([$backURL]) ?>" class="btn btn-info btn-icon "><i class="fa fa-arrow-left text-white"></i> <span>Back</span></a>
                <?php } ?>
            </div>
        </header>
        <div class="content-body">
            <div class="row">

                <div class="col-md-12 mb-0">
                    <video id="myVideo" class="video-js resize" controls preload="none" controlsList="nodownload"  poster="<?= $poster; ?>"  autoplay onended="endVideo('<?= $model->uuid ?>', '<?= $p ?>')" >
                        <source id="myVideo_src" src="<?= $file_url; ?>" type="video/mp4" >
                    </video>
                </div>


                <div class="col-lg-8 col-md-12 col-12" style="border-right-style: ridge;">
                    <div class="form-row video-details">
                        <div class="col-md-12 mb-0">
                          <ul class="list-unstyled list-inline">
                          <?php
                            $value = [];
                            $i = 1;
                            if(!empty($videos)) {
                              foreach($videos as $key => $result) {
                          ?>

                            <li class="list-inilne-item" style="float:left; margin:1%;">
                            <a href="javascript:void(0)" class="video-list" data-id="<?= $model->uuid ?>" data-p="<?= $i ?>">
                              <?php if($p == $i){ ?><b style="color:black" disabled>Part <?= $i ?></b> <?php } else { ?>
                              <b>Part <?= $i ?></b> <?php } ?>
                            </a>
                            </li>

                          <?php
                                if($i < count($videos)) {
                                  $i++;
                                }
                              }
                            } else {
                              echo "<h6>No videos found</h6>";
                            }

                          ?>
                          </ul>
                        </div>
                        <div class="col-md-12 mb-0">
                            <h4 id ="title" onchange="myFunction()"><b><?= $model->studentUu->first_name.' '.$model->studentUu->last_name; ?> </b></h4>
                            <p id ="created_at"><small class="text-muted"><i class="fa fa-clock-o"></i> <?= \app\libraries\General::displayDate($model->start_datetime).' '.\app\libraries\General::displayTime($model->start_datetime).' - '.\app\libraries\General::displayTime($model->end_datetime); ?>&nbsp;<a class="text-primary moreSession" data-id="<?= $model->uuid; ?>"> More </a></small></p>

                            <p class="text-primary" id ="tutor_name"><i class="fa fa-smile-o" aria-hidden="true" style="font-size:26px;"></i>&nbsp;<b style="font-size:17px;"><?= $model->tutorUu->first_name . ' ' . $model->tutorUu->last_name; ?> </b></p>

                            <?php if ($model->status !== 'PUBLISHED') { ?>
                                <button class="btn btn-purple  btn-icon bottom15 right15 edit">
                                    <i class="fa fa-pencil"></i> &nbsp; <span>Edit</span>
                                </button>
                                <?php
                            }
                            if ($model->status !== 'PUBLISHED') {/*
                                ?>
                                <a href="<?php echo Yii::$app->params['media']['session_recorded_video']['url'] . $model->filename; ?>" download>
                                    <button class="btn btn-purple  btn-icon bottom15 right15 download">
                                        <i class="fa fa-download"></i> &nbsp; <span>Download</span>
                                    </button>
                                </a>
                            <?php } else { ?>
                                <a href="<?php echo Yii::$app->params['media']['session_recorded_video']['url'] . $model->filename; ?>" download>
                                    <button class="btn btn-purple  btn-icon bottom15 right15 custom-download">
                                        <i class="fa fa-download"></i> &nbsp; <span>Download</span>
                                    </button>
                                </a>
                                <?php*/
                            }

                            if ($model->status !== 'PUBLISHED') {
                                ?>
                                <button class="btn btn-purple  btn-icon bottom15 right15 published-enabled" data-id="<?= $model->uuid; ?>">
                                    <i class="fa fa-check-square-o" aria-hidden="true"></i> &nbsp; <span> Published </span>
                                </button>
                                <?php
                            }

                        if (!empty($view_count) && ($view_count > 0)) { ?>
                            <p><b><?= $view_count."</b> views"; ?></p>
                            <?php } 

                            if (!empty($model->studentBookingUu->instrument_uuid)) {
                                $instrument = Instrument::getInstrumentUsingUuid($model->studentBookingUu->instrument_uuid);
                                ?>
                                <p id="instrument"><b>Instrument :  </b><?= $instrument->name; ?></p>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <br><br>
                    <div class="row form-open" style="display:none; border-top-style: ridge;">
                        <div class="col-lg-12 col-md-12 col-12">
                            <div class="form-row">
                                <h3 class="title float-left"><b>Update Session Video</b></h3>
                                <div class="col-md-12 mb-0">

                                    <div id="uploadFormDiv">
                                        <?=
                                        $this->render('_admin_session_update_form', [
                                            'model' => $model,
                                        ])
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-lg-4 col-md-12 col-12">
                    <div class="col-lg-12">
                        <h4><b> More Videos </b></h4>
                        <!--<p>In design, space is concerned with the area <del>deep within the moment</del> of designated design.</p>-->


                        <div class="tab-pane fade show active" id="web-1">
                            <?php
                            if ($model->status !== 'PUBLISHED') {
                                $recordData = app\models\BookedSession::find()->joinWith(['studentUu','tutorUu'])->where(['<>', 'booked_session.uuid', $model->uuid])->andWhere(['booked_session.status' => 'COMPLETED'])->asArray()->limit(5)->orderBy('booked_session.updated_at')->all();
                            } else {
                                //$recordData = app\models\BookedSession::find()->joinWith(['studentUu','tutorUu'])->where(['<>', 'booked_session.uuid', $model->uuid])->andWhere(['booked_session.status' => 'PUBLISHED'])->asArray()->limit(5)->orderBy('booked_session.updated_at')->all();
                                if (Yii::$app->user->identity->role == 'STUDENT') {
                            		$recordData = app\models\BookedSession::find()->joinWith(['studentUu','tutorUu'])->where(['<>', 'booked_session.uuid', $model->uuid])->andWhere(['booked_session.status' => 'PUBLISHED','booked_session.student_uuid'=> Yii::$app->user->identity->student_uuid])->asArray()->limit(5)->orderBy('booked_session.updated_at')->all();
				   }elseif(Yii::$app->user->identity->role == 'TUTOR'){
					$recordData = app\models\BookedSession::find()->joinWith(['studentUu','tutorUu'])->where(['<>', 'booked_session.uuid', $model->uuid])->andWhere(['booked_session.status' => 'PUBLISHED','booked_session.tutor_uuid'=> Yii::$app->user->identity->tutor_uuid])->asArray()->limit(5)->orderBy('booked_session.updated_at')->all();
				   }else{
			   		$recordData = app\models\BookedSession::find()->joinWith(['studentUu','tutorUu'])->where(['<>', 'booked_session.uuid', $model->uuid])->andWhere(['booked_session.status' => 'PUBLISHED'])->asArray()->limit(5)->orderBy('booked_session.updated_at')->all();
				   }
                            }

                            if (!empty($recordData)) {
                                foreach ($recordData as $key => $value) {

                                    $thumb_url = is_file(Yii::$app->params['media']['session_recorded_video']['thumbnail']['path'] . $value['uuid'].'.jpg') ?
                                                        Yii::$app->params['media']['session_recorded_video']['thumbnail']['url'] . $value['uuid'].'.jpg' :
                                                        Yii::$app->params['theme_assets']['url'] . "images/thumbnail.jpeg";
                                    ?>
                                    <div class="search_result_more_video">
                                        <div class="float-left col-md-5 col-3 related-video">
                                            <a href="<?= Url::to(['/video/tution-view/', 'id' => $value['uuid']]) ?>">
                                                <img class="small-size" src="<?= $thumb_url; ?>">
                                            </a>
                                        </div>
                                        <div class="float-left col-md-7 col-9 title-details" >
                                            <h6><a href="<?= Url::to(['/video/tution-view/', 'id' => $value['uuid']]) ?>"><?= $value['studentUu']['first_name'].' '.$value['studentUu']['last_name']; ?></a></h6>
                                            <p><?= $value['tutorUu']['first_name'].' '.$value['tutorUu']['last_name']; ?></p>
                                        </div>
                                    </div>
                                    <?php

                                }
                            } else{
                                echo "<h6>No video found</h6>";
                            }
                            ?>

                        </div>

                    </div>

                </div>

            </div>
        </div>
    </section>
</div>
