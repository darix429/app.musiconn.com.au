<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tutor;

/**
 * TutorSearch represents the model behind the search form of `app\models\Tutor`.
 */
class TutorSearch extends Tutor {

    /**
     * {@inheritdoc}
     */
    public $instrument_name;
    public $plan_category;
    public function rules() {
        return [
                [['uuid', 'first_name', 'last_name', 'email', 'phone', 'street', 'city', 'state', 'country', 'postal_code', 'description', 'abn', 'contract_expiry', 'child_certi_number', 'child_certi_expiry', 'status', 'join_date', 'update_at', 'profile_image', 'instrument_name', 'plan_category'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $modeDatatable = false, $statusnot = '') {

        $query = Tutor::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->andFilterWhere(['<>', 'uuid', Yii::$app->user->identity->tutor_uuid]);

        if (!empty($statusnot)) {
            if (is_array($statusnot)) {

                $query->andWhere(['not in', 'status', $statusnot]);
            } else {

                $query->andWhere(['<>', 'status', $statusnot]);
            }
        }

        $query->andFilterWhere(['status' => $this->status]);

        if (!empty($this->first_name)) {
            $query->andFilterWhere(['ilike', "first_name", $this->first_name]);
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'contract_expiry' => $this->contract_expiry,
            'child_certi_expiry' => $this->child_certi_expiry,
            'join_date' => $this->join_date,
            'update_at' => $this->update_at,
        ]);

        $query->andFilterWhere(['ilike', 'uuid', $this->uuid])
                ->andFilterWhere(['ilike', 'first_name', $this->first_name])
                ->andFilterWhere(['ilike', 'last_name', $this->last_name])
                ->andFilterWhere(['ilike', 'email', $this->email])
                ->andFilterWhere(['ilike', 'phone', $this->phone])
                ->andFilterWhere(['ilike', 'description', $this->description])
                ->andFilterWhere(['ilike', 'abn', $this->abn])
                ->andFilterWhere(['ilike', 'child_certi_number', $this->child_certi_number])
                ->andFilterWhere(['ilike', 'status', $this->status]);


        if ($modeDatatable) {
            $result = $query->asArray()->orderBy('uuid DESC')->all();
            return $result;
        }
        return $dataProvider;
    }
    
    //Main OnlineTutorialPackageTutorwise model
    public function searchReportTutorList($params, $modeDatatable = false, $statusnot = '') {

        $query = TutorInstrument::find()->joinWith(['tutor','instrument']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->andFilterWhere(['<>', 'tutor.uuid', Yii::$app->user->identity->tutor_uuid]);

        if (!empty($statusnot)) {
            if (is_array($statusnot)) {

                $query->andWhere(['not in', 'tutor.status', $statusnot]);
            } else {

                $query->andWhere(['<>', 'tutor.status', $statusnot]);
            }
        }

        $query->andFilterWhere(['tutor.status' => $this->status]);
        
        $query->andFilterWhere(['ilike', 'instrument.name', $this->instrument_name]);
        
        $query->andFilterWhere(['tutor.online_tutorial_package_type_uuid' => $this->plan_category]);

        // grid filtering conditions
        $query->andFilterWhere([
            'tutor.update_at' => $this->update_at,
            'tutor.uuid' => $this->uuid,
        ]);
        
        if (!is_null($this->contract_expiry) && 
            strpos($this->contract_expiry, ' - ') !== false ) {
            list($start_date, $end_date) = explode(' - ', $this->contract_expiry);

            $query->andFilterWhere(['>=', '"contract_expiry"', date("Y-m-d", strtotime($start_date))." 00:00:00"]);
            $query->andFilterWhere(['<=', '"contract_expiry"', date("Y-m-d", strtotime($end_date))." 23:59:59"]);
        }
        
        if (!is_null($this->child_certi_expiry) && 
            strpos($this->child_certi_expiry, ' - ') !== false ) {
            list($start_date_certi, $end_date_certi) = explode(' - ', $this->child_certi_expiry);

            $query->andFilterWhere(['>=', '"child_certi_expiry"', date("Y-m-d", strtotime($start_date_certi))." 00:00:00"]);
            $query->andFilterWhere(['<=', '"child_certi_expiry"', date("Y-m-d", strtotime($end_date_certi))." 23:59:59"]);
        }
        
        if (!is_null($this->join_date) && 
            strpos($this->join_date, ' - ') !== false ) {
            list($start_date_join, $end_date_join) = explode(' - ', $this->join_date);

            $query->andFilterWhere(['>=', '"join_date"', date("Y-m-d", strtotime($start_date_join))." 00:00:00"]);
            $query->andFilterWhere(['<=', '"join_date"', date("Y-m-d", strtotime($end_date_join))." 23:59:59"]);
        }

        $query->andFilterWhere(['ilike', 'first_name', $this->first_name])
                ->andFilterWhere(['ilike', 'last_name', $this->last_name])
                ->andFilterWhere(['ilike', 'email', $this->email])
                ->andFilterWhere(['ilike', 'phone', $this->phone])
                ->andFilterWhere(['ilike', 'description', $this->description])
                ->andFilterWhere(['ilike', 'abn', $this->abn])
                ->andFilterWhere(['ilike', 'child_certi_number', $this->child_certi_number])
            ;


        if ($modeDatatable) {
            $result = $query->asArray()->orderBy('tutor.uuid DESC')->all();
            return $result;
        }
        return $dataProvider;
    }

}
