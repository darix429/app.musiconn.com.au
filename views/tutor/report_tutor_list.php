<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\web\View;
use app\models\OnlineTutorialPackageType;

$this->title = Yii::t('app', 'Tutors Listing');
$this->params['breadcrumbs'][] = $this->title;
$plan_cat_array = ArrayHelper::map(OnlineTutorialPackageType::find()->all(), 'uuid', 'display_name');

$this->registerJs(
   '
$(document).on("click","#search_btn", function(){        
    $.ajax({
        type: "GET",
        url: "'.Url::to(['/tutor/report-tutor-list/']).'",
        data: $("#search_frm").serialize(),
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            $("#coupon_table_parent_div").html(result);
            window.ULTRA_SETTINGS.dataTablesInit();
        },
        error:function(e){
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});

$(document).on("click","#search_reset_btn", function(){        
    $.ajax({
        type: "GET",
        url: "'.Url::to(['/tutor/report-tutor-list/']).'",
        data: {},
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            $("#coupon_table_parent_div").html(result);
            window.ULTRA_SETTINGS.dataTablesInit();
        },
        error:function(e){
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});
    
$(".datefilter").daterangepicker({
      autoUpdateInput: false,
      locale: {
          cancelLabel: "Clear",
          format: "DD-MM-YYYY"
      },ranges : {
                    "Today": [moment(), moment()],
                    "Yesterday": [moment().subtract("days", 1), moment().subtract("days", 1)],
                    "Last 7 Days": [moment().subtract("days", 6), moment()],
                    "Last 30 Days": [moment().subtract("days", 29), moment()],
                    "This Month": [moment().startOf("month"), moment().endOf("month")],
                    "Last Month": [moment().subtract("month", 1).startOf("month"), moment().subtract("month", 1).endOf("month")]
                }
  });

  $(".datefilter").on("apply.daterangepicker", function(ev, picker) {
      $(this).val(picker.startDate.format("DD-MM-YYYY") + " - " + picker.endDate.format("DD-MM-YYYY"));
  });

  $(".datefilter").on("cancel.daterangepicker", function(ev, picker) {
      $(this).val("");
  });
    
',View::POS_END
);
?>

<div class="col-xl-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Tutors Listing</h2>
            <div class="actions panel_actions float-right">
                <i class="fa fa-search icon-primary icon-xs icon-orange mail_head_icon custom-toggle" data-togglediv="datatable-search-div" title="Advance Search"></i>
                
            </div>
        </header>
        <div class="content-body"> 
            <div class="row ">
                <div class="col-lg-12 col-md-12 col-12 ">
                    <div class="datatable-search-div " style="display: none;">
                    
                    <?php $form = ActiveForm::begin([
                                'method' => 'get',
                                'options' => [
                                    'id' => 'search_frm',
                                    'class' => 'form-inline'
                                ],
                            ]); ?>
                   	    
		                <div class="col-md-3 mb-0">
		                    <?= $form->field($searchModel, 'first_name')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'First Name','title' => 'First Name',])->label(false) ?>
		                </div>
		                <div class="col-md-3 mb-0">
		                    <?= $form->field($searchModel, 'last_name')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Last Name','title' => 'Last Name',])->label(false) ?>
		                </div>
				<div class="col-md-3 mb-0">
                                    <?= $form->field($searchModel, 'email')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Email','title' => 'Email',])->label(false) ?>
		                </div>
				<div class="col-md-3 mb-0">
                                    <?= $form->field($searchModel, 'instrument_name')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Instrument', 'title' => 'Instrument',])->label(false) ?>
		                </div>
				<div class="col-md-3 mb-0">
                                    <?= $form->field($searchModel, 'plan_category')->dropDownList($plan_cat_array, ['prompt' => 'Select Plan Category','class' => 'form-control', 'style' => 'width:100%','title' => 'Select Plan Category',])->label(false) ?>
                                    
		                </div>
				<div class="col-md-3 mb-0">
                                    <?= $form->field($searchModel, 'contract_expiry')->textInput(['class' => 'form-control datefilter', 'maxlength' => true, 'title' => 'Contract Expiry', 'placeholder' => 'Contract Expiry'])->label(false) ?>
                                </div>
				<div class="col-md-3 mb-0">
                                    <?= $form->field($searchModel, 'child_certi_expiry')->textInput(['class' => 'form-control datefilter', 'maxlength' => true, 'title' => 'Certification Expiry', 'placeholder' => 'Certification Expiry'])->label(false) ?>
                                </div>
				<div class="col-md-3 mb-0">
                                    <?= $form->field($searchModel, 'join_date')->textInput(['class' => 'form-control datefilter', 'maxlength' => true, 'title' => 'Join Date', 'placeholder' => 'Join Date'])->label(false) ?>
                                </div>
                        <br><br>
			                       
                        <?= Html::Button(Yii::t('app', 'Search'), ['class' => 'btn btn-primary mb-0', 'id'=>'search_btn','style'=>'margin-left: 17px;']) ?>
                        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-purple mb-0', 'id'=>'search_reset_btn']) ?>
                    
                    <?php ActiveForm::end(); ?>
                </div>
                </div>
            </div>
            <br>
            <div class=""></div>
            <div class="clearfix"></div>
            <div class="row" id="coupon_table_parent_div">
                <?php echo Yii::$app->controller->renderPartial('_report_tutor_list', ['tutorList' => $tutorList,]); ?>
            </div>
        </div>
    </section> 
</div>
