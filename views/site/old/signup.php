<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use app\libraries\General;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \mdm\admin\models\form\Signup */

$timezone = General::getAllTimeZones();
if($model->timezone == '') {
 $model->timezone = Yii::$app->params['timezone'];
}
$this->title = 'Registration';
$this->params['breadcrumbs'][] = $this->title;
$this->registerJs('
$(".birthdate_signup").datepicker({
        minViewMode:  0,
        format: "dd-mm-yyyy",
        /* startDate: getValue($this, "startDate", ""),
        endDate: getValue($this, "endDate", ""),*/
        /* daysOfWeekDisabled: getValue($this, "disabledDays", ""),*/
        startView: 2,
        autoclose:true
    });

$("#student-country").change(function(){
                if($(this).val() == "other"){
                    $(".state_dd").hide();
                    $(".state_text").show();
                }else{
                    $(".state_text").hide();
                    $(".state_dd").show();
                }
            })
    ');
?>
<div class="login-wrapper">
    <div id="signup" class=" offset-xl-2 col-xl-8 offset-lg-12 col-lg-6 offset-md-3 col-md-6 col-offset-0 col-12">
        <!-- <h2 class="login-logo-text"><a href="#" title="Login Page" tabindex="-1"><?= Yii::$app->name; ?></a></h2> -->
        <h2 class="login-logo-text"><a href="<?= Yii::$app->params['base_url']; ?>" title="Login Page" tabindex="-1"><img src="/theme_assets/images/musiconn_logo.png"></a></h2>
        <section class="box" style="background-color:transparent">
            <header class="panel_header" style="background-color:#f5f5f5a8 !important;">
                <h2 class="title float-left"><?= $this->title; ?></h2>
                <div class="actions panel_actions float-right">
                    <!--i class="box_toggle fa fa-chevron-down"></i>
                    <i class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></i>
                    <i class="box_close fa fa-times"></i-->
                    <a  href="<?= Url::to(['/site/login/']) ?>"><i class="fa fa-sign-in test" style=' vertical-align: middle; font-size:2em;' title="Go to Sign In"></i></a>
                    <?php //echo Html::a('Go to Sign In', ['site/login'],['class'=>'btn btn-success btn-icon'])  ?>
                </div>
            </header>
            <!--?= Html::errorSummary($model) ?-->
            <!--?= Html::errorSummary($modelStudent) ?-->
            <?php
            $form = ActiveForm::begin([
                        'id' => 'form-signup',
                        'options' => ['name' => 'signupform'],
            ]);
            ?>
            <div class="content-body" style="background-color:#f5f5f5d6 !important;">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-12">
                        <div class="form-row inputPosition">
                            <div class="col-md-4">
                                <?= $form->field($modelStudent, 'first_name')->textInput(['autofocus' => false, 'class' => 'form-control', 'placeholder' => "First name"]); ?>
                            </div>
                            <div class="col-md-4">
                                <?= $form->field($modelStudent, 'last_name')->textInput(['autofocus' => false, 'class' => 'form-control', 'placeholder' => "Last name"]); ?>
                            </div>
                            <div class="col-md-4">
                                <?php $modelStudent->birthdate = \app\libraries\General::displayDate($modelStudent->birthdate);
                                echo $form->field($modelStudent, 'birthdate')->textInput(['autofocus' => false, 'class' => 'form-control birthdate_signup', 'placeholder' => "DD-MM-YYYY"]); ?>
                            </div>
                        </div>
                        <div class="form-row inputPosition">
                            <div class="col-md-6">
                                <?= $form->field($model, 'email')->textInput(['autofocus' => false, 'class' => 'input form-control', 'placeholder' => "Email"]); ?>
                            </div>
                            
                             <div class="col-md-6">
                                <?= $form->field($model, 'timezone')->dropDownList($timezone, ['class' => 'form-control myselect2', 'prompt' => 'Select Timezone', 'maxlength' => true]) ?>
                            </div>
                        </div>
                        <div class="form-row inputPosition">
                            <div class="col-md-6">
                                <?= $form->field($model, 'password_hash')->passwordInput(['autofocus' => false, 'class' => 'input form-control', 'placeholder' => "Password"]); ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'confirmPassword')->passwordInput(['autofocus' => false, 'class' => 'input form-control', 'placeholder' => "Confirm Password"]); ?>
                            </div>
                        </div>
                        <div class="form-row inputPosition">
                            <div class="col-md-4">
                                <?= $form->field($modelStudent, 'street')->textInput(['autofocus' => false, 'class' => 'input form-control', 'placeholder' => "Street"]); ?>
                            </div>
                            <div class="col-md-4">
                                <?= $form->field($modelStudent, 'city')->textInput(['autofocus' => false, 'class' => 'input form-control', 'placeholder' => "City"]); ?>
                            </div>
                            <div class="col-md-4">
                                <?php echo $form->field($modelStudent, 'postal_code')->widget(\yii\widgets\MaskedInput::className(['plceholder'=>'']), ['mask' => '9999', 'clientOptions' => ['removeMaskOnSubmit' => true],'options' => ['placeholder' => 'xxxx', 'class' => 'form-control']]) ?>
                                <?php // $form->field($modelStudent, 'postal_code')->textInput(['autofocus' => false, 'maxlength' => 4,'minlength' => 4,'class' => 'input form-control', 'placeholder' => "Postal Code"]); ?>
                            </div>
                        </div>
                        <?php if($modelStudent->country == 'other'){
                                $display_state_dd= 'none';
                                $display_state_text= 'block';
                            }else{
                                $display_state_dd= 'block';
                                $display_state_text= 'none';

                            } ?>
                        <div class="form-row inputPosition">
                            <div class="col-md-4">

                                <?= $form->field($modelStudent, 'country')->dropDownList(['AUS' => 'Australia','other' => 'Other'], ['class' => 'form-control', 'maxlength' => true]) ?>
                                <!--?= $form->field($modelStudent, 'country')->textInput(['autofocus' => false, 'class' => 'input form-control','placeholder'=>"Country"]); ?-->
                            </div>
                            <div class="col-md-4 state_dd" style="display:<?= $display_state_dd;?>">
                                <?= $form->field($modelStudent, 'state')->dropDownList(['New South Wales' => '	New South Wales', 'Queensland' => 'Queensland', 'South Australia' => 'South Australia', 'Tasmania' => 'Tasmania', 'Victoria' => 'Victoria', 'Western Australia' => 'Western Australia', 'Australian Capital Territory' => 'Australian Capital Territory', 'Northern Territory' => 'Northern Territory'], ['class' => 'form-control', 'maxlength' => true, 'prompt' => "Select " . $modelStudent->getAttributeLabel('state')]) ?>
                                <!--?= $form->field($modelStudent, 'state')->dropDownList(['NSW'=>'	New South Wales','QLD'=>'Queensland','SA'=>'South Australia','TAS'=>'Tasmania','VIC'=>'Victoria','WA'=>'Western Australia'], ['class' => 'form-control', 'maxlength' => true, 'prompt' => "Select " . $modelStudent->getAttributeLabel('state')]) ?-->
                                <!--?= $form->field($modelStudent, 'state')->textInput(['autofocus' => false, 'class' => 'input form-control','placeholder'=>"State"]); ?-->
                            </div>
                            <div class="col-md-4 state_text" style="display:<?= $display_state_text;?>">
                                <?= $form->field($modelStudent, 'state_text')->textInput(['autofocus' => false, 'class' => 'input form-control state_text', 'placeholder' => "State"]); ?>
                            </div>
                            <div class="col-md-4">
                                <?php // $form->field($modelStudent, 'phone_number')->textInput(['maxlength' => 10,'autofocus' => false, 'class' => 'input form-control', 'placeholder' => "Phone Number"]); ?>
                                <?= $form->field($modelStudent, 'phone_number')->widget(\yii\widgets\MaskedInput::className(['plceholder'=>'']), ['mask' => '9999 999 999', 'clientOptions' => ['removeMaskOnSubmit' => true],'options' => ['placeholder' => 'XXXX XXX XXX', 'class' => 'form-control']]) ?>
                            </div>
                        </div>
                        <div class="form-row float-right">
                            <div class="col-md-12">
                                <input class="form-control btn btn-primary float-right" type="submit" value="Register">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </section>
    </div>
</div>
