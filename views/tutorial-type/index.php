<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Tutorial Type');
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs(
        '
        $(document).on("click",".tutorialType_disabled_link", function(){  
            if(confirm("Are you sure you want to disable?")){
            var element = $(this);
                $.ajax({
                    type: "GET",
                    url: "' . Url::to(['/tutorial-type/disabled/']) . '",
                    data: { "id": $(this).data("uuid")},
                    //beforeSend:function(){ $(".local-spinner").show(); },
                    success:function(result){
                        if(result.code == 200){
                            $(element).removeClass("badge-primary");
                            $(element).addClass("badge-secondary");
                            $(element).removeClass("tutorialType_disabled_link");
                            $(element).addClass("tutorialType_enabled_link");
                            $(element).text("DISABLED");
                            showSuccess(result.message);
                        }else{
                            showErrorMessage(result.message);
                        }
                    },
                    error:function(e){ 
                        showErrorMessage("Sorry, something wrong.");
                    }
                });
            }
        });
    
        $(document).on("click",".tutorialType_enabled_link", function(){ 
            if(confirm("Are you sure you want to enable?")){
            var element = $(this);
                $.ajax({
                    type: "GET",
                    url: "' . Url::to(['/tutorial-type/enabled/']) . '",
                    data: { "id": $(this).data("uuid")},
                    //beforeSend:function(){ $(".local-spinner").show(); },
                    success:function(result){
                        if(result.code == 200){
                            $(element).removeClass("badge-secondary");
                            $(element).addClass("badge-primary");
                            $(element).removeClass("tutorialType_enabled_link");
                            $(element).addClass("tutorialType_disabled_link");
                            $(element).text("ENABLED");
                            showSuccess(result.message);
                        }else{
                            showErrorMessage(result.message);
                        }
                    },
                    error:function(e){ 
                        showErrorMessage("Sorry, something wrong.");
                    }
                });
            }
        });        

        $(document).on("click","#search_btn", function(){        
            $.ajax({
                type: "GET",
                url: "' . Url::to(['/tutorial-type/index/']) . '",
                data: $("#search_frm").serialize(),
		beforeSend:function(){ blockBody(); },
                success:function(result){
		    unblockBody();
                    $("#tutorial_type_table_parent_div").html(result);
                    window.ULTRA_SETTINGS.dataTablesInit();
                },
                error:function(e){
		    unblockBody(); 
                    showErrorMessage(e.responseText);
                }
            });
        });
        $(document).on("click","#search_reset_btn", function(){        
            $.ajax({
                type: "GET",
                url: "' . Url::to(['/tutorial-type/index/']) . '",
                data: {},
		beforeSend:function(){ blockBody(); },
                success:function(result){
		    unblockBody();
                    $("#tutorial_type_table_parent_div").html(result);
                    window.ULTRA_SETTINGS.dataTablesInit();
                },
                error:function(e){
		    unblockBody(); 
                    showErrorMessage(e.responseText);
                }
            });
        });
    '
);


?>

<div class="col-xl-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Tutorial Type List</h2>
            <div class="actions panel_actions float-right">
                <i class="fa fa-search icon-primary icon-xs icon-orange mail_head_icon custom-toggle" data-togglediv="datatable-search-div" title="Advance Search"></i>
            </div>
        </header>
        <div class="content-body">   
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12 ">
                    <div class="datatable-search-div " style="display: none;">

                        <?php 
                        $form = ActiveForm::begin([
                                    'method' => 'get',
                                    'options' => [
                                        'id' => 'search_frm',
                                        'class' => 'form-inline'
                                    ],
                        ]);
                        ?>
			
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'code')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Code', 'style' => 'width:100%'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'status')->dropDownList(['ENABLED' => 'ENABLED', 'DISABLED' => 'DISABLED'], ['class' => 'form-control', 'style' => 'width:100%'])->label(false) ?>
                        </div>  
                        
                            <?= Html::Button(Yii::t('app', 'Search'), ['class' => 'btn btn-primary mb-0', 'id' => 'search_btn']) ?>
                            <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-purple mb-0', 'id'=>'search_reset_btn']) ?>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>

            </div>
            <br>
            <div class="clearfix"></div>
            <div class="row" id="tutorial_type_table_parent_div">
                <?php echo Yii::$app->controller->renderPartial('_tutorial_type_list', ['tutorialTypeList' => $tutorialTypeList]); ?>
            </div>
        </div>
    </section>
</div>

















