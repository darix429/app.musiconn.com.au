<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PreRecordedVideo */

$this->title = Yii::t('app', 'Upload Video');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Upload Videos'), 'url' => ['uploaded']];
$this->params['breadcrumbs'][] = $this->title;
?>
<!--<div class="pre-recorded-video-create">

    <h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

<!--</div>-->
