<?php

use yii\helpers\Html;
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['name'] ?>,</p>
            
            <p>You have successfully video upload in <?= Yii::$app->name; ?>.</p>
            
            <p><strong>Title :</strong> <?= $content['title']; ?></p>
            <p><strong>Video Link :</strong> <?= $content['link'] ?></p>       
                      
            <p> If any query for your account then please contact to administrator.</p>

            <p>Sincerely,</p>
            <p>System Administrators</p>
        </td>
    </tr>
</table>

