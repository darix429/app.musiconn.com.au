<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "online_tutorial_package_type".
 *
 * @property string $uuid
 * @property string $name
 */
class OnlineTutorialPackageType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'online_tutorial_package_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'name'], 'required'],
            [['uuid', 'name'], 'string'],
            [['uuid'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'name' => Yii::t('app', 'Name'),
        ];
    }
}
