

/******************************
 Collepse div hide/show
 *****************************/
$(".custom-toggle").click(function () {
    var el = $(this).data("togglediv");
    $("." + el).fadeToggle("slow", "swing");
    return false;
});

$(document).on("focus", ".custom_date_picker", function () {
    $("#" + $(this).attr("id")).datepicker({
        minViewMode: 0,
        format: "dd-mm-yyyy",
        /* startDate: getValue($this, "startDate", ""),
         endDate: getValue($this, "endDate", ""),*/
        /* daysOfWeekDisabled: getValue($this, "disabledDays", ""),*/
        startView: 2,
        autoclose: true
    });
});

//Datetime picker
$(document).on("focus", ".custom_datetime_picker", function () {
    $("#" + $(this).attr("id")).datetimepicker({
        format: "dd-mm-yyyy hh:ii",
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        showMeridian: 0,
        startDate: '+1d'
    });
});

// Timepicker
if ($.isFunction($.fn.timepicker)) {

    $(".custom_time_picker").each(function (i, e) {
        var $this = $(e),
                options = {
                    template: getValue($this, 'template', false),
                    showSeconds: getValue($this, 'showSeconds', false),
                    defaultTime: getValue($this, 'defaultTime', 'current'),
                    showMeridian: getValue($this, 'showMeridian', true),
                    minuteStep: getValue($this, 'minuteStep', 15),
                    secondStep: getValue($this, 'secondStep', 15)
                },
                $nxt = $this.next(),
                $prv = $this.prev();

        $this.timepicker(options);
        //console.log($this);
        if ($nxt.is('.input-group-addon') && $nxt.has('a')) {
            $nxt.on('click', function (ev) {
                ev.preventDefault();

                $this.timepicker('showWidget');
            });
        }

        if ($prv.is('.input-group-addon') && $prv.has('a')) {
            $prv.on('click', function (ev) {
                ev.preventDefault();

                $this.timepicker('showWidget');
            });
        }
    });
}

$('.add-break').click(function () {
    var tr =
            '<tr>' +
            '<td class="break-day"><select name="breakAdd[]" id="breakAdd" class="form-control" style="width:auto"><option value="sunday">Sunday</option><option value="monday">Monday</option><option value="tuesday">Tuesday</option><option value="wednesday">Wednesday</option><option value="thursday">Thursday</option><option value="friday">Friday</option><option value="saturday">Saturday</option></select></td>' +
            '<td class="break-start"><input readonly name="breakAdd1[sunday][start][]" id="start" class="work-start form-control input-sm custom_time_picker" value="09:00 AM" data-template="dropdown" data-default-time="09:00 AM" data-show-meridian="true" data-minute-step="30"></td>' +
            '<td class="break-end"><input readonly name="breakAdd1[sunday][end][]" id="end" class="work-start form-control input-sm custom_time_picker" value="06:00 PM" data-template="dropdown" data-default-time="06:00 PM" data-show-meridian="true" data-minute-step="30"></td>' +
            '<td style="vertical-align:middle;text-align:center; color:red">' +
            '<a href="javascript:;" class="delete-break  badge badge-danger badge-md icon-lg icon-bordered icon-danger animated animated-delay-500ms rollIn " title="remove"  >' +
            '<span class="fa fa-times"></span>' +
            '</a>' +
            '</td>' +
            '</tr>';
    $('.breaks').prepend(tr);
    $(this).parent().parent().find('.break-start input, .break-end input').timepicker({
        timeFormat: 'hh:mm A',
    });
    // Bind editable and event handlers.
    tr = $('.breaks tr')[1];

});

$(document).on('click', '.delete-break', function () {
    $(this).parent().parent().remove();
});

$(document).on("change", "#breakAdd", function () {

    // Make all cells in current row editable.
    $(this).parent().parent().children().trigger('edit');
    // console.log($(this).parent().parent().find('.break-start input'));
    var testtt = [];
    $(this).parent().parent().find('.break-start input').attr('name', 'breakAdd1[' + this.value + '][start][]');
    ;
    $(this).parent().parent().find('.break-end input').attr('name', 'breakAdd1[' + this.value + '][end][]');
    $(this).parent().parent().find('.break-start input, .break-end input').timepicker({
        timeFormat: 'HH:mm A',
    });
    $('table.breaks tbody tr').each(function (i, el) {

        var value1 = $(el).children().eq(0).text();

        var value2 = $(el).children().eq(1).text();
        console.log($(el).children());
//            var res = $(el).children().eq(2).text();
//            if(value1 == something1 && value2 == something2){
//                inc = (inc)+1;
//                res = parseInt(res)+(1);
//                $(this).children(":eq(2)").text(res);
//            }
//            cnt = cnt+1;
    });
});


//Pre Recorded video play into popup
$(document).on("click", ".openVideoPopup", function () {
    var id = $(this).attr("data-id");
    var url = web_url + "video/video-play";

    $.ajax({
        type: "GET",
        url: url,
        data: {id: id},
        success: function (result) {
            if (result.code == 200) {
                //showSuccess(result.message);
                var title = "<h6 class=\"modal-title\">" + result.title + "</h6>";

                var video = "<video id=\"my-video\" class=\"video-js custom\" controls preload=\"auto\" controlsList=\"nodownload\" width=\"740\" height=\"380\" >"
                        + "<source src=\"" + result.file_url + "\" type=\"video/mp4\">"
                        + "<video>";

                $("#title").html(title);
                $("#video-content").html(video);
                $("#myModal").modal({show: true});

            } else {
                $("#myModal").hide();
                showErrorMessage(result.message);

            }
        }
    });
});

//video more content
$(document).on("click", ".more", function () {
    var id = $(this).attr("data-id");
    var url = web_url + "video/video-more-details";

    $.ajax({
        type: "GET",
        url: url,
        data: {id: id},
        success: function (result) {
            //console.log(result);

            var title = "<h5 class=\"modal-title\"><b>Details</b></h5>";

            var detail = "<p><b>Title &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :  &nbsp;&nbsp;</b>" + result.title + "</p>";

            if (result.instrument !== undefined) {
                detail = detail + "<p><b>Instrument :  </b>&nbsp;&nbsp;" + result.instrument + "</p>";
            }
            if (result.category !== undefined) {
                detail = detail + "<p><b>Category &nbsp;&nbsp;&nbsp;&nbsp; :  </b>&nbsp;&nbsp;" + result.category + "</p>";
            }
            detail = detail + "<p><b>Uploader &nbsp;&nbsp;&nbsp; :  </b>&nbsp;&nbsp;" + result.user + "</p>";
            detail = detail + "<p><b>Uploded &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :  </b>&nbsp;&nbsp;" + result.updated_at + "</p>";
            if (result.publish_date !== undefined) {
                detail = detail + "<p><b>Published &nbsp;&nbsp; :  </b>&nbsp;&nbsp;" + result.publish_date + "</p>";
            }
            if (result.description !== undefined) {
                detail = detail + "<p><b>Description :  </b>&nbsp;&nbsp;" + result.description + "</p>";
            }
            // +"<video>";

            $("#title-demo").html(title);
            $("#video-content").html(detail);
            $("#myModal").modal({show: true});


        }
    });
});


//model close video pause
$(document).on('hide.bs.modal', '#myModal', function (e) {
    $('#my-video').trigger('pause');
});

//Student play session button
$(document).on('click', '.session_start_button', function () {

    var myWindow = window.open($(this).data("url"), '_newWindow', 'fullscreen=yes,height=' + screen.height + ',width=' + screen.width + '');
    myWindow.focus();
});

$(document).on("click", ".moreSession", function () {

    var id = $(this).attr("data-id");
    var url = web_url + "video/tution-more-details";

    $.ajax({
        type: "GET",
        url: url,
        data: {id: id},
        success: function (result) {

            var title = "<h5 class=\"modal-title\"><b>Tuition Details</b></h5>";

            var detail = "<p><b>Student &emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;:  &nbsp;&nbsp;</b>" + result.student_name + "</p>";
            detail = detail + "<p><b>Tutor &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;</b>" + result.tutor_name + "</p>";
            detail = detail + "<p><b>Instrument&emsp;&emsp;&emsp;&emsp;: &nbsp;&nbsp;</b>" + result.instrument + "</p>";
            detail = detail + "<p><b>Session Date &emsp;&emsp;&emsp; :  </b>&nbsp;&nbsp;" + result.session_date + "</p>";
            detail = detail + "<p><b>Session Time &emsp;&emsp;&emsp; :  </b>&nbsp;&nbsp;" + result.session_time + "</p>";
            // +"<video>";

            $("#title-demo").html(title);
            $("#video-content").html(detail);
            $("#myModal").modal({show: true});


        }
    });
});

/************
 Loader
 ************/
var send_block_ui_body = true;

function blockPanel(id, msg = 'Please wait...')
{
    var block = $('.' + id);
    $(block).block({
        message: '<span class="loader_text_wrapper"><i class="fa fa-refresh fa-spin position-left"></i>&nbsp; ' + msg + '</span>',
        overlayCSS: {
            'z-index': 999999,
            backgroundColor: '#1b2024',
            opacity: 0.8,
            cursor: 'wait'
        },
        css: {
            'z-index': 999999,
            border: 0,
            padding: 0,
            color: '#fff',
            width: 'auto',
            '-webkit-border-radius': 2,
            '-moz-border-radius': 2,
            backgroundColor: 'transparent'
        }
    });
}

function unblockPanel(id)
{
    $('#' + id).unblock();
}
function unblockAllPanel()
{
    $('.blockUI').hide();
}


function blockUIBody(msg = 'Loading...')
{
    if (send_block_ui_body) {
        send_block_ui_body = false;
        $.blockUI({
            message: '<span class="loader_text_wrapper"><i class="fa fa-refresh fa-spin position-left"></i>&nbsp; ' + msg + '</span>',
            //timeout: 2000, //unblock after 2 seconds
            //fadeIn: 1,
            //fadeOut: 1,
            overlayCSS: {
                'z-index': 999999,
                backgroundColor: '#1b2024',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                'z-index': 999999,
                border: 0,
                color: '#fff',
                padding: 0,
                backgroundColor: 'transparent'
            }
        });

    }
    setTimeout(function () {
        send_block_ui_body = true;
    }, 500);
}
function unblockUIBody()
{
    $.unblockUI({
        //onUnblock: function(){  }
    });
}

function blockBody(msg = 'Loading...')
{
    $("#loader_message").text(msg);
    if(!$("#preloader").is(":visible")){
        $("#preloader").show();
    }
}
function unblockBody()
{
    $("#preloader").fadeOut('slow');
}
//
//$(window).on('load', function() {
//
//     console.log('window load')
//    $("#preloader").show();
//     console.log('show')
//})
//$(document).ready(function(){
//    console.log('redy')
//   $("#preloader").hide();
//    console.log('hide')
//
//});

/*
 $.blockUI({
 message: '<span class="loader_text_wrapper"><i class="fa fa-refresh fa-spin position-left"></i>&nbsp; Loading...</span>',
 timeout: 1000,
 overlayCSS: {
 'z-index': 9999,
 backgroundColor: '#1b2024',
 opacity: 0.8,
 cursor: 'wait'
 },
 css: {
 'z-index': 9999,
 border: 0,
 color: '#fff',
 padding: 0,
 backgroundColor: 'transparent'
 }
 });*/

//Tutor Dashbaord
$(document).on("click", ".tutor-unavailability", function () {
    //$("#tutorUnavailability").modal({show:true});
    var id = $(this).attr("data-id");
    var url = web_url + "site/tutor-unavailability";
    $.ajax({
        type: "GET",
        url: url,
        data: {id: id},
        beforeSend:function(){ blockBody(); },
        success: function (result) {
            unblockBody();
            var title = "<h5 class=\"modal-title\"><b>Unavailability</b></h5>";

            $("#title-tutorUnavailability").html(title);
            $("#unavailability-content").html(result);
            $("#tutorUnavailability").modal({show: true});
        },
        error:function(e){
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});

$(document).on("click", "#tutor_unavailability", function () {
    //$("#tutorUnavailability").modal({show:true});
    var id = $(this).attr("data-id");
    var data =  $("#unavailability").serialize();
    var url = web_url + "site/tutor-unavailability";
    $.ajax({
        type: "POST",
        url: url,
        data: $("#unavailability").serialize(),
        //beforeSend:function(){ blockBody(); },
        success: function (result) {
           // unblockBody();
            $("#unavailability-content").html(result);
            if(result.code == 200){
                $("#tutorUnavailability").modal('hide');
				window.location.reload();
                showSuccess(result.message);
            }else{
                //showErrorMessage(result.message);
            }
        },
        error:function(e){
          //  unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});

/****Student Booking Option Popup********/
$(document).on("click", "#studentBookingOption_button", function () {
    $("#model_credit_session_count").text($("#studentBookingOption_button").data('creditsession'));
    $("#studentBookingOption").modal('show');
});
$(document).on("click", ".redirect_booking_btn", function () {
    blockBody();
    $("#studentBookingOption").modal('hide');
    window.location.href = $(this).data('href');
});


/**** All Video Tag's right click prevent *******/
$(document).ready(function() {
    $("video").bind("contextmenu",function(){
        return false;
        });
 } );

 /******* form submit loader display*****/
// $( 'form' ).submit(function( event ) {alert(1)
//        blockBody();
//    });

/************** Tutor avaibility calender ********************/
$(document).on("click", ".tutor_availability_calender_btn", function () {
    //$("#tutorUnavailability").modal({show:true});
    var id = $(this).attr("data-id");
    var name = $(this).attr("data-tutorname");
    var url = web_url + "tutor/booking-availability";
    $.ajax({
        type: "GET",
        url: url,
        data: { "tutor_uuid" : id},
        beforeSend:function(){ blockBody(); },
        success: function (result) {

            $("#title-tutorAvailabilityCalender").html(name);
            $("#tutorAvailabilityCalender-content").html(result);
            $("#tutorAvailabilityCalender").modal('show');

            setTimeout(function (){

                $('.fc-month-button').click();
                //$('.fc-agendaWeek-button').click();
            }, 200);
            unblockBody();
        },
        error:function(e){
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});

$(document).on("click", "#edit_tutor_unavailability", function () {
    var id = $(this).data("id");
    var data =  $("#unavailability").serialize();
    var url = web_url + "tutor/tutor-get-unavailability?id="+id;
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function (result) {
	    console.log(result);
            $("#unavailability-content").html(result);
            if(result.code == 200){
                $("#tutorUnavailability").modal('hide');
		$('#tutor_calendar .edit-popover').parents().eq(2).remove();
		window.location.reload();
		//$('#tutor_calendar').fullCalendar('updateEvents', id);	
                showSuccess(result.message);
            }else{
                showErrorMessage(result.message);
            }
        },
        error:function(e){
            showErrorMessage(e.responseText);
        }
    });
});

$(document).on("click", "#tutor_search_btn", function () {
    var id = $("#tutor_select").val();
    var url = web_url + "student/tutor_available";
    $.ajax({
        type: "GET",
        url: url,
        data: { "tutor_uuid" : id},
        beforeSend:function(){ blockBody(); },
        success: function (result) {
            if(result.code == 500 || result.code == 404) {
                showErrorMessage(result.message);
            } else {
                $("#student_tutor_calendar_parent_div").html(result);
            }
            unblockBody();
        },
        error:function(e){
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});

/************** Musicoin credit/debit form from ADMIN side = START ********************/
function openCreditDebitCoinForm(){
   $.ajax({
        type: "GET",
        url: web_url + "musicoin/credit-debit?v=prod2",
        data: {},
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            $("#credit_debit_model_body").html(result);
            $("#credit_debit_modal").modal("show");
            window.ULTRA_SETTINGS.otherScripts();
        },
        error:function(e){ 
            unblockBody();
            showErrorMessage(e.responseText);
        }
    }); 
}

$(document).on("click","#credit_debit_save_btn", function(){        
    $.ajax({
        type: "POST",
        url: web_url + "musicoin/credit-debit/",
        data: $("#credit_debit_form").serialize(),
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            if(result.code == 200){
                showSuccess(result.message);
                $("#credit_debit_modal").modal("hide");
                $("#search_btn").trigger("click");
            }else if(typeof result.code !== "undefined" &&  result.code != 200){
                showErrorMessage(result.message);
            }else{
                $("#credit_debit_model_body").html(result);
                window.ULTRA_SETTINGS.otherScripts();
            }
        },
        error:function(e){ 
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});
/************** Musicoin credit/debit form from ADMIN side = END ********************/
