<?php

namespace app\models;

use Yii;
use app\libraries\General;

/**
 * This is the model class for table "student".
 *
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $phone_number
 * @property string $skype
 * @property string $street
 * @property string $city
 * @property string $state
 * @property string $country
 * @property string $postal_code
 * @property string $status
 * @property string $enrollment_id
 * @property string $student_uuid
 */
class Student extends \yii\db\ActiveRecord {

    public $state_text;
    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'student';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
                //html tag filter
                [['first_name','last_name','phone_number','street', 'city', 'country', 'postal_code','state_text'], 'match', 'pattern' => \app\libraries\General::validTextPattern(), 'message' => 'Invalid charecters used.'],    
                [['first_name', 'last_name', 'email', 'phone_number', 'street', 'city', 'country', 'postal_code', 'birthdate', 'referer_source'], 'required'],
                [['first_name', 'last_name', 'email', 'skype', 'street', 'city', 'state', 'country', 'postal_code', 'status', 'enrollment_id', 'student_uuid', 'referer_source'], 'string'],

                [['referer_source'], 'default', 'value'=>'N/A'],
                
                [['student_uuid'], 'unique'],
                [['created_at', 'updated_at'], 'integer'],
            
                [['birthdate'], 'ckeckDateMaxCurrentdate'],
                [['activation_link_token','state','state_text','musicoin_balance'], 'safe'],
                [['activation_link_token'], 'string'],
                [['birthdate'], 'date', 'format' => 'php:d-m-Y'],
                
                [['phone_number'], 'match',
                    'pattern'=> '/^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$/',
                    'message' => 'Enter a valid Phone Number.',
                    'when' => function($model) {
                    return ($model->country != "other");
                }, 'whenClient' => "function (attribute, value) {
                    return ( $('#student-country').val() !== 'other')?true:false;
                }"],
                        
                [['phone_number'], 'match',
                    'pattern'=> '/^\(?\b([0-9]{4})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{3})\b$/',
                    'message' => 'Enter a 10 digit Phone Number.',
                    'when' => function($model) {
                    return ($model->country == "other");
                }, 'whenClient' => "function (attribute, value) {
                    return ( $('#student-country').val() === 'other')?true:false;
                }"],
                        
                [['postal_code'], 'match',
                    'pattern'=> '/^\d{4}$/',
                    'message' => 'Enter a 4 digit postal code.',
                    'when' => function($model) {
                    return ($model->country != "other");
                }, 'whenClient' => "function (attribute, value) {
                    return ( $('#student-country').val() !== 'other')?true:false;
                }"],
                        
                [['postal_code'], 'string', 'max' => 10,
                    'when' => function($model) {
                    return ($model->country == "other");
                }, 'whenClient' => "function (attribute, value) {
                    
                    return ( $('#student-country').val() === 'other')?true:false;
                }"],

                [['state'], 'required', 'when' => function($model) {
                    return ($model->country != "other");
                }, 'whenClient' => "function (attribute, value) {
                    return ( $('#student-country').val() !== 'other')?true:false;
                }"],
                [['state_text'], 'required', 'when' => function($model) {
                    return ($model->country == "other");
                },'whenClient' => "function (attribute, value) {
                    return ( $('#student-country').val() == 'other')?true:false;
                }"],
                [['birthdate','state','state_text'], 'safe','on'=>'student_notification']
                
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'email' => Yii::t('app', 'Email'),
            'phone_number' => Yii::t('app', 'Phone Number'),
            'skype' => Yii::t('app', 'Skype'),
            'street' => Yii::t('app', 'Street'),
            'city' => Yii::t('app', 'City'),
            'state' => Yii::t('app', 'State/Territory'),
            'state_text' => Yii::t('app', 'State/Territory'),
            'country' => Yii::t('app', 'Country'),
            'postal_code' => Yii::t('app', 'Postal Code'),
            'status' => Yii::t('app', 'Status'),
            'enrollment_id' => Yii::t('app', 'Enrollment ID'),
            'student_uuid' => Yii::t('app', 'Student Uuid'),
            'birthdate' => Yii::t('app', 'Birthdate'),
            'created_at' => Yii::t('app', 'Join Date'),
            'activation_link_token' => 'Activation Link Token',
            'referer_source' => Yii::t('app', 'How did you hear about us?')
        ];
    }
    
    public function getUser()
    {
        return $this->hasOne(User::className(), ['student_uuid' => 'student_uuid']);
    }

    public function ckeckDateMaxCurrentdate($attribute) {
        $label = $this->getAttributeLabel($attribute);
        $currDate = date('Y-m-d');
        $attr_val = $attribute;
        $att_date = date("Y-m-d", strtotime($this->$attr_val));
        if ($att_date == $currDate) {
            $this->addError($attribute, $label . " must be less than today date.");
        } elseif ($att_date > $currDate) {
            $this->addError($attribute, $label . " can not allow to future date.");
        }
    }

    public static function isActivationTokenValid($token, $uuid) {
        $flag = false;
        if (empty($token)) {
            $flag = false;
        }
        if (!empty($uuid)) {
            $model = Student::find()->where(['student_uuid' => $uuid])->one();
            if (!empty($model)) {
                if ($model->activation_link_token == $token) {
                    $expire = Yii::$app->params['user.passwordResetTokenExpire'];
                    $parts = explode('_', $token);
                    $timestamp = (int) end($parts);
                    $flag = ($timestamp + $expire >= time());
                }
            }
        }

        return $flag;
    }

    public function generateActivationToken() {
        $token = Yii::$app->security->generateRandomString() . '_' . time();
        return $token;
    }

    public function CheckBookedTution($student_id) {
        $student = \app\models\BookedSession::find()->joinWith(['studentUu'])->where(['booked_session.student_uuid' => $student_id, 'booked_session.status' => 'SCHEDULE'])->all();
        if (!empty($student)) {
            return $student;
        } else {
            return array();
        }
    }

    public function CancelBookedTution($book_id) {
        $status = false;
        $studentSession = \app\models\BookedSession::find()->where(['booked_session.uuid' => $book_id, 'booked_session.status' => 'SCHEDULE'])->all();
        if (!empty($studentSession)) {
            foreach($studentSession as $key => $value){
                $value->status = 'CANCELLED';
                $value->cancel_by = 'STUDENT';
                $value->cancel_user_uuid = $value->student_uuid;
                $value->cancel_reason = 'ACCOUNT_DELETED';
                $value->cancel_datetime = date('Y-m-d H:i:s', time());
                if ($value->save(false)) {
                    $status = true;
                }else{
                    $status = false;
                }
            }
//            $value->-save();
            return $status;
        } else {
            return $status;
        }
    }

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['step_2'] = ['number', 'name', 'cvv', 'cc_month_year'];
        $scenarios['student_notification'] = ['student_uuid'];
        return $scenarios;
    }

    public function split_events($start,$end,$week_day_number) {
        $week_days_array = ['sunday','monday','tuesday','wednesday','thursday','friday','saturday'];
        $before_event = [];
        $after_event = [];
        if('00:00' < $start){
            $before_event['start'] = '00:00';
            $before_event['end'] =  $start;
            $before_event['week_day'] = $week_days_array[$week_day_number];
            $before_event['dow'] = [$week_day_number];
            $before_event['title'] = "Not Working";
            $before_event['description'] = "Not Working";
            $before_event['className'] = 'fc-un-available fa-none-working';
            $before_event['allDay'] = false;
            $before_event['editable'] = false;
            $before_event['type'] = 'none_working';
        }

        if('23:59' > $end){
            $after_event['start'] = $end;
            $after_event['end'] = '23:59';
            $after_event['week_day'] = $week_days_array[$week_day_number];
            $after_event['dow'] = [$week_day_number];
            $after_event['title'] = "Not Working";
            $after_event['description'] = "Not Working";
            $after_event['className'] = 'fc-un-available fa-none-working';
            $after_event['allDay'] = false;
            $after_event['editable'] = false;
            $after_event['type'] = 'none_working';
        }
        return array($after_event, $before_event);
    }


    public function break_events($breaks, $week_day_number) {
        $event_array = [];
        $week_days_array = ['sunday','monday','tuesday','wednesday','thursday','friday','saturday'];

        if(!empty($breaks)){    
            foreach ($breaks as $key => $value) {
                $event['start'] = $value['start'];
                $event['end'] = $value['end'];
                $event['week_day'] = $week_days_array[$week_day_number];
                $event['dow'] = [$week_day_number];
                $event['title'] = "Break";
                $event['description'] = "Break";
                $event['className'] = 'fc-un-available fa-break';
                $event['allDay'] = false;
                $event['editable'] = false;
                $event['type'] = 'break';

                $event_array[] = $event;
            }
        }


        return $event_array;
    }

    public function prepareTutorUnavailabilityEvents($tutor_uuid = '', $start_date = '', $end_date = '') {

        $entdays = [];
        if (!empty($tutor_uuid) && !empty($start_date) && !empty($end_date)) {
            $where_clause = '("tutor_uuid"= \'' . $tutor_uuid . '\')
                AND ((to_char(start_datetime, \'YYYY-MM-DD\') > \'' . $start_date . '\' AND to_char(start_datetime, \'YYYY-MM-DD\') < \'' . $end_date . '\')
                or (to_char(end_datetime, \'YYYY-MM-DD\') > \'' . $start_date . '\' AND to_char(end_datetime, \'YYYY-MM-DD\') < \'' . $end_date . '\')
                or (to_char(start_datetime, \'YYYY-MM-DD\') <= \'' . $start_date . '\' AND to_char(end_datetime, \'YYYY-MM-DD\') >= \'' . $end_date . '\'))
            ';

            $query = 'SELECT *, \'Yes\' AS "is_tutor_unavailability" FROM "tutor_unavailability" WHERE ' . $where_clause;

            $command = Yii::$app->db->createCommand($query);
            $model_unavailability = $command->queryAll();

            if (!empty($model_unavailability)) {

                foreach ($model_unavailability as $key => $value) {
                    $ent['id'] = $value['uuid'];
                    $ent['start'] = General::convertSystemToUserTimezone($value['start_datetime'], 'Y-m-d H:i:s');
                    $ent['end'] = General::convertSystemToUserTimezone($value['end_datetime'], 'Y-m-d H:i:s');
                    $ent['title'] = "Unavailable - ".substr($value['reason'], 0, 50);
                    $ent['description'] = $value['reason'];
                    $ent['className'] = 'fc-un-available fa-unavailability';
                    $ent['allDay'] = false;
                    $ent['editable'] = false;
                    $ent['type'] = 'unavailability';

                    $entdays[] = $ent;
                }
            }
        }
        return $entdays;
    }

        public function prepareTutorWeekDayNoneWorkingEvents_booking($tutor_uuid,$start_date, $end_date, $tutor_timezone, $user_timezone) {

        $entdays = [];
        if (($model = Tutor::findOne($tutor_uuid)) !== null) {

            $working_plan = json_decode($model->working_plan,true);
            $week_days_array = ['sunday','monday','tuesday','wednesday','thursday','friday','saturday'];
            $tutor_week_days_array = array_keys($working_plan);

            if(!empty($week_days_array)){
                foreach ($week_days_array as $week_key => $week_day) {

                    if(isset($working_plan[$week_day]) && is_array($working_plan[$week_day])){

                        if(!empty($working_plan[$week_day]['start']) && $working_plan[$week_day]['end']){

                            $split_event = self::split_events($working_plan[$week_day]['start'], $working_plan[$week_day]['end'],$week_key);


                            $break_event = [];
                            if(isset($working_plan[$week_day]['breaks']) && is_array($working_plan[$week_day]['breaks'])){
                                $break_event = self::break_events($working_plan[$week_day]['breaks'], $week_key);
                            }

                            $merge_events = array_merge($break_event,$split_event);

                            $split_event_filter = array_filter($merge_events);
                            if(!empty($split_event_filter)){
                                foreach ($split_event_filter as $filter_array) {
                                    $entdays[] = $filter_array;
                                }
                            }
                        }
                    }else{
                        //Day not available

                        $ent['start'] = '00:00';
                        $ent['end'] = '23:59';
                        $ent['week_day'] = $week_days_array[$week_key];
                        $ent['dow'] = [$week_key];
                        $ent['title'] = "Not Working";
                        $ent['description'] = "Not Working";
                        $ent['className'] = 'fc-un-available fa-none-working';
                        $ent['allDay'] = false;
                        $ent['editable'] = false;
                        $ent['type'] = 'none_working';

                        $entdays[] = $ent;
                    }
                }
            }
        }

        $start_date = General::convertTimezone($start_date.' 00:00',$user_timezone,$tutor_timezone,'Y-m-d H:i');
        $end_date = General::convertTimezone($end_date.' 23:59',$user_timezone,$tutor_timezone,'Y-m-d H:i');

        $new_event = [];
        foreach ($entdays as $key => $event) {

            $current_date = $start_date;

            while ($end_date > $current_date ) {

                $dy = strtolower(General::displayTimeFormat($current_date,'l'));

                if($dy == $event['week_day']){

                    $start_time = General::displayTimeFormat($current_date,'Y-m-d').' '.$event['start'];
                    $end_time = General::displayTimeFormat($current_date,'Y-m-d').' '.$event['end'];
                    $array = $event;
                    $array['start'] = General::convertTimezone($start_time,$tutor_timezone,$user_timezone,'Y-m-d H:i:s');
                    $array['end'] = General::convertTimezone($end_time,$tutor_timezone,$user_timezone,'Y-m-d H:i:s');
                    $new_event[] = $array;
        }

                $current_date = date('Y-m-d H:i', strtotime($current_date. ' +1 day'));

    }
        }

        return $new_event;
    }

    public function prepareTutorScheduleAppoinmentEvents($tutor_uuid = '', $start_date = '', $end_date = '') {

        $entdays = [];
        if (!empty($tutor_uuid) && !empty($start_date) && !empty($end_date)) {
            $where_clause = '(booked_session."tutor_uuid"= \'' . $tutor_uuid . '\' AND (booked_session."status" = \'SCHEDULE\') )
                AND ((to_char(start_datetime, \'YYYY-MM-DD\') > \'' . $start_date . '\' AND to_char(start_datetime, \'YYYY-MM-DD\') < \'' . $end_date . '\')
                or (to_char(end_datetime, \'YYYY-MM-DD\') > \'' . $start_date . '\' AND to_char(end_datetime, \'YYYY-MM-DD\') < \'' . $end_date . '\')
                or (to_char(start_datetime, \'YYYY-MM-DD\') <= \'' . $start_date . '\' AND to_char(end_datetime, \'YYYY-MM-DD\') >= \'' . $end_date . '\'))
            ';

            $join = ' LEFT JOIN student on student.student_uuid = booked_session.student_uuid ';
            $join .= ' LEFT JOIN tutor on tutor.uuid = booked_session.tutor_uuid ';

            $query = 'SELECT booked_session.*,CONCAT(student.first_name,\' \',student.last_name) as student_name,CONCAT(tutor.first_name,\' \',tutor.last_name) as tutor_name FROM "booked_session" '.$join.' WHERE ' . $where_clause;
            $command = Yii::$app->db->createCommand($query);
            $model_tution = $command->queryAll();

            if (!empty($model_tution)) {

                foreach ($model_tution as $key => $value) {
                    $ent['id'] = $value['uuid'];
                    $ent['start'] = General::convertSystemToUserTimezone($value['start_datetime'], 'Y-m-d H:i:s');
                    $ent['end'] = General::convertSystemToUserTimezone($value['end_datetime'], 'Y-m-d H:i:s');
                    $ent['allDay'] = false;
                    $ent['editable'] = false;
                    $ent['type'] = 'schedule_tution';
                    $ent['student_name'] = $value['student_name'];
                    $ent['tutor_name'] = $value['tutor_name'];
                    $ent['title'] = "Schedule Lesson";
                    $ent['description'] = "Schedule Lesson";
                    $ent['className'] = 'fc-un-available fa-schedule-tution';

                    $entdays[] = $ent;
                }
            }
        }
        return $entdays;
    }

    public function prepareStudentScheduleAppoinmentEvents($tutor_uuid = '', $start_date = '', $end_date = '') {

        $entdays = [];
        if (!empty($tutor_uuid) && !empty($start_date) && !empty($end_date)) {
            $where_clause = '(booked_session."tutor_uuid"= \'' . $tutor_uuid . '\' AND (booked_session."status" = \'SCHEDULE\') )
                AND ((to_char(start_datetime, \'YYYY-MM-DD\') > \'' . $start_date . '\' AND to_char(start_datetime, \'YYYY-MM-DD\') < \'' . $end_date . '\')
                or (to_char(end_datetime, \'YYYY-MM-DD\') > \'' . $start_date . '\' AND to_char(end_datetime, \'YYYY-MM-DD\') < \'' . $end_date . '\')
                or (to_char(start_datetime, \'YYYY-MM-DD\') <= \'' . $start_date . '\' AND to_char(end_datetime, \'YYYY-MM-DD\') >= \'' . $end_date . '\'))
            ';

            $join = ' LEFT JOIN student on student.student_uuid = booked_session.student_uuid ';
            $join .= ' LEFT JOIN tutor on tutor.uuid = booked_session.tutor_uuid ';

            $query = 'SELECT booked_session.*,CONCAT(student.first_name,\' \',student.last_name) as student_name,CONCAT(tutor.first_name,\' \',tutor.last_name) as tutor_name FROM "booked_session" '.$join.' WHERE ' . $where_clause;
            $command = Yii::$app->db->createCommand($query);
            $model_tution = $command->queryAll();

            if (!empty($model_tution)) {

                foreach ($model_tution as $key => $value) {
                    
                    $ent['id'] = $value['uuid'];
                    $ent['start'] = General::convertSystemToUserTimezone($value['start_datetime'], 'Y-m-d H:i:s');
                    $ent['end'] = General::convertSystemToUserTimezone($value['end_datetime'], 'Y-m-d H:i:s');
                    $ent['allDay'] = false;
                    $ent['editable'] = false;
                    $ent['type'] = 'student_schedule_tution';
                    $ent['student_name'] = $value['student_name'];
                    $ent['tutor_name'] = $value['tutor_name'];
                    $ent['title'] = "Student Scheduled Lesson";
                    $ent['description'] = "Student Scheduled Lesson";
                    $ent['className'] = 'fc-un-available fa-student-schedule-tution';

                    $entdays[] = $ent;
                }
            }
        }
        return $entdays;
    }

    public function prepareTutorRescheduleAppoinmentEvents($tutor_uuid = '', $start_date = '', $end_date = '') {

        $entdays = [];
        if (!empty($tutor_uuid) && !empty($start_date) && !empty($end_date)) {
            $where_clause = '(reschedule_session."tutor_uuid"= \'' . $tutor_uuid . '\' AND (reschedule_session."status" = \'AWAITING\') )
                AND ((to_char(to_start_datetime, \'YYYY-MM-DD\') > \'' . $start_date . '\' AND to_char(to_start_datetime, \'YYYY-MM-DD\') < \'' . $end_date . '\')
                or (to_char(to_end_datetime, \'YYYY-MM-DD\') > \'' . $start_date . '\' AND to_char(to_end_datetime, \'YYYY-MM-DD\') < \'' . $end_date . '\')
                or (to_char(to_start_datetime, \'YYYY-MM-DD\') <= \'' . $start_date . '\' AND to_char(to_end_datetime, \'YYYY-MM-DD\') >= \'' . $end_date . '\'))
            ';

            $join = ' LEFT JOIN student on student.student_uuid = reschedule_session.student_uuid ';
            $join .= ' LEFT JOIN tutor on tutor.uuid = reschedule_session.tutor_uuid ';

            $query = 'SELECT reschedule_session.*,CONCAT(student.first_name,\' \',student.last_name) as student_name,CONCAT(tutor.first_name,\' \',tutor.last_name) as tutor_name FROM "reschedule_session" '.$join.' WHERE ' . $where_clause;
            $command = Yii::$app->db->createCommand($query);
            $model_tution = $command->queryAll();

            if (!empty($model_tution)) {

                foreach ($model_tution as $key => $value) {
                    $ent['id'] = $value['uuid'];
                    $ent['start'] = General::convertSystemToUserTimezone($value['start_datetime'], 'Y-m-d H:i:s');
                    $ent['end'] = General::convertSystemToUserTimezone($value['end_datetime'], 'Y-m-d H:i:s');
                    $ent['allDay'] = false;
                    $ent['editable'] = false;
                    $ent['type'] = 're_schedule_lesson';
                    $ent['student_name'] = $value['student_name'];
                    $ent['tutor_name'] = $value['tutor_name'];
                    $ent['title'] = "Tutor Reschedule Lesson";
                    $ent['description'] = "Pending Tutor Reschedule Lesson Request Approval";
                    $ent['className'] = 'fc-un-available fa-tutor-reschedule-lesson';

                    $entdays[] = $ent;
                }
            }
        }
        return $entdays;
    }
}
