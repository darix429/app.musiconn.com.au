<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\PremiumStandardPlanRate;
use app\models\TutorFee;

?>

<div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">

    <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Booking ID</th>
                <th>Student ID</th>
                <th>Student</th>
                <th>Tutor</th>
                <th>Session Pin</th>
                <th>Lesson Date</th>
                <th>Time</th>
                <th>Lesson Duration</th>
                <th>Tutor Plan Category</th>
                <th>Tutor Plan Option</th>
                <th>Tutor Pay Rate(GST-Inclusive)</th>
                <th>Tutorial Package Purchased</th>
                <th>Free Credit</th>
                <th>Cancellation Date</th>
                <th>Status</th>
            </tr>
        </thead>

        <tbody>
            <?php
            if (!empty($bookedSessionList)) {
                foreach ($bookedSessionList as $key => $value) {
                $tutor_fee = '0.00';
                $tutorial_name = (isset($value['creditSessionUu']['credited_by']) && $value['creditSessionUu']['credited_by'] == 'OWNER'|| $value['creditSessionUu']['credited_by'] == 'ADMIN' || $value['creditSessionUu']['credited_by'] == 'SUBADMIN') ? "Free Credit" : "No";
                $student_name = $value['studentUu']['first_name']. ' ' . $value['studentUu']['last_name'];
                $tutor_name = $value['tutorUu']['first_name']. ' ' .$value['tutorUu']['last_name']; 
                $plan_model = PremiumStandardPlanRate::find()->joinWith(['onlineTutorialPackageTypeUu'])->where(['premium_standard_plan_rate.uuid'=>$value['studentBookingUu']['premium_standard_plan_rate_uuid']])->asArray()->one();
                $option = $value['studentBookingUu']['plan_rate_option'];
                
                if(!empty($value['studentBookingUu']['premium_standard_plan_rate_uuid']))
                {
                $tutor_fee_model = TutorFee::find()->where(['online_tutorial_package_type_uuid'=> $plan_model['onlineTutorialPackageTypeUu']['uuid'],'option'=>$option,'sesson_length'=>$value['session_min']])->one();
                
                if(isset($tutor_fee_model->tutor_fee))
                {
                $tutor_fee = $tutor_fee_model->tutor_fee;
                }
                }
                
                ?>
                    <tr>
                        <td><?= $key + 1; ?></td>
                        <td><?= (!empty($value['studentBookingUu']['booking_id'])) ? "#".$value['studentBookingUu']['booking_id'] : ''; ?></td>
                        <td><?= $value['studentUu']['enrollment_id']; ?></td>
                        <td><?= (!empty($student_name)) ? $student_name : ''; ?></td>
                        <td><?= (!empty($tutor_name)) ? $tutor_name : ''; ?></td>
                        <td><?= $value['session_pin']; ?></td>
                        <td><?= \app\libraries\General::displayDate($value['start_datetime']); ?></td>
                        <td><?= \app\libraries\General::displayTime($value['start_datetime']) ." - ". \app\libraries\General::displayTime($value['end_datetime']) ?></td>
                        <td><?= $value['session_min']; ?> minutes</td>
                        <td><?= strtoupper($plan_model['onlineTutorialPackageTypeUu']['display_name']) ?></td>
                        <td><?= ($value['studentBookingUu']['plan_rate_option']!=0) ? $value['studentBookingUu']['plan_rate_option']:''?></td>
                        <td><?= "$".$tutor_fee;?></td>
                        <td><?= $value['studentBookingUu']['tutorial_name']; ?></td>
                        <td><?= $tutorial_name == 'Free Credit' ? 'Yes' : ''; ?></td>
                        <td><?= (!empty($value['cancel_datetime'])) ? \app\libraries\General::displayDate($value['cancel_datetime']) : '-'; ?></td>
                        <td>
                            <?php
                         
                            if ($value['status'] == 'SCHEDULE') {
                                $e_class = "badge-info";
				//$e_link_class = "coupon_disabled_link";
                            } elseif ($value['status'] == 'PUBLISHED') {
                                $e_class = "badge-success";
				//$e_link_class = "coupon_enabled_link";
                            } elseif ($value['status'] == 'EXPIRED') {
                                $e_class = "badge-secondary";
				//$e_link_class = "coupon_enabled_link";
                            } elseif ($value['status'] == 'INPROCESS') {
                                $e_class = "badge-warning";
				//$e_link_class = "coupon_enabled_link";
                            } else {
                                $e_class = "badge-danger";
				//$e_link_class = "";
                            }
                            ?>
                            <span class="badge badge-pill <?= $e_class ?> "  ><?= $value['status']; ?></span>
                        </td>
                        
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>

</div>
