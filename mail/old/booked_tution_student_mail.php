<?php

use yii\helpers\Html;

?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['student_name']; ?>,</p>
            
            <p>Thank you for your payment. Attached is a receipt (<?= $content['order_id']; ?>) as record of payment to the tutorial package and/or online content access you have selected.</p>
            
            <p>Below are the details of your purchase:</p>
            
            <p><strong>Scheduled tutorials</strong></p>
            
            <p><strong>Booking Reference :</strong> <?= "#".$content['booking_id']; ?></p>
            <p><strong>Tutor Name :</strong> <?= $content['tutor_name']; ?></p>
            <p><strong>Instrument :</strong> <?= $content['instrument']; ?></p>
            <p><strong>Scheduled Lessons :</strong> 
                <?php if(is_array($content['session_dates'])){ ?>
                <ul>
                    <?php foreach ($content['session_dates'] as $key => $value) {
                        echo "<li>".$value." ".$content['time']."</li>";
                    } ?>
                </ul>
                <?php } ?>
            </p>
            <p><strong>Subscription to online contents :</strong> <?= (!empty($content['student_monthly_subscription_uuid']) ) ? "Yes" : "No"; ?></p>
            
            <p>For terms and conditions of your purchase, please refer to the <a target="_blank" href="https://www.musiconn.com.au/">Musiconn website</a>.</p>
            
            <p>Please do not reply to this unattended mailbox.  Any queries can be directed to admin@musiconn.com.au.</p>
            
            <p>Regards,<br>
            Julian Fung <br>
            CEO – Musiconn Pty Ltd</p>
        </td>
    </tr>
</table>

