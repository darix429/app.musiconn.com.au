<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "standard_plan_option_rate".
 *
 * @property string $uuid
 * @property string $premium_standard_plan_rate_uuid
 * @property string $price
 * @property string $gst
 * @property string $gst_description
 * @property string $total_price
 * @property string $option
 *
 * @property PremiumStandardPlanRate $premiumStandardPlanRateUu
 */
class StandardPlanOptionRate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'standard_plan_option_rate';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'premium_standard_plan_rate_uuid', 'gst_description', 'option'], 'required'],
            [['uuid', 'premium_standard_plan_rate_uuid', 'gst_description', 'option'], 'string'],
            [['price', 'gst', 'total_price'], 'number'],
            [['uuid'], 'unique'],
            [['premium_standard_plan_rate_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => PremiumStandardPlanRate::className(), 'targetAttribute' => ['premium_standard_plan_rate_uuid' => 'uuid']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'premium_standard_plan_rate_uuid' => Yii::t('app', 'Premium Standard Plan Rate Uuid'),
            'price' => Yii::t('app', 'Price'),
            'gst' => Yii::t('app', 'Gst'),
            'gst_description' => Yii::t('app', 'Gst Description'),
            'total_price' => Yii::t('app', 'Total Price'),
            'option' => Yii::t('app', 'Option'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPremiumStandardPlanRateUu()
    {
        return $this->hasOne(PremiumStandardPlanRate::className(), ['uuid' => 'premium_standard_plan_rate_uuid']);
    }
}
