<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = Yii::t('app', 'Update Tutor: ' . $model->first_name." ".$model->last_name, [
    'nameAttribute' => '' . $model->first_name.' '.$model->last_name,
]);
?>
<style>
    .bootstrap-timepicker-widget table td input{
        padding:0;
    }
    .plan_error_td{
        border-top:none !important; padding:0;background-color: transparent;
    }
    .plan_error_td p{padding: 0;margin: 0}
</style>
<div class="col-xl-12 col-lg-12 col-12 col-md-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Tutor Availability</h2>
            <div class="actions panel_actions float-right">
                <!--<i class="box_toggle fa fa-chevron-down"></i>-->
                <a href="<?= Yii::$app->getUrlManager()->createUrl(['/tutor/index/']) ?>" class="btn btn-info btn-icon "><i class="fa fa-arrow-left text-white"></i> <span>Back</span></a>
            </div>
        </header>
        <div class="content-body">

            <?php $form = ActiveForm::begin(['id' => 'form-change']); ?>
            <div class="row">
                <div class="col-md-6">
                    <div class="working-plan-view provider-view" >
                        <?php
                        $arrayworking_plan = json_decode($model->working_plan);
                        ?>
                        <div>
                            <!--                            <button id="reset-working-plan" class="btn btn-primary"
                                                                title="reset_working_plan">
                                                            <span class="glyphicon glyphicon-repeat"></span>
                                                            Reset Plan
                                                        </button>-->
                            <h4 class="title float-left">Availability</h4>
                        </div>

                        <table class="working-plan table table-striped" style="    white-space: nowrap;">
                            <thead>
                                <tr>
                                    <th>Day</th>
                                    <th>Start</th>
                                    <th>End</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ((array) $arrayworking_plan as $key => $value): ?>
                                <tr>
                                        <td style="vertical-align:bottom; border-top: none">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="working_plan[<?= $key; ?>][chk]" id="breakAdd" <?= (!empty($value) ? 'checked' : '') ?> class="skin-square-purple"> <?= ucfirst($key); ?>
                                                </label>
                                            </div>
                                        </td>
                                        <td style="border-top: none;">
                                            <?php if (!empty($plan_error[$key])) { ?>
                                                <input readonly name="working_plan[<?= $key; ?>][start]" id="<?= $key; ?>-start" class="work-start form-control input-sm custom_time_picker" value='<?= date("h:i A", strtotime($_POST['working_plan'][$key]['start'])); ?>' data-template="dropdown" data-default-time="<?= date("h:i A", strtotime($value->start)); ?>" data-show-meridian="true" data-minute-step="30">
                                            <?php }else if (!empty($value)) { ?>
                                                <input readonly name="working_plan[<?= $key; ?>][start]" id="<?= $key; ?>-start" class="work-start form-control input-sm custom_time_picker" value='<?= date("h:i A", strtotime($value->start)); ?>' data-template="dropdown" data-default-time="<?= date("h:i A", strtotime($value->start)); ?>" data-show-meridian="true" data-minute-step="30">

                                            <?php } else { ?> 
                                                <input readonly name="working_plan[<?= $key; ?>][start]" id="<?= $key; ?>-start" class="work-start form-control input-sm custom_time_picker" value='<?= date("h:i A", strtotime('09:00')); ?>' data-template="dropdown" data-default-time="<?= date("h:i A", strtotime('09:00')); ?>" data-show-meridian="true" data-minute-step="30">
                                            <?php } ?> 
                                        </td> <td style="border-top: none;">
                                            <?php if (!empty($plan_error[$key])) { ?>
                                                <input readonly name="working_plan[<?= $key; ?>][end]"id="<?= $key; ?>-end" class="work-end form-control input-sm custom_time_picker" value='<?= date("h:i A", strtotime($_POST['working_plan'][$key]['end'])); ?>' data-template="dropdown" data-default-time="<?= date("h:i A", strtotime($value->end)); ?>" data-show-meridian="true" data-minute-step="30">
                                            <?php }elseif (!empty($value)) { ?>
                                                <input readonly name="working_plan[<?= $key; ?>][end]"id="<?= $key; ?>-end" class="work-end form-control input-sm custom_time_picker" value='<?= date("h:i A", strtotime($value->end)); ?>' data-template="dropdown" data-default-time="<?= date("h:i A", strtotime($value->end)); ?>" data-show-meridian="true" data-minute-step="30">
                                            <?php } else { ?> 
                                                <input readonly name="working_plan[<?= $key; ?>][end]" id="<?= $key; ?>-end" class="work-end form-control input-sm custom_time_picker" value='<?= date("h:i A", strtotime('18:00')); ?>' data-template="dropdown" data-default-time="<?= date("h:i A", strtotime('18:00')); ?>" data-show-meridian="true" data-minute-step="30">
                                            <?php } ?> 
                                        </td>
                                    </tr>
                                    <?php if(!empty($plan_error[$key])){ ?>
                                    <tr><td colspan="3" class="plan_error_td"><p class="text-danger"><?= $plan_error[$key] ?></p></td></tr>
                                    <?php }?>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-6">
                    <div>
                        <button type="button" class="add-break btn btn-primary pull-right">
                            <span class="glyphicon glyphicon-plus"></span>
                            Add Break
                        </button><h4 class="title float-left">Breaks</h4>
                    </div>
                    <br>
                    <table class="breaks table table-striped">
                        <thead>
                            <tr>
                                <th>Day</th>
                                <th>Start</th>
                                <th>End</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ((array) $arrayworking_plan as $key => $value): ?>
                                <?php
                                if (isset($value->breaks) && !empty($value->breaks)) {
                                    foreach ($value->breaks as $keyBreak => $valueBreak) {
                                        ?>
                                        <tr>
                                            <td class="break-day" style="vertical-align:bottom">

                                                <select name="breakAdd[]" id="breakAdd" class='form-control' style="width: auto;">
                                                    <option value='sunday' <?php echo ($key == 'sunday') ? 'selected' : ''; ?>>Sunday</option>
                                                    <option value='monday' <?php echo ($key == 'monday') ? 'selected' : ''; ?>>Monday</option>
                                                    <option value='tuesday' <?php echo ($key == 'tuesday') ? 'selected' : ''; ?>>Tuesday</option>
                                                    <option value='wednesday' <?php echo ($key == 'wednesday') ? 'selected' : ''; ?>>Wednesday</option>
                                                    <option value='thursday' <?php echo ($key == 'thursday') ? 'selected' : ''; ?>>Thursday</option>
                                                    <option value='friday' <?php echo ($key == 'friday') ? 'selected' : ''; ?>>Friday</option>
                                                    <option value='saturday' <?php echo ($key == 'saturday') ? 'selected' : ''; ?>>Saturday</option>
                                                </select>

                                            </td>
                                            <td  class="break-start" style="vertical-align:bottom">
                                                <?php if(!empty($break_error[$key][$keyBreak])){ ?>
                                                <input readonly name="breakAdd1[<?= $key; ?>][start][]" id="start" class="work-start form-control input-sm custom_time_picker" value="<?= date("h:i A", strtotime($_POST['breakAdd1'][$key]['start'][$keyBreak])); ?>" data-template="dropdown" data-default-time="<?= date("h:i A", strtotime($valueBreak->start)); ?>" data-show-meridian="true" data-minute-step="30">
                                                <?php }else{ ?>
                                                <input readonly name="breakAdd1[<?= $key; ?>][start][]" id="start" class="work-start form-control input-sm custom_time_picker" value="<?= date("h:i A", strtotime($valueBreak->start)); ?>" data-template="dropdown" data-default-time="<?= date("h:i A", strtotime($valueBreak->start)); ?>" data-show-meridian="true" data-minute-step="30">
                                                <?php } ?>
                                            </td>
                                            <td class="break-end" style="vertical-align:bottom">
                                                <?php if(!empty($break_error[$key][$keyBreak])){ ?>
                                                <input readonly name="breakAdd1[<?= $key; ?>][end][]" id="end" class="work-start form-control input-sm custom_time_picker" value="<?= date("h:i A", strtotime($_POST['breakAdd1'][$key]['end'][$keyBreak])); ?>" data-template="dropdown" data-default-time="<?= date("h:i A", strtotime($valueBreak->end)); ?>" data-show-meridian="true" data-minute-step="30">
                                                <?php }else{ ?>
                                                <input readonly name="breakAdd1[<?= $key; ?>][end][]" id="end" class="work-start form-control input-sm custom_time_picker" value="<?= date("h:i A", strtotime($valueBreak->end)); ?>" data-template="dropdown" data-default-time="<?= date("h:i A", strtotime($valueBreak->end)); ?>" data-show-meridian="true" data-minute-step="30">
                                                <?php } ?>
                                            </td>
                                            <td style="vertical-align:middle;text-align:center;">
                                                <a href="javascript:;" class="delete-break badge badge-info badge-md icon-lg icon-bordered icon-primary animated animated-delay-500ms rollIn" title="remove">
                                                    <span class="fa fa-times"></span>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php if(!empty($break_error[$key][$keyBreak])){ ?>
                                            <tr><td colspan="3" class="plan_error_td"><p class="text-danger"><?= $break_error[$key][$keyBreak] ?></p></td></tr>
                                        <?php }?>
                                        <?php
                                    }
                                }
                                ?> 

                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-primary pull-right']) ?>    
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </section>
</div>
