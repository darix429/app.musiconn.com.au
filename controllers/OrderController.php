<?php

namespace app\controllers;

use Yii;
use app\models\Order;
use app\models\OrderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                        [
                        'actions' => ['index'],
                        'allow' => Yii::$app->user->can('/order/index'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['history'],
                        'allow' => Yii::$app->user->can('/order/history'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['invoice-check'],
                        'allow' => Yii::$app->user->can('/order/invoice-check'),
                        'roles' => ['@'],
                    ],
//                        [
//                        'actions' => ['test'],
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
                        
			    
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    
//    public function actionView($id)
//    {
//        return $this->render('view', [
//            'model' => $this->findModel($id),
//        ]);
//    }
//
//    
//    public function actionCreate()
//    {
//        $model = new Order();
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->uuid]);
//        }
//
//        return $this->render('create', [
//            'model' => $model,
//        ]);
//    }
//
//    
//    public function actionUpdate($id)
//    {
//        $model = $this->findModel($id);
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->uuid]);
//        }
//
//        return $this->render('update', [
//            'model' => $model,
//        ]);
//    }
//
//    
//    public function actionDelete($id)
//    {
//        $this->findModel($id)->delete();
//
//        return $this->redirect(['index']);
//    }

    
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    
    public function actionHistory() { 

        $searchModel = new OrderSearch();
        $orderList = $searchModel->search(Yii::$app->request->queryParams, true);
        //echo "<pre>"; print_r($orderList); exit;
        if (Yii::$app->user->identity->role == 'STUDENT') { 
            
            $this->layout = "student_layout";
            
            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('_student_order_list', [
                            'orderList' => $orderList,
                ]);
            } else {
                return $this->render('index_student', [
                            'orderList' => $orderList,
                            'searchModel' => $searchModel,
                ]);
            }
        }else{
            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('_order_list', [
                            'orderList' => $orderList,
                ]);
            } else {
                return $this->render('index', [
                            'orderList' => $orderList,
                            'searchModel' => $searchModel,
                ]);
            }
        }
    }
    
    public function actionInvoiceCheck($id) {
        
        $return = "";
        $order = Order::find()->where(['uuid' => $id])->one();
        //$invoice_file = $order['invoice_file'];
        
        $invoicePath = Yii::$app->params['media']['invoice']['path'].$order['invoice_file'];
        
        if(empty(is_file($invoicePath))){ 
            $return = ['code' => 200, 'message' => 'Invoice not found.'];
        }
        Yii::$app->response->format = 'json';
        return $return;
    }
    
//    public function actionTest() {
//        $content = "";
//        return Yii::$app->mailer->compose('test1', ['content' => $content], ['htmlLayout' => 'layouts/html'])
//                        ->setFrom(Yii::$app->params['supportEmail'])
//                        ->setTo('rupal@vindaloovoip.com')
//                        ->setSubject('Test mail - ' . Yii::$app->name)
//                        ->send();
//    }
}
