<?php
namespace app\libraries;
use \DrewM\MailChimp\MailChimp;
use Yii;

class MailChimpApi {

	public function connect() {
		$api_key = Yii::$app->params['mailchimp']['api_key'];
		$MailChimp = new MailChimp($api_key);exit;
		return $MailChimp;
	}

	public function lists() {
		$MailChimp = MailChimpApi::connect();
		$return = array(
            'success' => '',
            'browser' => '',
            'browser_version' => '',
            'device' => '',
            'msg' => 'Sorry! No data found.'
        );

		if(isset($MailChimp)) {
			$result = $MailChimp->get('lists');
			if(isset($result['lists']) && !empty($result['lists'])) {
				$return = array(
	                'success' => 'true',
	                'data' => $result,
	                'list_id' => $result['lists'][0]['id'],
	                'msg' => 'Mailchimp List Found.'
            	);
			} else {
				$return = array(
				       	'success' => 'false',
				       	'msg' => $result['title'],
					'code' => $result['status']
				);
			}
		} else {
			$return = array(
                'success' => 'false',
                'msg' => 'Mailchimp not conencted. Please insert valid api key.'
            );
		}
		return $return;
	}

	public function insert($email, $first_name, $last_name) {
		$MailChimp = MailChimpApi::connect();
echo "dasd";exit;
		$return = array(
            'success' => '',
            'browser' => '',
            'browser_version' => '',
            'device' => '',
            'msg' => 'Sorry! No data found.'
        );

		if(isset($MailChimp)) {
			$list = MailChimpApi::lists();
			if(isset($list['list_id']) && !empty($list['list_id'])) {
				$list_id = $list['list_id'];
				$result = $MailChimp->post("lists/$list_id/members", [
					'email_address' => $email,
					'merge_fields' => array(
						'FNAME' => $first_name,
						'LNAME' => $last_name
					),
					'status'  => 'subscribed',
				]);
				if(isset($result['member_rating'])) {
					$return = array(
		                'success' => 'true',
		                'user' => $result,
		                'msg' => 'Mailchimp user created successfully.'
	            	);
				} else {
					$return = array(
		                'success' => 'false',
		                'msg' => $result['title'],
		                'code' => $result['status']
	            	);
				}
			} else {
					$return = array(
						'success' => 'false',
						'msg' => $list['msg'],
						'code' => $list['code']
					);
			}			
		} else {
			$return = array(
                'success' => 'false',
                'msg' => 'Mailchimp not connected. Please insert valid api key.'
            );
		}
		return $return;
	}
}
?>
<!-- 
	Developer: Akash Shrimali
	Task: Add data in Mailchimp when student Signup
	Date: 12-03-2020
	End
-->
