<?php

use yii\helpers\Html;
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['name'] ?>,</p>
            
            <p>Thank you for your interest in learning with Musiconn Pty Ltd.  To complete your registration process, please click on the link below:</p>

            <p><a style="background:#ff6f3d;min-height:46px;margin:10px 0;padding:0 40px;line-height:46px;border-radius:4px;font-size:18px;color:#fff;display:inline-block;" href="<?= $content['link'] ?>">Activate account</a></p>
            
            <p style="overflow-wrap: break-word; word-wrap: break-word; -ms-word-break: break-all;  word-break: break-all; word-break: break-word;-ms-hyphens: auto;-moz-hyphens: auto;-webkit-hyphens: auto;hyphens: auto;">
                <?= $content['link'] ?>
            </p>
            
            <p>Please do not reply to this unattended mailbox.  Any queries can be directed to admin@musiconn.com.au, or call us on 1300 068 742.</p>
            
            <p>Regards,<br>
            Julian Fung <br> CEO – Musiconn Pty Ltd</p>
        </td>
    </tr>
</table>
