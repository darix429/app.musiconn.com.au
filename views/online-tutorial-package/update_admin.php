<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
//rupal
use yii\web\View;
use yii\helpers\ArrayHelper;
use app\models\TutorialType;
use app\models\PaymentTerm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Admin */
$paymentTermArray = ArrayHelper::map(PaymentTerm::find()->where(['status' => 'ENABLED'])->asArray()->all(), 'uuid', 'term');
$query = PaymentTerm::find()->where(['uuid' => $model->payment_term_uuid])->one();
/*$where = "";
if (!empty($query)) {
    if ($query->term == "Monthly" || $query->term == "Quarterly") {
        $where = ['type' => 'W', 'status' => 'ENABLED'];
    } elseif ($query->term == "Fortnightly") {
        $where = ['type' => 'F', 'status' => 'ENABLED'];
    }
}*/
$tutorialTypeName = TutorialType::find()->where(['uuid' => $model->tutorial_type_uuid])->one();
//$tutorialTypeArray = (is_array($where)) ? \yii\helpers\ArrayHelper::map(TutorialType::find()->where($where)->asArray()->all(), 'uuid', 'description') : [];
$tutorialTypeArray = ArrayHelper::map(TutorialType::find()->where(['status' => 'ENABLED'])->asArray()->all(), 'uuid', 'description');

$this->title = Yii::t('app', 'Online Tutorial Package : ' . $model->name, [
            'nameAttribute' => '' . $model->name,
        ]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Online Tutorial Package'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->uuid, 'url' => ['view', 'id' => $model->uuid]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>


<style>
    /*    .input-group .form-control{
            width: 285px;
        }*/
    .semi-bold {
        font-weight: 900;
    }
</style>

<div class="col-xl-12 col-lg-12 col-12 col-md-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Please Fill Information</h2>
            <div class="actions panel_actions float-right">
                <a href="<?= Yii::$app->getUrlManager()->createUrl(['/online-tutorial-package/index/']) ?>" class="btn btn-info btn-icon "><i class="fa fa-arrow-left text-white"></i> <span>Back</span></a>
            </div>
        </header>
        <div class="content-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12">
                    <?php $form = ActiveForm::begin(); ?>
                    <div class="form-row">
                        <div class="col-md-6 mb-0">
                            <div class="form-group">
                                <?= $form->field($model, 'name')->textInput(['value' => $premiumstandardplanmodel->name, 'class' => 'form-control code', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('name')]) ?>
                            </div>
                        </div>
                        <div class="col-md-6 mb-0">
                            <div class="form-group">
                                <label class="control-label" ><?= $model->getAttributeLabel('status'); ?></label>
                                <span class="form-control bg-gray-field" aria-required="true" aria-invalid="true"><?= $model->status; ?></span>
                            </div>
                        </div>

                    </div>
                    <div class="form-row">

<!--                        <div class="col-md-6 mb-0">
                            <div class="form-group">
                                <label class="control-label" ><?php // $model->getAttributeLabel('payment_term_uuid'); ?></label>
                                <span class="form-control bg-gray-field" aria-required="true" aria-invalid="true"><?php // $query->term; ?></span>
                                ?= $form->field($model, 'payment_term_uuid')->dropDownList($paymentTermArray, ['class' => 'form-control', 'maxlength' => true, 'prompt' => "Select Payment Term"]) ?
                            </div>
                        </div>-->
                        <div class="col-md-6 mb-0">
                            <div class="form-group">
                                <label class="control-label" ><?= $model->getAttributeLabel('tutorial_type_uuid'); ?></label>
                                <span class="form-control bg-gray-field" aria-required="true" aria-invalid="true"><?= $tutorialTypeName->description; ?></span>
                                <!--?= $form->field($model, 'tutorial_type_uuid')->dropDownList($tutorialTypeArray, ['class' => 'form-control', 'maxlength' => true, 'prompt' => "Select Tutorial Type"]) ?-->
                            </div>
                        </div>
                        <div class="col-md-6 mb-0">
                            <?php
                            $items = array();
                            foreach ($packegetypeModel as $key => $value) {
                                $items[$value->uuid] = strtoupper($value->display_name);
                            }
                            ?>
                            <?= $form->field($premiumstandardplanmodel, 'online_tutorial_package_type_uuid')->dropDownList($items, ['prompt' => 'Select Plan Type', 'disabled' => 'disabled'])->label('Plan Type'); ?>
                        </div>

                    </div>


                    <div class="form-row">
                        <div class="col-md-6 mb-0">
                            <?= $form->field($model, 'description')->textarea(['value' => $premiumstandardplanmodel->description, 'class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('description')]) ?>
                        </div>
                        <!--                        <div class="col-md-6 mb-0">
                        <?php //echo  $form->field($premiumstandardplanmodel, 'gst_description')->textarea(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('gst_description')])  ?>
                                                </div>     
                        
                        -->
                        
                    </div>
                    <hr>
                    <div id="standard_plan_div" style="<?= ($premiumstandardplanmodel->plantext == "PREMIUM") ? 'display:none' : '' ?> "> 
                        <div class="form-row">                       


                            <div class="col-md-6 mb-0" >
                                <?= $form->field($premiumstandardplanmodel, 'default_option')->hiddenInput(['value' => "1"])->label(false); ?>

                                <?php // $form->field($premiumstandardplanmodel, 'default_option')->dropDownList(["1" => "Option 1 ($79/hour)", "2" => "Option 2 ($69/hour)", "3" => "Option 3 ($59/hour)", "4" => "Option 4 ($49/hour)", "5" => "Option 5 ($39/hour)"], [])->label('Default Plan Option');  ?>
                            </div>

                        </div>


                        <?php
                        if (!empty($StandardPlanOptionRateList)) {
                            ?>
                            <div class="form-row">
                                <div class="col-md-1 mb-0 semi-bold">Option</div>
                                <div class="col-md-3 mb-0 semi-bold">Price</div>
                                <div class="col-md-3 mb-0 semi-bold">Gst</div>
                                <div class="col-md-3 mb-0 semi-bold">Total Price</div>
                                <div class="col-md-2 mb-0 semi-bold">GST Description</div>

                            </div>
                            <?php foreach ($StandardPlanOptionRateList as $key => $value) { ?>
                                <div class="form-row">
                                    <div class="col-md-1 mb-0">Option <?= $value['option']; ?>
                                        <?= $form->field($premiumstandardplanmodel, 'standard_plan_options_option_' . $value['option'])->hiddenInput(['value' => $value['option']])->label(false); ?>
                                    </div>
                                    <div class="col-md-3 mb-0"><?=
                                        $form->field($premiumstandardplanmodel, 'standard_plan_options_price_' . $value['option'], ['template' => '{label}<div class="input-group"><div class="input-group-prepend" ><span class="input-group-text">$</span></div>{input}{error}</div>']
                                        )->textInput(['value' => $value['price'], 'class' => 'form-control common', 'id' => 'price_' . $value['option'], 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('price'), 'onchange' => 'myFunctionsta(' . $value['option'] . ')', 'onkeypress' => 'myFunctionsta(' . $value['option'] . ')', 'aria-label' => 'Amount (to the nearest dollar)', 'style' => 'text-align:right;width:80%;'])->label(false)
                                        ?></div>
                                    <div class="col-md-3 mb-0"> <?=
                                $form->field($premiumstandardplanmodel, 'standard_plan_options_gst_' . $value['option'], ['template' => '{label}<div class="input-group"><div class="input-group-prepend" ><span class="input-group-text">$</span></div>{input}{error}</div>']
                                )->textInput(['value' => $value['gst'], 'class' => 'form-control common', 'id' => 'gst_' . $value['option'], 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('gst'), 'onchange' => 'myFunctionsta(' . $value['option'] . ')', 'onkeypress' => 'myFunctionsta(' . $value['option'] . ')', 'aria-label' => 'Amount (to the nearest dollar)', 'style' => 'text-align:right;width:80%;'])->label(false)
                                        ?></div>
                                    <div class="col-md-3 mb-0"><?=
                                $form->field($premiumstandardplanmodel, 'standard_plan_options_total_price_' . $value['option'], ['template' => '{label}<div class="input-group"><div class="input-group-prepend" ><span class="input-group-text">$</span></div>{input}{error}</div>']
                                )->textInput(['value' => $value['total_price'], 'class' => 'form-control common', 'id' => 'total_price_' . $value['option'], 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('total_price'), 'aria-label' => 'Amount (to the nearest dollar)', 'style' => 'text-align:right;', 'readonly' => 'true'])->label(false)
                                        ?></div>
                                    <div class="col-md-2 mb-0"><?=
                                $form->field($premiumstandardplanmodel, 'standard_plan_options_gst_description_' . $value['option'], []
                                )->textInput(['value' => $value['gst_description'], 'class' => 'form-control', 'id' => 'gst_description_' . $value['option'], 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('gst_description'), 'aria-label' => 'GST Description'])->label(false)
                                        ?>
                                    </div>

                                </div>




                                <?php
                            }
                        }
                        ?>
                    </div>

                    <div id="permium_plan_div" style="<?= ($premiumstandardplanmodel->plantext == "STANDARD") ? 'display:none' : '' ?> "> 
                        <div class="form-row">
                            <div class="col-md-4 mb-0">
                                <?php
                                echo
                                $form->field($premiumstandardplanmodel, 'price', ['template' => '{label}<div class="input-group"><div class="input-group-prepend" ><span class="input-group-text">$</span></div>{input}{error}</div>']
                                )->textInput(['class' => 'form-control common', 'id' => 'price', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('price'), 'onchange' => 'myFunction()', 'onkeypress' => 'myFunction()', 'aria-label' => 'Amount (to the nearest dollar)', 'style' => 'text-align:right;width: 80%;'])
                                ?>
                            </div>
                            <div class="col-md-4 mb-0">
                                <?php
                                echo
                                $form->field($premiumstandardplanmodel, 'gst', ['template' => '{label}<div class="input-group"><div class="input-group-prepend" ><span class="input-group-text">$</span></div>{input}{error}</div>']
                                )->textInput(['class' => 'form-control common', 'id' => 'gst', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('gst'), 'onchange' => 'myFunction()', 'onkeypress' => 'myFunction()', 'aria-label' => 'Amount (to the nearest dollar)', 'style' => 'text-align:right;width: 80%;'])
                                ?>
                            </div>
                            <div class="col-md-4 mb-0">
                                <?php
                                echo
                                $form->field($premiumstandardplanmodel, 'total_price', ['template' => '{label}<div class="input-group"><div class="input-group-prepend" ><span class="input-group-text">$</span></div>{input}{error}</div>']
                                )->textInput(['class' => 'form-control common', 'id' => 'total_price', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('total_price'), 'aria-label' => 'Amount (to the nearest dollar)', 'style' => 'text-align:right;', 'readonly' => 'true'])
                                ?>
                            </div>		

                        </div>

                        <div class="form-row">

                            <div class="col-md-6 mb-0">
                                <?php echo $form->field($premiumstandardplanmodel, 'gst_description')->textarea(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('gst_description')]) ?>
                            </div>     

                        </div>
                    </div>


                    <?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-primary', 'style' => 'float: right;']) ?>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>

        </div>
    </section>
</div>

<?php
//$this->registerJsFile('http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js');
$this->registerJs(
        '
	
   	$(function() {
	  $(".common").on("input", function() {
	    match = (/(\d{0,10})[^.]*((?:\.\d{0,2})?)/g).exec(this.value.replace(/[^\d.]/g, ""));
	    this.value = match[1] + match[2];
	  });
	});

	function myFunction() { 
		var price = parseFloat("0"+$("#price").val());
		var gst = parseFloat("0"+$("#gst").val());
		var price_str = price.toString();
		var price_array = price_str.split(".");
		var price_1 = price_array[0];
		var price_2 = (price_array[1] > 0)?price_array[1]:"";
		var gst_str = gst.toString();
		var gst_array = gst_str.split(".");
		var gst_1 = gst_array[0];
		var gst_2 = (gst_array[1] > 0)?gst_array[1]:"";
		//alert(price_1.length);

		if(price_1.length <= 10 && price_2.length <= 2 && gst_1.length <= 10 && gst_2.length <= 2){
			$("#total_price").val((price + gst).toFixed(2));
		}
	} 
        
function myFunctionsta(i) { 
		var price = parseFloat("0"+$("#price_"+i).val());
		var gst = parseFloat("0"+$("#gst_"+i).val());
		var price_str = price.toString();
		var price_array = price_str.split(".");
		var price_1 = price_array[0];
		var price_2 = (price_array[1] > 0)?price_array[1]:"";
		var gst_str = gst.toString();
		var gst_array = gst_str.split(".");
		var gst_1 = gst_array[0];
		var gst_2 = (gst_array[1] > 0)?gst_array[1]:"";
		//alert(price_1.length);

		if(price_1.length <= 10 && price_2.length <= 2 && gst_1.length <= 10 && gst_2.length <= 2){
			$("#total_price_"+i).val((price + gst).toFixed(2));
		}
	} 
        
        $(document).ready(function(){
            $("#onlinetutorialpackage-payment_term_uuid").on("change",function(){ ///alert("hello");
                var payment_term = $(this).val();
                //alert(payment_term);
                $.ajax({
                    url: "' . Url::to(['/online-tutorial-package/get-tutorial-type/']) . '",
                    method: "get",
                    data: {id: payment_term},
                    //dataType: "json",
                    success: function(result){
                        if(result.code == 200){
                            $("#onlinetutorialpackage-tutorial_type_uuid").html(result.data);
                        }else{
                            showErrorMessage(result.message);
                        }
                      
                    }
                 });
                
            });
            

        });
    
', View::POS_END
);
?>
