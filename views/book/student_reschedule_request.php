<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\helpers\Url;
use app\models\StudentTuitionBooking;
use app\models\Tutor;
use app\models\User;
use app\models\RescheduleSession;
use app\models\OnlineTutorialPackage;
use app\libraries\General;

$this->title = Yii::t('app', 'RESCHEDULE LESSON');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'MY LESSONS'), 'url' => ['/booked-session/my-lessons']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_icon'] = 'my-lesson-tag.png';

$tutor_uuid = $modelBS->tutor_uuid;
$student_uuid = $modelBS->student_uuid;
$tutor_tz = General::getTutorTimezone($tutor_uuid);
$student_tz = General::getStudentTimezone($student_uuid);
$rescheduled_awaiting_entry = RescheduleSession::find()->where(['booked_session_uuid' => $modelBS->uuid, 'status' => 'AWAITING'])->one();

$model_start_hour = $model->start_hour;
$start_hour_json = '{}';
$selecteddate = General::convertSystemToUserTimezone($modelBS->start_datetime, 'Y-m-d'); //date("Y-m-d");
$model->session_when_start = $selecteddate;



$this->registerJs('

var todaydate = "' . date("Y-m-d") . '";
var selecteddate = "' . $selecteddate . '";
//alert(todaydate)
$("#new_when_startdate_div").datepicker({
    inline: true,
    //todayBtn: true,
    format: "yyyy-mm-dd",
    viewSelect: "Default",
    startDate:todaydate,
    todayHighlight:false
}).on("changeDate", function(e) {
    $("#book-session_when_start").val( e.format() );
    clear_selected_data();
    get_timeslots();
});

$("#new_when_startdate_div").datepicker("update", selecteddate);
get_timeslots();
function get_timeslots(){
    var whendate = $("#book-session_when_start").val();
    if(whendate !== ""){
        $.ajax({
            type: "GET",
            url: "' . Url::to(['book/get-timeslots-reschedule']) . '",
            data: {"selected_when_date":whendate,"bs_uuid":"' . $modelBS->uuid . '"},
            beforeSend: function() {
                blockBody();
            },
            success:function(result){
                $("#book-start_hour").html(result.html);
                $("#start_hour_array").val(JSON.stringify(result.result));
                NEW_CUSTOM.iCheck();
                unblockBody();
            },error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
    }
}

function select_time(tm){
    if(tm !== ""){
        $.ajax({
            type: "POST",
            url: "' . Url::to(['book/select-time-reschedule']) . '",
            data: {"selected_time":tm,"student_uuid":"' . $student_uuid . '","bs_uuid":"' . $modelBS->uuid . '"},
            beforeSend: function() {
                blockBody();
            },
            success:function(result){

                $("#selected_date_data_parent").html(result.html);
                $(".selected_sesson_html").val(result.html);
                $("#book-all_lesson_data").val(JSON.stringify(result.all_lesson_data));
                $("#btn_next_btn").show();
                unblockBody();
            },error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
    }
}

function clear_selected_data(){
    $("#selected_date_data_parent").html("<div><strong>No Lesson Available</strong></div>");
    $(".selected_sesson_html").val("<div><strong>No Lesson Available</strong></div>");
    $("#book-all_lesson_data").val("");
    $("#btn_next_btn").hide();
}

function reschedlue_lesson_request_cancel(id){
    if(confirm("Are you sure you want to cancel this request?")){
        $.ajax({
            type: "POST",
            url: "' . Url::to(['booked-session/student-reschedule-lesson-request-cancel']) . '?id="+id,
            data: {"is_cancel": "yes"},
            beforeSend: function() {
                blockBody();
            },
            success:function(result){
                console.log(result);
                if(result.code == 200){
                    $("#reschedlue_lesson_request_cancel_parent").html("");
                    window.location.href= "' . Url::to(['/booked-session/upcoming/']) . '";
                    showSuccess(result.message);
                }else{
                    $("#reschedlue_lesson_request_cancel_parent").html(result);
                    NEW_CUSTOM.iCheck();
                }
                unblockBody();
            },
            error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
        blockBody();
    } else {
        unblockBody();
        return false;
    }

}
function reschedule_request_action (id, action) {
    $.ajax({
        type: "POST",
        url: "' . Url::to(['booked-session/reschedule-request-action']) . '?id="+id,
        data: {"action": action},
        beforeSend: function() {
            blockBody();
        },
        success:function(result){
            console.log(result);
            if(result.code == 200){
                $("#reschedlue_lesson_request_cancel_parent").html("");
                window.location.href= "' . Url::to(['/booked-session/upcoming/']) . '";
                showSuccess(result.message);
            }else{
                $("#reschedlue_lesson_request_cancel_parent").html(result);
                NEW_CUSTOM.iCheck();
            }
            unblockBody();
        },
        error:function(e){
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
}

    ', View::POS_END);
?>
<style>
    #new_when_startdate_div{

    }
    #new_when_startdate_div .datepicker-inline{

        border: 1px solid #ccc;
        padding: 7px 0px;
        border-radius: 30px;
        min-width: 100% !important;
        max-width: 100% !important;
    }
    .datepicker::before{content:none;}
    .field-book-session_when_start, .field-book-selected_sesson_html,.field-book-all_lesson_data{margin: 0;}
</style>
<section class="content">
    <div class="row">
        <div class="panel-group box-header with-border" id="accordion" role="tablist" aria-multiselectable="true">

            <div class="col-md-4 mar-bottom-10">
                <div class="panel panel-default " style="border-radius: 35px; padding: 18px;">
                    <div class="panel-body">
                        <div class="">
                            <h4 class="mar-bottom-10 pad-bottom-10">LESSON INFORMATION</h4>
                            <div><strong>Lesson Date:</strong> <?= General::displayDate($modelBS->start_datetime); ?></div>
                            <div><strong>Lesson Time:</strong> <?= General::displayTime($modelBS->start_datetime) . ' - ' . General::displayTime($modelBS->end_datetime); ?></div>
                            <div><strong>Lesson Length:</strong> <?= $modelBS->session_min . ' Minutes'; ?></div>
                            <div><strong>Instrument:</strong> <?= $modelBS->instrumentUu->name; ?></div>
                            <div><strong>Tutor Name:</strong> <?= $modelBS->tutorUu->first_name; ?></div>
                            <div><strong>Student Name:</strong> <?= $modelBS->studentUu->first_name; ?></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-8" >

                <?php if (!empty($rescheduled_awaiting_entry) || $modelBS->is_reschedule_request == 2) { ?>
                    <div class="panel panel-default" style="border-radius: 35px; padding: 0px;">
                        <div class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingFour">
                            
                            <div class="col-md-12 box-panel-header " style="border-bottom-left-radius: 0px;border-bottom-right-radius: 0px;margin-bottom: 10px;">
                                <h4 class="col-md-6 text-uppercase">Reschedule Lesson</h4>
                                <div class="col-md-6 ">
                                    <a href="<?= Yii::$app->getUrlManager()->createUrl(['/booked-session/upcoming/']) ?>" class="box-panel-header-right-btn pull-right mar-left-10 reschedule-back-btn"><i class="fa fa-arrow-left text-white"></i> <span>Back</span></a>
                                </div>
                            </div>

                            <div class="panel-body" style="padding: 20px;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4 class="mar-bottom-10 pad-bottom-10 text-uppercase">Reschedule Request Information</h4>
                                    </div>
                                    <div class="col-md-7">
                                        <div><strong>Lesson Date:</strong> <?= General::displayDate($rescheduled_awaiting_entry->to_start_datetime); ?></div>
                                        <div><strong>Lesson Time:</strong> <?= General::displayTime($rescheduled_awaiting_entry->to_start_datetime) . ' - ' . General::displayTime($rescheduled_awaiting_entry->to_end_datetime); ?></div>
                                        <div><strong>Requested Date:</strong> <?= General::displayDateTime($rescheduled_awaiting_entry->created_at); ?></div>
                                        <div><strong>Requested By:</strong> <?= $rescheduled_awaiting_entry->request_by; ?></div>
                                    </div>
                                    <div class="col-md-5" style="">
                                        <?php if ($modelBS->is_reschedule_request == 2){ ?>
                                            <div  id="reschedlue_lesson_request_cancel_parent" class="mar-top-10">
                                                <button type="button" class="btn btn-success btn-md btn-rounded " onclick="reschedule_request_action('<?= $rescheduled_awaiting_entry->uuid ?>', 'approve');" style="white-space: break-spaces;">Approve</button>
                                                <button type="button" class="btn btn-danger btn-md btn-rounded " onclick="reschedule_request_action('<?= $rescheduled_awaiting_entry->uuid ?>', 'reject');" style="white-space: break-spaces;">Reject</button>
                                            </div>
                                        <?php } else if (Yii::$app->user->identity->role == 'STUDENT') { ?>
                                            <div  id="reschedlue_lesson_request_cancel_parent" class="mar-top-10">
                                                <button type="button" class="btn btn-danger btn-md btn-rounded " onclick="reschedlue_lesson_request_cancel('<?= $modelBS->uuid ?>');" style="white-space: break-spaces;">Cancel Reschedule Lesson Request</button>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                <?php } else { 
                    
                    $form = ActiveForm::begin([
                        'action' => ['reschedule-request', 'id' => $modelBS->uuid],
                        'id' => 'frm_step_4',
                        'enableClientValidation' => true,
                        'validateOnChange' => true,
                        'options' => ['class' => 'form-horizontal']]);
                ?>
                    <div class="panel panel-default" style="border-radius: 35px; padding: 0px;">
                        
                        <div class="col-md-12 box-panel-header " style="border-bottom-left-radius: 0px;border-bottom-right-radius: 0px;margin-bottom: 10px;">
                            <h4 class="col-md-6 text-uppercase">Reschedule Lesson</h4>
                            <div class="col-md-6 ">
                                <a href="<?= Yii::$app->getUrlManager()->createUrl(['/booked-session/upcoming/']) ?>" class="box-panel-header-right-btn pull-right mar-left-10 reschedule-back-btn"><i class="fa fa-arrow-left text-white"></i> <span>Back</span></a>
                            </div>
                        </div>

                        <!-- SELECT DATE AND TIME -->
                        <div class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingFour" style="padding:10px;">
                            <div class="panel-body" style="padding:0;">
                                <div class="row">
                                    
                                    <!-- Date selection -->
                                    <div class="col-md-6">
                                    <div id="new_when_startdate_div"></div>
                                        <?php echo $form->field($model, 'session_when_start')->hiddenInput(['class' => 'form-control session_when_start', 'maxlength' => true,])->label(false) ?>

                                        <ul class="products-list product-list-in-box">
                                            <li>                                                    
                                                <?php
                                                    $model->selected_sesson_html = (empty($model->selected_sesson_html)) ? "<div><strong>No Lesson Available</strong></div>" : $model->selected_sesson_html;
                                                    echo $form->field($model, 'selected_sesson_html')->hiddenInput(['class' => 'form-control selected_sesson_html', 'maxlength' => true,])->label(false);
                                                    $displayNextBtn = (!empty($model->start_hour)) ? "block" : "none";
                                                ?>
                                                <div class="sst_btn sst_btn2 text-center" >
                                                    <div class="" id="selected_date_data_parent">
                                                        <?= $model->selected_sesson_html; ?>
                                                    </div>
                                                    <br>
                                                    <button id="btn_next_btn" style="cursor: pointer; padding-top: 4px; display:<?= $displayNextBtn ?>;" class="label label-warning custom-hm-1 ">SAVE LESSON</button>
                                                </div>
                                            </li>
                                        </ul>

                                        <?php echo $form->field($model, 'all_lesson_data')->hiddenInput(['class' => 'form-control all_lesson_data', 'maxlength' => true,])->label(false) ?>
                                    </div>

                                    <!-- Time selection -->
                                    <div class="col-md-6">
                                        <div style="border: 1px solid #ccc; border-radius: 30px;">

                                            <div class="col-md-12" style="margin-top: 10px;"><div class="sst_btn">Select Start Time</div><br></div>
                                            
                                            <div class="row">
                                                <div class="col-md-12">

                                                    <?php
                                                    $start_hour_array = json_decode($start_hour_json, true);
                                                    $start_hour_array = (!empty($start_hour_array)) ? $start_hour_array : [0 => 'dummy'];
                                                    echo $form->field($model, 'start_hour', ['template' => '{input}<div class="col-md-12">{error}</div>', 'options' => ['tag' => false]], ['class' => 'dasdasd'])
                                                            ->radioList(
                                                                $start_hour_array, [
                                                                'item' => function($index, $label, $name, $checked, $value) use($model_start_hour, $start_hour_array) {

                                                                    if (count($start_hour_array) == 1 && $value == 'dummy') {
                                                                        return '<div class="col-md-12 text-center">Please select the date.</div>';
                                                                    }
                                                                    $checked_val = ($model_start_hour == $value) ? 'checked="checked"' : "";

                                                                    $html = '<div class="col-md-6 col-sm-3">';
                                                                    $html .= '<ul class="products-list product-list-in-box">';
                                                                    $html .= '<li class="st-p3">';
                                                                    $html .= '<label class="contai_r">';
                                                                    $html .= '<input type="radio" onclick="select_time(\'' . $value . '\');" value="' . $value . '" name="' . $name . '" ' . $checked_val . '>';
                                                                    $html .= '<span class="checkmark"></span>';
                                                                    $html .= '</label>';
                                                                    $html .= '<span class="mn_3">' . $label . '</span>';
                                                                    $html .= '</li>';
                                                                    $html .= '</ul>';
                                                                    $html .= '</div>';
                                                                    return $html;
                                                                }
                                                                    ]
                                                            )
                                                            ->label(false);
                                                    ?>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <!-- Time selection end -->

                                </div>
                            </div>
                        </div>

                    </div>
                    <!--END SELECT DATE AND TIME -->
                <?php 
                    ActiveForm::end(); 
                    } 
                ?>
            </div>

        </div>

    </div>
</section>