<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TutorStandardRateHistory;

/**
 * TutorStandardRateHistorySearch represents the model behind the search form of `app\models\TutorStandardRateHistory`.
 */
class TutorStandardRateHistorySearch extends TutorStandardRateHistory {

    public $firstName;
    public $lastName;
    public $tutorName;
    public $instrumentName;

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
                [['uuid', 'tutor_uuid', 'instrument_uuid', 'old_option', 'option', 'datetime', 'changed_by_uuid', 'changed_by_role', 'action', 'change_tutor_category', 'online_tutorial_package_type_uuid', 'tutor_category_history_uuid', 'tutorName', 'instrumentName', 'old_online_tutorial_package_type_uuid', 'firstName', 'lastName'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $modeDatatable = false, $statusnot = '', $orderBy = 'datetime desc') {
        $query = TutorStandardRateHistory::find()->joinWith(['instrumentUu', 'onlineTutorialPackageTypeUu', 'tutorUu']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($this->tutorName)) {
            $query->andFilterWhere(['ilike', "concat(tutor.first_name,'',tutor.last_name)", $this->tutorName]);
        }

        if (!empty($this->instrumentName)) {
            $query->andFilterWhere(['ilike', "instrument.name", $this->instrumentName]);
        }



        if (!is_null($this->datetime) &&
                strpos($this->datetime, ' - ') !== false) {
            list($start_date, $end_date) = explode(' - ', $this->datetime);


            $query->andFilterWhere(['>=', '"tutor_standard_rate_history"."datetime"', date("Y-m-d", strtotime($start_date)) . " 00:00:00"]);
            $query->andFilterWhere(['<=', '"tutor_standard_rate_history"."datetime"', date("Y-m-d", strtotime($end_date)) . " 23:59:59"]);
        }


        $query->andFilterWhere(['ilike', 'uuid', $this->uuid])
                ->andFilterWhere(['ilike', 'tutor_uuid', $this->tutor_uuid])
                ->andFilterWhere(['ilike', 'instrument_uuid', $this->instrument_uuid])
                ->andFilterWhere(['ilike', 'old_option', $this->old_option])
                ->andFilterWhere(['ilike', 'option', $this->option])
                ->andFilterWhere(['ilike', 'changed_by_uuid', $this->changed_by_uuid])
                ->andFilterWhere(['ilike', 'changed_by_role', $this->changed_by_role])
                ->andFilterWhere(['ilike', 'action', $this->action])
                ->andFilterWhere(['ilike', 'change_tutor_category', $this->change_tutor_category])
                ->andFilterWhere(['ilike', 'online_tutorial_package_type_uuid', $this->online_tutorial_package_type_uuid])
                ->andFilterWhere(['ilike', 'tutor_category_history_uuid', $this->tutor_category_history_uuid]);
        if ($modeDatatable) {
            $result = $query->asArray()->orderBy($orderBy)->all();
            return $result;
        }
        return $dataProvider;
    }
    
    
    public function searchPlanChange($params, $modeDatatable = false, $statusnot = '', $orderBy = 'datetime desc') {
        
        $query = TutorStandardRateHistory::find()->joinWith(['instrumentUu', 'onlineTutorialPackageTypeUu', 'tutorUu']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($this->firstName)) {
            $query->andFilterWhere(['ilike', "tutor.first_name", $this->firstName]);
        }
        if (!empty($this->lastName)) {
            $query->andFilterWhere(['ilike', "tutor.last_name", $this->lastName]);
        }

        if (!empty($this->instrumentName)) {
            $query->andFilterWhere(['ilike', "instrument.name", $this->instrumentName]);
        }

        if (!is_null($this->datetime) &&
                strpos($this->datetime, ' - ') !== false) {
            list($start_date, $end_date) = explode(' - ', $this->datetime);


            $query->andFilterWhere(['>=', '"tutor_standard_rate_history"."datetime"', date("Y-m-d", strtotime($start_date)) . " 00:00:00"]);
            $query->andFilterWhere(['<=', '"tutor_standard_rate_history"."datetime"', date("Y-m-d", strtotime($end_date)) . " 23:59:59"]);
        }
        
        $query->andFilterWhere([
                    'old_online_tutorial_package_type_uuid' => $this->old_online_tutorial_package_type_uuid,
                    'old_option' => $this->old_option,
                    'tutor_standard_rate_history.online_tutorial_package_type_uuid' => $this->online_tutorial_package_type_uuid,
                    'option' => $this->option,
                ]);
        
        if ($modeDatatable) {
            $result = $query->asArray()->orderBy($orderBy)->all();
            //echo $query->createCommand()->getRawSql();  exit;
            return $result;
        }
        return $dataProvider;
    }

}
