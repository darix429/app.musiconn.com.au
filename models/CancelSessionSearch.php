<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CancelSession;

/**
 * CancelSessionSearch represents the model behind the search form of `app\models\CancelSession`.
 */
class CancelSessionSearch extends CancelSession
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'booked_session_uuid', 'student_uuid', 'tutor_uuid', 'cancel_by', 'canceler_uuid', 'approved_by', 'approver_uuid', 'approve_datetime', 'rejected_by', 'rejecter_uuid', 'reject_datetime', 'status', 'cancel_note', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $modeDatatable = false, $statusnot = '')
    {
        $query = CancelSession::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($statusnot)) {
            if (is_array($statusnot)) {

                $query->andWhere(['not in', 'status', $statusnot]);
            } else {

                $query->andWhere(['<>', 'status', $statusnot]);
            }
        }

        $query->andFilterWhere(['status' => $this->status]);

        // grid filtering conditions
        $query->andFilterWhere([
            'approve_datetime' => $this->approve_datetime,
            'reject_datetime' => $this->reject_datetime,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['ilike', 'uuid', $this->uuid])
            ->andFilterWhere(['ilike', 'booked_session_uuid', $this->booked_session_uuid])
            ->andFilterWhere(['ilike', 'student_uuid', $this->student_uuid])
            ->andFilterWhere(['ilike', 'tutor_uuid', $this->tutor_uuid])
            ->andFilterWhere(['ilike', 'cancel_by', $this->cancel_by])
            ->andFilterWhere(['ilike', 'canceler_uuid', $this->canceler_uuid])
            ->andFilterWhere(['ilike', 'approved_by', $this->approved_by])
            ->andFilterWhere(['ilike', 'approver_uuid', $this->approver_uuid])
            ->andFilterWhere(['ilike', 'rejected_by', $this->rejected_by])
            ->andFilterWhere(['ilike', 'rejecter_uuid', $this->rejecter_uuid])
            ->andFilterWhere(['ilike', 'status', $this->status])
            ->andFilterWhere(['ilike', 'cancel_note', $this->cancel_note]);
        if ($modeDatatable) {
            $result = $query->asArray()->orderBy('created_at DESC')->all();
            return $result;
        }
        return $dataProvider;
    }
}
