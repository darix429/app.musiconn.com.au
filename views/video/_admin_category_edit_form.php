<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\ArrayHelper;
use app\models\InstrumentCategory;
use app\models\Instrument;
use yii\helpers\Url;

$categoryArray = ArrayHelper::map(InstrumentCategory::find()->asArray()->all(), 'uuid', 'name');
//$instrumentArray = ArrayHelper::map(Instrument::find()->where(['status' => 'ACTIVE'])->asArray()->all(), 'uuid', 'name');
?>

<?php
//echo "<pre>";
//print_r($model->getErrors());
//echo "</pre>";
?>
<div class="row form-open" id="categoryUpdateDiv" style=" border-top-style: ridge;">
    <div class="col-lg-12 col-md-12 col-12">
        <div class="form-row">
            <h3 class="title float-left"><b>Update Recorded Video</b></h3>
            <div class="col-md-12 mb-0">
                <?php
                $form = ActiveForm::begin(['options' => ['id' => 'category_form', 'enableClientValidation' => true, 'enableAjaxValidation' => true, 'validateOnChange' => true,]]);
                ?>
                <div class="form-row">
                    <div class="col-md-6 mb-0">
                        <?= $form->field($model, 'instrument_category_uuid')->dropDownList($categoryArray, ['class' => 'form-control', 'prompt' => 'Select Category',]) ?>
                    </div>
                    <div class="col-md-6 mb-0">
                        <div class="form-group ">
                            <label class="control-label" for=""></label>
                <?= Html::Button(Yii::t('app', 'Update'), ['id' => 'categorySubmitbutton', 'class' => 'btn btn-primary', 'style' => 'margin-top:30px;']) ?>
                        </div>
                    </div>
                </div>


                <?php ActiveForm::end(); ?>                            
            </div>
        </div>
    </div>
</div>