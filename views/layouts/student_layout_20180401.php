<?php
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/js/jquery-3.2.1.min.js');

$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/datatables/js/datatables.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/datatables/extensions/Buttons/js/dataTables.buttons.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/datatables/extensions/Buttons/js/buttons.flash.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/datatables/extensions/Buttons/js/buttons.html5.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/datatables/extensions/Buttons/js/buttons.print.min.js');


$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/messenger/js/messenger.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/messenger/js/messenger-theme-future.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/messenger/js/messenger-theme-flat.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/js/messenger.js');

//Calender
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/calendar/moment.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/jquery-ui/smoothness/jquery-ui.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/calendar/fullcalendar.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/js/jquery.form.js');

$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/jquery-validation/js/jquery.validate.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/js/form-validation.js');

foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
    if ($key == 'success') {
        $this->registerJs("showSuccess('$message');");
    } else {
        $this->registerJs("showErrorMessage('$message');");
    }
}

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\NewAppAsset;
use yii\helpers\Url;
use yii\web\View;
use app\models\Tutor;
use app\models\Student;

NewAppAsset::register($this);

$page_length = (Yii::$app->params['datatable']['page_length'] > 0 ) ? Yii::$app->params['datatable']['page_length'] : 10;
$this->registerJs("
        var dt_page_length = " . $page_length . ";
        var web_url = " . Url::to(['/']) . ";
       ", View::POS_HEAD);


$this->registerJs(
        '
// Global form submit loader display code
$("form").on("beforeValidate", function (event, messages, deferreds) {
    blockBody("Loading...");

    // called when the validation is triggered by submitting the form
    // return false if you want to cancel the validation for the whole form

}).on("afterValidate", function (event, messages,errorAttributes) {
    
    if (errorAttributes.length) {
    
        unblockBody();
    }

}).on("beforeSubmit", function () {
    blockBody("Loading...");
   //return false;
    // after all validations have passed
    // you can do ajax form submission here
    // return false if you want to stop form submission
});

', yii\web\View::POS_END
);


$profile_url = Url::to(['student/profile']);

$currentController = Yii::$app->controller->id;
$currentAction = Yii::$app->controller->action->id;
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>

        <link rel="shortcut icon" href="/theme_assets/images/favicon.ico" type="image/x-icon" /><!-- Favicon -->
        <link rel="apple-touch-icon-precomposed" href="/theme_assets/images/apple-touch-icon-57-precomposed.png"><!-- For iPhone -->
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/theme_assets/images/apple-touch-icon-114-precomposed.png">   <!-- For iPhone 4 Retina display -->
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/theme_assets/images/apple-touch-icon-72-precomposed.png"><!-- For iPad -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/theme_assets/images/apple-touch-icon-144-precomposed.png"><!-- For iPad Retina display -->

        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>
        
        <?php echo Html::beginForm(['/site/logout'], 'post', ['id' => 'logoutfrm']) ?>
<!--                                        <a href="javascript:$('#logoutfrm').submit();">
                                            <i class="fa fa-lock"></i>
                                            Logout
                                        </a>-->
        <?php echo Html::endForm() ?>
        <!-- START TOPBAR -->
        
        <!-- Loader code -->
        <div id="preloader" style="display: block;">
            <div id="loader"></div>
            <p id="loader_message"></p>
        </div>

        <div class="left-sidebar-pro">
            <nav id="sidebar" class="">
                <div class="musicco-profile">
                    <div class="profile-dtl">
                        <a href="#"><img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new/'?>img/profile.png" alt="" /></a>
                        <h2><?= substr(Yii::$app->user->identity->first_name . ' ' . Yii::$app->user->identity->last_name,0,20) ?></h2>
                        <p class="min-dtn"><?= Yii::$app->user->identity->role; ?></p>
                    </div>
                </div>
                <div class="left-custom-menu-adp-wrap comment-scrollbar">
                    <nav class="sidebar-nav left-sidebar-menu-pro">
                        <ul class="metismenu" id="menu1">
                            
                            
                            
                            <li class="<?= ( 
                                        ($currentController == 'student' && in_array($currentAction ,['profile',]) )
                                ) ? 'active' : '' ?>">
                                <a class="" href="<?= Yii::$app->getUrlManager()->createUrl('/student/profile'); ?>">
                                    <i class="fa fa-edit"></i><span class="mini-click-non"> MY PROFILE</span>
                                </a>
                            </li>
                            
                            <li class="<?= ( 
                                        ($currentController == 'booked-session' && in_array($currentAction ,['upcoming','completed','cancelled','credited']) )
                                ) ? 'active' : '' ?>">
                                <a class="" href="<?= Yii::$app->getUrlManager()->createUrl('/booked-session/upcoming'); ?>">
                                    <i class="fa fa-book"></i><span class="mini-click-non"> MY LESSONS</span>
                                </a>
                            </li>
                            
                            <li class="<?= ( 
                                        ($currentController == 'video' && in_array($currentAction ,['student-video-list', 'review']) )
                                ) ? 'active' : '' ?>">
                                <a class="" href="<?= Yii::$app->getUrlManager()->createUrl('/video/student-video-list'); ?>">
                                    <i class="fa fa-play-circle-o"></i><span class="mini-click-non"> VIDEO LIBRARY</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <div class="logo_bottom">
                        <img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new/'?>img/logo.png" alt="" />
                    </div>
                </div>

            </nav>
        </div>
        
        <!-- Start Welcome area -->
        <div class="all-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="logo-pro">
                            <a href="index.html"><img class="main-logo" src="<?= Yii::$app->request->baseUrl . '/theme_assets/new/'?>img/brand.png" alt="" /></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-advance-area">
                <div class="header-top-area">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-7 col-md-0 col-sm-7 col-xs-12">
                                <div class="menu-switcher-pro">
                                    <div id="sidebarCollapse" class="">
                                        <i class="fa fa-list"></i><span>Welcome <?= substr(Yii::$app->user->identity->first_name . ' ' . Yii::$app->user->identity->last_name,0,20) ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                <div class="header-right-info">
                                    <ul class="nav navbar-nav mai-top-nav header-right-menu">
                                        <li class="nav-item">
                                            <a href="javascript:$('#logoutfrm').submit();"  class="nav-link dropdown-toggle">
                                                <i class="fa fa-power-off"></i>
                                                <span class="admin-name">Log Out</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <?php if(Yii::$app->session->get('user.idbeforeswitch.admin') && Yii::$app->session->get('user.idbeforeswitch.admin') > 0 && in_array(Yii::$app->user->identity->role, ['STUDENT']) ){ ?>
                                    <ul class="nav navbar-nav mai-top-nav header-right-menu">
                                        <li class="nav-item">
                                            <a href="<?= Url::to(['/admins/back-to-admin/'])?>"  class="nav-link dropdown-toggle">
                                                <i class="fa fa-level-up"></i>
                                                <span class="admin-name">Back To <?= ucfirst(strtolower(Yii::$app->session->get('user.idbeforeswitch.admin.role')));?></span>
                                            </a>
                                        </li>
                                    </ul>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Mobile Menu start -->
                <div class="mobile-menu-area">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="mobile-menu">
                                    <nav id="dropdown">
                                        <ul class="mobile-menu-nav">
                                            <li class="active"><a class="" href="index.html"><i class="fa fa-edit"></i><span class="mini-click-non"> MY PROFILE</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="" href="mailbox.html" aria-expanded="false"><i class="fa fa-book"></i> <span class="mini-click-non">MY LESSONS</span></a>
                                            </li>
                                            <li>
                                                <a class="" href="mailbox.html" aria-expanded="false"><i class="fa fa-play-circle-o"></i> <span class="mini-click-non">VIDEO LIBRARY</span></a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Mobile Menu end -->
                <div class="breadcome-area">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="breadcome-list">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                            <div class="breadcomb-wp">
                                                <div class="breadcomb-icon">
                                                    <i class="<?php echo (!empty($this->params['page_icon'])) ? $this->params['page_icon'] : 'fa fa-user'; ?>"></i>
                                                </div>
                                                <div class="breadcomb-ctn">
                                                    <h1><?= $this->title; ?></h1>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- START CONTAINER -->
            <?= $content ?>
            <!-- END CONTAINER -->

        </div>
        <!-- End Welcome area -->

        

        <!-- modal start -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="ultraModal-Label" aria-hidden="true">
            <div class="modal-dialog animated zoomIn">
                <div class="modal-content">
                    <div class="modal-header">
                        <div id="title-demo"></div>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div id="video-content"></div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <!--<button class="btn btn-success" type="button">Save changes</button>-->
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="tutorUnavailability" tabindex="-1" role="dialog" aria-labelledby="ultraModal-Label" aria-hidden="true">
            <div class="modal-dialog animated zoomIn">
                <div class="modal-content">
                    <div class="modal-header">
                        <div id="title-tutorUnavailability"></div>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div id="unavailability-content"></div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="tutorAvailabilityCalender" tabindex="-1" role="dialog" aria-labelledby="ultraModal-Label" aria-hidden="true">
            <style>#tutorAvailabilityCalender-content > section.box { margin: 0;}</style>
            <div class="modal-dialog animated zoomIn " style="max-width:90%;">
                <div class="modal-content border-0">
                    <div class="modal-header bg-primary">
                        <h4 id="title-tutorAvailabilityCalender" class="modal-title bg-primary"></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div id="tutorAvailabilityCalender-content"></div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="studentBookingOption" tabindex="-1" role="dialog" aria-labelledby="ultraModal-Label" aria-hidden="true">
            <div class="modal-dialog animated zoomIn">
                <div class="modal-content">
                    <div class="modal-header bg-orange" style="    border-radius: 0;">
                        <h4 class="text-white" id="title-studentBookingOption">Choose your booking option</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div id="studentBookingOption-content" class="modal-body">

                        <div class="card credit_card_custom_css">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-6 text-center" style="border-right: 1px solid #bbb8b8;">
                                        <p> </p>
                                        <a class="btn btn-purple btn-icon redirect_booking_btn" href="javascript:void(0)" data-href="<?= Yii::$app->params['base_url'] . 'book-music-tution'; ?>" >
                                            Book a New Lesson
                                        </a>
                                    </div>
                                    <div class="col-lg-6 text-center" >
                                        <p> </p>
                                        <a class="btn btn-orange btn-icon redirect_booking_btn" href="javascript:void(0)" data-href="<?= Yii::$app->params['base_url'] . 'book-music-tution/credit-schedule'; ?>" >
                                            Book a Credit Lesson
                                        </a>
                                        <!--<div class="clearfix"></div>-->
                                        <p>You have <span id="model_credit_session_count">0</span> credit Lessons.</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- modal end -->
        

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
