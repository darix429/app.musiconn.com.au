<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "coupon_log".
 *
 * @property string $uuid
 * @property string $coupon_uuid
 * @property string $order_uuid
 * @property string $booking_uuid
 * @property string $booking_id
 * @property string $student_uuid
 * @property string $tutor_uuid
 * @property string $instrument_uuid
 * @property string $booking_datetime
 * @property string $total_price
 * @property string $discount
 * @property string $grand_total
 */
class CouponLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'coupon_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'coupon_uuid', 'order_uuid', 'booking_uuid', 'booking_id', 'student_uuid', 'tutor_uuid', 'instrument_uuid'], 'string'],
            [['coupon_uuid', 'booking_id', 'student_uuid', 'tutor_uuid', 'instrument_uuid', 'booking_datetime'], 'required'],
            [['booking_datetime'], 'safe'],
            [['total_price', 'discount', 'grand_total'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uuid' => 'Uuid',
            'coupon_uuid' => 'Coupon Uuid',
            'order_uuid' => 'Order Uuid',
            'booking_uuid' => 'Booking Uuid',
            'booking_id' => 'Booking ID',
            'student_uuid' => 'Student Uuid',
            'tutor_uuid' => 'Tutor Uuid',
            'instrument_uuid' => 'Instrument Uuid',
            'booking_datetime' => 'Booking Datetime',
            'total_price' => 'Total Price',
            'discount' => 'Discount',
            'grand_total' => 'Grand Total',
        ];
    }
}
