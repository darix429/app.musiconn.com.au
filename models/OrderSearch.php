<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Order;

/**
 * OrderSearch represents the model behind the search form of `app\models\Order`.
 */
class OrderSearch extends Order
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'order_id', 'student_uuid', 'order_datetime', 'invoice_file'], 'safe'],
            [['total', 'discount', 'grand_total'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $modeDatatable = false, $statusnot = '')
    {
        $query = Order::find();
        $query->joinWith(['paymentTransactionUu']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            "to_char(order_datetime, 'DD-MM-YYYY')" => $this->order_datetime,
            'total' => $this->total,
            'discount' => $this->discount,
            'grand_total' => $this->grand_total,
        ]);
        
        if (Yii::$app->user->identity->role == 'STUDENT') { 
            $query->andFilterWhere(['order.student_uuid' => Yii::$app->user->identity->student_uuid]);
        }

        $query->andFilterWhere(['ilike', 'order.uuid', $this->uuid])
            ->andFilterWhere(['ilike', 'order_id', $this->order_id])
            //->andFilterWhere(['ilike', 'student_uuid', $this->student_uuid])
            ->andFilterWhere(['ilike', 'invoice_file', $this->invoice_file]);
        //echo $query->createCommand()->getRawSql();exit;
        
        if ($modeDatatable) {
            $result = $query->asArray()->orderBy('order_datetime DESC')->all();
            return $result;
        }
        return $dataProvider;
    }
}
