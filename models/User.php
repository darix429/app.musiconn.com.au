<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property string $role
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string $admin_uuid
 * @property string $tutor_uuid
 * @property string $student_uuid
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    public $confirmPassword;

    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'password_hash', 'email', 'created_at', 'updated_at', 'role', 'referer_source'], 'required'],
            [['username', 'role', 'first_name', 'last_name', 'admin_uuid', 'tutor_uuid', 'student_uuid', 'referer_source'], 'string'],
            [['status', 'created_at', 'updated_at', 'timezone'], 'default', 'value' => null],
            [['referer_source'], 'default', 'value'=>'N/A'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['auth_key'], 'string', 'max' => 32],
            [['password_hash', 'password_reset_token', 'email','confirmPassword'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 20],

            [['username','email'], 'unique'],
            [['email','username'], 'email'],
            [['password_hash'], 'string', 'min' => 6, 'max' => 64],
            [['password_hash'], 'match','on'=>['signup','admin_create'], 'pattern' => '/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d!@#$%^&*+_-]{6,}$/', 'message' => 'Password must contain upper and lower case letters and at least 1 number. (eg. TeSt&12)'],
            [['confirmPassword'], 'required','on'=>['signup','admin_create']],
            ['confirmPassword', 'compare', 'compareAttribute'=>'password_hash', 'skipOnEmpty' => false,'message'=>"Passwords don't match" ,'on'=>['signup','admin_create']],
            [['username','email'], 'unique','on'=>['admin_profile','admin_update','student_profile','tutor_create','tutor_update']]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'password_hash' => Yii::t('app', 'Password'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'email' => Yii::t('app', 'Email'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'role' => Yii::t('app', 'Role'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'phone' => Yii::t('app', 'Phone'),
            'admin_uuid' => Yii::t('app', 'Admin Uuid'),
            'tutor_uuid' => Yii::t('app', 'Tutor Uuid'),
            'student_uuid' => Yii::t('app', 'Student Uuid'),
      	    'confirmPassword' => Yii::t('app', 'Confirm Password'),
            'timezone' => Yii::t('app', 'Timezone'),
            'referer_source' => Yii::t('app', 'How did you hear about us?')
        ];
    }

    public function getAdmin()
    {
        return $this->hasOne(Admin::className(), ['admin_uuid' => 'admin_uuid']);
    }

    public function getStudent()
    {
        return $this->hasOne(Student::className(), ['student_uuid' => 'student_uuid']);
    }

    public function getTutor()
    {
        return $this->hasOne(Tutor::className(), ['uuid' => 'tutor_uuid']);
    }

    /****** Dashboard Functions **********/
    public function enabledTutorCount()
    {
        return Tutor::find()->where(['status' => 'ENABLED'])->count();
    }

    public function disabledTutorCount()
    {
        return Tutor::find()->where(['status' => 'DISABLED'])->count();
    }

    public function enabledStudentCount()
    {
        return Student::find()->where(['status' => 'ENABLED'])->count();
    }
    public function disabledStudentCount()
    {
        return Student::find()->where(['status' => 'DISABLED'])->count();
    }

    public function scheduleTutionCount()
    {
        return BookedSession::find()->where(['status' => 'SCHEDULE'])->count();
    }

    public function completedTutionCount()
    {
        return BookedSession::find()->where(['status' => 'COMPLETED'])->count();
    }
    public function publishedTutionCount()
    {
        return BookedSession::find()->where(['status' => 'PUBLISHED'])->count();
    }

    public function ongoingTutionCount()
    {
        return BookedSession::find()->where(['status' => 'INPROCESS'])->count();
    }

    public function publishedVideoCount()
    {
        return PreRecordedVideo::find()->where(['status' => 'PUBLISHED'])->count();
    }

    public function uploadedVideoCount()
    {
        return PreRecordedVideo::find()->where(['status' => 'UPLOADED'])->count();
    }

    public function isFreeVideoCount()
    {
        return PreRecordedVideo::find()->where(['status' => 'PUBLISHED', 'is_free' => 1])->count();
    }

    public function inactiveStudentCount()
    {
        return Student::find()->where(['status' => 'INACTIVE'])->count();
    }
    
    public static function loginUserProfileCharecter()
    {
        if (Yii::$app->getUser()->isGuest) {
            return 'NA';
        }else{
            $f = strtoupper(substr(Yii::$app->user->identity->first_name,0,1));
            $l = strtoupper(substr(Yii::$app->user->identity->last_name,0,1));
            return $f.$l;
        }
    }

}
