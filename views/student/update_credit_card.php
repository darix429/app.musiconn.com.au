<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\libraries\General;
?>

<div class="modal-body">
    <div class="row vertical-center-box vertical-center-box-tablet" >
        <?php $form = ActiveForm::begin(['id' => 'form-create-card']); ?>
        
        <label class="cont_title_add">These credit card details will be used for subscription and lesson payments</label>
        <div class="form-group">
            <span><label class="control-label" for="studentcreditcard-name">Card Holder Name*</label></span>
            <span type="text" class="form-control" disabled="disabled"><?= $creditCard['name']; ?></span>
        </div>
        <div class="form-group">
            <span><label class="control-label" for="studentcreditcard-number">Card Number*</label></span>
            <span type="text" class="form-control bg-gray-field" disabled="disabled">
                <?php
                $number = General::decrypt_str($creditCard['number']);
                echo General::ccMasking($number);
                ?>
            </span>
        </div>
        <div class="padd_zero col-md-5 ">
            <span><label class="control-label" for="studentcreditcard-cc_month_year">Expiry*</label></span>
            <span type="text" class="form-control" disabled="disabled"><?= date("Y-m", strtotime($creditCard['expire_date'])); ?></span>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<div class="modal-footer">
    <button type="button" id="" class="btn btn-primary delete_credit_card" data-uuid="<?= $creditCard['uuid']; ?>">Remove Card</button>
</div>