<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Category Demo');
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs(
        '
    function videoShow(id){
        var url = "' . Url::to(['test/get-videos-from-category']) . '?id="+id;
        $.ajax({
            type: "GET",
            url: url,
            beforeSend: function() {
                blockBody();
            },
            success:function(data){
                var url = $("#video_search").attr("href");
                console.log("ID: " +id);
                //$(".category_video #"+id).show();
                console.log(url);
                // if(id == url){
                //     // cosole.log("ID: " +id);
                //     // console.log($(this)[0])
                //     // console.log($(this)[0].url);
                //     console.log("if");
                //     $(".category_video #myTab5").show();
                // } else {
                //     console.log("else");
                //     $(".category_video #myTab5").hide();
                // }
                //console.log("Hidden: " + $(".hidden").length);
                var category = $(".hidden").children(".category_name").text();
                var category_uuid = $(".hidden").children(".category_uuid").text();
                var category_parent_id = $(".hidden").children(".category_parent_id").text();
                if($(".hidden").length == "1") {
                    //console.log("if");
                    var html = "<a href="+category_uuid+" id=\"video_search\" data-uuid="+category_uuid+"  data-parentid="+category_parent_id+"  data-name="+category+" data-toggle=\"tab\" class=\"nav-link\" onclick=videoShow(\'"+category_uuid+"\');>"+category+"</a>";
                    $(".category_child").html(html);
                } else if($(".hidden").length == "0") {
                    if(id == $("#video_search").data("uuid")){
                        $("#video_search").addClass("active show");
                    }
                    // $(".category_video #myTab5").hide();
                } else {
                    //console.log("else");
                    var html = [];
                    $(".hidden").each(function(cat) {
                        var category = $(this).children(".category_name").text();
                        var category_array = [];
                        var category_uuid = $(this).children(".category_uuid").text();
                        var category_uuid_array = [];
                        var category_parent_id = $(this).children(".category_parent_id").text();
                        var category_parent_id_array = [];
                        category_array.push(category);
                        category_uuid_array.push(category_uuid);
                        category_parent_id_array.push(category_parent_id);
                        var html_new = "<a href= "+category_uuid_array+" id=\"video_search\" data-uuid="+category_uuid_array+"  data-parentid="+category_parent_id_array+"  data-name="+category_array+" data-toggle=\"tab\" class=\"nav-link\"  onclick=videoShow(\'"+category_uuid_array+"\')>"+category_array+"</a>";
                        html.push(html_new);
                    });
                    $(".category_child").html(html);
                    var new_html = "<ul class=\"nav nav-tabs float-left  vertical col-lg-2 col-xl-2 col-md-3 col-3 left-aligned\" id=\"myTab5\" role=\"tablist\">";
                    new_html += "<li class=\"category_sub_child\">";
                    new_html += "</li>";
                    new_html += "</ul>";
                    $(".category_sub_child").html(new_html);
                }
                
                unblockBody();
                $(".showVideo").html(data);
            },
            error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
    }

    function videoAllShow(){
        $(".category_video #myTab5").toggle();
        $(".category_video #myTab5").hide();
        var url = "' . Url::to(['test/get-videos-from-category']) . '?id=";
        $.ajax({
            type: "GET",
            url: url,
            beforeSend: function() {
                blockBody();
            },
            success:function(data){
                unblockBody();
                $(".showVideo").html(data);
            },
            error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
    }
  ', View::POS_END
);
?>

<div class="col-xl-12">
    <section class="box">
        <div class="col-lg-12 search_data">
            <ul class="nav nav-tabs float-left  vertical col-lg-2 col-xl-2 col-md-3 col-3 left-aligned" id="myTab5" role="tablist">

                <li class="nav-item">
                    <a href="#" id="all_video" data-name="Allvideo" data-toggle="tab" class="nav-link" onclick="videoAllShow();">
                        <i class="fa fa-home"></i> All Videos
                    </a>
                </li>
                <?php
                if (!empty($model)) {
                    foreach ($model as $key => $value) { ?>
                    <li class="category_video">
                        <a href="<?= $value['uuid']; ?>" id="video_search" data-uuid="<?= $value['uuid']; ?>"  data-parentid="<?= $value['parent_id']; ?>"  data-name="<?= $value['name']; ?>" data-toggle="tab" class="nav-link" onclick="videoShow('<?= $value['uuid']; ?>');">
                            <?= $value['name']; ?>
                        </a>
                        <ul class="nav nav-tabs float-left  vertical col-lg-2 col-xl-2 col-md-3 col-3 left-aligned" data-id="<?= $value['uuid']; ?>" id="<?= $value['uuid']; ?>" role="tablist">
                            <li class="category_child">
                                <ul class="nav nav-tabs float-left  vertical col-lg-2 col-xl-2 col-md-3 col-3 left-aligned" id="<?= $value['uuid']; ?>" role="tablist">
                                    <li class="category_sub_child">
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    
                    <?php
                    }
                }
                ?>
            </ul>

            <div class="tab-content float-left  vertical col-xl-10 col-lg-10 col-md-3 col-3 left-aligned" id="myTabContent5">
                <?php
                if (!empty($model)) { ?>
                    <div id="<?= $value['uuid']; ?>" class="tab-pane showVideo fade active show">
                    </div>                    
                <?php
                }
                ?>
            </div>

        </div>
    </section>
</div>




















