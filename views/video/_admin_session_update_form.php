<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\ArrayHelper;
use app\models\InstrumentCategory;
use app\models\Instrument;
use yii\helpers\Url;

$categoryArray = ArrayHelper::map(InstrumentCategory::find()->asArray()->all(), 'uuid', 'name');
//$instrumentArray = ArrayHelper::map(Instrument::find()->where(['status' => 'ACTIVE'])->asArray()->all(), 'uuid', 'name');

if($model['tutor_uuid'] != NULL){ 
    $tutorInstrumentModel = \app\models\TutorInstrument::find()->where(['tutor_uuid' => $model['tutor_uuid']])->asArray()->all();
    $old_t_ins = array_column($tutorInstrumentModel, 'instrument_uuid');

    $instrumentArray = ArrayHelper::map(Instrument::find()->where(['in', 'uuid', $old_t_ins])->asArray()->all(), 'uuid', 'name');
}else{ 
    $instrumentArray = ArrayHelper::map(Instrument::find()->where(['status' => 'ACTIVE'])->asArray()->all(), 'uuid', 'name');
}

?>

<?php
//echo "<pre>";
//print_r($model->getErrors());
//echo "</pre>";
?>
<?php
$form = ActiveForm::begin(['options' => ['id' => 'upload_form', 'enctype' => 'multipart/form-data', 'enableClientValidation' => true,
                'enableAjaxValidation' => true, 'validateOnChange' => true,]]);
?>


<div class="form-row">
    <div class="col-md-9 mb-0">
        <?= $form->field($model, 'update_file')->fileInput(['class' => 'form-control']) ?>
    </div>
</div>
<div class="progress progress-md">
    <div id="progress-bar" class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
        <span class="sr-only" id="progress-status">0%</span>
    </div>
</div>
<div class="msg"></div>
<div class="spacer"></div>


<?= Html::Button(Yii::t('app', 'Update'), ['id' => 'savebutton', 'class' => 'btn btn-primary', 'style' => 'float: right;']) ?>

<?php ActiveForm::end(); ?>