<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%online_tutorial_package}}".
 *
 * @property string $uuid
 * @property string $name
 * @property string $description
 * @property string $tutorial_type_uuid
 * @property string $payment_term_uuid
 * @property string $price
 * @property string $gst
 * @property string $gst_description
 * @property string $total_price
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property PaymentTerm $paymentTermUu
 * @property TutorialType $tutorialTypeUu
 */
class OnlineTutorialPackage extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return '{{%online_tutorial_package}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            //html tag filter
            [['name','description'], 'match', 'pattern' => \app\libraries\General::validTextPattern(['@','$','(',')','\[','\]','\/',]), 'message' => 'Invalid charecters used.'], 
            
            [['name',  'tutorial_type_uuid', ], 'required'],
            [['uuid', 'name', 'description', 'tutorial_type_uuid', 'payment_term_uuid', 'gst_description', 'status'], 'string'],
            [['price', 'gst', 'total_price'], 'number'],
            [['created_at', 'updated_at','tutorial_type_uuid'], 'safe'],
            [['uuid'], 'unique'],
            [['payment_term_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentTerm::className(), 'targetAttribute' => ['payment_term_uuid' => 'uuid']],
            [['tutorial_type_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => TutorialType::className(), 'targetAttribute' => ['tutorial_type_uuid' => 'uuid']],
            [['price', 'gst'], 'match', 'pattern' => '/^\d{0,10}(\.\d{0,5})?$/', 'message' => 'Only allow decimal 5 digit.'],
            ['gst', 'compare', 'compareAttribute' => 'price', 'operator' => '<', 'enableClientValidation' => false],
            ['price', 'compare', 'compareAttribute' => 'gst', 'operator' => '>', 'enableClientValidation' => false],
            [['price', 'gst'], 'required', 'on' => ['admin_update_tutorial']],
            [['name', 'description'], 'safe','on' => ['admin_update_tutorial']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'tutorial_type_uuid' => Yii::t('app', 'Tutorial Type'),
            'payment_term_uuid' => Yii::t('app', 'Payment Term'),
            'price' => Yii::t('app', 'Price'),
            'gst' => Yii::t('app', 'GST'),
            'gst_description' => Yii::t('app', 'GST Description'),
            'total_price' => Yii::t('app', 'Total Price'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentTermUu() {
        return $this->hasOne(PaymentTerm::className(), ['uuid' => 'payment_term_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTutorialTypeUu() {
        return $this->hasOne(TutorialType::className(), ['uuid' => 'tutorial_type_uuid']);
    }

    /**
     * Start
     * Developed By : Pooja Beladiya    
     * Task : Tutor wise plan rate
     */
    public function getPremiumRate() {
        return $this->hasOne(PremiumPlanRate::className(), ['uuid' => 'online_tutorial_package_uuid']);
    }
    
    public function getStandardRate() {
        return $this->hasMany(StandardPlanRate::className(), ['uuid' => 'online_tutorial_package_uuid']);
    }

    /**
     * End
     * Task : Tutor wise plan rate
     */
    
    
    /* public function getTutorialTypes()
      {
      return $this->hasOne(TutorialType::className(), ['uuid' => 'tutorial_type_uuid'])->from(['tutorial_type' => TutorialType::tableName()]);;
      return $this->hasOne(Cities::className(), ['id' => 'city'])->from(['cities' => Cities::tableName()]);;
      } */
    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['admin_update_tutorial'] = ['price', 'gst','name', 'description'];
        return $scenarios;
    }

}
