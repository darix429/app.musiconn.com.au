<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%order_item}}".
 *
 * @property string $uuid
 * @property string $order_uuid
 * @property string $item
 * @property string $booking_uuid
 * @property string $student_monthly_subscription_uuid
 * @property string $price
 * @property string $tax
 * @property string $total
 *
 * @property Order $orderUu
 */
class OrderItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%order_item}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'order_uuid', 'item'], 'required'],
            [['uuid', 'order_uuid', 'item', 'booking_uuid', 'student_monthly_subscription_uuid'], 'string'],
            [['price', 'tax', 'total'], 'number'],
            [['uuid'], 'unique'],
            [['order_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_uuid' => 'uuid']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'order_uuid' => Yii::t('app', 'Order Uuid'),
            'item' => Yii::t('app', 'Item'),
            'booking_uuid' => Yii::t('app', 'Booking Uuid'),
            'student_monthly_subscription_uuid' => Yii::t('app', 'Student Monthly Subscription Uuid'),
            'price' => Yii::t('app', 'Price'),
            'tax' => Yii::t('app', 'Tax'),
            'total' => Yii::t('app', 'Total'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderUu()
    {
        return $this->hasOne(Order::className(), ['uuid' => 'order_uuid']);
    }
}
