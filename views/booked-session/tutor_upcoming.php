<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;
use app\models\Tutor;
use app\models\BookedSession;
use app\libraries\General;

$this->title = Yii::t('app', 'My Lessons');
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs(
        '
    $(document).on("click",".tutor_cancel_button",function(){
        if(confirm("Are you sure, you want to cancel this Lessons?")){
            blockBody();
            $("#cancellesson_cancel_note").val("");
            $("#cancelLesson").modal("show");
            var date = $(this).data("date");
            var start_time = $(this).data("stime");
            var end_time = $(this).data("etime");
            var uuid = $(this).data("uuid");
            $("#cancelLesson").find(".lesson-details").html("<b>Lesson: </b>"+date+" "+start_time+" - "+end_time);
            $("#cancelLesson").find("#tutor_cancel_lesson").attr("data-uuid", uuid);
            unblockBody();
        } else {
            unblockBody();
            return false;
        }
    });

    $(document).on("click", "#tutor_cancel_lesson",function(){
        var curr_uuid = $("#cancelLesson").find("#tutor_cancel_lesson").attr("data-uuid");
        var note = $("#cancellesson_cancel_note").val();
        $.ajax({
            type: "POST",
            url: "' . Url::to(['/booked-session/cancel-lesson-request-note/', ]) .'?id="+curr_uuid,
            data: {note: note},
            beforeSend: function() {
                blockBody();
            },
            success:function(result){
                if(result.code == 200) {
                    $("#cancelLesson").modal("hide");
                    $(".tr_"+curr_uuid).find(".tutor_cancel_button").replaceWith("<button class=\"btn btn-xs btn-orange tutor_cancel_lesson\" title=\"Pending Cancel Approval\" data-url=\"+dataurl+\" >Pending Cancel Approval</button>");
                    unblockBody();
                } else {
                    unblockBody();
                    showErrorMessage(result.message);
                }
            },
            error:function(e){
                unblockBody();
               showErrorMessage(e.responseText);
            }
        });
    });

    $(document).ready(function() {
        var table = $(".my-datatable").DataTable();
        $(".cancel_lesson_popup_btn").click(function(){
            var curr_uuid = $(this).data("uuid");
            $("#studentLessonCancelApproval").modal("show");
            $("#studentLessonCancelApproval_uuid").val(curr_uuid);
         });

        $("#studentLessonCancelApproval_approve_btn").click(function(){
            var curr_uuid = $(".cancel_lesson_popup_btn").data("uuid");
            $.ajax({
                type: "GET",
                url: "' . Url::to(['/booked-session/cancel-lesson-approval/', ]) .'",
                data: { id : curr_uuid, st : "yes"},
                beforeSend: function() {
                    blockBody();
                },
                success:function(result){
                    if(result.code == 200) {
                        table.row($(".tr_"+curr_uuid)).remove().draw(true);
                        $("#studentLessonCancelApproval").modal("toggle");
                        unblockBody();
                    } else {
                        unblockBody();
                        showErrorMessage(result.message);
                    }
                },
                error:function(e){
                    unblockBody();
                    showErrorMessage(e.responseText);
                }
            });
        });

        $("#studentLessonCancelApproval_reject_btn").click(function(){
            var curr_uuid = $(".cancel_lesson_popup_btn").data("uuid");
            var status;
            $.ajax({
                type: "GET",
                url: "' . Url::to(['/booked-session/cancel-lesson-approval/', ]) .'",
                data: { id : curr_uuid, st : "no"},
                beforeSend: function() {
                    blockBody();
                },
                success:function(result){
                    if(result.code == 200) {
                        var dataurl = web_url + "booked-session/cancel&id="+curr_uuid;
                        $(".tr_"+curr_uuid).find(".cancel_lesson_popup_btn").replaceWith("<button class=\"btn btn-xs btn-danger session_cancel_button\" title=\"Cancel\" data-url=\"+dataurl+\" >Cancel</button>");
                        $("#studentLessonCancelApproval").modal("toggle");
                        unblockBody();
                    } else {
                        unblockBody();
                        showErrorMessage(result.message);
                    }
                },
                error:function(e){
                    unblockBody();
                    showErrorMessage(e.responseText);
                }
            });
        });
    });
  
setInterval(function() {
    array = [];
    value = [];
    var d = new Date();
    var current_timestamp = Math.floor(d / 1000);
    d.setMinutes(d.getMinutes() + 5);
    var after_timestamp = Math.floor(d / 1000);

    $("tr.tution_tr").each(function(item){
        var uuid = array["uuid"] = $(this).attr("data-uuid");
        array["start_time"] = $(this).attr("data-stime");
        array["end_time"] = $(this).attr("data-etime");
        value.push(array);
        if(after_timestamp >= array["start_time"] && current_timestamp <= array["end_time"]) {
            $("#reschedulebutton_"+uuid).css({display:"none"});
            $("#cancelbutton_"+uuid).css({display:"none"});
        } else {
            $("#reschedulebutton_"+uuid).css({display:"inline"});
            $("#cancelbutton_"+uuid).css({display:"inline"});
            if(current_timestamp >= array["end_time"]) {
                var table = $(".my-datatable").DataTable();
                table.row( $(".tr_"+uuid) ).remove().draw(true);
            }
        }
        
    });
}, 500 * 60 * 1);
        ', View::POS_END
);
?>

<div class="col-lg-12">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <section class="box">
                <!-- Horizontal - start -->
                <div class="content-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <a class="btn btn-purple btn-icon  right15 pull-right tutor-unavailability" style="cursor:pointer"> Set Unavailability </a>
                            <div class="clearfix"></div>
                            <ul class="nav nav-tabs primary"  id="myTab1" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" role="tab" href="#upcoming_tution" data-toggle="tab">
                                        Upcoming Lessons
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="<?= Yii::$app->params['base_url'] . 'booked-session/completed'; ?>" >
                                        Completed Lessons
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="<?= Yii::$app->params['base_url']. 'booked-session/cancelled'; ?>" >
                                         Cancelled Lessons
                                    </a>
                                </li>
                            </ul>

                            <div class="tab-content primary" id="myTabContent1">
                                <div class="tab-pane fade show active" id="upcoming_tution">
                                    <div>


                                        <!---start---->
                                        <div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">
                                            <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <!-- <th>No.</th> -->
                                                        <th>Student</th>
                                                        <th>Date</th>
                                                        <th>Time</th>
                                                        <!--<th>Session Pin</th>-->
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if (!empty($upcomingList)) {
                                                        foreach ($upcomingList as $key => $value) {

                                                            $now = date('Y-m-d h:i A');
                                                            $timestamp = strtotime($now);
                                                            $timestamp_after_5 = date('Y-m-d h:i A', strtotime('+5 minutes', strtotime($now)));
                                                            ?>
                                                            <tr class="tr_<?= $value['uuid'];?> tution_tr" data-stime="<?= strtotime($value['start_datetime']); ?>" data-etime="<?= strtotime($value['end_datetime']); ?>" data-uuid="<?= $value['uuid']; ?>">
                                                                <!-- <td><?= $key + 1; ?></td> -->
                                                                <td><?= $value['studentUu']['first_name'] . ' ' . $value['studentUu']['last_name']; ?></td>
                                                                <td><?= General::displayDate($value['start_datetime']); ?></td>
                                                                <td><?= General::displayTime($value['start_datetime']) ." - ".General::displayTime($value['end_datetime']); ?></td>
                                                                <td>
                                                                    <?php
                                                                    if ($value['status'] == "SCHEDULE" || $value['status'] == "INPROCESS") {

                                                                        $enabled = (strtotime($timestamp_after_5) >= strtotime($value['start_datetime']) && $timestamp <= strtotime($value['end_datetime'])) ? "" : "disabled='disabled'";
                                                                        $enabled_class = (strtotime($timestamp_after_5) >= strtotime($value['start_datetime']) && $timestamp <= strtotime($value['end_datetime'])) ? "btn-primary" : "btn-default";
                                                                        
                                                                        // $button_text = ( (strtotime($timestamp_after_5) >= strtotime($value['start_datetime']) && $timestamp <= strtotime($value['end_datetime'])) ) ? "Join" : "Start";
                                                                        $isStarting = ( (strtotime($timestamp_after_5) >= strtotime($value['start_datetime']) && $timestamp <= strtotime($value['end_datetime'])) );

                                                                        if(strtotime($timestamp_after_5) >= strtotime($value['start_datetime']) && $timestamp <= strtotime($value['end_datetime'])) {
                                                                            $reschedule_display = "display:none";
                                                                            $cancel_display = "display:none";                                                                
                                                                        } else {
                                                                            $reschedule_display = "display:inline";
                                                                            $cancel_display = "display:inline";
                                                                        } 
                                                                        
                                                                        $btn_text = "Reschedule";
                                                                        $btn_class = "btn-warning";
                                                                        if ($value["is_reschedule_request"] == 1) {
                                                                            $btn_text = "Pending Approval";
                                                                            $btn_class = "btn-success";
                                                                        } else if ($value["is_reschedule_request"] == 2) {
                                                                            $btn_text = "Reschedule Initiated";
                                                                            $btn_class = "btn-orange";
                                                                        }
                                                                        
                                                                        ?>
                                                                    
                                                                        <!-- JOIN button -->
                                                                        <button 
                                                                        class="btn btn-xs session_start_button <?= ($isStarting)? "btn-primary":"" ?>" 
                                                                        id="startbutton_<?= $value['uuid'] ?>" 
                                                                        <?= $isStarting? "":"disabled title='This will be active 5 minutes before your lesson time.'" ?>
                                                                        data-uuid="<?= $value['uuid'] ?>" data-url="<?= BookedSession::getStartSessionLink($value['uuid'], 'tutor'); ?>"
                                                                        >
                                                                            Join
                                                                        </button>
                                                                        
                                                                        <!-- Action buttons -->
                                                                        <?php if ($value['start_datetime'] > date('Y-m-d H:i:s')) {  ?>
                                                                                                                                                        
                                                                            <a class="btn btn-xs session_reschedule_button <?= $btn_class?>" style="<?= $reschedule_display; ?>" title="<?= $btn_text ?>" href="<?= Url::to(['/book/reschedule-request', 'id' => $value['uuid']]) ?>" id="reschedulebutton_<?= $value['uuid'] ?>" ><?= $btn_text ?></a>
                                                                            
                                                                            
                                                                            <?php if ($value['is_cancelled_request'] == 1) {
                                                                                $cancel_lesson_request = BookedSession::getCancleLessonRequest($value['uuid']);
                                                                                if ($cancel_lesson_request['cancel_by'] == 'TUTOR') {
                                                                                ?>
                                                                                    <button class="btn btn-xs btn-orange tutor_cancel_lesson" style="<?= $cancel_display; ?>" title="Pending Cancel Approval" data-uuid="<?= $value['uuid'] ?>" >Pending Cancel Approval</button>
                                                                                <?php } else { ?>
                                                                                     <button class="btn btn-xs btn-info cancel_lesson_popup_btn" style="<?= $cancel_display; ?>" title="Pending Cancel Approval" data-uuid="<?= $value['uuid'] ?>" >Pending Cancel Approval</button>
                                                                                <?php
                                                                                }
                                                                                ?>
                                                                                
                                                                            <?php } else { ?>
                                                                                <!-- <button class="btn btn-xs btn-danger session_cancel_button" style="<?= $cancel_display; ?>" title="Cancel" data-url="<?= Url::to(['/booked-session/cancel-lesson-request', 'id' => $value['uuid']]) ?>" id="cancelbutton_<?= $value['uuid'] ?>" >Cancel</button> -->
                                                                                <button class="btn btn-xs btn-danger tutor_cancel_button" style="<?= $cancel_display; ?>" title="Cancel" data-url="<?= Url::to(['/booked-session/cancel-lesson-request', 'id' => $value['uuid']]) ?>" id="cancelbutton_<?= $value['uuid'] ?>" data-uuid="<?= $value['uuid'] ?>" data-date="<?= General::displayDate($value['start_datetime']); ?>" data-stime="<?= General::displayTime($value['start_datetime']); ?>"
                                                                                data-etime="<?= General::displayTime($value['end_datetime']); ?>">Cancel</button>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                        <?php
                                                                    } else {
                                                                        echo ucfirst($value['status']);
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>

                                        </div>
                                        <!---end--->
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="completed_tution">

                                </div>

                            </div>

                        </div>

                    </div>
                    <!-- Horizontal - end -->
                </div>
            </section>

        </div>
    </div>
</div>

<div class="modal fade" id="studentLessonCancelApproval" tabindex="-1" role="dialog" aria-labelledby="ultraModal-Label" aria-hidden="true">
    <div class="modal-dialog animated zoomIn">
        <div class="modal-content">
            <div class="modal-header bg-orange" style="border-radius: 0;">
                <h4 class="text-white" >Cancel lesson request</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div id="studentLessonCancelApproval-content" class="modal-body">
                <input type="hidden" id="studentLessonCancelApproval_uuid">
                <div class="card credit_card_custom_css">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6 text-center" style="border-right: 1px solid #bbb8b8;">
                                <p> </p>
                                <a class="btn btn-purple btn-icon " href="javascript:void(0)" id="studentLessonCancelApproval_approve_btn">
                                    Approve
                                </a>
                            </div>
                            <div class="col-lg-6 text-center">
                                <p> </p>
                                <a class="btn btn-orange btn-icon " href="javascript:void(0)" id="studentLessonCancelApproval_reject_btn">
                                    Reject
                                </a>
                                <!--<div class="clearfix"></div>-->
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="cancelLesson" tabindex="-1" role="dialog" aria-labelledby="cancelLessonLabel" aria-hidden="true">
    <div class="modal-dialog animated zoomIn">
        <div class="modal-content">
            <div class="modal-header bg-danger"  style="border-radius: 0;">
                <h4 class="text-white"><b>Cancel Lesson Request</b></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div id="cancellesson-content" class="modal-body">
                <div class="row">
                    <div class="col-md-12 mb-10">
                    <p class="lesson-details"></p>
                    </div>
                    <div class="col-md-12 mb-10">
                        <label class="control-label" for="cancellesson-cancel_note">Cancel Note</label>
                        <textarea id="cancellesson_cancel_note" class="form-control" name="cancellesson[cancel_note]" placeholder="Enter Cancel Note" aria-required="true"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="tutor_cancel_lesson" class="btn btn-primary" style="float: right;">Submit</button>
            </div>
        </div>
    </div>
</div>