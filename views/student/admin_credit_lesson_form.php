<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\ArrayHelper;
use app\models\Student;
use app\models\TutorialType;
use app\models\Instrument;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Add Credit Lesson');
$this->params['breadcrumbs'][] = $this->title;

$studentArray = ArrayHelper::map(Student::find()->where(['status' => 'ENABLED'])->all(), 'student_uuid', function ($s) { return $s->first_name.' '.$s->last_name; } );


$tutorialarray = [['id' => '30W', 'name' => '1/2 Hour Weekly'],
                  ['id' => '60W', 'name' => '1 Hour Weekly']
                ];

$tutorialArray = ArrayHelper::map($tutorialarray, 'id', 'name');
?>

<div class="col-xl-12 col-lg-12 col-12 col-md-12" id="adminCreditSession">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Admin Add Credit Lesson</h2>
            <div class="actions panel_actions float-right">
                <a href="<?= Yii::$app->getUrlManager()->createUrl(['/student/index/'])?>" class="btn btn-info btn-icon "><i class="fa fa-arrow-left text-white"></i> <span>Back</span></a>
            </div>
        </header>
        <div class="content-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12">
                    <?php $form = ActiveForm::begin(['options' => ['id' => 'credit_session','enableClientValidation' => true, 'enableAjaxValidation' => true, 'validateOnChange' => true,]]); ?>
                    <div class="form-row">
                        <div class="col-md-6 mb-0">
                            <?= $form->field($model, 'student_uuid')->dropDownList($studentArray, ['class' => 'form-control myselect2', 'prompt' => 'Select Student', 'style' => 'width: 100%']) ?>
                        </div>
                        <div class="col-md-6 mb-0">
                            <?= $form->field($model, 'tutorial_type_code')->dropDownList($tutorialArray, ['class' => 'form-control', 'prompt' => 'Select Tutorial Type',]) ?>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6 mb-0">
                            <?= $form->field($model, 'credited_by')->hiddenInput(['value'=> 'ADMIN'])->label(false); ?>
                        </div>
                        <div class="col-md-6 mb-0">
                            <?= $form->field($model, 'credited_note')->hiddenInput(['value'=> 'Credited By ADMIN'])->label(false); ?>
                        </div>
                    </div>
                    <?= Html::submitButton(Yii::t('app', 'Create'), ['id' => 'adminCreditSessionSubmit', 'class' => 'btn btn-primary', 'style' => 'float: right;']) ?>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>

        </div>
    </section>
</div>
