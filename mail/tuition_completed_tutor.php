<?php

use yii\helpers\Html;
//echo $content['name']; exit;
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['tutor_name'] ?>,</p>

	    <p>Your Lesson has been completed successfully on Musiconn's platform.</p>

	    <p><strong>Student :</strong> <?= $content['student_name']; ?></p>
	    <p><strong>Lesson Time :</strong> <?= $content['time']; ?></p>
            <!--<p><strong>Video Link :</strong> <?php //echo $content['link'] ?></p>--> 

            <p>Please do not reply to this unattended mailbox.  Any queries or feedback can be directed to admin@musiconn.com.au, or call us on 1300 068 742.</p>
            
            <p>Regards,<br>
               System Administrators</p>
        </td>
    </tr>
</table>

