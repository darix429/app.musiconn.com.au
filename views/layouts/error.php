<?php
/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\ErrorAsset;

ErrorAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>

        <link rel="shortcut icon" href="/theme_assets/images/favicon.ico" type="image/x-icon" />    <!-- Favicon -->
<!--        <link rel="apple-touch-icon-precomposed" href="/theme_assets/images/apple-touch-icon-57-precomposed.png">	 For iPhone 
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/theme_assets/images/apple-touch-icon-114-precomposed.png">     For iPhone 4 Retina display 
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/theme_assets/images/apple-touch-icon-72-precomposed.png">     For iPad 
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/theme_assets/images/apple-touch-icon-144-precomposed.png">     For iPad Retina display -->

        <?php $this->head() ?>
    </head>
    <body class=" ">
        <?php $this->beginBody() ?>

        <?= $content ?>

        <p class="text-center text-orange "><?= Yii::$app->params["copyright"]; ?></p>
        

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
