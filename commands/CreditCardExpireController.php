<?php

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
//rupal
use app\models\StudentCreditCard;
use app\libraries\General;
use app\models\Student;
use Yii;

class CreditCardExpireController extends Controller
{

    public function actionIndex($message = 'CreditCardExpire cron Index')
    {
        echo $message . "\n";

        return ExitCode::OK;
    }

    public function actionNotification() {

        $currDate = date("Y-m-d H:i:s");
        $expire_date_after = date('Y-m-d', strtotime('+10 days', strtotime($currDate)));
        $creditCardlist = StudentCreditCard::find()->where(['expire_date' => $expire_date_after])->all();

	if(!empty($creditCardlist)){
		foreach($creditCardlist as $key => $value){

		    $student = \app\models\User::find()->joinWith('student')->where(['user.student_uuid' => $value->student_uuid, 'student.student_uuid' => $value->student_uuid, 'student.status'=>'ENABLED'])->one();
		    $number = General::decrypt_str($value->number);

		    $creditCardDetails = array(
		        'student_first_name' => $student->first_name,
		        'student_last_name' => $student->last_name,
		        'student_email' => $student->email,
		        'name' => $value->name,
		        'number' => General::ccMasking($number),
		        'expire_date' => General::convertTimezone($value->expire_date, Yii::$app->params['timezone'], $student->timezone,'Y-m-d H:i:s'),
		    );

	//            //php /var/www/html/musiconn/yii credit-card-expire/notification
		    StudentCreditCard::expireCreditCardNotification($creditCardDetails);
		}
	}
    }


}
