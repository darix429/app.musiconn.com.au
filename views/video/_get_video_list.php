<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

$this->registerJs(
'
    $("a.droppable").mouseover(function(){
        $(this).addClass("hover");
        $(this).css({"color":"#FFFFFF"});
    });
    $("a.droppable").mouseout(function(){
        $(this).removeClass("hover");
        $(this).css({"color":""});
    });
    
    $(".draggable").draggable({
        revert: "invalid",
        cursorAt: { left: 2, top: 2},
        stop: Stop,
        drag: Drag,
        start: function(event, ui){
            $(ui.helper).addClass("ui-helper");
        }
    });

    $(".droppable").droppable({
        drop: Drop
        // tolerance: "pointer",
        // over: function(event, ui){
        //     console.log("OVER");
        //     console.log($(this).attr("data-name"));
        //     $(this).addClass("hover");
        // },
        // out: function(event, ui){
        //     console.log("OUT");
        //     $(this).removeClass("hover");
        // }
    });

    function Stop(event, ui) {
        $(".nested").css({"display":""});
        $(".caret").siblings(".nested").fadeOut();
        $(".caret").removeClass("caret-down");
        $(ui.helper).removeClass("ui-helper");
        $(ui.helper).css({"position": "relative", "right":"", "bottom":"", "left":"", "top":"", "height":"", "width":"", "background":"", "border-radius":""});
        $(ui.helper).children().css({"position":"", "height":"", "width":"", "display":""});
        $(ui.helper).children(".team-info").css({"display":""});
        $(ui.helper).children(".team-info").children().css({"color":"", "text-align":"", "margin-top":"", cursor:""});
        $(".datatable-search-div").css({"overflow-y": ""});
    }

    function Drag(event, ui) {
        $(this).css("top", event.pageY);
        $(this).css("left", event.pageX);
        $(ui.helper).css({"right":"", "bottom":"", "left":"", "top":"", "height":"30px", "width":"220px", "background":"#00C6D7", "border-radius":"10px"});
        $(ui.helper).children().css({"display":"none"});
        $(ui.helper).children(".team-info").css({"display":"inline"});
        $(ui.helper).children(".team-info").children().css({"color":"#FFFFFF", "text-align":"center", "margin-top":"2px", cursor:"pointer"});
        // const ps = new PerfectScrollbar(".datatable-search-div", {
        //   "suppressScrollY": true
        // });
        $(".datatable-search-div").css({"overflow-y": "scroll"});
        $("ul.nested").children().children().children().css({"width": "172.25"});
        $(".caret").siblings(".nested").fadeIn();
        $(".caret").addClass("caret-down");
    }

    function Drop(event, ui) {
        var draggableId = ui.draggable.children(".thumb").attr("data-uuid");
        $(this).removeClass("hover");
        var droppableName = $(this).attr("data-name");
        var droppableId = $(this).attr("data-uuid");
        $(this).addClass("droppableActive");
        $(ui.helper).css({"position":"relative"});
        $(ui.helper).children().css({"position":"relative"});
        $(ui.helper).children().children().css({"position":"relative"});
        $(ui.helper).children().children().children().css({"position": "relative"});
        ui.helper.remove();
        var title = $.trim(ui.draggable.find("h4").text());
        var instrument_uuid = ui.draggable.find("a").attr("data-instrument_uuid");
        $.ajax({
            method: "POST",
            data: {"title": title, "instrument_category_uuid": droppableId, "instrument_uuid": instrument_uuid},
            url: "' . Url::to(['video/video-category-update']) . '?id="+draggableId,
            beforeSend: function() {
              blockBody();
            },
            success:function(result){
                unblockBody();
                ui.helper.remove();
                showSuccess(result.message);
                window.location.reload();
            },
            error:function(e){
                unblockBody();
                window.location.reload();
                showErrorMessage(e.responseText);
            }
        });
        $(this).removeClass("droppableActive");
    }

    //delete code
    $(document).on("click",".delete_pre_recorded_video_link", function(){
        if(confirm("Are you sure, you want to delete this video?")){
        var element = $(this);
        var id = $(this).data("uuid");
            $.ajax({
                type: "POST",
                // url: "'.Url::to(['/video/delete/']).'",
                url: "'.Url::to(['/video/delete-video/']).'?id="+id,
                success:function(result){
                    if(result.code == 200){

                        $(element).closest("div.music_genre").remove();

                        showSuccess(result.message);
                    }else{
                        showErrorMessage(result.message);
                    }
                },
                error:function(e){
                    showErrorMessage(e.responseText);
                }
            });
        }
    });
', View::POS_END
);
?>
<style>
    .custom-box{
        height: 141px;
        width: 211px;
        text-align: center;
        font-size: 20px;
        margin-top:93px;
    }
    .music_genre .thumb {
        height:96%;
    }
    .gallery-parent-div {
        border: 0px solid;
        padding: 0;
    }
    .hover {
        background : green;
        color : white;
    }
    .droppableActive {
        background : red;
        color : white;
    }
    .ui-helper {
        width: 20px;
        height: 20px;
    }
</style>

<div class="">
    <h4>
        <?php
        if (!empty($videoList)) {
            if(Yii::$app->getRequest()->get('id')) {
                print_r($videoList[0]['instrumentCategoryUu']['name']);
            } else {
                echo "";
            }
        }
        ?>
    </h4>
    <div class="row">
        <?php 
        
        if (!empty($videoList)) {
            foreach ($videoList as $key => $value) {
                
                $thumb_url = is_file(Yii::$app->params['media']['pre_recorded_video']['thumbnail']['path'] . $value['uuid'].'.jpg') ? 
                                    Yii::$app->params['media']['pre_recorded_video']['thumbnail']['url'] . $value['uuid'].'.jpg' :
                                    Yii::$app->params['theme_assets']['url'] . "images/thumbnail.jpeg";
                ?>

                <div class="col-lg-4 col-sm-4 col-md-4 music_genre">
                    <div class="team-member gallery-parent-div draggable">
                        <div class="thumb" data-uuid="<?= $value['uuid'] ?>">
                            <a href="<?= Url::to(['/video/review/', 'id' => $value['uuid']]) ?>" data-instrument_uuid="<?= $value['instrument_uuid'] ?>">
                                <img class="video_thumb_image" src="<?= $thumb_url; ?>">
                            </a>
                            <div class="overlay">                                
                                <a href="<?= Url::to(['/video/review/', 'id' => $value['uuid']]) ?>" class="overlay">
                                    <i class="fa fa-eye"></i>
                                </a>
                            </div>
                        </div>

                        <div class="team-info ">
                            <h4>&nbsp;
                                <?= $value['title']; ?>
                            </h4>
                        </div>

                        <div class="" align="center">
                            <span><a href="<?= Url::to(['/video/review/', 'id' => $value['uuid']]) ?>" style="color: rgba(147, 149, 150, 1);" title="Review"><i class="box_toggle fa fa-eye" style="font-size:18px;"></i></a></span>&nbsp;&nbsp;&nbsp;&nbsp;
                            <span><a href="javascript:void(0)" data-uuid="<?= $value['uuid']; ?>"  class="delete_pre_recorded_video_link" title="Delete" style="color: rgba(147, 149, 150, 1);"><i class="box_close fa fa-trash"  style="font-size:18px;"></i></a></span>
                        </div>

                    </div>
                </div>
                <?php
            }
        } else {
            ?>
            <h4>
                <?php
                if (!empty($videoList)) {
                    print_r($videoList[0]['instrumentCategoryUu']['name']);
                }
                ?>
            </h4>
            <h5 style="margin-left: 25px;"><b>No video found</b></h5>
        <?php }
        ?>
    </div>
</div>
