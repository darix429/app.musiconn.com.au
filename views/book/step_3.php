<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Url;
use app\libraries\General;
use app\models\StudentTuitionBooking;
use app\models\Instrument;
use app\models\Tutor;

$this->title = Yii::t('app', 'BOOK A MUSIC LESSON');
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_icon'] = 'my-lesson-tag.png';

if (!empty($step_data['step_1']['instrument_uuid'])) {
    $instrumentDetail = Instrument::find()->where(['uuid' => $step_data['step_1']['instrument_uuid']])->one();
} else {
    return \yii\web\Controller::redirect(['index']);
}

if (isset($step_data['step_2']['tutor_uuid']) && $step_data['step_2']['tutor_uuid'] != "") {
    $tutorDetail = Tutor::findOne($step_data['step_2']['tutor_uuid']);
    $model->tutor_uuid = $step_data['step_2']['tutor_uuid'];
    $model->tutorcheckbox = $model->tutor_uuid;
} else {
    return \yii\web\Controller::redirect(['index']);
}

if (!empty($step_data['step_3']['lesson_length'])) {
    $model->lesson_length = $step_data['step_3']['lesson_length'];
}
if (!empty($step_data['step_3']['recurring'])) {
    $model->recurring = $step_data['step_3']['recurring'];
}
if (!empty($step_data['step_3']['lesson_type'])) {
    $model->lesson_type = $step_data['step_3']['lesson_type'];
}
if (!empty($step_data['step_3']['lesson_total'])) {
    $model->lesson_total = $step_data['step_3']['lesson_total'];
}

$this->registerJs(
        '
    $("input[name=\"Book[recurring]\"]").on("click", function (event) {
        if($(this).val() == "NO"){
           $(".show_recurring").addClass("hide_no_recurring"); 
           $(".show_recurring").removeClass("show_recurring"); 
           
           $(".recurring_select_btn").removeClass("d-none"); 
        }
        if($(this).val() == "YES"){
           $(".hide_no_recurring").addClass("show_recurring"); 
           $(".hide_no_recurring").removeClass("hide_no_recurring"); 
           
           $(".recurring_select_btn").addClass("d-none");
        }
    });

    $( "#frm_step_3" ).submit(function( event ) {
        blockBody();
    });


        ', View::POS_END
);
?>
<style>
    .hr{
        width:100%;display: inline-block;margin: 12px 0;
    }
    .hide_no_recurring{
        display:none !important;
    }
    .show_recurring{
        display:block !important;
    }
</style>

<?= Yii::$app->controller->renderPartial('step_prev_booking', ['model' => $model]); ?>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-12"><strong>BOOK A NEW MUSIC LESSON</strong></div><br>
            <div class="panel-group box-header with-border" id="accordion" role="tablist" aria-multiselectable="true">

                <!-- SELECT INSTRUMENT -->
                <div class="panel panel-default">

                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseInstrument" aria-expanded="false" aria-controls="collapseTwo">
                                SELECT INSTRUMENT 
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </a>
                            <span class="pull-right"><?= $instrumentDetail->name; ?> <a href="javascript:$('#gostep1').submit();"><div class="label label-warning hm-1 mr-2 siba">CHANGE</div></a></span>
                        </h4>
                    </div>
                </div>
                <!-- END SELECT INSTRUMENT -->

                <!-- SELECT TUTOR -->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseLength" aria-expanded="false" aria-controls="collapseThree">
                                SELECT TUTOR
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </a>
                            <?php /* .' <!--small>('.$step_data['step_2']['tutor_half_hour_price_in_coin'].' coins / &#189;hr OR '.$step_data['step_2']['tutor_hour_price_in_coin'].'coins / hr)<small-->' */ ?>
                            <span class="pull-right"><?= $tutorDetail->first_name . ' ' . $tutorDetail->last_name; ?> <a href="javascript:$('#gostep2').submit();"><div class="label label-warning hm-1 mr-2 siba">CHANGE</div></a></span>
                        </h4>
                    </div>

                </div>
                <!-- END SELECT TUTOR -->

                <!-- SELECT LENGTH OF LESSON -->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingfour">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseLength" aria-expanded="false" aria-controls="collapsefour">
                                SELECT LENGTH OF LESSON
                            </a>
                        </h4>
                    </div>
                    <div class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
                        <?php
                        $form = ActiveForm::begin([
                                    'action' => ['index'],
                                    'id' => 'frm_step_3',
                                    'enableClientValidation' => true,
                                    'validateOnChange' => true,
                                    'options' => ['class' => 'form-horizontal']]);
                        ?>
                        <div class="panel-body">
                            <?php
                            $model_lesson_length = $model->lesson_length;
                            $lesson_length_array = [
                                30 => '30 Minutes',
                                60 => '60 Minutes',
                            ];
                            echo $form->field($model, 'lesson_length', ['template' => '{input}<div class="col-md-12">{error}</div>', 'options' => ['tag' => false]])
                                    ->radioList(
                                            $lesson_length_array, [
                                        'item' => function($index, $label, $name, $checked, $value) use($model_lesson_length, $lesson_length_array) {

                                            $displayClass = ($model_lesson_length == $value) ? "" : "d-none";
                                            $checked_val = ($model_lesson_length == $value) ? 'checked="checked"' : "";

                                            $extra_h = (count($lesson_length_array) == ($index + 1)) ? '<div class="col-md-4"></div>' : '';

                                            $html = '<div class="col-md-4 ">';
                                            $html .= '<ul class="products-list product-list-in-box">';
                                            $html .= '<li class="st-p st-p2">';
                                            $html .= '<label class="contai_r">';
                                            $html .= '<input type="radio" value="' . $value . '" name="' . $name . '" ' . $checked_val . '>';
                                            $html .= '<span class="checkmark"></span>';
                                            $html .= '</label>';
                                            $html .= '<span class="mn_3">' . $label . '</span>';
                                            $html .= '</li>';
                                            $html .= '</ul>';
                                            $html .= '</div>' . $extra_h;
                                            return $html;
                                        }
                                            ]
                                    )
                                    ->label(false);
                            ?>


                            <hr class="hr">

                            <div class="col-md-12 "><strong>Would you like to make this booking recurring?</strong></div>
                            <?php
                            $model->recurring = ($model->recurring == "YES") ? "YES" : "NO";
                            $model_recurring = $model->recurring;
                            $hide_no_recurring = ($model->recurring == "NO") ? 'hide_no_recurring' : 'show_recurring';
                            $recurring_array = [
                                'NO' => 'No',
                                'YES' => 'Yes',
                            ];
                            echo $form->field($model, 'recurring', ['template' => '{input}<div class="col-md-12">{error}</div>', 'options' => ['tag' => false]])
                                    ->radioList(
                                            $recurring_array, [
                                        'item' => function($index, $label, $name, $checked, $value) use($model_recurring, $recurring_array) {

                                            $displayClass = ("NO" == $value && $model_recurring == "NO") ? "" : "d-none";
                                            $checked_val = ($model_recurring == $value) ? 'checked="checked"' : "";
					    $btn = ' <button style="cursor: pointer; padding-top: 4px;" class="label label-warning custom-hm-1 recurring_select_btn ' . $displayClass . '">NEXT</button>';
                                            //$btn = '<button class="label label-warning hm-1 mr-2 black1 recurring_select_btn ' . $displayClass . '">NEXT</button>';
                                            $btn = ("NO" == $value ) ? $btn : "";
                                            $extra_h = (count($recurring_array) == ($index + 1)) ? '<div class="col-md-4"></div>' : '';

                                            $html = '<div class="col-md-4  ">';
                                            $html .= '<ul class="products-list product-list-in-box">';
                                            $html .= '<li class="st-p st-p2">';
                                            $html .= '<label class="contai_r">';
                                            $html .= '<input type="radio" value="' . $value . '" name="' . $name . '" ' . $checked_val . '>';
                                            $html .= '<span class="checkmark"></span>';
                                            $html .= '</label>';
                                            $html .= '<span class="mn_3">' . $label . '</span>' . $btn;
                                            $html .= '</li>';
                                            $html .= '</ul>';
                                            $html .= '</div>' . $extra_h;
                                            return $html;
                                        }
                                            ]
                                    )
                                    ->label(false);
                            ?>

                            <div class="col-md-4 <?= $hide_no_recurring ?>"></div>
                            <hr class="hr ">
                            <div class="col-md-12 <?= $hide_no_recurring ?>"><strong>Would you like your lessons to recur weekly or fortnightly?</strong></div>
                            <?php
                            $model->lesson_type = ($model->lesson_type == "F") ? "F" : "W";
                            $model_lesson_type = $model->lesson_type;
                            $lesson_type_array = [
                                'W' => 'Weekly',
                                'F' => 'Fortnightly',
                            ];
                            echo $form->field($model, 'lesson_type', ['template' => '{input}{error}', 'options' => ['tag' => false]])
                                    ->radioList(
                                            $lesson_type_array, [
                                        'item' => function($index, $label, $name, $checked, $value) use($model_lesson_type, $hide_no_recurring) {

                                            //$displayClass = ($model_lesson_type == $value) ? "" : "d-none";
                                            $checked_val = ($model_lesson_type == $value) ? 'checked="checked"' : "";



                                            $html = '<div class="col-md-4 ' . $hide_no_recurring . ' ">';
                                            $html .= '<ul class="products-list product-list-in-box">';
                                            $html .= '<li class="st-p st-p2">';
                                            $html .= '<label class="contai_r">';
                                            $html .= '<input type="radio" value="' . $value . '" name="' . $name . '" ' . $checked_val . '>';
                                            $html .= '<span class="checkmark"></span>';
                                            $html .= '</label>';
                                            $html .= '<span class="mn_3">' . $label . '</span>';
                                            $html .= '</li>';
                                            $html .= '</ul>';
                                            $html .= '</div>';
                                            return $html;
                                        }
                                            ]
                                    )
                                    ->label(false);
                            ?>
                            <div class="col-md-4 <?= $hide_no_recurring ?>"></div>


                            <div class="col-md-12 <?= $hide_no_recurring ?>">
                                <hr class="hr" >
                                <strong>Select the total number of lessons you would like to book</strong>
                            </div>


                            <?php
                            $model->lesson_total = ($model->lesson_total > 1) ? $model->lesson_total : 1;
                            $model_lesson_total = $model->lesson_total;
                            $lesson_total_array = [
                                1 => 1,
                                2 => 2,
                                3 => 3,
                                4 => 4,
                                5 => 5,
                                6 => 6,
                                7 => 7,
                                8 => 8,
                                9 => 9,
                                10 => 10,
                                11 => 11,
                                12 => 12,
                            ];
                            echo $form->field($model, 'lesson_total', ['template' => '{input}{error}', 'options' => ['tag' => false]])
                                    ->radioList(
                                            $lesson_total_array, [
                                        'item' => function($index, $label, $name, $checked, $value) use($model_lesson_total, $lesson_total_array, $hide_no_recurring) {

                                            $displayClass = ($model_lesson_total == $value) ? "" : "d-none";
                                            $checked_val = ($model_lesson_total == $value) ? 'checked="checked"' : "";

					    $btn = '<li class="st-p st-p2 btm_txb">
                                                        <button style="cursor: pointer; padding-top: 4px;" class="label label-warning custom-hm-1 totalcount_select_btn">NEXT</button>
                                                    </li>';
                                            //$btn = '<li class="st-p st-p2 btm_txb">
                                            //            <button class="label label-warning hm-1 mr-2 black1 totalcount_select_btn">NEXT</button>
                                            //        </li>';
                                            $btn = (count($lesson_total_array) == ($index + 1)) ? $btn : "";

                                            $befor_html = ($index == 0) ? '<div class="col-md-12 ' . $hide_no_recurring . ' "><ul class="products-list product-list-in-box stt_3">' : '';
                                            $after_html = ($index == (count($lesson_total_array) - 1)) ? '</ul></div>' : '';

                                            $html = '';
                                            $html .= '<li class="st-p st-p2 btm_txb">';
                                            $html .= '<label class="contai_r">';
                                            $html .= '<input type="radio" value="' . $value . '" name="' . $name . '" ' . $checked_val . '>';
                                            $html .= '<span class="checkmark"></span>';
                                            $html .= '</label>';
                                            $html .= '<br>';
                                            $html .= '<div class="slol_b">' . $label . '</div>';
                                            $html .= '</li>';
                                            $html .= $btn;
                                            $html .= '';
                                            return $befor_html . $html . $after_html;
                                        }
                                            ]
                                    )
                                    ->label(false);
                            ?>
                            <div class="col-md-12" style="">
                                <input type="hidden" name="step" value="3">
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
                <!--END SELECT LENGTH OF LESSON -->

                <!-- SELECT DATE AND TIME -->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingfive">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseDate" aria-expanded="false" aria-controls="collapsefive">
                                SELECT DATE AND TIME
                            </a>
                        </h4>
                    </div>
                </div>
                <!--END SELECT DATE AND TIME -->

                <!-- CONFIRM BOOKING AND PAYMENT -->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingsix">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsePayment" aria-expanded="false" aria-controls="collapsesix">
                                CONFIRM BOOKING AND PAYMENT
                            </a>
                        </h4>
                    </div>
                </div>
                <!--END CONFIRM BOOKING AND PAYMENT -->

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <a class="btn btn-primary btn-sm black_button_in" href="<?= Yii::$app->getUrlManager()->createUrl('/student/home'); ?>">RETURN TO HOME</a>
        </div>
    </div>
</section>

<div class="other_forms">
    <?php echo Html::beginForm(['index'], 'post', ['id' => 'gostep1', 'class' => 'form-inline d-block']) ?>
    <input type="hidden" name="step" value="1">
    <input type="hidden" name="prev" value="1">
    <?php echo Html::endForm() ?>
    <?php echo Html::beginForm(['index'], 'post', ['id' => 'gostep2', 'class' => 'form-inline d-block']) ?>
    <input type="hidden" name="step" value="2">
    <input type="hidden" name="prev" value="2">
    <?php echo Html::endForm() ?>
</div>
