<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MusicoinCoupon */

$this->title = Yii::t('app', 'Musicoin Promo Code: {name}', [
    'name' => $model->code,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Musicoin Promo Codes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->uuid, 'url' => ['view', 'id' => $model->uuid]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<?= $this->render('_form', [
        'model' => $model,
    ]) ?>