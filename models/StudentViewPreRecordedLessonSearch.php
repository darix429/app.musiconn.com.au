<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\StudentViewPreRecordedLesson;
use yii\db\Expression;

/**
 * StudentViewPreRecordedLessonSearch represents the model behind the search form of `app\models\StudentViewPreRecordedLesson`.
 */
class StudentViewPreRecordedLessonSearch extends StudentViewPreRecordedLesson {

     public $tutorName;
     public $instrumentName;
     public $studentName;
     public $studentId;
    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['uuid', 'pre_recorded_video_uuid', 'title', 'student_uuid', 'tutor_uuid', 'instrument_uuid', 'datetime','tutorName','instrumentName','studentName','studentId'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $modeDatatable = false, $statusnot = '') {

        $query = StudentViewPreRecordedLesson::find()->select('distinct(pre_recorded_video_uuid),*')->joinWith(['studentUu', 'tutorUu', 'instrumentUu']);


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->andWhere('tutor_uuid IS NOT NULL');
         if (!empty($this->tutorName)) {
            $query->andFilterWhere(['ilike', "concat(tutor.first_name,'',tutor.last_name)", $this->tutorName]);
        }
         if (!empty($this->studentName)) {
            $query->andFilterWhere(['ilike', "concat(student.first_name,'',student.last_name)", $this->studentName]);
        }
        
        if (!empty($this->instrumentName)) {
            $query->andFilterWhere(['ilike', "instrument.name", $this->instrumentName]);
        }
        
 
        
         if (!is_null($this->datetime) && 
            strpos($this->datetime, ' - ') !== false ) {
            list($start_date, $end_date) = explode(' - ', $this->datetime);
            

            $query->andFilterWhere(['>=', '"student_view_pre_recorded_lesson"."datetime"', date("Y-m-d", strtotime($start_date))." 00:00:00"]);
            $query->andFilterWhere(['<=', '"student_view_pre_recorded_lesson"."datetime"', date("Y-m-d", strtotime($end_date))." 23:59:59"]);
        }

        

       

        $query->andFilterWhere(['ilike', 'title', $this->title]);
        if ($modeDatatable) {
            
            $result = $query->asArray()->orderBy('datetime DESC')->all();

            //echo "<pre>"; print_r($result); 
            return $result;
        }
        return $dataProvider;
    }

    
        public function searchDetail($params, $modeDatatable = false, $statusnot = '') {

        $query = StudentViewPreRecordedLesson::find()->joinWith(['studentUu', 'tutorUu', 'instrumentUu']);


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
      

         if (!empty($this->tutor_uuid)) {
            $query->andFilterWhere(['=', '"student_view_pre_recorded_lesson"."tutor_uuid"', $this->tutor_uuid]);
        } else {
            $null = new Expression('NULL');

                        $query->andFilterWhere(['is not', '"student_view_pre_recorded_lesson"."tutor_uuid"', $null]);

        }
         if (!empty($this->studentName)) {
            $query->andFilterWhere(['ilike', "concat(student.first_name,'',student.last_name)", $this->studentName]);
        }
        
         if (!empty($this->studentId)) {
            $query->andFilterWhere(['ilike', "student.enrollment_id", $this->studentId]);
        }
        
        if (!empty($this->instrumentName)) {
            $query->andFilterWhere(['ilike', "instrument.name", $this->instrumentName]);
        }
        
 
        
         if (!is_null($this->datetime) && 
            strpos($this->datetime, ' - ') !== false ) {
            list($start_date, $end_date) = explode(' - ', $this->datetime);
            

            $query->andFilterWhere(['>=', '"student_view_pre_recorded_lesson"."datetime"', date("Y-m-d", strtotime($start_date))." 00:00:00"]);
            $query->andFilterWhere(['<=', '"student_view_pre_recorded_lesson"."datetime"', date("Y-m-d", strtotime($end_date))." 23:59:59"]);
        }

        

       

        $query->andFilterWhere(['ilike', 'title', $this->title])
        ->andFilterWhere(['=', 'pre_recorded_video_uuid', $this->pre_recorded_video_uuid]);
        if ($modeDatatable) {
           // echo $query->createCommand()->getRawSql();exit;
            $result = $query->asArray()->orderBy('datetime DESC')->all();

            //echo "<pre>"; print_r($result); 
            return $result;
        }
        return $dataProvider;
    }

}
