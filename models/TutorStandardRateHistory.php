<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tutor_standard_rate_history".
 *
 * @property string $uuid
 * @property string $tutor_uuid
 * @property string $instrument_uuid
 * @property string $old_option
 * @property string $option
 * @property string $datetime
 * @property string $changed_by_uuid
 * @property string $changed_by_role
 * @property string $action
 * @property string $change_tutor_category
 * @property string $online_tutorial_package_type_uuid
 * @property string $tutor_category_history_uuid
 *
 * @property Instrument $instrumentUu
 * @property OnlineTutorialPackageType $onlineTutorialPackageTypeUu
 * @property Tutor $tutorUu
 * @property TutorCategoryHistory $tutorCategoryHistoryUu
 */
class TutorStandardRateHistory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tutor_standard_rate_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'tutor_uuid', 'option', 'changed_by_uuid', 'changed_by_role', 'online_tutorial_package_type_uuid'], 'required'],
            [['uuid', 'tutor_uuid', 'instrument_uuid', 'old_option', 'option', 'changed_by_uuid', 'changed_by_role', 'action', 'change_tutor_category', 'online_tutorial_package_type_uuid', 'tutor_category_history_uuid'], 'string'],
            [['datetime'], 'safe'],
            [['uuid'], 'unique'],
            [['instrument_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Instrument::className(), 'targetAttribute' => ['instrument_uuid' => 'uuid']],
            [['online_tutorial_package_type_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => OnlineTutorialPackageType::className(), 'targetAttribute' => ['online_tutorial_package_type_uuid' => 'uuid']],
            [['tutor_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Tutor::className(), 'targetAttribute' => ['tutor_uuid' => 'uuid']],
            [['tutor_category_history_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => TutorCategoryHistory::className(), 'targetAttribute' => ['tutor_category_history_uuid' => 'uuid']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'tutor_uuid' => Yii::t('app', 'Tutor Uuid'),
            'instrument_uuid' => Yii::t('app', 'Instrument Uuid'),
            'old_option' => Yii::t('app', 'Old Option'),
            'option' => Yii::t('app', 'Option'),
            'datetime' => Yii::t('app', 'Datetime'),
            'changed_by_uuid' => Yii::t('app', 'Changed By Uuid'),
            'changed_by_role' => Yii::t('app', 'Changed By Role'),
            'action' => Yii::t('app', 'Action'),
            'change_tutor_category' => Yii::t('app', 'Change Tutor Category'),
            'online_tutorial_package_type_uuid' => Yii::t('app', 'Online Tutorial Package Type Uuid'),
            'tutor_category_history_uuid' => Yii::t('app', 'Tutor Category History Uuid'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstrumentUu()
    {
        return $this->hasOne(Instrument::className(), ['uuid' => 'instrument_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOnlineTutorialPackageTypeUu()
    {
        return $this->hasOne(OnlineTutorialPackageType::className(), ['uuid' => 'online_tutorial_package_type_uuid']);
    }
    
  
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTutorUu()
    {
        return $this->hasOne(Tutor::className(), ['uuid' => 'tutor_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTutorCategoryHistoryUu()
    {
        return $this->hasOne(TutorCategoryHistory::className(), ['uuid' => 'tutor_category_history_uuid']);
    }
}
