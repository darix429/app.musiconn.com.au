<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Url;
use app\models\Instrument;
use app\models\TutorialType;
use app\models\PaymentTerm;
use app\models\Tutor;

$this->title = Yii::t('app', 'BOOK MUSIC LESSONS');
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_icon'] = 'my-lesson-tag.png';

$activePaymentTerm = PaymentTerm::find()->where(['status' => 'ENABLED'])->asArray()->all();
/* if (isset($step_data['step_2']) && !empty($step_data['step_2'])) {
  $model->onlinetutorial_uuid = $step_data['step_2']['onlinetutorial_uuid'];
  $model->onlinetutorialcheckbox = $step_data['step_2']['onlinetutorial_uuid'];
  $model->onlinetutorialtype = (!empty($step_data['step_2']['onlinetutorialtype'])) ? $step_data['step_2']['onlinetutorialtype'] : '';
  } */

$tutor_uuid = $step_data['step_2']['tutor_uuid'];
$instrument_uuid = $step_data['step_1']['instrument_uuid'];
$student_uuid = Yii::$app->user->identity->student_uuid;
$tutorDetail = Tutor::findOne($tutor_uuid);
if (!empty($step_data['step_1']['instrument_uuid'])) {
    $instrumentDetail = Instrument::find()->where(['uuid' => $step_data['step_1']['instrument_uuid']])->one();
} else {
    return \yii\web\Controller::redirect(['index']);
}

$this->registerJs(
        '
$(document).on("ifClicked","input[name=\"StudentTuitionBooking[onlinetutorialcheckbox]\"]",function (event) {
    $("#studenttuitionbooking-onlinetutorial_uuid").val(this.value);
    $("#studenttuitionbooking-option").val($(this).attr("data-option"));
    $("#studenttuitionbooking-option_total_price").val($(this).attr("data-price"));
    $(".onlinetutorial_select_btn").addClass("d-none");
    $(this).closest("div.onlinetutorial_tr").find(".onlinetutorial_select_btn").removeClass("d-none");

});

function load_tutorial(typ="",tutor_uuid,instrument_uuid){
    $.ajax({
            type: "GET",
            url: "' . Url::to(['book-music-tution/get-online-tutorial']) . '",
            data: {"type":typ,"tutor_uuid":tutor_uuid,"instrument_uuid":instrument_uuid},
            beforeSend: function() {
                blockBody();
            },
            success:function(result){
                $("#tutorial_list_parent").html(result);
                NEW_CUSTOM.iCheck();
                unblockBody();
            },error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
}

$("input[name=\"StudentTuitionBooking[onlinetutorialtype]\"]").on("change", function (event) {
    load_tutorial($(this).val(),"' . $tutor_uuid . '","'.$instrument_uuid.'");
});

$( "#frm_step_2,#gostep1" ).submit(function( event ) {
        blockBody();
    });
', View::POS_END
);

if (!empty($step_data['step_2']['onlinetutorialtype'])) {
    $this->registerJs(' load_tutorial("' . $step_data['step_2']['onlinetutorialtype'] . '","' . $model->tutor_uuid . '","'.$instrument_uuid.'");', View::POS_END);
}
?>
<section class="content">
    <label class="warning-info mar-bottom-10">For issues concerning tutor availability, please contact <a href="mailto:<?= Yii::$app->params['adminEmail']; ?>"><?= Yii::$app->params['adminEmail']; ?></a> for assistance.</label>
    <div class="row mar-bottom-10">
        <div class="col-xs-12">
            <div class="box">
                <div class="col-md-12 box-panel-header ">
                    <h4 class="col-md-6">Select Instrument &nbsp;<i class="fa fa-check"></i></h4>
                    <div class="col-md-6 ">
                        <?php echo Html::beginForm(['index'], 'post', ['id' => 'gostep1', 'class' => 'form-inline d-block']) ?>
                        <input type="hidden" name="step" value="1">
                        <input type="hidden" name="prev" value="1">
                        <a href="javascript:$('#gostep1').submit();" class="box-panel-header-right-btn pull-right">
                            Change
                        </a>
                        <p class="box-panel-header-right-p text-white pull-right"> <?= $instrumentDetail->name; ?>&nbsp;</p>

                        <?php echo Html::endForm() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row mar-bottom-10">
        <div class="col-xs-12">
            <div class="box">
                <div class="col-md-12 box-panel-header ">
                    <h4 class="col-md-6">Select Tutor &nbsp;<i class="fa fa-check"></i></h4>
                    <div class="col-md-6 ">                        
                        <?php echo Html::beginForm(['index'], 'post', ['id' => 'gostep3', 'class' => 'form-inline d-block']) ?>
                        <input type="hidden" name="step" value="2">
                        <input type="hidden" name="prev" value="2">
                        <a href="javascript:$('#gostep3').submit();" class="box-panel-header-right-btn pull-right">
                            Change
                        </a>
                        <p class="box-panel-header-right-p text-white pull-right"> <?= $tutorDetail->first_name . ' ' . $tutorDetail->last_name; ?>&nbsp;</p>
                        <?php echo Html::endForm() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row mar-bottom-10">
        <div class="col-xs-12">
            <div class="box">
                <div class="col-md-12 box-panel-header border-bottom-radious-none">
                    <h4 class="col-md-6">Select Plan</h4>
                    <div class="col-md-6 "> </div>
                </div>
                <div class="box-body box-panel-body" style="max-height: 500px;overflow-x: hidden;overflow-y: scroll;border-top-left-radius: 0px;border-top-right-radius: 0px;">
                    <div class="row vertical-center-box vertical-center-box-tablet"  style="">
                        <?php
                        $form = ActiveForm::begin([
                                    'action' => ['index'],
                                    'id' => 'frm_step_3',
                                    'enableClientValidation' => true,
//                                'enableAjaxValidation' => true,
                                    'validateOnChange' => true,
                                    'options' => ['class' => 'form-horizontal']]);
                        ?>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <div class="row" style="">
                                    <input type="hidden" name="step" value="3">
                                    <div class=" col-md-2 ">
                                        <?= $form->field($model, 'onlinetutorial_uuid')->hiddenInput(['class' => 'form-control', 'maxlength' => true,])->label(false) ?>
                                    </div>    
                                    <div class=" col-md-2 ">
                                        <?= $form->field($model, 'option')->hiddenInput(['class' => 'form-control', 'maxlength' => true,])->label(false) ?>
                                    </div>    
                                    <div class=" col-md-2 ">                                   
                                        <?= $form->field($model, 'option_total_price')->hiddenInput(['class' => 'form-control', 'maxlength' => true,])->label(false) ?>
                                    </div>
                                </div>
                                <div class="row " >
                                    <div class="col-md-12 ">
                                        <div class="my-inline-button" >
                                            <div class=" text-center" >
                                                <ul class="my-inline-button-filter list-inline btn-group btn-group-toggle " data-toggle="buttons">
                                                    <?php
                                                    if (!empty($activePaymentTerm)) {
                                                        foreach ($activePaymentTerm as $key_pt => $value_pt) {
                                                            ?>
                                                            <li>
                                                                <label class="btn btn-primary <?= ($model->onlinetutorialtype == $value_pt['term']) ? "active" : "" ?>">
                                                                    <input type="radio" name="StudentTuitionBooking[onlinetutorialtype]" class="tutorial_type" autocomplete="off" value="<?= $value_pt['term']; ?>" <?= ($model->onlinetutorialtype == $value_pt['term']) ? "checked" : "" ?> > <?= $value_pt['term']; ?>
                                                                </label>
                                                            </li>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="tutorial_list_parent">
                                    <?php
                                    ?>
                                </div>


                            </div>

                        </div>
                    </div>
                    <?php ActiveForm::end(); ?> 
                </div>
            </div>
        </div>
    </div>



    <div class="row mar-bottom-10">
        <div class="col-xs-12">
            <div class="box">
                <div class="col-md-12 box-panel-header ">
                    <h4 class="col-md-6">Select Date</h4>
                    <div class="col-md-6 "></div>
                </div>
            </div>
        </div>
    </div>

    <div class="row mar-bottom-10">
        <div class="col-xs-12">
            <div class="box">
                <div class="col-md-12 box-panel-header ">
                    <h4 class="col-md-6">Confirm and Payment</h4>
                    <div class="col-md-6 "></div>
                </div>
            </div>
        </div>
    </div>

    <a class="btn btn-primary btn-sm black_button_in" href="<?= Yii::$app->getUrlManager()->createUrl('/booked-session/my-lessons'); ?>">RETURN TO MY LESSONS</a>

</section>