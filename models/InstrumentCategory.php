<?php

namespace app\models;
use Yii;
//rupal
use app\models\PreRecordedVideo;

/**
 * This is the model class for table "{{%instrument_category}}".
 *
 * @property string $uuid
 * @property string $name
 * @property string $parent_id
 * @property int $level
 */
class InstrumentCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%instrument_category}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //html tag filter
            [['name',], 'match', 'pattern' => \app\libraries\General::validTextPattern(['.']), 'message' => 'Invalid charecters used.'], 
            
            [['uuid', 'name', 'parent_id'], 'string'],
            [['name'], 'required'],
	    [['name'],'unique'],
            [['level'], 'default', 'value' => null],
            [['level'], 'integer'],
	    
	    
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'name' => Yii::t('app', 'Name'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'level' => Yii::t('app', 'Level'),
        ];
    }
    
    public function CategoryDelete($id)
    {
        $parent_id = InstrumentCategory::find()->where(['parent_id' => $id])->one();
        
        $uuid = PreRecordedVideo::find()->where(['instrument_category_uuid' => $id])->one();
        $videoId = $uuid['instrument_category_uuid'];
        
        if($parent_id !== null || $videoId !== null){
            return $id;
        } 
        else{
            return false;
        }
    }
    
    
    
    /*public function VideoCategoryDelete($id)
    {
        //print_r($id); exit;
        $id = PreRecordedVideo::find()->where(['uuid' => $id])->one();
        
        $categoryId = InstrumentCategory::find()->where(['uuid' => $videoId])->one();
        //$videoId = $id->instrument_category_uuid;
        echo "<pre>"; print_r($categoryId); exit;
        if($categoryId !== null || $videoId !== null){
            return $id;
        } 
        else{
            return false;
        }
    }*/
    
        
    public function getCategoryTreeHtml($level = null, $prefix = '') {
        
        $rows = InstrumentCategory::find()->where(['parent_id' => $level])->asArray()->all();
        $category = '';
        if (count($rows) > 0) {
            foreach ($rows as $row) {
                $sub_count = InstrumentCategory::find()->where(['parent_id' => $row['uuid']])->count();
                if($sub_count > 0){
                    $category .=  '<li style="padding: 5px;"><span class="caret"><a href="javascript:;" id="video_search" data-uuid="'.$row['uuid'].'"  data-parentid="'.$row['parent_id'].'" data-name="'.$row['name'].'" onclick=videoShow("'.$row['uuid'].'"); class="droppable">' . $row['name'] . "</a></span>";
                    $category .=  '<ul class="nested">';
                    
                    $category .= self::getCategoryTreeHtml($row['uuid'], $prefix . '-');
                    
                    $category .=  '</ul>';
                    $category .=  '</li>';
                }else{
                    
                    $category .=  '<li><a href="javascript:;" id="video_search" data-uuid="'.$row['uuid'].'"  data-parentid="'.$row['parent_id'].'" data-name="'.$row['name'].'" onclick=videoShow("'.$row['uuid'].'"); class="droppable">' . $row['name'] . "</a></li>";
                    // Append subcategories
                    //$category .= $this->getCategoryTree($row->id, $prefix . '-');
                }
            }
        }
        return $category;
    }

}
