<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\libraries\General;
use kartik\mpdf\Pdf;
use yii\helpers\FileHelper;

/**
 * Password reset request form
 */
class Book extends Model {

    public $tutor_uuid, $student_uuid, $instrument_uuid, $instrumentcheckbox, $tutorcheckbox, $lesson_length, $recurring, $lesson_type, $lesson_total, $tutor_half_hour_price, $tutor_half_hour_price_in_coin, $tutor_hour_price, $tutor_hour_price_in_coin, $tutor_further_details;
    public $session_when_start, $all_lesson_data, $start_hour;
    public $cb_tutor_uuid, $cb_student_uuid, $cb_lesson_length, $cb_select_datetime, $cb_session_when_start, $cb_start_hour, $cb_lesson_data;
    public $discount, $coupon_uuid;
    public $cart_total;
    public $coupon_code;
    public $creative_kids_voucher_code, $kids_name, $kids_dob, $kids_postal_code;
    public $tutorial_type_code_checkbox;
    public $term_condition_checkbox;
    public $selected_sesson_html;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['tutor_uuid', 'student_uuid', 'instrument_uuid', 'tutorcheckbox', 'tutor_half_hour_price', 'tutor_half_hour_price_in_coin', 'tutor_hour_price', 'tutor_hour_price_in_coin', 'lesson_type', 'tutor_further_details'], 'safe'],
            [['instrument_uuid'], 'required', 'on' => 'step_1', 'message' => 'Instrument cannot be blank. Select any one Instrument.'],
            [['tutor_uuid'], 'required', 'on' => 'step_2', 'message' => 'Tutor cannot be blank. Select any one Tutor.'],
            [['lesson_length'], 'required', 'on' => 'step_3', 'message' => 'Select lesson length.'],
            [['recurring'], 'required', 'on' => 'step_3', 'message' => 'Select this booking is recurring?'],
            ['recurring', 'default', 'value' => 'NO'],
            ['lesson_type', 'default', 'value' => 'W'],
            [['lesson_type'], 'required', 'on' => 'step_3', 'message' => 'Select this booking is recurring?',
                'when' => function ($model) {
                    return ($model->recurring == 'YES');
                }, 'whenClient' => 'function (attribute, value) { $("input[name=\"Book[recurring]\"]:checked").val() == "YES"; }',
            ],
            ['lesson_type', 'default', 'value' => 1],
            [['lesson_total'], 'required', 'on' => 'step_3', 'message' => 'Select the total number of lessons.',],
            [['session_when_start'], 'required', 'on' => 'step_4', 'message' => 'Select start date.'],
            [['all_lesson_data'], 'required', 'on' => 'step_4', 'message' => 'Please select atleast one lesson time.'],
            [['start_hour'], 'required', 'on' => 'step_4', 'message' => 'Please select lesson time.'],
            [['selected_sesson_html'], 'safe', 'on' => 'step_4', ],
            //Change booking
            [['cb_tutor_uuid', 'cb_student_uuid',], 'required', 'on' => 'new_change_booking',],
            [['cb_lesson_length'], 'required', 'on' => 'new_change_booking', 'message' => 'Select lesson length.'],
            //[['cb_session_when_start'], 'required', 'on' => 'step_4', 'message' => 'Select start date.'],
            [['cb_select_datetime'], 'required', 'on' => 'new_change_booking', 'message' => 'Select lesson time.'],
            [['cb_start_hour'], 'required', 'on' => 'new_change_booking', 'message' => 'Please select lesson time.'],
            [['discount', 'coupon_code', 'creative_kids_voucher_code', 'kids_name', 'kids_dob', 'kids_postal_code'], 'safe', 'on' => 'step_5'],
            [['cart_total'], 'required', 'on' => 'step_5',],
            [['term_condition_checkbox'], 'required', 'on' => 'step_5', 'requiredValue' => 1, 'message' => 'Accept Terms and Condition'],
            [['creative_kids_voucher_code', 'kids_name', 'kids_dob', 'kids_postal_code'], 'creativeCodeCriteria', 'on' => 'step_5'],
            [['coupon_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Coupon::className(), 'targetAttribute' => ['coupon_uuid' => 'uuid']],
            [['creative_kids_voucher_code', 'kids_name', 'coupon_code'], 'match', 'pattern' => \app\libraries\General::validTextPattern(), 'message' => 'Invalid charecters used.'],
                        
            [['tutor_uuid', 'student_uuid'], 'required', 'on' => 'lesson_reschedule'],
            [['session_when_start'], 'required', 'on' => 'lesson_reschedule', 'message' => 'Select start date.'],
            [['all_lesson_data'], 'required', 'on' => 'lesson_reschedule', 'message' => 'Please select atleast one lesson time.'],
            [['start_hour'], 'required', 'on' => 'lesson_reschedule', 'message' => 'Please select lesson time.'],
            [['selected_sesson_html'], 'safe', 'on' => 'lesson_reschedule', ],
        ];
    }

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['step_1'] = ['instrument_uuid', 'instrumentcheckbox'];
        $scenarios['step_2'] = ['tutor_uuid', 'tutorcheckbox'];
        $scenarios['step_3'] = ['lesson_length', 'recurring', 'lesson_type', 'lesson_total'];
        $scenarios['step_4'] = ['session_when_start', 'start_hour', 'all_lesson_data'];
        $scenarios['step_5'] = ['discount', 'cart_total', 'coupon_code', 'term_condition_checkbox', 'creative_kids_voucher_code', 'kids_name', 'kids_postal_code', 'kids_dob'];
        $scenarios['new_change_booking'] = ['cb_tutor_uuid', 'cb_student_uuid', 'cb_session_when_start', 'cb_start_hour', 'cb_lesson_data'];
        $scenarios['lesson_reschedule'] = ['session_when_start', 'start_hour', 'all_lesson_data', 'tutor_uuid', 'student_uuid'];

        return $scenarios;
    }

    public function attributeLabels() {
        return [
            'cb_tutor_uuid' => Yii::t('app', 'Select Tutor'),
            'cb_student_uuid' => Yii::t('app', 'Student'),
            'cb_lesson_length' => Yii::t('app', 'Lesson Length'),
            'cb_select_datetime' => Yii::t('app', 'Schedule Time'),
            'cb_session_when_start' => Yii::t('app', 'Lesson Start Date'),
	    'creative_kids_voucher_code' => Yii::t('app', 'Voucher Code'),
	    'kids_name' => Yii::t('app', 'Name'),
	    'kids_postal_code' => Yii::t('app', 'Postal Code'),
	    'kids_dob' => Yii::t('app', 'Date of Birth'),
            
        ];
    }

    public static function creativeCodeCriteria() {
        if (
                (!empty($this->creative_kids_voucher_code) && ( empty($this->kids_name) || empty($this->kids_dob) || empty($this->kids_postal_code))) || (!empty($this->kids_name) && ( empty($this->creative_kids_voucher_code) || empty($this->kids_dob) || empty($this->kids_postal_code))) || (!empty($this->kids_dob) && ( empty($this->kids_name) || empty($this->creative_kids_voucher_code) || empty($this->kids_postal_code))) || (!empty($this->kids_postal_code) && ( empty($this->kids_name) || empty($this->creative_kids_voucher_code) || empty($this->kids_dob)))
        ) {

            if (empty($this->creative_kids_voucher_code)) {
                $this->addError('creative_kids_voucher_code', 'Voucher Code Required.');
            }

            if (empty($this->kids_name)) {
                $this->addError('kids_name', 'Name Required.');
            }

            if (empty($this->kids_dob)) {
                $this->addError('kids_dob', 'Date of Birth Required.');
            }
            if (empty($this->kids_postal_code)) {
                $this->addError('kids_postal_code', 'Postal Code Required.');
            }
        }
    }

    public static function get_tutor_available_time_period(
    $tutor_uuid, $working_plan, $selected_date, $student_uuid = '', $tutor_tz, $student_tz, $logged_user_tz = '') {

        $logged_user_day_start_time = $selected_date . ' 00:00';
        $logged_user_day_end_time = $selected_date . ' 23:59';

        $system_tz = Yii::$app->params['timezone'];
        $logged_user_tz = !empty($logged_user_tz) ? $logged_user_tz : Yii::$app->params['timezone'];
        $day_start_tutor = General::convertTimezone($selected_date . ' 00:00', $logged_user_tz, $tutor_tz, 'Y-m-d H:i');
        $day_end_tutor = General::convertTimezone($selected_date . ' 23:59', $logged_user_tz, $tutor_tz, 'Y-m-d H:i');

        $tutor_start_day_working_plan = $working_plan[strtolower(date('l', strtotime($day_start_tutor)))];
        $tutor_start_day_breaks = (isset($tutor_start_day_working_plan['breaks'])) ? $tutor_start_day_working_plan['breaks'] : [];

        $tutor_end_day_working_plan = $working_plan[strtolower(date('l', strtotime($day_end_tutor)))];
        $tutor_end_day_breaks = (isset($tutor_end_day_working_plan['breaks'])) ? $tutor_end_day_working_plan['breaks'] : [];

        $periods = [];
        $periods_start = [];
        $periods_end = [];

        /*         * ***tutor start date array*** */
        if (isset($tutor_start_day_breaks) && isset($tutor_start_day_working_plan)) {

            $st = ( (strtolower(date('Y-m-d', strtotime($day_start_tutor))) . ' ' . $tutor_start_day_working_plan['start']) > $day_start_tutor ) ? strtolower(date('Y-m-d', strtotime($day_start_tutor))) . ' ' . $tutor_start_day_working_plan['start'] : $day_start_tutor;
            $et = ( (strtolower(date('Y-m-d', strtotime($day_start_tutor))) . ' ' . $tutor_start_day_working_plan['end']) > $st ) ? (strtolower(date('Y-m-d', strtotime($day_start_tutor))) . ' ' . $tutor_start_day_working_plan['end']) : $st;
            $periods_start[] = [
                'start' => $st,
                'end' => $et
            ];

            $day_start = new \DateTime($st, new \DateTimeZone($tutor_tz));
            $day_end = new \DateTime($et, new \DateTimeZone($tutor_tz));

            if (!empty($tutor_start_day_breaks)) {
                foreach ($tutor_start_day_breaks as $index => $break) {


                    $break_start = new \DateTime(date('Y-m-d', strtotime($day_start_tutor)) . ' ' . $break['start'], new \DateTimeZone($tutor_tz));
                    $break_end = new \DateTime(date('Y-m-d', strtotime($day_start_tutor)) . ' ' . $break['end'], new \DateTimeZone($tutor_tz));

                    if ($break_start < $day_start) {
                        $break_start = $day_start;
                    }

                    if ($break_end > $day_end) {
                        $break_end = $day_end;
                    }

                    if ($break_start >= $break_end) {
                        continue;
                    }

                    foreach ($periods_start as $key => $period) {
                        $period_start = new \DateTime($period['start'], new \DateTimeZone($tutor_tz));
                        $period_end = new \DateTime($period['end'], new \DateTimeZone($tutor_tz));

                        $remove_current_period = FALSE;

                        if ($break_start > $period_start && $break_start < $period_end && $break_end > $period_start) {
                            $periods_start[] = [
                                'start' => $period_start->format('Y-m-d H:i'),
                                'end' => $break_start->format('Y-m-d H:i')
                            ];

                            $remove_current_period = TRUE;
                        }

                        if ($break_start < $period_end && $break_end > $period_start && $break_end < $period_end) {
                            $periods_start[] = [
                                'start' => $break_end->format('Y-m-d H:i'),
                                'end' => $period_end->format('Y-m-d H:i')
                            ];

                            $remove_current_period = TRUE;
                        }

                        if ($break_start == $period_start && $break_end == $period_end) {
                            $periods_start[] = [
                                'start' => $period_start->format('Y-m-d H:i'),
                                'end' => $break_start->format('Y-m-d H:i')
                            ];

                            $remove_current_period = TRUE;
                        }

                        if ($remove_current_period) {
                            unset($periods_start[$key]);
                        }
                    }
                }
            }
        }

        /*         * ***tutor end date array*** */
        if (isset($tutor_end_day_breaks) && isset($tutor_end_day_working_plan)) {

            $st = ( (strtolower(date('Y-m-d', strtotime($day_end_tutor))) . ' ' . $tutor_end_day_working_plan['start']) < $day_end_tutor ) ? strtolower(date('Y-m-d', strtotime($day_end_tutor))) . ' ' . $tutor_end_day_working_plan['start'] : $day_end_tutor;
            $et = ( (strtolower(date('Y-m-d', strtotime($day_end_tutor))) . ' ' . $tutor_end_day_working_plan['end']) > $st ) ? (strtolower(date('Y-m-d', strtotime($day_end_tutor))) . ' ' . $tutor_end_day_working_plan['end']) : $st;
            $periods_end[] = [
                'start' => $st,
                'end' => $et
            ];
//            echo "<pre>";
//            print_r((strtolower(date('Y-m-d', strtotime($day_end_tutor))).' '.$tutor_end_day_working_plan['end']));

            $day_start = new \DateTime($periods_end[0]['start'], new \DateTimeZone($tutor_tz));
            $day_end = new \DateTime($periods_end[0]['end'], new \DateTimeZone($tutor_tz));

            if (!empty($tutor_end_day_breaks)) {
                foreach ($tutor_end_day_breaks as $index => $break) {


                    $break_start = new \DateTime(date('Y-m-d', strtotime($day_end_tutor)) . ' ' . $break['start'], new \DateTimeZone($tutor_tz));
                    $break_end = new \DateTime(date('Y-m-d', strtotime($day_end_tutor)) . ' ' . $break['end'], new \DateTimeZone($tutor_tz));

                    //$break_start = new \DateTime($break['start'],new \DateTimeZone($tutor_tz));
                    //$break_end = new \DateTime($break['end'],new \DateTimeZone($tutor_tz));

                    if ($break_start < $day_start) {
                        $break_start = $day_start;
                    }

                    if ($break_end > $day_end) {
                        $break_end = $day_end;
                    }

                    if ($break_start >= $break_end) {
                        continue;
                    }

                    foreach ($periods_end as $key => $period) {
                        $period_start = new \DateTime($period['start'], new \DateTimeZone($tutor_tz));
                        $period_end = new \DateTime($period['end'], new \DateTimeZone($tutor_tz));

                        $remove_current_period = FALSE;

                        if ($break_start > $period_start && $break_start < $period_end && $break_end > $period_start) {
                            $periods_end[] = [
                                'start' => $period_start->format('Y-m-d H:i'),
                                'end' => $break_start->format('Y-m-d H:i')
                            ];

                            $remove_current_period = TRUE;
                        }

                        if ($break_start < $period_end && $break_end > $period_start && $break_end < $period_end) {
                            $periods_end[] = [
                                'start' => $break_end->format('Y-m-d H:i'),
                                'end' => $period_end->format('Y-m-d H:i')
                            ];

                            $remove_current_period = TRUE;
                        }

                        if ($break_start == $period_start && $break_end == $period_end) {
                            $periods_end[] = [
                                'start' => $period_start->format('Y-m-d H:i'),
                                'end' => $break_start->format('Y-m-d H:i')
                            ];

                            $remove_current_period = TRUE;
                        }

                        if ($remove_current_period) {
                            unset($periods_end[$key]);
                        }
                    }
                }
            }
        }

        $tutor_all_periods = array_merge($periods_start, $periods_end);

        /*         * ****** $periods variable converted to sytem timezone wise event ******** */
        $periods = self::periodConvertTimezoneWise($tutor_all_periods, $tutor_tz, $system_tz);


        // Break the empty periods with the reserved booked session.
        $tutor_appointments = self::get_tutor_booked_appointments($tutor_uuid);
        $tutor_unavailability = self::get_tutor_unavailability($tutor_uuid);
        $student_appointments = (!empty($student_uuid)) ? self::get_student_booked_appointments($student_uuid) : [];
        $reschedule_appointments = self::get_reschedule_appointments($tutor_uuid);

        $provider_appointments_unavailability = array_merge($tutor_appointments, $tutor_unavailability);
        $provider_appointments_all = array_merge($provider_appointments_unavailability, $student_appointments);
        $provider_appointments_all_final = array_merge($provider_appointments_all, $reschedule_appointments);
        $provider_appointments = self::add_break_after_lesson($provider_appointments_all_final);
        //echo "<pre>provider_appointments";print_r($provider_appointments);exit;

        foreach ($provider_appointments as $provider_appointment) {
            foreach ($periods as $index => &$period) {

                $appointment_start = new \DateTime($provider_appointment['start_datetime']);
                $appointment_end = new \DateTime($provider_appointment['end_datetime']);
                $period_start = new \DateTime($period['start']);
                $period_end = new \DateTime($period['end']);

                if ($appointment_start <= $period_start && $appointment_end <= $period_end && $appointment_end <= $period_start) {
                    // The appointment does not belong in this time period, so we  will not change anything.
                } else {
                    if ($appointment_start <= $period_start && $appointment_end <= $period_end && $appointment_end >= $period_start) {
                        // The appointment starts before the period and finishes somewhere inside. We will need to break
                        // this period and leave the available part.
                        $period['start'] = $appointment_end->format('Y-m-d H:i');
                    } else {
                        if ($appointment_start >= $period_start && $appointment_end < $period_end) {
                            // The appointment is inside the time period, so we will split the period into two new
                            // others.
                            unset($periods[$index]);

                            $periods[] = [
                                'start' => $period_start->format('Y-m-d H:i'),
                                'end' => $appointment_start->format('Y-m-d H:i')
                            ];

                            $periods[] = [
                                'start' => $appointment_end->format('Y-m-d H:i'),
                                'end' => $period_end->format('Y-m-d H:i')
                            ];
                        } else if ($appointment_start == $period_start && $appointment_end == $period_end) {
                            unset($periods[$index]); // The whole period is blocked so remove it from the available periods array.
                        } else {
                            if ($appointment_start >= $period_start && $appointment_end >= $period_start && $appointment_start <= $period_end) {
                                // The appointment starts in the period and finishes out of it. We will need to remove
                                // the time that is taken from the appointment.
                                $period['end'] = $appointment_start->format('Y-m-d H:i');
                            } else {
                                if ($appointment_start >= $period_start && $appointment_end >= $period_end && $appointment_start >= $period_end) {
                                    // The appointment does not belong in the period so do not change anything.
                                } else {
                                    if ($appointment_start <= $period_start && $appointment_end >= $period_end && $appointment_start <= $period_end) {
                                        // The appointment is bigger than the period, so this period needs to be removed.
                                        unset($periods[$index]);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $result_all_available_period_system_tz = array_values($periods);

        /*         * ****** $result variable converted to sytem timezone to student tz wise event ******** */
        $result = self::periodConvertTimezoneWise($result_all_available_period_system_tz, $system_tz, $logged_user_tz, "Y-m-d H:i");

        /*         * ***logged user date data**** */
        $final_result = [];
        foreach ($result as $result_key => $result_value) {

            $l_start = new \DateTime($logged_user_day_start_time);
            $l_end = new \DateTime($logged_user_day_end_time);
            $r_start = new \DateTime($result_value['start']);
            $r_end = new \DateTime($result_value['end']);

            if ($l_start->format('Y-m-d') > $r_start->format('Y-m-d')) {
                $s = $l_start->format('Y-m-d 00:00');
            } elseif ($l_start->format('Y-m-d') < $r_start->format('Y-m-d')) {
                $s = $l_start->format('Y-m-d 23:59');
            } else {
                $s = $result_value['start'];
            }

            if ($l_end->format('Y-m-d') > $r_end->format('Y-m-d')) {
                $e = $e_end->format('Y-m-d 00:00');
            } elseif ($l_end->format('Y-m-d') < $r_end->format('Y-m-d')) {
                $e = $l_end->format('Y-m-d 23:59');
            } else {
                $e = $result_value['end'];
            }

            $final_result[] = ['start' => $s, 'end' => $e];
        }

        return $final_result;
    }

    public static function get_final_hours(
    array $all_lesson_data, $selected_date, $service_duration, $manage_mode = FALSE, $availabilities_type = 'flexible', $tutor_tz, $student_tz, array $get_hours) {
        $final_result = [];
        $duration = ($service_duration == '60') ? '30' : '15';
        usort($all_lesson_data, function($a, $b) {
            return strtotime($a['lesson_time']) - strtotime($b['lesson_time']);
        });

        foreach ($get_hours as $value) {
            // echo $value;
            $dasdasd = $value;
            $start_after_duration = date('Y-m-d H:i', strtotime("+" . $duration . " minutes", strtotime($value)));
            foreach ($all_lesson_data as $keee) {
                //if (date("Y-m-d", strtotime($selected_date)) == date("Y-m-d", strtotime($keee['lesson_time']))) {
                if (((date('Y-m-d H:i', strtotime($dasdasd)) <= date('Y-m-d H:i', strtotime($keee['lesson_time'])) && date('Y-m-d H:i', strtotime($start_after_duration)) >= date('Y-m-d H:i', strtotime($keee['lesson_time']))))) {
                    $final_result[] = $keee['lesson_time'];
//                       print_r($keee);
                    //echo date('Y-m-d H:i',strtotime($dasdasd)) .'<='.  date('Y-m-d H:i',strtotime($keee['lesson_time'])) .'&&'. date('Y-m-d H:i',strtotime($start_after_duration)) .'>='. date('Y-m-d H:i',strtotime($keee['lesson_time'])).'<br>';
                } elseif ((date('Y-m-d H:i', strtotime($dasdasd)) <= date('Y-m-d H:i', strtotime("+60 minutes", strtotime($keee['lesson_time']))) && date('Y-m-d H:i', strtotime($start_after_duration)) >= date('Y-m-d H:i', strtotime("+60 minutes", strtotime($keee['lesson_time']))))) {

                    break;
                }
                //}
            }
        }
        //echo "<pre>";
        // print_r($tertert);
        //echo "</pre>";
//         echo count($get_hours);
        $tertert = array();
        $match = false;
        foreach ($final_result as $rrrrr) {
            $s_time = $rrrrr;
            //$e_time = date('Y-m-d H:i', strtotime("+30 minutes", strtotime($s_time)));

            $start_before_duration = date('Y-m-d H:i', strtotime("-" . $duration . " minutes", strtotime($s_time)));
            $start_after_duration = date('Y-m-d H:i', strtotime("+" . $duration . " minutes", strtotime($s_time)));
            $tttt = 0;

            $pos = array_search($s_time, $get_hours);
            unset($get_hours[$pos]);
            $pos = array_search($start_before_duration, $get_hours);
            unset($get_hours[$pos]);
            $pos = array_search($start_after_duration, $get_hours);
            unset($get_hours[$pos]);
        }

        $final_result = $get_hours;


        return $final_result;
    }

    //-------- Get available hours----------------------
    public static function get_available_hours($tutor_uuid, $tutor_working_plan, $session_date, $session_minute, $student_uuid = '', $tutor_tz, $student_tz, $logged_user_tz) {
        $system_tz = Yii::$app->params['timezone'];
        $session_date = date('Y-m-d', strtotime($session_date));
        $empty_periods = self::get_tutor_available_time_period($tutor_uuid, $tutor_working_plan, $session_date, $student_uuid, $tutor_tz, $student_tz, $logged_user_tz);

        $available_hours = self::_calculate_available_hours($empty_periods, $session_date, $session_minute, filter_var(false, FILTER_VALIDATE_BOOLEAN), 'flexible', $tutor_tz, $student_tz);

        // If the selected date is today, remove past hours. It is important  include the timeout before
        // booking that is set in the back-office the system. Normally we might want the customer to book
        // an appointment that is at least half or one hour from now. The setting is stored in minutes.

        if ($session_date <= General::convertTimezone(date('Y-m-d H:i'), $system_tz, $logged_user_tz, 'Y-m-d')) {
            $book_advance_timeout = Yii::$app->params['book_advance_timeout'];

            foreach ($available_hours as $index => $value) {

                $current_logg_user_time = General::convertTimezone(date('Y-m-d H:i'), $system_tz, $logged_user_tz, 'Y-m-d H:i');
                $available_hour = strtotime($value);
                $current_hour = strtotime('+' . $book_advance_timeout . ' minutes', strtotime($current_logg_user_time));
                if ($available_hour <= $current_hour) {
                    unset($available_hours[$index]);
                }
            }
        }

        $available_hours = array_values($available_hours);
        sort($available_hours, SORT_STRING);
        $available_hours = array_values($available_hours);

        return $available_hours;
    }

    //--------Calculate available hours slots----------------------
    protected static function _calculate_available_hours(
    array $empty_periods, $selected_date, $service_duration, $manage_mode = FALSE, $availabilities_type = 'flexible', $tutor_tz, $student_tz
    ) {

        $available_hours = [];

        foreach ($empty_periods as $period) {

            $start_hour = new \DateTime($period['start']);
            $end_hour = new \DateTime($period['end']);
//            $start_hour = new \DateTime($period['start'], new \DateTimeZone($student_tz));
//            $end_hour = new \DateTime($period['end'], new \DateTimeZone($student_tz));

            $interval = $availabilities_type === Yii::$app->params['availabilities_type_fixed'] ? (int) $service_duration : 30;

            $current_hour = $start_hour;
            $diff = $current_hour->diff($end_hour);

//            while (($diff->h * 60 + $diff->i) >= intval($service_duration)) { //Alpesh when 23:59 time set
            while (($diff->h * 60 + $diff->i) >= intval($service_duration) || ($diff->h * 60 + $diff->i) >= intval($service_duration - 1)) {
                $available_hours[] = $current_hour->format('Y-m-d H:i');
                $current_hour->add(new \DateInterval('PT' . $interval . 'M'));
                $diff = $current_hour->diff($end_hour);
            }
        }

        return $available_hours;
    }

    //--------Time period convert from to To timezone with date format----------------------
    public static function periodConvertTimezoneWise($periods, $from_tz, $to_tz, $format = 'Y-m-d H:i T') {
        $return = [];
        if (!empty($periods)) {
            foreach ($periods as $key => $value) {
                $return[] = ['start' => General::convertTimezone($value['start'], $from_tz, $to_tz, $format), 'end' => General::convertTimezone($value['end'], $from_tz, $to_tz, $format)];
            }
        }
        return $return;
    }

    //--------Get list of already booked Tutor's appointment----------------------
    public static function get_tutor_booked_appointments($tutor_uuid) {

        $model = BookedSession::find()->select('*, \'Yes\' AS "is_tutor_appointment"')->where(['tutor_uuid' => $tutor_uuid, 'status' => 'SCHEDULE'])->asArray()->all();
        if (!empty($model)) {
            return $model;
        } else {
            return [];
        }
    }

    //--------Get list of Tutor's unavailable time, which is define in to his profile ----------------------
    public static function get_tutor_unavailability($tutor_uuid, $only_future = true) {

        $where = ($only_future) ? ' AND ((to_char(start_datetime, \'YYYY-MM-DD\') >= \'' . date('Y-m-d') . '\') OR  (to_char(end_datetime, \'YYYY-MM-DD\') >= \'' . date('Y-m-d') . '\'))' : "";
        $query = 'SELECT *, \'Yes\' AS "is_tutor_unavailability" FROM "tutor_unavailability" WHERE ("tutor_uuid"= \'' . $tutor_uuid . '\') ' . $where;
        $command = Yii::$app->db->createCommand($query);
        $model = $command->queryAll();

        if (!empty($model)) {
            return $model;
        } else {
            return [];
        }
    }

    //--------Get list of Tutor lesson's reschedule AWAITING requests ----------------------
    public static function get_reschedule_appointments($tutor_uuid) {

        $query = 'SELECT *, "to_start_datetime" AS "start_datetime", "to_end_datetime" AS "end_datetime" FROM "reschedule_session" WHERE ("tutor_uuid"= \'' . $tutor_uuid . '\') AND "status" = \'AWAITING\' ';
        $command = Yii::$app->db->createCommand($query);
        $model = $command->queryAll();

        if (!empty($model)) {
            return $model;
        } else {
            return [];
        }
    }

    //--------Get list of already booked Student's appointment ----------------------
    public static function get_student_booked_appointments($student_uuid) {

        $model = BookedSession::find()->where(['student_uuid' => $student_uuid, 'status' => 'SCHEDULE'])->asArray()->all();
        if (!empty($model)) {
            return $model;
        } else {
            return [];
        }
    }

    //--------Add break after lesson. Means add rest time after lesson complete. ----------------------
    public static function add_break_after_lesson($provider_appointments) {
        $return = [];
        if (!empty($provider_appointments)) {
            foreach ($provider_appointments as $key => $app) {
                if (isset($app['is_tutor_appointment']) && $app['is_tutor_appointment'] == 'Yes') {

                    $appointment_between_gap_min = Yii::$app->params['interval_after_lesson'];
                    $app['end_datetime'] = date('Y-m-d H:i:s', strtotime($app['end_datetime'] . " +" . $appointment_between_gap_min . " minutes"));
                }
                $return[] = $app;
            }
        }
        return $return;
    }

    //--------Get all lesson detail for selected date ----------------------
    public static function get_lesson_for_booking($selected_time, $student_uuid, $tutor_uuid, $lesson_minute = 30, $lesson_total = 1, $bitween_days = 7) {
        $return = [];
        if (!empty($selected_time) && !empty($tutor_uuid) && !empty($student_uuid)) {

            $tutorDetail = Tutor::findOne($tutor_uuid);
            $tutor_working_plan = json_decode($tutorDetail->working_plan, true);
            $tutor_tz = General::getTutorTimezone($tutor_uuid);
            $student_tz = General::getStudentTimezone($student_uuid);
            $logged_user_tz = Yii::$app->user->identity->timezone;

            $selected_date = date('Y-m-d', strtotime($selected_time));
            $only_time = date('H:i', strtotime($selected_time));
            if ($selected_date != '') {
                $next = false;
                $start_date = $selected_date;
            }
            $start_date = date('Y-m-d', strtotime($start_date));
            for ($i = 0; $i < $lesson_total; $i++) {

                //------get lesson date
                if ($next) {
                    $session_date = date('Y-m-d', strtotime($start_date . ' +' . $bitween_days . ' day'));
                } else {
                    $session_date = $start_date;
                }
                $start_date = $session_date;
                $next = true;

                $get_hours = Book::get_available_hours($tutor_uuid, $tutor_working_plan, $session_date, $lesson_minute, $student_uuid, $tutor_tz, $student_tz, $logged_user_tz);

                $check_final_time = $session_date . ' ' . $only_time;


                if (in_array($check_final_time, $get_hours)) {
                    $avaibility = ['is_available' => "YES", 'lesson_time' => $check_final_time];
                } else {
                    $avaibility = ['is_available' => "NO", 'lesson_time' => $check_final_time];
                }

                $return[] = array_merge($avaibility, ['tutor_uuid' => $tutor_uuid, 'lesson_length' => $lesson_minute]);
            }
        }

        return $return;
    }

    public static function make_cart_submit_array($postdata = []) {

        $musicoin_array = [];
        $student_detail = Student::findOne(Yii::$app->user->identity->student_uuid);
        $token = "NEWBOOK_" . Yii::$app->user->identity->student_uuid;
        $step_data = StudentTuitionBooking::getSession($token);
//        print_r($postdata);exit;
        if (isset($postdata['Book']) && (isset($step_data) && !empty($step_data))) {
            // $musicoin_array = ['stripeToken' => $postdata['stripeToken']];
//            print_r($step_data);exit;

            $cart_total = 0;
            $discount = 0;
//            $monthly_sub_total = 0;
//
//            if ($student_detail->isMonthlySubscription == 1) {
//                $monthly_sub_check = '0';
//            } else {
//                $monthly_sub_check = $postdata['StudentTuitionBooking']['monthly_sub_checkbox'];
//            }
//
//            $monthly_sub_uuid = $postdata['StudentTuitionBooking']['monthly_sub_uuid'];
            $coupon_code = $postdata['Book']['coupon_code'];


//            $tutorDetail = Tutor::findOne($step_data['step_2']['tutor_uuid']);
//
//            $package_type_model = OnlineTutorialPackageType::find()->where(['uuid' => $tutorDetail->online_tutorial_package_type_uuid])->one();
//            $package_type = $package_type_model->name;
//            $packageDetail = PremiumStandardPlanRate::find()->joinWith(['onlineTutorialPackageUu'])->where(['premium_standard_plan_rate.uuid' => $step_data['step_3']['onlinetutorial_uuid']])->one();
            if (!is_array($step_data['step_2']['tutor_further_details'])) {
                $step_data['step_2']['tutor_further_details'] = json_decode($step_data['step_2']['tutor_further_details'], true);
            }

            $lesson_length_word = ($step_data['step_3']['lesson_length'] == 30) ? "half" : "full";
            if ($step_data['step_2']['tutor_further_details']['package_type'] == "PREMIUM") {

                $plan_price = $step_data['step_2']['tutor_further_details'][$lesson_length_word];
            } else {

                //$planDetail = StandardPlanOptionRate::find()->where(['premium_standard_plan_rate_uuid' => $step_data['step_3']['onlinetutorial_uuid'], 'option' => $step_data['step_3']['option']])->one();
                $plan_price = $step_data['step_2']['tutor_further_details'][$lesson_length_word];
                //$plan_base_price = $planDetail->price;
                $plan_option = $step_data['step_2']['tutor_further_details']['option'][$lesson_length_word];

                //Get id of tutorwise paln 
//                $tutorwise_model = OnlineTutorialPackageTutorwise::find()->where(['premium_standard_plan_rate_uuid' => $step_data['step_3']['onlinetutorial_uuid'], 'tutor_uuid' => $step_data['step_2']['tutor_uuid'], 'instrument_uuid' => $step_data['step_1']['instrument_uuid']])->one();
//                $online_tutorial_package_tutorwise_uuid = $tutorwise_model->uuid;
                //Standard tutor loking period
//                $locking_per_model_count = StudentTuitionStandardBookingLockingPeriod::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid, 'tutor_uuid' => $step_data['step_2']['tutor_uuid'], 'instrument_uuid' => $step_data['step_1']['instrument_uuid']])->count();
//                if ($locking_per_model_count > 0) {
//
//                    $locking_per_model = StudentTuitionStandardBookingLockingPeriod::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid, 'tutor_uuid' => $step_data['step_2']['tutor_uuid'], 'instrument_uuid' => $step_data['step_1']['instrument_uuid']])->one();
//                    $locking_end_datetime = $locking_per_model->locking_end_datetime;
//                    $locking_start_datetime = $locking_per_model->locking_start_datetime;
//                    $locking_option = $locking_per_model->option;
//
//                    $current_date = date('Y-m-d H:i:s');
//
//                    if (($plan_option < $locking_option) && ($current_date <= $locking_end_datetime)) {
//                        $lockingplanDetail = StandardPlanOptionRate::find()->where(['premium_standard_plan_rate_uuid' => $step_data['step_3']['onlinetutorial_uuid'], 'option' => $locking_option])->one();
//                        $plan_price = $lockingplanDetail->total_price;
//                    }
//                }
            }

//            if ($monthly_sub_check == '1') {
//                //$monthly_model = MonthlySubscriptionFee::find()->where(['with_tutorial' => 1, 'status' => 'ENABLED'])->one();//change by pooja for comman price for manthly subscription
//                $monthly_model = MonthlySubscriptionFee::find()->where(['status' => 'ENABLED'])->one();
//                if (!empty($monthly_model)) {
//                    $monthly_sub_total = $monthly_model->total_price;
//                }
//            }

            $model_coupon = StudentTuitionBooking::check_coupon_available($coupon_code, $plan_price);
            if ($model_coupon['is_available']) {
                $discount = $model_coupon['discount'];
            }

//            $cart_total = (($plan_price + $monthly_sub_total ) - $discount);
            $cart_total = ($plan_price - $discount);

//            $musicoin_array['monthly_sub_checkbox'] = $monthly_sub_check;
            $musicoin_array['coupon_code'] = $coupon_code;
            $musicoin_array['creative_kids_voucher_code'] = $postdata['Book']['creative_kids_voucher_code'];
            $musicoin_array['kids_name'] = $postdata['Book']['kids_name'];
            $musicoin_array['kids_dob'] = $postdata['Book']['kids_dob'];
            $musicoin_array['coupon_uuid'] = $postdata['Book']['coupon_uuid'];
//            $musicoin_array['monthly_sub_uuid'] = $monthly_sub_uuid;
//            $musicoin_array['monthly_sub_total'] = $monthly_sub_total;
            $musicoin_array['discount'] = $postdata['Book']['discount'];
            $musicoin_array['cart_total'] = $postdata['Book']['cart_total'];
            $musicoin_array['plan_name'] = $step_data['step_2']['tutor_further_details']['name'][$lesson_length_word];
            $musicoin_array['plan_price'] = $step_data['step_2']['tutor_further_details'][$lesson_length_word];
            $musicoin_array['student_ID'] = $student_detail->enrollment_id;
            $musicoin_array['student_name'] = $student_detail->first_name . ' ' . $student_detail->last_name;
        }

        return $musicoin_array;
    }

    public static function musicoinPayment($musicoin_array) {

        $return = ['code' => 421, 'message' => 'Something went wrong.'];
        $student = Student::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid])->one();
        $token = "NEWBOOK_" . Yii::$app->user->identity->student_uuid;
        $step_data = StudentTuitionBooking::getSession($token);
//print_r($musicoin_array['cart_total']);exit;
        if (isset($musicoin_array['cart_total'])) { // && !empty($musicoin_array['cart_total'])

            // $token = $musicoin_array['stripeToken'];
            $email = $student->email;
            $orderID = General::generateTransactionID();
            $count = json_decode($step_data['step_4']['all_lesson_data'], true);
            $available = 0;
            $unavailable = 0;

            foreach ($count as $value) {
                if ($value['is_available'] == "YES") {

                    $available++;
                } else {
                    $unavailable++;
                }
            }
            // exit;
            //$dollar_amount = (int) $musicoin_array['cart_total'];
            $dollar_amount = (float) $musicoin_array['cart_total'];

            if (true) {
                if (!is_array($step_data['step_2']['tutor_further_details'])) {
                    $step_data['step_2']['tutor_further_details'] = json_decode($step_data['step_2']['tutor_further_details'], true);
                }
                $lesson_length_word = ($step_data['step_3']['lesson_length'] == 30) ? "half" : "full";
                //print_r($planDetail);
                // exit;
                $planDetail = PremiumStandardPlanRate::find()->joinWith(['paymentTermUu', 'tutorialTypeUu'])->where(['premium_standard_plan_rate.uuid' => $step_data['step_2']['tutor_further_details']['uuid'][$lesson_length_word]])->one();
//                $tutorDetail = Tutor::findOne($step_data['step_2']['tutor_uuid']);
//echo "<pre>";print_r($planDetail->tutorialTypeUu->code);echo "</pre>";exit;
//                $package_type_model = OnlineTutorialPackageType::find()->where(['uuid' => $tutorDetail->online_tutorial_package_type_uuid])->one();
//                $package_type = $package_type_model->name;
                $packageDetail = PremiumStandardPlanRate::find()->joinWith(['onlineTutorialPackageUu'])->where(['premium_standard_plan_rate.uuid' => $step_data['step_2']['tutor_further_details']['uuid'][$lesson_length_word]])->one();

                if ($step_data['step_2']['tutor_further_details']['package_type'] == "PREMIUM") {
                    $plan_total_price = $step_data['step_2']['tutor_further_details'][$lesson_length_word];
                    $plan_price = $step_data['step_2']['tutor_further_details']['price'][$lesson_length_word];
                    $plan_gst = $step_data['step_2']['tutor_further_details']['gst'][$lesson_length_word];
                    $plan_gst_description = $step_data['step_2']['tutor_further_details']['gst_description'][$lesson_length_word];
                    $plan_option = 0;
                } else {
                    //$planoptionDetail = StandardPlanOptionRate::find()->where(['premium_standard_plan_rate_uuid' => $step_data['step_3']['onlinetutorial_uuid'], 'option' => $step_data['step_3']['option']])->one();
                    $plan_total_price = $step_data['step_2']['tutor_further_details'][$lesson_length_word];
                    $plan_price = $step_data['step_2']['tutor_further_details']['price'][$lesson_length_word];
                    $plan_gst = $step_data['step_2']['tutor_further_details']['gst'][$lesson_length_word];
                    $plan_gst_description = $step_data['step_2']['tutor_further_details']['gst_description'][$lesson_length_word];
                    $plan_option = $step_data['step_2']['tutor_further_details']['option'][$lesson_length_word];

                    //Get id of tutorwise paln 
//                    $tutorwise_model = OnlineTutorialPackageTutorwise::find()->where(['premium_standard_plan_rate_uuid' => $step_data['step_3']['onlinetutorial_uuid'], 'tutor_uuid' => $step_data['step_2']['tutor_uuid'], 'instrument_uuid' => $step_data['step_1']['instrument_uuid']])->one();
//                    $online_tutorial_package_tutorwise_uuid = $tutorwise_model->uuid;
                    //Standard tutor loking period
//                    $locking_per_model_count = StudentTuitionStandardBookingLockingPeriod::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid, 'tutor_uuid' => $step_data['step_2']['tutor_uuid'], 'instrument_uuid' => $step_data['step_1']['instrument_uuid']])->count();
//
//                    if ($locking_per_model_count > 0) {
//
//                        $locking_per_model = StudentTuitionStandardBookingLockingPeriod::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid, 'tutor_uuid' => $step_data['step_2']['tutor_uuid'], 'instrument_uuid' => $step_data['step_1']['instrument_uuid']])->one();
//                        $locking_end_datetime = $locking_per_model->locking_end_datetime;
//                        $locking_start_datetime = $locking_per_model->locking_start_datetime;
//                        $locking_option = $locking_per_model->option;
//                        $current_date = date('Y-m-d H:i:s');
//
//                        if (($plan_option < $locking_option) && ($current_date <= $locking_end_datetime)) {
//
//                            $lockingplanDetail = StandardPlanOptionRate::find()->where(['premium_standard_plan_rate_uuid' => $step_data['step_3']['onlinetutorial_uuid'], 'option' => $locking_option])->one();
//                            $plan_total_price = $lockingplanDetail->total_price;
//                            $plan_price = $lockingplanDetail->price;
//                            $plan_gst = $lockingplanDetail->gst;
//                            $plan_gst_description = $lockingplanDetail->gst_description;
//                            $plan_option = $locking_option;
//                        } elseif (($plan_option > $locking_option) || (($current_date > $locking_end_datetime))) {
//                            $locking_end_date = date("Y-m-d", strtotime("+6 months", strtotime(date("Y-m-d"))));
//
//                            $locking_per_model->locking_start_datetime = date('Y-m-d') . ' 00:00:00';
//                            $locking_per_model->locking_end_datetime = $locking_end_date . ' 23:59:59';
//                            $locking_per_model->booking_datetime = date('Y-m-d H:i:s');
//                            $locking_per_model->option = $plan_option;
//                            $locking_per_model->online_tutorial_package_tutorwise_uuid = $online_tutorial_package_tutorwise_uuid;
//                            $locking_per_model->premium_standard_plan_rate_uuid = $step_data['step_3']['onlinetutorial_uuid'];
//
//                            $locking_per_model->save(false);
//                        }
//                    } else {
//                        $locking_end_date = date("Y-m-d", strtotime("+6 months", strtotime(date("Y-m-d"))));
//                        $locking_per_model_add = new StudentTuitionStandardBookingLockingPeriod();
//                        $locking_per_model_add->student_uuid = Yii::$app->user->identity->student_uuid;
//                        $locking_per_model_add->instrument_uuid = $step_data['step_1']['instrument_uuid'];
//                        $locking_per_model_add->tutor_uuid = $step_data['step_2']['tutor_uuid'];
//                        $locking_per_model_add->premium_standard_plan_rate_uuid = $step_data['step_3']['onlinetutorial_uuid'];
//                        $locking_per_model_add->option = $plan_option;
//                        $locking_per_model_add->booking_datetime = date('Y-m-d H:i:s');
//                        $locking_per_model_add->locking_start_datetime = date('Y-m-d') . ' 00:00:00';
//                        $locking_per_model_add->locking_end_datetime = $locking_end_date . ' 23:59:59';
//                        $locking_per_model_add->online_tutorial_package_tutorwise_uuid = $online_tutorial_package_tutorwise_uuid;
//                        $locking_per_model_add->save(false);
//                    }
                }


                $booking_uuid = NULL;
                $save_m_s_flag = false;

                //Transaction_payment
                $modelPayment = new PaymentTransaction();
                $modelPayment->student_uuid = Yii::$app->user->identity->student_uuid;
                $modelPayment->transaction_number = $orderID;
                $modelPayment->payment_datetime = date("Y-m-d H:i:s");
                $modelPayment->amount = ($musicoin_array['cart_total']) / 2;
                $modelPayment->payment_method = 'Musicoin';
                $modelPayment->reference_number = $orderID;
                $modelPayment->status = 'succeeded';
                $modelPayment->book_uuid = $booking_uuid;
                $modelPayment->response = NULL;

                $modelPayment->save(false);
                //student_tuition_booking table entry
                $modelSTBooking = new StudentTuitionBooking();
                $modelSTBooking->booking_id = General::generateBookingID();
                $modelSTBooking->booking_datetime = date('Y-m-d H:i:s');
                $modelSTBooking->student_uuid = Yii::$app->user->identity->student_uuid;
                $modelSTBooking->tutor_uuid = $step_data['step_2']['tutor_uuid'];
                $modelSTBooking->instrument_uuid = $step_data['step_1']['instrument_uuid'];
                $modelSTBooking->tutorial_name = $packageDetail->name;
                $modelSTBooking->tutorial_description = $packageDetail->description;
                $modelSTBooking->tutorial_type_code = $planDetail->tutorialTypeUu->code;
                $modelSTBooking->tutorial_min = $planDetail->tutorialTypeUu->min;
//                $modelSTBooking->payment_term_code = $planDetail->tutorialTypeUu->code;
                $modelSTBooking->payment_term_code = 'W';
                $modelSTBooking->total_session = $available;
                $modelSTBooking->session_length_day = $planDetail->tutorialTypeUu->days;
                $modelSTBooking->price = $plan_price * $available;
                $modelSTBooking->gst = $plan_gst * $available;
                $modelSTBooking->gst_description = $plan_gst_description;
                $modelSTBooking->total_price = $plan_total_price * $available;
                $modelSTBooking->status = "BOOKED";

//                $modelSTBooking->monthly_subscription_price = $musicoin_array['monthly_sub_total'];
                $modelSTBooking->monthly_subscription_price = '0.00';

                $modelSTBooking->student_monthly_subscription_uuid = NULL;
                $modelSTBooking->coupon_uuid = (!empty($musicoin_array['coupon_uuid'])) ? $musicoin_array['coupon_uuid'] : NULL;

                $modelSTBooking->creative_kids_voucher_code = (!empty($musicoin_array['creative_kids_voucher_code'])) ? $musicoin_array['creative_kids_voucher_code'] : NULL;
                $modelSTBooking->kids_name = (!empty($musicoin_array['kids_name'])) ? $musicoin_array['kids_name'] : NULL;
                $modelSTBooking->kids_dob = (!empty($musicoin_array['kids_dob'])) ? date("Y-m-d", strtotime($musicoin_array['kids_dob'])) : NULL;


                $modelSTBooking->discount = $musicoin_array['discount'] / 2;
                $modelSTBooking->grand_total = ($musicoin_array['cart_total'] / 2);
                $modelSTBooking->plan_rate_option = $plan_option;

                $modelSTBooking->premium_standard_plan_rate_uuid = $step_data['step_2']['tutor_further_details']['uuid'][$lesson_length_word];
                $modelSTBooking->tutor_count = $uniqueCount = count(array_unique(array_column($count, 'tutor_uuid')));
                $total_tutors = array();
                foreach (array_unique(array_column($count, 'tutor_uuid')) as $value) {
                    $total_tutors[] = $value;
                }
                $modelSTBooking->other_tutor = json_encode($total_tutors);

                //exit;
                if ($modelSTBooking->save(false)) {
//                if (true) {
                    if ((!empty($musicoin_array['coupon_uuid']))) {
                        $coupon_log = new CouponLog();
                        $coupon_log->booking_uuid = $modelSTBooking->uuid;
                        $coupon_log->booking_id = $modelSTBooking->booking_id;
                        $coupon_log->booking_datetime = $modelSTBooking->booking_datetime;
                        $coupon_log->student_uuid = Yii::$app->user->identity->student_uuid;
                        $coupon_log->tutor_uuid = $step_data['step_2']['tutor_uuid'];
                        $coupon_log->instrument_uuid = $step_data['step_1']['instrument_uuid'];
                        $coupon_log->total_price = $modelSTBooking->total_price;
                        $coupon_log->coupon_uuid = (!empty($musicoin_array['coupon_uuid'])) ? $musicoin_array['coupon_uuid'] : NULL;
                        $coupon_log->discount = $musicoin_array['discount'] / 2;
                        $coupon_log->grand_total = ($musicoin_array['cart_total'] / 2);
                        $coupon_log->transaction_uuid = $modelPayment->uuid;
                        $coupon_log->save(false);
                    }


                    //Booked Session entry
                    $session_date_array = explode(',', $step_data['step_4']['all_lesson_data']);

                    //update Transaction Booking id
                    $modelPaymentUpdate = PaymentTransaction::find()->where(['uuid' => $modelPayment->getPrimaryKey()])->one();
                    $modelPaymentUpdate->book_uuid = $modelSTBooking->getPrimaryKey();
                    $modelPaymentUpdate->save(false);

		    //Each Session discount coins
		    $session_discount_price =  ($modelSTBooking->discount / count($count));
		    $session_discount_musicoins = \app\libraries\General::priceToMusicoins($session_discount_price);

                    //if ($modelSTBooking->total_session > 0) {

                    for ($i = 0; $i < count($count); $i++) {

                        if ($count[$i]['is_available'] == "YES") {
                            //$start_hour = $count[$i]['lesson_time'];
                            $start_datetime = date('Y-m-d H:i:s', strtotime($count[$i]['lesson_time']));

                            $end_datetime = date('Y-m-d H:i:s', strtotime($start_datetime . " +" . $modelSTBooking->tutorial_min . " minutes"));
                            $digits = 4;
                            $pin_sql = "SELECT random_num
FROM (
  SELECT SUBSTRING(FLOOR(random() * 9999 + 10000)::text, 2) AS random_num 
  FROM booked_session
  UNION
  SELECT SUBSTRING(FLOOR(random() * 9999 + 10000)::text, 2) AS random_num
) AS numbers_mst_plus_1
WHERE random_num NOT IN (SELECT session_pin FROM booked_session  WHERE session_pin IS NOT NULL)
LIMIT 1";

                            $pin_res = Yii::$app->db->createCommand($pin_sql)->queryOne();
                            $pin = $pin_res['random_num'];

                            //$pin = str_pad(rand(0, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT);

                            $modelBookedSession = new BookedSession();
                            $modelBookedSession->booking_uuid = $modelSTBooking->getPrimaryKey();
                            $modelBookedSession->student_uuid = $modelSTBooking->student_uuid;
                            $modelBookedSession->tutor_uuid = $count[$i]['tutor_uuid'];
                            $modelBookedSession->instrument_uuid = $modelSTBooking->instrument_uuid;
                            $modelBookedSession->tutorial_type_code = $modelSTBooking->tutorial_type_code;
                            $modelBookedSession->start_datetime = General::convertUserToSystemTimezone($start_datetime);
                            $modelBookedSession->end_datetime = General::convertUserToSystemTimezone($end_datetime);
                            $modelBookedSession->session_min = $modelSTBooking->tutorial_min;
                            $modelBookedSession->session_pin = $pin;
                            $modelBookedSession->status = 'SCHEDULE';

$tutor_fee_model = TutorFee::find()->where(['online_tutorial_package_type_uuid' => $step_data['step_2']['tutor_further_details']['online_tutorial_package_type_uuid'], 'option' => $step_data['step_2']['tutor_further_details']['option'][$lesson_length_word], 'sesson_length' => $step_data['step_3']['lesson_length'] ])->one();

$tutor_pay_rate = (!empty($tutor_fee_model)) ? $tutor_fee_model->tutor_fee :0;

$modelBookedSession->tutor_plan_category = $step_data['step_2']['tutor_further_details']['package_type'];
$modelBookedSession->tutor_plan_option = $step_data['step_2']['tutor_further_details']['option'][$lesson_length_word];
$modelBookedSession->tutor_pay_rate = $tutor_pay_rate;
$modelBookedSession->session_price = $step_data['step_2']['tutor_further_details']['price'][$lesson_length_word];
$modelBookedSession->session_gst = $step_data['step_2']['tutor_further_details']['gst'][$lesson_length_word];
$modelBookedSession->session_total_price = $step_data['step_2']['tutor_further_details']['price'][$lesson_length_word] + $step_data['step_2']['tutor_further_details']['gst'][$lesson_length_word];
$modelBookedSession->session_musicoins = \app\libraries\General::priceToMusicoins($modelBookedSession->session_total_price);
$modelBookedSession->session_discount_musicoins = $session_discount_musicoins;


                            $modelBookedSession->save(false);
                        }
                    }

                    //}
                    //if Monthly subscription plan then entry in it.
//                    if ($musicoin_array['monthly_sub_checkbox'] == 1) {
//
//                        $MonthlySub = MonthlySubscriptionFee::find()->where(['uuid' => $musicoin_array['monthly_sub_uuid']])->one();
//
//                        $modelStudentSubscription = StudentSubscription::find()->where(['student_uuid' => $modelSTBooking->student_uuid])->one();
//
//                        $start_date = date('Y-m-d H:i:s');
//                        if ($student->isMonthlySubscription == 1) {
//                            if (!empty($modelStudentSubscription)) {
//                                $start_date = date('Y-m-d H:i:s', strtotime($modelStudentSubscription->next_renewal_date));
//                            }
//                        }
//
//                        //$start_date = (!empty($modelStudentSubscription)) ? date('Y-m-d H:i:s', strtotime($modelStudentSubscription->next_renewal_date)) : date('Y-m-d H:i:s');
//                        $end_date = date('Y-m-d H:i:s', strtotime($start_date . " +30 days"));
//                        $next_renewal_date = date('Y-m-d', strtotime($end_date . " +1 days"));
//
//                        $modelStudentMonthlySub = new StudentMonthlySubscription();
//                        $modelStudentMonthlySub->student_uuid = $modelSTBooking->student_uuid;
//                        $modelStudentMonthlySub->name = $MonthlySub->name;
//                        $modelStudentMonthlySub->price = $MonthlySub->price;
//                        $modelStudentMonthlySub->tax = $MonthlySub->tax;
//                        $modelStudentMonthlySub->total_price = $MonthlySub->total_price;
//                        $modelStudentMonthlySub->description = $MonthlySub->description;
//                        $modelStudentMonthlySub->with_tutorial = $MonthlySub->with_tutorial;
//                        $modelStudentMonthlySub->status = 'ENABLED';
//                        $modelStudentMonthlySub->start_datetime = General::convertUserToSystemTimezone($start_date);
//                        $modelStudentMonthlySub->end_datetime = General::convertUserToSystemTimezone($end_date);
//                        if ($modelStudentMonthlySub->save(false)) {
//
//                            $up_st_booking = StudentTuitionBooking::findOne($modelSTBooking->getPrimaryKey());
//                            if (!empty($up_st_booking)) {
//
//                                $up_st_booking->student_monthly_subscription_uuid = $modelStudentMonthlySub->uuid;
//                                $up_st_booking->save(false);
//
//                                $modelSTBooking->student_monthly_subscription_uuid = $modelStudentMonthlySub->uuid;
//                            }
//
//                            $save_m_s_flag = true;
//                        }
//                        $monthly_item = ['item' => $modelStudentMonthlySub->name, 'booking_uuid' => NULL, 'student_monthly_subscription_uuid' => $modelStudentMonthlySub->uuid, 'tax' => $modelStudentMonthlySub->tax, 'price' => $modelStudentMonthlySub->price, 'total' => $modelStudentMonthlySub->total_price];
//
//                        if (empty($modelStudentSubscription)) {
//                            $modelStudentSubscription = new StudentSubscription();
//                        }
//                        $modelStudentSubscription->student_uuid = $modelSTBooking->student_uuid;
//                        $modelStudentSubscription->subscription_uuid = $modelStudentMonthlySub->uuid;
//                        $modelStudentSubscription->next_renewal_date = $next_renewal_date;
//                        $modelStudentSubscription->save(false);
//
//                        //update student monthly sub flag
//                        $student->isMonthlySubscription = 1;
//                        $student->isCancelMonthlySubscriptionRequest = 0;
//                        $student->save(false);
//                    }

                    $item_array[0] = ['item' => $modelSTBooking->tutorial_name, 'booking_uuid' => $modelSTBooking->uuid, 'student_monthly_subscription_uuid' => NULL, 'tax' => $modelSTBooking->gst, 'price' => $modelSTBooking->price, 'total' => $modelSTBooking->total_price];
                    $order_total = $item_array[0]['total'];
//                    if ($musicoin_array['monthly_sub_checkbox'] == 1 && isset($monthly_item)) {
//                        $item_array[1] = $monthly_item;
//                        $order_total = $order_total + $monthly_item['total'];
//                    }
                    $order_array['student_uuid'] = $modelSTBooking->student_uuid;
                    $order_array['payment_uuid'] = $modelPayment->getPrimaryKey();
                    $order_array['discount'] = $modelSTBooking->discount;
                    $order_array['item'] = $item_array;
                    $order_array['total'] = $order_total;
                    $grand_total = (($order_total - $order_array['discount'])> 0)? ($order_total - $order_array['discount']):0;
                    $order_array['grand_total'] = $grand_total;

                    //create order
                    $order_result = Book::generate_order($order_array);

                    $arrayCoin = [
                        'student_uuid' => $modelSTBooking->student_uuid,
                        'coins' => \app\libraries\General::priceToMusicoins($order_array['grand_total']),
                        'summary' => 'Lesson booked with order ID: #'. (isset($order_result['order_id'])?$order_result['order_id']:$orderID),
                    ];
                    $resultMusicoinLog = MusicoinLog::debitMusicoin($arrayCoin);
                    self::bookedTutionMail($modelSTBooking->student_uuid, $modelSTBooking->getPrimaryKey(), $order_result, $save_m_s_flag);

                    if ($save_m_s_flag) {
                        //StudentMonthlySubscription::MonthlySubscriptionBuyMail($modelStudentMonthlySub,$modelSTBooking->student_uuid);
                    }
                }

                return $return = ['code' => 200, 'message' => 'success'];
            } else {
                return $return = ['code' => 421, 'message' => 'Transaction has been failed.'];
                //Yii::$app->session->setFlash('success', Yii::t('app', 'Transaction has been failed.'));
                //return \yii\web\Controller::redirect(['index']);
            }
        } else {
            return $return = ['code' => 421, 'message' => 'Form submission error.'];
            //Yii::$app->session->setFlash('success', Yii::t('app', 'Form submission error.'));
            //return \yii\web\Controller::redirect(['index']);
        }

        return $return;
    }

    public static function generate_order($order_array) {

        if (!empty($order_array)) {

            $filename = '';
            $modelOrder = new Order();
            $modelOrder->order_id = General::generateOrderID();
            $modelOrder->student_uuid = $order_array['student_uuid'];
            $modelOrder->payment_uuid = $order_array['payment_uuid'];
            $modelOrder->order_datetime = date('Y-m-d H:i:s');
            $modelOrder->total = $order_array['total'];
            $modelOrder->discount = $order_array['discount'];
            $modelOrder->grand_total = $order_array['grand_total'];
            if ($modelOrder->save(false)) {

                if (!empty($order_array['item'])) {
                    foreach ($order_array['item'] as $key => $item) {

                        $modelOrderItem = new OrderItem();
                        $modelOrderItem->order_uuid = $modelOrder->getPrimaryKey();
                        $modelOrderItem->item = $item['item'];
                        $modelOrderItem->booking_uuid = $item['booking_uuid'];
                        $modelOrderItem->student_monthly_subscription_uuid = $item['student_monthly_subscription_uuid'];
                        $modelOrderItem->price = $item['price'];
                        $modelOrderItem->tax = $item['tax'];
                        $modelOrderItem->total = $item['total'];
                        $modelOrderItem->save(false);

                        if (!empty($item['booking_uuid'])) {
                            $modelSTBooking = StudentTuitionBooking::find()->where(['uuid' => $item['booking_uuid']])->one();
                            if (!empty($modelSTBooking)) {
                                $modelSTBooking->order_uuid = $modelOrder->getPrimaryKey();
                                $modelSTBooking->save(false);
                            }
                        }

                        if (!empty($item['student_monthly_subscription_uuid'])) {
                            $modelStudentMonthlySub = StudentMonthlySubscription::find()->where(['uuid' => $item['student_monthly_subscription_uuid']])->one();
                            if (!empty($modelStudentMonthlySub)) {
                                $modelStudentMonthlySub->order_uuid = $modelOrder->getPrimaryKey();
                                $modelStudentMonthlySub->save(false);
                            }
                        }
                        if (!empty($item['student_musicoin_package_uuid'])) {
                            $modelStudentMusicoinPackage = StudentMusicoinPackage::find()->where(['uuid' => $item['student_musicoin_package_uuid']])->one();
                            if (!empty($modelStudentMusicoinPackage)) {
                                $modelStudentMusicoinPackage->order_uuid = $modelOrder->getPrimaryKey();
                                $modelStudentMusicoinPackage->save(false);
                            }
                        }
                    }
                }
                $transaction_model = \app\models\PaymentTransaction::find()->where(['uuid' => $modelOrder->payment_uuid])->asArray()->one();
                if (!empty($transaction_model['transaction_number'])) {
                    $filename = 'Invoice_' . $transaction_model['transaction_number'] . '.pdf';
                } else {
                    $filename = 'Invoice_' . $modelOrder->order_id . '.pdf';
                }
            }
            
            $pdf_res = self::create_pdf($filename, $modelOrder->getPrimaryKey());

            $modelStudent = Student::find()->where(['student_uuid' => $order_array['student_uuid']])->one();

            $content['student_name'] = $modelStudent->first_name;
            $content['order_uuid'] = $modelOrder->getPrimaryKey();
            $content['order_id'] = $modelOrder->order_id;
            $content['order_array'] = $order_array;
            $content['order_date'] = General::displayDate($modelOrder->order_datetime);
            $content['order_file'] = $pdf_res['file'];
//            $content['order_file'] = $filename;

            return $content;
        }
        return [];
    }

    public static function create_pdf($filename = '', $order_uuid) {

        $file = Yii::$app->params['media']['invoice']['path'] . $filename;

        $model = \app\models\Order::findOne($order_uuid);

        if (!empty($model)) {

            $order_item = \app\models\OrderItem::find()->where(['order_uuid' => $order_uuid])->asArray()->all();
            $transaction_model = \app\models\PaymentTransaction::find()->where(['uuid' => $model->payment_uuid])->asArray()->one();

            $content = Yii::$app->controller->renderPartial('//order/book_lesson_invoice_pdf', ['model' => $model, 'order_item' => $order_item,'transaction_model' => $transaction_model]);

            if ($filename == '') {
                if (!empty($transaction_model['transaction_number'])) {
                    $filename = 'Invoice_' . $transaction_model['transaction_number'] . '.pdf';
                } else {
                    $filename = 'Invoice_' . $model->order_id . '.pdf';
                }
            }
            $file = Yii::$app->params['media']['invoice']['path'] . $filename;

            if (!is_dir(Yii::$app->params['media']['invoice']['path'])) {
                FileHelper::createDirectory(Yii::$app->params['media']['invoice']['path']);
            }

            $css_content = file_get_contents(Yii::$app->params['theme_assets']['url'] . 'plugins/bootstrap/css/bootstrap.min.css');
            $css_content .= file_get_contents(Yii::$app->params['theme_assets']['url'] . 'css/new_pdf.css');
            $css_content .= 'body {background-image: url(\'' . Yii::$app->params['theme_assets']['url'] . 'images/pdf_bg.png' . '\'); background-position: center bottom;background-repeat: no-repeat;}';
            
            $footer_html = '<table class="footer_tbl"><tr><td>ADMIN@MUSICONN.COM.AU</td><td>1300 068 742</td><td>MUSICONN.COM.AU</td></tr></table>';

            $params = [
                'mode' => Pdf::MODE_CORE,
                'format' => Pdf::FORMAT_A4,
                'orientation' => Pdf::ORIENT_PORTRAIT,
                'destination' => Pdf::DEST_FILE,
                'content' => $content,
                'filename' => Yii::$app->params['media']['invoice']['path'] . $filename,
                'tempPath' => 'mpdf/',
                'cssInline' => '.kv-heading-1{font-size:12px;border:none;}',
                'cssInline' => $css_content,
                'options' => ['title' => 'Invoice'],
                'methods' => [
                    'SetHeader' => [''],
                    'SetFooter' => [$footer_html],
                ]
            ];
            $pdf = new Pdf($params);
            $pdf->render();

            if (is_file($file)) {

                $model->invoice_file = $filename;
                $model->save(false);

                return ['status' => '1', 'file' => $file, 'filename' => $filename];
            } else {
                return ['status' => '0', 'file' => $file, 'filename' => $filename];
            }
        } else {
            return ['status' => '0', 'file' => $file, 'filename' => $filename];
        }
    }

    public static function bookedTutionMail($student_uuid, $booking_uuid, $order_array, $ismonthlysubscription = false) {

        if (!empty($student_uuid) && !empty($booking_uuid)) {

            $modelSTBooking = StudentTuitionBooking::find()->where(['uuid' => $booking_uuid])->one();
            $modelStudent = Student::find()->where(['student_uuid' => $student_uuid])->one();
            if (!empty($modelSTBooking)) {

                $modelTutor = Tutor::find()->where(['uuid' => $modelSTBooking->tutor_uuid])->one();
                $modelInstrument = Instrument::find()->where(['uuid' => $modelSTBooking->instrument_uuid])->one();

                $tutor_user_model = User::find()->where(['tutor_uuid' => $modelSTBooking->tutor_uuid])->one();
                $student_user_model = User::find()->where(['student_uuid' => $student_uuid])->one();
                $tutor_tz = (!empty($tutor_user_model)) ? $tutor_user_model->timezone : Yii::$app->params['timezone'];
                $student_tz = (!empty($student_user_model)) ? $student_user_model->timezone : Yii::$app->params['timezone'];
                $system_tz = Yii::$app->params['timezone'];

                $session_dates = $session_dates_tutor = [];
                $session_time = $session_time_tutor = [];
                $session_t_name = $session_t_name_tutor = [];
                $modelAllTusion = BookedSession::find()->where(['booking_uuid' => $booking_uuid])->all();
                if (!empty($modelAllTusion)) {
                    foreach ($modelAllTusion as $key => $value) {
//                        $session_dates[] = General::displayDate($value->start_datetime);
//                        $session_time = General::displayTime($value->start_datetime).' - '.General::displayTime($value->end_datetime);
                        $modelTutordd = Tutor::find()->where(['uuid' => $value->tutor_uuid])->one();
                        $tutor_user_model = User::find()->where(['tutor_uuid' => $value->tutor_uuid])->one();
                        $tutor_tz = (!empty($tutor_user_model)) ? $tutor_user_model->timezone : Yii::$app->params['timezone'];
                        $session_dates[$modelTutordd->uuid][] = General::convertTimezone($value->start_datetime, $system_tz, $student_tz, 'l, d-m-Y');
                        // $session_dates[$modelTutordd->uuid]['t_name']=$modelTutordd->first_name;
                        $session_time[$modelTutordd->uuid][] = General::convertTimezone($value->start_datetime, $system_tz, $student_tz, 'h:i A') . ' - ' . General::convertTimezone($value->end_datetime, $system_tz, $student_tz, 'h:i A');

                        $session_dates_tutor[$modelTutordd->uuid][] = General::convertTimezone($value->start_datetime, $system_tz, $tutor_tz, 'l, d-m-Y');
                        $session_time_tutor[$modelTutordd->uuid][] = General::convertTimezone($value->start_datetime, $system_tz, $tutor_tz, 'h:i A') . ' - ' . General::convertTimezone($value->end_datetime, $system_tz, $tutor_tz, 'h:i A');
                        $session_t_name[$modelTutordd->uuid]['first_name'] = $modelTutordd->first_name;
                        $session_t_name[$modelTutordd->uuid]['last_name'] = $modelTutordd->last_name;
                        $session_t_name[$modelTutordd->uuid]['phone'] = $modelTutordd->phone;
                        $session_t_name[$modelTutordd->uuid]['email'] = $modelTutordd->email;
                    }
                }

                $tutor_name = $modelTutor->first_name;
                $student_name = $modelStudent->first_name;
                $package_payment_term = '';
                if ($modelSTBooking->payment_term_code == 'Q') {
                    $package_payment_term = 'Quarterly';
                } elseif ($modelSTBooking->payment_term_code == 'M') {
                    $package_payment_term = 'Monthly';
                } elseif ($modelSTBooking->payment_term_code == 'F') {
                    $package_payment_term = 'Fortnightly';
                }

                $content['creative_kids_voucher_code'] = $modelSTBooking->creative_kids_voucher_code;
                $content['student_name'] = $student_name;
                $content['student_last_name'] = $modelStudent->last_name;
                $content['booking_id'] = $modelSTBooking->booking_id;
                $content['tutor_name'] = $session_t_name;
                /* $content['tutor_last_name'] = $modelTutor->last_name;
                  $content['tutor_contact_number'] = $modelTutor->phone;
                  $content['tutor_email'] = $modelTutor->email; */
                $content['tutorial_name'] = $modelSTBooking->tutorial_name;
                $content['package_payment_term'] = $package_payment_term;
                $content['instrument'] = $modelInstrument->name;
                $content['tution_duration'] = $modelSTBooking->tutorial_min;
                $content['time'] = $session_time;
                $content['session_dates'] = $session_dates;
                $content['monthly_subscription_price'] = $modelSTBooking->monthly_subscription_price;
                $content['student_monthly_subscription_uuid'] = $modelSTBooking->student_monthly_subscription_uuid;
                $content['grand_total'] = $modelSTBooking->grand_total;
                $content['total_price'] = $modelSTBooking->total_price;
                $content['discount'] = $modelSTBooking->discount;
                $content['order_id'] = (isset($order_array['order_id'])) ? "#" . $order_array['order_id'] : "";
                $isInvoiceFile = (isset($order_array['order_file'])) ? $order_array['order_file'] : "";

                $content_tutor['student_name'] = $student_name;
                $content_tutor['student_last_name'] = $modelStudent->last_name;
                $content_tutor['student_contact_number'] = $modelStudent->phone_number;
                $content_tutor['student_email'] = $modelStudent->email;
                $content_tutor['booking_id'] = $modelSTBooking->booking_id;
                $content_tutor['tutorial_name'] = $modelSTBooking->tutorial_name;
                $content_tutor['tution_duration'] = $modelSTBooking->tutorial_min;
                $content_tutor['instrument'] = $modelInstrument->name;
                $content_tutor['time'] = $session_time_tutor;
                $content_tutor['session_dates'] = $session_dates_tutor;
                $content_tutor['student_monthly_subscription_uuid'] = $modelSTBooking->student_monthly_subscription_uuid;

                /*$content_admin['creative_kids_voucher_code'] = $modelSTBooking->creative_kids_voucher_code;
                $content_admin['student_name'] = $student_name;
                $content_admin['student_last_name'] = $modelStudent->last_name;
                $content_admin['booking_id'] = $modelSTBooking->booking_id;
                $content_admin['purchased_package'] = $modelSTBooking->tutorial_name;
                $content_admin['payment_term'] = ($modelSTBooking->session_length_day == 14) ? $modelSTBooking->total_session . " fortnights" : $modelSTBooking->total_session . " weeks";
                $content_admin['tutor_name'] = $session_t_name;
//                $content_admin['tutor_last_name'] = $modelTutor->last_name;
//                $content_admin['tutor_contact_number'] = $modelTutor->phone;
//                $content_admin['tutor_email'] = $modelTutor->email;
                $content_admin['tutorial_name'] = $modelSTBooking->tutorial_name;
                $content_admin['package_payment_term'] = $package_payment_term;
                $content_admin['instrument'] = $modelInstrument->name;
                $content_admin['tution_duration'] = $modelSTBooking->tutorial_min;
                $content_admin['time'] = $session_time;
                $content_admin['session_dates'] = $session_dates;
                $content_admin['monthly_subscription_price'] = $modelSTBooking->monthly_subscription_price;
                $content_admin['student_monthly_subscription_uuid'] = $modelSTBooking->student_monthly_subscription_uuid;
                $content_admin['grand_total'] = $modelSTBooking->grand_total;
                $content_admin['total_price'] = $modelSTBooking->total_price;
                $content_admin['discount'] = $modelSTBooking->discount;
                $content_admin['order_id'] = (isset($order_array['order_id'])) ? "#" . $order_array['order_id'] : "";
                */

                $ownersEmail = General::getOwnersEmails(['OWNER', 'ADMIN', 'SUBADMIN']);

                $mail = Yii::$app->mailer->compose('booked_tution_student_mail_m', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                        ->setFrom(Yii::$app->params['supportEmail'])
                        ->setTo($modelStudent->email)
                        ->setBcc($ownersEmail)
                        ->setSubject(ucfirst(strtolower(Yii::$app->name)) . ' Notification:  Confirmation of Booking');

                /*if (is_file($isInvoiceFile)) {
                    $mail->attach($isInvoiceFile);
                }*/
                $mail->send();
                
                foreach ($session_t_name as $key => $value) {
                    $content_tutor['tutor_name'] = $session_t_name[$key];
                    $content_tutor['tutor_uuid'] = $key;

                    Yii::$app->mailer->compose('booked_tution_tutor_mail_m', ['content' => $content_tutor], ['htmlLayout' => 'layouts/html'])
                            ->setFrom(Yii::$app->params['supportEmail'])
                            ->setTo($modelTutor->email)
                            ->setBcc($ownersEmail)
                            ->setSubject(ucfirst(strtolower(Yii::$app->name)) . ' Notification: A student has booked lessons with you')
                            ->send();
                }
                /*$mail = Yii::$app->mailer->compose('booked_tution_admin_mail_m', ['content' => $content_admin], ['htmlLayout' => 'layouts/html'])
                        ->setFrom(Yii::$app->params['supportEmail'])
                        ->setTo($ownersEmail)
//                        ->setTo('pratik+admin@vindaloovoip.com')
                        ->setSubject(ucfirst(strtolower(Yii::$app->name)) . ' Notification: New Student Booking: #' . $content_admin['booking_id'] . ' : ' . $content_admin['student_name']);

                if (is_file($isInvoiceFile)) {
                    $mail->attach($isInvoiceFile);
            }
                $mail->send();*/
        }
        }

        return true;
    }

}
