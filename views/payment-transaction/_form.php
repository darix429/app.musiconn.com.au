<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentTransaction */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-transaction-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'uuid')->textInput() ?>

    <?= $form->field($model, 'student_uuid')->textInput() ?>

    <?= $form->field($model, 'transaction_number')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'book_uuid')->textInput() ?>

    <?= $form->field($model, 'type')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'payment_datetime')->textInput() ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <?= $form->field($model, 'payment_method')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'reference_number')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'subscription_uuid')->textInput() ?>

    <?= $form->field($model, 'response')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
