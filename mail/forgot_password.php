<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<?php
$resetLink = Url::to(['site/reset-password', 'token' => $user->password_reset_token], true);
?>
<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $user->first_name ?>,</p>
            
            <p><a href="<?= $resetLink ?>">Click here to reset your password.</a></p>
            
            <p>Please do not reply to this unattended mailbox.  Any queries or feedback can be directed to admin@musiconn.com.au.</p>

            <p>Regards,<br>
            Admin Team – Musiconn Pty Ltd</p>
        </td>
    </tr>
</table>
