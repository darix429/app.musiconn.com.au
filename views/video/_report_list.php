<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">

    <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Title</th>
                <th>Uploaded Date</th>
                <th>Uploaded By</th>
                <th>Published Date</th>
                <th>Published By</th>
                <th>Is Free</th>
                <th>Status</th>
            </tr>
        </thead>

        <tbody>
            <?php
            if (!empty($report)) {
                foreach ($report as $key => $value) {
                    ?>
                    <tr>
                        <td><?= $key + 1; ?></td>
                        <td><?= $value['title']; ?></td>
                        <td><?= \app\libraries\General::displayDate($value['uploaded_date']); ?></td>
                        <td><?= $value['userUu']['first_name'] . " " . $value['userUu']['last_name'] . " (" . $value['userUu']['role'] . ")"; ?></td>
                        <td><?= \app\libraries\General::displayDate($value['publish_date']); ?></td>
                        <td><?= isset($value['adminUu']) ? $value['adminUu']['first_name'] . " " . $value['adminUu']['last_name'] . " (" . $value['published_by'] . ")" : ''; ?></td>
                        <td><?= ($value['is_free'] == 1) ? 'Yes' : 'No'; ?></td>
                        <td>
                            <?php
                            if ($value['status'] == 'PUBLISHED') {
                                $e_class = "badge-success";
                            } elseif ($value['status'] == 'UPLOADED') {
                                $e_class = "badge-info";
                            } else {
                                $e_class = "badge-danger";
                            }
                            ?>
                            <span class="badge badge-pill <?= $e_class ?> "  ><?= $value['status']; ?></span>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>

</div>
