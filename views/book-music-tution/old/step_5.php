<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Url;
use app\models\Instrument;
use app\models\OnlineTutorialPackage;
use app\models\StudentTuitionBooking;
use app\models\Tutor;
use app\models\StudentCreditCard;
use app\models\MonthlySubscriptionFee;
use app\libraries\General;
use app\models\Student;

$this->title = Yii::t('app', 'Book Music Lessons');
$this->params['breadcrumbs'][] = $this->title;

if (!empty($step_data['step_1']['instrument_uuid'])) {
    $instrumentDetail = Instrument::find()->where(['uuid' => $step_data['step_1']['instrument_uuid']])->one();
} else {
    return \yii\web\Controller::redirect(['index']);
}

$student_detail = Student::findOne(Yii::$app->user->identity->student_uuid);
$planDetail = OnlineTutorialPackage::find()->where(['uuid' => $step_data['step_2']['onlinetutorial_uuid']])->one();
$start_hour_array = StudentTuitionBooking::start_hour_array($step_data['step_2']['onlinetutorial_uuid']);
$tutorDetail = Tutor::findOne($step_data['step_3']['tutor_uuid']);
$saved_cc_detail = StudentCreditCard::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid, 'status' => 'ENABLED'])->one();
$monthly_package = MonthlySubscriptionFee::find()->where(['with_tutorial' => 1, 'status' => 'ENABLED'])->one();

//echo "<pre>";
//print_r($step_data);
//print_r($monthly_package);
//exit;

$this->registerJsFile('https://js.stripe.com/v2/');
$this->registerJs(
        '
$( "#gostep1,#gostep2,#gostep3,#gostep4" ).submit(function( event ) {
        //blockBody();
});

$(document).on("click",".delete_credit_card",function(){
    if(confirm("Are you sure, you want to delete this card ?")){
        $.ajax({
            type: "POST",
            url: "' . Url::to(['/book-music-tution/remove-card/']) . '",
            data: { "id": $(this).data("uuid")},
            beforeSend: function() {
                blockBody();
            },
            success:function(result){
                if(result.code == 200){

                    $("#gostep-reload-5").submit();

                }else{
                    showErrorMessage(result.message);
                }
                unblockBody();
            },
            error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
    }
})

function cart_refresh(loader_hide = true){

    var monthly_sub_check = ($("#studenttuitionbooking-monthly_sub_checkbox").is(":checked")) ? "yes" : "no";
    var monthly_sub_uuid = $("#studenttuitionbooking-monthly_sub_uuid").val();
    var coupon_code = $("#studenttuitionbooking-coupon_code").val();
    $.ajax({
        type: "GET",
        url: "' . Url::to(['/book-music-tution/refresh-cart/']) . '",
        data: { "monthly_sub_check": monthly_sub_check,"monthly_sub_uuid":monthly_sub_uuid,"coupon_code":coupon_code},
        beforeSend: function() {
            blockBody();
        },
        success:function(result){

            if(result.code == 200){

                $("#studenttuitionbooking-cart_total").val(result.result.cart_total);
                $("#cart_total_text").html(result.result.cart_total);

                $("#studenttuitionbooking-coupon_uuid").val(result.result.coupon.coupon_uuid);
                $("#studenttuitionbooking-discount").val(result.result.coupon.discount);
                $("#discount_text").html(result.result.coupon.discount);

                if(result.result.coupon.coupon_code != ""){
                    $(".promo-code-msg").text(result.result.coupon.message);
                }
                if(result.result.coupon.is_available){
                    $(".promo-code-msg").removeClass("text-danger");
                    $(".promo-code-msg").addClass("text-success");
                }else{
                    $(".promo-code-msg").removeClass("text-success");
                    $(".promo-code-msg").addClass("text-danger");
                }

                if(result.result.monthly_sub_check != "yes"){
                    $("#monthly_sub_price_text_parent").hide();
                }else{
                    $("#monthly_sub_price_text_parent").show();
                }
                if(loader_hide){
                    unblockBody();
                }
            }else{
                unblockBody();
                showErrorMessage(result.message);
            }
        },
        error:function(e){
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
}

$(document).on("ifChanged","input[name=\"StudentTuitionBooking[monthly_sub_checkbox]\"]",function (event) {
    cart_refresh();
});

cart_refresh();

/*********************************/

//set your publishable key
Stripe.setPublishableKey("'. Yii::$app->params['stripe']['publishable_key'] .'");

//callback to handle the response from stripe
    function stripeResponseHandler(status, response) {
        if (response.error) {
            $("#payBtn").removeAttr("disabled");
            $(".payment-errors").html(response.error.message);
            unblockBody();
            showSuccess(response.error.message);
        } else {
            var form$ = $("#frm_step_5");

            // var token = response["id"];
            var token = response.id;

            form$.append("<input type=\"hidden\" name=\"stripeToken\" value=\""+token+"\" />");
            blockBody("Submiting...");
            form$.get(0).submit();
	    blockBody("Please waiting...");
        }
    }

function beforeValidation(){

    $("#frm_step_5" ).find("input").each( //console.log()
        function(index) {
                idnya = $(this).attr("id");
                $("#frm_step_5").yiiActiveForm("validateAttribute", idnya);
                err = $(this).parent().has("has-error");
                msg = $(this).parent().find(".help-block").html();
               // alert(idnya+"=="+msg);

        }
    );
}

 function submitform()
    {
        cart_refresh(false);
        beforeValidation();

        $("#payBtn").attr("disabled", "disabled");
        var dateText = $("#studenttuitionbooking-cc_month_year").val();
        var pieces = dateText.split("-");
        var year = (pieces[0] != undefined && pieces[0] > 0) ? pieces[0] : "1970";
        var month = (pieces[1] != undefined && pieces[1] > 0) ? pieces[1] : "00";

        var token = Stripe.createToken({
            number: $("#studenttuitionbooking-cc_number").val(),
            cvc: $("#studenttuitionbooking-cc_cvv").val(),
            exp_month: month,
            exp_year: year,
        }, stripeResponseHandler);
    }
    
//    $("#frm_step_5").submit(function (event) {
//    console.log(event)
//        //submit from callback
//        alert("submit")
//        blockBody();
//        return false;
//    });

    $(document).bind("cut copy paste","#studenttuitionbooking-cc_number",function(e){
        e.preventDefault();
    });

        ', View::POS_END
);

?>
<style>
    .field-studenttuitionbooking-cc_checkbox{
        margin-top: 15px;
    }
    .form-group{
        /*margin-bottom: 0px;*/
    }
    .field-studenttuitionbooking-cc_checkbox{
        margin-top: 0px;
        margin-bottom: 0px;
    }
    .field-studenttuitionbooking-monthly_sub_uuid,
    .field-studenttuitionbooking-monthly_sub_total,
    .field-studenttuitionbooking-discount,
    .field-studenttuitionbooking-cart_total,
    .coupon_parent_div,
    .field-studenttuitionbooking-coupon_code,
    .field-studenttuitionbooking-cc_uuid,
    .field-studenttuitionbooking-cc_name,
    .field-studenttuitionbooking-cc_number,
    .field-studenttuitionbooking-cc_month_year,
    .field-studenttuitionbooking-cc_cvv
    {
        margin-bottom: 0px !important;

    }

    .my_input_border{
            border: 1px solid #bbb8b8 !important;
    }
</style>
<div class="col-xl-12">
    <nav class="navbar navbar-expand-lg navbar-light bg-primary text-light" style="">
        <h2 class="booking-nav-title float-left text-white" style="">Select Instrument &nbsp;<i class="fa fa-check"></i> </h2>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#change-step1" aria-controls="change-step1" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="change-step1">
            <?php echo Html::beginForm(['index'], 'post', ['id' => 'gostep1', 'class' => 'form-inline d-block']) ?>
            <input type="hidden" name="step" value="1">
            <input type="hidden" name="prev" value="1">
            <a href="javascript:$('#gostep1').submit();" class="btn btn-outline-success pull-right">
                Change
            </a>
            <p class="pull-right"> <?= $instrumentDetail->name; ?>&nbsp;</p>
            <?php echo Html::endForm() ?>
        </div>
    </nav>

    <div class="spacer"></div>

    <nav class="navbar navbar-expand-lg navbar-light bg-primary text-light" style="">
        <h2 class="booking-nav-title float-left text-white" style="">Select Plan &nbsp;<i class="fa fa-check"></i> </h2>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#change-step2" aria-controls="change-step2" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="change-step2">
            <?php echo Html::beginForm(['index'], 'post', ['id' => 'gostep2', 'class' => 'form-inline d-block']) ?>
            <input type="hidden" name="step" value="2">
            <input type="hidden" name="prev" value="2">
            <a href="javascript:$('#gostep2').submit();" class="btn btn-outline-success pull-right">
                Change
            </a>
            <p class="pull-right"> <?= $planDetail->name . " Total : $" . $planDetail->total_price; ?>&nbsp;</p>
            <?php echo Html::endForm() ?>
        </div>
    </nav>

    <div class="spacer"></div>

    <nav class="navbar navbar-expand-lg navbar-light bg-primary text-light" style="">
        <h2 class="booking-nav-title float-left text-white" style="">Select Tutor &nbsp;<i class="fa fa-check"></i> </h2>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#change-step3" aria-controls="change-step3" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="change-step3">
            <?php echo Html::beginForm(['index'], 'post', ['id' => 'gostep3', 'class' => 'form-inline d-block']) ?>
            <input type="hidden" name="step" value="3">
            <input type="hidden" name="prev" value="3">
            <a href="javascript:$('#gostep3').submit();" class="btn btn-outline-success pull-right">
                Change
            </a>
            <p class="pull-right"> <?= $tutorDetail->first_name . ' ' . $tutorDetail->last_name; ?>&nbsp;</p>
            <?php echo Html::endForm() ?>
        </div>
    </nav>

    <div class="spacer"></div>

    <nav class="navbar navbar-expand-lg navbar-light bg-primary text-light" style="">
        <h2 class="booking-nav-title float-left text-white" style="">Select Date &nbsp;<i class="fa fa-check"></i> </h2>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#change-step4" aria-controls="change-step4" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="change-step4">
            <?php echo Html::beginForm(['index'], 'post', ['id' => 'gostep4', 'class' => 'form-inline d-block']) ?>
            <input type="hidden" name="step" value="4">
            <input type="hidden" name="prev" value="4">
            <a href="javascript:$('#gostep4').submit();" class="btn btn-outline-success pull-right">
                Change
            </a>
            <p class="pull-right"> <?= $step_data['step_4']['all_dates']; ?>&nbsp;</p>
            <?php echo Html::endForm() ?>
        </div>
    </nav>

    <section class="box ">
        <header class="panel_header bg-primary ">
            <h2 class="title float-left text-white">Confirm and Payment</h2>
            <div class="actions panel_actions float-right">
            </div>
        </header>
        <div class="content-body padding-imp-30">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12">
                    <?php
                    $form = ActiveForm::begin([
                                'action' => ['index'],
                                'id' => 'frm_step_5',
                                'enableClientValidation' => true,
                                //'enableAjaxValidation' => true,
                                'validateOnChange' => true,
                                'options' => ['class' => 'form-horizontal']]);
                    ?>
                    <input type="hidden" name="step" value="5">

                    <div class="row">

                        <div class="col-lg-12 col-md-12 col-12 booking_search_data___">
                            <div class="tab-pane fade show active " >
                                <div class="row credit-card-form">

                                    <div class="col-lg-8">
                                        <div id="accordion" role="tablist" class="accordion-group">

                                            <div class="card credit_card_custom_css">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-lg-7" style="border-right: 1px solid #bbb8b8;">

                                                            <div class="form-row">
                                                                <?php if ($student_detail->isMonthlySubscription != 1 && !empty($monthly_package)) { ?>
                                                                    <div class="col-md-12 mb-0">
                                                                        <label class="control-label" for="" >Monthly Subscription </label>
                                                                        <?php
                                                                        echo $form->field($model, 'monthly_sub_checkbox')->checkbox([
                                                                            'label' => $monthly_package->name . ' $' . $monthly_package->total_price,
                                                                            'template' => '<p class="pull-left"><label class="icheck-label form-label" for="save subcription"> {input}  </label> <p>',
                                                                            'class' => 'skin-square-red float-left monthly_sub_checkbox',])->label(false);
                                                                        ?>

                                                                    </div>
                                                                <?php } ?>
                                                                <div class="col-md-12 mb-0">
                                                                    <?=
                                                                            $form->field($model, 'coupon_code', ['inputOptions' => ['autocomplete' => 'off'],'template' => ''
                                                                                . '<div class="input-group mb-3 coupon_parent_div">'
                                                                                . '{input}'
                                                                                . '<div class="input-group-append">'
                                                                                . '<button type="button" id="coupon_btn" class="btn btn-md btn-secondary" onclick="cart_refresh();">Apply</button>'
                                                                                . '</div>'
                                                                                . '</div>'
                                                                                . '{error}'])
                                                                            ->textInput(['class' => 'form-control my_input_border', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('coupon_code')])
                                                                    ?>
                                                                    <p class="promo-code-msg "></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-5 text-right" >
                                                            <div class=" has-static">
                                                                <label class="form-label">Plan Price : <strong>$<span id="plan_price_text"><?= $planDetail->price; ?></span></strong></label>
                                                            </div>
                                                            <div class=" has-static">
                                                                <label class="form-label">Plan GST : <strong>$<span id="plan_gst_text"><?= $planDetail->gst; ?></span></strong></label>
                                                            </div>
                                                            <div class=" has-static">
                                                                <label class="form-label">Plan Total : <strong>$<span id="plan_total_text"><?= $planDetail->total_price; ?></span></strong></label>
                                                            </div>
                                                            <?php if(!empty($monthly_package)){ ?>
                                                            <div class=" has-static" id="monthly_sub_price_text_parent" style="<?php echo ($model->monthly_sub_checkbox) ? "display:block" : "display:none" ?>">
                                                                <label class="form-label">Monthly Subscription : <strong>$<span id="monthly_sub_price_text"><?= $monthly_package->total_price; ?></span></strong></label>
                                                            </div>
                                                            <?php } ?>
                                                            <div class=" has-static">
                                                                <label class="form-label">Discount : <strong>$<span id="discount_text"><?= $model->discount; ?></span></strong></label>
                                                            </div>
                                                            <div class=" has-static">
                                                                <label class="form-label">Amount Payable : <strong>$<span id="cart_total_text"><?= $model->cart_total; ?></span></strong></label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <?= $form->field($model, 'monthly_sub_uuid', ['template' => '{input}{error}'])->hiddenInput(['value' => (!empty($monthly_package)) ? $monthly_package->uuid : ''])->label(false); ?>
                                                    <?= $form->field($model, 'monthly_sub_total', ['template' => '{input}{error}'])->hiddenInput(['value' => (!empty($monthly_package)) ? $monthly_package->total_price : 0])->label(false); ?>

                                                    <?= $form->field($model, 'discount', ['template' => '{input}{error}'])->hiddenInput()->label(false); ?>
                                                    <?= $form->field($model, 'cart_total', ['template' => '{input}{error}'])->hiddenInput()->label(false); ?>
                                                    <?= $form->field($model, 'coupon_uuid', ['template' => '{input}{error}'])->hiddenInput()->label(false); ?>
                                                </div>
                                            </div>

                                            <div class="spacer"></div>


                                            <?php if (!empty($saved_cc_detail)) { ?>
                                                <div class="card credit_card_custom_css">

                                                    <?= $form->field($model, 'cc_uuid', ['template' => '{input}{error}'])->hiddenInput(['value' => $saved_cc_detail['uuid']])->label(false); ?>
                                                    <?= $form->field($model, 'cc_name', ['template' => '{input}{error}'])->hiddenInput(['value' => $saved_cc_detail['name']])->label(false); ?>
                                                    <?= $form->field($model, 'cc_number', ['template' => '{input}{error}'])->hiddenInput(['value' => General::decrypt_str($saved_cc_detail['number']) ])->label(false); ?>
                                                    <?php
                                                    $exp_date = date('Y-m', strtotime($saved_cc_detail['expire_date']));
                                                    echo $form->field($model, 'cc_month_year', ['template' => '{input}{error}'])->hiddenInput(['value' => $exp_date])->label(false);
                                                    ?>
                                                    <div class="card-header" role="tab">
                                                        <button type="button" class="btn btn-purple btn-sm float-right delete_credit_card" data-uuid="<?= $saved_cc_detail['uuid']; ?>">Remove Card</button>
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="form-row">
                                                            <div class="col-md-7 mb-0">
                                                                <span class="form-control bg-gray-field marginBottom15" ><?= $saved_cc_detail['name']; ?></span>
                                                            </div>
                                                            <div class="col-md-5 mb-0">
                                                                <span class="form-control bg-gray-field marginBottom15" ><?= General::ccMasking(General::decrypt_str($saved_cc_detail['number'])); ?></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="col-md-3 mb-0">
                                                                <?= $form->field($model, 'cc_cvv')->passwordInput(['class' => 'form-control my_input_border', 'maxlength' => 5, 'placeholder' => 'CVV', 'autocomplete' => 'off'])->label(false) ?>
                                                            </div>
                                                            <div class="col-md-4 mb-0">
                                                                <span class="form-control bg-gray-field" ><?= date("Y-m", strtotime($saved_cc_detail['expire_date'])); ?></span>
                                                            </div>
                                                        </div>
                                                        <div class="spacer"></div>
                                                        <div class="form-row">
                                                            <div class="col-md-8 term_condition_checkbox_parent_col">
                                                                <?php
                                                                echo $form->field($model, 'term_condition_checkbox',['template' => ' {input} <a data-toggle="modal" href="#terms_condition_model">I agree to Musiconn Terms and Conditions</a>{error} '])->checkbox([
                                                                    'class' => 'skin-square-red float-left '],false);
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            <?php } else { ?>
                                                <div class="card credit_card_custom_css">
                                                    <div class="card-body">
                                                        <div class="form-row">
                                                            <div class="col-md-7 mb-0">
                                                                <?= $form->field($model, 'cc_name',['inputOptions' => ['autocomplete' => 'off']])->textInput(['class' => 'form-control my_input_border marginBottom15', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('cc_name')])->label(false) ?>
                                                            </div>
                                                            <div class="col-md-5 mb-0">
                                                                <?= $form->field($model, 'cc_number',['inputOptions' => ['autocomplete' => 'new-password']])->textInput(['class' => 'form-control my_input_border marginBottom15', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('cc_number')])->label(false) ?>
                                                            </div>

                                                        </div>
                                                        <div class="form-row">
                                                            <div class="col-md-3 mb-0">
                                                                <?= $form->field($model, 'cc_cvv',['inputOptions' => ['autocomplete' => 'new-password']])->passwordInput(['class' => 'form-control my_input_border marginBottom15', 'maxlength' => 5, 'placeholder' => $model->getAttributeLabel('cc_cvv')])->label(false) ?>
                                                            </div>

                                                            <div class="col-md-4 mb-0">
                                                                <?= $form->field($model, 'cc_month_year')->textInput(['class' => 'form-control custom_expire_month_year my_input_border', 'data-min-view-mode' => 'months', 'data-start-view' => '2', 'data-format' => 'yyyy-mm', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('cc_month_year')])->label(false) ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="col-md-4 cc_ckeckbx_parent_col">
                                                                <?php
                                                                echo $form->field($model, 'cc_checkbox')->checkbox([
                                                                    'template' => '<p class="save_future pull-left"><label class="icheck-label form-label" for="save"> {input}  </label> <p>',
                                                                    'class' => 'skin-square-red float-left save_future'])->label(false);
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="col-md-8 term_condition_checkbox_parent_col">
                                                                <?php
                                                                echo $form->field($model, 'term_condition_checkbox',['template' => ' {input} <a data-toggle="modal" href="#terms_condition_model">I agree to Musiconn Terms and Conditions</a>{error} '])->checkbox([
                                                                    'class' => 'skin-square-red float-left '],false);
                                                                ?>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <div class="spacer"></div>
                                            <div class="payment-errors text-danger"></div>

                                            <?= Html::button(Yii::t('app', 'Proceed Securely'), ['class' => 'btn btn-primary', 'id' => 'payBtn', 'onclick' => 'submitform()', 'style' => 'float: right;']) ?>

                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="card credit_card_custom_css">
                                            <div class="card-body">
                                                <p class="text-primary"><strong>Instrument :</strong> <?= $instrumentDetail->name; ?></p>
                                                <p class="text-primary"><strong>Plan :</strong> <?= $planDetail->name; ?></p>
                                                <p class="text-primary"><strong>Tutor :</strong> <?= $tutorDetail->first_name . ' ' . $tutorDetail->last_name; ?></p>
                                                <p class="text-primary"><strong>Session Time:</strong> <?= General::displayTimeFormat($step_data['step_4']['start_hour'],'h:i A'); ?></p>
                                                <p class="text-primary"><strong>Session Dates:</strong> <?= $step_data['step_4']['all_dates']; ?></p>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>


            <?php echo Html::beginForm(['index'], 'post', ['id' => 'gostep-reload-5', 'class' => 'form-inline d-block']) ?>
            <input type="hidden" name="step" value="5">
            <input type="hidden" name="prev" value="5">
            <input type="hidden" name="remove_card" value="yes">
            <?php echo Html::endForm() ?>
            </section>
        </div>

        <!-- modal start -->
        <div class="modal fade" id="terms_condition_model" tabindex="-1" role="dialog" aria-labelledby="ultraModal-Label" aria-hidden="true">
            <div class="modal-dialog animated zoomIn">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Terms and Conditions</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                            <?= \app\models\Settings::getSettingsByName('payment_terms_and_conditions'); ?>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-primary" type="button">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- modal end -->



<script type="text/javascript">






</script>
