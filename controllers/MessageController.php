<?php

namespace app\controllers;

use Yii;
use app\models\Chat;
use app\models\ChatAdmin;
use app\models\Message;
use app\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

use yii\web\UploadedFile;

use app\models\BookedSession;

class MessageController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['report'],
                        'allow' => true,
                        'roles' => ['OWNER','ADMIN'],
                    ]
                ],
            ]
        ];
    }

    public function actionIndex()
    {
        $request = Yii::$app->getRequest();
        $admins = array(
            "be68a826-6993-410d-a82a-d7e694d8a15d",
            "4bf67ea2-83cc-4c6c-ac74-3d15622bbd3d"
        );

        if ($request->isPost) {
            //post data attachment
            $data = ($request->post());
            $model = new Message();
            $model->attachment = UploadedFile::getInstance($model, 'attachment');
            return $model->uploadFile();
        } else {
            $from = ""; 
            $to = Yii::$app->request->get('to');
            $role = Yii::$app->user->identity->role;
            $contacts = array(); $idFilter = array();

            // GET CONTACTS
            if (in_array($role, ["TUTOR","STUDENT"])) {
                $uuid = strtolower($role)."_uuid";
                $from = Yii::$app->user->identity->$uuid;
                $collection = BookedSession::find()
                ->where([$uuid=>$from])
                ->distinct()
                ->all();
                $contact = $role == "STUDENT"? "tutorUu": "studentUu";
                $contact_field = $role == "STUDENT"? "tutor_uuid":"student_uuid";
                foreach($collection as $bs) {
                    if (!in_array($bs[$contact_field], $idFilter)) {
                        array_push($idFilter, $bs[$contact_field]);
                        array_push($contacts, $bs[$contact]);
                    }
                }
            } else {
                $from = Yii::$app->user->identity->admin_uuid;
                $collection = User::find()
                ->where(["admin_uuid"=>null])
                ->andWhere(["tutor_uuid"=>null])
                ->all();

                foreach($collection as $user) {
                    if (!empty($user["student_uuid"]) && !in_array($user["student_uuid"], $contacts)) {
                        array_push($idFilter, $user["student_uuid"]);
                        array_push($contacts, $user["student"]);
                    }
                    if (!empty($user["tutor_uuid"]) && !in_array($user["tutor_uuid"], $contacts)) {
                        array_push($idFilter, $user["tutor_uuid"]);
                        array_push($contacts, $user["tutor"]);
                    }
                }

            }


            $tutor_uuid = ""; $student_uuid = "";
            if ($role == "STUDENT") {
                $student_uuid = $from;
            } else if ($role == "TUTOR") {
                $tutor_uuid = $from;
            }
            // GET MESSAGES
            $chat = array();
            if (!empty($to)) {
                if ($to == "admin" || in_array($from, $admins)) {
                    $sender_uuid = $to=="admin"? $from:$to;
                    $chat = ChatAdmin::find()->where(["sender_uuid"=>$sender_uuid])->one();
                    if (!in_array($role, ["TUTOR", "STUDENT"])) {
                        $student_uuid = $to; // only sudent has contact with admin
                    }
                } else {
                    $filter = [];
                    if ($role == "STUDENT") {
                        $filter = ["student_uuid"=>$from,"tutor_uuid"=>$to];
                        $tutor_uuid = $to;
                    } else {
                        $filter = ["tutor_uuid"=>$from,"student_uuid"=>$to];
                        $student_uuid = $to;
                    }
                    $chat = Chat::find()->where($filter)->one();
                }
            }


            if ($role == "STUDENT") {
                $this->layout = "student_layout";
            }
            $res = [
                "model"=>new Message(),
                "contacts" => $contacts,
                "chat" => $chat,
                "user_role"=>$role,
                "tutor_uuid"=>$tutor_uuid,
                "student_uuid"=>$student_uuid,
                "from"=>$from,
                "to"=>$to,
                "attachment_url"=>Yii::$app->params["messages"]["attachment_url"],
                "websocket_url"=>Yii::$app->params["websocket_url"],
                "admin_uuids"=>$admins
            ];
            return $this->render('index', $res);
        }
    }

    public function actionReport() {
        $collection = Chat::find()->all();

        $res = [
            "collection"=>$collection
        ];
        return $this->render('report', $res);
    }


}