<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\filters\AccessControl;
use app\libraries\General;
use app\models\StudentTuitionBookingSearch;
use app\models\StudentTuitionBooking;
use app\models\BookedSession;
use app\models\Student;
use app\models\Tutor;
use app\models\User;
use app\models\Instrument;
use app\models\OnlineTutorialPackage;
use app\models\MonthlySubscriptionFee;
use app\models\StudentCreditCard;
use app\models\TutorialType;
use app\models\RescheduleSession;
use app\models\StandardPlanOptionRate;
use app\models\OnlineTutorialPackageType;
use app\models\OnlineTutorialPackageTutorwise;
use app\models\StudentTuitionStandardBookingLockingPeriod;
use app\models\Book;

class BookController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
             'access' => [
		      'class' => AccessControl::className(),
		      'rules' => [
			      [
			      'actions' => ['index'],
			      'allow' => true,
			      'roles' => ['STUDENT'],
			      ],
			      [
			      'actions' => ['book-again'],
			      'allow' => true,
			      'roles' => ['STUDENT'],
			      ],
			      [
			      'actions' => ['get-timeslots'],
			      'allow' => true,
			      'roles' => ['STUDENT'],
			      ],
			      [
			      'actions' => ['select-time'],
			      'allow' => true,
			      'roles' => ['STUDENT'],
			      ],
				[
			      'actions' => ['cb-select-time'],
			      'allow' => true,
			      'roles' => ['STUDENT'],
			      ],
				[
			      'actions' => ['change-booking'],
			      'allow' => true,
			      'roles' => ['STUDENT'],
			      ],
				[
			      'actions' => ['cb-get-timeslots'],
			      'allow' => true,
			      'roles' => ['STUDENT'],
			      ],
				[
			      'actions' => ['refresh-cart'],
			      'allow' => true,
			      'roles' => ['STUDENT'],
			      ],
				[
			      'actions' => ['reschedule-request', 'get-timeslots-reschedule', 'select-time-reschedule'],
			      'allow' => true,
			      'roles' => ['STUDENT', 'TUTOR'],
			      ],
				[
			      'actions' => ['reschedule'],
			      'allow' => true,
			      'roles' => ['TUTOR'],
			      ],
                [
                'actions' => ['reschedule', 'cancel-tutor-reschedule-request'],
                'allow' => true,
                'roles' => ['TUTOR'],
                ],
		      ],
              ], 
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
        ];
    }

    public function beforeAction($action) {
        if ($action->id == "index") {
            //IF Book Again then direct post on step 3 - Start
            $BA_step_data = StudentTuitionBooking::getSession("BOOKAGAIN_");
            if (!empty($BA_step_data)) {
                $this->enableCsrfValidation = false;
                $_POST = (isset($BA_step_data['postdata'])) ? $BA_step_data['postdata'] : [];
                StudentTuitionBooking::unsetSession("BOOKAGAIN_");
            }
            //IF Book Again then direct post on step 3 - End
        }
        if ($action->id == "cancel-tutor-reschedule-request") {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function actionTest() {

        $this->layout = "student_layout";

        $model_student = Student::findOne('5d193b03-fd52-4e63-a704-636458635847');
        $model_tutor = Tutor::findOne('54e2b0ee-ba41-4fcc-a49e-b4d222a3015f');
        $selected_date = '2020-02-03';
        $logged_user_tz = 'Australia/Sydney';
        $tutor_working_plan = json_decode($model_tutor->working_plan, true);


        $result = Book::get_tutor_available_time_period(
                        $model_tutor->uuid, $tutor_working_plan, $selected_date, $model_student->student_uuid, $model_tutor->user->timezone, $model_student->user->timezone, $logged_user_tz
        );

        echo "<pre>";
        echo "<br>logged_user_tz >> ";
        print_r($logged_user_tz);
        echo "<br>Tutor_tz >> ";
        print_r($model_tutor->user->timezone);
        echo "<br>Student tz >> ";
        print_r($model_student->user->timezone);
        echo "<br>Date >> ";
        print_r($selected_date);
        echo "<br>tutor_working_plan >> ";
        print_r($tutor_working_plan);
        echo "</pre>";
        exit;
    }

    public function actionBookAgain($id) {

        if (($modelSB = StudentTuitionBooking::findOne($id)) !== null) {

            $model = new Book();
            $model->scenario = 'step_1';

            $token = "NEWBOOK_" . $modelSB->student_uuid;
            StudentTuitionBooking::unsetSession($token);
            $step_data['step_1'] = ['instrument_uuid' => $modelSB->instrument_uuid, 'instrumentcheckbox' => 0];
            StudentTuitionBooking::setSession($token, 'step_1', $step_data['step_1']);
            $postdata['Book'] = ['tutor_uuid' => '', 'tutorcheckbox' => ''];
            $postdata['step'] = 2;

            $tutorList = StudentTuitionBooking::getInstrumentWiseTutorList($step_data['step_1']['instrument_uuid']);
            $tutor_uuid_array = array_column($tutorList, 'uuid');
            if (in_array($modelSB->tutor_uuid, $tutor_uuid_array)) {

                //Pratik Start
                $tutorBA = array_search($modelSB->tutor_uuid, $tutor_uuid_array);
                if (isset($tutorBA) && isset($tutorList[$tutorBA])) {
                    $tutorBADetails = $tutorList[$tutorBA];

                    $TutorInstrumentPlanOptions = StudentTuitionBooking::getTutorInstrumentPlanOptions($tutorBADetails['online_tutorial_package_type_uuid'], $tutorBADetails['uuid'], $step_data['step_1']['instrument_uuid']);
                    $InstrumentWiseTutorPriceList = StudentTuitionBooking::getInstrumentWiseTutorPriceList($tutorBADetails['online_tutorial_package_type_uuid'], $TutorInstrumentPlanOptions['standard_plan_rate_option']);
                    // $tutor_img_dataUri = \app\libraries\General::getTutorImage($e['uuid']);
                    $tutor30minPriceInCoin = \app\libraries\General::priceToMusicoins($InstrumentWiseTutorPriceList['half']);
                    $tutor60minPriceInCoin = \app\libraries\General::priceToMusicoins($InstrumentWiseTutorPriceList['full']);
                    //exit;
                    $step_data['step_2']['tutor_half_hour_price'] = $InstrumentWiseTutorPriceList['half'];
                    $step_data['step_2']['tutor_half_hour_price_in_coin'] = $tutor30minPriceInCoin;
                    $step_data['step_2']['tutor_hour_price'] = $InstrumentWiseTutorPriceList['full'];
                    $step_data['step_2']['tutor_hour_price_in_coin'] = $tutor60minPriceInCoin;
                    $step_data['step_2']['tutor_further_details'] = $InstrumentWiseTutorPriceList;
                }
//print_r($step_data);
//                exit;
                //Pratik End
                $step_data['step_2'] = ['tutor_uuid' => $modelSB->tutor_uuid, 'tutorcheckbox' => $modelSB->tutor_uuid, 'tutor_half_hour_price' => $InstrumentWiseTutorPriceList['half'], 'tutor_half_hour_price_in_coin' => $tutor30minPriceInCoin, 'tutor_hour_price' => $InstrumentWiseTutorPriceList['full'], 'tutor_hour_price_in_coin' => $tutor60minPriceInCoin, 'tutor_further_details' => $InstrumentWiseTutorPriceList];
                // print_r($step_data);exit;
                StudentTuitionBooking::setSession($token, 'step_2', $step_data['step_2']);
                $postdata['Book'] = ['lesson_length' => $modelSB->tutorial_min];
                $postdata['step'] = 3;
            }

            //---------- Set Book Again Data
            StudentTuitionBooking::setSession("BOOKAGAIN_", 'steps', $step_data);
            StudentTuitionBooking::setSession("BOOKAGAIN_", 'postdata', $postdata);

            return $this->redirect(['/book']);
        }
        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * New Lesson Booking 
     */
    public function actionIndex() {

        $this->layout = "student_layout";
        $token = "NEWBOOK_" . Yii::$app->user->identity->student_uuid;
//    StudentTuitionBooking::unsetSession($token);    
        $model = new Book();
        $model->student_uuid = Yii::$app->user->identity->student_uuid;

        if (!Yii::$app->request->isPost && empty($_POST)) {
            StudentTuitionBooking::unsetSession($token);
        }

        $step_data = StudentTuitionBooking::getSession($token);


        if (!empty($_POST['step']) && $_POST['step'] == '1') {

            $model->scenario = 'step_1';
            if (empty($_POST['Book'])) {
                $model->instrument_uuid = $step_data['step_1']['instrument_uuid'];
            } else {
                $model->attributes = $_POST['Book'];
            }

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                StudentTuitionBooking::setSession($token, 'step_1', $_POST['Book']);
                $step_data = StudentTuitionBooking::getSession($token);

                return $this->render('step_2', [
                            'model' => $model,
                            'step_data' => $step_data,
                ]);
            }

            return $this->render('step_1', [
                        'model' => $model,
                        'step_data' => $step_data,
            ]);
        } elseif (!empty($_POST['step']) && $_POST['step'] == '2') {

            $model->scenario = 'step_2';
            if (empty($_POST['Book'])) {
                $model->tutor_uuid = $step_data['step_2']['tutor_uuid'];
                $model->tutorcheckbox = $step_data['step_2']['tutor_uuid'];
                $tutorList = StudentTuitionBooking::getInstrumentWiseTutorList($step_data['step_1']['instrument_uuid']);
            $tutor_uuid_array = array_column($tutorList, 'uuid');
            if (in_array($model->tutor_uuid, $tutor_uuid_array)) {

                //Pratik Start
                $tutorBA = array_search($model->tutor_uuid, $tutor_uuid_array);
                if (isset($tutorBA) && isset($tutorList[$tutorBA])) {
                    $tutorBADetails = $tutorList[$tutorBA];

                    $TutorInstrumentPlanOptions = StudentTuitionBooking::getTutorInstrumentPlanOptions($tutorBADetails['online_tutorial_package_type_uuid'], $tutorBADetails['uuid'], $step_data['step_1']['instrument_uuid']);
                    $InstrumentWiseTutorPriceList = StudentTuitionBooking::getInstrumentWiseTutorPriceList($tutorBADetails['online_tutorial_package_type_uuid'], $TutorInstrumentPlanOptions['standard_plan_rate_option']);
                    // $tutor_img_dataUri = \app\libraries\General::getTutorImage($e['uuid']);
                    $tutor30minPriceInCoin = \app\libraries\General::priceToMusicoins($InstrumentWiseTutorPriceList['half']);
                    $tutor60minPriceInCoin = \app\libraries\General::priceToMusicoins($InstrumentWiseTutorPriceList['full']);
                    //exit;
                    $step_data['step_2']['tutor_half_hour_price'] = $InstrumentWiseTutorPriceList['half'];
                    $step_data['step_2']['tutor_half_hour_price_in_coin'] = $tutor30minPriceInCoin;
                    $step_data['step_2']['tutor_hour_price'] = $InstrumentWiseTutorPriceList['full'];
                    $step_data['step_2']['tutor_hour_price_in_coin'] = $tutor60minPriceInCoin;
                     $step_data['step_2']['InstrumentWiseTutorPriceList'] = $InstrumentWiseTutorPriceList;
                }
//print_r($step_data);
//                exit;
                //Pratik End
                $step_data['step_2'] = ['tutor_uuid' => $model->tutor_uuid, 'tutorcheckbox' => $model->tutor_uuid, 'tutor_half_hour_price' => $InstrumentWiseTutorPriceList['half'], 'tutor_half_hour_price_in_coin' => $tutor30minPriceInCoin, 'tutor_hour_price' => $InstrumentWiseTutorPriceList['full'], 'tutor_hour_price_in_coin' => $tutor60minPriceInCoin, 'tutor_further_details' => $InstrumentWiseTutorPriceList];
                // print_r($step_data);exit;
                //StudentTuitionBooking::setSession($token, 'step_2', $step_data['step_2']);
                //$postdata['Book'] = ['lesson_length' => $modelSB->tutorial_min];
                //$postdata['step'] = 3;
            }
            } else {
                $model->attributes = $_POST['Book'];
            }

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {

                StudentTuitionBooking::setSession($token, 'step_2', $_POST['Book']);
                $step_data = StudentTuitionBooking::getSession($token);

                return $this->render('step_3', [
                            'model' => $model,
                            'step_data' => $step_data,
                ]);
            }

            return $this->render('step_2', [
                        'model' => $model,
                        'step_data' => $step_data,
            ]);
        } elseif (!empty($_POST['step']) && $_POST['step'] == '3') {

            $model->scenario = 'step_3';
            if (empty($_POST['Book'])) {
                $model->lesson_length = $step_data['step_3']['lesson_length'];
                $model->recurring = $step_data['step_3']['recurring'];
                $model->lesson_type = $step_data['step_3']['lesson_type'];
                $model->lesson_total = $step_data['step_3']['lesson_total'];
            } else {
                $model->attributes = $_POST['Book'];
            }

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {

		if($model->recurring == "NO"){
			$_POST['Book']['lesson_type'] = 'W';
			$_POST['Book']['lesson_total'] = 1;
		}
                StudentTuitionBooking::setSession($token, 'step_3', $_POST['Book']);
                $step_data = StudentTuitionBooking::getSession($token);

                return $this->render('step_4', [
                            'model' => $model,
                            'step_data' => $step_data,
                ]);
            }

            return $this->render('step_3', [
                        'model' => $model,
                        'step_data' => $step_data,
            ]);
        } elseif (!empty($_POST['step']) && $_POST['step'] == '4') {


            $model->scenario = 'step_4';
	    if (empty($_POST['Book'])) {
                $model->session_when_start = $step_data['step_4']['session_when_start'];
                $model->start_hour = $step_data['step_4']['start_hour'];
                $model->all_lesson_data = $step_data['step_4']['all_lesson_data'];
                $model->selected_sesson_html = $step_data['step_4']['selected_sesson_html'];
            } else {
                //$model->attributes = $_POST['Book'];
		$model->attributes = Yii::$app->getRequest()->post('Book');
            }

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {

                StudentTuitionBooking::setSession($token, 'step_4', Yii::$app->getRequest()->post('Book'));
                $step_data = StudentTuitionBooking::getSession($token);
//echo "<pre>";print_r($step_data);echo "</pre>";exit;
                return $this->render('step_5', [
                            'model' => $model,
                            'step_data' => $step_data,
                ]);
            }

            return $this->render('step_4', [
                        'model' => $model,
                        'step_data' => $step_data,
            ]);
        } elseif (!empty($_POST['step']) && $_POST['step'] == '5') {


            $model->scenario = 'step_5';
            if (empty(Yii::$app->getRequest()->post('Book'))) {
                $model->term_condition_checkbox = $step_data['step_5']['term_condition_checkbox'];
            } else {
                $model->attributes = Yii::$app->getRequest()->post('Book');
//                echo "<pre>";
//                print_r($model);
//                echo "</pre>";
            }

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                echo '<script>blockBody("Please wait request under process...");</script>';
                //StudentTuitionBooking::setSession($token, 'step_4', Yii::$app->getRequest()->post('Book'));
                $step_data = StudentTuitionBooking::getSession($token);
                //echo "<pre>";print_r($step_data);echo "</pre>";exit;
                $musicoin_array = Book::make_cart_submit_array($_POST);
                $payment_response = Book::musicoinPayment($musicoin_array);
                StudentTuitionBooking::unsetSession($token);

                if (isset($payment_response['code']) && $payment_response['code'] == 200) {

                    Yii::$app->getSession()->setFlash('success', 'Lessons booked successfully.');
                } else {

                    $message = (isset($payment_response['message'])) ? $payment_response['message'] : 'Sorry, something went wrong.';
                    Yii::$app->getSession()->setFlash('danger', $message);
                }

                return $this->redirect(['booked-session/upcoming']);
               
            }

            return $this->render('step_5', [
                        'model' => $model,
                        'step_data' => $step_data,
            ]);
        } else {
            $model->scenario = 'step_1';
            return $this->render('step_1', [
                        'model' => $model,
                        'step_data' => $step_data,
            ]);
        }
    }

    /**
     * Get time slots of selected date
     */
    public function actionGetTimeslots() {

        if (!Yii::$app->request->isAjax) {
            throw new \yii\web\HttpException(505, 'You are not authorised to access this page');
        }
        $result = [];
        $student_uuid = Yii::$app->getRequest()->get('student_uuid');
        $token = "NEWBOOK_" . $student_uuid;
        $step_data = StudentTuitionBooking::getSession($token);
        if (!empty($step_data['step_2']['tutor_uuid'])) {

            $tutor_uuid = $step_data['step_2']['tutor_uuid'];
            $tutorDetail = Tutor::findOne($tutor_uuid);
            $session_date = Yii::$app->getRequest()->get('selected_when_date');
            $session_minute = $step_data['step_3']['lesson_length'];

            $tutor_working_plan = json_decode($tutorDetail->working_plan, true);
            $tutor_tz = General::getTutorTimezone($tutor_uuid);
            $student_tz = General::getStudentTimezone($student_uuid);
            $logged_user_tz = Yii::$app->user->identity->timezone;
            $get_hours = Book::get_available_hours($tutor_uuid, $tutor_working_plan, $session_date, $session_minute, $student_uuid, $tutor_tz, $student_tz, $logged_user_tz);
            $get_hours = array_unique($get_hours);
            $option_html = '';

            if (!empty($get_hours)) {
                foreach ($get_hours as $value) {
                    $display = date('h:i A', strtotime($value));
                    $result[$value] = $display;

                    $checked = (isset($step_data['step_4']['start_hour']) && $value == $step_data['step_4']['start_hour']) ? 'checked="checked"' : '';
                    $option_html .= '<div class="col-md-6 ">'
                            . '<ul class="products-list product-list-in-box" style="white-space: nowrap;">'
                            . '<li class="st-p3"><label class="contai_r" style="padding-left: 17px;">'
                            . '<input type="radio" onclick="select_time(\'' . $value . '\');" value="' . $value . '" name="Book[start_hour]" ' . $checked . '>'
                            . '<span class="checkmark"></span>'
                            . '</label>'
                            . '<span class="mn_3">' . $display . '</span>'
                            . '</li>'
                            . '</ul>'
                            . '</div>';
                }
            } else {
                $option_html = '<div class="col-md-12 text-center">No time available for this date.</div>';
            }
        }

        Yii::$app->response->format = 'json';
        return ['result' => $result, 'html' => $option_html];
    }

    /**
     * Get time slots of selected date
     */
    public function actionSelectTime() {

        if (!Yii::$app->request->isAjax) {
            throw new \yii\web\HttpException(505, 'You are not authorised to access this page');
        }

        $student_uuid = Yii::$app->getRequest()->post('student_uuid');
        $selected_time = Yii::$app->getRequest()->post('selected_time');
        $token = "NEWBOOK_" . $student_uuid;
        $step_data = StudentTuitionBooking::getSession($token);
        $result = '';
        $html = '<div><strong>Selected:</strong></div>';

        if (!empty($step_data['step_2']['tutor_uuid']) && !empty($selected_time)) {

            $tutor_uuid = $step_data['step_2']['tutor_uuid'];
            $lesson_minute = (!empty($step_data['step_3']['lesson_length'])) ? $step_data['step_3']['lesson_length'] : 30;
            $bitween_days = ($step_data['step_3']['lesson_type'] == 'F') ? 14 : 7;
            $lesson_type_label = ($step_data['step_3']['lesson_type'] == 'F') ? 'Fortnightly' : 'Weeks';
            $lesson_total = ($step_data['step_3']['lesson_total'] > 1) ? $step_data['step_3']['lesson_total'] : 1;
            $result = Book::get_lesson_for_booking($selected_time, $student_uuid, $tutor_uuid, $lesson_minute, $lesson_total, $bitween_days);

            $result = (!empty($result)) ? $result : '';
            if (!empty($result)) {

                if ($lesson_total > 1) {
                    $html .= '<div>Every ' . date('l, h:iA', strtotime($result[0]['lesson_time'])) . ' - ' . date('h:iA', strtotime($result[0]['lesson_time'] . ' +' . $lesson_minute . ' minute')) . '</div><br>';
                    $html .= '<div><strong>Recurring:</strong> ' . $lesson_total . ' ' . $lesson_type_label . '</div>';
                }
                foreach ($result as $value) {
                    $btn = $text_class = '';
                    if (isset($value['is_available']) && $value['is_available'] == 'NO') {
                        $btn = '<button type="button" class="label label-warning rebook_btn" data-time="' . $value['lesson_time'] . '" data-student="' . $student_uuid . '">CHANGE</button>';
                        $text_class = 'text-red';
                    }
                    $display_date = date('d M Y - h:i A', strtotime($value['lesson_time']));
                    $html .= '<div class="' . $text_class . '">' . $display_date . $btn . '</div>';
                }
            }
        }

        Yii::$app->response->format = 'json';
        return ['all_lesson_data' => $result, 'html' => $html];
    }

    public function actionCbSelectTime() {

        if (!Yii::$app->request->isAjax) {
            throw new \yii\web\HttpException(505, 'You are not authorised to access this page');
        }

        $student_uuid = Yii::$app->getRequest()->post('student_uuid');
        $selected_time = Yii::$app->getRequest()->post('selected_time');
        $token = "NEWBOOK_" . $student_uuid;
        $step_data = StudentTuitionBooking::getSession($token);
        $result = '';
        $html = '<div><strong>Selected:</strong></div>';

        if (!empty($step_data['step_2']['tutor_uuid']) && !empty($selected_time)) {

            $tutor_uuid = Yii::$app->getRequest()->post('cb_tutor_uuid');
            $lesson_minute = (!empty($step_data['step_3']['lesson_length'])) ? $step_data['step_3']['lesson_length'] : 30;
            $bitween_days = ($step_data['step_3']['lesson_type'] == 'F') ? 14 : 7;
            $lesson_type_label = ($step_data['step_3']['lesson_type'] == 'F') ? 'Fortnightly' : 'Weeks';
            $lesson_total = ($step_data['step_3']['lesson_total'] > 1) ? $step_data['step_3']['lesson_total'] : 1;
            $result = Book::get_lesson_for_booking($selected_time, $student_uuid, $tutor_uuid, $lesson_minute, $lesson_total, $bitween_days);

            $result = (!empty($result)) ? $result : '';
            if (!empty($result)) {

                /* if($lesson_total > 1){
                  $html .= '<div>Every '.date('l, h:iA', strtotime($result[0]['lesson_time'])).' - '.date('h:iA', strtotime($result[0]['lesson_time'].' +'.$lesson_minute.' minute')).'</div><br>';
                  $html .= '<div><strong>Recurring:</strong> '.$lesson_total.' '.$lesson_type_label.'</div>';
                  } */
                foreach ($result as $value) {
                    $btn = $text_class = '';
                    /* if(isset($value['is_available']) && $value['is_available'] == 'NO'){
                      $btn = '<button type="button" class="label label-warning rebook_btn" data-time="'.$value['lesson_time'].'" data-student="'.$student_uuid.'">CHANGE</button>';
                      $text_class = 'text-red';
                      } */
                    $display_date = date('d M Y - h:i A', strtotime($value['lesson_time']));
                    $html .= '<div class="' . $text_class . '">' . $display_date . $btn . '</div>';
                    break;
                }
            }
        }

        Yii::$app->response->format = 'json';
        return ['all_lesson_data' => ((is_array($result) && !empty($result)) ? $result[0] : ''), 'html' => $html];
    }

    /**
     * Change time of selected date unavailable time slot (Re-book)
     */
    public function actionChangeBooking() {

        if (!Yii::$app->request->isAjax) {
            throw new \yii\web\HttpException(505, 'You are not authorised to access this page');
        }

        $student_uuid = Yii::$app->getRequest()->get('student_uuid');
        $current_datetime = Yii::$app->getRequest()->get('current_datetime');
        $token = "NEWBOOK_" . $student_uuid;
        $step_data = StudentTuitionBooking::getSession($token);
        $all_lesson_data = Yii::$app->getRequest()->get('all_lesson_data');
        $model = new Book();
        $model->student_uuid = $student_uuid;
        $model->scenario = 'new_change_booking';
        if (!empty(Yii::$app->getRequest()->get('cb_lesson_data'))) {
            $token = "NEWBOOK_" . $student_uuid;
            $step_data = StudentTuitionBooking::getSession($token);
            $all_lesson_data = json_decode($all_lesson_data, true);
            $cb_lesson_data = json_decode(Yii::$app->getRequest()->get('cb_lesson_data'), true);
            $final_array = array();
            if (!empty($step_data['step_2']['tutor_uuid'])) {
                foreach ($all_lesson_data as $final) {
                    //echo date("Y-m-d H:i",strtotime($final['lesson_time'])) .' == '. date('Y-m-d H:i', strtotime($current_datetime));
                    if ($final['is_available'] == 'NO' && $final['tutor_uuid'] == $step_data['step_2']['tutor_uuid'] && date("Y-m-d H:i", strtotime($final['lesson_time'])) == date('Y-m-d H:i', strtotime($current_datetime))) {
                        $final_array[] = $cb_lesson_data;
                    } else {
                        $final_array[] = $final;
                    }
                }

                //echo "</pre>";print_r( $final_array);echo "</pre>";exit;
                // echo "fsfdsfs";
                $html = '<div><strong>Selected:</strong></div>';
                if (!empty($final_array)) {

                    /* if ($lesson_total > 1) {
                      $html .= '<div>Every ' . date('l, h:iA', strtotime($final_array[0]['lesson_time'])) . ' - ' . date('h:iA', strtotime($final_array[0]['lesson_time'] . ' +' . $lesson_minute . ' minute')) . '</div><br>';
                      $html .= '<div><strong>Recurring:</strong> ' . $lesson_total . ' ' . $lesson_type_label . '</div>';
                      } */
                    foreach ($final_array as $value) {
                        $btn = $text_class = '';
                        if (isset($value['is_available']) && $value['is_available'] == 'NO') {
                            $btn = '<button type="button" class="label label-warning rebook_btn" data-time="' . $value['lesson_time'] . '" data-student="' . $student_uuid . '">CHANGE</button>';
                            $text_class = 'text-red';
                        }
                        $display_date = date('d M Y - h:i A', strtotime($value['lesson_time']));
                        $html .= '<div class="' . $text_class . '">' . $display_date . $btn . '</div>';
                    }
                }


                Yii::$app->response->format = 'json';
                return ['all_lesson_data' => $final_array, 'html' => $html];
                exit;
//                return $this->render('step_4', [
//                            'model' => $model,
//                            'step_data' => $step_data,
//                ]);
            }
        }
        return $this->renderAjax('change_booking', [
                    'step_data' => $step_data,
                    'current_datetime' => $current_datetime,
                    'model' => $model,
                    'student_uuid' => $student_uuid,
                    'all_lesson_data' => $all_lesson_data,
        ]);
    }

    public function actionCbGetTimeslots() {

        if (!Yii::$app->request->isAjax) {
            throw new \yii\web\HttpException(505, 'You are not authorised to access this page');
        }
        $result = [];
        //print_r(Yii::$app->getRequest()->get());
        $student_uuid = Yii::$app->getRequest()->get('student_uuid');
        $token = "NEWBOOK_" . $student_uuid;
        $step_data = StudentTuitionBooking::getSession($token);
        if (!empty(Yii::$app->getRequest()->get('cb_tutor_uuid'))) {
            //Yii::$app->getRequest()->get('all_lesson_data') = '';
            $all_lesson_data = (array) Yii::$app->getRequest()->get('all_lesson_data');
            $tutor_uuid = Yii::$app->getRequest()->get('cb_tutor_uuid');
            $tutorDetail = Tutor::findOne($tutor_uuid);
            $session_date = Yii::$app->getRequest()->get('selected_when_date');
            $session_minute = $step_data['step_3']['lesson_length'];

            $tutor_working_plan = json_decode($tutorDetail->working_plan, true);
            $tutor_tz = General::getTutorTimezone($tutor_uuid);
            $student_tz = General::getStudentTimezone($student_uuid);
            $logged_user_tz = Yii::$app->user->identity->timezone;
            $get_hours = Book::get_available_hours($tutor_uuid, $tutor_working_plan, $session_date, $session_minute, $student_uuid, $tutor_tz, $student_tz, $logged_user_tz);
            $cb_get_hours = Book::get_final_hours($all_lesson_data, $session_date, $session_minute, filter_var(false, FILTER_VALIDATE_BOOLEAN), 'flexible', $tutor_tz, $student_tz, $get_hours);
            $option_html = '';
            // print_r($get_hours);exit;
            $cb_get_hours = array_unique($cb_get_hours);
            if (!empty($cb_get_hours)) {
                foreach ($cb_get_hours as $value) {
                    $display = date('h:i A', strtotime($value));
                    $result[$value] = $display;

                    $checked = (isset($step_data['step_4']['start_hour']) && $value == $step_data['step_4']['start_hour']) ? 'checked="checked"' : '';
                    $option_html .= '<div class="col-md-6 ">'
                            . '<ul class="products-list product-list-in-box" style="white-space: nowrap;">'
                            . '<li class="st-p3"><label class="contai_r" style="padding-left: 17px;">'
                            . '<input type="radio" onclick="cb_select_time(\'' . $value . '\');" value="' . $value . '" name="Book[start_hour]" ' . $checked . '>'
                            . '<span class="checkmark"></span>'
                            . '</label>'
                            . '<span class="mn_3">' . $display . '</span>'
                            . '</li>'
                            . '</ul>'
                            . '</div>';
                }
            } else {
                $option_html = '<div class="col-md-12 text-center">No time available for this date.</div>';
            }
        }

        Yii::$app->response->format = 'json';
        return ['result' => $result, 'html' => $option_html];
    }

    public function actionRefreshCart() {

        $token = "NEWBOOK_" . Yii::$app->user->identity->student_uuid;
        $step_data = StudentTuitionBooking::getSession($token);
        $student_detail = Student::findOne(Yii::$app->user->identity->student_uuid);
        $count = json_decode($step_data['step_4']['all_lesson_data'], true);
        $available = 0;
        $unavailable = 0;
        foreach ($count as $value) {
            if ($value['is_available'] == "YES") {
                $available++;
            } else {
                $unavailable++;
            }
        }


        $return = ['monthly_sub_total' => 0];

        $cart_total = 0;
        $discount = 0;
        /* if ($student_detail->isMonthlySubscription == 1) {
          $monthly_sub_check = 'no';
          } else {
          $monthly_sub_check = Yii::$app->getRequest()->get('monthly_sub_check');
          } */
        // $monthly_sub_uuid = Yii::$app->getRequest()->get('monthly_sub_uuid');
        $coupon_code = Yii::$app->getRequest()->get('coupon_code');

        if (!is_array($step_data['step_2']['tutor_further_details'])) {
            $step_data['step_2']['tutor_further_details'] = json_decode($step_data['step_2']['tutor_further_details'], true);
        }
        $package_type = $step_data['step_2']['tutor_further_details']['package_type'];


        if ($package_type != "STANDARD") {
            if ($step_data['step_3']['lesson_length'] == 30) {
                $return['plan_base_price'] = $step_data['step_2']['tutor_further_details']['price']['half'];
                $return['plan_gst'] = $step_data['step_2']['tutor_further_details']['gst']['half'];
                $return['plan_price'] = $step_data['step_2']['tutor_further_details']['half'] * $available;
                $return['plan_option'] = $step_data['step_2']['tutor_further_details']['option']['half'];
            } else {
                $return['plan_base_price'] = $step_data['step_2']['tutor_further_details']['price']['full'];
                $return['plan_gst'] = $step_data['step_2']['tutor_further_details']['gst']['full'];
                $return['plan_price'] = $step_data['step_2']['tutor_further_details']['full'] * $available;
                $return['plan_option'] = $step_data['step_2']['tutor_further_details']['option']['full'];
            }
        } else {

            if ($step_data['step_3']['lesson_length'] == 30) {
                $return['plan_base_price'] = $step_data['step_2']['tutor_further_details']['price']['half'];
                $return['plan_gst'] = $step_data['step_2']['tutor_further_details']['gst']['half'];
                $return['plan_price'] = $step_data['step_2']['tutor_further_details']['half'] * $available;
                $return['plan_option'] = $step_data['step_2']['tutor_further_details']['option']['half'];
            } else {
                $return['plan_base_price'] = $step_data['step_2']['tutor_further_details']['price']['full'];
                $return['plan_gst'] = $step_data['step_2']['tutor_further_details']['gst']['full'];
                $return['plan_price'] = $step_data['step_2']['tutor_further_details']['full'] * $available;
                $return['plan_option'] = $step_data['step_2']['tutor_further_details']['option']['full'];
            }


            //Get id of tutorwise paln 
//            $tutorwise_model = OnlineTutorialPackageTutorwise::find()->where(['premium_standard_plan_rate_uuid' => $step_data['step_3']['onlinetutorial_uuid'], 'tutor_uuid' => $step_data['step_2']['tutor_uuid']])->one();
//            $online_tutorial_package_tutorwise_uuid = $tutorwise_model->uuid;

            //Standard tutor loking period
//            $locking_per_model_count = StudentTuitionStandardBookingLockingPeriod::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid, 'tutor_uuid' => $step_data['step_2']['tutor_uuid']])->count();

            /* if ($locking_per_model_count > 0) {

              $locking_per_model = StudentTuitionStandardBookingLockingPeriod::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid, 'tutor_uuid' => $step_data['step_2']['tutor_uuid']])->one();
              $locking_end_datetime = $locking_per_model->locking_end_datetime;
              $locking_start_datetime = $locking_per_model->locking_start_datetime;
              $locking_option = $locking_per_model->option;


              $current_date = date('Y-m-d H:i:s');

              if (($return['plan_option'] < $locking_option) && ($current_date <= $locking_end_datetime)) {

              $lockingplanDetail = StandardPlanOptionRate::find()->where(['premium_standard_plan_rate_uuid' => $step_data['step_3']['onlinetutorial_uuid'], 'option' => $locking_option])->one();


              $return['plan_price'] = $lockingplanDetail->total_price;
              $return['plan_base_price'] = $lockingplanDetail->price;
              $return['plan_gst'] = $lockingplanDetail->gst;
              $return['plan_option'] = $lockingplanDetail->option;
              }
              } */
        }
        $model_coupon = StudentTuitionBooking::check_coupon_available($coupon_code, $return['plan_price']);


        /* if ($monthly_sub_check == 'yes') {
          //$monthly_model = MonthlySubscriptionFee::find()->where(['with_tutorial' => 1, 'status' => 'ENABLED'])->one();
          //change by pooja for comman price for manthly subscription
          $monthly_model = MonthlySubscriptionFee::find()->where(['status' => 'ENABLED'])->one();
          if (!empty($monthly_model)) {
          $return['monthly_sub_total'] = $monthly_model->total_price;
          }
          } */

        if ($model_coupon['is_available']) {
            $discount = \app\libraries\General::priceToMusicoins($model_coupon['discount']);
        }
        $model_coupon['discount'] = $discount;
       // $return['coupon'] = $model_coupon;

        //$cart_total = (($return['plan_price'] + $return['monthly_sub_total'] ) - $discount);
        $cart_total = (\app\libraries\General::priceToMusicoins($return['plan_price']) - $discount);
        $model_coupon['discount'] = ($cart_total>0)?$discount:\app\libraries\General::priceToMusicoins($return['plan_price']);
        $return['coupon'] =$model_coupon;
        $return['cart_total'] = ($cart_total>0)?$cart_total:0;

        $return = ['code' => 200, 'message' => 'success', 'result' => $return];

        Yii::$app->response->format = 'json';
        return $return;
    }
    
    /** Lesson Reschedule Request */
    public function actionRescheduleRequest($id) {
        if (Yii::$app->user->identity->role == 'STUDENT') {

            $this->layout = "student_layout";
            $view_file = 'student_reschedule_request';
        } else {
            $view_file = 'tutor_reschedule_request';
        }

        if (($modelBS = BookedSession::find()->joinWith(['studentUu', 'instrumentUu', 'tutorUu'])->where(['booked_session.uuid' => $id])->one()) == null) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }

        if ($modelBS->status != "SCHEDULE") {
            throw new \yii\web\HttpException(505, 'This lesson not scheduled. Only scheduled lesson are reschedule.');
        }

        if (date('Y-m-d H:i:s') >= date('Y-m-d H:i:s', strtotime($modelBS->start_datetime))) {
            throw new \yii\web\HttpException(505, 'This lesson time is started. So you can not reschedule it.');
        }

        $model = new Book();
        $model->scenario = 'lesson_reschedule';
        $model->tutor_uuid = $modelBS->tutor_uuid;
        $model->student_uuid = $modelBS->student_uuid;

        $model->attributes = Yii::$app->getRequest()->post('Book');

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $session_date_array = json_decode($model->all_lesson_data, true);
            
            if (!empty($session_date_array[0]['is_available']) && $session_date_array[0]['is_available'] == 'YES' && !empty($session_date_array[0]['lesson_time']) && !empty($session_date_array[0]['tutor_uuid']) ) {

                $lesson_time = $session_date_array[0]['lesson_time'];
                $tutor_uuid = $session_date_array[0]['tutor_uuid'];
                $lesson_length = $modelBS->session_min; //$session_date_array[0]['lesson_length'];

                $from_start_datetime = $modelBS->start_datetime;
                $from_end_datetime = $modelBS->end_datetime;
                $student_timezone = General::getStudentTimezone($modelBS->student_uuid);
                $tutor_timezone = General::getTutorTimezone($tutor_uuid);

                $to_start_datetime = date('Y-m-d H:i:s', strtotime($lesson_time));
                $to_end_datetime = date('Y-m-d H:i:s', strtotime($to_start_datetime . " +" . $lesson_length . " minutes"));

                $modelRS = new RescheduleSession;
                $modelRS->booked_session_uuid = $modelBS->uuid;
                $modelRS->student_uuid = $model->student_uuid;
                $modelRS->tutor_uuid = $tutor_uuid;
                $modelRS->from_start_datetime = $modelBS->start_datetime;
                $modelRS->from_end_datetime = $modelBS->end_datetime;
                $modelRS->to_start_datetime = General::convertUserToSystemTimezone($to_start_datetime);
                $modelRS->to_end_datetime = General::convertUserToSystemTimezone($to_end_datetime);
                $modelRS->request_by = 'STUDENT';
                $modelRS->requester_uuid = $model->student_uuid;
                $modelRS->status = 'AWAITING'; // APPROVED / REJECTED / AWAITING
                $modelRS->referance_number = General::generateRescheduleID();
                if ($modelRS->save(false)) {
                    $modelBSUpdate = BookedSession::findOne($modelBS->uuid);
                    if (!empty($modelBSUpdate)) {
                        $modelBSUpdate->is_reschedule_request = 1;
                        $modelBSUpdate->save(false);
                    }

                    //send mail
                    $content['student_name'] = $modelBS->studentUu->first_name;
                    $content['student_last_name'] = $modelBS->studentUu->last_name;
                    $content['tutor_name'] = $modelBS->tutorUu->first_name;
                    $content['tutor_last_name'] = $modelBS->tutorUu->last_name;
                    $content['reschedule_number'] = $modelRS->referance_number;
                    $content['instrument_name'] = $modelBS->instrumentUu->name;
                    $content['link'] = Url::to(['/book/reschedule', 'id' => $modelBS->uuid], true);
                    $content['from_start_datetime'] = $from_start_datetime;
                    $content['from_end_datetime'] = $from_end_datetime;
                    $content['to_start_datetime'] = $modelRS->to_start_datetime;
                    $content['to_end_datetime'] = $modelRS->to_end_datetime;
                    $content['student_timezone'] = $student_timezone;
                    $content['tutor_timezone'] = $tutor_timezone;
                    //$ownersEmail = General::getOwnersEmails(['OWNER','ADMIN']);
                    Yii::$app->mailer->compose('reschedule_lesson_request_student_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                            ->setFrom(Yii::$app->params['supportEmail'])
                            ->setTo($modelBS->studentUu->email)
                            ->setSubject(ucfirst(strtolower(Yii::$app->name)) . ' Notification: Awaiting Approval – Student Lesson Change Request #' . $modelRS->referance_number)
                            ->send();

                    Yii::$app->mailer->compose('reschedule_lesson_request_tutor_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                            ->setFrom(Yii::$app->params['supportEmail'])
                            ->setTo($modelBS->tutorUu->email)
                            ->setSubject('Awaiting Your Approval – Student booking change #' . $modelRS->referance_number)
                            ->send();

                    Yii::$app->mailer->compose('reschedule_lesson_request_admin_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                            ->setFrom(Yii::$app->params['supportEmail'])
                            ->setTo(Yii::$app->params['adminEmail'])
                            ->setSubject('Awaiting Approval – Student booking change #' . $modelRS->referance_number)
                            ->send();

                    Yii::$app->getSession()->setFlash('success', 'You have successfully sent rescheduled lesson request.');
                    return $this->redirect(['/booked-session/upcoming']);
                } else {
                    Yii::$app->getSession()->setFlash('danger', 'Reschedule lesson not successfully saved.');
                }
            } else {
                Yii::$app->getSession()->setFlash('danger', 'Reschedule lesson not successfully saved.');
            }
        }

        return $this->render($view_file, [
                    'modelBS' => $modelBS,
                    'model' => $model,
        ]);
    }

    public function actionGetTimeslotsReschedule() {

        if (!Yii::$app->request->isAjax) {
            throw new \yii\web\HttpException(505, 'You are not authorised to access this page');
        }
        
        $result = [];
        
        $bs_uuid = Yii::$app->getRequest()->get('bs_uuid');
        
        if (($modelBS = BookedSession::find()->joinWith(['studentUu', 'instrumentUu', 'tutorUu'])->where(['booked_session.uuid' => $bs_uuid])->one()) == null) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
        
        $student_uuid = $modelBS->student_uuid;
        $tutor_uuid = $modelBS->tutor_uuid;
        if (!empty($tutor_uuid)) {            
            $tutorDetail = Tutor::findOne($tutor_uuid);
            $session_date = Yii::$app->getRequest()->get('selected_when_date');
            $session_minute = $modelBS->session_min;

            $tutor_working_plan = json_decode($tutorDetail->working_plan, true);
            $tutor_tz = General::getTutorTimezone($tutor_uuid);
            $student_tz = General::getStudentTimezone($student_uuid);
            $logged_user_tz = Yii::$app->user->identity->timezone;
            $get_hours = Book::get_available_hours($tutor_uuid, $tutor_working_plan, $session_date, $session_minute, $student_uuid, $tutor_tz, $student_tz, $logged_user_tz);
            $get_hours = array_unique($get_hours);
            $option_html = (Yii::$app->user->identity->role == 'STUDENT')?'':'<ul class="list-unstyled row">';

            if (!empty($get_hours)) {
                foreach ($get_hours as $key => $value) {
                    $display = date('h:i A', strtotime($value));
                    $result[$value] = $display;

                    $checked = '';
                    if(Yii::$app->user->identity->role == 'STUDENT'){
                    $option_html .= '<div class="col-md-6 col-sm-3">'
                            . '<ul class="products-list product-list-in-box" style="white-space: nowrap;">'
                            . '<li class="st-p3"><label class="contai_r" style="padding-left: 10px;">'
                            . '<input type="radio" onclick="select_time(\'' . $value . '\');" value="' . $value . '" name="Book[start_hour]" ' . $checked . '>'
                            . '<span class="checkmark"></span>'
                            . '</label>'
                            . '<span class="mn_3">' . $display . '</span>'
                            . '</li>'
                            . '</ul>'
                            . '</div>';
                    }else{
                        $option_html .= '<li class="col-md-6 col-sm-3">';
                        $option_html .= '<input tabindex="5" type="radio" id="square-radio-'.$key.'" class="skin-square-green" onclick="select_time(\'' . $value . '\');" value="' . $value . '" name="Book[start_hour]" ' . $checked . '>';
                        $option_html .= '<label class="iradio-label form-label" for="square-radio-'.$key.'">' . $display . '</label>';
                        $option_html .= '</li>';
                    }
                }
                $option_html .= (Yii::$app->user->identity->role == 'STUDENT')?'':'</ul>';
            } else {
                $option_html = '<div class="col-md-12 text-center">No time available for this date.</div>';
            }
        }

        Yii::$app->response->format = 'json';
        return ['result' => $result, 'html' => $option_html];
    }
    
    /**
     * selected time for reschedule
     */
    public function actionSelectTimeReschedule() {

        if (!Yii::$app->request->isAjax) {
            throw new \yii\web\HttpException(505, 'You are not authorised to access this page');
        }
        
        $bs_uuid = Yii::$app->getRequest()->post('bs_uuid');
        
        if (($modelBS = BookedSession::find()->joinWith(['studentUu', 'instrumentUu', 'tutorUu'])->where(['booked_session.uuid' => $bs_uuid])->one()) == null) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
        
        $student_uuid = $modelBS->student_uuid;
        $tutor_uuid = $modelBS->tutor_uuid;
        $selected_time = Yii::$app->getRequest()->post('selected_time');
        
        $result = '';
        $html = '<div><strong>Selected:</strong></div>';

        if (!empty($tutor_uuid) && !empty($selected_time)) {
            
            $lesson_minute = $modelBS->session_min;
            $bitween_days = 7;
            $lesson_type_label = 'Weeks';
            $lesson_total = 1;
            $result = Book::get_lesson_for_booking($selected_time, $student_uuid, $tutor_uuid, $lesson_minute, $lesson_total, $bitween_days);

            $result = (!empty($result)) ? $result : '';
            if (!empty($result)) {

                if ($lesson_total > 1) {
                    $html .= '<div>Every ' . date('l, h:iA', strtotime($result[0]['lesson_time'])) . ' - ' . date('h:iA', strtotime($result[0]['lesson_time'] . ' +' . $lesson_minute . ' minute')) . '</div><br>';
                    $html .= '<div><strong>Recurring:</strong> ' . $lesson_total . ' ' . $lesson_type_label . '</div>';
                }
                foreach ($result as $value) {
                    $btn = $text_class = '';
                    if (isset($value['is_available']) && $value['is_available'] == 'NO') {
                        $btn = '<button type="button" class="label label-warning rebook_btn" data-time="' . $value['lesson_time'] . '" data-student="' . $student_uuid . '">CHANGE</button>';
                        $text_class = 'text-red';
                    }
                    $display_date = date('d M Y - h:i A', strtotime($value['lesson_time']));
                    $html .= '<div class="' . $text_class . '">' . $display_date . $btn . '</div>';
                }
            }
        }

        Yii::$app->response->format = 'json';
        return ['all_lesson_data' => $result, 'html' => $html];
    }
    
    /** RESCHEDULE REQUEST > TUTOR */
    public function actionReschedule($id) {

        if (($modelBS = BookedSession::find()->joinWith(['studentUu', 'instrumentUu', 'tutorUu'])->where(['booked_session.uuid' => $id])->one()) == null) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }

        if ($modelBS->status != "SCHEDULE") {
            throw new \yii\web\HttpException(505, 'This lesson not scheduled. Only scheduled lesson are reschedule.');
        }

        $model = new Book();
        $model->scenario = 'lesson_reschedule';
        $model->tutor_uuid = $modelBS->tutor_uuid;
        $model->student_uuid = $modelBS->student_uuid;

        $model->attributes = Yii::$app->getRequest()->post('Book');

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $session_date_array = json_decode($model->all_lesson_data, true);
            if (!empty($session_date_array[0]['is_available']) && $session_date_array[0]['is_available'] == 'YES' && !empty($session_date_array[0]['lesson_time']) && !empty($session_date_array[0]['tutor_uuid'])) {

                $lesson_time = $session_date_array[0]['lesson_time'];
                $tutor_uuid = $session_date_array[0]['tutor_uuid'];
                $lesson_length = $modelBS->session_min; //$session_date_array[0]['lesson_length'];

                $from_start_datetime = $modelBS->start_datetime;
                $from_end_datetime = $modelBS->end_datetime;
                $student_timezone = General::getStudentTimezone($modelBS->student_uuid);
                $tutor_timezone = General::getTutorTimezone($tutor_uuid);

                $to_start_datetime = date('Y-m-d H:i:s', strtotime($lesson_time));
                $to_end_datetime = date('Y-m-d H:i:s', strtotime($to_start_datetime . " +" . $lesson_length . " minutes"));

                $modelRS = new RescheduleSession;
                $modelRS->booked_session_uuid = $modelBS->uuid;
                $modelRS->student_uuid = $model->student_uuid;
                $modelRS->tutor_uuid = $tutor_uuid;
                $modelRS->from_start_datetime = $modelBS->start_datetime;
                $modelRS->from_end_datetime = $modelBS->end_datetime;
                $modelRS->to_start_datetime = General::convertUserToSystemTimezone($to_start_datetime);
                $modelRS->to_end_datetime = General::convertUserToSystemTimezone($to_end_datetime);
                $modelRS->request_by = 'TUTOR';
                $modelRS->requester_uuid = $model->tutor_uuid;
                $modelRS->status = 'AWAITING'; // APPROVED / REJECTED / AWAITING
                $modelRS->referance_number = General::generateRescheduleID();
                if ($modelRS->save(false)) {
                    $modelUpdateBS = BookedSession::findOne($modelBS->uuid);
                    if (!empty($modelUpdateBS)) {
                        // $modelUpdateBS->start_datetime = $modelRS->to_start_datetime;
                        // $modelUpdateBS->end_datetime = $modelRS->to_end_datetime;
                        // $modelUpdateBS->save(false);
                        $modelUpdateBS->is_reschedule_request = 2;
                        $modelUpdateBS->save(false);

                        //send mail to student lesson reschedule by tutor
                        $content['student_name'] = $modelBS->studentUu->first_name;
                        $content['student_last_name'] = $modelBS->studentUu->last_name;
                        $content['tutor_name'] = $modelBS->tutorUu->first_name;
                        $content['tutor_last_name'] = $modelBS->tutorUu->last_name;
                        $content['instrument_name'] = $modelBS->instrumentUu->name;
                        $content['reschedule_number'] = $modelRS->referance_number;
                        $content['from_start_datetime'] = $from_start_datetime;
                        $content['from_end_datetime'] = $from_end_datetime;
                        $content['to_start_datetime'] = $modelRS->to_start_datetime;
                        $content['to_end_datetime'] = $modelRS->to_end_datetime;
                        $content['student_timezone'] = $student_timezone;
                        $content['tutor_timezone'] = $tutor_timezone;

                        $ownersEmail = General::getOwnersEmails(['OWNER', 'ADMIN']);
                        Yii::$app->mailer->compose('tutor_reschedule_lesson_student_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                                ->setFrom(Yii::$app->params['supportEmail'])
                                ->setTo($modelBS->studentUu->email)
                                ->setSubject(ucfirst(strtolower(Yii::$app->name)) . ' Notification - Tutor ' . $content['tutor_name'] . ' ' . $content['tutor_last_name'] . ' has made changes to a booked lesson with you')
                                ->send();

                        Yii::$app->mailer->compose('tutor_reschedule_lesson_tutor_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                                ->setFrom(Yii::$app->params['supportEmail'])
                                ->setTo($modelBS->tutorUu->email)
                                ->setSubject(ucfirst(strtolower(Yii::$app->name)) . ' Notification - Changes to booked lesson with ' . $content['student_name'] . ' ' . $content['student_last_name'] . ' are now confirmed in the booking system')
                                ->send();

                        Yii::$app->mailer->compose('tutor_reschedule_lesson_admin_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                                ->setFrom(Yii::$app->params['supportEmail'])
                                ->setTo($ownersEmail)
                                ->setSubject(ucfirst(strtolower(Yii::$app->name)) . ' Notification - Tutor ' . $content['tutor_name'] . ' ' . $content['tutor_last_name'] . '  has initiated changes to a booked lesson with ' . $content['student_name'] . ' ' . $content['student_last_name'] . ' : #' . $modelRS->referance_number)
                                ->send();


                        Yii::$app->getSession()->setFlash('success', 'You have successfully rescheduled lesson.');
                        return $this->redirect(['/booked-session/upcoming']);
                    } else {
                        Yii::$app->getSession()->setFlash('danger', 'Reschedule lesson not successfully saved.');
                    }
                } else {
                    Yii::$app->getSession()->setFlash('danger', 'Reschedule lesson not successfully saved.');
                }
            } else {
                Yii::$app->getSession()->setFlash('danger', 'Reschedule lesson not successfully saved.');
            }
        }
        return $this->render('tutor_reschedule_request', [
                    'modelBS' => $modelBS,
                    'model' => $model,
        ]);
    }
    public function actionCancelTutorRescheduleRequest($id) {
        if (!Yii::$app->request->isAjax) {
            throw new \yii\web\HttpException(505, 'You are not authorised to access this page');
        }
        
        $tutor_cancel_request = Yii::$app->getRequest()->post('tutor_cancel_request');
        if ($tutor_cancel_request == "true") {
            $role = Yii::$app->user->identity->role;
            
            $modelRS = RescheduleSession::find()->where(['booked_session_uuid' => $id])->andWhere(['status' => 'AWAITING'])->one();
            $modelRS->request_cancel_by = Yii::$app->user->identity->role;
            $modelRS->request_canceler_uuid = Yii::$app->user->identity->tutor_uuid;
            $modelRS->request_cancel_datetime = Yii::$app->formatter->asDate(time(), 'php:Y-m-d H:i:s');
            $modelRS->status = "CANCELLED";
            if(!$modelRS->save(false)) {
                $return = ['code' => 501, 'message' => 'Failed to cancel your reschedule request.'];
            } else {
                $modelBS = BookedSession::find()->where(['booked_session.uuid' => $modelRS->booked_session_uuid])->one();
                $modelBS->is_reschedule_request = 0;
                $modelBS->save(false);
                $return = ['code' => 200, 'message' => 'Your request to change lesson schedule has been canceled successfuly.'];
            }
            Yii::$app->response->format = 'json';
            return $return;

            
        } else {
            $return = ['code' => 200, 'message' => 'No changes made.'];
            Yii::$app->response->format = 'json';
            return $return;
        }

    }

}
