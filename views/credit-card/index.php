<?php 
//print_r($creditCardList); exit;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;

if(Yii::$app->session['monthlyPlan_uuid']){
    
$this->title = Yii::t('app', 'Credit Cards');
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs(
        '
            
            //delete card 
            $(document).on("click",".delete_credit_card", function(){ 
                if(confirm("Are you sure, you want to delete this card ?")){
                var element = $(this); 
                    $.ajax({
                        type: "POST",
                        url: "'.Url::to(['/credit-card/delete/']).'",
                        data: { "id": $(this).data("uuid")},
                        success:function(result){ console.log(result);
                            if(result.code == 200){

                                $(element).closest("div.card").remove();
                                $(element).addClass("not-found");
                                showSuccess(result.message);
                            }else{
                                showErrorMessage(result.message);
                            }
                        },
                        error:function(e){ 
                            showErrorMessage(e.responseText);
                        }
                    });
                }
            }); 
            
            //card number validation
//            $(function() { 
//                $("#studentcreditcard-number").on("input", function() { 
//                  match = (/(\d{0,16})[^]*((?:\.\d{0,0})?)/g).exec(this.value.replace(/[^\d.]/g, ""));
//                  this.value = match[1] + match[2];
//                });
//            });
            
            //cvv number validation
//            $(function() {
//                $("#studentcreditcard-cvv").on("input", function() {
//                  match = (/(\d{0,4})[^]*((?:\.\d{0,0})?)/g).exec(this.value.replace(/[^\d.]/g, ""));
//                  this.value = match[1] + match[2];
//                });
//            });

              //card number formatting validation
//            $(function() {
//                $("#studentcreditcard-number").on("keypress change", function() { 
//                    $(this).val(function (index, value) { 
//                        return value.replace(/\W/gi, "").replace(/(.{4})/g, "$1 ");
//                      });
//                });
//            });
            

            $(document).on("click",".add-credit-card", function(){ 
                $(".credit-card-form").toggle();
                
            });
            
            $(document).on("click",".add-credit-card", function(){ 
                $(".not-found").hide();
            });
            
            //card number validation
            $(document).on("input","#studentcreditcard-number", function(){
                    match = (/(\d{0,16})[^]*((?:\.\d{0,0})?)/g).exec(this.value.replace(/[^\d.]/g, ""));
                    this.value = match[1] + match[2];
            });
            
            //card cvv validation
            $(document).on("input","#studentcreditcard-cvv", function(){
                    match = (/(\d{0,4})[^]*((?:\.\d{0,0})?)/g).exec(this.value.replace(/[^\d.]/g, ""));
                    this.value = match[1] + match[2];
            });
            
            //card number formatting validation
//            $(document).on("keypress change","#studentcreditcard-number", function(){
//                  $(this).val(function (index, value) { 
//                        return value.replace(/\W/gi, "").replace(/(.{4})/g, "$1 ");
//                      });  
//            });
            
             
',View::POS_END
);


?>

<div class="col-xl-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Please select a credit card</h2>
            <div class="actions panel_actions float-right">
                <div class="button"><div class="btn btn-primary add-credit-card">Add New Credit Card</div></div>
            </div>
        </header>
        <div class="content-body"> 
            <div class="row" id="credit_card_table_parent_div">
                <?php echo Yii::$app->controller->renderPartial('_credit_card_list', ['creditCardList' => $creditCardList, 'model' => $model]); ?>
            </div>
        </div>
    </section> 
</div>
<?php } ?>
