<?php

use yii\helpers\Html;

?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['tutor_name']; ?>,</p>
            
            <p>You have got a cancel lesson request. Please approve or reject.</p>
            
            <p>The details of your lesson change request are as follows:</p>
            
            <p><strong>Student Name :</strong> <?= $content['student_name']; ?></p>
            <p><strong>Lesson Date :</strong> <?= $content['from_date']; ?></p>
            <p><strong>Lesson Time :</strong> <?= $content['from_time']; ?></p>
            
            <p>Regards,<br>
            Admin Team – Musiconn Pty Ltd</p>
        </td>
    </tr>
</table>

