<?php

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use app\models\StudentMonthlySubscription;
use app\models\StudentSubscription;
use app\models\Student;
use app\models\StudentCreditCard;
use app\models\MonthlySubscriptionFee;
use app\models\StudentTuitionBooking;
use app\models\PaymentTransaction;
use app\models\User;
use app\libraries\General;
use Yii;

class BuyMonthlySubscriptionController extends Controller
{

    public function actionIndex($message = 'BuyMonthlySubscription cron Index')
    {
        echo $message . "\n";

        return ExitCode::OK;
    }

    public function actionNotification() {

        $currDate = date("Y-m-d H:i:s");
        $renew_date_after = date('Y-m-d', strtotime('+2 days', strtotime($currDate)));
        $studentsublist = StudentSubscription::find()->where(['next_renewal_date' => $renew_date_after])->all();
        if(!empty($studentsublist)){
          foreach($studentsublist as $key => $value){

		    $student = User::find()->joinWith('student')->where(['user.student_uuid' => $value->student_uuid, 'student.status'=>'ENABLED'])->one();

        $monthlySubscription = StudentMonthlySubscription::find()->where(['uuid' => $value->subscription_uuid])->one();

		    if(!empty($student) && !empty($monthlySubscription)){

		        $studentsubDetails = array(
		            'student_name' => $student->first_name,
		            'email' => $student->email,
		            'name' => $monthlySubscription->name,
		            'price' => $monthlySubscription->price,
		            'tax' => $monthlySubscription->tax,
		            'total_price' => $monthlySubscription->total_price,
                'timezone' => $student->timezone,
		            'end_datetime' => General::convertTimezone($monthlySubscription->end_datetime, Yii::$app->params['timezone'], $student->timezone,'Y-m-d H:i:s')
		        );
		        //php /var/www/html/musiconn/yii buy-monthly-subscription/notification
		        StudentMonthlySubscription::ExpireSubscriptionNotification($studentsubDetails);
		    }
		}
	}

    }

    /* Every day night 12:01 AM crone run for Expire Subscription and if cc saved then  auto renewal subscription */
    public function actionExpireStudentSubscription() {

        $currDate = date("Y-m-d");
        $studentsublist = StudentSubscription::find()->where(['next_renewal_date' => $currDate])->all();
        //echo "<pre>"; print_r($studentsublist);exit;

        if(!empty($studentsublist)){

            foreach($studentsublist as $key => $value){

                $student = Student::find()->where(['student_uuid' => $value->student_uuid, 'status' => 'ENABLED'])->one();
                $monthlySubscription = StudentMonthlySubscription::find()->where(['uuid' => $value->subscription_uuid, 'status' => 'ENABLED'])->one();

                if(!empty($student) && !empty($monthlySubscription) && $student->isMonthlySubscription == 1){

                    $studentsubDetails = array(
                        'student_name' => $student->first_name.' '.$student->last_name,
                        'email' => $student->email,
                        'name' => $monthlySubscription->name,
                        'price' => $monthlySubscription->price,
                        'tax' => $monthlySubscription->tax,
                        'total_price' => $monthlySubscription->total_price,
                    );

                    $student->isMonthlySubscription = 0;
                    $student->updated_at = time();
                    $student->save(false);

                    $monthlySubscription->status = 'EXPIRED';
                    $monthlySubscription->updated_at = date("Y-m-d H:i:s");
                    $monthlySubscription->save(false);
                    //Expire subscription end

                    //check auto renew enable or not
                    $studentCC = StudentCreditCard::find()->where(['student_uuid' => $value->student_uuid,'status' => 'ENABLED'])->one();
                    if(!empty($studentCC)){

                        //Renewal process
                        $renew_result = self::stripPayment($value->student_uuid,$studentCC);
                        echo json_encode($renew_result);
                    } else {
                        // Send Expire mail
                        StudentSubscription::ExpireStudentSubscriptionMail($studentsubDetails);
                    }
                }
            }
        }

    }


    /******  Auto Renewal payment process *********/
    public function stripPayment($student_uuid,$studentCC) {

        $return = ['code' => 421, 'message' => 'Something went wrong.'];
        $studentMonthlySub = [];
        $student = Student::find()->where(['student_uuid' => $student_uuid])->one();
        $studentSubDetail = StudentSubscription::find()->where(['student_uuid' => $student_uuid])->one();
        if(!empty($studentSubDetail)){
            $studentMonthlySub = StudentMonthlySubscription::find()->where(['uuid' => $studentSubDetail->subscription_uuid])->one();
        }

        if(!empty($student) && !empty($studentCC->stripe_customer_id) && !empty($studentMonthlySub) ){

        		$subscriptionFeeList = StudentMonthlySubscription::getsubscriptionFeeList($student_uuid);

            try {

                //$dollar_amount = (int) $subscriptionFeeList->total_price;
                $dollar_amount = (float) $subscriptionFeeList->total_price;
                $cents_amount = $dollar_amount * 100;       // 1 AUD = 100 cents
                $strip_customer_id = $studentCC->stripe_customer_id;
                //$strip_customer_id = "cus_DyJmohDRidHOrk";
                $orderID = General::generateTransactionID();

                \Stripe\Stripe::setApiKey(Yii::$app->params['stripe']['secret_key']);

                //charge a credit or a debit card
                $charge = \Stripe\Charge::create([
                  "customer" => $strip_customer_id,
                  "amount" => $cents_amount,
                  "currency" => Yii::$app->params['stripe']['currency'],
                  "description" => $studentMonthlySub->name,
                  "metadata" => [
                      'order_id' => $orderID
                        ]
                ]);

            } catch (\Stripe\Error\ApiConnection $e) {
                return $return = ['code' => 421, 'message' => 'Network problem, perhaps try again.'];
            } catch (\Stripe\Error\InvalidRequest $e) {
                return $return = ['code' => 421, 'message' => 'Your request is invalid.'];
            } catch (\Stripe\Error\Api $e) {
                return $return = ['code' => 421, 'message' => 'Stripe servers are down!'];
            } catch (\Stripe\Error\Card $e) {
                return $return = ['code' => 421, 'message' => 'This Card is Declined.'];
            }

            $chargeJson = $charge->jsonSerialize();

            if ($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1) {

                //Payment Transaction
                $modelPT = new PaymentTransaction();
                $modelPT->student_uuid = $student_uuid;
                $modelPT->transaction_number = $orderID;
                $modelPT->payment_datetime = date("Y-m-d H:i:s");
                $modelPT->amount = $chargeJson['amount'] / 100;
                $modelPT->payment_method = 'Stripe';
                $modelPT->reference_number = $chargeJson['balance_transaction'];
                $modelPT->status = $chargeJson['status'];
                $modelPT->subscription_uuid = NULL;
                $modelPT->response = json_encode($chargeJson);
                $modelPT->save(false);

                //student_subscription table update
                $modelStudentSubscription = StudentSubscription::find()->where(['student_uuid' => $student_uuid])->one();
                if(empty($modelStudentSubscription)){
                    $modelStudentSubscription = new StudentSubscription;
                    $modelStudentSubscription->student_uuid = $student_uuid;
                }

                $start_date = date('Y-m-d H:i:s');
                if($student->isMonthlySubscription == 1 ){
                    if(!empty($modelStudentSubscription)){
                        $start_date = date('Y-m-d H:i:s', strtotime($modelStudentSubscription->next_renewal_date));
                    }
                }
                $end_date = date('Y-m-d H:i:s', strtotime($start_date . " +30 days"));
                $next_renewal_date = date('Y-m-d', strtotime($end_date . " +1 days"));

                $modelMS = new StudentMonthlySubscription();
                $modelMS->student_uuid = $student_uuid;
                $modelMS->name = $subscriptionFeeList->name;
                $modelMS->price = $subscriptionFeeList->price;
                $modelMS->tax = $subscriptionFeeList->tax;
                $modelMS->total_price = $subscriptionFeeList->total_price;
                $modelMS->description = $subscriptionFeeList->description;
                $modelMS->with_tutorial = $subscriptionFeeList->with_tutorial;
                $modelMS->status = 'ENABLED';
                $modelMS->start_datetime = $start_date;
                $modelMS->end_datetime = $end_date;
                $modelMS->created_at = $modelMS->updated_at = date("Y-m-d H:i:s");
                if ($modelMS->save(false)) {

                    //update Transaction Subscription id
                    $modelPaymentUpdate = PaymentTransaction::find()->where(['uuid' => $modelPT->getPrimaryKey()])->one();
                    $modelPaymentUpdate->subscription_uuid = $modelMS->getPrimaryKey();
                    $modelPaymentUpdate->save(false);

                    $save_m_s_flag = true;

                    //student_subscription table update
                    $modelStudentSubscription->subscription_uuid = $modelMS->getPrimaryKey();
                    $modelStudentSubscription->next_renewal_date = $next_renewal_date;
                    $modelStudentSubscription->save(false);

                    //update student monthly sub flag
                    $student->isMonthlySubscription = 1;
                    $student->isCancelMonthlySubscriptionRequest = 0;
                    $student->save(false);

                    $item_array[0] = ['item' => $modelMS->name, 'booking_uuid' => NULL, 'student_monthly_subscription_uuid' => $modelMS->getPrimaryKey(), 'tax' => $modelMS->tax, 'price' => $modelMS->price, 'total' => $modelMS->total_price];

                    $order_array['student_uuid'] = $modelMS->student_uuid;
                    $order_array['payment_uuid'] = $modelPT->getPrimaryKey();
                    $order_array['discount'] = 0;
                    $order_array['item'] = $item_array;
                    $order_array['total'] = $modelMS->total_price;
                    $order_array['grand_total'] = $modelMS->total_price;

                    $order_result = StudentMonthlySubscription::generate_order($order_array);

                    // Send renewal email to student
                    $content['student_name'] = $student->first_name;
                    $content['order_id'] = (isset($order_result['order_id'])) ? "#".$order_result['order_id'] : "";
                    $isInvoiceFile = (isset($order_result['order_file'])) ? $order_result['order_file'] : "";

                    $content_admin['recurring_payment'] = 'Yes';
                    $content_admin['student_name'] = $student->first_name;
                    $content_admin['student_last_name'] = $student->last_name;
                    $content_admin['student_id'] = "#".$student->enrollment_id;
                    $content_admin['order_id'] = (isset($order_result['order_id'])) ? "#".$order_result['order_id'] : "";
                    $content_admin['order_date'] = (isset($order_result['order_date'])) ? $order_result['order_date'] : "";
                    $content_admin['title'] = $subscriptionFeeList->name;
                    $content_admin['price'] = $subscriptionFeeList->price;
                    $content_admin['tax'] = $subscriptionFeeList->tax;
                    $content_admin['total_price'] = $subscriptionFeeList->total_price;
                    $content_admin['transaction_id'] = $orderID;

                    $ownersEmail = General::getOwnersEmails(['OWNER', 'ADMIN']);

                    $mail = Yii::$app->mailer->compose('monthly_subscription_renew', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                        ->setFrom(Yii::$app->params['supportEmail'])
                        ->setTo($student->email)
                        ->setSubject('Confirmation of Musiconn Online Subscription Renewal');
                        if (is_file($isInvoiceFile)) {
                            $mail->attach($isInvoiceFile);
                        }
                        $mail->send();

                    $mail_1 = Yii::$app->mailer->compose('monthly_subscription_buy_admin', ['content' => $content_admin], ['htmlLayout' => 'layouts/html'])
                        ->setFrom(Yii::$app->params['supportEmail'])
                        ->setTo($ownersEmail)
                        ->setSubject('Online Content Subscription Purchased: Transaction ID '.$content_admin['order_id'].', Client ID '.$content_admin['student_id']);
                        if (is_file($isInvoiceFile)) {
                            $mail_1->attach($isInvoiceFile);
                        }
                        $mail_1->send();

                    return $return = ['code' => 200, 'message' => 'success'];
                }


            } else {
                return $return = ['code' => 421, 'message' => 'Transaction has been failed.'];
            }
        }
        return $return;
    }
}
