<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'musiconn',
    'name' => 'MUSICONN',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'timeZone' => $params['timezone'],
    'components' => [
    
        'authManager' => [
            'class'        => 'yii\rbac\DbManager',
         ],
        'as access' => [
              'class' => 'mdm\admin\components\AccessControl',
              'allowActions' => [
                  'site/*',
                  'admin/*',
                  // The actions listed here will be allowed to everyone including guests.
                  // So, 'admin/*' should not appear here in the production, of course.
                  // But in the earlier stages of your development, you may probably want to
                  // add a lot of actions here until you finally completed setting up rbac,
                  // otherwise you may not even take a first step.
              ]
          ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'Ffo_IoqoojMZzu3H8kMVFONCPelD9y66',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'mdm\admin\models\User',
            'loginUrl'=>array('site/login'),
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction'=> 'site/error',
        ],
        'mailer' => [            
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@app/mail',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'username',
                'password' => 'password',
                'port' => '587',
                'encryption' => 'tls',
            ]
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => true,
            'rules' => [
            ],
        ],
        
        'assetManager' => [
	    'bundles' => [
		'yii\web\JqueryAsset' => [
		    'js'=>[]
		],
		'yii\bootstrap\BootstrapPluginAsset' => [
		    'js'=>[]
		],
		'yii\bootstrap\BootstrapAsset' => [
		    'css' => [],
		],

	    ],
	],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                    [
                    'class' => 'notamedia\sentry\SentryTarget',
                    'dsn' => 'SentryIO_ACCOUNT_DSN',//'http://2682ybvhbs347:235vvgy465346@sentry.io/1',
                    //'levels' => ['error', 'warning',],
                    'levels' => ['error', ],
                    // Write the context information (the default is true):
                    'context' => true,
                    // Additional options for `Sentry\init`:
                    'clientOptions' => ['release' => 'my-project-name@2.3.12']
                ],
            ],
        ],
    ],
    'modules' => [
        'admin' => [
            'class' => 'mdm\admin\Module',
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1','192.168.1.203','192.168.1.201','192.168.1.204'],
    ];
}

return $config;
