<?php

namespace app\controllers;

use Yii;
use app\models\MusicoinLog;
use app\models\MusicoinCoupon;
use app\models\MusicoinPackage;
use app\models\MusicoinLogSearch;
use app\models\StudentMusicoinPackage;
use app\models\StudentTuitionBooking;
use app\models\StudentCreditCard;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * MusicoinController implements the CRUD actions for MusicoinLog model.
 */
class MusicoinController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                        [
                        'actions' => ['index'],
                        'allow' => Yii::$app->user->can('/musicoin/index'),
                        'roles' => ['@'],
                    ],
			[
                        'actions' => ['credit-debit'],
                        'allow' => Yii::$app->user->can('/musicoin/credit-debit'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['history'],
                        'allow' => Yii::$app->user->can('/musicoin/history'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['buy','refresh-cart','remove-card'],
                        'allow' => true, //Yii::$app->user->can('/musicoin/buy'),
                        'roles' => ['STUDENT'],
                    ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MusicoinLog models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MusicoinLogSearch();
        
        $now = date('d-m-Y');
        $searchModel->created_at = date('d-m-Y', strtotime($now." -30 days")). " - ". $now;
        $recordList = $searchModel->search(Yii::$app->request->queryParams, true);
        
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_recorded_list', [
                        'recordList' => $recordList,
                        'searchModel' => $searchModel,
            ]);
        } else {
            return $this->render('index', [
                        'recordList' => $recordList,
                        'searchModel' => $searchModel,
            ]);
        }
    }
    
    /**
     * Lists all MusicoinLog models.
     * @return mixed
     */
    public function actionHistory()
    {
        $this->layout = "student_layout";
        $searchModel = new MusicoinLogSearch();
        $searchModel->student_uuid = Yii::$app->user->identity->student_uuid;
        
        $now = date('d-m-Y');
        $searchModel->created_at = date('d-m-Y', strtotime($now." -30 days")). " - ". $now;
        $recordList = $searchModel->search(Yii::$app->request->queryParams, true);
        
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_history_list', [
                        'recordList' => $recordList,
                        'searchModel' => $searchModel,
            ]);
        } else {
            return $this->render('history', [
                        'recordList' => $recordList,
                        'searchModel' => $searchModel,
            ]);
        }
    }

    /**
     * Creates a new MusicoinLog model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreditDebit()
    {
        $model = new MusicoinLog();
        $model->scenario = 'credit_debit';
        $model->coins = '0';

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if($model->type == 'CREDIT'){
                $array = [
                    'student_uuid' => $model->student_uuid,
                    'coins' => $model->coins,
                    'summary' => $model->summary,
                ];
                $result = MusicoinLog::creditMusicoin($array);
            }else{
                $array = [
                    'student_uuid' => $model->student_uuid,
                    'coins' => $model->coins,
                    'summary' => $model->summary,
                ];
                $result = MusicoinLog::debitMusicoin($array);
            }
            
            if(isset($result['code']) && $result['code'] == 200){
                $return = ['code' => 200, 'message' => 'Musicoin '. strtolower($model->type).'ed successfully.'];
            } elseif(isset($result['code']) && $result['code'] != 200){
                $return = ['code' => 421, 'message' => $result['message'] ];
            } else {
                $return = ['code' => 421, 'message' => 'Sorry! something wrong into saved data.' ];
            }
            
            Yii::$app->response->format = 'json';
            return $return;
        }

        return $this->renderAjax('_credit_debit_form', [
            'model' => $model,
        ]);
    }
    
    /**
     * Buy a new Musicoin Package.
     * @return mixed
     */
    public function actionBuy()
    {
        $model = new StudentMusicoinPackage();
        $model->scenario = 'buycoin_popup';
        $model->student_uuid = Yii::$app->user->identity->student_uuid;
        $error = '';
        
        $model->attributes = Yii::$app->getRequest()->post('StudentMusicoinPackage');
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
           
            //Prepare stripe array
            $postdata = Yii::$app->getRequest()->post('StudentMusicoinPackage');
            $postdata['stripeToken'] = $_POST['stripeToken'];
            
            $strip_array = StudentMusicoinPackage::make_cart_submit_array($postdata);
           
            if(!empty($strip_array)){
                   
                $payment_response = StudentMusicoinPackage::stripePayment($strip_array);

                if (isset($payment_response['code']) && $payment_response['code'] == 200) {

                    $return = ['code' => 200, 'message' => 'You have successfully purchased Musicoins.', 'musicoin_balance' => $payment_response['musicoin_balance']];
                    Yii::$app->response->format = 'json';
                    return $return;
                }else{
                    $error = $payment_response['message'];
                }
            }else{
                $error = 'Somthing wrong went to prepare payment process.';
            }
        }
        
        return $this->renderAjax('buy', [
                        'model' => $model,
                        'error' => $error,
            ]);
    }
        
    public function actionRefreshCart() {

        $step_data = StudentTuitionBooking::getSession("buy_coin_".Yii::$app->user->identity->student_uuid);
        
        $coupon_code = Yii::$app->getRequest()->get('coupon_code');
        $package_id = Yii::$app->getRequest()->get('package_id');
        $package_id = (!empty($package_id))? $package_id : NULL;
        
        $model_coupon = StudentMusicoinPackage::check_coupon_available($coupon_code);
        $package = MusicoinPackage::find()->where(['uuid' => $package_id,'status' => 'ENABLED'])->one();
        
        if(!empty($package)){
            $price = $package->price;
            $gst = $package->gst;
            $total_price = $package->total_price;
            $cart_total = $package->total_price;
            $coins = $package->coins;
            $bonus_coins = $package->bonus_coins;
            $total_coins = $package->total_coins;
            $cart_total_coins = $package->total_coins;
        }else{
            $price = 0;
            $gst = 0;
            $total_price = 0;
            $cart_total = 0;
            $coins = 0;
            $bonus_coins = 0;
            $total_coins = 0;
            $cart_total_coins = 0;
        }
        
        $result = [
            'price' => $price,
            'gst' => $gst,
            'total_price' => $total_price,
            'discount' => 0,
            'cart_total' => $cart_total,
            'coins' => $coins,
            'bonus_coins' => $bonus_coins,
            'total_coins' => $total_coins,
            'promo_coins' => 0,
            'cart_total_coins' => $cart_total_coins,
            'coupon_uuid' => '',
            'coupon_code' => $coupon_code,
            'is_available' => $model_coupon['is_available'],
            'message' => $model_coupon['message'],
        ];
        
        if($model_coupon['is_available']){
            $result['coupon_uuid'] = $model_coupon['coupon_uuid'];
            $result['promo_coins'] = $model_coupon['promo_coins'];
            $result['cart_total_coins'] = (int) ($model_coupon['promo_coins'] + $result['cart_total_coins']);
        }
                
        $return = ['code' => 200, 'message' => 'success', 'result' => $result];

        Yii::$app->response->format = 'json';
        return $return;
    }
    
    public function actionRemoveCard() {

        $id = Yii::$app->getRequest()->post('id');
        if ((($model = StudentCreditCard::findOne($id)) !== null)) {
            $model->delete();
            $return = ['code' => 200, 'message' => 'Credit Card deleted successfully.'];
        } else {

            $return = ['code' => 404, 'message' => 'The requested page does not exist.'];
        }
        Yii::$app->response->format = 'json';
        return $return;
    }

    /**
     * Finds the MusicoinLog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return MusicoinLog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MusicoinLog::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
