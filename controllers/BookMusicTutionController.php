<?php

namespace app\controllers;

use Yii;
use app\models\StudentTuitionBookingSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\libraries\General;
use yii\helpers\Url;
use yii\filters\AccessControl;
use app\models\StudentTuitionBooking;
use app\models\BookedSession;
use app\models\Student;
use app\models\Tutor;
use app\models\User;
use app\models\Instrument;
use app\models\OnlineTutorialPackage;
use app\models\MonthlySubscriptionFee;
use app\models\StudentCreditCard;
use app\models\TutorialType;
use app\models\RescheduleSession;
use app\models\StandardPlanOptionRate;
use app\models\OnlineTutorialPackageType;
use app\models\OnlineTutorialPackageTutorwise;
use app\models\StudentTuitionStandardBookingLockingPeriod;

class BookMusicTutionController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => Yii::$app->user->can('/book-music-tution/index'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['remove-card'],
                        'allow' => Yii::$app->user->can('/book-music-tution/remove-card'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['refresh-cart'],
                        'allow' => Yii::$app->user->can('/book-music-tution/refresh-cart'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['get-online-tutorial'],
                        'allow' => Yii::$app->user->can('/book-music-tution/get-online-tutorial'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['get-day-changes'],
                        'allow' => Yii::$app->user->can('/book-music-tution/get-day-changes'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['get-day-start-time'],
                        'allow' => Yii::$app->user->can('/book-music-tution/get-day-start-time'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['credit-schedule'],
                        'allow' => Yii::$app->user->can('/book-music-tution/credit-schedule'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['credit-get-day-changes'],
                        'allow' => Yii::$app->user->can('/book-music-tution/credit-get-day-changes'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['credit-get-day-start-time'],
                        'allow' => Yii::$app->user->can('/book-music-tution/credit-get-day-start-time'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['card-validation'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['history'],
                        'allow' => Yii::$app->user->can('/book-music-tution/history'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['view'],
                        'allow' => Yii::$app->user->can('/book-music-tution/view'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['sales-representative-commissions'],
                        'allow' => Yii::$app->user->can('/book-music-tution/sales-representative-commissions'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['reschedule'],
                        'allow' => Yii::$app->user->can('/book-music-tution/reschedule'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['reschedule-get-day-changes'],
                        'allow' => Yii::$app->user->can('/book-music-tution/reschedule-get-day-changes'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['reschedule-get-day-start-time'],
                        'allow' => Yii::$app->user->can('/book-music-tution/reschedule-get-day-start-time'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['reschedule-request'],
                        'allow' => Yii::$app->user->can('/book-music-tution/reschedule-request'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['reschedule-request-approval'],
                        'allow' => Yii::$app->user->can('/book-music-tution/reschedule-request-approval'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['get-tutor-available-hours'],
                        'allow' => Yii::$app->user->can('/book-music-tution/get-tutor-available-hours'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['get-current-user-selected-time-info'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
        ];
    }

    public function beforeAction($action) {
        if ($action->id == "reschedule-request-approval") {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function actionIndex() {

        $this->layout = "student_layout";

        $model = new StudentTuitionBooking();
        $model->cc_checkbox = true;
        $model->cart_total = 0;
        $instrumentList = StudentTuitionBooking::getInstrumentList();
        //$onlineTutorialList = StudentTuitionBooking::getOnlineTutorialList();
        $monthlyPackage = MonthlySubscriptionFee::studentRecomandedMonthlySubscription(Yii::$app->user->identity->student_uuid);
        $step_data = StudentTuitionBooking::getSession(Yii::$app->user->identity->student_uuid);
        $saved_cc_detail = StudentCreditCard::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid, 'status' => 'ENABLED'])->one();

        if (Yii::$app->getRequest()->post('step') == '1') {
//            echo "<pre>";print_r(Yii::$app->getRequest()->post());exit;
            $model->scenario = 'step_1';
            if (empty(Yii::$app->getRequest()->post('StudentTuitionBooking'))) {
                $model->instrument_uuid = $step_data['step_1']['instrument_uuid'];
            } else {
                $model->attributes = Yii::$app->getRequest()->post('StudentTuitionBooking');
            }


            if ($model->load(Yii::$app->request->post()) && $model->validate()) {

                StudentTuitionBooking::setSession(Yii::$app->user->identity->student_uuid, 'step_1', Yii::$app->getRequest()->post('StudentTuitionBooking'));
                $step_data = StudentTuitionBooking::getSession(Yii::$app->user->identity->student_uuid);
                //echo "<pre>";print_r($step_1_data);exit;
                return $this->render('step_2', [
                            'model' => $model,
                            'monthlyPackage' => $monthlyPackage,
                            'step_data' => $step_data,
                ]);
            }

            return $this->render('step_1', [
                        'model' => $model,
                        'instrumentList' => $instrumentList,
                        'step_data' => $step_data,
            ]);
        } elseif (Yii::$app->getRequest()->post('step') == '2') {

            $model->scenario = 'step_2';

            if (empty(Yii::$app->getRequest()->post('StudentTuitionBooking'))) {
                $model->tutor_uuid = $step_data['step_2']['tutor_uuid'];
                $model->tutorcheckbox = $step_data['step_2']['tutor_uuid'];
                //echo 3;exit;
            } else {

                $model->attributes = Yii::$app->getRequest()->post('StudentTuitionBooking');
                //echo 4;exit;
            }

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {

                StudentTuitionBooking::setSession(Yii::$app->user->identity->student_uuid, 'step_2', Yii::$app->getRequest()->post('StudentTuitionBooking'));
                $step_data = StudentTuitionBooking::getSession(Yii::$app->user->identity->student_uuid);

                return $this->render('step_3', [
                            'model' => $model,
                            //'onlineTutorialList' => $onlineTutorialList,
                            'step_data' => $step_data,
                ]);
            }

            return $this->render('step_2', [
                        'model' => $model,
                        //'monthlyPackage' => $monthlyPackage,
                        'step_data' => $step_data,
            ]);
        } elseif (Yii::$app->getRequest()->post('step') == '3') {

            $model->scenario = 'step_3';

            if (empty(Yii::$app->getRequest()->post('StudentTuitionBooking'))) {

                $model->onlinetutorial_uuid = $step_data['step_3']['onlinetutorial_uuid'];
                $model->onlinetutorialcheckbox = $step_data['step_3']['onlinetutorial_uuid'];
            } else {
                $model->attributes = Yii::$app->getRequest()->post('StudentTuitionBooking');
            }

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {

                StudentTuitionBooking::setSession(Yii::$app->user->identity->student_uuid, 'step_3', Yii::$app->getRequest()->post('StudentTuitionBooking'));
                $step_data = StudentTuitionBooking::getSession(Yii::$app->user->identity->student_uuid);



                return $this->render('step_4', [
                            'model' => $model,
                            'monthlyPackage' => $monthlyPackage,
                            'step_data' => $step_data,
                ]);
            }

            return $this->render('step_3', [
                        'model' => $model,
                       // 'onlineTutorialList' => $onlineTutorialList,
                        'step_data' => $step_data,
            ]);
        } elseif (Yii::$app->getRequest()->post('step') == '4') {

            $model->scenario = 'step_4';

            if (empty(Yii::$app->getRequest()->post('StudentTuitionBooking'))) {

                $model->all_dates = $step_data['step_4']['all_dates'];
                $model->session_week_day = $step_data['step_4']['session_week_day'];
                $model->session_when_start = $step_data['step_4']['session_when_start'];
                $model->start_hour = $step_data['step_4']['start_hour'];
                $model->session_text_info = $step_data['step_4']['session_text_info'];
            } else {
                $model->attributes = Yii::$app->getRequest()->post('StudentTuitionBooking');
                // echo "<pre>";print_r(Yii::$app->getRequest()->post('StudentTuitionBooking'));exit;
            }

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                StudentTuitionBooking::setSession(Yii::$app->user->identity->student_uuid, 'step_4', Yii::$app->getRequest()->post('StudentTuitionBooking'));
                $step_data = StudentTuitionBooking::getSession(Yii::$app->user->identity->student_uuid);

                return $this->render('step_5', [
                            'model' => $model,
                            'step_data' => $step_data,
                ]);
            }

            return $this->render('step_4', [
                        'model' => $model,
                        'step_data' => $step_data,
            ]);
        } elseif (Yii::$app->getRequest()->post('step') == '5' && Yii::$app->getRequest()->post('remove_card')) {

            $model->scenario = 'step_5';

            $model->attributes = Yii::$app->getRequest()->post('StudentTuitionBooking');

            return $this->render('step_5', [
                        'model' => $model,
                        'step_data' => $step_data,
            ]);
        } elseif (Yii::$app->getRequest()->post('step') == '5') {

            $model->scenario = 'step_5';

            if (empty(Yii::$app->getRequest()->post('StudentTuitionBooking'))) {

                $model->cc_name = $step_data['step_5']['cc_name'];
                $model->cc_number = $step_data['step_5']['cc_number'];
                $model->cc_cvv = $step_data['step_5']['cc_cvv'];
                $model->cc_month_year = $step_data['step_5']['cc_month_year'];
                $model->cc_checkbox = $step_data['step_5']['cc_checkbox'];
                $model->term_condition_checkbox = $step_data['step_5']['term_condition_checkbox'];
            } else {
                $model->attributes = Yii::$app->getRequest()->post('StudentTuitionBooking');
            }

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                if (!isset($_POST['stripeToken'])) {
                    Yii::$app->getSession()->setFlash('danger', 'Stripe token not found');
                    return $this->render('step_5', [
                                'model' => $model,
                                'step_data' => $step_data,
                    ]);
                }
//                echo "<pre>";
//                print_r($_POST);
//                echo "</pre>";
//                exit;
                echo '<script>blockBody("Please wait request under process...");</script>';
                $strip_array = StudentTuitionBooking::make_cart_submit_array($_POST);

                $payment_response = StudentTuitionBooking::stripePayment($strip_array);
                //Unset cart
                StudentTuitionBooking::unsetSession(Yii::$app->user->identity->student_uuid);

                if (isset($payment_response['code']) && $payment_response['code'] == 200) {

                    Yii::$app->getSession()->setFlash('success', 'You have successfully booked Lessons.');
                } else {

                    $message = (isset($payment_response['message'])) ? $payment_response['message'] : 'Sorry, something went wrong.';
                    Yii::$app->getSession()->setFlash('danger', $message);
                }

                return $this->redirect(['booked-session/upcoming']);
            }

            return $this->render('step_5', [
                        'model' => $model,
                        'step_data' => $step_data,
            ]);
        } else {
            $model->scenario = 'step_1';
            return $this->render('step_1', [
                        'model' => $model,
                        'instrumentList' => $instrumentList,
                        'step_data' => $step_data,
            ]);
        }
    }

    public function actionCardValidation() {

        if (!Yii::$app->request->isAjax) {
            throw new \yii\web\HttpException(505, 'You are not authorised to access this page');
        }
        $validatemulti = [];
        $model = new StudentTuitionBooking();
        $model->scenario = 'step_5';
        $model->cc_checkbox = true;
        $model->cart_total = 0;

        $model->attributes = Yii::$app->getRequest()->post('StudentTuitionBooking');

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            // ajax validation
            $validatemulti = \yii\widgets\ActiveForm::validate($model);
        }
        Yii::$app->response->format = 'json';
        return $validatemulti;
    }

    public function actionRemoveCard() {

        $id = Yii::$app->getRequest()->post('id');
        if ((($model = StudentCreditCard::findOne($id)) !== null)) {
            $model->delete();
            $return = ['code' => 200, 'message' => 'Credit Card deleted successfully.'];
        } else {

            $return = ['code' => 404, 'message' => 'The requested page does not exist.'];
        }
        Yii::$app->response->format = 'json';
        return $return;
    }

    public function actionRefreshCart() {

        $step_data = StudentTuitionBooking::getSession(Yii::$app->user->identity->student_uuid);
        $student_detail = Student::findOne(Yii::$app->user->identity->student_uuid);



        $return = ['monthly_sub_total' => 0];

        $cart_total = 0;
        $discount = 0;
        if ($student_detail->isMonthlySubscription == 1) {
            $monthly_sub_check = 'no';
        } else {
            $monthly_sub_check = Yii::$app->getRequest()->get('monthly_sub_check');
        }
        $monthly_sub_uuid = Yii::$app->getRequest()->get('monthly_sub_uuid');
        $coupon_code = Yii::$app->getRequest()->get('coupon_code');

        $return['monthly_sub_check'] = $monthly_sub_check;

        $tutorDetail = Tutor::findOne($step_data['step_2']['tutor_uuid']);

        $package_type_model = OnlineTutorialPackageType::find()->where(['uuid' => $tutorDetail->online_tutorial_package_type_uuid])->one();
        $package_type = $package_type_model->name;



        if ($package_type != "STANDARD") {

            //$planDetail = OnlineTutorialPackage::find()->where(['uuid' => $step_data['step_3']['onlinetutorial_uuid']])->one();
            $query = new \yii\db\Query;
            $planDetail = $query->select(['o.uuid as package_uuid', 'p.uuid', 'p.name', 'p.price', 'p.gst', 'p.total_price', 'p.status', 'p.default_option', 'p.online_tutorial_package_type_uuid']
                    )
                    ->from('online_tutorial_package as o')
                    ->join('RIGHT JOIN', 'premium_standard_plan_rate as p', 'o.uuid = p.online_tutorial_package_uuid')
                    ->where(['p.uuid' => $step_data['step_3']['onlinetutorial_uuid']])
                    //->asArray()
                    ->one();

            $return['plan_base_price'] = $planDetail['price'];
            $return['plan_gst'] = $planDetail['gst'];
            $return['plan_price'] = $planDetail['total_price'];
        } else {
            $planDetail = StandardPlanOptionRate::find()->where(['premium_standard_plan_rate_uuid' => $step_data['step_3']['onlinetutorial_uuid'], 'option' => $step_data['step_3']['option']])->one();
            $return['plan_base_price'] = $planDetail->price;
            $return['plan_gst'] = $planDetail->gst;
            $return['plan_price'] = $planDetail->total_price;
            $return['plan_option'] = $planDetail->option;
            
            
            //Get id of tutorwise paln 
    $tutorwise_model = OnlineTutorialPackageTutorwise::find()->where(['premium_standard_plan_rate_uuid' => $step_data['step_3']['onlinetutorial_uuid'], 'tutor_uuid' => $step_data['step_2']['tutor_uuid']])->one();
    $online_tutorial_package_tutorwise_uuid = $tutorwise_model->uuid;

    //Standard tutor loking period
        $locking_per_model_count = StudentTuitionStandardBookingLockingPeriod::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid, 'tutor_uuid' => $step_data['step_2']['tutor_uuid']])->count();

    if ($locking_per_model_count > 0) {

        $locking_per_model = StudentTuitionStandardBookingLockingPeriod::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid, 'tutor_uuid' => $step_data['step_2']['tutor_uuid']])->one();
        $locking_end_datetime = $locking_per_model->locking_end_datetime;
        $locking_start_datetime = $locking_per_model->locking_start_datetime;
        $locking_option = $locking_per_model->option;
       

        $current_date = date('Y-m-d H:i:s');

        if (($return['plan_option'] < $locking_option) && ($current_date <= $locking_end_datetime)) {

            $lockingplanDetail = StandardPlanOptionRate::find()->where(['premium_standard_plan_rate_uuid' => $step_data['step_3']['onlinetutorial_uuid'], 'option' => $locking_option])->one();
                            
                            
            $return['plan_price'] = $lockingplanDetail->total_price;
            $return['plan_base_price'] = $lockingplanDetail->price;
            $return['plan_gst'] = $lockingplanDetail->gst;
            $return['plan_option'] = $lockingplanDetail->option;
        }
    }
    
    
        }





        // echo "<pre>";
        //print_r($planDetail);exit;


        $model_coupon = StudentTuitionBooking::check_coupon_available($coupon_code, $return['plan_price']);
        $return['coupon'] = $model_coupon;

        if ($monthly_sub_check == 'yes') {
            //$monthly_model = MonthlySubscriptionFee::find()->where(['with_tutorial' => 1, 'status' => 'ENABLED'])->one();
            //change by pooja for comman price for manthly subscription
            $monthly_model = MonthlySubscriptionFee::find()->where([ 'status' => 'ENABLED'])->one();
            if (!empty($monthly_model)) {
                $return['monthly_sub_total'] = $monthly_model->total_price;
            }
        }

        if ($model_coupon['is_available']) {
            $discount = $model_coupon['discount'];
        }

        $cart_total = (($return['plan_price'] + $return['monthly_sub_total'] ) - $discount);
        $return['cart_total'] = number_format((float)$cart_total, 2, '.', '');

        $return = ['code' => 200, 'message' => 'success', 'result' => $return];

        Yii::$app->response->format = 'json';
        return $return;
    }

    public function actionGetOnlineTutorial() {
        if (!Yii::$app->request->isAjax) {
            throw new \yii\web\HttpException(505, 'You are not authorised to access this page');
        }


        $onlineTutorialList = StudentTuitionBooking::getOnlineTutorialList(Yii::$app->request->get('type'), Yii::$app->request->get('tutor_uuid'), Yii::$app->request->get('instrument_uuid'));
        return $this->renderAjax('step_2_inner', [
                    'onlineTutorialList' => $onlineTutorialList,
        ]);
    }

    public function actionGetDaywiseSession() {

        if (!Yii::$app->request->isAjax) {
            throw new \yii\web\HttpException(505, 'You are not authorised to access this page');
        }
        //echo "--".Yii::$app->request->get('day');exit;
        $step_data = StudentTuitionBooking::getSession(Yii::$app->user->identity->student_uuid);

        $sessionDetail = StudentTuitionBooking::getDaywiseSeesionDetail($step_data['step_3']['tutor_uuid'], Yii::$app->request->get('day'), $step_data['step_2']['onlinetutorial_uuid']);

        $return = ['code' => 200, 'message' => 'Success', 'session' => $sessionDetail];
        Yii::$app->response->format = 'json';
        return $return;
    }

    public function actionGetDayChanges() {

        if (!Yii::$app->request->isAjax) {
            throw new \yii\web\HttpException(505, 'You are not authorised to access this page');
        }

        $step_data = StudentTuitionBooking::getSession(Yii::$app->user->identity->student_uuid);
        $selected_time = Yii::$app->request->get('selected_time');
        $selected_day = strtolower(Yii::$app->request->get('day'));
        $selected_date = Yii::$app->request->get('selected_when_date');

        $tutor_user_model = User::find()->where(['tutor_uuid' => $step_data['step_2']['tutor_uuid']])->one();
        $student_user_model = User::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid])->one();
        $tutor_tz = (!empty($tutor_user_model)) ? $tutor_user_model->timezone : Yii::$app->params['timezone'];
        $student_tz = (!empty($student_user_model)) ? $student_user_model->timezone : Yii::$app->params['timezone'];

        if ($selected_date != '') {
            $session_date = date('d-m-Y', strtotime($selected_date));//General::displayDate($selected_date);
        } else {
            $session_date = (date("l") == ucfirst($selected_day)) ? date('Y-m-d') : date('Y-m-d', strtotime(date('Y-m-d') . ' next ' . $selected_day));
            $session_date = date('d-m-Y', strtotime($selected_date)); //General::displayDate($selected_date);
        }
        if ($selected_day == '') {
            $selected_day = strtolower(date("l", strtotime($session_date)));
        }
        
        $disabled_day = StudentTuitionBooking::disabled_week_day($selected_day);
        $between = StudentTuitionBooking::time_between_day_working_period($step_data['step_2']['tutor_uuid'], $selected_day, $selected_time, $step_data['step_3']['onlinetutorial_uuid'], $tutor_tz, $student_tz, $session_date);
        //$between = true;
        if ($between) {
            $sessionDetail = StudentTuitionBooking::getDayChanges($step_data['step_2']['tutor_uuid'], $selected_day, $step_data['step_3']['onlinetutorial_uuid'], $selected_time, $selected_date, Yii::$app->user->identity->student_uuid);
            $session_text_info = StudentTuitionBooking::convert_session_text($sessionDetail, $step_data['step_3']['onlinetutorialtype']);

            $all_dates = '';
            if (!empty($sessionDetail['session_array'])) {
                $all_dates_array = array_column($sessionDetail['session_array'], 'date');
                $all_dates = implode(',', $all_dates_array);
            }

            $return = ['code' => 200, 'message' => ($sessionDetail['message'] != '') ? $sessionDetail['message'] : 'success', 'session_when_start' => $session_date, 'session' => $sessionDetail, 'disabled_day' => $disabled_day, 'session_text_info' => $session_text_info, 'selected_day' => $selected_day, 'all_dates' => $all_dates];
        } else {

            //$return = ['code' => 421, 'message' => 'This time period is not available in ' . ucfirst($selected_day), 'session_when_start' => $session_date, 'disabled_day' => $disabled_day, 'session_text_info' => '','selected_day' => $selected_day,'all_dates' => ''];
            $return = ['code' => 421, 'message' => 'Your selected tutor is not available for one or more of the selected times/days. ' . ucfirst($selected_day) . ' ' . General::displayTimeFormat($selected_time), 'session_when_start' => $session_date, 'disabled_day' => $disabled_day, 'session_text_info' => '', 'selected_day' => $selected_day, 'all_dates' => ''];
        }
        Yii::$app->response->format = 'json';
        return $return;
    }

    public function actionGetDayStartTime() {

        if (!Yii::$app->request->isAjax) {
            throw new \yii\web\HttpException(505, 'You are not authorised to access this page');
        }

        $step_data = StudentTuitionBooking::getSession(Yii::$app->user->identity->student_uuid);
        $day = strtolower(Yii::$app->request->get('day'));

        $tutorDetail = Tutor::findOne($step_data['step_3']['tutor_uuid']);

        $tutor_user_model = User::find()->where(['tutor_uuid' => $step_data['step_3']['tutor_uuid']])->one();
        $student_user_model = User::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid])->one();
        $tutor_tz = (!empty($tutor_user_model)) ? $tutor_user_model->timezone : Yii::$app->params['timezone'];
        $student_tz = (!empty($student_user_model)) ? $student_user_model->timezone : Yii::$app->params['timezone'];

        $package = OnlineTutorialPackage::find()->joinWith(['paymentTermUu', 'tutorialTypeUu'])->where(['online_tutorial_package.uuid' => $step_data['step_2']['onlinetutorial_uuid']])->asArray()->one();
        $sessionDetail = StudentTuitionBooking::get_available_hours($step_data['step_3']['tutor_uuid'], json_decode($tutorDetail->working_plan, true), date('Y-m-d', strtotime(date('Y-m-d') . ' next ' . $day)), $package['tutorialTypeUu']['min'], Yii::$app->user->identity->student_uuid, $tutor_tz, $student_tz);

        $time = "00:00";
        if (!empty($sessionDetail)) {
            $time = [];
            foreach ($sessionDetail as $key => $val) {
                $timestamp = strtotime($val);
                $key = date('h:i', $timestamp);
                $value = date('h:i A', $timestamp);
                $time[$key] = $value;
            }
        }
        $session_date = (date("l") == ucfirst($day)) ? date('Y-m-d') : date('Y-m-d', strtotime(date('Y-m-d') . ' next ' . $day));
        $session_date = General::displayDate($session_date);
        $disabled_day = StudentTuitionBooking::disabled_week_day($day);
        $return = ['code' => 200, 'message' => 'Success', 'time' => $time, 'session_when_start' => $session_date, 'day' => $day, 'disabled_day' => $disabled_day];
        Yii::$app->response->format = 'json';
        return $return;
    }

    public function actionCreditSchedule() {

        $this->layout = "student_layout";

        $s_c_count = \app\models\CreditSession::getStudentCreditSessionCount(Yii::$app->user->identity->student_uuid);
        if ($s_c_count < 1) {

            Yii::$app->getSession()->setFlash('danger', 'You do not have sufficient credit.');

            return $this->redirect(['booked-session/upcoming']);
        }

        $model = new StudentTuitionBooking();
        $instrumentList = StudentTuitionBooking::getInstrumentList();
        $monthlyPackage = MonthlySubscriptionFee::studentRecomandedMonthlySubscription(Yii::$app->user->identity->student_uuid);
        $step_data = StudentTuitionBooking::getSession("credit_" . Yii::$app->user->identity->student_uuid);

        if (Yii::$app->getRequest()->post('step') == '1') {

            $model->scenario = 'credit_step_1';
            if (empty(Yii::$app->getRequest()->post('StudentTuitionBooking'))) {
                $model->instrument_uuid = $step_data['step_1']['instrument_uuid'];
            } else {
                $model->attributes = Yii::$app->getRequest()->post('StudentTuitionBooking');
            }

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {

                StudentTuitionBooking::setSession("credit_" . Yii::$app->user->identity->student_uuid, 'step_1', Yii::$app->getRequest()->post('StudentTuitionBooking'));
                $step_data = StudentTuitionBooking::getSession("credit_" . Yii::$app->user->identity->student_uuid);

                return $this->render('credit_step_2', [
                             'model' => $model,
                            'monthlyPackage' => $monthlyPackage,
                            'step_data' => $step_data,
                ]);
            }

            return $this->render('credit_step_1', [
                        'model' => $model,
                        'instrumentList' => $instrumentList,
                        'step_data' => $step_data,
            ]);
        } elseif (Yii::$app->getRequest()->post('step') == '2') {
            
            
            $model->scenario = 'credit_step_2';
            if (empty(Yii::$app->getRequest()->post('StudentTuitionBooking'))) {

                $model->tutor_uuid = $step_data['step_2']['tutor_uuid'];
                $model->tutorcheckbox = $step_data['step_2']['tutor_uuid'];
            } else {
                $model->attributes = Yii::$app->getRequest()->post('StudentTuitionBooking');
            }

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {

                StudentTuitionBooking::setSession("credit_" . Yii::$app->user->identity->student_uuid, 'step_2', Yii::$app->getRequest()->post('StudentTuitionBooking'));
                $step_data = StudentTuitionBooking::getSession("credit_" . Yii::$app->user->identity->student_uuid);

                return $this->render('credit_step_3', [
                            'model' => $model,
                            'step_data' => $step_data,
                ]);
            }

            return $this->render('credit_step_2', [
                        'model' => $model,
                'monthlyPackage' => $monthlyPackage,
                        'step_data' => $step_data,
            ]);

            
        } elseif (Yii::$app->getRequest()->post('step') == '3') {

            
            $model->scenario = 'credit_step_3';
            if (empty(Yii::$app->getRequest()->post('StudentTuitionBooking'))) {

                $model->tutorial_type_code = $step_data['step_3']['tutorial_type_code'];
                $model->tutorial_type_code_checkbox = $step_data['step_3']['tutorial_type_code_checkbox'];
            } else {
                $model->attributes = Yii::$app->getRequest()->post('StudentTuitionBooking');
            }

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {

                StudentTuitionBooking::setSession("credit_" . Yii::$app->user->identity->student_uuid, 'step_3', Yii::$app->getRequest()->post('StudentTuitionBooking'));
                $step_data = StudentTuitionBooking::getSession("credit_" . Yii::$app->user->identity->student_uuid);

                 return $this->render('credit_step_4', [
                            'model' => $model,
                            'step_data' => $step_data,
                ]);
            }

            return $this->render('credit_step_3', [
                        'model' => $model,
                        'step_data' => $step_data,
            ]);
            
        } elseif (Yii::$app->getRequest()->post('step') == '4') {

            $model->scenario = 'credit_step_4';
            if (empty(Yii::$app->getRequest()->post('StudentTuitionBooking'))) {

                $model->all_dates = $step_data['step_4']['all_dates'];
                $model->session_week_day = $step_data['step_4']['session_week_day'];
                $model->session_when_start = $step_data['step_4']['session_when_start'];
                $model->start_hour = $step_data['step_4']['start_hour'];
                $model->session_text_info = $step_data['step_4']['session_text_info'];
            } else {
                $model->attributes = Yii::$app->getRequest()->post('StudentTuitionBooking');
            }

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {

                StudentTuitionBooking::setSession("credit_" . Yii::$app->user->identity->student_uuid, 'step_4', Yii::$app->getRequest()->post('StudentTuitionBooking'));
                $step_data = StudentTuitionBooking::getSession("credit_" . Yii::$app->user->identity->student_uuid);

                $response = StudentTuitionBooking::bookCreditTution($step_data, Yii::$app->user->identity->student_uuid);

                if (isset($response['code']) && $response['code'] == 200) {

                    Yii::$app->getSession()->setFlash('success', 'You have successfully booked Lesson.');
                } else {

                    $message = (isset($response['message'])) ? $response['message'] : 'Sorry, something went wrong.';
                    Yii::$app->getSession()->setFlash('danger', $message);
                }

                return $this->redirect(['booked-session/upcoming']);
            }
        } else {
            $model->scenario = 'credit_step_1';
            return $this->render('credit_step_1', [
                        'model' => $model,
                        'instrumentList' => $instrumentList,
                        'step_data' => $step_data,
            ]);
        }
    }

    public function actionCreditScheduleOld() {
        exit;
        $s_c_count = \app\models\CreditSession::getStudentCreditSessionCount(Yii::$app->user->identity->student_uuid);
        if ($s_c_count < 1) {

            Yii::$app->getSession()->setFlash('danger', 'You do not have sufficient credit.');

            return $this->redirect(['booked-session/upcoming']);
        }

        $model = new StudentTuitionBooking();
        $instrumentList = StudentTuitionBooking::getInstrumentList();
        $monthlyPackage = MonthlySubscriptionFee::studentRecomandedMonthlySubscription(Yii::$app->user->identity->student_uuid);
        $step_data = StudentTuitionBooking::getSession("credit_" . Yii::$app->user->identity->student_uuid);

        if (Yii::$app->getRequest()->post('step') == '1') {

            $model->scenario = 'credit_step_1';
            if (empty(Yii::$app->getRequest()->post('StudentTuitionBooking'))) {
                $model->instrument_uuid = $step_data['step_1']['instrument_uuid'];
            } else {
                $model->attributes = Yii::$app->getRequest()->post('StudentTuitionBooking');
            }

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {

                StudentTuitionBooking::setSession("credit_" . Yii::$app->user->identity->student_uuid, 'step_1', Yii::$app->getRequest()->post('StudentTuitionBooking'));
                $step_data = StudentTuitionBooking::getSession("credit_" . Yii::$app->user->identity->student_uuid);

                return $this->render('credit_step_2', [
                            'model' => $model,
                            'step_data' => $step_data,
                ]);
            }

            return $this->render('credit_step_1', [
                        'model' => $model,
                        'instrumentList' => $instrumentList,
                        'step_data' => $step_data,
            ]);
        } elseif (Yii::$app->getRequest()->post('step') == '2') {

            $model->scenario = 'credit_step_2';
            if (empty(Yii::$app->getRequest()->post('StudentTuitionBooking'))) {

                $model->tutorial_type_code = $step_data['step_2']['tutorial_type_code'];
                $model->tutorial_type_code_checkbox = $step_data['step_2']['tutorial_type_code_checkbox'];
            } else {
                $model->attributes = Yii::$app->getRequest()->post('StudentTuitionBooking');
            }

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {

                StudentTuitionBooking::setSession("credit_" . Yii::$app->user->identity->student_uuid, 'step_2', Yii::$app->getRequest()->post('StudentTuitionBooking'));
                $step_data = StudentTuitionBooking::getSession("credit_" . Yii::$app->user->identity->student_uuid);

                return $this->render('credit_step_3', [
                            'model' => $model,
                            'monthlyPackage' => $monthlyPackage,
                            'step_data' => $step_data,
                ]);
            }

            return $this->render('credit_step_2', [
                        'model' => $model,
                        'step_data' => $step_data,
            ]);
        } elseif (Yii::$app->getRequest()->post('step') == '3') {

            $model->scenario = 'credit_step_3';
            if (empty(Yii::$app->getRequest()->post('StudentTuitionBooking'))) {

                $model->tutor_uuid = $step_data['step_3']['tutor_uuid'];
                $model->tutorcheckbox = $step_data['step_3']['tutor_uuid'];
            } else {
                $model->attributes = Yii::$app->getRequest()->post('StudentTuitionBooking');
            }

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {

                StudentTuitionBooking::setSession("credit_" . Yii::$app->user->identity->student_uuid, 'step_3', Yii::$app->getRequest()->post('StudentTuitionBooking'));
                $step_data = StudentTuitionBooking::getSession("credit_" . Yii::$app->user->identity->student_uuid);

                return $this->render('credit_step_4', [
                            'model' => $model,
                            'step_data' => $step_data,
                ]);
            }

            return $this->render('credit_step_3', [
                        'model' => $model,
                        'step_data' => $step_data,
            ]);
        } elseif (Yii::$app->getRequest()->post('step') == '4') {

            $model->scenario = 'credit_step_4';
            if (empty(Yii::$app->getRequest()->post('StudentTuitionBooking'))) {

                $model->all_dates = $step_data['step_4']['all_dates'];
                $model->session_week_day = $step_data['step_4']['session_week_day'];
                $model->session_when_start = $step_data['step_4']['session_when_start'];
                $model->start_hour = $step_data['step_4']['start_hour'];
                $model->session_text_info = $step_data['step_4']['session_text_info'];
            } else {
                $model->attributes = Yii::$app->getRequest()->post('StudentTuitionBooking');
            }

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {

                StudentTuitionBooking::setSession("credit_" . Yii::$app->user->identity->student_uuid, 'step_4', Yii::$app->getRequest()->post('StudentTuitionBooking'));
                $step_data = StudentTuitionBooking::getSession("credit_" . Yii::$app->user->identity->student_uuid);

                $response = StudentTuitionBooking::bookCreditTution($step_data, Yii::$app->user->identity->student_uuid);

                if (isset($response['code']) && $response['code'] == 200) {

                    Yii::$app->getSession()->setFlash('success', 'You have successfully booked Lesson.');
                } else {

                    $message = (isset($response['message'])) ? $response['message'] : 'Sorry, something went wrong.';
                    Yii::$app->getSession()->setFlash('danger', $message);
                }

                return $this->redirect(['booked-session/upcoming']);
            }
        } else {
            $model->scenario = 'credit_step_1';
            return $this->render('credit_step_1', [
                        'model' => $model,
                        'instrumentList' => $instrumentList,
                        'step_data' => $step_data,
            ]);
        }
    }

    public function actionCreditGetDayChanges() {

        if (!Yii::$app->request->isAjax) {
            throw new \yii\web\HttpException(505, 'You are not authorised to access this page');
        }

        $step_data = StudentTuitionBooking::getSession("credit_" . Yii::$app->user->identity->student_uuid);
        $selected_time = Yii::$app->request->get('selected_time');
        $selected_day = strtolower(Yii::$app->request->get('day'));
        $selected_date = Yii::$app->request->get('selected_when_date');

        $tutor_user_model = User::find()->where(['tutor_uuid' => $step_data['step_2']['tutor_uuid']])->one();
        $student_user_model = User::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid])->one();
        $tutor_tz = (!empty($tutor_user_model)) ? $tutor_user_model->timezone : Yii::$app->params['timezone'];
        $student_tz = (!empty($student_user_model)) ? $student_user_model->timezone : Yii::$app->params['timezone'];

        $system_tz = Yii::$app->params['timezone'];

        if ($selected_date != '') {
            //$session_date = General::displayDate($selected_date);
            $session_date = date('d-m-Y', strtotime($selected_date));
            //$session_date = General::convertTimezone($selected_date,Yii::$app->user->identity->timezone,$system_tz,'Y-m-d');
            //echo $session_date;exit;
        } else {
            $session_date = (date("l") == ucfirst($selected_day)) ? date('Y-m-d') : date('Y-m-d', strtotime(date('Y-m-d') . ' next ' . $selected_day));
            $session_date = General::displayDate($selected_date);
        }
        if ($selected_day == '') {
            $selected_day = strtolower(date("l", strtotime($session_date)));
        }

        $disabled_day = StudentTuitionBooking::disabled_week_day($selected_day);
        $between = StudentTuitionBooking::credit_time_between_day_working_period($step_data['step_2']['tutor_uuid'], $selected_day, $selected_time, $step_data['step_3']['tutorial_type_code'], $tutor_tz, $student_tz, $session_date);
        //now bypass
        //$between = true;
        //exit;
        if ($between) {
            $sessionDetail = StudentTuitionBooking::credit_getDayChanges($step_data['step_2']['tutor_uuid'], $selected_day, $step_data['step_3']['tutorial_type_code'], $selected_time, $selected_date, Yii::$app->user->identity->student_uuid);
            $session_text_info = StudentTuitionBooking::credit_convert_session_text($sessionDetail);

            $all_dates = '';
            if (!empty($sessionDetail['session_array'])) {
                $all_dates_array = array_column($sessionDetail['session_array'], 'date');
                $all_dates = implode(',', $all_dates_array);
            }

            $return = ['code' => 200, 'message' => ($sessionDetail['message'] != '') ? $sessionDetail['message'] : 'success', 'session_when_start' => $session_date, 'session' => $sessionDetail, 'disabled_day' => $disabled_day, 'session_text_info' => $session_text_info, 'selected_day' => $selected_day, 'all_dates' => $all_dates];
        } else {

            //$return = ['code' => 421, 'message' => 'This time period is not availabel in ' . ucfirst($selected_day), 'session_when_start' => $session_date, 'disabled_day' => $disabled_day, 'session_text_info' => '','selected_day' => $selected_day,'all_dates' => ''];
            $return = ['code' => 421, 'message' => 'Your selected tutor is not available for one or more of the selected times/days. ' . ucfirst($selected_day) . ' ' . General::displayTimeFormat($selected_time), 'session_when_start' => $session_date, 'disabled_day' => $disabled_day, 'session_text_info' => '', 'selected_day' => $selected_day, 'all_dates' => ''];
        }
        Yii::$app->response->format = 'json';
        return $return;
    }

    public function actionCreditGetDayStartTime() {

        if (!Yii::$app->request->isAjax) {
            throw new \yii\web\HttpException(505, 'You are not authorised to access this page');
        }

        $step_data = StudentTuitionBooking::getSession("credit_" . Yii::$app->user->identity->student_uuid);
        $day = strtolower(Yii::$app->request->get('day'));

        $tutorDetail = Tutor::findOne($step_data['step_2']['tutor_uuid']);

        $tutor_user_model = User::find()->where(['tutor_uuid' => $step_data['step_2']['tutor_uuid']])->one();
        $student_user_model = User::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid])->one();
        $tutor_tz = (!empty($tutor_user_model)) ? $tutor_user_model->timezone : Yii::$app->params['timezone'];
        $student_tz = (!empty($student_user_model)) ? $student_user_model->timezone : Yii::$app->params['timezone'];

        $tutorialType = TutorialType::find()->where(['code' => $step_data['step_3']['tutorial_type_code']])->asArray()->one();

        $sessionDetail = StudentTuitionBooking::get_available_hours($step_data['step_2']['tutor_uuid'], json_decode($tutorDetail->working_plan, true), date('Y-m-d', strtotime(date('Y-m-d') . ' next ' . $day)), $tutorialType['min'], Yii::$app->user->identity->student_uuid, $tutor_tz, $student_tz);

        $time = "00:00";
        if (!empty($sessionDetail)) {
            $time = $sessionDetail[0];
        }
        $session_date = (date("l") == ucfirst($day)) ? date('Y-m-d') : date('Y-m-d', strtotime(date('Y-m-d') . ' next ' . $day));
        $session_date = General::displayDate($session_date);
        $disabled_day = StudentTuitionBooking::disabled_week_day($day);
        $return = ['code' => 200, 'message' => 'Success', 'time' => $time, 'session_when_start' => $session_date, 'day' => $day, 'disabled_day' => $disabled_day];
        Yii::$app->response->format = 'json';
        return $return;
    }

    public function actionHistory() {
        $searchModel = new StudentTuitionBookingSearch();
        $tuitionBookingList = $searchModel->search(Yii::$app->request->queryParams, true);
        //echo "<pre>"; print_r($tuitionBookingList); exit;
        if (Yii::$app->user->identity->role == 'STUDENT') {

            $this->layout = "student_layout";

            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('_book_music_tuition_student_list', [
                            'tuitionBookingList' => $tuitionBookingList,
                ]);
            } else {
                return $this->render('index_student', [
                            //'inactiveAdmin' => $inactiveAdmin,
                            'tuitionBookingList' => $tuitionBookingList,
                            'searchModel' => $searchModel,
                ]);
            }
        } else {
            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('_book_music_tuition_owner_list', [
                            'tuitionBookingList' => $tuitionBookingList,
                ]);
            } else {
                return $this->render('index', [
                            //'inactiveAdmin' => $inactiveAdmin,
                            'tuitionBookingList' => $tuitionBookingList,
                            'searchModel' => $searchModel,
                ]);
            }
        }
    }

    public function actionHistoryold() {
        $searchModel = new StudentTuitionBookingSearch();
        $tuitionBookingList = $searchModel->search(Yii::$app->request->queryParams, true);
        //echo "<pre>"; print_r($tuitionBookingList); exit;
        if (Yii::$app->user->identity->role == 'STUDENT') {
            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('_book_music_tuition_student_list', [
                            'tuitionBookingList' => $tuitionBookingList,
                ]);
            } else {
                return $this->render('index_student', [
                            //'inactiveAdmin' => $inactiveAdmin,
                            'tuitionBookingList' => $tuitionBookingList,
                            'searchModel' => $searchModel,
                ]);
            }
        } else {
            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('_book_music_tuition_owner_list', [
                            'tuitionBookingList' => $tuitionBookingList,
                ]);
            } else {
                return $this->render('index', [
                            //'inactiveAdmin' => $inactiveAdmin,
                            'tuitionBookingList' => $tuitionBookingList,
                            'searchModel' => $searchModel,
                ]);
            }
        }
    }

    public function actionView($id) {
        $model = $this->findModel($id);

        if (Yii::$app->user->identity->role == 'STUDENT') {

            $this->layout = "student_layout";

            return $this->render('view_student', [
                        'model' => $model,
            ]);
        } else {
            return $this->render('view', [
                        'model' => $model,
            ]);
        }
    }

    protected function findModel($id) {
        if (($model = StudentTuitionBooking::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionSalesRepresentativeCommissions() {
        $searchModel = new StudentTuitionBookingSearch();

        $searchModel->booking_datetime = date('01-m-Y') . ' - ' . date('t-m-Y');

        //$tuitionBookingList = $searchModel->searchSalesCommissions(Yii::$app->request->queryParams, true,'','booking_by_promocode');
        $tuitionBookingList = $searchModel->searchSalesCommissions(Yii::$app->request->queryParams, true, '', '');

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_booking_by_promocode_list', [
                        'tuitionBookingList' => $tuitionBookingList,
            ]);
        } else {
            return $this->render('booking_by_promocode', [
                        'tuitionBookingList' => $tuitionBookingList,
                        'searchModel' => $searchModel,
            ]);
        }
    }

    public function actionReschedule($id) {
        
        //For new layout We are redirect to below url
        return $this->redirect(['/book/reschedule','id' => $id]);

        if (($modelBS = BookedSession::find()->joinWith(['studentUu', 'instrumentUu', 'tutorUu'])->where(['booked_session.uuid' => $id])->one()) !== null) {

            if ($modelBS->status != "SCHEDULE") {
                throw new \yii\web\HttpException(505, 'This lesson not scheduled. Only scheduled lesson are reschedule.');
            }

            $model = new StudentTuitionBooking();
            $model->scenario = 'tutor_reschedule';

            $model->attributes = Yii::$app->getRequest()->post('StudentTuitionBooking');

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {

                $session_date_array = explode(',', $model->all_dates);

                if (isset($session_date_array[0]) && !empty($session_date_array[0]) && !empty($model->start_hour)) {
                    
                    $from_start_datetime = $modelBS->start_datetime; 
                    $from_end_datetime = $modelBS->end_datetime;
                    $student_timezone = General::getStudentTimezone($modelBS->student_uuid);
                    $tutor_timezone = General::getTutorTimezone($modelBS->tutor_uuid);

                    $to_start_datetime = date('Y-m-d H:i:s', strtotime($session_date_array[0] . " " . $model->start_hour));
                    $to_end_datetime = date('Y-m-d H:i:s', strtotime($to_start_datetime . " +" . $modelBS->session_min . " minutes"));

                    $modelRS = new RescheduleSession;
                    $modelRS->booked_session_uuid = $modelBS->uuid;
                    $modelRS->student_uuid = $model->student_uuid;
                    $modelRS->tutor_uuid = $model->tutor_uuid;
                    $modelRS->from_start_datetime = $modelBS->start_datetime;
                    $modelRS->from_end_datetime = $modelBS->end_datetime;
                    $modelRS->to_start_datetime = General::convertUserToSystemTimezone($to_start_datetime);
                    $modelRS->to_end_datetime = General::convertUserToSystemTimezone($to_end_datetime);
                    $modelRS->request_by = 'TUTOR';
                    $modelRS->requester_uuid = $model->tutor_uuid;
                    $modelRS->approved_by = 'TUTOR';
                    $modelRS->approver_uuid = $model->tutor_uuid;
                    $modelRS->approve_datetime = date('Y-m-d H:i:s');
                    $modelRS->status = 'APPROVED'; // APPROVED / REJECTED / AWAITING
                    $modelRS->referance_number = General::generateRescheduleID();
                    if ($modelRS->save(false)) {

                        $modelUpdateBS = BookedSession::findOne($modelBS->uuid);
                        if (!empty($modelUpdateBS)) {
                            $modelUpdateBS->start_datetime = $modelRS->to_start_datetime;
                            $modelUpdateBS->end_datetime = $modelRS->to_end_datetime;
//                            $modelUpdateBS->start_datetime = $to_start_datetime;
//                            $modelUpdateBS->end_datetime = $to_end_datetime;
                            $modelUpdateBS->save(false);

                            //send mail to student lesson reschedule by tutor
                            $content['student_name'] = $modelBS->studentUu->first_name;
                            $content['student_last_name'] = $modelBS->studentUu->last_name;
                            $content['tutor_name'] = $modelBS->tutorUu->first_name;
                            $content['tutor_last_name'] = $modelBS->tutorUu->last_name;
                            $content['instrument_name'] = $modelBS->instrumentUu->name;
                            $content['reschedule_number'] = $modelRS->referance_number;
                            //$content['from_date'] = General::displayDate($modelBS->start_datetime);
                            //$content['from_time'] = General::displayTime($modelBS->start_datetime) . ' - ' . General::displayTime($modelBS->end_datetime);
                            //$content['to_date'] = General::displayDate($modelRS->to_start_datetime);
                            //$content['to_time'] = General::displayTime($modelRS->to_start_datetime) . ' - ' . General::displayTime($modelRS->to_end_datetime);
                            $content['from_start_datetime'] = $from_start_datetime;
                            $content['from_end_datetime'] = $from_end_datetime;
                            $content['to_start_datetime'] = $modelRS->to_start_datetime;
                            $content['to_end_datetime'] = $modelRS->to_end_datetime;
                            $content['student_timezone'] = $student_timezone;
                            $content['tutor_timezone'] = $tutor_timezone;
                            
                            $ownersEmail = General::getOwnersEmails(['OWNER', 'ADMIN']);
                            Yii::$app->mailer->compose('tutor_reschedule_lesson_student_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                                    ->setFrom(Yii::$app->params['supportEmail'])
                                    ->setTo($modelBS->studentUu->email)
                                    //->setBcc($ownersEmail)
                                    ->setSubject(ucfirst(strtolower(Yii::$app->name)) . ' Notification - Tutor ' . $content['tutor_name'] . ' ' . $content['tutor_last_name'] . ' has made changes to a booked lesson with you')
                                    ->send();

                            //Send mail to tutor confirmed reschedule
                            Yii::$app->mailer->compose('tutor_reschedule_lesson_tutor_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                                    ->setFrom(Yii::$app->params['supportEmail'])
                                    ->setTo($modelBS->tutorUu->email)
                                    ->setSubject(ucfirst(strtolower(Yii::$app->name)) . ' Notification - Changes to booked lesson with ' . $content['student_name'] . ' ' . $content['student_last_name'] . ' are now confirmed in the booking system')
                                    ->send();

                            Yii::$app->mailer->compose('tutor_reschedule_lesson_admin_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                                    ->setFrom(Yii::$app->params['supportEmail'])
                                    ->setTo($ownersEmail)
                                    ->setSubject(ucfirst(strtolower(Yii::$app->name)) . ' Notification - Tutor ' . $content['tutor_name'] . ' ' . $content['tutor_last_name'] . '  has initiated changes to a booked lesson with ' . $content['student_name'] . ' ' . $content['student_last_name'] . ' : #' . $modelRS->referance_number)
                                    ->send();


                            Yii::$app->getSession()->setFlash('success', 'You have successfully rescheduled lesson.');
                            return $this->redirect(['/booked-session/upcoming']);
                        } else {
                            Yii::$app->getSession()->setFlash('danger', 'Reschedule lesson not successfully saved.');
                        }
                    } else {
                        Yii::$app->getSession()->setFlash('danger', 'Reschedule lesson not successfully saved.');
                    }
                } else {
                    Yii::$app->getSession()->setFlash('danger', 'Reschedule lesson not successfully saved.');
                }
            }

            return $this->render('reschedule', [
                        'modelBS' => $modelBS,
                        'model' => $model,
            ]);
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    public function actionRescheduleGetDayChanges() {

        if (!Yii::$app->request->isAjax) {
            throw new \yii\web\HttpException(505, 'You are not authorised to access this page');
        }

        $tutor_uuid = Yii::$app->request->get('tutor_uuid');
        $student_uuid = Yii::$app->request->get('student_uuid');
        $tutorial_type_code = Yii::$app->request->get('tutorial_type_code');

        $tutor_user_model = User::find()->where(['tutor_uuid' => $tutor_uuid])->one();
        $student_user_model = User::find()->where(['student_uuid' => $student_uuid])->one();
        $tutor_tz = (!empty($tutor_user_model)) ? $tutor_user_model->timezone : Yii::$app->params['timezone'];
        $student_tz = (!empty($student_user_model)) ? $student_user_model->timezone : Yii::$app->params['timezone'];

        $selected_time = Yii::$app->request->get('selected_time');
        $selected_day = strtolower(Yii::$app->request->get('day'));
        $selected_date = Yii::$app->request->get('selected_when_date');

        if ($selected_date != '') {
            //$session_date = General::displayDate($selected_date);
            $session_date = date('d-m-Y', strtotime($selected_date));
        } else {
            $session_date = (date("l") == ucfirst($selected_day)) ? date('Y-m-d') : date('Y-m-d', strtotime(date('Y-m-d') . ' next ' . $selected_day));
            $session_date = General::displayDate($selected_date);
        }
        if ($selected_day == '') {
            $selected_day = strtolower(date("l", strtotime($session_date)));
        }

        $disabled_day = StudentTuitionBooking::disabled_week_day($selected_day);
        $between = StudentTuitionBooking::credit_time_between_day_working_period($tutor_uuid, $selected_day, $selected_time, $tutorial_type_code, $tutor_tz, $student_tz, $session_date);
        //$between = true;
        if ($between) {
            $sessionDetail = StudentTuitionBooking::credit_getDayChanges($tutor_uuid, $selected_day, $tutorial_type_code, $selected_time, $selected_date, $student_uuid);
            $session_text_info = StudentTuitionBooking::credit_convert_session_text($sessionDetail);

            $all_dates = '';
            if (!empty($sessionDetail['session_array'])) {
                $all_dates_array = array_column($sessionDetail['session_array'], 'date');
                $all_dates = implode(',', $all_dates_array);
            }

            $return = ['code' => 200, 'message' => ($sessionDetail['message'] != '') ? $sessionDetail['message'] : 'success', 'session_when_start' => $session_date, 'session' => $sessionDetail, 'disabled_day' => $disabled_day, 'session_text_info' => $session_text_info, 'selected_day' => $selected_day, 'all_dates' => $all_dates];
        } else {

            $return = ['code' => 421, 'message' => 'Your selected tutor is not available for one or more of the selected times/days. ' . ucfirst($selected_day) . ' ' . General::displayTimeFormat($selected_time), 'session_when_start' => $session_date, 'disabled_day' => $disabled_day, 'session_text_info' => '', 'selected_day' => $selected_day, 'all_dates' => ''];
        }
        Yii::$app->response->format = 'json';
        return $return;
    }

    public function actionRescheduleGetDayStartTime() {

        if (!Yii::$app->request->isAjax) {
            throw new \yii\web\HttpException(505, 'You are not authorised to access this page');
        }

        $tutor_uuid = Yii::$app->request->get('tutor_uuid');
        $student_uuid = Yii::$app->request->get('student_uuid');
        $tutorial_type_code = Yii::$app->request->get('tutorial_type_code');

        $day = strtolower(Yii::$app->request->get('day'));

        $tutorDetail = Tutor::findOne($tutor_uuid);

        $tutor_user_model = User::find()->where(['tutor_uuid' => $tutor_uuid])->one();
        $student_user_model = User::find()->where(['student_uuid' => $student_uuid])->one();
        $tutor_tz = (!empty($tutor_user_model)) ? $tutor_user_model->timezone : Yii::$app->params['timezone'];
        $student_tz = (!empty($student_user_model)) ? $student_user_model->timezone : Yii::$app->params['timezone'];

        $tutorialType = TutorialType::find()->where(['code' => $tutorial_type_code])->asArray()->one();

        $sessionDetail = StudentTuitionBooking::get_available_hours($tutor_uuid, json_decode($tutorDetail->working_plan, true), date('Y-m-d', strtotime(date('Y-m-d') . ' next ' . $day)), $tutorialType['min'], $student_uuid, $tutor_tz, $student_tz);

        $time = "00:00";
        if (!empty($sessionDetail)) {
            $time = $sessionDetail[0];
        }
        $session_date = (date("l") == ucfirst($day)) ? date('Y-m-d') : date('Y-m-d', strtotime(date('Y-m-d') . ' next ' . $day));
        $session_date = General::displayDate($session_date);
        $disabled_day = StudentTuitionBooking::disabled_week_day($day);
        $return = ['code' => 200, 'message' => 'Success', 'time' => $time, 'session_when_start' => $session_date, 'day' => $day, 'disabled_day' => $disabled_day];
        Yii::$app->response->format = 'json';
        return $return;
    }

    public function actionRescheduleRequest($id) {
        
        //For new layout We are redirect to below url
        return $this->redirect(['/book/reschedule-request','id' => $id]);

        if (Yii::$app->user->identity->role == 'STUDENT') {

            $this->layout = "student_layout";
            $view_file = 'student_reschedule';
        } else {
            $view_file = 'reschedule';
        }

        if (($modelBS = BookedSession::find()->joinWith(['studentUu', 'instrumentUu', 'tutorUu'])->where(['booked_session.uuid' => $id])->one()) !== null) {

            if ($modelBS->status != "SCHEDULE") {
                throw new \yii\web\HttpException(505, 'This lesson not scheduled. Only scheduled lesson are reschedule.');
            }
            if (date('Y-m-d H:i:s') >= date('Y-m-d H:i:s', strtotime($modelBS->start_datetime))) {
                throw new \yii\web\HttpException(505, 'This lesson time is started. So you can not reschedule it.');
            }

            $model = new StudentTuitionBooking();
            $model->scenario = 'tutor_reschedule';

            $model->attributes = Yii::$app->getRequest()->post('StudentTuitionBooking');

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {

                $session_date_array = explode(',', $model->all_dates);

                if (isset($session_date_array[0]) && !empty($session_date_array[0]) && !empty($model->start_hour)) {

                    $from_start_datetime = $modelBS->start_datetime; 
                    $from_end_datetime = $modelBS->end_datetime;
                    $student_timezone = General::getStudentTimezone($modelBS->student_uuid);
                    $tutor_timezone = General::getTutorTimezone($modelBS->tutor_uuid);
                
                    $to_start_datetime = date('Y-m-d H:i:s', strtotime($session_date_array[0] . " " . $model->start_hour));
                    $to_end_datetime = date('Y-m-d H:i:s', strtotime($to_start_datetime . " +" . $modelBS->session_min . " minutes"));

                    $modelRS = new RescheduleSession;
                    $modelRS->booked_session_uuid = $modelBS->uuid;
                    $modelRS->student_uuid = $model->student_uuid;
                    $modelRS->tutor_uuid = $model->tutor_uuid;
                    $modelRS->from_start_datetime = $modelBS->start_datetime;
                    $modelRS->from_end_datetime = $modelBS->end_datetime;
                    $modelRS->to_start_datetime = General::convertUserToSystemTimezone($to_start_datetime);
                    $modelRS->to_end_datetime = General::convertUserToSystemTimezone($to_end_datetime);
                    $modelRS->request_by = 'STUDENT';
                    $modelRS->requester_uuid = $model->student_uuid;
                    $modelRS->status = 'AWAITING'; // APPROVED / REJECTED / AWAITING
                    $modelRS->referance_number = General::generateRescheduleID();
                    if ($modelRS->save(false)) {

                        $modelBSUpdate = BookedSession::findOne($modelBS->uuid);
                        if (!empty($modelBSUpdate)) {
                            $modelBSUpdate->is_reschedule_request = 1;
                            $modelBSUpdate->save(false);
                        }
                        //send mail
                        $content['student_name'] = $modelBS->studentUu->first_name;
                        $content['student_last_name'] = $modelBS->studentUu->last_name;
                        $content['tutor_name'] = $modelBS->tutorUu->first_name;
                        $content['tutor_last_name'] = $modelBS->tutorUu->last_name;
                        $content['reschedule_number'] = $modelRS->referance_number;
                        $content['instrument_name'] = $modelBS->instrumentUu->name;
                        $content['link'] = Url::to(['/book-music-tution/reschedule', 'id' => $modelBS->uuid], true);
                        
//                        $content['from_date'] = General::displayDate($modelBS->start_datetime);
//                        $content['from_time'] = General::displayTime($modelBS->start_datetime) . ' - ' . General::displayTime($modelBS->end_datetime);
//                        $content['to_date'] = General::displayDate($to_start_datetime);
//                        $content['to_time'] = General::displayTime($to_start_datetime) . ' - ' . General::displayTime($to_end_datetime);
                        $content['from_start_datetime'] = $from_start_datetime;
                        $content['from_end_datetime'] = $from_end_datetime;
                        $content['to_start_datetime'] = $modelRS->to_start_datetime;
                        $content['to_end_datetime'] = $modelRS->to_end_datetime;
                        $content['student_timezone'] = $student_timezone;
                        $content['tutor_timezone'] = $tutor_timezone;
                        //$ownersEmail = General::getOwnersEmails(['OWNER','ADMIN']);
                        Yii::$app->mailer->compose('reschedule_lesson_request_student_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                                ->setFrom(Yii::$app->params['supportEmail'])
                                ->setTo($modelBS->studentUu->email)
                                ->setSubject(ucfirst(strtolower(Yii::$app->name)) . ' Notification: Awaiting Approval – Student Lesson Change Request #' . $modelRS->referance_number)
                                ->send();

                        Yii::$app->mailer->compose('reschedule_lesson_request_tutor_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                                ->setFrom(Yii::$app->params['supportEmail'])
                                ->setTo($modelBS->tutorUu->email)
                                ->setSubject('Awaiting Your Approval – Student booking change #' . $modelRS->referance_number)
                                ->send();

                        Yii::$app->mailer->compose('reschedule_lesson_request_admin_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                                ->setFrom(Yii::$app->params['supportEmail'])
                                ->setTo(Yii::$app->params['adminEmail'])
                                ->setSubject('Awaiting Approval – Student booking change #' . $modelRS->referance_number)
                                ->send();

                        Yii::$app->getSession()->setFlash('success', 'You have successfully sent rescheduled lesson request.');
                        return $this->redirect(['/booked-session/upcoming']);
                    } else {
                        Yii::$app->getSession()->setFlash('danger', 'Reschedule lesson not successfully saved.');
                    }
                } else {
                    Yii::$app->getSession()->setFlash('danger', 'Reschedule lesson not successfully saved.');
                }
            }

            return $this->render($view_file, [
                        'modelBS' => $modelBS,
                        'model' => $model,
            ]);
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    public function actionRescheduleRequestApproval($id) {

        if (!Yii::$app->request->isAjax) {
            throw new \yii\web\HttpException(505, 'You are not authorised to access this page');
        }
        if (($model = RescheduleSession::find()->where(['uuid' => $id, 'status' => 'AWAITING'])->one()) !== null) {

            if (Yii::$app->request->isPost && !empty(Yii::$app->getRequest()->post('is_approval'))) {

                $is_approval = Yii::$app->getRequest()->post('is_approval');

                $return = ['code' => 421, 'message' => 'Something missing.'];
                $status = ($is_approval == "yes") ? "APPROVED" : "REJECTED";

                $modelUpdateBS = BookedSession::find()->joinWith(['studentUu', 'instrumentUu', 'tutorUu'])->where(['booked_session.uuid' => $model->booked_session_uuid])->one();

                $from_start_datetime = $modelUpdateBS->start_datetime; 
                $from_end_datetime = $modelUpdateBS->end_datetime; 
                $student_timezone = General::getStudentTimezone($modelUpdateBS->student_uuid);
                $tutor_timezone = General::getTutorTimezone($modelUpdateBS->tutor_uuid);

                /* if($status == "APPROVED"){

                  $modelTutor = Tutor::findOne($model->tutor_uuid);
                  $tutor_working_plan = json_decode($modelTutor->working_plan, true);
                  $session_date = date('Y-m-d', strtotime($model->to_start_datetime));
                  $selected_time = date('H:i', strtotime($model->to_start_datetime));
                  $session_minute = $modelUpdateBS->session_min;
                  $get_hours = StudentTuitionBooking::get_available_hours($model->tutor_uuid, $tutor_working_plan, $session_date, $session_minute, $model->student_uuid);
                  $flag = (!in_array($selected_time, $get_hours)) ? true : false;
                  } */

                $model->status = $status;
                if (Yii::$app->user->identity->role == 'TUTOR') {
                    $approved_by = Yii::$app->user->identity->role;
                    $approver_uuid = Yii::$app->user->identity->tutor_uuid;
                } else {
                    $approved_by = Yii::$app->user->identity->role;
                    $approver_uuid = Yii::$app->user->identity->admin_uuid;
                }

                if ($status == "APPROVED") {

                    $model->approved_by = $approved_by;
                    $model->approver_uuid = $approver_uuid;
                    $model->approve_datetime = date('Y-m-d H:i:s');
                } else {

                    $model->rejected_by = $approved_by;
                    $model->rejecter_uuid = $approver_uuid;
                    $model->reject_datetime = date('Y-m-d H:i:s');
                }

                if ($model->save(false)) {

                    if (!empty($modelUpdateBS)) {

                        if ($status == "APPROVED") {
                            $modelUpdateBS->start_datetime = $model->to_start_datetime;
                            $modelUpdateBS->end_datetime = $model->to_end_datetime;
                        }
                        $modelUpdateBS->is_reschedule_request = 0;
                        $modelUpdateBS->save(false);

                        //send mail
                        $content['student_name'] = $modelUpdateBS->studentUu->first_name;
                        $content['student_last_name'] = $modelUpdateBS->studentUu->last_name;
                        $content['tutor_name'] = $modelUpdateBS->tutorUu->first_name . ' ' . $modelUpdateBS->tutorUu->last_name;
                        $content['tutor_last_name'] = $modelUpdateBS->tutorUu->last_name;
                        $content['reschedule_number'] = $model->referance_number;
                        $content['status'] = strtolower($status);
                        $content['instrument_name'] = $modelUpdateBS->instrumentUu->name;
                        $content['from_start_datetime'] = $from_start_datetime;
                        $content['from_end_datetime'] = $from_end_datetime;
                        $content['to_start_datetime'] = $model->to_start_datetime;
                        $content['to_end_datetime'] = $model->to_end_datetime;
                        $content['student_timezone'] = $student_timezone;
                        $content['tutor_timezone'] = $tutor_timezone;
                        
                        //$ownersEmail = General::getOwnersEmails(['OWNER','ADMIN']);
                        Yii::$app->mailer->compose('reschedule_lesson_request_approved_rejected_student_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                                ->setFrom(Yii::$app->params['supportEmail'])
                                ->setTo($modelUpdateBS->studentUu->email)
                                //->setBcc($ownersEmail)
                                ->setSubject(ucfirst(strtolower(Yii::$app->name)) . ' Notification - Your booking change request has been ' . $content['status'] . ' by your tutor: #' . $model->referance_number)
                                ->send();

                        Yii::$app->mailer->compose('reschedule_lesson_request_approved_rejected_tutor_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                                ->setFrom(Yii::$app->params['supportEmail'])
                                ->setTo($modelUpdateBS->tutorUu->email)
                                ->setSubject(ucfirst(strtolower(Yii::$app->name)) . ' Notification - Student booking change request has been ' . $content['status'] . ' by you: #' . $model->referance_number)
                                ->send();

                        Yii::$app->mailer->compose('reschedule_lesson_request_approved_rejected_admin_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                                ->setFrom(Yii::$app->params['supportEmail'])
                                ->setTo(Yii::$app->params['adminEmail'])
                                ->setSubject(ucfirst(strtolower(Yii::$app->name)) . ' Notification - Student booking change request has been ' . $content['status'] . ' by the tutor: #' . $model->referance_number)
                                ->send();

                        $return = ['code' => 200, 'message' => 'Reschedule request successfully ' . strtolower($status)];
                    }
                }

                Yii::$app->response->format = 'json';
                return $return;
            }

            return $this->renderAjax('_reschedule_approval_form', [
                        'model' => $model,
            ]);
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    public function actionGetTutorAvailableHours($tutor_uuid, $day, $date, $session_minute, $typ, $student_uuid) {


        $system_tz = Yii::$app->params['timezone'];

        if ($typ == 'day') {
            $session_date = (date("l") == ucfirst($day)) ? date('d-m-Y') : date('d-m-Y', strtotime(date('d-m-Y') . ' next ' . $day));
        } else {
            $session_date = $date;
        }

        /* $session_date = (date("l") == ucfirst($day)) ? $date : date('d-m-Y', strtotime($date));
          if(!$date) {
          $session_date = (date("l") == ucfirst($day)) ? date('d-m-Y') : date('d-m-Y', strtotime(date('d-m-Y') . ' next ' . $day));
          } */
        $tutor = Tutor::findOne($tutor_uuid);

        $tutor_user_model = User::find()->where(['tutor_uuid' => $tutor_uuid])->one();
        $student_user_model = User::find()->where(['student_uuid' => $student_uuid])->one();
        $tutor_tz = (!empty($tutor_user_model)) ? $tutor_user_model->timezone : Yii::$app->params['timezone'];
        $student_tz = (!empty($student_user_model)) ? $student_user_model->timezone : Yii::$app->params['timezone'];

        if (empty($day)) {
            $result = ['code' => 402, 'message' => 'data not found.', 'data' => $day];
        } else {
            $tutor_plan = json_decode($tutor->working_plan, true);

            $time = StudentTuitionBooking::get_tutor_available_hours($tutor_uuid, $tutor_plan, $session_date, $session_minute, $student_uuid, $tutor_tz, $student_tz);
            $return = ['code' => 200, 'session_when_start' => $session_date, 'time' => $time, 'day' => $day];
        }
        Yii::$app->response->format = 'json';
        return $return;
    }

    public function actionGetCurrentUserSelectedTimeInfo() {

        $sel_date = Yii::$app->getRequest()->get('sel_date');

        $user_tz = Yii::$app->user->identity->timezone;

        $date_info = new \DateTime($sel_date, new \DateTimeZone($user_tz));
        $date = $date_info->format('d-m-Y');
        $week_day = $date_info->format('l');
        $week_day_no = $date_info->format('w');

        Yii::$app->response->format = 'json';
        return ['date' => $date, 'week_day' => strtolower($week_day), 'week_day_no' => $week_day_no];
    }

}
