<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\TutorialType;
use app\models\PremiumStandardPlanRate;

?>

<div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">

    <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Booking ID</th>
                <th>Customer ID</th>
                <th>Customer Name</th>
                <th>Booking Date</th>
                <th>Tutorial Type</th>
                <th>Payment Term</th>
                <th>Package Start Date</th>
                <th>Package End Date</th>
                <th>Tutor Name</th>
                <th>Instrument</th>
                <th>Online Content Subscription</th>
                <th>Package Gross Price($)</th>
                <th>GST($)</th>
                <th>Promo Code</th>
                <th>NSW Creative Kids Voucher Code</th>
                <th>Kid's Name</th>
                <th>Kid's Date Of Birth</th>
                <th>Discount Applied($)</th>
                <th>Payment Amount($)</th>
                <th>Income($)</th>
                <th>Order ID</th>
                <th>Plan</th>
                
            </tr>
        </thead>

        <tbody>
            <?php
            if (!empty($tuitionBookingList)) {
                foreach ($tuitionBookingList as $key => $value) {  //echo "<pre>";print_r($value);echo "</pre>";exit;
                $student_name = $value['studentUu']['first_name']. ' ' . $value['studentUu']['last_name'];
                $tutor_name = $value['tutorUu']['first_name']. ' ' .$value['tutorUu']['last_name']; 
                $tutorial_type_model = TutorialType::getTutorialTypeByCode($value['tutorial_type_code']);
                $payment_term_model = \app\models\PaymentTerm::getPaymentTermByCode($value['payment_term_code']);
                $package_start_end = \app\models\StudentTuitionBooking::bookingFirstLastSession($value['uuid']);
                                $plan_model = PremiumStandardPlanRate::find()->joinWith(['onlineTutorialPackageTypeUu'])->where(['premium_standard_plan_rate.uuid'=>$value['premium_standard_plan_rate_uuid']])->asArray()->one();

                    ?>
                    <tr>
                        <td><?= $key + 1; ?></td>
                        <td><a href="<?php echo Url::to(['book-music-tution/view', 'id' => $value['uuid']]); ?>"><?= "#".$value['booking_id'] ?></a></td>
                        <td><?= $value['studentUu']['enrollment_id'] ?></td>
                        <td><?= (!empty($student_name)) ? $student_name : ''; ?></td>
                        <td><?= \app\libraries\General::displayDate($value['booking_datetime']); ?></td>
                        <td><?=  (isset($tutorial_type_model['description'])) ? $tutorial_type_model['description'] : ''; ?></td>
                        <td><?= (isset($payment_term_model['term'])) ? $payment_term_model['term'] : ''; ?></td>
                        <td><?= $package_start_end['start'] ?></td>
                        <td><?= $package_start_end['end'] ?></td>
                        <td><?= (!empty($tutor_name)) ? $tutor_name : ''; ?></td>
                        <td><?= $value['instrumentUu']['name'] ?></td>
                        <td><?= (!empty($value['student_monthly_subscription_uuid'])) ? "Yes" : "No" ?></td>
                        <td><?= $value['price'] ?></td>
                        <td><?= $value['gst'] ?></td>
                        <td><?= $value['couponUu']['code'] ?></td>
                        <td><?= $value['creative_kids_voucher_code'] ?></td>
                        <td><?= $value['kids_name'] ?></td>
                        <td><?= \app\libraries\General::displayDate($value['kids_dob']); ?></td>
                        <td><?= $value['discount'] ?></td>
                        <td><?= $value['grand_total'] ?></td>
                        <td><?= ($value['grand_total']) ?></td>
                        <td><a target="_blank" href="<?php echo Yii::$app->params['media']['invoice']['url'] . $value['orderUu']['invoice_file']; ?>"><?= (!empty($value['orderUu']['order_id'])) ? "#".$value['orderUu']['order_id'] : ''; ?></a></td>
                        <td><?= $plan_model['onlineTutorialPackageTypeUu']['display_name'] ?></td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>

</div>
