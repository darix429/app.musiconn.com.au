<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\ArrayHelper;
use app\models\OnlineTutorialPackageTutorwise;

$this->title = Yii::t('app', 'MUSICONN+ Online Tutorial Packages');
$this->params['breadcrumbs'][] = $this->title;


$this->registerJs(
        '
            
function change_option(e){

var price_id = e.id;
var res = price_id.split("_");
var gst_id = res[0]+"_gst";
var total_price_id = res[0]+"_total_price";
$("#"+gst_id).val(e.value);
$("#"+total_price_id).val(e.value);

}

    
', View::POS_END
);
?>

<style>
    .form-control-label
    {
        border : 0;
        margin-left: -6%;
    }
    .semi-bold {
        font-weight: 900;
    }
    #tutor_table_parent_div{
        padding-right: 15px;
    padding-left: 15px;
    }
   .form-group
    {
        margin-bottom: 0;
    }
    
</style>

<div class="col-xl-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left"><?= $model['first_name'].' '.$model['last_name'];?>'s Online Tutorial Package List</h2>
           <div class="actions panel_actions float-right">
<?php if (Yii::$app->user->identity->role != 'TUTOR') { ?>
                <a href="<?= Yii::$app->getUrlManager()->createUrl(['/tutor/index/']) ?>" class="btn btn-info btn-icon "><i class="fa fa-arrow-left text-white"></i> <span>Back</span></a>
               <?php } ?>             </div>
        </header>
        <div class="content-body"> 

            <div class=""></div>
            <div class="clearfix"></div>
            <div class="row" id="tutor_table_parent_div">
                <div class="col-lg-12 col-md-12 col-12">
                    <?= Html::hiddenInput('tutor_uuid', $model['uuid']);?>
                    <br><br>
                      <div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">
    <table  class="table table-striped dt-responsive display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Plan Name</th>
                <th style="text-align:right">Price</th>
                <th style="text-align:right">GST</th>
                
                <th style="text-align:right">Total Price</th>
               
            </tr>
        </thead>

        <tbody>

                    <?php
                    //echo "<pre>";
                    //print_r($plans);
                    //exit;
                    if (!empty($plans)) {
                        foreach ($plans as $key => $value) {

                            $tutor_plan = OnlineTutorialPackageTutorwise::find()->where(['tutor_uuid' => $model['uuid'], 'premium_standard_plan_rate_uuid' => $value['uuid']])->asArray()->one();
                            //echo "<pre>";
                            //print_r($value);exit;
                            ?>
                    <tr>
                               <td><?= $key + 1; ?></td>
                                <td><?= $value['name']; ?></td>
                                <td align="right"><?= $value['price']; ?></td>
                                <td align="right"><?= $value['gst']; ?></td>
                               <td align="right"><?= $value['total_price']; ?></td>
                                
                            </tr>
                           
                            <?php
                        }
                    }
                    ?>
                     </tbody>
    </table>

</div>


                </div>

            </div>
        </div>
    </section>
</div>


