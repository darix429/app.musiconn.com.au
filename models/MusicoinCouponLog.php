<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "musicoin_coupon_log".
 *
 * @property string $uuid
 * @property string $student_uuid
 * @property string $order_uuid
 * @property string $student_musicoin_package_uuid
 * @property string $transaction_uuid
 * @property string $created_at
 *
 * @property Student $studentUu
 * @property StudentMusicoinPackage $studentMusicoinPackageUu
 */
class MusicoinCouponLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'musicoin_coupon_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'student_uuid', 'student_musicoin_package_uuid'], 'required'],
            [['uuid', 'student_uuid', 'order_uuid', 'student_musicoin_package_uuid', 'transaction_uuid'], 'string'],
            [['created_at'], 'safe'],
            [['uuid'], 'unique'],
            [['student_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['student_uuid' => 'student_uuid']],
            [['student_musicoin_package_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => StudentMusicoinPackage::className(), 'targetAttribute' => ['student_musicoin_package_uuid' => 'uuid']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'student_uuid' => Yii::t('app', 'Student Uuid'),
            'order_uuid' => Yii::t('app', 'Order Uuid'),
            'student_musicoin_package_uuid' => Yii::t('app', 'Student Musicoin Package Uuid'),
            'transaction_uuid' => Yii::t('app', 'Transaction Uuid'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentUu()
    {
        return $this->hasOne(Student::className(), ['student_uuid' => 'student_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentMusicoinPackageUu()
    {
        return $this->hasOne(StudentMusicoinPackage::className(), ['uuid' => 'student_musicoin_package_uuid']);
    }
}
