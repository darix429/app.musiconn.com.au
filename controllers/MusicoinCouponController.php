<?php

namespace app\controllers;

use Yii;
use app\models\MusicoinCoupon;
use app\models\MusicoinCouponSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * MusicoinCouponController implements the CRUD actions for MusicoinCoupon model.
 */
class MusicoinCouponController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                        [
                        'actions' => ['index'],
                        'allow' => Yii::$app->user->can('/musicoin-coupon/index'),
                        'roles' => ['@'],
                    ],
			[
                        'actions' => ['coupon-enabled'],
                        'allow' => Yii::$app->user->can('/musicoin-coupon/coupon-enabled'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['coupon-disabled'],
                        'allow' => Yii::$app->user->can('/musicoin-coupon/coupon-disabled'),
                        'roles' => ['@'],
                    ],
			[
                        'actions' => ['create'],
                        'allow' => Yii::$app->user->can('/musicoin-coupon/create'),
                        'roles' => ['@'],
                    ],
			[
                        'actions' => ['update'],
                        'allow' => Yii::$app->user->can('/musicoin-coupon/update'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['delete'],
                        'allow' => Yii::$app->user->can('/musicoin-coupon/delete'),
                        'roles' => ['@'],
                    ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MusicoinCoupon models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MusicoinCouponSearch();
        $searchModel->status = 'ENABLED';
	$couponList = $searchModel->search(Yii::$app->request->queryParams, true);
	if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_coupon_list', [
                        'couponList' => $couponList,
            ]);
        } else {
            return $this->render('index', [
                        'couponList' => $couponList,
                        'searchModel' => $searchModel,
            ]);
        }
    }

    /**
     * Displays a single MusicoinCoupon model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MusicoinCoupon model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MusicoinCoupon();
        $model->scenario = 'coupon_create';

        $model->attributes = Yii::$app->getRequest()->post('Coupon');
        $model->created_at = $model->updated_at = date('Y-m-d H:i:s', time());

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
            $model->start_date = date('Y-m-d 23:59:59', strtotime($model->start_date)); 
            $model->end_date = date('Y-m-d 23:59:59', strtotime($model->end_date));
            $model->created_at = $model->updated_at = date('Y-m-d H:i:s', time());
            if($model->save(false)){
                Yii::$app->session->setFlash('success', Yii::t('app', 'Musicoin Coupon created successfully.'));
                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MusicoinCoupon model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        try {
            $model = $this->findModel($id);
            
            $model->attributes = Yii::$app->getRequest()->post('Coupon');
            $model->updated_at = date('Y-m-d H:i:s', time());

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
                $model->start_date = date('Y-m-d 23:59:59', strtotime($model->start_date)); 
                $model->end_date = date('Y-m-d 23:59:59', strtotime($model->end_date));
                $model->updated_at = date('Y-m-d H:i:s', time());
                if($model->save(false)){
                    Yii::$app->session->setFlash('success', Yii::t('app', 'Musicoin Coupon updated successfully.'));
                    return $this->redirect(['index']);
                }
            }
            return $this->render('update', [
                        'model' => $model,
            ]);
        } catch (\yii\base\Exception $exception) {
            throw new \yii\web\HttpException(505, $exception->getMessage());
        }
    }

    /**
     * Deletes an existing MusicoinCoupon model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete()
    {
	$id = Yii::$app->getRequest()->post('id');
	if ((($model = MusicoinCoupon::findOne($id)) !== null))
	{
		$model->delete();
		$return = ['code' => 200, 'message' => 'Promo code deleted successfully.'];
        } else {

            $return = ['code' => 404, 'message' => 'The requested page does not exist.'];
        }
	Yii::$app->response->format = 'json';
        return $return;
    }
    
    public function actionCouponDisabled($id) {
        
        if (($coupon = MusicoinCoupon::findOne($id)) !== null) {

            if ($coupon->status == "ENABLED") {
                
                    $coupon->status = 'DISABLED';
                    $coupon->save(false);

                    $return = ['code' => 200, 'message' => 'Promo code disabled successfully.'];
            } else {
                $return = ['code' => 421, 'message' => 'Promo code not enabled.'];
            }
        } else {
            $return = ['code' => 404, 'message' => 'Record does not exist.'];
        }

        Yii::$app->response->format = 'json';
        return $return;
    }
    
    public function actionCouponEnabled($id) {

        if (($coupon = MusicoinCoupon::findOne($id)) !== null) {

            if ($coupon->status == "DISABLED") {
                
                    $coupon->status = 'ENABLED';
                    $coupon->save(false);

                    $return = ['code' => 200, 'message' => 'Promo code enabled successfully.'];
            } else {
                $return = ['code' => 421, 'message' => 'Promo code not disabled.'];
            }
        } else {
            $return = ['code' => 404, 'message' => 'Record does not exist.'];
        }

        Yii::$app->response->format = 'json';
        return $return;
    }

    /**
     * Finds the MusicoinCoupon model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return MusicoinCoupon the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MusicoinCoupon::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
