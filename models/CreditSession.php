<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%credit_session}}".
 *
 * @property string $uuid
 * @property string $booked_session_uuid
 * @property string $student_uuid
 * @property string $instrument_uuid
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $tutor_uuid
 * @property string $tutorial_type_code
 * @property string $credited_by
  * @property string $credited_note
 *
 * @property BookedSession $bookedSessionUu
 * @property Instrument $instrumentUu
 * @property Student $studentUu
 * @property Tutor $tutorUu
 */
class CreditSession extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%credit_session}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'booked_session_uuid', 'student_uuid', 'instrument_uuid', 'tutor_uuid', 'tutorial_type_code', 'credited_by'], 'required'],
            [['uuid', 'booked_session_uuid', 'student_uuid', 'instrument_uuid', 'status', 'tutor_uuid','booking_uuid','credited_note'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['tutorial_type_code'], 'string', 'max' => 4],
            [['uuid'], 'unique'],
            [['booked_session_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => BookedSession::className(), 'targetAttribute' => ['booked_session_uuid' => 'uuid']],
            [['instrument_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Instrument::className(), 'targetAttribute' => ['instrument_uuid' => 'uuid']],
            [['student_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['student_uuid' => 'student_uuid']],
            [['tutor_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Tutor::className(), 'targetAttribute' => ['tutor_uuid' => 'uuid']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'booked_session_uuid' => Yii::t('app', 'Booked Session Uuid'),
            'student_uuid' => Yii::t('app', 'Student'),
            'instrument_uuid' => Yii::t('app', 'Instrument Uuid'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'tutor_uuid' => Yii::t('app', 'Tutor Uuid'),
            'tutorial_type_code' => Yii::t('app', 'Tutorial Type Code'),
            'credited_by' => Yii::t('app', 'Credited By'),
            'credited_note' => Yii::t('app', 'Credited Note')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookedSessionUu()
    {
        return $this->hasOne(BookedSession::className(), ['uuid' => 'booked_session_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstrumentUu()
    {
        return $this->hasOne(Instrument::className(), ['uuid' => 'instrument_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentUu()
    {
        return $this->hasOne(Student::className(), ['student_uuid' => 'student_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTutorUu()
    {
        return $this->hasOne(Tutor::className(), ['uuid' => 'tutor_uuid']);
    }
    
    public function getStudentCreditSessionTutorialType($student_uuid) {
        
        $return = []; 
        $model = CreditSession::find()->where(['student_uuid' => $student_uuid, 'status' => 'PENDING'])->asArray()->all();
        if(!empty($model)){
            
            $code = array_column($model,'tutorial_type_code');
            $return = TutorialType::find()->where(['in', 'code', $code])->andWhere(['status' => 'ENABLED'])->asArray()->all();
            
        }
        return $return;
    }
    
    public static function getStudentCreditSessionCount($student_uuid) {
        
        return CreditSession::find()->where(['student_uuid' => $student_uuid, 'status' => 'PENDING'])->count();
    }

    public function addStudentFreeSession($student_uuid) {
        $studentModel = Student::find()->where(['student_uuid' => $student_uuid, 'status' => 'ENABLED'])->one();
        if($studentModel) {
            $model = new \app\models\CreditSession();
            $model->student_uuid = $student_uuid;
            $model->tutorial_type_code = "30W";
            $model->credited_by = "OWNER";
            $model->credited_note = "Free Credit";
            $model->save(false);
            return $model;
        }
    }
}
