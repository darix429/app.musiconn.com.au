<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Settings */
/* @var $form yii\widgets\ActiveForm */
// echo "<pre>";
// print_r($model->getErrors());
// echo "</pre>";

$this->registerJs(
        "
       

         moment.tz.setDefault('America/Los_Angeles');
        var todaysDate = moment().format('D MMM YYYY');
        alert(todaysDate)
        $('#session_when_start').datepicker({ format: 'dd M yyyy', startDate: todaysDate,locale: 'us' });


        ", \yii\web\View::POS_END
);
?>
<div class="test-form">

    <?php /*
    $form->field($model, 'session_when_start', ['template' => "{label}\n{input}\n{hint}\n{error}",
        'labelOptions' => ['class' => 'control-label']])->textInput(
            ['class' => 'form-control when-datepicker', 'data-date-format' => 'dd-mm-yyyy', 'data-disabled-days' => '', 'data-start-date' => '0d', 'onchange' => 'date_change(this);', 'readonly' => true])->label(false)
    */ ?>
    <input type="text" name="session_when_start" id="session_when_start" class="form-control new-when-datepicker" data-date-format="dd-mm-yyyy">

</div>
