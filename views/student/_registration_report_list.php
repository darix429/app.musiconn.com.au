<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\libraries\General;
?>
<div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">
    <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Student ID</th>
                <th>Registration Date</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Birth Year</th>
                <th>Phone</th>
                <th>Street</th>
                <th>City</th>
                <th>State</th>
                <th>Post Code</th>
                <th>Musiconn Campaign</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (!empty($studentList)) {
                foreach ($studentList as $key => $value) {
                    ?>
                    <tr>
                        <td><?= $key + 1; ?></td>
                        <td><?= $value['enrollment_id']; ?></td>
                        <td><?= General::displayDate(date('Y-m-d', $value['created_at'])); ?></td>
                        <td><?= $value['first_name']; ?></td>
                        <td><?= $value['last_name']; ?></td>
                        <td><?= $value['email']; ?></td>
                        <td><?= $value['birthyear']; ?></td>
                        <td><?= $value['phone_number']; ?></td>
                        <td><?= $value['street']; ?></td>
                        <td><?= $value['city']; ?></td>
                        <td><?= $value['state']; ?></td>
                        <td><?= $value['postal_code']; ?></td>
                        <td><?= $value['referer_source']; ?></td>
                        <td align="center">
                            <?php
                            if ($value['status'] == 'ENABLED') {
                                $e_class = "badge-primary";
                                $pointer = 'style="cursor:pointer;color:#fff"';
                                $e_link_class = "student_disabled_link";
                            } elseif ($value['status'] == 'DISABLED') {
                                $e_class = "badge-secondary";
                                $pointer = 'style="cursor:pointer;color:#fff"';
                                $e_link_class = "student_enabled_link";
                            } elseif ($value['status'] == 'INACTIVE') {
                                $e_class = "badge-orange";
                                $pointer = '';
                                $e_link_class = "";
                            } else {
                                $e_class = "badge-danger";
                                $pointer = '';
                                $e_link_class = "";
                            }
                            ?>
                            <span class="badge badge-pill <?= $e_class ?> <?= $e_link_class ?>"  data-uuid="<?= $value['student_uuid']; ?>" <?= $pointer ?>><?= $value['status']; ?></span>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>

</div>
