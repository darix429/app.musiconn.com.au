<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Url;
use app\models\StudentMonthlySubscription;

$this->title = Yii::t('app', 'Monthly Subscription');
$this->params['breadcrumbs'][] = $this->title;

/* if(isset($step_data['step_1']['monthlysubscriptionfee_uuid']) && $step_data['step_1']['monthlysubscriptionfee_uuid'] != ""){ //echo "yes"; exit;
  $model->monthlysubscriptionfee_uuid = $step_data['step_1']['monthlysubscriptionfee_uuid'];
  $model->instrumentcheckbox = $model->instrument_uuid;
  } */
//echo "<pre>";
//print_r($step_data);
//echo "</pre>";
$student_uuid = Yii::$app->user->identity->student_uuid;
$monthlySubscriptionDetail = StudentMonthlySubscription::find()->where(['student_uuid' => $student_uuid])->one();

$this->registerJsFile('http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js');
$this->registerJs(
        '
//    $("input[name=\"StudentMonthlySubscription[monthlysubscriptionfeecheckbox]\"]").on("ifClicked", function (event) { //alert(123); 
//        $("#studentmonthlysubscription-monthlysubscriptionfeecheckbox").val(this.value);
//
//        //$(".monthly_sub_fee_select_btn").addClass("d-none"); 
//        //$(this).closest("tr.monthly_sub_fee_tr").find(".monthly_sub_fee_select_btn").removeClass("d-none"); 
//    });
    
    $(document).on("click",".monthly_sub_fee_select_btn", function(){ 
        var id = $(".monthly_sub_fee_select_btn").data("id"); 
        //$(".payment").show(); 
        
    });

        ', View::POS_END
);
?>
<style>
    .field-studentmonthlysubscription-instrumentcheckbox{
        margin-bottom: 0px !important;
    }
</style>
<div class="col-xl-12">
    <section class="box ">
        <!--<header class="panel_header bg-primary ">
            <h2 class="title float-left text-white">Select Monthly Subscription</h2>
            <div class="actions panel_actions float-right">

            </div>
        </header>-->
        <div class="content-body padding-imp-30">    
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12">

                    <?php
                    $form = ActiveForm::begin([
                                'action' => ['index'],
                                'id' => 'frm_step_1_else',
                                'enableClientValidation' => true,
//                              'enableAjaxValidation' => true,
                                'validateOnChange' => true,
                                'options' => ['class' => 'form-horizontal']]);
                    ?>
                    <div class="col-lg-12 col-md-12 col-12 booking_search_data">
                        <div class="tab-pane fade show active " >
                            <input type="hidden" name="step_else" value="1">
                            <input type="hidden" name="monthlysubscriptionfee_uuid" value="<?= $subscriptionFeeList->uuid; ?>">

                            <table class="table table-striped table-hover">

                                <tr class="unread monthly_sub_fee_tr" >
                                    <td class="" style="width: 10%">
                                        <?php
//                                    $model->monthlysubscriptionfeecheckbox = $subscriptionFeeList->uuid;
//                                        echo $form->field($model, 'monthlysubscriptionfeecheckbox')->radio([
//                                            'label' => '',
//                                            'template' => '{input}{error} ',
//                                            'class' => 'skin-square-red float-left monthlysubscriptionfee_checkbox', 'value' => $subscriptionFeeList->uuid])->label('');
                                        ?>
                                    </td>

                                    <td class="open-view">
                                        <!-- start -->
                                        <div class="pricing-tables">

                                            <div class="row">
                                                <div class="col-md-2 col-lg-2">
                                                </div>

                                                <div class="col-md-7 col-lg-7">

                                                    <div class="price-pack" align="center">

                                                        <div class="head">
                                                            <h3><?= $subscriptionFeeList->name; ?></h3>
                                                        </div> 
                                                        <ul class="item-list list-unstyled">
                                                            <li><strong>Price - </strong> <?= $subscriptionFeeList->price; ?></li>
                                                            <li><strong>Tax - </strong> <?= $subscriptionFeeList->tax; ?></li>
                                                            <?php if(!empty($monthlySubscriptionDetail)) { ?>
                                                            <li><strong>Start Date</strong> <?= date('Y-m-d', strtotime($monthlySubscriptionDetail->start_datetime)); ?> </li>
                                                            <li><strong>Renew Date</strong>  <?= date('Y-m-d', strtotime('+1 days', strtotime($monthlySubscriptionDetail->end_datetime))); ?></li>
                                                            <?php } ?>
                                                        </ul>
                                                        <div class="price">
                                                            <h3><span class="symbol">$</span><?= $subscriptionFeeList->total_price; ?></h3>
                                                            <h4>per month</h4>
                                                        </div>
                                                        <?php
                                                        if(!empty($monthlySubscriptionDetail)) {
                                                            $end_date = strtotime($monthlySubscriptionDetail->end_datetime);
                                                            $currentDate = date("Y-m-d");
                                                            $days_ago = date('Y-m-d', strtotime('-10 days', $end_date)); 
                                                            if ($currentDate >= $days_ago) {
                                                                ?>
                                                                <button class="btn btn-purple  btn-icon bottom15 right15 monthly_sub_fee_select_btn" data-id="<?= $subscriptionFeeList->uuid; ?>">
                                                                    <span> BUY </span>
                                                                </button>
                                                        <?php 
                                                            } 
                                                        } 
                                                        ?>
                                                        <!--<td class="open-view">
                                                            <?= Html::submitButton('Select', ['class' => "btn btn-orange btn-md monthly_sub_fee_select_btn"]) ?>
                                                        </td>-->
                                                        
                                                    <!--<a href="<?= Url::to(['/monthly-subscription/credit-card/']) ?>">
                                                        <button class="btn btn-purple  btn-icon bottom15 right15" data-id="<?= $subscriptionFeeList->uuid; ?>">
                                                            <span> BUY </span>
                                                        </button>
                                                    </a>-->

                                                    </div>
                                                </div>

                                            </div>
                                            <!-- end -->
                                    </td>

                                    <!--<td class="open-view">
                                    <?= Html::submitButton('Select', ['class' => "btn btn-orange btn-md monthly_sub_fee_select_btn1"]) ?>
                                    </td>-->
                                </tr>

                            </table>


                        </div>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </section>

    <!--<section class="box payment" style="display:none">
        <header class="panel_header">
            <h2 class="title float-left">Payment</h2>
            <div class="actions panel_actions float-right">

            </div>
        </header>
    </section>-->


</div>
