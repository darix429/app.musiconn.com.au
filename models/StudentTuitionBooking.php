<?php

namespace app\models;

use Yii;
use app\libraries\General;
use kartik\mpdf\Pdf;
use yii\helpers\FileHelper;
use app\models\StudentTuitionStandardBookingLockingPeriod;
use app\models\OnlineTutorialPackageTutorwise;

/**
 * This is the model class for table "{{%student_tuition_booking}}".
 *
 * @property string $uuid
 * @property string $booking_id
 * @property string $booking_datetime
 * @property string $student_uuid
 * @property string $tutor_uuid
 * @property string $instrument_uuid
 * @property string $tutorial_name
 * @property string $tutorial_description
 * @property string $tutorial_type_code
 * @property int $tutorial_min
 * @property string $payment_term_code
 * @property int $total_session
 * @property int $session_length_day
 * @property string $price
 * @property string $gst
 * @property string $gst_description
 * @property string $total_price
 * @property string $status
 * @property string $created_at
 *
 * @property Instrument $instrumentUu
 * @property Student $studentUu
 * @property Tutor $tutorUu
 */
class StudentTuitionBooking extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return '{{%student_tuition_booking}}';
    }

    public $instrumentcheckbox;
    public $onlinetutorial_uuid;
    public $onlinetutorialcheckbox;
    public $onlinetutorialtype;
    public $tutorcheckbox;
    public $session_week_day;
    public $session_when_start;
    public $start_hour;
    public $all_dates;
    public $session_text_info;
    public $cc_name;
    public $cc_number;
    public $cc_cvv;
    public $cc_month_year;
    public $cc_uuid;
    public $cc_checkbox;
    public $monthly_sub_uuid;
    public $monthly_sub_total;
    public $monthly_sub_checkbox;
    //public $discount;
    public $cart_total;
    public $coupon_code;
    //public $creative_kids_voucher_code;
    public $tutorial_type_code_checkbox;
    public $term_condition_checkbox;
    public $AVAILABILITIES_TYPE_FIXED = 'fixed';
    public $option;
    public $option_total_price;

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            //html tag filter
            [['cc_name', 'creative_kids_voucher_code', 'kids_name', 'coupon_code'], 'match', 'pattern' => \app\libraries\General::validTextPattern(), 'message' => 'Invalid charecters used.'],
            [['instrument_uuid'], 'required', 'on' => 'step_1', 'message' => 'Instrument cannot be blank. Select any one Instrument.'],
            [['instrument_uuid'], 'required', 'on' => 'credit_step_1', 'message' => 'Instrument cannot be blank. Select any one Instrument.'],
            [['tutorial_type_code'], 'required', 'on' => 'credit_step_3', 'message' => 'Tutorial type cannot be blank. Select any one type.'],
            [['onlinetutorial_uuid'], 'required', 'on' => 'step_3', 'message' => 'Online Tutorial cannot be blank. Select any one Online Tutorial.'],
            [['tutor_uuid'], 'required', 'on' => 'step_2', 'message' => 'Tutor cannot be blank. Select any one Tutor.'],
            [['tutor_uuid'], 'required', 'on' => 'credit_step_2', 'message' => 'Tutor cannot be blank. Select any one Tutor.'],
            [['session_week_day'], 'required', 'on' => 'step_4', 'message' => 'Select any one week day.'],
            [['start_hour'], 'required', 'on' => 'step_4', 'message' => 'Select hour.'],
            [['all_dates'], 'required', 'on' => 'step_4', 'message' => ''],
            [['session_text_info'], 'safe', 'on' => 'step_4'],
            [['session_week_day'], 'required', 'on' => 'credit_step_4', 'message' => 'Select any one week day.'],
            [['start_hour'], 'required', 'on' => 'credit_step_4', 'message' => 'Select hour.'],
            [['all_dates'], 'required', 'on' => 'credit_step_4', 'message' => ''],
            [['session_text_info'], 'safe', 'on' => 'credit_step_4'],
            [['session_when_start'], 'required', 'on' => 'credit_step_4', 'message' => 'Select start date.'],
            [['cc_checkbox', 'cc_uuid', 'monthly_sub_uuid', 'monthly_sub_total', 'discount', 'monthly_sub_checkbox', 'coupon_code', 'creative_kids_voucher_code', 'kids_name', 'kids_dob'], 'safe', 'on' => 'step_5'],
            [['cc_name', 'cc_number', 'cc_cvv', 'cc_month_year', 'cart_total'], 'required', 'on' => 'step_5',],
            //[['cc_month'],'number','min'=>date("m"), 'on' => 'step_5', ],
            [['cc_month_year'], 'date', 'format' => 'php:Y-m', 'on' => 'step_5',],
            [['term_condition_checkbox'], 'required', 'on' => 'step_5', 'requiredValue' => 1, 'message' => 'Accept Terms and Condition'],
            [['cc_number'], 'integer', 'on' => 'step_5',],
            [['cc_number'], 'string', 'max' => 16, 'on' => 'step_5',],
            [['cc_number'], 'string', 'min' => 14, 'on' => 'step_5',],
            [['cc_cvv'], 'string', 'min' => 3, 'on' => 'step_5',],
            [['cc_cvv'], 'string', 'max' => 5, 'on' => 'step_5',],
            [['creative_kids_voucher_code', 'kids_name', 'kids_dob'], 'creativeCodeCriteria', 'on' => 'step_5'],
            ['discount', 'default', 'value' => 0],
            ['cart_total', 'default', 'value' => 0],
            [['session_when_start'], 'required', 'on' => 'step_4', 'message' => 'Select start date.'],
            [['uuid', 'booking_id', 'student_uuid', 'tutor_uuid', 'instrument_uuid', 'tutorial_name', 'tutorial_description', 'gst_description', 'status'], 'string'],
            [['booking_id', 'student_uuid', 'tutorial_name', 'tutorial_description', 'tutorial_type_code', 'tutorial_min', 'payment_term_code', 'total_session', 'session_length_day', 'gst_description'], 'required'],
            [['booking_datetime', 'created_at', 'instrumentcheckbox', 'onlinetutorialcheckbox', 'onlinetutorialtype', 'tutorial_type_code_checkbox', 'tutorcheckbox', 'session_week_day','tutor_count'], 'safe'],
            [['tutorial_min', 'total_session', 'session_length_day'], 'default', 'value' => null],
            [['tutorial_min', 'total_session', 'session_length_day'], 'integer'],
            [['price', 'gst', 'total_price'], 'number'],
            [['tutorial_type_code'], 'string', 'max' => 4],
            [['payment_term_code'], 'string', 'max' => 1],
            [['instrument_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Instrument::className(), 'targetAttribute' => ['instrument_uuid' => 'uuid']],
            [['student_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['student_uuid' => 'student_uuid']],
            [['tutor_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Tutor::className(), 'targetAttribute' => ['tutor_uuid' => 'uuid']],
            [['order_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_uuid' => 'uuid']],
            [['student_monthly_subscription_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => StudentMonthlySubscription::className(), 'targetAttribute' => ['student_monthly_subscription_uuid' => 'uuid']],
            [['coupon_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Coupon::className(), 'targetAttribute' => ['coupon_uuid' => 'uuid']],
            //scenario = tutor_reschedule
            [['tutor_uuid', 'student_uuid', 'tutorial_type_code', 'session_when_start'], 'required', 'on' => 'tutor_reschedule'],
            [['session_week_day'], 'required', 'on' => 'tutor_reschedule', 'message' => 'Select any one week day.'],
            [['start_hour'], 'required', 'on' => 'tutor_reschedule', 'message' => 'Select hour.'],
            [['all_dates'], 'required', 'on' => 'tutor_reschedule', 'message' => ''],
            [['session_text_info'], 'safe', 'on' => 'tutor_reschedule'],
            [['session_when_start'], 'required', 'on' => 'tutor_reschedule', 'message' => 'Select start date.'],
            [['option_total_price', 'plan_rate_option', 'premium_standard_plan_rate_uuid'], 'safe'],
            [['creative_kids_voucher_code',], 'trim', 'on' => 'step_5'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'booking_id' => Yii::t('app', 'Booking ID'),
            'booking_datetime' => Yii::t('app', 'Booking Date'),
            'student_uuid' => Yii::t('app', 'Student Name'),
            'tutor_uuid' => Yii::t('app', 'Tutor Name'),
            'instrument_uuid' => Yii::t('app', 'Instrument'),
            'tutorial_name' => Yii::t('app', 'Tutorial Name'),
            'tutorial_description' => Yii::t('app', 'Tutorial Description'),
            'tutorial_type_code' => Yii::t('app', 'Tutorial Type Code'),
            'tutorial_min' => Yii::t('app', 'Tutorial Min'),
            'payment_term_code' => Yii::t('app', 'Payment Term Code'),
            'total_session' => Yii::t('app', 'Total Session'),
            'session_length_day' => Yii::t('app', 'Session Length Day'),
            'price' => Yii::t('app', 'Price'),
            'gst' => Yii::t('app', 'Gst'),
            'gst_description' => Yii::t('app', 'Gst Description'),
            'total_price' => Yii::t('app', 'Total Price'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'cc_checkbox' => 'Save for future use',
            'cc_name' => 'Name',
            'cc_number' => 'Card Number',
            'cc_cvv' => 'CVV',
            'cc_month' => 'Month',
            'cc_year' => 'Year',
            'cc_month_year' => 'Expire Month',
            'order_uuid' => Yii::t('app', 'Order ID'),
            'student_monthly_subscription_uuid' => Yii::t('app', 'Subscription Name'),
            'term_condition_checkbox' => 'Term and condition',
            'coupon_uuid' => 'Coupon',
            'coupon_code' => 'Promo Code',
            'creative_kids_voucher_code' => 'NSW Creative Kids Voucher Code',
            'kids_name' => 'Kid\'s Name',
            'kids_dob' => 'Kid\'s Date of Birth',
        ];
    }

    public static function creativeCodeCriteria() {
        if ((!empty($this->creative_kids_voucher_code) && ( empty($this->kids_name) || empty($this->kids_dob))) || (!empty($this->kids_name) && ( empty($this->creative_kids_voucher_code) || empty($this->kids_dob))) || (!empty($this->kids_dob) && ( empty($this->kids_name) || empty($this->creative_kids_voucher_code)))) {

            if (empty($this->creative_kids_voucher_code)) {
                $this->addError('creative_kids_voucher_code', 'NSW Creative Kids Voucher Code Required.');
            }

            if (empty($this->kids_name)) {
                $this->addError('kids_name', 'Kid\'s Name Required.');
            }

            if (empty($this->kids_dob)) {
                $this->addError('kids_dob', 'Kid\'s Date of Birth Required.');
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstrumentUu() {
        return $this->hasOne(Instrument::className(), ['uuid' => 'instrument_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentUu() {
        return $this->hasOne(Student::className(), ['student_uuid' => 'student_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTutorUu() {
        return $this->hasOne(Tutor::className(), ['uuid' => 'tutor_uuid']);
    }

    public function getStudentMonthlySubscriptionUu() {
        return $this->hasOne(StudentMonthlySubscription::className(), ['uuid' => 'student_monthly_subscription_uuid']);
    }

    public function getOrderUu() {
        return $this->hasOne(Order::className(), ['uuid' => 'order_uuid']);
    }

    public function getCouponUu() {
        return $this->hasOne(Coupon::className(), ['uuid' => 'coupon_uuid']);
    }

    public static function setSession($_id, $key, $value) {
        if (!isset(Yii::$app->session[$_id])) {
            Yii::$app->session[$_id] = serialize(array($key => $value));
        } else {
            $session = unserialize(Yii::$app->session[$_id]);
            $session[$key] = $value;
            Yii::$app->session[$_id] = serialize($session);
        }
    }

    public static function getSession($_id, $key = NULL) {
        $session = unserialize(Yii::$app->session[$_id]);

        if ($key === NULL) {
            return $session;
        } else {
            return isset($session[$key]) ? $session[$key] : array();
        }
    }

    public static function unsetSession($token) {
        unset(Yii::$app->session[$token]);
    }

    public static function getInstrumentList() {
        return Instrument::find()->where(['status' => 'ACTIVE'])->all();
    }

    public static function getOnlineTutorialList($type = '', $tutuor_uuid, $instrument_uuid) {

        $tutor_model = Tutor::find()->where(['uuid' => $tutuor_uuid])->one();
        $tutor_type_uuid = $tutor_model->online_tutorial_package_type_uuid;


        $package_type_model = OnlineTutorialPackageType::find()->where(['uuid' => $tutor_model->online_tutorial_package_type_uuid])->one();
        $package_type = $package_type_model->name;


        $payment_term_model = PaymentTerm::find()->where(['term' => $type])->one();
        $tutorial_type_uuid = $payment_term_model->uuid;


        if ($package_type == 'STANDARD') {
            $tutorwise_plan_model = OnlineTutorialPackageTutorwise::find()->where(['tutor_uuid' => $tutor_model->uuid, 'instrument_uuid' => $instrument_uuid, 'online_tutorial_package_type_uuid' => $tutor_type_uuid])->asArray()->all();
        } else {
            $tutorwise_plan_model = OnlineTutorialPackageTutorwise::find()->where(['tutor_uuid' => $tutor_model->uuid, 'online_tutorial_package_type_uuid' => $tutor_type_uuid])->asArray()->all();
        }
        $tutor_plan_array = [];
        $i = 1;


        if (!empty($tutorwise_plan_model)) {
            foreach ($tutorwise_plan_model as $key => $value) {
                $query = new \yii\db\Query;

                $result = $query->select(['o.uuid as package_uuid', 'p.uuid', 'p.name', 'p.price', 'p.gst', 'p.total_price', 'p.status', 'p.default_option', 'p.online_tutorial_package_type_uuid']
                        )
                        ->from('online_tutorial_package as o')
                        ->join('RIGHT JOIN', 'premium_standard_plan_rate as p', 'o.uuid = p.online_tutorial_package_uuid')
                        ->where(['p.uuid' => $value['premium_standard_plan_rate_uuid'], 'p.online_tutorial_package_type_uuid' => $value['online_tutorial_package_type_uuid'], 'p.status' => 'ENABLED', 'p.payment_term_uuid' => $tutorial_type_uuid])
                        //->asArray()
                        ->one();


                if (!empty($result)) {

                    if ($package_type == "PREMIUM") {

                        $tutor_plan_array[$i] = ['online_packge_uuid' => $result['package_uuid'],
                            'premium_stanadrd_plan_uuid' => $result['uuid'],
                            'name' => $result['name'],
                            'tutor_option' => $result['default_option'],
                            'tutor_uuid' => $tutor_model->uuid,
                            'tutor_plan_total_price' => $result['total_price'],
                            'online_tutorial_package_type_uuid' => $result['online_tutorial_package_type_uuid']];
                    } else {

                        $option_model = StandardPlanOptionRate::find()->where(['premium_standard_plan_rate_uuid' => $value['premium_standard_plan_rate_uuid'], 'option' => $value['standard_plan_rate_option']])->one();
                        $plan_price = $option_model->price;
                        $plan_total_price = $option_model->total_price;
                        $plan_option = $option_model->option;

                        //Get id of tutorwise paln 
                        $tutorwise_model = OnlineTutorialPackageTutorwise::find()->where(['premium_standard_plan_rate_uuid' => $result['uuid'], 'tutor_uuid' => $tutuor_uuid, 'instrument_uuid' => $instrument_uuid])->one();
                        $online_tutorial_package_tutorwise_uuid = $tutorwise_model->uuid;

                        //Standard tutor loking period
                        $locking_per_model_count = StudentTuitionStandardBookingLockingPeriod::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid, 'tutor_uuid' => $tutuor_uuid, 'instrument_uuid' => $instrument_uuid])->count();

                        if ($locking_per_model_count > 0) {

                            $locking_per_model = StudentTuitionStandardBookingLockingPeriod::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid, 'tutor_uuid' => $tutuor_uuid, 'instrument_uuid' => $instrument_uuid])->one();
                            $locking_end_datetime = $locking_per_model->locking_end_datetime;
                            $locking_start_datetime = $locking_per_model->locking_start_datetime;
                            $locking_option = $locking_per_model->option;


                            $current_date = date('Y-m-d H:i:s');

                            if (($plan_option < $locking_option) && ($current_date <= $locking_end_datetime)) {

                                $lockingplanDetail = StandardPlanOptionRate::find()->where(['premium_standard_plan_rate_uuid' => $value['premium_standard_plan_rate_uuid'], 'option' => $locking_option])->one();
                                $plan_total_price = $lockingplanDetail->total_price;
                            }
                        }

                        $tutor_plan_array[$i] = ['online_packge_uuid' => $result['package_uuid'],
                            'premium_stanadrd_plan_uuid' => $result['uuid'],
                            'name' => $result['name'],
                            'tutor_option' => $value['standard_plan_rate_option'],
                            'tutor_uuid' => $tutor_model->uuid,
                            'tutor_plan_total_price' => $plan_total_price,
                            'online_tutorial_package_type_uuid' => $result['online_tutorial_package_type_uuid']];
                    }

                    $i++;
                }
            }
        }


        return $tutor_plan_array;
    }

    public static function getInstrumentWiseTutorList($instrument_uuid = '') {

        $sql = "select tutor.*,tutor_instrument.instrument_uuid,tutor_instrument.uuid as tutor_instrument_table_uuid,online_tutorial_package_type.name as tutor_cat_name,online_tutorial_package_type.display_name as tutor_cat_display_name from tutor 
            LEFT JOIN tutor_instrument ON tutor_instrument.tutor_uuid = tutor.uuid
            LEFT JOIN online_tutorial_package_type ON online_tutorial_package_type.uuid = tutor.online_tutorial_package_type_uuid
            where 1=1 and tutor.status = 'ENABLED' ";
        if ($instrument_uuid != "") {
            $sql .= " AND tutor_instrument.instrument_uuid = '$instrument_uuid' ";
        }

        $command = Yii::$app->db->createCommand($sql);
        $result = $command->queryAll();
        return $result;
    }

    public static function getTutorInstrumentPlanOptions($online_tutorial_package_type_uuid = '', $tutor_uuid = '', $instrument_uuid = '') {
        $result = OnlineTutorialPackageTutorwise::find()->where(['tutor_uuid' => $tutor_uuid, 'instrument_uuid' => $instrument_uuid, 'online_tutorial_package_type_uuid' => $online_tutorial_package_type_uuid])->one();
        return $result;
    }

    public static function getInstrumentWiseTutorPriceList($online_tutorial_package_type_uuid = '', $standard_plan_rate_option = '') {
        $query = new \yii\db\Query;
        $plans = $query->select(['o.*', 'p.uuid', 'p.name', 'p.price', 'p.gst', 'p.total_price', 'p.status', 'p.default_option', 'p.online_tutorial_package_type_uuid']
                        )
                        ->from('online_tutorial_package as o')
                        ->join('RIGHT JOIN', 'premium_standard_plan_rate as p', 'o.uuid = p.online_tutorial_package_uuid')
                        ->where(['p.online_tutorial_package_type_uuid' => $online_tutorial_package_type_uuid, 'p.status' => 'ENABLED'])
                        ->orderBy('p.payment_term_uuid')->all();
        $option_array = array();
        $i = 0;
//        echo "<pre>"; print_r($plans);echo "</pre>";
        if ($plans[0]['total_price'] > $plans[1]['total_price']) {
            $beforeswitch = $plans[1];
            $plans[1] = $plans[0];
            $plans[0] = $beforeswitch;
        }//echo "<pre>"; print_r($plans);echo "</pre>";
        foreach ($plans as $option_row) {

            $optionmodel = StandardPlanOptionRate::find()->where(['premium_standard_plan_rate_uuid' => $option_row['uuid'], 'option' => $standard_plan_rate_option])->asArray()->one();
//echo "<pre>"; print_r($optionmodel);echo "</pre>";
            $option_array[$i]['uuid'] = $option_row['uuid'];
            $option_array[$i]['name'] = $option_row['name'];

            $option_array[$i]['price'] = (!empty($optionmodel) ? $optionmodel['price'] : $option_row['price']);
            $option_array[$i]['gst'] = (!empty($optionmodel) ? $optionmodel['gst'] : $option_row['gst']);
            $option_array[$i]['total_price'] = (!empty($optionmodel) ? $optionmodel['total_price'] : $option_row['total_price']);
            $option_array[$i]['option'] = (!empty($optionmodel) ? $optionmodel['option'] : $option_row['default_option']);
            $option_array[$i]['package_type'] = (!empty($optionmodel) ? 'STANDARD' : 'PREMIUM');
            $option_array[$i]['gst_description'] = (!empty($optionmodel) ? $optionmodel['gst_description']:$option_row['gst_description']);
            $i++;
        }
//$option_array[$i]['uuid'] = $option_row['uuid'];
//            $option_array[$i]['name'] = $option_row['name'];
//
//            $option_array[$i]['price'] = $option_row['price'];
//            $option_array[$i]['gst'] = $option_row['gst'];
//            $option_array[$i]['total_price'] = $option_row['total_price'];
//            $option_array[$i]['option'] = $option_row['default_option'];
//            $option_array[$i]['gst_description'] = $option_row['gst_description'];
//
//            if (!empty($optionmodel)) {
//                $option_array[$i]['price'] = $optionmodel['price'];
//                $option_array[$i]['gst'] = $optionmodel['gst'];
//                $option_array[$i]['total_price'] = $optionmodel['total_price'];
//                $option_array[$i]['option'] = $optionmodel['option'];
//                $option_array[$i]['gst_description'] = $optionmodel['gst_description'];
//            }
//        print_r($option_array);exit;
        $tutor_price['full'] = (isset($option_array[1]['total_price']) ? $option_array[1]['total_price'] : $plans[1]['total_price']);
        $tutor_price['half'] = (isset($option_array[0]['total_price']) ? $option_array[0]['total_price'] : '$' . $plans[0]['total_price']);
        
        $tutor_price['package_type'] = $option_array[0]['package_type'];
        $tutor_price['online_tutorial_package_type_uuid']= $online_tutorial_package_type_uuid;
        
        $tutor_price['uuid']['full'] = $option_array[1]['uuid'];
        $tutor_price['price']['full'] = $option_array[1]['price'];
        $tutor_price['gst']['full'] = $option_array[1]['gst'];
        $tutor_price['option']['full'] = $option_array[1]['option'];
        $tutor_price['name']['full'] = $option_array[1]['name'];
        $tutor_price['gst_description']['full'] = $option_array[1]['gst_description'];
        
        $tutor_price['uuid']['half'] = $option_array[0]['uuid'];
        $tutor_price['price']['half'] = $option_array[0]['price'];
        $tutor_price['gst']['half'] = $option_array[0]['gst'];
        $tutor_price['option']['half'] = $option_array[0]['option'];
        $tutor_price['name']['half'] = $option_array[0]['name'];
        $tutor_price['gst_description']['half'] = $option_array[0]['gst_description'];
        
        
//                $tutor_price['half'] =$option_array[$i]['uuid'];
        return $tutor_price;
    }

    public static function getInstrumentWiseTutorListSingle($instrument_uuid = '',$tutorUUID) {

        $sql = "select tutor.*,tutor_instrument.instrument_uuid,tutor_instrument.uuid as tutor_instrument_table_uuid from tutor LEFT JOIN tutor_instrument ON tutor_instrument.tutor_uuid = tutor.uuid
            where 1=1 and tutor.status = 'ENABLED' ";
        if ($instrument_uuid != "") {
            $sql .= " AND tutor_instrument.instrument_uuid = '$instrument_uuid' AND tutor_uuid='$tutorUUID' ";
        }

        $command = Yii::$app->db->createCommand($sql);
        $result = $command->queryAll();
        return $result;
    }
    
    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['step_1'] = ['instrument_uuid', 'instrumentcheckbox'];
        $scenarios['step_3'] = ['onlinetutorial_uuid', 'onlinetutorialcheckbox', 'onlinetutorialtype'];
        $scenarios['step_2'] = ['tutorcheckbox', 'tutor_uuid'];
        $scenarios['step_4'] = ['session_week_day', 'session_when_start', 'start_hour', 'all_dates', 'session_text_info'];
        $scenarios['step_5'] = ['cc_name', 'cc_number', 'cc_cvv', 'cc_month_year', 'cc_checkbox', 'cc_uuid', 'monthly_sub_uuid', 'monthly_sub_total', 'discount', 'cart_total', 'monthly_sub_checkbox', 'coupon_code', 'term_condition_checkbox', 'creative_kids_voucher_code', 'kids_name', 'kids_name', 'kids_dob'];
        $scenarios['credit_step_1'] = ['instrument_uuid', 'instrumentcheckbox'];
        $scenarios['credit_step_3'] = ['tutorial_type_code', 'tutorial_type_code_checkbox'];
        $scenarios['credit_step_2'] = ['tutorcheckbox', 'tutor_uuid'];
        $scenarios['credit_step_4'] = ['session_week_day', 'session_when_start', 'start_hour', 'all_dates', 'session_text_info'];
        $scenarios['tutor_reschedule'] = ['tutor_uuid', 'student_uuid', 'tutorial_type_code', 'session_week_day', 'session_when_start', 'start_hour', 'all_dates', 'session_text_info'];
        return $scenarios;
    }

    public static function hoursRange($lower = 0, $upper = 86400, $step = 1800, $format = '') {

        $times = array();
        if (empty($format)) {
            $format = 'h:i A';
        }
        foreach (range($lower, $upper, $step) as $increment) {
            $increment = gmdate('H:i', $increment);
            list( $hour, $minutes ) = explode(':', $increment);
            $date = new \DateTime($hour . ':' . $minutes);
            $times[(string) $increment] = $date->format($format);
        }
        return $times;
    }

    public static function start_hour_array() {

        $formatter = function ($time) {
            if ($time % 3600 == 0) {
                return date('H:i', $time);
                //return date('ga', $time);
            } else {
                return date('H:i', $time);
                //return date('g:ia', $time);
            }
        };
        $halfHourSteps = range(0, 47 * 1800, 1800);

        $res = array_map($formatter, $halfHourSteps);
        $resDisplay = array_map(function ($a) {
            return date('h:i A', strtotime($a));
        }, $res);

        $result = array_combine($res, $resDisplay);

        return $result;
    }

    public static function disabled_week_day($day = '') {
        $array = ['sunday' => 0, 'monday' => 1, 'tuesday' => 2, 'wednesday' => 3, 'thursday' => 4, 'friday' => 5, 'saturday' => 6];
        if ($day != '') {
            unset($array[$day]);
        }
        return implode(',', array_values($array));
    }

    public static function convert_session_text($sessionDetail, $paymentTerm = '') {
        $text = '';
        $day = '';
        $start = '';
        $end = '';
        $date = [];
        if (!empty($sessionDetail['session_array'])) {

            foreach ($sessionDetail['session_array'] as $key => $value) {
                $day = ucfirst($value['day']);
                $start = ucfirst($value['start']);
                $end = ucfirst($value['end']);
                $date[] = $value['date'];
            }
            $date_txt = '';
            if (count($date) > 0) {
                $date_txt = '<p class="uibadges">';
                foreach ($date as $k => $v) {
                    $date_txt .= '<span class="badge badge-pill badge-info">' . General::displayTimeFormat($v, 'd-m-Y') . '</span>';
                }
                $date_txt .= '</p>';
            }
            if ($paymentTerm == 'Fortnightly') {
                $text = '<strong>' . $day . ', ' . General::displayTimeFormat($start, 'h:i A') . ' - ' . General::displayTimeFormat($end, 'h:i A') . ', ' . count($date) . ' occurrence(s)</strong> ' . $date_txt;
            } else {
                $text = '<strong>Every ' . $day . ', ' . General::displayTimeFormat($start, 'h:i A') . ' - ' . General::displayTimeFormat($end, 'h:i A') . ', untill ' . General::displayTimeFormat(end($date), 'd-m-Y') . ", " . count($date) . " occurrence(s)</strong> " . $date_txt;
            }
        }
        return $text;
    }

    public static function credit_convert_session_text($sessionDetail) {
        $text = '';
        $day = '';
        $start = '';
        $end = '';
        $date = [];
        if (!empty($sessionDetail['session_array'])) {

            foreach ($sessionDetail['session_array'] as $key => $value) {
                $day = ucfirst($value['day']);
                $start = ucfirst($value['start']);
                $end = ucfirst($value['end']);
                $date[] = $value['date'];
            }
            $date_txt = '';
            if (count($date) > 0) {
                $date_txt = '<p class="uibadges">';
                foreach ($date as $k => $v) {
                    $date_txt .= '<span class="badge badge-pill badge-info">' . General::displayTimeFormat($v, 'd-m-Y') . '</span>';
                }
                $date_txt .= '</p>';
            }
            $text = '<strong>' . $day . ', ' . General::displayTimeFormat($start, 'h:i A') . ' - ' . General::displayTimeFormat($end, 'h:i A') . '</strong> ' . $date_txt;
        }
        return $text;
    }

    public static function credit_time_between_day_working_period($tutor_uuid, $day = 'sunday', $selected_time = "00:00", $tutorial_code, $tutor_tz, $student_tz, $session_date) {

        $flag = false;
        $system_tz = Yii::$app->params['timezone'];
        $logged_user_tz = Yii::$app->user->identity->timezone;

        $tutor_selected_time = General::convertTimezone($session_date . ' ' . $selected_time, $logged_user_tz, $tutor_tz, 'Y-m-d H:i');
        $tutor_selected_date = General::convertTimezone($session_date . ' ' . $selected_time, $logged_user_tz, $tutor_tz, 'Y-m-d');
//        $tutor_selected_time = General::convertTimezone($session_date.' '.$selected_time,$student_tz,$tutor_tz,'Y-m-d H:i');
//        $tutor_selected_date = General::convertTimezone($session_date.' '.$selected_time,$student_tz,$tutor_tz,'Y-m-d');

        $day = strtolower(General::convertTimezone($session_date . ' ' . $selected_time, $logged_user_tz, $tutor_tz, 'l'));
//        $day = strtolower(General::convertTimezone($session_date.' '.$selected_time,$student_tz,$tutor_tz,'l'));

        $tutorDetail = Tutor::findOne($tutor_uuid);

        $tutorialType = TutorialType::find()->where(['code' => $tutorial_code])->asArray()->one();

        if (!empty($tutorDetail) && !empty($tutorialType)) {

            $tutor_working_plan = json_decode($tutorDetail->working_plan, true);
            $ses_min = $tutorialType['min'];

            if (!empty($tutor_working_plan[$day])) {

                $start = $tutor_selected_date . " " . $tutor_working_plan[$day]['start'];
                $end = $tutor_selected_date . " " . $tutor_working_plan[$day]['end'];

                $period_start = new \DateTime($start, new \DateTimeZone($tutor_tz));
                $period_end = new \DateTime($end, new \DateTimeZone($tutor_tz));
                $selected_time_date_start = new \DateTime($tutor_selected_time, new \DateTimeZone($tutor_tz));

                $period_end = new \DateTime(date('Y-m-d H:i:s', strtotime($period_end->format('Y-m-d H:i:s') . " +" . $ses_min . " minute")), new \DateTimeZone($tutor_tz));


                if ($selected_time_date_start >= $period_start && $selected_time_date_start <= $period_end) {

                    $flag = TRUE;
                }
            }
        }
        return $flag;
    }

    public static function credit_getDayChanges($tutor_uuid, $day = '', $tutorial_code, $selected_time, $selected_date = '', $student_uuid = '') {

        $session_array = [];
        $session_when_start = '';
        $return = ['session_week_day' => $day, 'session_when_start' => $session_when_start, 'session_array' => $session_array, 'message' => ''];
        if (!empty($day)) {

            $tutorDetail = Tutor::findOne($tutor_uuid);
            $tutor_user_model = User::find()->where(['tutor_uuid' => $tutor_uuid])->one();
            $student_user_model = User::find()->where(['student_uuid' => $student_uuid])->one();
            $tutor_tz = (!empty($tutor_user_model)) ? $tutor_user_model->timezone : Yii::$app->params['timezone'];
            $student_tz = (!empty($student_user_model)) ? $student_user_model->timezone : Yii::$app->params['timezone'];
            $tutorialType = TutorialType::find()->where(['code' => $tutorial_code])->asArray()->one();
            //$package = OnlineTutorialPackage::find()->joinWith(['paymentTermUu', 'tutorialTypeUu'])->where(['online_tutorial_package.uuid' => $online_package_uuid])->asArray()->one();

            if (!empty($tutorDetail) && !empty($tutorialType)) {

                $total_session = 1;
                $session_minute = $tutorialType['min'];
                $session_type = $tutorialType['type']; // W OR F
                $tutor_working_plan = json_decode($tutorDetail->working_plan, true);
                $skip_count = 50;

                if (!empty($tutor_working_plan[$day])) {

                    $day_break_array = isset($tutor_working_plan[$day]['breaks']) ? $tutor_working_plan[$day]['breaks'] : [];

                    if ($selected_date != '') {
                        $next = false;
                        $start_date = $selected_date;
                    }
                    /* else {
                      // if Today date include in booking then this section will enable
                      $next = true;
                      $start_date = date('Y-m-d');
                      } */

                    $start_date = date('Y-m-d', strtotime($start_date));

                    for ($i = 0; $i < $total_session; $i++) {

                        if ($next) {
                            $session_date = date('Y-m-d', strtotime($start_date . ' next ' . $day));
                        } else {
                            $session_date = $start_date;
                        }
                        $start_date = $session_date;
                        $next = true;

                        $start = $selected_time;
                        $end = date('H:i', strtotime($selected_time . " +" . $session_minute . " minute"));


                        $get_hours = self::get_available_hours($tutor_uuid, $tutor_working_plan, $session_date, $session_minute, $student_uuid, $tutor_tz, $student_tz);

                        $skip = (!in_array($session_date . ' ' . $selected_time, $get_hours)) ? true : false;
                        if ($skip) {
                            if ($skip_count == 0) {
                                //$return['message'] = "This time period is not available in ". ucfirst($day);
                                $return['message'] = "Your selected tutor is not available for one or more of the selected times/days. " . ucfirst($day) . " " . date('h:i A', strtotime($start));
                                break;
                            }
                            $skip_count--;
                            $i--;
                            continue;
                        }
                        $current_session = ['date' => date('d-m-Y', strtotime($session_date)), 'day' => $day, 'start' => $start, 'end' => $end, 'time_array' => $get_hours];
                        $session_array[] = $current_session;
                    }
                }
            }
        }
        $return['session_when_start'] = (!empty($session_array)) ? $session_array[0]['date'] : '';
        $return['session_array'] = $session_array;
        return $return;
    }

    public static function time_between_day_working_period($tutor_uuid, $day = 'sunday', $selected_time = "00:00", $online_package_uuid, $tutor_tz, $student_tz, $session_date) {

        $flag = false;

        $system_tz = Yii::$app->params['timezone'];

        $tutor_selected_time = General::convertTimezone($session_date . ' ' . $selected_time, $student_tz, $tutor_tz, 'Y-m-d H:i');
        $tutor_selected_date = General::convertTimezone($session_date . ' ' . $selected_time, $student_tz, $tutor_tz, 'Y-m-d');

        $day = strtolower(General::convertTimezone($session_date . ' ' . $selected_time, $student_tz, $tutor_tz, 'l'));

        $tutorDetail = Tutor::findOne($tutor_uuid);
        $package = PremiumStandardPlanRate::find()->joinWith(['paymentTermUu', 'tutorialTypeUu'])->where(['premium_standard_plan_rate.uuid' => $online_package_uuid])->asArray()->one();
        if (!empty($tutorDetail) && !empty($package)) {

            $tutor_working_plan = json_decode($tutorDetail->working_plan, true);
            $ses_min = $package['tutorialTypeUu']['min'];

            if (!empty($tutor_working_plan[$day])) {

                $start = $tutor_selected_date . " " . $tutor_working_plan[$day]['start'];
                $end = $tutor_selected_date . " " . $tutor_working_plan[$day]['end'];

                $period_start = new \DateTime($start, new \DateTimeZone($tutor_tz));
                $period_end = new \DateTime($end, new \DateTimeZone($tutor_tz));
                $selected_time_date_start = new \DateTime($tutor_selected_time, new \DateTimeZone($tutor_tz));

                $period_end = new \DateTime(date('Y-m-d H:i:s', strtotime($period_end->format('Y-m-d H:i:s') . " +" . $ses_min . " minute")), new \DateTimeZone($tutor_tz));

                if ($selected_time_date_start >= $period_start && $selected_time_date_start <= $period_end) {
                    $flag = TRUE;
                }
            }
        }
        return $flag;
    }

    public static function getDayChanges($tutor_uuid, $day = '', $online_package_uuid, $selected_time, $selected_date = '', $student_uuid = '') {
        // echo $selected_time;
        // echo "<br>".$selected_date;
        $session_array = [];
        $session_when_start = '';
        $return = ['session_week_day' => $day, 'session_when_start' => $session_when_start, 'session_array' => $session_array, 'message' => ''];
        if (!empty($day)) {

            $tutorDetail = Tutor::findOne($tutor_uuid);
            $tutor_user_model = User::find()->where(['tutor_uuid' => $tutor_uuid])->one();
            $student_user_model = User::find()->where(['student_uuid' => $student_uuid])->one();
            $tutor_tz = (!empty($tutor_user_model)) ? $tutor_user_model->timezone : Yii::$app->params['timezone'];
            $student_tz = (!empty($student_user_model)) ? $student_user_model->timezone : Yii::$app->params['timezone'];

            $package = PremiumStandardPlanRate::find()->joinWith(['paymentTermUu', 'tutorialTypeUu'])->where(['premium_standard_plan_rate.uuid' => $online_package_uuid])->asArray()->one();

            if (!empty($tutorDetail) && !empty($package)) {

                $total_session = $package['duration'];
                $session_minute = $package['tutorialTypeUu']['min'];
                $session_type = $package['tutorialTypeUu']['type']; // W OR F
                $bitween_days = $package['tutorialTypeUu']['days']; // W OR F
                $tutor_working_plan = json_decode($tutorDetail->working_plan, true);
                $skip_count = 50;

                if (!empty($tutor_working_plan[$day])) {

                    $day_break_array = isset($tutor_working_plan[$day]['breaks']) ? $tutor_working_plan[$day]['breaks'] : [];

                    if ($selected_date != '') {
                        $next = false;
                        $start_date = $selected_date;
                    }
                    /* else {
                      // if Today date include in booking then this section will enable
                      $next = true;
                      $start_date = date('Y-m-d');
                      } */

                    $start_date = date('Y-m-d', strtotime($start_date));
                    $prev_date = $start_date;

                    for ($i = 0; $i < $total_session; $i++) {

                        if ($next) {
                            $session_date = date('Y-m-d', strtotime($start_date . ' next ' . $day));

                            $date1 = new \DateTime($prev_date);
                            $date2 = new \DateTime($session_date);
                            $diff = $date1->diff($date2);
                            $diff_days = $diff->format("%a");
                            $diff_days = ($diff_days > 0) ? $diff_days : 0;
                            if ($diff_days < $bitween_days) {
                                $session_date = date('Y-m-d', strtotime($session_date . ' next ' . $day));
                            }
                        } else {
                            $session_date = $start_date;
                        }
                        $start_date = $session_date;
                        $prev_date = $start_date;
                        $next = true;

                        $start = $selected_time;
                        $end = date('H:i', strtotime($selected_time . " +" . $session_minute . " minute"));


                        $get_hours = self::get_available_hours($tutor_uuid, $tutor_working_plan, $session_date, $session_minute, $student_uuid, $tutor_tz, $student_tz);

                        // $hours = [];
                        // foreach($get_hours as $key => $value){
                        //     $key = General::convertTime($value,$tutor_tz,$student_tz, 'h:i');
                        //     $value = General::convertTime($value,$tutor_tz,$student_tz, 'Y-m-d H:i');
                        //     $hours[$key] = $value;
                        // }
                        $skip = (!in_array($session_date . ' ' . $selected_time, $get_hours)) ? true : false;
                        // $skip = (!in_array($session_date.' '.$selected_time, $hours)) ? true : false;
                        if ($skip) {
                            if ($skip_count == 0) {
                                //$return['message'] = "This time period is not available in ". ucfirst($day);
                                $return['message'] = "Your selected tutor is not available for one or more of the selected times/days. " . ucfirst($day) . " " . date('h:i A', strtotime($start));
                                break;
                            }
                            $skip_count--;
                            $i--;
                            continue;
                        }
                        $current_session = ['date' => date('d-m-Y', strtotime($session_date)), 'day' => $day, 'start' => $start, 'end' => $end, 'time_array' => $get_hours];
                        // $current_session = ['date' => General::displayDate($session_date), 'day' => $day, 'start' => $start, 'end' => $end, 'time_array' => $hours];
                        $session_array[] = $current_session;
                    }//exit;
                }
            }
        }
        $return['session_when_start'] = (!empty($session_array)) ? $session_array[0]['date'] : '';
        $return['session_array'] = $session_array;
        return $return;
    }

    public static function is_tutor_available_perticular_date_time($tutor_uuid, $tutor_working_plan, $session_date, $session_minute, $selected_time = "00:00") {

        // Check if the provider is available for the requested date.
        $empty_periods = self::get_tutor_available_time_period($tutor_uuid, $tutor_working_plan, $session_date);

        $available_hours = self::_calculate_available_hours($empty_periods, $session_date, $session_minute, FALSE, 'flexible');

        return (in_array($selected_time, $selected_time)) ? true : false;
    }

    public static function get_available_hours($tutor_uuid, $tutor_working_plan, $session_date, $session_minute, $student_uuid = '', $tutor_tz, $student_tz) {
        $system_tz = Yii::$app->params['timezone'];
        $session_date = date('Y-m-d', strtotime($session_date));
        $empty_periods = self::get_tutor_available_time_period($tutor_uuid, $tutor_working_plan, $session_date, $student_uuid, $tutor_tz, $student_tz);

        $available_hours = self::_calculate_available_hours($empty_periods, $session_date, $session_minute, filter_var(false, FILTER_VALIDATE_BOOLEAN), 'flexible', $tutor_tz, $student_tz);
        //echo "<pre>";
        //print_r($empty_periods);
        //print_r($available_hours);
        // If the selected date is today, remove past hours. It is important  include the timeout before
        // booking that is set in the back-office the system. Normally we might want the customer to book
        // an appointment that is at least half or one hour from now. The setting is stored in minutes.
        //if (date('Y-m-d', strtotime($session_date)) === date('Y-m-d')) {
        if (General::convertTimezone($session_date, $student_tz, $student_tz, 'Y-m-d') === General::convertTimezone(date('Y-m-d'), $system_tz, $student_tz, 'Y-m-d')) {
            $book_advance_timeout = Yii::$app->params['book_advance_timeout'];

            foreach ($available_hours as $index => $value) {
                $available_hour = strtotime($value);
                $current_hour = strtotime('+' . $book_advance_timeout . ' minutes', strtotime('now'));
                if ($available_hour <= $current_hour) {
                    unset($available_hours[$index]);
                }
            }
        }

        $available_hours = array_values($available_hours);
        sort($available_hours, SORT_STRING);
        $available_hours = array_values($available_hours);

        //print_r($available_hours);exit;
        return $available_hours;
    }

    public static function periodConvertTimezoneWise($periods, $from_tz, $to_tz, $format = 'Y-m-d H:i') {
        $return = [];
        if (!empty($periods)) {
            foreach ($periods as $key => $value) {
                $return[] = ['start' => General::convertTimezone($value['start'], $from_tz, $to_tz, $format), 'end' => General::convertTimezone($value['end'], $from_tz, $to_tz, $format)];
            }
        }
        return $return;
    }

    public static function get_tutor_available_time_period_old($tutor_uuid, $working_plan, $selected_date, $student_uuid = '', $tutor_tz, $student_tz) {
        $system_tz = Yii::$app->params['timezone'];

        $day_start_tutor = General::convertTimezone($selected_date . ' 00:00', $student_tz, $tutor_tz, 'Y-m-d H:i');
        $day_end_tutor = General::convertTimezone($selected_date . ' 23:59', $student_tz, $tutor_tz, 'Y-m-d H:i');


        $tutor_start_day_working_plan = $working_plan[strtolower(date('l', strtotime($day_start_tutor)))];
        $tutor_start_day_breaks = (isset($tutor_start_day_working_plan['breaks'])) ? $tutor_start_day_working_plan['breaks'] : [];

        $tutor_end_day_working_plan = $working_plan[strtolower(date('l', strtotime($day_end_tutor)))];
        $tutor_end_day_breaks = (isset($tutor_end_day_working_plan['breaks'])) ? $tutor_end_day_working_plan['breaks'] : [];



        $periods = [];
        $periods_start = [];
        $periods_end = [];

        /*         * ***tutor start date array*** */
        if (isset($tutor_start_day_breaks) && isset($tutor_start_day_working_plan)) {

            $st = ( (strtolower(date('Y-m-d', strtotime($day_start_tutor))) . ' ' . $tutor_start_day_working_plan['start']) > $day_start_tutor ) ? strtolower(date('Y-m-d', strtotime($day_start_tutor))) . ' ' . $tutor_start_day_working_plan['start'] : $day_start_tutor;
            $et = ( (strtolower(date('Y-m-d', strtotime($day_start_tutor))) . ' ' . $tutor_start_day_working_plan['end']) > $st ) ? (strtolower(date('Y-m-d', strtotime($day_start_tutor))) . ' ' . $tutor_start_day_working_plan['end']) : $st;
            $periods_start[] = [
                'start' => $st,
                'end' => $et
            ];


            $day_start = new \DateTime($st, new \DateTimeZone($tutor_tz));
            $day_end = new \DateTime($et, new \DateTimeZone($tutor_tz));

            if (!empty($tutor_start_day_breaks)) {
                foreach ($tutor_start_day_breaks as $index => $break) {


                    $break_start = new \DateTime(date('Y-m-d', strtotime($day_start_tutor)) . ' ' . $break['start'], new \DateTimeZone($tutor_tz));
                    $break_end = new \DateTime(date('Y-m-d', strtotime($day_start_tutor)) . ' ' . $break['end'], new \DateTimeZone($tutor_tz));

                    if ($break_start < $day_start) {
                        $break_start = $day_start;
                    }

                    if ($break_end > $day_end) {
                        $break_end = $day_end;
                    }

                    if ($break_start >= $break_end) {
                        continue;
                    }

                    foreach ($periods_start as $key => $period) {
                        $period_start = new \DateTime($period['start'], new \DateTimeZone($tutor_tz));
                        $period_end = new \DateTime($period['end'], new \DateTimeZone($tutor_tz));

                        $remove_current_period = FALSE;

                        if ($break_start > $period_start && $break_start < $period_end && $break_end > $period_start) {
                            $periods_start[] = [
                                'start' => $period_start->format('Y-m-d H:i'),
                                'end' => $break_start->format('Y-m-d H:i')
                            ];

                            $remove_current_period = TRUE;
                        }

                        if ($break_start < $period_end && $break_end > $period_start && $break_end < $period_end) {
                            $periods_start[] = [
                                'start' => $break_end->format('Y-m-d H:i'),
                                'end' => $period_end->format('Y-m-d H:i')
                            ];

                            $remove_current_period = TRUE;
                        }

                        if ($break_start == $period_start && $break_end == $period_end) {
                            $periods_start[] = [
                                'start' => $period_start->format('Y-m-d H:i'),
                                'end' => $break_start->format('Y-m-d H:i')
                            ];

                            $remove_current_period = TRUE;
                        }

                        if ($remove_current_period) {
                            unset($periods_start[$key]);
                        }
                    }
                }
            }
        }

        /*         * ***tutor end date array*** */
        if (isset($tutor_end_day_breaks) && isset($tutor_end_day_working_plan)) {

            $st = ( (strtolower(date('Y-m-d', strtotime($day_end_tutor))) . ' ' . $tutor_end_day_working_plan['start']) < $day_end_tutor ) ? strtolower(date('Y-m-d', strtotime($day_end_tutor))) . ' ' . $tutor_end_day_working_plan['start'] : $day_end_tutor;
            $et = ( (strtolower(date('Y-m-d', strtotime($day_end_tutor))) . ' ' . $tutor_end_day_working_plan['end']) > $st ) ? (strtolower(date('Y-m-d', strtotime($day_end_tutor))) . ' ' . $tutor_end_day_working_plan['end']) : $st;
            $periods_end[] = [
                'start' => $st,
                'end' => $et
            ];
//            echo "<pre>";
//            print_r((strtolower(date('Y-m-d', strtotime($day_end_tutor))).' '.$tutor_end_day_working_plan['end']));

            $day_start = new \DateTime($periods_end[0]['start'], new \DateTimeZone($tutor_tz));
            $day_end = new \DateTime($periods_end[0]['end'], new \DateTimeZone($tutor_tz));

            if (!empty($tutor_end_day_breaks)) {
                foreach ($tutor_end_day_breaks as $index => $break) {


                    $break_start = new \DateTime(date('Y-m-d', strtotime($day_end_tutor)) . ' ' . $break['start'], new \DateTimeZone($tutor_tz));
                    $break_end = new \DateTime(date('Y-m-d', strtotime($day_end_tutor)) . ' ' . $break['end'], new \DateTimeZone($tutor_tz));

                    //$break_start = new \DateTime($break['start'],new \DateTimeZone($tutor_tz));
                    //$break_end = new \DateTime($break['end'],new \DateTimeZone($tutor_tz));

                    if ($break_start < $day_start) {
                        $break_start = $day_start;
                    }

                    if ($break_end > $day_end) {
                        $break_end = $day_end;
                    }

                    if ($break_start >= $break_end) {
                        continue;
                    }

                    foreach ($periods_end as $key => $period) {
                        $period_start = new \DateTime($period['start'], new \DateTimeZone($tutor_tz));
                        $period_end = new \DateTime($period['end'], new \DateTimeZone($tutor_tz));

                        $remove_current_period = FALSE;

                        if ($break_start > $period_start && $break_start < $period_end && $break_end > $period_start) {
                            $periods_end[] = [
                                'start' => $period_start->format('Y-m-d H:i'),
                                'end' => $break_start->format('Y-m-d H:i')
                            ];

                            $remove_current_period = TRUE;
                        }

                        if ($break_start < $period_end && $break_end > $period_start && $break_end < $period_end) {
                            $periods_end[] = [
                                'start' => $break_end->format('Y-m-d H:i'),
                                'end' => $period_end->format('Y-m-d H:i')
                            ];

                            $remove_current_period = TRUE;
                        }

                        if ($break_start == $period_start && $break_end == $period_end) {
                            $periods_end[] = [
                                'start' => $period_start->format('Y-m-d H:i'),
                                'end' => $break_start->format('Y-m-d H:i')
                            ];

                            $remove_current_period = TRUE;
                        }

                        if ($remove_current_period) {
                            unset($periods_end[$key]);
                        }
                    }
                }
            }
        }

        $tutor_all_periods = array_merge($periods_start, $periods_end);
        //echo "All";
        //print_r($tutor_all_periods);exit;
        /*         * ****** $periods variable converted to sytem timezone wise event ******** */
        $periods = self::periodConvertTimezoneWise($tutor_all_periods, $tutor_tz, $system_tz);


        // Break the empty periods with the reserved booked session.
        $tutor_appointments = self::get_tutor_booked_appointments($tutor_uuid);
        $tutor_unavailability = self::get_tutor_unavailability($tutor_uuid);
        $student_appointments = (!empty($student_uuid)) ? self::get_student_booked_appointments($student_uuid) : [];
        $reschedule_appointments = self::get_reschedule_appointments($tutor_uuid);

        $provider_appointments_unavailability = array_merge($tutor_appointments, $tutor_unavailability);
        $provider_appointments_all = array_merge($provider_appointments_unavailability, $student_appointments);
        $provider_appointments_all_final = array_merge($provider_appointments_all, $reschedule_appointments);
        $provider_appointments = self::add_break_after_lesson($provider_appointments_all_final);
        //echo "<pre>provider_appointments";print_r($provider_appointments);exit;

        foreach ($provider_appointments as $provider_appointment) {
            foreach ($periods as $index => &$period) {

                $appointment_start = new \DateTime($provider_appointment['start_datetime']);
                $appointment_end = new \DateTime($provider_appointment['end_datetime']);
                $period_start = new \DateTime($period['start']);
                $period_end = new \DateTime($period['end']);

                if ($appointment_start <= $period_start && $appointment_end <= $period_end && $appointment_end <= $period_start) {
                    // The appointment does not belong in this time period, so we  will not change anything.
                } else {
                    if ($appointment_start <= $period_start && $appointment_end <= $period_end && $appointment_end >= $period_start) {
                        // The appointment starts before the period and finishes somewhere inside. We will need to break
                        // this period and leave the available part.
                        $period['start'] = $appointment_end->format('Y-m-d H:i');
                    } else {
                        if ($appointment_start >= $period_start && $appointment_end < $period_end) {
                            // The appointment is inside the time period, so we will split the period into two new
                            // others.
                            unset($periods[$index]);

                            $periods[] = [
                                'start' => $period_start->format('Y-m-d H:i'),
                                'end' => $appointment_start->format('Y-m-d H:i')
                            ];

                            $periods[] = [
                                'start' => $appointment_end->format('Y-m-d H:i'),
                                'end' => $period_end->format('Y-m-d H:i')
                            ];
                        } else if ($appointment_start == $period_start && $appointment_end == $period_end) {
                            unset($periods[$index]); // The whole period is blocked so remove it from the available periods array.
                        } else {
                            if ($appointment_start >= $period_start && $appointment_end >= $period_start && $appointment_start <= $period_end) {
                                // The appointment starts in the period and finishes out of it. We will need to remove
                                // the time that is taken from the appointment.
                                $period['end'] = $appointment_start->format('Y-m-d H:i');
                            } else {
                                if ($appointment_start >= $period_start && $appointment_end >= $period_end && $appointment_start >= $period_end) {
                                    // The appointment does not belong in the period so do not change anything.
                                } else {
                                    if ($appointment_start <= $period_start && $appointment_end >= $period_end && $appointment_start <= $period_end) {
                                        // The appointment is bigger than the period, so this period needs to be removed.
                                        unset($periods[$index]);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $result_all_available_period_system_tz = array_values($periods);

        /*         * ****** $result variable converted to sytem timezone to student tz wise event ******** */
        $result = self::periodConvertTimezoneWise($result_all_available_period_system_tz, $system_tz, $student_tz, "Y-m-d H:i");
        //print_r($result);exit;
        return $result;
    }

    public static function get_tutor_available_time_period($tutor_uuid, $working_plan, $selected_date, $student_uuid = '', $tutor_tz, $student_tz) {
        $system_tz = Yii::$app->params['timezone'];
        $logged_user_tz = Yii::$app->user->identity->timezone;
        //$logged_user_tz = $student_tz;

        $day_start_tutor = General::convertTimezone($selected_date . ' 00:00', $logged_user_tz, $tutor_tz, 'Y-m-d H:i');
        $day_end_tutor = General::convertTimezone($selected_date . ' 23:59', $logged_user_tz, $tutor_tz, 'Y-m-d H:i');
//        $day_start_tutor = General::convertTimezone($selected_date.' 00:00',$student_tz,$tutor_tz,'Y-m-d H:i');
//        $day_end_tutor = General::convertTimezone($selected_date.' 23:59',$student_tz,$tutor_tz,'Y-m-d H:i');


        $tutor_start_day_working_plan = $working_plan[strtolower(date('l', strtotime($day_start_tutor)))];
        $tutor_start_day_breaks = (isset($tutor_start_day_working_plan['breaks'])) ? $tutor_start_day_working_plan['breaks'] : [];

        $tutor_end_day_working_plan = $working_plan[strtolower(date('l', strtotime($day_end_tutor)))];
        $tutor_end_day_breaks = (isset($tutor_end_day_working_plan['breaks'])) ? $tutor_end_day_working_plan['breaks'] : [];



        $periods = [];
        $periods_start = [];
        $periods_end = [];

        /*         * ***tutor start date array*** */
        if (isset($tutor_start_day_breaks) && isset($tutor_start_day_working_plan)) {

            $st = ( (strtolower(date('Y-m-d', strtotime($day_start_tutor))) . ' ' . $tutor_start_day_working_plan['start']) > $day_start_tutor ) ? strtolower(date('Y-m-d', strtotime($day_start_tutor))) . ' ' . $tutor_start_day_working_plan['start'] : $day_start_tutor;
            $et = ( (strtolower(date('Y-m-d', strtotime($day_start_tutor))) . ' ' . $tutor_start_day_working_plan['end']) > $st ) ? (strtolower(date('Y-m-d', strtotime($day_start_tutor))) . ' ' . $tutor_start_day_working_plan['end']) : $st;
            $periods_start[] = [
                'start' => $st,
                'end' => $et
            ];


            $day_start = new \DateTime($st, new \DateTimeZone($tutor_tz));
            $day_end = new \DateTime($et, new \DateTimeZone($tutor_tz));

            if (!empty($tutor_start_day_breaks)) {
                foreach ($tutor_start_day_breaks as $index => $break) {


                    $break_start = new \DateTime(date('Y-m-d', strtotime($day_start_tutor)) . ' ' . $break['start'], new \DateTimeZone($tutor_tz));
                    $break_end = new \DateTime(date('Y-m-d', strtotime($day_start_tutor)) . ' ' . $break['end'], new \DateTimeZone($tutor_tz));

                    if ($break_start < $day_start) {
                        $break_start = $day_start;
                    }

                    if ($break_end > $day_end) {
                        $break_end = $day_end;
                    }

                    if ($break_start >= $break_end) {
                        continue;
                    }

                    foreach ($periods_start as $key => $period) {
                        $period_start = new \DateTime($period['start'], new \DateTimeZone($tutor_tz));
                        $period_end = new \DateTime($period['end'], new \DateTimeZone($tutor_tz));

                        $remove_current_period = FALSE;

                        if ($break_start > $period_start && $break_start < $period_end && $break_end > $period_start) {
                            $periods_start[] = [
                                'start' => $period_start->format('Y-m-d H:i'),
                                'end' => $break_start->format('Y-m-d H:i')
                            ];

                            $remove_current_period = TRUE;
                        }

                        if ($break_start < $period_end && $break_end > $period_start && $break_end < $period_end) {
                            $periods_start[] = [
                                'start' => $break_end->format('Y-m-d H:i'),
                                'end' => $period_end->format('Y-m-d H:i')
                            ];

                            $remove_current_period = TRUE;
                        }

                        if ($break_start == $period_start && $break_end == $period_end) {
                            $periods_start[] = [
                                'start' => $period_start->format('Y-m-d H:i'),
                                'end' => $break_start->format('Y-m-d H:i')
                            ];

                            $remove_current_period = TRUE;
                        }

                        if ($remove_current_period) {
                            unset($periods_start[$key]);
                        }
                    }
                }
            }
        }

        /*         * ***tutor end date array*** */
        if (isset($tutor_end_day_breaks) && isset($tutor_end_day_working_plan)) {

            $st = ( (strtolower(date('Y-m-d', strtotime($day_end_tutor))) . ' ' . $tutor_end_day_working_plan['start']) < $day_end_tutor ) ? strtolower(date('Y-m-d', strtotime($day_end_tutor))) . ' ' . $tutor_end_day_working_plan['start'] : $day_end_tutor;
            $et = ( (strtolower(date('Y-m-d', strtotime($day_end_tutor))) . ' ' . $tutor_end_day_working_plan['end']) > $st ) ? (strtolower(date('Y-m-d', strtotime($day_end_tutor))) . ' ' . $tutor_end_day_working_plan['end']) : $st;
            $periods_end[] = [
                'start' => $st,
                'end' => $et
            ];
//            echo "<pre>";
//            print_r((strtolower(date('Y-m-d', strtotime($day_end_tutor))).' '.$tutor_end_day_working_plan['end']));

            $day_start = new \DateTime($periods_end[0]['start'], new \DateTimeZone($tutor_tz));
            $day_end = new \DateTime($periods_end[0]['end'], new \DateTimeZone($tutor_tz));

            if (!empty($tutor_end_day_breaks)) {
                foreach ($tutor_end_day_breaks as $index => $break) {


                    $break_start = new \DateTime(date('Y-m-d', strtotime($day_end_tutor)) . ' ' . $break['start'], new \DateTimeZone($tutor_tz));
                    $break_end = new \DateTime(date('Y-m-d', strtotime($day_end_tutor)) . ' ' . $break['end'], new \DateTimeZone($tutor_tz));

                    //$break_start = new \DateTime($break['start'],new \DateTimeZone($tutor_tz));
                    //$break_end = new \DateTime($break['end'],new \DateTimeZone($tutor_tz));

                    if ($break_start < $day_start) {
                        $break_start = $day_start;
                    }

                    if ($break_end > $day_end) {
                        $break_end = $day_end;
                    }

                    if ($break_start >= $break_end) {
                        continue;
                    }

                    foreach ($periods_end as $key => $period) {
                        $period_start = new \DateTime($period['start'], new \DateTimeZone($tutor_tz));
                        $period_end = new \DateTime($period['end'], new \DateTimeZone($tutor_tz));

                        $remove_current_period = FALSE;

                        if ($break_start > $period_start && $break_start < $period_end && $break_end > $period_start) {
                            $periods_end[] = [
                                'start' => $period_start->format('Y-m-d H:i'),
                                'end' => $break_start->format('Y-m-d H:i')
                            ];

                            $remove_current_period = TRUE;
                        }

                        if ($break_start < $period_end && $break_end > $period_start && $break_end < $period_end) {
                            $periods_end[] = [
                                'start' => $break_end->format('Y-m-d H:i'),
                                'end' => $period_end->format('Y-m-d H:i')
                            ];

                            $remove_current_period = TRUE;
                        }

                        if ($break_start == $period_start && $break_end == $period_end) {
                            $periods_end[] = [
                                'start' => $period_start->format('Y-m-d H:i'),
                                'end' => $break_start->format('Y-m-d H:i')
                            ];

                            $remove_current_period = TRUE;
                        }

                        if ($remove_current_period) {
                            unset($periods_end[$key]);
                        }
                    }
                }
            }
        }

        $tutor_all_periods = array_merge($periods_start, $periods_end);
        //echo "All";
        //print_r($tutor_all_periods);exit;
        /*         * ****** $periods variable converted to sytem timezone wise event ******** */
        $periods = self::periodConvertTimezoneWise($tutor_all_periods, $tutor_tz, $system_tz);


        // Break the empty periods with the reserved booked session.
        $tutor_appointments = self::get_tutor_booked_appointments($tutor_uuid);
        $tutor_unavailability = self::get_tutor_unavailability($tutor_uuid);
        $student_appointments = (!empty($student_uuid)) ? self::get_student_booked_appointments($student_uuid) : [];
        $reschedule_appointments = self::get_reschedule_appointments($tutor_uuid);

        $provider_appointments_unavailability = array_merge($tutor_appointments, $tutor_unavailability);
        $provider_appointments_all = array_merge($provider_appointments_unavailability, $student_appointments);
        $provider_appointments_all_final = array_merge($provider_appointments_all, $reschedule_appointments);
        $provider_appointments = self::add_break_after_lesson($provider_appointments_all_final);
        //echo "<pre>provider_appointments";print_r($provider_appointments);exit;

        foreach ($provider_appointments as $provider_appointment) {
            foreach ($periods as $index => &$period) {

                $appointment_start = new \DateTime($provider_appointment['start_datetime']);
                $appointment_end = new \DateTime($provider_appointment['end_datetime']);
                $period_start = new \DateTime($period['start']);
                $period_end = new \DateTime($period['end']);

                if ($appointment_start <= $period_start && $appointment_end <= $period_end && $appointment_end <= $period_start) {
                    // The appointment does not belong in this time period, so we  will not change anything.
                } else {
                    if ($appointment_start <= $period_start && $appointment_end <= $period_end && $appointment_end >= $period_start) {
                        // The appointment starts before the period and finishes somewhere inside. We will need to break
                        // this period and leave the available part.
                        $period['start'] = $appointment_end->format('Y-m-d H:i');
                    } else {
                        if ($appointment_start >= $period_start && $appointment_end < $period_end) {
                            // The appointment is inside the time period, so we will split the period into two new
                            // others.
                            unset($periods[$index]);

                            $periods[] = [
                                'start' => $period_start->format('Y-m-d H:i'),
                                'end' => $appointment_start->format('Y-m-d H:i')
                            ];

                            $periods[] = [
                                'start' => $appointment_end->format('Y-m-d H:i'),
                                'end' => $period_end->format('Y-m-d H:i')
                            ];
                        } else if ($appointment_start == $period_start && $appointment_end == $period_end) {
                            unset($periods[$index]); // The whole period is blocked so remove it from the available periods array.
                        } else {
                            if ($appointment_start >= $period_start && $appointment_end >= $period_start && $appointment_start <= $period_end) {
                                // The appointment starts in the period and finishes out of it. We will need to remove
                                // the time that is taken from the appointment.
                                $period['end'] = $appointment_start->format('Y-m-d H:i');
                            } else {
                                if ($appointment_start >= $period_start && $appointment_end >= $period_end && $appointment_start >= $period_end) {
                                    // The appointment does not belong in the period so do not change anything.
                                } else {
                                    if ($appointment_start <= $period_start && $appointment_end >= $period_end && $appointment_start <= $period_end) {
                                        // The appointment is bigger than the period, so this period needs to be removed.
                                        unset($periods[$index]);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $result_all_available_period_system_tz = array_values($periods);
        //echo "<pre>";print_r($result_all_available_period_system_tz);exit;
        /*         * ****** $result variable converted to sytem timezone to student tz wise event ******** */
        $result = self::periodConvertTimezoneWise($result_all_available_period_system_tz, $system_tz, $logged_user_tz, "Y-m-d H:i");
//        $result  = self::periodConvertTimezoneWise($result_all_available_period_system_tz,$system_tz,$student_tz,"Y-m-d H:i");
        //echo "<pre>";print_r($result);exit;
        return $result;
    }

    public static function get_tutor_available_time_period_1($tutor_uuid, $working_plan, $selected_date, $student_uuid = '', $tutor_tz, $student_tz) {

        $system_tz = Yii::$app->params['timezone'];
        $selected_date_working_plan = $working_plan[strtolower(date('l', strtotime($selected_date)))];
        $selected_date_working_plan['breaks'] = (isset($selected_date_working_plan['breaks'])) ? $selected_date_working_plan['breaks'] : [];

        $periods = [];
        if (isset($selected_date_working_plan['breaks']) && isset($selected_date_working_plan['start'])) {
            $periods[] = [
                'start' => $selected_date_working_plan['start'],
                'end' => $selected_date_working_plan['end']
            ];

//            $day_start = General::convertTimezone($selected_date.' '.$selected_date_working_plan['start'],$tutor_tz,$system_tz,'Y-m-d H:i');
//            $day_end = General::convertTimezone($selected_date.' '.$selected_date_working_plan['end'],$tutor_tz,$system_tz,'Y-m-d H:i');
            $day_start = new \DateTime($selected_date_working_plan['start']);
            $day_end = new \DateTime($selected_date_working_plan['end']);



            // Split the working plan to available time periods that do not contain the breaks in them.
            foreach ($selected_date_working_plan['breaks'] as $index => $break) {
//                $break_start = General::convertTimezone($selected_date.' '.$break['start'],$tutor_tz,$student_tz,'Y-m-d H:i');
//                $break_end = General::convertTimezone($selected_date.' '.$break['end'],$tutor_tz,$student_tz,'Y-m-d H:i');
                $break_start = new \DateTime($break['start']);
                $break_end = new \DateTime($break['end']);

                if ($break_start < $day_start) {
                    $break_start = $day_start;
                }

                if ($break_end > $day_end) {
                    $break_end = $day_end;
                }

                if ($break_start >= $break_end) {
                    continue;
                }

                foreach ($periods as $key => $period) {
                    $period_start = new \DateTime($period['start']);
                    $period_end = new \DateTime($period['end']);

                    $remove_current_period = FALSE;

                    if ($break_start > $period_start && $break_start < $period_end && $break_end > $period_start) {
                        $periods[] = [
                            'start' => $period_start->format('H:i'),
                            'end' => $break_start->format('H:i')
                        ];

                        $remove_current_period = TRUE;
                    }

                    if ($break_start < $period_end && $break_end > $period_start && $break_end < $period_end) {
                        $periods[] = [
                            'start' => $break_end->format('H:i'),
                            'end' => $period_end->format('H:i')
                        ];

                        $remove_current_period = TRUE;
                    }

                    if ($break_start == $period_start && $break_end == $period_end) {
                        $periods[] = [
                            'start' => $period_start->format('H:i'),
                            'end' => $break_start->format('H:i')
                        ];

                        $remove_current_period = TRUE;
                    }

                    if ($remove_current_period) {
                        unset($periods[$key]);
                    }
                }
            }
        }

        // Break the empty periods with the reserved booked session.
        $tutor_appointments = self::get_tutor_booked_appointments($tutor_uuid);
        $tutor_unavailability = self::get_tutor_unavailability($tutor_uuid);
        $student_appointments = (!empty($student_uuid)) ? self::get_student_booked_appointments($student_uuid) : [];
        $reschedule_appointments = self::get_reschedule_appointments($tutor_uuid);

        $provider_appointments_unavailability = array_merge($tutor_appointments, $tutor_unavailability);
        $provider_appointments_all = array_merge($provider_appointments_unavailability, $student_appointments);
        $provider_appointments_all_final = array_merge($provider_appointments_all, $reschedule_appointments);
        $provider_appointments = self::add_break_after_lesson($provider_appointments_all_final);
        //echo "<pre>provider_appointments";print_r($provider_appointments);exit;
        echo "<pre>provider_appointments";
        print_r($periods);
        exit;
        foreach ($provider_appointments as $provider_appointment) {
            foreach ($periods as $index => &$period) {

                //Alpesh After session rest minute include
                //$appointment_between_gap_min = (isset($provider_appointment['is_tutor_unavailability']) && $provider_appointment['is_tutor_unavailability'] == 'Yes') ? 0 : 0;
                //$provider_appointment['end_datetime'] = date('Y-m-d H:i:s', strtotime($provider_appointment['end_datetime'] . " +" . $appointment_between_gap_min . " minutes"));

                $appointment_start = new \DateTime($provider_appointment['start_datetime']);
                $appointment_end = new \DateTime($provider_appointment['end_datetime']);
                $period_start = new \DateTime($selected_date . ' ' . $period['start']);
                $period_end = new \DateTime($selected_date . ' ' . $period['end']);
//echo "<pre>";
//print_r($appointment_start);
//print_r($appointment_end);
//print_r($period_start);
//print_r($period_end);
//exit;
                if ($appointment_start <= $period_start && $appointment_end <= $period_end && $appointment_end <= $period_start) {
                    // The appointment does not belong in this time period, so we  will not change anything.
                } else {
                    if ($appointment_start <= $period_start && $appointment_end <= $period_end && $appointment_end >= $period_start) {
                        // The appointment starts before the period and finishes somewhere inside. We will need to break
                        // this period and leave the available part.
                        $period['start'] = $appointment_end->format('H:i');
                    } else {
                        if ($appointment_start >= $period_start && $appointment_end < $period_end) {
                            // The appointment is inside the time period, so we will split the period into two new
                            // others.
                            unset($periods[$index]);

                            $periods[] = [
                                'start' => $period_start->format('H:i'),
                                'end' => $appointment_start->format('H:i')
                            ];

                            $periods[] = [
                                'start' => $appointment_end->format('H:i'),
                                'end' => $period_end->format('H:i')
                            ];
                        } else if ($appointment_start == $period_start && $appointment_end == $period_end) {
                            unset($periods[$index]); // The whole period is blocked so remove it from the available periods array.
                        } else {
                            if ($appointment_start >= $period_start && $appointment_end >= $period_start && $appointment_start <= $period_end) {
                                // The appointment starts in the period and finishes out of it. We will need to remove
                                // the time that is taken from the appointment.
                                $period['end'] = $appointment_start->format('H:i');
                            } else {
                                if ($appointment_start >= $period_start && $appointment_end >= $period_end && $appointment_start >= $period_end) {
                                    // The appointment does not belong in the period so do not change anything.
                                } else {
                                    if ($appointment_start <= $period_start && $appointment_end >= $period_end && $appointment_start <= $period_end) {
                                        // The appointment is bigger than the period, so this period needs to be removed.
                                        unset($periods[$index]);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        $result = array_values($periods);
        //echo "<pre>provider_appointments=====";print_r($result);exit;
        // $result = [['start' => "09:00",'end'=>'18:00']];
        return $result;
    }

    protected function _calculate_available_hours(
    array $empty_periods, $selected_date, $service_duration, $manage_mode = FALSE, $availabilities_type = 'flexible', $tutor_tz, $student_tz
    ) {

        $available_hours = [];

        foreach ($empty_periods as $period) {

            //$start_hour = new \DateTime($selected_date . ' ' . $period['start']);
            //$end_hour = new \DateTime($selected_date . ' ' . $period['end']);
            $start_hour = new \DateTime($period['start'], new \DateTimeZone($student_tz));
            $end_hour = new \DateTime($period['end'], new \DateTimeZone($student_tz));

            $interval = $availabilities_type === Yii::$app->params['availabilities_type_fixed'] ? (int) $service_duration : 30;

            $current_hour = $start_hour;
            $diff = $current_hour->diff($end_hour);

//            while (($diff->h * 60 + $diff->i) >= intval($service_duration)) { //Alpesh when 23:59 time set
            while (($diff->h * 60 + $diff->i) >= intval($service_duration) || ($diff->h * 60 + $diff->i) >= intval($service_duration - 1)) {
                $available_hours[] = $current_hour->format('Y-m-d H:i');
                $current_hour->add(new \DateInterval('PT' . $interval . 'M'));
                $diff = $current_hour->diff($end_hour);
            }
        }

        return $available_hours;
    }

    public static function get_tutor_booked_appointments($tutor_uuid) {

        $model = BookedSession::find()->select('*, \'Yes\' AS "is_tutor_appointment"')->where(['tutor_uuid' => $tutor_uuid, 'status' => 'SCHEDULE'])->asArray()->all();
        if (!empty($model)) {
            return $model;
        } else {
            return [];
        }
    }

    public static function get_tutor_unavailability($tutor_uuid, $only_future = true) {

        $where = ($only_future) ? ' AND ((to_char(start_datetime, \'YYYY-MM-DD\') >= \'' . date('Y-m-d') . '\') OR  (to_char(end_datetime, \'YYYY-MM-DD\') >= \'' . date('Y-m-d') . '\'))' : "";
        $query = 'SELECT *, \'Yes\' AS "is_tutor_unavailability" FROM "tutor_unavailability" WHERE ("tutor_uuid"= \'' . $tutor_uuid . '\') ' . $where;
        $command = Yii::$app->db->createCommand($query);
        $model = $command->queryAll();

        if (!empty($model)) {
            return $model;
        } else {
            return [];
        }
    }

    public static function get_reschedule_appointments($tutor_uuid) {

        $query = 'SELECT *, "to_start_datetime" AS "start_datetime", "to_end_datetime" AS "end_datetime" FROM "reschedule_session" WHERE ("tutor_uuid"= \'' . $tutor_uuid . '\') AND "status" = \'AWAITING\' ';
        $command = Yii::$app->db->createCommand($query);
        $model = $command->queryAll();

        if (!empty($model)) {
            return $model;
        } else {
            return [];
        }
    }

    public static function get_student_booked_appointments($student_uuid) {

        $model = BookedSession::find()->where(['student_uuid' => $student_uuid, 'status' => 'SCHEDULE'])->asArray()->all();
        if (!empty($model)) {
            return $model;
        } else {
            return [];
        }
    }

    public static function add_break_after_lesson($provider_appointments) {
        $return = [];
        if (!empty($provider_appointments)) {
            foreach ($provider_appointments as $key => $app) {
                if (isset($app['is_tutor_appointment']) && $app['is_tutor_appointment'] == 'Yes') {

                    $appointment_between_gap_min = Yii::$app->params['interval_after_lesson'];
                    $app['end_datetime'] = date('Y-m-d H:i:s', strtotime($app['end_datetime'] . " +" . $appointment_between_gap_min . " minutes"));
                }
                $return[] = $app;
            }
        }
        return $return;
    }

    public static function check_coupon_available($coupon_code = "", $plan_price, $student_uuid = "") {

        $return = ['is_available' => false, 'message' => 'This Promo code not available.', 'discount' => 0, 'coupon_code' => $coupon_code, 'coupon_uuid' => ''];
        $model = Coupon::find()->where(['code' => $coupon_code, 'status' => 'ENABLED'])->one();

        if (!empty($model)) {
            $today = date('Y-m-d');
            $start = date('Y-m-d', strtotime($model->start_date));
            $end = date('Y-m-d', strtotime($model->end_date));
            $type = $model->type;


            if ($type == "Percentage") {

                $amount = ($plan_price * $model->amount ) / 100;
                $amount = number_format($amount, 2, '.', '');
            } else {
                $amount = $model->amount;
            }

            if ($today >= $start && $today <= $end) {
                $return = ['is_available' => true, 'message' => 'This Coupon apply successfully.', 'discount' => $amount, 'coupon_uuid' => $model->uuid];
            } else {
                $return['message'] = "This coupon is expired or not available.";
            }
        }
        return $return;
    }

    public static function make_cart_submit_array($postdata = []) {

        $stripe_array = [];
        $student_detail = Student::findOne(Yii::$app->user->identity->student_uuid);
        if (isset($postdata['StudentTuitionBooking'])) {
            $stripe_array = ['stripeToken' => $postdata['stripeToken']];
            $step_data = StudentTuitionBooking::getSession(Yii::$app->user->identity->student_uuid);
            $cart_total = 0;
            $discount = 0;
            $monthly_sub_total = 0;

            if ($student_detail->isMonthlySubscription == 1) {
                $monthly_sub_check = '0';
            } else {
                $monthly_sub_check = $postdata['StudentTuitionBooking']['monthly_sub_checkbox'];
            }

            $monthly_sub_uuid = $postdata['StudentTuitionBooking']['monthly_sub_uuid'];
            $coupon_code = $postdata['StudentTuitionBooking']['coupon_code'];


            $tutorDetail = Tutor::findOne($step_data['step_2']['tutor_uuid']);

            $package_type_model = OnlineTutorialPackageType::find()->where(['uuid' => $tutorDetail->online_tutorial_package_type_uuid])->one();
            $package_type = $package_type_model->name;
            $packageDetail = PremiumStandardPlanRate::find()->joinWith(['onlineTutorialPackageUu'])->where(['premium_standard_plan_rate.uuid' => $step_data['step_3']['onlinetutorial_uuid']])->one();


            if ($package_type == "PREMIUM") {

                $plan_price = $packageDetail->total_price;
            } else {

                $planDetail = StandardPlanOptionRate::find()->where(['premium_standard_plan_rate_uuid' => $step_data['step_3']['onlinetutorial_uuid'], 'option' => $step_data['step_3']['option']])->one();
                $plan_price = $planDetail->total_price;
                $plan_base_price = $planDetail->price;
                $plan_option = $planDetail->option;

                //Get id of tutorwise paln 
                $tutorwise_model = OnlineTutorialPackageTutorwise::find()->where(['premium_standard_plan_rate_uuid' => $step_data['step_3']['onlinetutorial_uuid'], 'tutor_uuid' => $step_data['step_2']['tutor_uuid'], 'instrument_uuid' => $step_data['step_1']['instrument_uuid']])->one();
                $online_tutorial_package_tutorwise_uuid = $tutorwise_model->uuid;

                //Standard tutor loking period
                $locking_per_model_count = StudentTuitionStandardBookingLockingPeriod::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid, 'tutor_uuid' => $step_data['step_2']['tutor_uuid'], 'instrument_uuid' => $step_data['step_1']['instrument_uuid']])->count();

                if ($locking_per_model_count > 0) {

                    $locking_per_model = StudentTuitionStandardBookingLockingPeriod::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid, 'tutor_uuid' => $step_data['step_2']['tutor_uuid'], 'instrument_uuid' => $step_data['step_1']['instrument_uuid']])->one();
                    $locking_end_datetime = $locking_per_model->locking_end_datetime;
                    $locking_start_datetime = $locking_per_model->locking_start_datetime;
                    $locking_option = $locking_per_model->option;

                    $current_date = date('Y-m-d H:i:s');

                    if (($plan_option < $locking_option) && ($current_date <= $locking_end_datetime)) {
                        $lockingplanDetail = StandardPlanOptionRate::find()->where(['premium_standard_plan_rate_uuid' => $step_data['step_3']['onlinetutorial_uuid'], 'option' => $locking_option])->one();
                        $plan_price = $lockingplanDetail->total_price;
                    }
                }
            }

            if ($monthly_sub_check == '1') {
                //$monthly_model = MonthlySubscriptionFee::find()->where(['with_tutorial' => 1, 'status' => 'ENABLED'])->one();//change by pooja for comman price for manthly subscription
                $monthly_model = MonthlySubscriptionFee::find()->where(['status' => 'ENABLED'])->one();
                if (!empty($monthly_model)) {
                    $monthly_sub_total = $monthly_model->total_price;
                }
            }

            $model_coupon = StudentTuitionBooking::check_coupon_available($coupon_code, $plan_price);
            if ($model_coupon['is_available']) {
                $discount = $model_coupon['discount'];
            }

            $cart_total = (($plan_price + $monthly_sub_total ) - $discount);

            $stripe_array['monthly_sub_checkbox'] = $monthly_sub_check;
            $stripe_array['coupon_code'] = $coupon_code;
            $stripe_array['creative_kids_voucher_code'] = $postdata['StudentTuitionBooking']['creative_kids_voucher_code'];
            $stripe_array['kids_name'] = $postdata['StudentTuitionBooking']['kids_name'];
            $stripe_array['kids_dob'] = $postdata['StudentTuitionBooking']['kids_dob'];
            $stripe_array['coupon_uuid'] = $postdata['StudentTuitionBooking']['coupon_uuid'];
            $stripe_array['monthly_sub_uuid'] = $monthly_sub_uuid;
            $stripe_array['monthly_sub_total'] = $monthly_sub_total;
            $stripe_array['discount'] = $discount;
            $stripe_array['cart_total'] = $cart_total;
            $stripe_array['card_name'] = $postdata['StudentTuitionBooking']['cc_name'];
            $stripe_array['card_number'] = $postdata['StudentTuitionBooking']['cc_number'];
            $stripe_array['card_cvv'] = $postdata['StudentTuitionBooking']['cc_cvv'];
            $stripe_array['card_month'] = date('m', strtotime($postdata['StudentTuitionBooking']['cc_month_year']));
            $stripe_array['card_year'] = date('Y', strtotime($postdata['StudentTuitionBooking']['cc_month_year']));
            $stripe_array['cc_checkbox'] = (isset($postdata['StudentTuitionBooking']['cc_checkbox'])) ? $postdata['StudentTuitionBooking']['cc_checkbox'] : 0;
            $stripe_array['plan_name'] = $packageDetail->name;
            $stripe_array['plan_price'] = $plan_price;
            $stripe_array['student_ID'] = $student_detail->enrollment_id;
            $stripe_array['student_name'] = $student_detail->first_name . ' ' . $student_detail->last_name;
        }

        return $stripe_array;
    }

    public static function stripePayment($stripe_array) {

        $return = ['code' => 421, 'message' => 'Something went wrong.'];
        $student = Student::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid])->one();
        $step_data = StudentTuitionBooking::getSession(Yii::$app->user->identity->student_uuid);

        if (!empty($stripe_array['stripeToken'])) {

            $token = $stripe_array['stripeToken'];
            $email = $student->email;

            //$dollar_amount = (int) $stripe_array['cart_total'];
            $dollar_amount = (float) $stripe_array['cart_total'];
            $cents_amount = $dollar_amount * 100;   // 1 AUD = 100 cents
            //include Stripe PHP library
            require Yii::$app->basePath . '/vendor/stripe/stripe-php/init.php';

            //set api key
            $stripe = array(
                "secret_key" => Yii::$app->params['stripe']['secret_key'],
                "publishable_key" => Yii::$app->params['stripe']['publishable_key']
            );

            try {
                \Stripe\Stripe::setApiKey($stripe['secret_key']);
                $customer = \Stripe\Customer::create(array(
                            'email' => $email,
                            'source' => $token
                ));

                //item information
                $itemName = $stripe_array['plan_name'];
                $orderID = General::generateTransactionID();
                // $itemDescription = ($stripe_array['monthly_sub_checkbox'] == '1') ? $stripe_array['plan_name'] . " with Monthly Subscription" : $stripe_array['plan_name'];
                $itemDescription = ($stripe_array['monthly_sub_checkbox'] == '1') ? $orderID . ' - ' . $stripe_array['student_ID'] . ' - ' . $stripe_array['student_name'] . ' - Tutorial Package Type and Monthly Subscription' : $stripe_array['student_ID'] . ' - Tutorial Package Type Purchased';
                $itemPrice = $cents_amount;
                $currency = "aud";
//                $orderID = mt_rand(100000, 999999);
                //charge a credit or a debit card
                $charge = \Stripe\Charge::create(array(
                            'customer' => $customer->id,
                            'amount' => $itemPrice,
                            'currency' => $currency,
                            'description' => $itemDescription,
                            'metadata' => array(
                                'order_id' => $orderID
                            )
                ));
            } catch (\Stripe\Error\ApiConnection $e) {
                return $return = ['code' => 421, 'message' => 'Network problem, perhaps try again.'];
                //Yii::$app->session->setFlash('danger', Yii::t('app', 'Network problem, perhaps try again.'));
                //return \yii\web\Controller::redirect(['index']);
            } catch (\Stripe\Error\InvalidRequest $e) {
                return $return = ['code' => 421, 'message' => 'Your request is invalid.'];
                //Yii::$app->session->setFlash('danger', Yii::t('app', 'Your request is invalid.'));
                //return \yii\web\Controller::redirect(['index']);
            } catch (\Stripe\Error\Api $e) {
                return $return = ['code' => 421, 'message' => 'Stripe servers are down!'];
                //Yii::$app->session->setFlash('danger', Yii::t('app', 'Stripe servers are down!'));
                //return \yii\web\Controller::redirect(['index']);
            } catch (\Stripe\Error\Card $e) {
                return $return = ['code' => 421, 'message' => 'This Card is Declined.'];
                //Yii::$app->session->setFlash('danger', Yii::t('app', 'This Card is Declined.'));
                //return \yii\web\Controller::redirect(['index']);
            }

            $chargeJson = $charge->jsonSerialize();

//echo "<pre>chargeJson";
//print_r($chargeJson);exit;

            if ($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1) {
                //pooja
                $planDetail = PremiumStandardPlanRate::find()->joinWith(['paymentTermUu', 'tutorialTypeUu'])->where(['premium_standard_plan_rate.uuid' => $step_data['step_3']['onlinetutorial_uuid']])->one();
                $tutorDetail = Tutor::findOne($step_data['step_2']['tutor_uuid']);

                $package_type_model = OnlineTutorialPackageType::find()->where(['uuid' => $tutorDetail->online_tutorial_package_type_uuid])->one();
                $package_type = $package_type_model->name;
                $packageDetail = PremiumStandardPlanRate::find()->joinWith(['onlineTutorialPackageUu'])->where(['premium_standard_plan_rate.uuid' => $step_data['step_3']['onlinetutorial_uuid']])->one();

                if ($package_type == "PREMIUM") {

                    $plan_total_price = $packageDetail->total_price;
                    $plan_price = $packageDetail->price;
                    $plan_gst = $packageDetail->gst;
                    $plan_gst_description = $packageDetail->gst_description;
                    $plan_option = 0;
                } else {
                    $planoptionDetail = StandardPlanOptionRate::find()->where(['premium_standard_plan_rate_uuid' => $step_data['step_3']['onlinetutorial_uuid'], 'option' => $step_data['step_3']['option']])->one();
                    $plan_total_price = $planoptionDetail->total_price;
                    $plan_price = $planoptionDetail->price;
                    $plan_gst = $planoptionDetail->gst;
                    $plan_gst_description = $planoptionDetail->gst_description;
                    $plan_option = $planoptionDetail->option;

                    //Get id of tutorwise paln 
                    $tutorwise_model = OnlineTutorialPackageTutorwise::find()->where(['premium_standard_plan_rate_uuid' => $step_data['step_3']['onlinetutorial_uuid'], 'tutor_uuid' => $step_data['step_2']['tutor_uuid'], 'instrument_uuid' => $step_data['step_1']['instrument_uuid']])->one();
                    $online_tutorial_package_tutorwise_uuid = $tutorwise_model->uuid;

                    //Standard tutor loking period
                    $locking_per_model_count = StudentTuitionStandardBookingLockingPeriod::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid, 'tutor_uuid' => $step_data['step_2']['tutor_uuid'], 'instrument_uuid' => $step_data['step_1']['instrument_uuid']])->count();

                    if ($locking_per_model_count > 0) {

                        $locking_per_model = StudentTuitionStandardBookingLockingPeriod::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid, 'tutor_uuid' => $step_data['step_2']['tutor_uuid'], 'instrument_uuid' => $step_data['step_1']['instrument_uuid']])->one();
                        $locking_end_datetime = $locking_per_model->locking_end_datetime;
                        $locking_start_datetime = $locking_per_model->locking_start_datetime;
                        $locking_option = $locking_per_model->option;
                        $current_date = date('Y-m-d H:i:s');

                        if (($plan_option < $locking_option) && ($current_date <= $locking_end_datetime)) {

                            $lockingplanDetail = StandardPlanOptionRate::find()->where(['premium_standard_plan_rate_uuid' => $step_data['step_3']['onlinetutorial_uuid'], 'option' => $locking_option])->one();
                            $plan_total_price = $lockingplanDetail->total_price;
                            $plan_price = $lockingplanDetail->price;
                            $plan_gst = $lockingplanDetail->gst;
                            $plan_gst_description = $lockingplanDetail->gst_description;
                            $plan_option = $locking_option;
                        } elseif (($plan_option > $locking_option) || (($current_date > $locking_end_datetime))) {
                            $locking_end_date = date("Y-m-d", strtotime("+6 months", strtotime(date("Y-m-d"))));

                            $locking_per_model->locking_start_datetime = date('Y-m-d') . ' 00:00:00';
                            $locking_per_model->locking_end_datetime = $locking_end_date . ' 23:59:59';
                            $locking_per_model->booking_datetime = date('Y-m-d H:i:s');
                            $locking_per_model->option = $plan_option;
                            $locking_per_model->online_tutorial_package_tutorwise_uuid = $online_tutorial_package_tutorwise_uuid;
                            $locking_per_model->premium_standard_plan_rate_uuid = $step_data['step_3']['onlinetutorial_uuid'];

                            $locking_per_model->save(false);
                        }
                    } else {
                        $locking_end_date = date("Y-m-d", strtotime("+6 months", strtotime(date("Y-m-d"))));
                        $locking_per_model_add = new StudentTuitionStandardBookingLockingPeriod();
                        $locking_per_model_add->student_uuid = Yii::$app->user->identity->student_uuid;
                        $locking_per_model_add->instrument_uuid = $step_data['step_1']['instrument_uuid'];
                        $locking_per_model_add->tutor_uuid = $step_data['step_2']['tutor_uuid'];
                        $locking_per_model_add->premium_standard_plan_rate_uuid = $step_data['step_3']['onlinetutorial_uuid'];
                        $locking_per_model_add->option = $plan_option;
                        $locking_per_model_add->booking_datetime = date('Y-m-d H:i:s');
                        $locking_per_model_add->locking_start_datetime = date('Y-m-d') . ' 00:00:00';
                        $locking_per_model_add->locking_end_datetime = $locking_end_date . ' 23:59:59';
                        $locking_per_model_add->online_tutorial_package_tutorwise_uuid = $online_tutorial_package_tutorwise_uuid;
                        $locking_per_model_add->save(false);
                    }
                }


                $booking_uuid = NULL;
                $save_m_s_flag = false;

                //Transaction_payment
                $modelPayment = new PaymentTransaction();
                $modelPayment->student_uuid = Yii::$app->user->identity->student_uuid;
                $modelPayment->transaction_number = $orderID;
                $modelPayment->payment_datetime = date("Y-m-d H:i:s");
                $modelPayment->amount = $chargeJson['amount'] / 100;
                $modelPayment->payment_method = 'Stripe';
                $modelPayment->reference_number = $chargeJson['balance_transaction'];
                $modelPayment->status = $chargeJson['status'];
                $modelPayment->book_uuid = $booking_uuid;
                $modelPayment->response = json_encode($chargeJson);
                $modelPayment->save(false);

                //student_tuition_booking table entry
                $modelSTBooking = new StudentTuitionBooking();
                $modelSTBooking->booking_id = General::generateBookingID();
                $modelSTBooking->booking_datetime = date('Y-m-d H:i:s');
                $modelSTBooking->student_uuid = Yii::$app->user->identity->student_uuid;
                $modelSTBooking->tutor_uuid = $step_data['step_2']['tutor_uuid'];
                $modelSTBooking->instrument_uuid = $step_data['step_1']['instrument_uuid'];
                $modelSTBooking->tutorial_name = $packageDetail->name;
                $modelSTBooking->tutorial_description = $packageDetail->description;
                $modelSTBooking->tutorial_type_code = $planDetail->tutorialTypeUu->code;
                $modelSTBooking->tutorial_min = $planDetail->tutorialTypeUu->min;
                $modelSTBooking->payment_term_code = $planDetail->paymentTermUu->code;
                $modelSTBooking->total_session = $planDetail->duration;
                $modelSTBooking->session_length_day = $planDetail->tutorialTypeUu->days;
                $modelSTBooking->price = $plan_price;
                $modelSTBooking->gst = $plan_gst;
                $modelSTBooking->gst_description = $plan_gst_description;
                $modelSTBooking->total_price = $plan_total_price;
                $modelSTBooking->status = "BOOKED";
                $modelSTBooking->monthly_subscription_price = $stripe_array['monthly_sub_total'];
                $modelSTBooking->student_monthly_subscription_uuid = NULL;
                $modelSTBooking->coupon_uuid = (!empty($stripe_array['coupon_uuid'])) ? $stripe_array['coupon_uuid'] : NULL;

                $modelSTBooking->creative_kids_voucher_code = (!empty($stripe_array['creative_kids_voucher_code'])) ? $stripe_array['creative_kids_voucher_code'] : NULL;
                $modelSTBooking->kids_name = (!empty($stripe_array['kids_name'])) ? $stripe_array['kids_name'] : NULL;
                $modelSTBooking->kids_dob = (!empty($stripe_array['kids_dob'])) ? date("Y-m-d", strtotime($stripe_array['kids_dob'])) : NULL;


                $modelSTBooking->discount = $stripe_array['discount'];
                $modelSTBooking->grand_total = $chargeJson['amount'] / 100;
                $modelSTBooking->plan_rate_option = $plan_option;
                $modelSTBooking->premium_standard_plan_rate_uuid = $step_data['step_3']['onlinetutorial_uuid'];
                if ($modelSTBooking->save(false)) {


                    if ((!empty($stripe_array['coupon_uuid']))) {
                        $coupon_log = new CouponLog();
                        $coupon_log->booking_uuid = $modelSTBooking->uuid;
                        $coupon_log->booking_id = $modelSTBooking->booking_id;
                        $coupon_log->booking_datetime = $modelSTBooking->booking_datetime;
                        $coupon_log->student_uuid = Yii::$app->user->identity->student_uuid;
                        $coupon_log->tutor_uuid = $step_data['step_2']['tutor_uuid'];
                        $coupon_log->instrument_uuid = $step_data['step_1']['instrument_uuid'];
                        $coupon_log->total_price = $planDetail->total_price;
                        $coupon_log->coupon_uuid = (!empty($stripe_array['coupon_uuid'])) ? $stripe_array['coupon_uuid'] : NULL;
                        $coupon_log->discount = $stripe_array['discount'];
                        $coupon_log->grand_total = $chargeJson['amount'] / 100;
                        $coupon_log->transaction_uuid = $modelPayment->uuid;
                        $coupon_log->save(false);
                    }


                    //Booked Session entry
                    $session_date_array = explode(',', $step_data['step_4']['all_dates']);

                    //update Transaction Booking id
                    $modelPaymentUpdate = PaymentTransaction::find()->where(['uuid' => $modelPayment->getPrimaryKey()])->one();
                    $modelPaymentUpdate->book_uuid = $modelSTBooking->getPrimaryKey();
                    $modelPaymentUpdate->save(false);

                    if ($modelSTBooking->total_session > 0) {

                        for ($i = 0; $i < $modelSTBooking->total_session; $i++) {


                            $start_hour = $step_data['step_4']['start_hour'];
                            $start_datetime = date('Y-m-d H:i:s', strtotime($session_date_array[$i] . " " . $start_hour));

                            $end_datetime = date('Y-m-d H:i:s', strtotime($start_datetime . " +" . $modelSTBooking->tutorial_min . " minutes"));
                            $digits = 4;
                            $pin_sql = "SELECT random_num
FROM (
  SELECT SUBSTRING(FLOOR(random() * 9999 + 10000)::text, 2) AS random_num 
  FROM booked_session
  UNION
  SELECT SUBSTRING(FLOOR(random() * 9999 + 10000)::text, 2) AS random_num
) AS numbers_mst_plus_1
WHERE random_num NOT IN (SELECT session_pin FROM booked_session  WHERE session_pin IS NOT NULL)
LIMIT 1";

                            $pin_res = Yii::$app->db->createCommand($pin_sql)->queryOne();
                            $pin = $pin_res['random_num'];

                            //$pin = str_pad(rand(0, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT);

                            $modelBookedSession = new BookedSession();
                            $modelBookedSession->booking_uuid = $modelSTBooking->getPrimaryKey();
                            $modelBookedSession->student_uuid = $modelSTBooking->student_uuid;
                            $modelBookedSession->tutor_uuid = $modelSTBooking->tutor_uuid;
                            $modelBookedSession->instrument_uuid = $modelSTBooking->instrument_uuid;
                            $modelBookedSession->tutorial_type_code = $modelSTBooking->tutorial_type_code;
                            $modelBookedSession->start_datetime = General::convertUserToSystemTimezone($start_datetime);
                            $modelBookedSession->end_datetime = General::convertUserToSystemTimezone($end_datetime);
                            $modelBookedSession->session_min = $modelSTBooking->tutorial_min;
                            $modelBookedSession->session_pin = $pin;
                            $modelBookedSession->status = 'SCHEDULE';
                            $modelBookedSession->save(false);
                        }
                    }


                    //cc save or not
                    if ($stripe_array['cc_checkbox'] == 1) {
                        $modelCC = StudentCreditCard::find()->where(['student_uuid' => $modelSTBooking->student_uuid, 'status' => 'ENABLED'])->one();
                        if (empty($modelCC)) {
                            $modelCC = new StudentCreditCard();
                        }
                        $modelCC->student_uuid = $modelSTBooking->student_uuid;
                        $modelCC->number = General::encrypt_str($stripe_array['card_number']);
                        $modelCC->cvv = General::encrypt_str($stripe_array['card_cvv']);
                        $modelCC->expire_date = date('Y-m-t', strtotime($stripe_array['card_year'] . "-" . $stripe_array['card_month'] . "-01"));
                        $modelCC->name = $stripe_array['card_name'];
                        $modelCC->status = 'ENABLED';
                        $modelCC->stripe_customer_id = $chargeJson['customer'];
                        $modelCC->save(false);
                    }

                    //if Monthly subscription plan then entry in it.
                    if ($stripe_array['monthly_sub_checkbox'] == 1) {

                        $MonthlySub = MonthlySubscriptionFee::find()->where(['uuid' => $stripe_array['monthly_sub_uuid']])->one();

                        $modelStudentSubscription = StudentSubscription::find()->where(['student_uuid' => $modelSTBooking->student_uuid])->one();

                        $start_date = date('Y-m-d H:i:s');
                        if ($student->isMonthlySubscription == 1) {
                            if (!empty($modelStudentSubscription)) {
                                $start_date = date('Y-m-d H:i:s', strtotime($modelStudentSubscription->next_renewal_date));
                            }
                        }

                        //$start_date = (!empty($modelStudentSubscription)) ? date('Y-m-d H:i:s', strtotime($modelStudentSubscription->next_renewal_date)) : date('Y-m-d H:i:s');
                        $end_date = date('Y-m-d H:i:s', strtotime($start_date . " +30 days"));
                        $next_renewal_date = date('Y-m-d', strtotime($end_date . " +1 days"));

                        $modelStudentMonthlySub = new StudentMonthlySubscription();
                        $modelStudentMonthlySub->student_uuid = $modelSTBooking->student_uuid;
                        $modelStudentMonthlySub->name = $MonthlySub->name;
                        $modelStudentMonthlySub->price = $MonthlySub->price;
                        $modelStudentMonthlySub->tax = $MonthlySub->tax;
                        $modelStudentMonthlySub->total_price = $MonthlySub->total_price;
                        $modelStudentMonthlySub->description = $MonthlySub->description;
                        $modelStudentMonthlySub->with_tutorial = $MonthlySub->with_tutorial;
                        $modelStudentMonthlySub->status = 'ENABLED';
                        $modelStudentMonthlySub->start_datetime = General::convertUserToSystemTimezone($start_date);
                        $modelStudentMonthlySub->end_datetime = General::convertUserToSystemTimezone($end_date);
                        if ($modelStudentMonthlySub->save(false)) {

                            $up_st_booking = StudentTuitionBooking::findOne($modelSTBooking->getPrimaryKey());
                            if (!empty($up_st_booking)) {

                                $up_st_booking->student_monthly_subscription_uuid = $modelStudentMonthlySub->uuid;
                                $up_st_booking->save(false);

                                $modelSTBooking->student_monthly_subscription_uuid = $modelStudentMonthlySub->uuid;
                            }

                            $save_m_s_flag = true;
                        }
                        $monthly_item = ['item' => $modelStudentMonthlySub->name, 'booking_uuid' => NULL, 'student_monthly_subscription_uuid' => $modelStudentMonthlySub->uuid, 'tax' => $modelStudentMonthlySub->tax, 'price' => $modelStudentMonthlySub->price, 'total' => $modelStudentMonthlySub->total_price];

                        if (empty($modelStudentSubscription)) {
                            $modelStudentSubscription = new StudentSubscription();
                        }
                        $modelStudentSubscription->student_uuid = $modelSTBooking->student_uuid;
                        $modelStudentSubscription->subscription_uuid = $modelStudentMonthlySub->uuid;
                        $modelStudentSubscription->next_renewal_date = $next_renewal_date;
                        $modelStudentSubscription->save(false);

                        //update student monthly sub flag
                        $student->isMonthlySubscription = 1;
                        $student->isCancelMonthlySubscriptionRequest = 0;
                        $student->save(false);
                    }

                    $item_array[0] = ['item' => $modelSTBooking->tutorial_name, 'booking_uuid' => $modelSTBooking->uuid, 'student_monthly_subscription_uuid' => NULL, 'tax' => $modelSTBooking->gst, 'price' => $modelSTBooking->price, 'total' => $modelSTBooking->total_price];
                    $order_total = $item_array[0]['total'];
                    if ($stripe_array['monthly_sub_checkbox'] == 1 && isset($monthly_item)) {
                        $item_array[1] = $monthly_item;
                        $order_total = $order_total + $monthly_item['total'];
                    }
                    $order_array['student_uuid'] = $modelSTBooking->student_uuid;
                    $order_array['payment_uuid'] = $modelPayment->getPrimaryKey();
                    $order_array['discount'] = $modelSTBooking->discount;
                    $order_array['item'] = $item_array;
                    $order_array['total'] = $order_total;
                    $order_array['grand_total'] = $order_total - $order_array['discount'];

                    //create order
                    $order_result = StudentTuitionBooking::generate_order($order_array);

                    self::bookedTutionMail($modelSTBooking->student_uuid, $modelSTBooking->getPrimaryKey(), $order_result, $save_m_s_flag);

                    if ($save_m_s_flag) {
                        //StudentMonthlySubscription::MonthlySubscriptionBuyMail($modelStudentMonthlySub,$modelSTBooking->student_uuid);
                    }
                }

                return $return = ['code' => 200, 'message' => 'success'];
            } else {
                return $return = ['code' => 421, 'message' => 'Transaction has been failed.'];
                //Yii::$app->session->setFlash('success', Yii::t('app', 'Transaction has been failed.'));
                //return \yii\web\Controller::redirect(['index']);
            }
        } else {
            return $return = ['code' => 421, 'message' => 'Form submission error.'];
            //Yii::$app->session->setFlash('success', Yii::t('app', 'Form submission error.'));
            //return \yii\web\Controller::redirect(['index']);
        }

        return $return;
    }

    public static function bookedTutionMail($student_uuid, $booking_uuid, $order_array, $ismonthlysubscription = false) {

        if (!empty($student_uuid) && !empty($booking_uuid)) {

            $modelSTBooking = StudentTuitionBooking::find()->where(['uuid' => $booking_uuid])->one();
            $modelStudent = Student::find()->where(['student_uuid' => $student_uuid])->one();
            if (!empty($modelSTBooking)) {

                $modelTutor = Tutor::find()->where(['uuid' => $modelSTBooking->tutor_uuid])->one();
                $modelInstrument = Instrument::find()->where(['uuid' => $modelSTBooking->instrument_uuid])->one();

                $tutor_user_model = User::find()->where(['tutor_uuid' => $modelSTBooking->tutor_uuid])->one();
                $student_user_model = User::find()->where(['student_uuid' => $student_uuid])->one();
                $tutor_tz = (!empty($tutor_user_model)) ? $tutor_user_model->timezone : Yii::$app->params['timezone'];
                $student_tz = (!empty($student_user_model)) ? $student_user_model->timezone : Yii::$app->params['timezone'];
                $system_tz = Yii::$app->params['timezone'];

                $session_dates = $session_dates_tutor = [];
                $session_time = $session_time_tutor = '';
                $modelAllTusion = BookedSession::find()->where(['booking_uuid' => $booking_uuid])->all();
                if (!empty($modelAllTusion)) {
                    foreach ($modelAllTusion as $key => $value) {
//                        $session_dates[] = General::displayDate($value->start_datetime);
//                        $session_time = General::displayTime($value->start_datetime).' - '.General::displayTime($value->end_datetime);
                        $session_dates[] = General::convertTimezone($value->start_datetime, $system_tz, $student_tz, 'T d-m-Y');
                        $session_time = General::convertTimezone($value->start_datetime, $system_tz, $student_tz, 'h:i A') . ' - ' . General::convertTimezone($value->end_datetime, $system_tz, $student_tz, 'h:i A');

                        $session_dates_tutor[] = General::convertTimezone($value->start_datetime, $system_tz, $tutor_tz, 'T d-m-Y');
                        $session_time_tutor = General::convertTimezone($value->start_datetime, $system_tz, $tutor_tz, 'h:i A') . ' - ' . General::convertTimezone($value->end_datetime, $system_tz, $tutor_tz, 'h:i A');
                    }
                }

                $tutor_name = $modelTutor->first_name;
                $student_name = $modelStudent->first_name;
                $package_payment_term = '';
                if ($modelSTBooking->payment_term_code == 'Q') {
                    $package_payment_term = 'Quarterly';
                } elseif ($modelSTBooking->payment_term_code == 'M') {
                    $package_payment_term = 'Monthly';
                } elseif ($modelSTBooking->payment_term_code == 'F') {
                    $package_payment_term = 'Fortnightly';
                }

                $content['creative_kids_voucher_code'] = $modelSTBooking->creative_kids_voucher_code;
                $content['student_name'] = $student_name;
                $content['student_last_name'] = $modelStudent->last_name;
                $content['booking_id'] = $modelSTBooking->booking_id;
                $content['tutor_name'] = $tutor_name;
                $content['tutor_last_name'] = $modelTutor->last_name;
                $content['tutor_contact_number'] = $modelTutor->phone;
                $content['tutor_email'] = $modelTutor->email;
                $content['tutorial_name'] = $modelSTBooking->tutorial_name;
                $content['package_payment_term'] = $package_payment_term;
                $content['instrument'] = $modelInstrument->name;
                $content['tution_duration'] = $modelSTBooking->tutorial_min;
                $content['time'] = $session_time;
                $content['session_dates'] = $session_dates;
                $content['monthly_subscription_price'] = $modelSTBooking->monthly_subscription_price;
                $content['student_monthly_subscription_uuid'] = $modelSTBooking->student_monthly_subscription_uuid;
                $content['grand_total'] = $modelSTBooking->grand_total;
                $content['total_price'] = $modelSTBooking->total_price;
                $content['discount'] = $modelSTBooking->discount;
                $content['order_id'] = (isset($order_array['order_id'])) ? "#" . $order_array['order_id'] : "";
                $isInvoiceFile = (isset($order_array['order_file'])) ? $order_array['order_file'] : "";

                $content_tutor['tutor_name'] = $tutor_name;
                $content_tutor['tutor_last_name'] = $modelTutor->last_name;
                $content_tutor['student_name'] = $student_name;
                $content_tutor['student_last_name'] = $modelStudent->last_name;
                $content_tutor['student_contact_number'] = $modelStudent->phone_number;
                $content_tutor['student_email'] = $modelStudent->email;
                $content_tutor['booking_id'] = $modelSTBooking->booking_id;
                $content_tutor['tutorial_name'] = $modelSTBooking->tutorial_name;
                $content_tutor['tution_duration'] = $modelSTBooking->tutorial_min;
                $content_tutor['instrument'] = $modelInstrument->name;
                $content_tutor['time'] = $session_time_tutor;
                $content_tutor['session_dates'] = $session_dates_tutor;
                $content_tutor['student_monthly_subscription_uuid'] = $modelSTBooking->student_monthly_subscription_uuid;

                $content_admin['creative_kids_voucher_code'] = $modelSTBooking->creative_kids_voucher_code;
                $content_admin['student_name'] = $student_name;
                $content_admin['student_last_name'] = $modelStudent->last_name;
                $content_admin['booking_id'] = $modelSTBooking->booking_id;
                $content_admin['purchased_package'] = $modelSTBooking->tutorial_name;
                $content_admin['payment_term'] = ($modelSTBooking->session_length_day == 14) ? $modelSTBooking->total_session . " fortnights" : $modelSTBooking->total_session . " weeks";
                $content_admin['tutor_name'] = $tutor_name;
                $content_admin['tutor_last_name'] = $modelTutor->last_name;
                $content_admin['tutor_contact_number'] = $modelTutor->phone;
                $content_admin['tutor_email'] = $modelTutor->email;
                $content_admin['tutorial_name'] = $modelSTBooking->tutorial_name;
                $content_admin['package_payment_term'] = $package_payment_term;
                $content_admin['instrument'] = $modelInstrument->name;
                $content_admin['tution_duration'] = $modelSTBooking->tutorial_min;
                $content_admin['time'] = $session_time;
                $content_admin['session_dates'] = $session_dates;
                $content_admin['monthly_subscription_price'] = $modelSTBooking->monthly_subscription_price;
                $content_admin['student_monthly_subscription_uuid'] = $modelSTBooking->student_monthly_subscription_uuid;
                $content_admin['grand_total'] = $modelSTBooking->grand_total;
                $content_admin['total_price'] = $modelSTBooking->total_price;
                $content_admin['discount'] = $modelSTBooking->discount;
                $content_admin['order_id'] = (isset($order_array['order_id'])) ? "#" . $order_array['order_id'] : "";


                $ownersEmail = General::getOwnersEmails(['OWNER', 'ADMIN']);

                $mail = Yii::$app->mailer->compose('booked_tution_student_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                        ->setFrom(Yii::$app->params['supportEmail'])
                        ->setTo($modelStudent->email)
                        //->setBcc($ownersEmail)
                        //->setSubject(ucfirst(strtolower(Yii::$app->name)) . ' Notification:  Confirmation of Payment and Booking – Receipt '.$content['order_id'] );
                        ->setSubject(ucfirst(strtolower(Yii::$app->name)) . ' Notification:  Confirmation of Payment and Booking');

                if (is_file($isInvoiceFile)) {
                    $mail->attach($isInvoiceFile);
                }
                $mail->send();


                Yii::$app->mailer->compose('booked_tution_tutor_mail', ['content' => $content_tutor], ['htmlLayout' => 'layouts/html'])
                        ->setFrom(Yii::$app->params['supportEmail'])
                        ->setTo($modelTutor->email)
                        //->setBcc($ownersEmail)
                        ->setSubject(ucfirst(strtolower(Yii::$app->name)) . ' Notification: A student has purchased lessons with you')
                        ->send();

                $mail = Yii::$app->mailer->compose('booked_tution_admin_mail', ['content' => $content_admin], ['htmlLayout' => 'layouts/html'])
                        ->setFrom(Yii::$app->params['supportEmail'])
                        ->setTo($ownersEmail)
                        ->setSubject(ucfirst(strtolower(Yii::$app->name)) . ' Notification: New Student Booking: #' . $content_admin['booking_id'] . ' : ' . $content_admin['student_name']);

                if (is_file($isInvoiceFile)) {
                    $mail->attach($isInvoiceFile);
                }
                $mail->send();
            }
        }

        return true;
    }

    public static function generate_order($order_array) {

        if (!empty($order_array)) {

            $modelOrder = new Order();
            $modelOrder->order_id = General::generateOrderID();
            $modelOrder->student_uuid = $order_array['student_uuid'];
            $modelOrder->payment_uuid = $order_array['payment_uuid'];
            $modelOrder->order_datetime = date('Y-m-d H:i:s');
            $modelOrder->total = $order_array['total'];
            $modelOrder->discount = $order_array['discount'];
            $modelOrder->grand_total = $order_array['grand_total'];
            if ($modelOrder->save(false)) {

                if (!empty($order_array['item'])) {
                    foreach ($order_array['item'] as $key => $item) {

                        $modelOrderItem = new OrderItem();
                        $modelOrderItem->order_uuid = $modelOrder->getPrimaryKey();
                        $modelOrderItem->item = $item['item'];
                        $modelOrderItem->booking_uuid = $item['booking_uuid'];
                        $modelOrderItem->student_monthly_subscription_uuid = $item['student_monthly_subscription_uuid'];
                        $modelOrderItem->price = $item['price'];
                        $modelOrderItem->tax = $item['tax'];
                        $modelOrderItem->total = $item['total'];
                        $modelOrderItem->save(false);

                        if (!empty($item['booking_uuid'])) {
                            $modelSTBooking = StudentTuitionBooking::find()->where(['uuid' => $item['booking_uuid']])->one();
                            if (!empty($modelSTBooking)) {
                                $modelSTBooking->order_uuid = $modelOrder->getPrimaryKey();
                                $modelSTBooking->save(false);
                            }
                        }

                        if (!empty($item['student_monthly_subscription_uuid'])) {
                            $modelStudentMonthlySub = StudentMonthlySubscription::find()->where(['uuid' => $item['student_monthly_subscription_uuid']])->one();
                            if (!empty($modelStudentMonthlySub)) {
                                $modelStudentMonthlySub->order_uuid = $modelOrder->getPrimaryKey();
                                $modelStudentMonthlySub->save(false);
                            }
                        }
                        if (!empty($item['student_musicoin_package_uuid'])) {
                            $modelStudentMusicoinPackage = StudentMusicoinPackage::find()->where(['uuid' => $item['student_musicoin_package_uuid']])->one();
                            if (!empty($modelStudentMusicoinPackage)) {
                                $modelStudentMusicoinPackage->order_uuid = $modelOrder->getPrimaryKey();
                                $modelStudentMusicoinPackage->save(false);
                            }
                        }
                    }
                }
            }

            $filename = 'Invoice_' . $modelOrder->order_id . '.pdf';
            $pdf_res = self::create_pdf($filename, $modelOrder->getPrimaryKey());

            $modelStudent = Student::find()->where(['student_uuid' => $order_array['student_uuid']])->one();

            $content['student_name'] = $modelStudent->first_name;
            $content['order_uuid'] = $modelOrder->getPrimaryKey();
            $content['order_id'] = $modelOrder->order_id;
            $content['order_array'] = $order_array;
            $content['order_date'] = General::displayDate($modelOrder->order_datetime);
            $content['order_file'] = $pdf_res['file'];

            /* $ownersEmail = General::getOwnersEmails(['OWNER']);

              $mail = Yii::$app->mailer->compose('new_order', ['content' => $content], ['htmlLayout' => 'layouts/html'])
              ->setFrom(Yii::$app->params['supportEmail'])
              ->setTo($modelStudent->email)
              ->setBcc($ownersEmail)
              ->setSubject('New Order - ' . Yii::$app->name);

              if (is_file($pdf_res['file'])) {
              $mail->attach($pdf_res['file']);
              }
              $mail->send(); */

            return $content;
        }
        return [];
    }

    /**
     * This function use generate PDF file for Invoice.
     *
     */
    public static function create_pdf($filename = '', $order_uuid) {

        $file = Yii::$app->params['media']['invoice']['path'] . $filename;

        $model = \app\models\Order::findOne($order_uuid);

        if (!empty($model)) {

            $order_item = \app\models\OrderItem::find()->where(['order_uuid' => $order_uuid])->asArray()->all();

            $content = Yii::$app->controller->renderPartial('//order/invoice_pdf', ['model' => $model, 'order_item' => $order_item]);

            $filename = ( $filename == '') ? 'Invoice_' . $model->order_id . '.pdf' : $filename;
            $file = Yii::$app->params['media']['invoice']['path'] . $filename;

            if (!is_dir(Yii::$app->params['media']['invoice']['path'])) {
                FileHelper::createDirectory(Yii::$app->params['media']['invoice']['path']);
            }

            $css_content = file_get_contents(Yii::$app->params['theme_assets']['url'] . 'plugins/bootstrap/css/bootstrap.min.css');
            $css_content .= file_get_contents(Yii::$app->params['theme_assets']['url'] . 'css/style.css');
            $css_content .= ".invoice-title{width: 20%;}.invoice-head-info{width: 20%;font-size: 14px !important;}..invoice-head-info span{font-size: 14px !important;} .invoice-head-info{width:20%;font-family: 'Open Sans', Arial, Helvetica, sans-serif;line-height: 23px;font-style: normal;font-weight: normal;font-size: 10px !important;} .invoice-logo{max-width: 20%;}";

            $params = [
                'mode' => Pdf::MODE_CORE,
                'format' => Pdf::FORMAT_A4,
                'orientation' => Pdf::ORIENT_PORTRAIT,
                'destination' => Pdf::DEST_FILE,
                'content' => $content,
                'filename' => Yii::$app->params['media']['invoice']['path'] . $filename,
                'tempPath' => 'mpdf/',
//                'cssFile' => '@web/theme_assets/css/style.css',
                'cssInline' => '.kv-heading-1{font-size:12px}',
                'cssInline' => $css_content,
                'options' => ['title' => 'Invoice'],
                'methods' => [
                    'SetHeader' => ['MUSICONN'],
                    'SetFooter' => ['{PAGENO}'],
                ]
            ];
            $pdf = new Pdf($params);
            $pdf->render();

            if (is_file($file)) {

                $model->invoice_file = $filename;
                $model->save(false);

                return ['status' => '1', 'file' => $file, 'filename' => $filename];
            } else {
                return ['status' => '0', 'file' => $file, 'filename' => $filename];
            }
        } else {
            return ['status' => '0', 'file' => $file, 'filename' => $filename];
        }
    }

    public static function bookCreditTution($step_data = [], $student_uuid = '') {

        $return = ['code' => 421, 'message' => 'Something went wrong.'];
        if (!empty($step_data['step_1']['instrument_uuid']) && !empty($step_data['step_3']['tutorial_type_code']) && !empty($step_data['step_2']['tutor_uuid']) && !empty($step_data['step_4']['all_dates']) && !empty($step_data['step_4']['start_hour']) && !empty($student_uuid)
        ) {

            $modelCreditTution = CreditSession::find()->where(['student_uuid' => $student_uuid, 'tutorial_type_code' => $step_data['step_3']['tutorial_type_code'], 'status' => 'PENDING'])->one();

            if (!empty($modelCreditTution)) {

                $TutorialType = TutorialType::find()->where(['code' => $step_data['step_3']['tutorial_type_code'], 'status' => 'ENABLED'])->one();

                if (!empty($TutorialType)) {

                    $modelStudent = Student::findOne($student_uuid);
                    $modelTutor = Tutor::findOne($step_data['step_2']['tutor_uuid']);
                    $modelInstrument = Instrument::findOne($step_data['step_1']['instrument_uuid']);

                    $session_date_array = explode(',', $step_data['step_4']['all_dates']);
                    $start_hour = $step_data['step_4']['start_hour'];
                    $start_datetime = date('Y-m-d H:i:s', strtotime($session_date_array[0] . " " . $start_hour));

                    $end_datetime = date('Y-m-d H:i:s', strtotime($start_datetime . " +" . $TutorialType->min . " minutes"));
                    $digits = 4;
                    $pin_sql = "SELECT random_num
FROM (
  SELECT SUBSTRING(FLOOR(random() * 9999 + 10000)::text, 2) AS random_num 
  FROM booked_session
  UNION
  SELECT SUBSTRING(FLOOR(random() * 9999 + 10000)::text, 2) AS random_num
) AS numbers_mst_plus_1
WHERE random_num NOT IN (SELECT session_pin FROM booked_session  WHERE session_pin IS NOT NULL)
LIMIT 1";

                    $pin_res = Yii::$app->db->createCommand($pin_sql)->queryOne();
                    $pin = $pin_res['random_num'];

                    //$pin = str_pad(rand(0, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT);

                    $modelBookedSession = new BookedSession();
                    $modelBookedSession->booking_uuid = $modelCreditTution->booking_uuid;
                    $modelBookedSession->credit_session_uuid = $modelCreditTution->getPrimaryKey();
                    $modelBookedSession->student_uuid = $student_uuid;
                    $modelBookedSession->tutor_uuid = $step_data['step_2']['tutor_uuid'];
                    $modelBookedSession->instrument_uuid = $step_data['step_1']['instrument_uuid'];
                    $modelBookedSession->tutorial_type_code = $step_data['step_3']['tutorial_type_code'];
                    $modelBookedSession->start_datetime = General::convertUserToSystemTimezone($start_datetime);
                    $modelBookedSession->end_datetime = General::convertUserToSystemTimezone($end_datetime);
                    $modelBookedSession->session_min = $TutorialType->min;
                    $modelBookedSession->session_pin = $pin;
                    $modelBookedSession->status = 'SCHEDULE';
                    $modelBookedSession->save(false);

                    //update credit session entry
                    $modelCreditTution->status = 'BOOKED';
                    $modelCreditTution->updated_at = date('Y-m-d H:i:s');
                    $modelCreditTution->save(false);

                    // return array set
                    $return = ['code' => 200, 'message' => 'success'];

                    //send mail
                    $tutor_user_model = User::find()->where(['tutor_uuid' => $step_data['step_2']['tutor_uuid']])->one();
                    $student_user_model = User::find()->where(['student_uuid' => $student_uuid])->one();
                    $tutor_tz = (!empty($tutor_user_model)) ? $tutor_user_model->timezone : Yii::$app->params['timezone'];
                    $student_tz = (!empty($student_user_model)) ? $student_user_model->timezone : Yii::$app->params['timezone'];
                    $system_tz = Yii::$app->params['timezone'];

                    $student_name = $modelStudent->first_name . ' ' . $modelStudent->last_name;
                    $tutor_name = $modelTutor->first_name . ' ' . $modelTutor->last_name;
                    //$session_time = General::displayTime($start_datetime) . ' - ' . General::displayTime($end_datetime);
                    //$session_dates[0] = General::displayDate($start_datetime);

                    $session_dates[0] = General::convertTimezone($modelBookedSession->start_datetime, $system_tz, $student_tz, 'T d-m-Y');
                    $session_time = General::convertTimezone($modelBookedSession->start_datetime, $system_tz, $student_tz, 'h:i A') . ' - ' . General::convertTimezone($modelBookedSession->end_datetime, $system_tz, $student_tz, 'h:i A');

                    $session_dates_tutor[0] = General::convertTimezone($modelBookedSession->start_datetime, $system_tz, $tutor_tz, 'T d-m-Y');
                    $session_time_tutor = General::convertTimezone($modelBookedSession->start_datetime, $system_tz, $tutor_tz, 'h:i A') . ' - ' . General::convertTimezone($modelBookedSession->end_datetime, $system_tz, $tutor_tz, 'h:i A');

                    $content['student_name'] = $student_name;
                    $content['tutor_name'] = $tutor_name;
                    $content['instrument'] = $modelInstrument->name;
                    $content['tution_duration'] = $TutorialType->min;
                    $content['time'] = $session_time;
                    $content['session_dates'] = $session_dates;

                    $content_tutor['tutor_name'] = $tutor_name;
                    $content_tutor['student_name'] = $student_name;
                    $content_tutor['instrument'] = $modelInstrument->name;
                    $content_tutor['tution_duration'] = $TutorialType->min;
                    $content_tutor['time'] = $session_time_tutor;
                    $content_tutor['session_dates'] = $session_dates_tutor;

                    $ownersEmail = General::getOwnersEmails(['OWNER']);

                    Yii::$app->mailer->compose('booked_credit_tution_student', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                            ->setFrom(Yii::$app->params['supportEmail'])
                            ->setTo($modelStudent->email)
                            ->setBcc($ownersEmail)
                            ->setSubject('Booked New Credited Lesson - ' . Yii::$app->name)
                            ->send();


                    Yii::$app->mailer->compose('booked_credit_tution_tutor', ['content' => $content_tutor], ['htmlLayout' => 'layouts/html'])
                            ->setFrom(Yii::$app->params['supportEmail'])
                            ->setTo($modelTutor->email)
                            ->setBcc($ownersEmail)
                            ->setSubject('Booked New Lesson - ' . Yii::$app->name)
                            ->send();
                } else {
                    $return = ['code' => 421, 'message' => 'This tutorial type is not available.'];
                }
            } else {
                $return = ['code' => 421, 'message' => 'You have not credit Tuion for this tutorial type.'];
            }
        }
        return $return;
    }

    public static function bookingFirstLastSession($booking_uuid) {

        $return = ['start' => '', 'end' => ''];
        $model = BookedSession::find()->where(['booking_uuid' => $booking_uuid])->asArray()->orderBy('start_datetime ASC')->all();

        if (!empty($model)) {
            $end_array = end($model);
            $return['start'] = (isset($model[0]['start_datetime'])) ? General::displayDate($model[0]['start_datetime']) : '';
            $return['end'] = (isset($end_array['start_datetime'])) ? General::displayDate($end_array['start_datetime']) : '';
        }
        return $return;
    }

    //This function return hours range between start and end time
    public static function get_hours_range($start, $end, $format = 'h:i A') {
        $times = array();
        $start_time = strtotime($start);
        $end_time = strtotime($end);
        // $end_time =  strtotime('-60 minutes', $end);
        $step = "1800";
        $range = range($start_time, $end_time, $step);
        foreach (range($start_time, $end_time, $step) as $timestamp) {
            $hour_mins = date('h:i A', $timestamp);
            if (!empty($format)) {
                $times[$hour_mins] = date('h:i A', $timestamp);
            } else
                $times[$hour_mins] = $hour_mins;
        }
        return $times;
    }

    //This function return key and value in time as [11:00] => [11:00 AM]
    public static function array_tutor_time($array) {
        if (!is_array($array)) {
            return FALSE;
        }
        $result = array();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $result = array_merge($result, self::array_tutor_time($value));
            } else {
                $result[$key] = $value;
            }
        }
        return $result;
    }

    //This function returns tutor booked session
    public static function tutorSession($tutor_uuid, $student_tz, $tutor_tz, $session_date) {
        $tutor_session = BookedSession::find()->where(['tutor_uuid' => $tutor_uuid])->andWhere(['status' => 'SCHEDULE'])->andwhere(['=', "to_char(start_datetime, 'YYYY-MM-DD')", $session_date])->asArray()->all();
        $session_start = [];
        $session_end = [];
        $session_start_time = [];
        $session_end_time = [];
        $session_all = [];
        foreach ($tutor_session as $key => $tutor_session_val) {
            $start_datetime = General::convertTimezone($tutor_session_val['start_datetime'], $student_tz, $tutor_tz, 'Y-m-d H:i');
            $end_datetime = General::convertTimezone($tutor_session_val['end_datetime'], $student_tz, $tutor_tz, 'Y-m-d H:i');
            $session_start[] = (isset($tutor_session_val['start_datetime'])) ? $start_datetime : '';
            $session_end[] = (isset($tutor_session_val['end_datetime'])) ? $end_datetime : '';
            foreach ($session_start as $key => $session_start_val) {
                $session_start_time[] = $session_start_val;
            }
            foreach ($session_end as $key => $session_end_val) {
                $session_end_time[] = $session_end_val;
            }
        }
        foreach (array_combine($session_start_time, $session_end_time) as $sesion_s_time => $sesion_e_time) {
            $session_all[] = self::get_hours_range($sesion_s_time, $sesion_e_time, 'h:i A');
        }
        $session_final = self::array_tutor_time($session_all);
        return $session_final;
    }

    //This function returns tutor break time
    public static function tutorBreaks($time) {
        $break_start = [];
        $break_end = [];
        $break_start_time = [];
        $break_end_time = [];
        $break_all = [];

        foreach ($time as $key => $breaks_val) {
            $break_start[] = $breaks_val['start'];
            $break_end[] = $breaks_val['end'];
            foreach ($break_start as $key => $break_start_val) {
                $break_start_time[] = $break_start_val;
            }
            foreach ($break_end as $key => $break_end_val) {
                $break_end_time[] = $break_end_val;
            }
        }
        foreach (array_combine($break_start_time, $break_end_time) as $break_s_time => $break_e_time) {
            $break_all[] = self::get_hours_range($break_s_time, $break_e_time, 'h:i A');
        }
        $break_final = self::array_tutor_time($break_all);
        return $break_final;
    }

    //This function returns tutor unavailable time
    public static function tutorUnavailable($tutor_uuid, $student_tz, $tutor_tz, $session_date) {
        $unavailable = TutorUnavailability::find()->where(['tutor_uuid' => $tutor_uuid])->andwhere(['=', "to_char(start_datetime, 'YYYY-MM-DD')", $session_date])->asArray()->all();
        $unavailable_start = [];
        $unavailable_end = [];
        $unavailable_start_time = [];
        $unavailable_end_time = [];
        $unavailable_all = [];
        foreach ($unavailable as $key => $uanavailable_val) {
            $unavailable_start[] = General::convertTimezone($uanavailable_val['start_datetime'], $student_tz, $tutor_tz, 'Y-m-d H:i');
            $unavailable_end[] = General::convertTimezone($uanavailable_val['end_datetime'], $student_tz, $tutor_tz, 'Y-m-d H:i');
            foreach ($unavailable_start as $key => $unavailable_start_val) {
                $unavailable_start_time[] = $unavailable_start_val;
            }
            foreach ($unavailable_end as $key => $unavailable_end_val) {
                $unavailable_end_time[] = $unavailable_end_val;
            }
        }
        foreach (array_combine($unavailable_start_time, $unavailable_end_time) as $unavailable_s_time => $unavailable_e_time) {
            $unavailable_all[] = self::get_hours_range($unavailable_s_time, $unavailable_e_time, 'h:i A');
        }
        $unavailable_final = self::array_tutor_time($unavailable_all);
        return $unavailable_final;
    }

    //This function returns student booked session
    public static function studentSession($student_uuid, $student_tz, $tutor_tz, $session_date) {
        $student_session = BookedSession::find()->where(['student_uuid' => $student_uuid])->andWhere(['status' => 'SCHEDULE'])->andwhere(['=', "to_char(start_datetime, 'YYYY-MM-DD')", $session_date])->asArray()->all();
        $session_start = [];
        $session_end = [];
        $session_start_time = [];
        $session_end_time = [];
        $session_all = [];
        foreach ($student_session as $key => $student_session_val) {
            $start_datetime = General::convertTimezone($student_session_val['start_datetime'], $student_tz, $tutor_tz, 'Y-m-d H:i');
            $end_datetime = General::convertTimezone($student_session_val['end_datetime'], $student_tz, $tutor_tz, 'Y-m-d H:i');
            $session_start[] = (isset($student_session_val['start_datetime'])) ? $start_datetime : '';
            $session_end[] = (isset($student_session_val['end_datetime'])) ? $end_datetime : '';
            foreach ($session_start as $key => $session_start_val) {
                $session_start_time[] = $session_start_val;
            }
            foreach ($session_end as $key => $session_end_val) {
                $session_end_time[] = $session_end_val;
            }
        }
        foreach (array_combine($session_start_time, $session_end_time) as $sesion_s_time => $sesion_e_time) {
            $session_all[] = self::get_hours_range($sesion_s_time, $sesion_e_time, 'h:i A');
        }
        $session_final = self::array_tutor_time($session_all);
        return $session_final;
    }

    //This function returns reschedule booked session
    public static function rescheduleSession($tutor_uuid, $student_tz, $tutor_tz, $session_date) {
        $reschedule_session = RescheduleSession::find()->where(['tutor_uuid' => $tutor_uuid])->andWhere(['status' => 'AWAITING'])->andwhere(['=', "to_char(to_start_datetime, 'YYYY-MM-DD')", $session_date])->asArray()->all();
        $reschedule_session_start = [];
        $reschedule_session_end = [];
        $reschedule_session_start_time = [];
        $reschedule_session_end_time = [];
        $reschedule_session_all = [];
        foreach ($reschedule_session as $key => $reschedule_session_val) {
            $start_datetime = General::convertTimezone($reschedule_session_val['start_datetime'], $student_tz, $tutor_tz, 'Y-m-d H:i');
            $end_datetime = General::convertTimezone($reschedule_session_val['end_datetime'], $student_tz, $tutor_tz, 'Y-m-d H:i');
            $reschedule_session_start[] = (isset($reschedule_session_val['start_datetime'])) ? $start_datetime : '';
            $reschedule_session_end[] = (isset($reschedule_session_val['end_datetime'])) ? $end_datetime : '';
            foreach ($reschedule_session_start as $key => $reschedule_session_start_val) {
                $reschedule_session_start_time[] = $reschedule_session_start_val;
            }
            foreach ($reschedule_session_end as $key => $session_end_val) {
                $reschedule_session_end_time[] = $session_end_val;
            }
        }
        foreach (array_combine($reschedule_session_start_time, $reschedule_session_end_time) as $sesion_s_time => $sesion_e_time) {
            $reschedule_session_all[] = self::get_hours_range($sesion_s_time, $sesion_e_time, 'h:i A');
        }
        $reschedule_session_final = self::array_tutor_time($reschedule_session_all);
        return $reschedule_session_final;
    }

    //This function used for tine converted into different timezone
    public static function timezoneTime($break_session_unavailability_result, $student_tz, $tutor_tz) {
        $time = [];
        foreach ($break_session_unavailability_result as $key => $new_time) {
            $convert_time_key = General::convertTime($new_time, $tutor_tz, $student_tz, 'H:i');
            $convert_time_value = General::convertTime($new_time, $tutor_tz, $student_tz, 'h:i A');

            $time[$convert_time_key] = $convert_time_value;
        }
        return $time;
    }

    public static function get_tutor_available_hours($tutor_uuid, $tutor_working_plan, $session_date, $session_minute, $student_uuid = '', $tutor_tz, $student_tz) {
        $system_tz = Yii::$app->params['timezone'];
        $session_date = date('Y-m-d', strtotime($session_date));

        $empty_periods = self::get_tutor_available_time_period($tutor_uuid, $tutor_working_plan, $session_date, $student_uuid, $tutor_tz, $student_tz);
        //echo "<pre>";print_r($empty_periods);exit;
        $availabilities_type = 'flexible';
        $service_duration = $session_minute;
        $available_hours = [];


        $available_hours = self::_calculate_tutor_available_hours($empty_periods, $session_date, $session_minute, filter_var(false, FILTER_VALIDATE_BOOLEAN), 'flexible', $tutor_tz, $student_tz);
//echo "<pre>";print_r($available_hours);exit;
//         if (General::convertTimezone($session_date,$student_tz,$student_tz,'Y-m-d') === General::convertTimezone(date('Y-m-d'),$system_tz,$student_tz,'Y-m-d')) {
//             $book_advance_timeout = Yii::$app->params['book_advance_timeout'];
//
//             foreach ($available_hours as $index => $value) {
//                 $available_hour = strtotime($value);
//                 $current_hour = strtotime('+' . $book_advance_timeout . ' minutes', strtotime('now'));
//                 if ($available_hour <= $current_hour) {
//                     unset($available_hours[$index]);
//                 }
//             }
//         }


        $available_hours = $available_hours;
        ksort($available_hours, SORT_STRING);
        return $available_hours;
    }

    protected function _calculate_tutor_available_hours(
    array $empty_periods, $selected_date, $service_duration, $manage_mode = FALSE, $availabilities_type = 'flexible', $tutor_tz, $student_tz
    ) {
        $available_hours = [];
        $available_hours_full_date = [];
//echo "_calculate_tutor_available_hours<br>".$selected_date;
        foreach ($empty_periods as $period) {


            $period['start'] = (date("H:i", strtotime($period['start'])) == "23:59") ? date("Y-m-d H:i", strtotime($period['start'] . " +1 minute")) : $period['start'];
            $period['end'] = (date("H:i", strtotime($period['end'])) == "23:59") ? date("Y-m-d H:i", strtotime($period['end'] . " +1 minute")) : $period['end'];

            $start_hour = new \DateTime($period['start'], new \DateTimeZone($student_tz));
            $end_hour = new \DateTime($period['end'], new \DateTimeZone($student_tz));

            $interval = $availabilities_type === Yii::$app->params['availabilities_type_fixed'] ? (int) $service_duration : 30;

            $current_hour = $start_hour;
            $diff = $current_hour->diff($end_hour);

            while (($diff->h * 60 + $diff->i) >= intval($service_duration)) {
//            while (($diff->h * 60 + $diff->i) >= intval($service_duration) || ($diff->h * 60 + $diff->i) >= intval($service_duration-1)) {
                if ($current_hour->format('Y-m-d') == $selected_date) {
                    $available_hours[$current_hour->format('H:i')] = $current_hour->format('h:i A');
                    $available_hours_full_date[$current_hour->format('Y-m-d H:i')] = $current_hour->format('d-m-Y h:i A');
                }
//                    $available_hours[$current_hour->format('H:i')] = $current_hour->format('h:i A');

                $current_hour->add(new \DateInterval('PT' . $interval . 'M'));
                $diff = $current_hour->diff($end_hour);
            }
        }

        return $available_hours;
    }

    /* public static function active_inactive_booking_week_day($tutor_uuid = '') {

      if ((($model = Tutor::findOne($tutor_uuid)) !== null)){
      $model->working_plan

      }

      $array = ['sunday' => 0, 'monday' => 1, 'tuesday' => 2, 'wednesday' => 3, 'thursday' => 4, 'friday' => 5, 'saturday' => 6];
      if ($day != '') {
      unset($array[$day]);
      }
      return implode(',', array_values($array));
      } */

    public static function get_tutor_disabled_week_day($tutor_working_plan_json = '') {

        $array = ['sunday' => 0, 'monday' => 1, 'tuesday' => 2, 'wednesday' => 3, 'thursday' => 4, 'friday' => 5, 'saturday' => 6];
        if (!empty($tutor_working_plan_json)) {

            $tutor_working_plan = json_decode($tutor_working_plan_json, true);
            $array_1 = $array;
            foreach ($array_1 as $key => $value) {
                if (!empty($tutor_working_plan[$key])) {
                    unset($array[$key]);
                }
            }
        }
        return implode(',', array_values($array));
    }

}
