<?php
 
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">
    <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Code</th>
                <th>Term</th>
                <th>Unit</th>
		<th>Status</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            if (!empty($paymentTermList)) { 
                foreach ($paymentTermList as $key => $value) { 
                    ?>
                    <tr>
                        <td><?= $key + 1; ?></td>
                        <td><?= $value['code']; ?></td>
                        <td><?= $value['term']; ?></td>
                        <td><?= $value['unit']; ?></td>
			<td>
                            <?php
                            if ($value['status'] == 'ENABLED') {
                                $e_class = "badge-primary";
                                $pointer = 'style="cursor:pointer;color:#fff"';
                                $e_link_class = "paymentTerm_disabled_link";
                            } elseif ($value['status'] == 'DISABLED') {
                                $e_class = "badge-secondary";
                                $pointer = 'style="cursor:pointer;color:#fff"';
                                $e_link_class = "paymentTerm_enabled_link";
                            } else {
                                $e_class = "badge-danger";
                                $pointer = '';
                                $e_link_class = "";
                            }
                            ?>
                            <span class="badge badge-pill <?= $e_class ?> <?= $e_link_class ?>"  data-uuid="<?= $value['uuid']; ?>" <?= $pointer ?>><?= $value['status']; ?></span>
                        </td>
                        
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>

</div>
