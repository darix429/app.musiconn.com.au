<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;


$this->title = Yii::t('app', 'Tutor Reset Password');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tutors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs('
    
    $(document).on("click","#getpassword", function(){ 
        $.ajax({
            type: "GET",
            url: "' . Url::to(['/tutor/generate-password/']) . '",
            success: function (result) { 
                    $("#changepassword-newpassword").val(result);
            },
        });
    });

    $(document).bind("cut copy paste","#changepassword-newpassword",function(e){
        e.preventDefault();
    });
    
    $(document).bind("cut copy paste","#changepassword-retypepassword",function(e){
        e.preventDefault();
    });
    
//    $("#form_reset_password").submit(function (event) { 
//                    //submit from callback
//                    blockBody();
//            });
            
//    $(".back-loader").click(function(){ 
//         blockBody();
//    });
//    
    ');

?>

<div class="col-xl-12 col-lg-12 col-12 col-md-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Please Fill Information</h2>
            <div class="actions panel_actions float-right">
                <!--<i class="box_toggle fa fa-chevron-down"></i>-->
                <a href="<?= Yii::$app->getUrlManager()->createUrl(['/tutor/index/']) ?>" class="btn btn-info btn-icon "><i class="fa fa-arrow-left text-white"></i> <span>Back</span></a>
            </div>
        </header>
        <div class="content-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12">

                    <?php $form = ActiveForm::begin([
                        'action' => Yii::$app->getUrlManager()->createUrl(['/tutor/reset-password/','id'=>$model->uuid]),
                        'id' => 'form_reset_password',
                        ]); ?>
                        <div class="form-row">
                            <div class="col-md-6 mb-0">
                                <div class="form-group ">
                                    <label class="control-label" >Name</label>
                                    <span class="form-control bg-gray-field" aria-required="true" aria-invalid="true"><?= $model->first_name . ' ' . $model->last_name; ?></span>
                                </div>
                            </div>
                            <div class="col-md-6 mb-0">
                                <div class="form-group ">
                                    <label class="control-label" >Username</label>
                                    <span class="form-control bg-gray-field" aria-required="true" aria-invalid="true"><?= $model->email; ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-0">
                                <?=
                                $form->field($modelCP, 'newPassword', ['template' => "{label}\n<div class='input-group'>"
                                    . "{input}<div class='input-group-append'>"
                                    . "<a class='btn btn-warning' id='getpassword' title='Generate Password'>"
                                    . "<i class='fa fa-refresh text-white'></i>&nbsp;"
                                    . "</a>"
                                    . "</div>"
                                    . "</div>\n{hint}\n{error}",
                                    'labelOptions' => ['class' => 'control-label']]
                                )->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $modelCP->getAttributeLabel('newPassword')])
                                ;
                                ?>
                                <?php //echo $form->field($modelCP, 'newPassword')->passwordInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $modelCP->getAttributeLabel('newPassword')]) ?>
                            </div>
                            <div class="col-md-6 mb-0">
                                <?= $form->field($modelCP, 'retypePassword')->passwordInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $modelCP->getAttributeLabel('retypePassword')]) ?>
                            </div>                                                       
                        </div>

                        <div class="form-row">
                            <div class="col-md-12 mb-0">
                                <?= Html::submitButton(Yii::t('app', 'Change'), ['class' => 'btn btn-primary', 'style' => 'float: right;']) ?>
                            </div>
                        </div>
                    <?php ActiveForm::end(); ?>

                </div>
            </div>

        </div>
    </section>
</div>


