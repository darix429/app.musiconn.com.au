<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">

    <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Name</th>
                <th>Value</th>
                <th>Action</th>
            </tr>
        </thead>

        <tbody>
            <?php
            if (!empty($settingList)) { 
                foreach ($settingList as $key => $value) {
 
                    ?>
                    <tr>
                        <td><?= $key + 1; ?></td>
                        <td><?= $value['name']; ?></td>
                        <td><?= $value['value']; ?></td>
                        <td>
                            	<a href="<?= Url::to(['/settings/update/', 'id' => $value['uuid']]) ?>" class="badge badge-info badge-md" title="Update"><i class="fa fa-pencil"></i></a>
                              
                                
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>

</div>
