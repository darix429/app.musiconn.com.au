<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Student */

$this->title = $model->student_uuid;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Students'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->student_uuid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->student_uuid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'student_uuid',
            'first_name:ntext',
            'last_name:ntext',
            'email:ntext',
            'phone_number:ntext',
            'skype:ntext',
            'street:ntext',
            'city:ntext',
            'state:ntext',
            'country:ntext',
            'postal_code:ntext',
            'status:ntext',
            'enrollment_id:ntext',
            'birthdate:ntext',
            'birthyear:ntext',
        ],
    ]) ?>

</div>
