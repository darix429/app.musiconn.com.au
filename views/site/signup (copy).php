<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \mdm\admin\models\form\Signup */

$this->title = 'Sign Up';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-wrapper">
    <div id="login" class="login loginpage offset-xl-4 col-xl-4 offset-lg-3 col-lg-6 offset-md-3 col-md-6 col-offset-0 col-12">
        <h2 class="login-logo-text"><a href="#" title="Login Page" tabindex="-1"><?= Yii::$app->name; ?></a></h2>
        <!--<h1><a href="#" title="Login Page" tabindex="-1" style="background-image:url('theme_assets/images/login-logo.png');"><?= Yii::$app->name; ?></a></h1>-->
        <p>Please fill out the following fields to signup:</p>
        <?= Html::errorSummary($model)?>
        <?php
        $form = ActiveForm::begin([
                    'id' => 'form-signup',
                    'options' => ['name' => 'signupform'],
        ]);
        ?>

        <?= $form->field($model, 'username')->textInput(['autofocus' => false, 'class' => 'input']) ?>
        <?= $form->field($model, 'email')->textInput(['autofocus' => false, 'class' => 'input']) ?>
        <?= $form->field($model, 'password')->passwordInput(['class' => 'input']) ?>
        
	<p class="submit">
            <?= Html::submitButton('Sign Up', ['class' => 'btn btn-orange btn-block', 'name' => 'signup-button']) ?>
            <!--<input type="submit" name="wp-submit" id="wp-submit" class="btn btn-orange btn-block" value="Sign In" />-->
        </p>

        <?php ActiveForm::end(); ?>
        <p id="nav">            
            <?= Html::a('Go to SignIn', ['site/login'],['class'=>'float-left']) ?>
        </p>


    </div>
</div>
