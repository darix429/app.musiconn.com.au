<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\ArrayHelper;
use app\models\InstrumentCategory;
use app\models\Instrument;

Yii::$app->session['status'] = 'published';

$this->title = Yii::t('app', 'CATEGORY WISE VIDEOS DEMO');
$this->params['breadcrumbs'][] = $this->title;


$categoryArray = ArrayHelper::map(InstrumentCategory::find()->asArray()->all(), 'uuid', 'name');
$instrumentArray = ArrayHelper::map(Instrument::find()->where(['status' => 'ACTIVE'])->asArray()->all(), 'uuid', 'name');

$this->registerJs(
  '
    var toggler = document.getElementsByClassName("caret");
    var i;

    for (i = 0; i < toggler.length; i++) {
      toggler[i].addEventListener("click", function() {
        this.parentElement.querySelector(".nested").classList.toggle("active");
        this.classList.toggle("caret-down");
      });
    }

    function videoShow(id){
      var url = "' . Url::to(['test/getvideo']) . '?id="+id;
      $.ajax({
        type: "GET",
        url: url,
        beforeSend: function() {
          blockBody();
        },
        success:function(data){
          $("ul.nested.active").css({"display":""});
          unblockBody();
          if($(".datatable-search-div").height() >= 250){
            $("ul.nested.active").children().children().children().css({"width": "172.25"});
            // const ps = new PerfectScrollbar(".datatable-search-div", {
            //   "suppressScrollY": true
            // });
            // console.log(ps);
            $(".datatable-search-div").css({"overflow-y": "scroll"});
          } else {
            // const ps = new PerfectScrollbar(".datatable-search-div", {
            //   "suppressScrollY" : false
            // });
            $(".datatable-search-div").css({"overflow-y": ""});
          }
          $("#pre_recorded_video_table_parent_div").html(data);
        },
        error:function(e){
          unblockBody();
          showErrorMessage(e.responseText);
        }
      });
    }

    function videoAllShow(){
      var url = "' . Url::to(['test/getvideo']) . '?id=";
      $.ajax({
        type: "GET",
        url: url,
        beforeSend: function() {
          blockBody();
        },
        success:function(data){
          unblockBody();
          $("#pre_recorded_video_table_parent_div").html(data);
        },
        error:function(e){
          unblockBody();
          showErrorMessage(e.responseText);
        }
      });
    }
', View::POS_END
);
?>
<style>
ul, #myUL {
  list-style-type: none;
}

#myUL {
  margin: 0;
  padding: 0;
}

.caret {
  cursor: pointer;
  -webkit-user-select: none; /* Safari 3.1+ */
  -moz-user-select: none; /* Firefox 2+ */
  -ms-user-select: none; /* IE 10+ */
  user-select: none;
}

.caret::before {
  content: "\25B6";
  color: black;
  display: inline-block;
  margin-right: 6px;
}

.caret-down::before {
  -ms-transform: rotate(90deg); /* IE 9 */
  -webkit-transform: rotate(90deg); /* Safari */'
  transform: rotate(90deg);  
}

.nested {
  display: none;
}

.active {
  display: block;
}
</style>
<div class="col-xl-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">CATEGORY WISE VIDEOS</h2>
            <div class="actions panel_actions float-right">
                <!-- <i class="fa fa-search icon-primary icon-xs icon-orange mail_head_icon custom-toggle" data-togglediv="datatable-search-div" title="Advance Search"></i> -->
            </div>
        </header>
        <div class="content-body"> 
            <div class="row ">
                <div class="col-lg-3 col-md-3 col-3">
                    <!-- <div class="datatable-search-div " style="display: none;"> -->
                    <div class="datatable-search-div">
                        <ul id="myUL" class="wraplist">
                            <li style="padding: 5px;"><a href="#" id="all_video" data-name="Allvideo" onclick=videoAllShow();><i class="fa fa-home"></i> All Videos</a></li>
                            <?php echo InstrumentCategory::getCategoryTreeHtml(); ?>
                        </ul>
                        
                        
                    </div>
                </div>

                <div class="col-md-9 col-sm-9 col-xs-9" id="pre_recorded_video_table_parent_div">
                <?php echo Yii::$app->controller->renderPartial('_get_video_list', ['videoList' => $videoList ]); ?>
                </div>


            </div>
            <br>
            <div class=""></div>
            <div class="clearfix"></div>
            
        </div>
    </section> 
</div>

