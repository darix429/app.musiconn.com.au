<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Hi <?= $content["receiverName"] ?>,</p>
            
            <p>You have <strong><?= $content["missedCount"] ?></strong> unread messages from <?= $content["senderName"] ?></p>
            
            <p>Please do not reply to this unattended mailbox.  Any queries or feedback can be directed to admin@musiconn.com.au.</p>

            <p>Regards,<br>
            Admin Team – Musiconn Pty Ltd</p>
        </td>
    </tr>
</table>
