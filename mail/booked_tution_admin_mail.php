<?php

use yii\helpers\Html;

?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear Musiconn Admin,</p>
            
            <p>A new student has just purchased a lesson package.  Below are details of lessons booked:</p>
                        
            <p style="text-decoration: underline;"><strong>Scheduled tutorials</strong></p>
            
            <p><strong>Booking Reference :</strong> <?= "#".$content['booking_id']; ?></p>
            <p><strong>Purchased Package :</strong> <?= $content['purchased_package']; ?></p>
            <p><strong>Payment Term :</strong> <?= $content['payment_term']; ?></p>
            <p><strong>Payment Received :</strong> <?= $content['grand_total']; ?></p>
            <p><strong>Tutor Name :</strong> <?= $content['tutor_name']." ".$content['tutor_last_name']; ?></p>
            <p><strong>Student Name :</strong> <?= $content['student_name']." ".$content['student_last_name']; ?></p>
            <p><strong>Instrument :</strong> <?= $content['instrument']; ?></p>
            
            <p><strong>Scheduled Lessons :</strong> 
                <?php if(is_array($content['session_dates'])){ ?>
                <ul>
                    <?php foreach ($content['session_dates'] as $key => $value) {
                        echo "<li>".$value." ".$content['time']."</li>";
                    } ?>
                </ul>
                <?php } ?>
            </p>
            <p><strong>Subscription to online contents :</strong> <?= (!empty($content['student_monthly_subscription_uuid']) ) ? "Yes" : "No"; ?></p>
            
            <p><strong>Creative Kids Voucher submitted :</strong> <?= (!empty($content['creative_kids_voucher_code']) ) ? "Yes" : "No"; ?></p>
            
            <p>You can find the invoice and receipt as record of your payment by logging on the <a target="_blank" href="https://app.musiconn.com.au/">booking system</a>.</p>
            
            <p>For terms and conditions of your purchase, please refer to the <a target="_blank" href="https://www.musiconn.com.au/">Musiconn website</a>.</p>
            
            <p>Please do not reply to this unattended mailbox.</p>
            
            
            <p>Regards,<br>
            Julian Fung <br>
            CEO – Musiconn Pty Ltd</p>
        </td>
    </tr>
</table>

