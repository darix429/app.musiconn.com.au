<?php

namespace app\controllers;

use Yii;
use app\models\PaymentTerm;
use app\models\PaymentTermSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
//use app\models\User;
use app\libraries\General;
use yii\helpers\Url;
use yii\filters\AccessControl;
use app\models\ChangePassword;

/**
 * PaymentTermController implements the CRUD actions for PaymentTerm model.
 */
class PaymentTermController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
	    'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                        [
                        'actions' => ['index'],
                        'allow' => Yii::$app->user->can('/payment-term/index'),
                        'roles' => ['@'],
                    ],
			[
                        'actions' => ['enabled'],
                        'allow' => Yii::$app->user->can('/payment-term/enabled'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['disabled'],
                        'allow' => Yii::$app->user->can('/payment-term/disabled'),
                        'roles' => ['@'],
                    ],
                        
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PaymentTerm models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PaymentTermSearch();
        $searchModel->status = "ENABLED";
        $paymentTermList = $searchModel->search(Yii::$app->request->queryParams,true);

	if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_payment_term_list', [
                        'paymentTermList' => $paymentTermList,
            ]);
        } else {
            return $this->render('index', [
                        //'inactiveAdmin' => $inactiveAdmin,
                        'paymentTermList' => $paymentTermList,
                        'searchModel' => $searchModel,
            ]);
        }
        /*return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);*/
    }

    /**
     * Displays a single PaymentTerm model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PaymentTerm model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    /*public function actionCreate()
    {
        $model = new PaymentTerm();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->uuid]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }*/

    /**
     * Updates an existing PaymentTerm model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    /*public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->uuid]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }*/

    /**
     * Deletes an existing PaymentTerm model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    /*public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }*/

    /**
     * Finds the PaymentTerm model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return PaymentTerm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PaymentTerm::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionDisabled($id) { 

        if (($paymentTerm = PaymentTerm::findOne($id)) !== null) {

            if ($paymentTerm->status == "ENABLED") { 

                
                    $paymentTerm->status = 'DISABLED';
                    $paymentTerm->save(false);

                    $return = ['code' => 200, 'message' => 'Payment Term disabled successfully.'];
            } else {
                $return = ['code' => 421, 'message' => 'Payment Term not enabled.'];
            }
        } else {
            $return = ['code' => 404, 'message' => 'Record does not exist.'];
        }

        Yii::$app->response->format = 'json';
        return $return;
    }

    public function actionEnabled($id) {  

        if (($paymentTerm = PaymentTerm::findOne($id)) !== null) { 

            if ($paymentTerm->status == "DISABLED") {

               
                    $paymentTerm->status = 'ENABLED';
                    $paymentTerm->save(false);

                    $return = ['code' => 200, 'message' => 'Payment Term enabled successfully.'];
            } else {
                $return = ['code' => 421, 'message' => 'Payment Term not disabled.'];
            }
        } else {
            $return = ['code' => 404, 'message' => 'Record does not exist.'];
        }

        Yii::$app->response->format = 'json';
        return $return;
    }
}
