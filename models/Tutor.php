<?php

namespace app\models;

use Yii;
use app\libraries\General;
/**
 * This is the model class for table "{{%tutor}}".
 *
 * @property string $uuid
 * @property string $first_namef
 * @property string $last_name
 * @property string $email
 * @property string $phone
 * @property string $street
 * @property string $city
 * @property string $state
 * @property string $country
 * @property string $postal_code
 * @property string $description
 * @property string $abn
 * @property string $contract_expiry
 * @property string $child_certi_number
 * @property string $child_certi_expiry
 * @property string $status
 * @property string $join_date
 * @property string $update_at
 * @property resource $profile_image
 */
class Tutor extends \yii\db\ActiveRecord {

    public $instrumentList;
    public $state_text;

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return '{{%tutor}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
                //html tag filter
                [['first_name', 'last_name','phone', 'street', 'city','country', 'postal_code', 'description', 'state_text', 'abn','child_certi_number'], 'match', 'pattern' => \app\libraries\General::validTextPattern(), 'message' => 'Invalid charecters used.'], 
            
                [['first_name', 'last_name', 'email', 'phone', 'street', 'city','country', 'postal_code', 'description', 'abn', 'contract_expiry', 'child_certi_number', 'child_certi_expiry'], 'required'],
                [['online_tutorial_package_type_uuid'],'required','message' => 'Please select atleast one tutor type.'],/** Added by pooja : Tutor wise plan rate **/
                [['uuid', 'first_name', 'last_name', 'email', 'street', 'city', 'state', 'country', 'postal_code', 'description', 'child_certi_number', 'status', 'profile_image', 'state_text'], 'string', 'on' => 'create'],
                [['contract_expiry', 'child_certi_expiry', 'join_date', 'update_at','state','state_text','standard_plan_rate_option'], 'safe'],
//                [['phone'], 'number'],
                //[['phone'], 'string', 'min' => 10,],
	        //[['phone'], 'match','pattern'=> '/^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$/','message' => 'Enter a valid Phone.'],
                [['abn'], 'string', 'max' => 14,'min' => 11],
                [['uuid'], 'unique'],
                [['instrumentList'], 'required', 'on' => ['tutor_create', 'tutor_update']],
                [['instrumentList'], 'required'],
                [['state'], 'required', 'when' => function($model) {
                    return ($model->country != "other");
                }, 'whenClient' => "function (attribute, value) {
                    return ( $('#tutor-country').val() !== 'other')?true:false;
                }"],
                [['state_text'], 'required', 'when' => function($model) {
                    return ($model->country == "other");
                },'whenClient' => "function (attribute, value) {
                    return ( $('#tutor-country').val() == 'other')?true:false;
                }"],
//                [['profile_image'], 'file','skipOnEmpty' => false, 'on' => 'profile_image_update'],
                //[['profile_image'], 'image', 'maxWidth' => 550, 'maxHeight' => 550, 'on' => 'profile_image_update'],
                // ['profile_image', 'image', 'maxWidth' => 550, 'maxHeight' => 550],
                ['profile_image', 'image'],
                [['profile_image'], 'file', 'on' => 'profile_image_update'],
                //['image', 'image', 'minWidth' => 250, 'maxWidth' => 300,'minHeight' => 200, 'maxHeight' => 250],
               // ['profile_image', 'image', 'maxWidth' => 550, 'maxHeight' => 550],
                [['contract_expiry'], 'ckeckDateMinCurrentdate','on' => 'tutor_create'],
                [['child_certi_expiry'], 'ckeckDateMinCurrentdate','on' => 'tutor_create'],
                //[['postal_code'], 'number'],
                //[['postal_code'], 'string', 'max' => 4, 'min' => 4],
                    
                [['phone'], 'match',
                    'pattern'=> '/^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$/',
                    'message' => 'Enter a valid Phone Number.',
                    'when' => function($model) {
                    return ($model->country != "other");
                }, 'whenClient' => "function (attribute, value) {
                    return ( $('#tutor-country').val() !== 'other')?true:false;
                }"],
                        
                [['phone'], 'match',
                    'pattern'=> '/^\(?\b([0-9]{4})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{3})\b$/',
                    'message' => 'Enter a 10 digit Phone Number.',
                    'when' => function($model) {
                    return ($model->country == "other");
                }, 'whenClient' => "function (attribute, value) {
                    return ( $('#tutor-country').val() === 'other')?true:false;
                }"],
                        
                [['postal_code'], 'match',
                    'pattern'=> '/^\d{4}$/',
                    'message' => 'Enter a 4 digit postal code.',
                    'when' => function($model) {
                    return ($model->country != "other");
                }, 'whenClient' => "function (attribute, value) {
                    return ( $('#tutor-country').val() !== 'other')?true:false;
                }"],
                        
                [['postal_code'], 'string', 'max' => 10,
                    'when' => function($model) {
                    return ($model->country == "other");
                }, 'whenClient' => "function (attribute, value) {
                    
                    return ( $('#tutor-country').val() === 'other')?true:false;
                }"],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'street' => Yii::t('app', 'Street'),
            'city' => Yii::t('app', 'City'),
            'state' => Yii::t('app', 'State'),
            'profile_image' => Yii::t('app', 'Tutor Image'),
            'state_text' => Yii::t('app', 'State'),
            'country' => Yii::t('app', 'Country'),
            'postal_code' => Yii::t('app', 'Postal Code'),
            'description' => Yii::t('app', 'Description'),
            'abn' => Yii::t('app', 'Australian Business Number'),
            'contract_expiry' => Yii::t('app', 'Contract Expiry'),
            'child_certi_number' => Yii::t('app', 'Child Certification Number'),
            'child_certi_expiry' => Yii::t('app', 'Child Certification Expiry'),
            'status' => Yii::t('app', 'Status'),
            'join_date' => Yii::t('app', 'Join Date'),
            'update_at' => Yii::t('app', 'Update At'),
        ];
    }

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['tutor_notification'] = ['uuid'];
	$scenarios['profile_image_update'] = ['profile_image'];

        return $scenarios;
    }
    
    public function getUser()
    {
        return $this->hasOne(User::className(), ['tutor_uuid' => 'uuid']);
    }
    
    public function getTimezone($tutor_uuid)
    {
        if (($m = User::find()->where(['tutor_uuid' => $tutor_uuid])->one() ) !== null) {
            return $m->timezone;
        }
        return '';
    }
    public function getPlanCategoryByPlanId($uuid)
    {
        if (($m = OnlineTutorialPackageType::find()->where(['uuid' => $uuid])->asArray()->one() ) !== null) {
            return $m;
        }
        return [];
    }
    public function getPlanOption($tutor_uuid, $plan_type_uuid, $instrument_uuid = null)
    {
        $q = OnlineTutorialPackageTutorwise::find()
                ->where(['tutor_uuid' => $tutor_uuid, 'online_tutorial_package_type_uuid' => $plan_type_uuid]);
        if(!empty($instrument_uuid)){
            $q->andWhere(['instrument_uuid' => $instrument_uuid]);
        } else {
            $q->andWhere(['IS','instrument_uuid','NULL']);
        }
        $m = $q->asArray()->one();
        
        if ( $m  !== null) {
            return $m['standard_plan_rate_option'];
        }
        return 0;
    }

    public function ckeckDateMinCurrentdate($attribute){
        $label = $this->getAttributeLabel($attribute);
        $currDate = date('Y-m-d');
        $attr_val = $attribute;
        $att_date = date("Y-m-d", strtotime($this->$attr_val));
        if($att_date < $currDate){
                $this->addError($attribute,$label." can not allow to past date.");
            }
    }

    public function contractExpireMail($array){

        $content['name'] =  $array['first_name'] .' '. $array['last_name'];
	$content['contract_expiry'] = $array['contract_expiry'];

        $owner = General::getOwnersEmails(['OWNER', 'ADMIN', 'SUBADMIN']);

        $mail = Yii::$app->mailer->compose('contract_expire', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                ->setFrom(Yii::$app->params['supportEmail'])
                ->setTo($array['tutor_email'])     //tutor email
                ->setBcc($owner)
                ->setSubject("Contract Expire -" . Yii::$app->name);

        $mail->send();

        return true;
    }

    public function prepareTutorAppoinmentEvents($tutor_uuid = '', $start_date = '', $end_date = '') {

        $entdays = [];
        if (!empty($tutor_uuid) && !empty($start_date) && !empty($end_date)) {
            $where_clause = '(booked_session."tutor_uuid"= \'' . $tutor_uuid . '\' AND (booked_session."status" = \'SCHEDULE\' OR booked_session."status" = \'CANCELLED\') )
                AND ((to_char(start_datetime, \'YYYY-MM-DD\') > \'' . $start_date . '\' AND to_char(start_datetime, \'YYYY-MM-DD\') < \'' . $end_date . '\')
                or (to_char(end_datetime, \'YYYY-MM-DD\') > \'' . $start_date . '\' AND to_char(end_datetime, \'YYYY-MM-DD\') < \'' . $end_date . '\')
                or (to_char(start_datetime, \'YYYY-MM-DD\') <= \'' . $start_date . '\' AND to_char(end_datetime, \'YYYY-MM-DD\') >= \'' . $end_date . '\'))
            ';

            $join = ' LEFT JOIN student on student.student_uuid = booked_session.student_uuid ';
            $join .= ' LEFT JOIN tutor on tutor.uuid = booked_session.tutor_uuid ';

            $query = 'SELECT booked_session.*,CONCAT(student.first_name,\' \',student.last_name) as student_name,CONCAT(tutor.first_name,\' \',tutor.last_name) as tutor_name FROM "booked_session" '.$join.' WHERE ' . $where_clause;
            $command = Yii::$app->db->createCommand($query);
            $model_tution = $command->queryAll();

            //$daysArray = ['sunday' => 0, 'monday' => 1, 'tuesday' => 2, 'wednesday' => 3, 'thursday' => 4, 'friday' => 5, 'saturday' => 6];

            if (!empty($model_tution)) {

                foreach ($model_tution as $key => $value) {

                    $ent['id'] = $value['uuid'];
                    $ent['start'] = General::convertSystemToUserTimezone($value['start_datetime'], 'Y-m-d H:i:s');
                    $ent['end'] = General::convertSystemToUserTimezone($value['end_datetime'], 'Y-m-d H:i:s');
                    $ent['allDay'] = false;
                    $ent['editable'] = false;
                    $ent['type'] = 'schedule_tution';
                    $ent['student_name'] = $value['student_name'];
                    $ent['tutor_name'] = $value['tutor_name'];
                    if($value['status'] == 'CANCELLED'){
                        $ent['title'] = "Cancelled Lesson";
                        $ent['description'] = "Cancelled Lesson";
                        $ent['className'] = 'fc-un-available fa-cancel-schedule-tution';
                    }else{
                        $ent['title'] = "Lesson";
                        $ent['description'] = "Lesson";
                        $ent['className'] = 'fc-un-available fa-schedule-tution';
                    }

                    $entdays[] = $ent;
                }
            }
        }
        return $entdays;
    }

    public function prepareTutorUnavailabilityEvents($tutor_uuid = '', $start_date = '', $end_date = '') {

        $entdays = [];
        if (!empty($tutor_uuid) && !empty($start_date) && !empty($end_date)) {
            $where_clause = '("tutor_uuid"= \'' . $tutor_uuid . '\')
                AND ((to_char(start_datetime, \'YYYY-MM-DD\') > \'' . $start_date . '\' AND to_char(start_datetime, \'YYYY-MM-DD\') < \'' . $end_date . '\')
                or (to_char(end_datetime, \'YYYY-MM-DD\') > \'' . $start_date . '\' AND to_char(end_datetime, \'YYYY-MM-DD\') < \'' . $end_date . '\')
                or (to_char(start_datetime, \'YYYY-MM-DD\') <= \'' . $start_date . '\' AND to_char(end_datetime, \'YYYY-MM-DD\') >= \'' . $end_date . '\'))
            ';

            $query = 'SELECT *, \'Yes\' AS "is_tutor_unavailability" FROM "tutor_unavailability" WHERE ' . $where_clause;
            $command = Yii::$app->db->createCommand($query);
            $model_unavailability = $command->queryAll();

            //$daysArray = ['sunday' => 0, 'monday' => 1, 'tuesday' => 2, 'wednesday' => 3, 'thursday' => 4, 'friday' => 5, 'saturday' => 6];

            if (!empty($model_unavailability)) {

                foreach ($model_unavailability as $key => $value) {

                    $ent['id'] = $value['uuid'];
                    $ent['start'] = General::convertSystemToUserTimezone($value['start_datetime'], 'Y-m-d H:i:s');
                    $ent['end'] = General::convertSystemToUserTimezone($value['end_datetime'], 'Y-m-d H:i:s');
                    $ent['title'] = "Unavailable - ".substr($value['reason'], 0, 50);
                    $ent['description'] = $value['reason'];
                    $ent['className'] = 'fc-un-available fa-unavailability';
                    $ent['allDay'] = false;
                    $ent['editable'] = false;
                    $ent['type'] = 'unavailability';

                    $entdays[] = $ent;
                }
            }
        }
        return $entdays;
    }

    public function prepareTutorWeekDayNoneWorkingEvents($tutor_uuid ) {

        $entdays = [];
        if (($model = Tutor::findOne($tutor_uuid)) !== null) {

            $working_plan = json_decode($model->working_plan,true);
            $week_days_array = ['sunday','monday','tuesday','wednesday','thursday','friday','saturday'];
            $tutor_week_days_array = array_keys($working_plan);

            if(!empty($week_days_array)){
                foreach ($week_days_array as $week_key => $week_day) {

                    if(isset($working_plan[$week_day]) && is_array($working_plan[$week_day])){

                        if(!empty($working_plan[$week_day]['start']) && $working_plan[$week_day]['end']){

                            $split_event = self::split_events($working_plan[$week_day]['start'], $working_plan[$week_day]['end'],$week_key);


                            $break_event = [];
                            if(isset($working_plan[$week_day]['breaks']) && is_array($working_plan[$week_day]['breaks'])){
                                $break_event = self::break_events($working_plan[$week_day]['breaks'], $week_key);
                            }

                            $merge_events = array_merge($break_event,$split_event);

                            $split_event_filter = array_filter($merge_events);
                            if(!empty($split_event_filter)){
                                foreach ($split_event_filter as $filter_array) {
                                    $entdays[] = $filter_array;
                                }
                            }
                        }
                    }else{
                        //Day not available

                        $ent['start'] = '00:00';
                        $ent['end'] = '23:59';
                        $ent['dow'] = [$week_key];
                        $ent['title'] = "Not Working";
                        $ent['description'] = "Not Working";
                        $ent['className'] = 'fc-un-available fa-none-working';
                        $ent['allDay'] = false;
                        $ent['editable'] = false;
                        $ent['type'] = 'none_working';

                        $entdays[] = $ent;
                    }
                }
            }

        }
        return $entdays;
    }

    public function prepareTutorWeekDayNoneWorkingEvents_booking($tutor_uuid,$start_date, $end_date ) {

        $entdays = [];
        if (($model = Tutor::findOne($tutor_uuid)) !== null) {

            $user_timezone = (!Yii::$app->getUser()->isGuest) ? Yii::$app->user->identity->timezone : Yii::$app->params['timezone'];
            $tutor_user_model = User::find()->where(['tutor_uuid' => $model->uuid])->one();
            $tutor_tz = (!empty($tutor_user_model)) ? $tutor_user_model->timezone : Yii::$app->params['timezone'];

            $working_plan = json_decode($model->working_plan,true);
            $week_days_array = ['sunday','monday','tuesday','wednesday','thursday','friday','saturday'];
            $tutor_week_days_array = array_keys($working_plan);

            if(!empty($week_days_array)){
                foreach ($week_days_array as $week_key => $week_day) {

                    if(isset($working_plan[$week_day]) && is_array($working_plan[$week_day])){

                        if(!empty($working_plan[$week_day]['start']) && $working_plan[$week_day]['end']){

                            $split_event = self::split_events($working_plan[$week_day]['start'], $working_plan[$week_day]['end'],$week_key);


                            $break_event = [];
                            if(isset($working_plan[$week_day]['breaks']) && is_array($working_plan[$week_day]['breaks'])){
                                $break_event = self::break_events($working_plan[$week_day]['breaks'], $week_key);
                            }

                            $merge_events = array_merge($break_event,$split_event);

                            $split_event_filter = array_filter($merge_events);
                            if(!empty($split_event_filter)){
                                foreach ($split_event_filter as $filter_array) {
                                    $entdays[] = $filter_array;
                                }
                            }
                        }
                    }else{
                        //Day not available

                        $ent['start'] = '00:00';
                        $ent['end'] = '23:59';
                        $ent['week_day'] = $week_days_array[$week_key];
                        $ent['dow'] = [$week_key];
                        $ent['title'] = "Not Working";
                        $ent['description'] = "Not Working";
                        $ent['className'] = 'fc-un-available fa-none-working';
                        $ent['allDay'] = false;
                        $ent['editable'] = false;
                        $ent['type'] = 'none_working';

                        $entdays[] = $ent;
                    }
                }
            }

        }
        //return $entdays;
        //print_r($entdays);exit;
        //echo $start_date;
        //echo $end_date;
        $start_date = General::convertTimezone($start_date.' 00:00',$user_timezone,$tutor_tz,'Y-m-d H:i');
        $end_date = General::convertTimezone($end_date.' 23:59',$user_timezone,$tutor_tz,'Y-m-d H:i');
        //print_r($start_date.' 00:00');

        $new_event = [];
        foreach ($entdays as $key => $event) {

            $current_date = $start_date;

            while ($end_date > $current_date ) {

                $dy = strtolower(General::displayTimeFormat($current_date,'l'));

                if($dy == $event['week_day']){

                    $st = General::displayTimeFormat($current_date,'Y-m-d').' '.$event['start'];
                    $et = General::displayTimeFormat($current_date,'Y-m-d').' '.$event['end'];
                    $array = $event;
                    $array['start'] = General::convertTimezone($st,$tutor_tz,$user_timezone,'Y-m-d H:i:s');
                    $array['end'] = General::convertTimezone($et,$tutor_tz,$user_timezone,'Y-m-d H:i:s');
                    $new_event[] = $array;
                }

                $current_date = date('Y-m-d H:i', strtotime($current_date. ' +1 day'));

            }
        }

        return $new_event;
    }

    public function weeklyEventsConvertFromToTimezone($events = [], $from, $to){
        $return = [];

        if(!empty($events)){
            foreach ($events as $key => $value) {
                $e = $value;
                $e['start'] = General::convertTimezone($value['start'],$from,$to,'H:i');
                $e['end'] = General::convertTimezone($value['end'],$from,$to,'H:i');
                $e['start_old'] = $value['start'];
                $e['end_old'] = $value['end'];
                $e['from'] = $from;
                $e['to'] = $to;
                $return[] = $e;
            }
        }
        //echo "<pre>";print_r($return);exit;
        return $return;
    }

    public function split_events($start,$end,$week_day_number) {
        $week_days_array = ['sunday','monday','tuesday','wednesday','thursday','friday','saturday'];
        $before_event = [];
        $after_event = [];
        if('00:00' < $start){
            $before_event['start'] = '00:00';
            $before_event['end'] = $start;
            $before_event['week_day'] = $week_days_array[$week_day_number];
            $before_event['dow'] = [$week_day_number];
            $before_event['title'] = "Not Working";
            $before_event['description'] = "Not Working";
            $before_event['className'] = 'fc-un-available fa-none-working';
            $before_event['allDay'] = false;
            $before_event['editable'] = false;
            $before_event['type'] = 'none_working';
        }

        if('23:59' > $end){
            $after_event['start'] = $end;
            $after_event['end'] = '23:59';
            $after_event['week_day'] = $week_days_array[$week_day_number];
            $after_event['dow'] = [$week_day_number];
            $after_event['title'] = "Not Working";
            $after_event['description'] = "Not Working";
            $after_event['className'] = 'fc-un-available fa-none-working';
            $after_event['allDay'] = false;
            $after_event['editable'] = false;
            $after_event['type'] = 'none_working';
        }
        return array($after_event, $before_event);
    }


    public function break_events($breaks, $week_day_number) {
        $event_array = [];
        $week_days_array = ['sunday','monday','tuesday','wednesday','thursday','friday','saturday'];

        if(!empty($breaks)){
            foreach ($breaks as $key => $value) {
                $event['start'] = $value['start'];
                $event['end'] = $value['end'];
                $event['week_day'] = $week_days_array[$week_day_number];
                $event['dow'] = [$week_day_number];
                $event['title'] = "Break";
                $event['description'] = "Break";
                $event['className'] = 'fc-un-available fa-break';
                $event['allDay'] = false;
                $event['editable'] = false;
                $event['type'] = 'break';

                $event_array[] = $event;
            }
        }


        return $event_array;
    }

    public function childCertificateExpireMail($array){

        $content['name'] = $array['first_name'] .' '. $array['last_name'];
        $content['child_certificate_expiry'] = $array['child_certificate_expiry'];

        $owner = General::getOwnersEmails(['OWNER', 'ADMIN', 'SUBADMIN']);

        $mail = Yii::$app->mailer->compose('child_certificate_expire', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                ->setFrom(Yii::$app->params['supportEmail'])
                ->setTo($array['tutor_email'])     //tutor email
                ->setBcc($owner)
                ->setSubject("Child Certificate Expire -" . Yii::$app->name);

        $mail->send();

        return true;
    }

    public function prepareTutorScheduleAppoinmentEvents($tutor_uuid = '', $start_date = '', $end_date = '') {

        $entdays = [];
        if (!empty($tutor_uuid) && !empty($start_date) && !empty($end_date)) {
            $where_clause = '(booked_session."tutor_uuid"= \'' . $tutor_uuid . '\' AND (booked_session."status" = \'SCHEDULE\') )
                AND ((to_char(start_datetime, \'YYYY-MM-DD\') > \'' . $start_date . '\' AND to_char(start_datetime, \'YYYY-MM-DD\') < \'' . $end_date . '\')
                or (to_char(end_datetime, \'YYYY-MM-DD\') > \'' . $start_date . '\' AND to_char(end_datetime, \'YYYY-MM-DD\') < \'' . $end_date . '\')
                or (to_char(start_datetime, \'YYYY-MM-DD\') <= \'' . $start_date . '\' AND to_char(end_datetime, \'YYYY-MM-DD\') >= \'' . $end_date . '\'))
            ';

            $join = ' LEFT JOIN student on student.student_uuid = booked_session.student_uuid ';
            $join .= ' LEFT JOIN tutor on tutor.uuid = booked_session.tutor_uuid ';

            $query = 'SELECT booked_session.*,CONCAT(student.first_name,\' \',student.last_name) as student_name,CONCAT(tutor.first_name,\' \',tutor.last_name) as tutor_name FROM "booked_session" '.$join.' WHERE ' . $where_clause;
            $command = Yii::$app->db->createCommand($query);
            $model_tution = $command->queryAll();

            if (!empty($model_tution)) {

                foreach ($model_tution as $key => $value) {

                    $ent['id'] = $value['uuid'];
                    $ent['start'] = General::convertSystemToUserTimezone($value['start_datetime'], 'Y-m-d H:i:s');
                    $ent['end'] = General::convertSystemToUserTimezone($value['end_datetime'], 'Y-m-d H:i:s');
                    $ent['allDay'] = false;
                    $ent['editable'] = false;
                    $ent['type'] = 'schedule_tution';
                    $ent['student_name'] = $value['student_name'];
                    $ent['tutor_name'] = $value['tutor_name'];
                    $ent['title'] = "Schedule Lesson";
                    $ent['description'] = "Schedule Lesson";
                    $ent['className'] = 'fc-un-available fa-schedule-tution';

                    $entdays[] = $ent;
                }
            }
        }
        return $entdays;
    }

    public function prepareTutorRescheduleAppoinmentEvents($tutor_uuid = '', $start_date = '', $end_date = '') {

        $entdays = [];
        if (!empty($tutor_uuid) && !empty($start_date) && !empty($end_date)) {
            $where_clause = '(reschedule_session."tutor_uuid"= \'' . $tutor_uuid . '\' AND (reschedule_session."status" = \'AWAITING\') )
                AND ((to_char(to_start_datetime, \'YYYY-MM-DD\') > \'' . $start_date . '\' AND to_char(to_start_datetime, \'YYYY-MM-DD\') < \'' . $end_date . '\')
                or (to_char(to_end_datetime, \'YYYY-MM-DD\') > \'' . $start_date . '\' AND to_char(to_end_datetime, \'YYYY-MM-DD\') < \'' . $end_date . '\')
                or (to_char(to_start_datetime, \'YYYY-MM-DD\') <= \'' . $start_date . '\' AND to_char(to_end_datetime, \'YYYY-MM-DD\') >= \'' . $end_date . '\'))
            ';

            $join = ' LEFT JOIN student on student.student_uuid = reschedule_session.student_uuid ';
            $join .= ' LEFT JOIN tutor on tutor.uuid = reschedule_session.tutor_uuid ';

            $query = 'SELECT reschedule_session.*,CONCAT(student.first_name,\' \',student.last_name) as student_name,CONCAT(tutor.first_name,\' \',tutor.last_name) as tutor_name FROM "reschedule_session" '.$join.' WHERE ' . $where_clause;
            $command = Yii::$app->db->createCommand($query);
            $model_tution = $command->queryAll();

            if (!empty($model_tution)) {

                foreach ($model_tution as $key => $value) {

                    $ent['id'] = $value['uuid'];
                    $ent['start'] = General::convertSystemToUserTimezone($value['to_start_datetime'], 'Y-m-d H:i:s');
                    $ent['end'] = General::convertSystemToUserTimezone($value['to_end_datetime'], 'Y-m-d H:i:s');
                    $ent['allDay'] = false;
                    $ent['editable'] = false;
                    $ent['type'] = 're_schedule_lesson';
                    $ent['student_name'] = $value['student_name'];
                    $ent['tutor_name'] = $value['tutor_name'];
                    $ent['title'] = "Tutor Reschedule Lesson";
                    $ent['description'] = "Pending Tutor Reschedule Lesson Request Approval";
                    $ent['className'] = 'fc-un-available fa-tutor-reschedule-lesson';

                    $entdays[] = $ent;
                }
            }
        }
        return $entdays;
    }

    public function prepareStudentScheduleAppoinmentEvents($tutor_uuid = '', $start_date = '', $end_date = '') {

        $entdays = [];
        if (!empty($tutor_uuid) && !empty($start_date) && !empty($end_date)) {
            $where_clause = '(booked_session."tutor_uuid"= \'' . $tutor_uuid . '\' AND (booked_session."status" = \'SCHEDULE\') )
                AND ((to_char(start_datetime, \'YYYY-MM-DD\') > \'' . $start_date . '\' AND to_char(start_datetime, \'YYYY-MM-DD\') < \'' . $end_date . '\')
                or (to_char(end_datetime, \'YYYY-MM-DD\') > \'' . $start_date . '\' AND to_char(end_datetime, \'YYYY-MM-DD\') < \'' . $end_date . '\')
                or (to_char(start_datetime, \'YYYY-MM-DD\') <= \'' . $start_date . '\' AND to_char(end_datetime, \'YYYY-MM-DD\') >= \'' . $end_date . '\'))
            ';

            $join = ' LEFT JOIN student on student.student_uuid = booked_session.student_uuid ';
            $join .= ' LEFT JOIN tutor on tutor.uuid = booked_session.tutor_uuid ';

            $query = 'SELECT booked_session.*,CONCAT(student.first_name,\' \',student.last_name) as student_name,CONCAT(tutor.first_name,\' \',tutor.last_name) as tutor_name FROM "booked_session" '.$join.' WHERE ' . $where_clause;
            $command = Yii::$app->db->createCommand($query);
            $model_tution = $command->queryAll();

            if (!empty($model_tution)) {

                foreach ($model_tution as $key => $value) {

                    $ent['id'] = $value['uuid'];
                    $ent['start'] = General::convertSystemToUserTimezone($value['start_datetime'], 'Y-m-d H:i:s');
                    $ent['end'] = General::convertSystemToUserTimezone($value['end_datetime'], 'Y-m-d H:i:s');
                    $ent['allDay'] = false;
                    $ent['editable'] = false;
                    $ent['type'] = 'student_schedule_tution';
                    $ent['student_name'] = $value['student_name'];
                    $ent['tutor_name'] = $value['tutor_name'];
                    $ent['title'] = "Student Scheduled Lesson";
                    $ent['description'] = "Student Scheduled Lesson";
                    $ent['className'] = 'fc-un-available fa-student-schedule-tution';

                    $entdays[] = $ent;
                }
            }
        }
        return $entdays;
    }

}
