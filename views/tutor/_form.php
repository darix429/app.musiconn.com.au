<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use app\libraries\General;

/* @var $this yii\web\View */
/* @var $model app\models\Admin */
/* @var $form yii\widgets\ActiveForm */

$timezone = General::getAllTimeZones();
if($userModel->timezone == '') {
 $userModel->timezone = Yii::$app->params['timezone'];
}

$this->registerJs('
    $(document).on("click","#getpassword", function(){
        $.ajax({
            type: "GET",
            url: "' . Url::to(['/tutor/generate-password/']) . '",
            success: function (result) {
                    $("#user-password_hash").val(result);
            },
        });
    });
$("#tutor-instrumentlist").multiSelect({
                selectableHeader: "<input type=\'text\' class=\'form-control search-input\' autocomplete=\'off\' placeholder=\'search...\'>",
                selectionHeader: "<input type=\'text\' class=\'form-control search-input\' autocomplete=\'off\' placeholder=\'search...\'>",
                afterInit: function(ms) {
                    var that = this,
                        $selectableSearch = that.$selectableUl.prev(),
                        $selectionSearch = that.$selectionUl.prev(),
                        selectableSearchString = "#" + that.$container.attr("id") + " .ms-elem-selectable:not(.ms-selected)",
                        selectionSearchString = "#" + that.$container.attr("id") + " .ms-elem-selection.ms-selected";

                    that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                        .on("keydown", function(e) {
                            if (e.which === 40) {
                                that.$selectableUl.focus();
                                return false;
                            }
                        });

                    that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                        .on("keydown", function(e) {
                            if (e.which == 40) {
                                that.$selectionUl.focus();
                                return false;
                            }
                        });
                },
                afterSelect: function() {
                    this.qs1.cache();
                    this.qs2.cache();
                },
                afterDeselect: function() {
                    this.qs1.cache();
                    this.qs2.cache();
                }
            });

            $("#tutor-country").change(function(){
                if($(this).val() == "other"){
                    $(".state_dd").hide();
                    $(".state_text").show();
                }else{
                    $(".state_text").hide();
                    $(".state_dd").show();
                }
            })

//            $("#tutor_form").submit(function (event) {
//                    //submit from callback
//                    blockBody();
//            });

'
);
?>
<style>
    .has-error .ms-selection .ms-list{border:1px #f05050 solid;}
    .has-success .ms-selection .ms-list{border:1px #66be78 solid;}
    .ms-selection .ms-list, .ms-container .ms-list{ height: 110px;}
    .ms-container {margin: 0;}
</style>
<div class="col-xl-12 col-lg-12 col-12 col-md-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Please Fill Information</h2>
            <div class="actions panel_actions float-right">
                <!--<i class="box_toggle fa fa-chevron-down"></i>-->
                <a href="<?= Yii::$app->getUrlManager()->createUrl(['/tutor/index/']) ?>" class="btn btn-info btn-icon "><i class="fa fa-arrow-left text-white"></i> <span>Back</span></a>
                <?php  if(!$new): ?>
                	<a href="<?= Yii::$app->getUrlManager()->createUrl(['/tutor/working-plan/','id'=>$model->uuid]) ?>" title="Wokring Plan" class="btn btn-orange btn-icon "><i class="fa fa-calendar text-white"></i></a>
		<?php endif; ?>
            </div>
        </header>
        <div class="content-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12">
                    <?php $form = ActiveForm::begin([
                        'options' => ['enctype' => 'multipart/form-data'],
                        'action' => (!$new) ? Yii::$app->getUrlManager()->createUrl(['/tutor/update/','id'=>$model->uuid]) : ['create'],
                        'id' => 'tutor_form',
                'enableClientValidation' => true,
                // 'enableAjaxValidation' => true,
                'validateOnChange' => true,
                    ]); ?>
                    <div class="form-row">
                        <div class="col-md-3 mb-0">
                            <?= $form->field($model, 'first_name')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('first_name')]) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($model, 'last_name')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('last_name')]) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($userModel, 'timezone')->dropDownList($timezone, ['class' => 'form-control myselect2', 'prompt' => 'Select Timezone', 'maxlength' => true]) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($model, 'abn')->widget(\yii\widgets\MaskedInput::className(), ['mask' => '99 999 999 999', 'clientOptions' => ['removeMaskOnSubmit' => true],'options' => ['placeholder' => 'xx xxx xxx xxx', 'class' => 'form-control']]) ?>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class=" <?= $new ? 'col-md-4' : 'col-md-6'; ?> mb-0">
                            <?= $form->field($userModel, 'username')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $userModel->getAttributeLabel('email')]) ?>
                        </div>
                        <?php if ($new) { ?>
                            <div class="col-md-4 mb-0">
                                <?=
                                $form->field($userModel, 'password_hash', ['template' => "{label}\n<div class='input-group'>"
                                    . "{input}<div class='input-group-append'>"
                                    . "<a class='btn btn-warning' id='getpassword' title='Generate Password'>"
                                    . "<i class='fa fa-refresh text-white'></i>&nbsp;"
                                    . "</a>"
                                    . "</div>"
                                    . "</div>\n{hint}\n{error}",
                                    'labelOptions' => ['class' => 'control-label']]
                                )->textInput(['class' => 'form-control', 'readonly' => false, 'maxlength' => true, 'placeholder' => $userModel->getAttributeLabel('password_hash')])
                                ;
                                ?>
                            </div>
                        <?php } ?>
                        <div class=" <?= $new ? 'col-md-4' : 'col-md-6'; ?> mb-0">
                            <?= $form->field($model, 'profile_image')->fileInput(['class' => 'form-control']) ?>
                        </div>
                    </div>
                    <div class="form-row inputPosition">
                        <?php if($model->country == 'other'){
                                $display_state_dd= 'none';
                                $display_state_text= 'block';
                            }else{
                                $display_state_dd= 'block';
                                $display_state_text= 'none';

                            } ?>
                        <div class="col-md-4">

                            <?= $form->field($model, 'country')->dropDownList(['AUS' => 'Australia','other' => 'Other'], ['class' => 'form-control', 'maxlength' => true]) ?>
                        </div>
                        <div class="col-md-4 state_dd" style="display:<?= $display_state_dd;?>">
                            <?= $form->field($model, 'state')->dropDownList(['New South Wales' => 'New South Wales', 'Queensland' => 'Queensland', 'South Australia' => 'South Australia', 'Tasmania' => 'Tasmania', 'Victoria' => 'Victoria', 'Western Australia' => 'Western Australia', 'Australian Capital Territory' => 'Australian Capital Territory', 'Northern Territory' => 'Northern Territory'], ['class' => 'form-control', 'maxlength' => true, 'prompt' => "Select " . $model->getAttributeLabel('state')]) ?>
                        </div>
                        <div class="col-md-4 state_text" style="display:<?= $display_state_text;?>">
                            <?= $form->field($model, 'state_text')->textInput(['autofocus' => false, 'class' => 'input form-control state_text', 'placeholder' => "State"]); ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'city')->textInput(['autofocus' => false, 'class' => 'input form-control', 'placeholder' => "City"]); ?>
                        </div>

                    </div>
                    <div class="form-row inputPosition">
                        <div class="col-md-4">
                            <?= $form->field($model, 'street')->textInput(['autofocus' => false, 'class' => 'input form-control', 'placeholder' => "Street"]); ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'postal_code')->textInput(['autofocus' => false, 'maxlength' => true, 'class' => 'input form-control', 'placeholder' => "Postal Code"]); ?>
                            <?php //echo $form->field($model, 'postal_code')->widget(\yii\widgets\MaskedInput::className(['plceholder'=>'']), ['mask' => '9999', 'clientOptions' => ['removeMaskOnSubmit' => true],'options' => ['placeholder' => 'xxxx', 'class' => 'form-control']]) ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), ['mask' => '9999 999 999', 'clientOptions' => ['removeMaskOnSubmit' => true],'options' => ['placeholder' => 'XXXX XXX XXX', 'class' => 'form-control']]) ?>
                            
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-12 mb-0">
                            <?php
                            $items = array();
                            foreach ($instrumentModel as $key => $value) {
                                $items[$value->uuid] = $value->name;
                            }
                            ?>
                            <?= $form->field($model, 'instrumentList')->dropDownList($items, ['multiple' => true])->label('Instruments'); ?>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6 mb-0">
                            <?= $form->field($model, 'description')->textarea(['rows' => 2]) ?>
                        </div>
                        <!--
                         /**
                             * start
                             * Devloped By : Pooja Beladiya
                             * Task : Tutor wise plan rate
                             */-->
                         <div class="col-md-6 mb-0">
                         <?php
                            $items = array();
                            foreach ($packegetypeModel as $key => $value) {
                                $items[$value->uuid] = strtoupper($value->display_name);
                            }
                            if($new) {
                            ?>
                            <?= $form->field($model, 'online_tutorial_package_type_uuid')->dropDownList($items, ['prompt' => 'Select Type'])->label('Tutor Category'); ?>
                            <?php } else { 
                             
                             if (Yii::$app->user->identity->role != 'TUTOR') {?>
                            <?= $form->field($model, 'online_tutorial_package_type_uuid')->dropDownList($items, ['prompt' => 'Select Type'])->label('Tutor Category'); ?>
                             <?php } else { ?>
                                                             <?= $form->field($model, 'online_tutorial_package_type_uuid')->dropDownList($items, ['prompt' => 'Select Type',"disabled"=>"disabled"])->label('Tutor Category'); ?>

                             <?php }
                                
                             }?>
                        </div>
                        <!--/**
                             * End
                             * Task : Tutor wise plan rate
                             */-->
                    </div>
                    <div class="form-row">
                        <div class="col-md-4 mb-0">
                            <?php $model->contract_expiry = \app\libraries\General::displayDate($model->contract_expiry);
                            echo $form->field($model, 'contract_expiry')->textInput(['readonly' => true, 'class' => 'form-control custom_date_picker', 'maxlength' => true, 'placeholder' => 'DD-MM-YYYY']) ?>
                        </div>
                        <div class="col-md-4 mb-0">
                            <?= $form->field($model, 'child_certi_number')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('child_certi_number')]) ?>
                        </div>
                        <div class="col-md-4 mb-0">
                            <?php $model->child_certi_expiry = \app\libraries\General::displayDate($model->child_certi_expiry);
                            echo $form->field($model, 'child_certi_expiry')->textInput(['readonly' => true, 'class' => 'form-control custom_date_picker', 'maxlength' => true, 'placeholder' => 'DD-MM-YYYY']) ?>
                        </div>
                    </div>

                    <?= Html::submitButton(Yii::t('app', $new ? 'Create' : 'Update'), ['class' => 'btn btn-primary', 'style' => 'float: right;']) ?>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>

        </div>
    </section>
</div>
