<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\TutorialType;
use app\models\PaymentTerm;
use app\models\OnlineTutorialPackageType;
?>

<div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">

    <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Instrument</th>
                <th>Prev. Category</th>
                <th>Prev. Option</th>
                <th>New Category</th>
                <th>New Option</th>
                <th>Date of Change</th>
                
            </tr>
        </thead>

        <tbody>
            <?php
            if (!empty($historyList)) {
                foreach ($historyList as $key => $value) { 
                    
                    $old_cate = '';
                    if(!empty($value['old_online_tutorial_package_type_uuid'])){
                        $old_cat_model = OnlineTutorialPackageType::find()->where(['uuid' => $value['old_online_tutorial_package_type_uuid']])->one();
                        $old_cate = (!empty($old_cat_model['display_name'])) ? $old_cat_model['display_name'] : '';
                        if(!empty($old_cat_model['display_name'])){
                            $old_cat_cls = ($old_cat_model['name'] == 'STANDARD') ? 'badge-warning' : 'badge-success';
                            $old_cate = '<span class="badge badge-pill '.$old_cat_cls.' " >'.$old_cat_model['display_name'].'</span>';
                        }
                    }
                    
                    $new_cat = '';
                    if(!empty($value['onlineTutorialPackageTypeUu']['display_name'])){
                        $new_cat_cls = ($value['onlineTutorialPackageTypeUu']['name'] == 'STANDARD') ? 'badge-warning' : 'badge-success';
                        $new_cat = '<span class="badge badge-pill '.$new_cat_cls.' " >'.$value['onlineTutorialPackageTypeUu']['display_name'].'</span>';
                    }
                    ?>
                    <tr>
                        <td><?= $key + 1; ?></td>
                        <td><?= $value['tutorUu']['first_name']; ?></td>
                        <td><?= $value['tutorUu']['last_name']; ?></td>
                        <td><?= $value['instrumentUu']['name']; ?></td>
                        <td><?= $old_cate; ?></td>
                        <td><?= $value['old_option']; ?></td>
                        <td><?= $new_cat; ?></td>
                        <td><?= $value['option']; ?></td>
                        <td><?= \app\libraries\General::displayDate($value['datetime']); ?></td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>

</div>

