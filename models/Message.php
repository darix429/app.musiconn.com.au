<?php

namespace app\models;
use Yii;
use yii\base\Model;

class Message extends Model {

    public $role;
    public $type;
    public $from;
    public string $to;
    public $content;
    public $attach_url;
    public $status;
    public $timestamp;
    public $attachment;

    public function rules() {
        return [
            [["attachment"], "file"]
        ];
    }
    public function attributeLabels() {
        return [];
    }

    public function uploadFile() {
        $this->attach_url = time() . "." . $this->attachment->extension;
        $this->attachment->saveAs(Yii::$app->params["messages"]["attachment_upload_path"]  . $this->attach_url);
        return $this->attach_url;
    }
}