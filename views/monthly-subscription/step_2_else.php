<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Url;
//rupal
use app\models\MonthlySubscriptionFee;
use app\libraries\General;

$this->title = Yii::t('app', 'MONTHLY SUBSCRIPTION');
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_icon'] = 'my-lesson-tag.png';

//echo "<pre>";
//print_r($step_data);
//print_r($model->getErrors());
//echo "</pre>";

if (!empty($step_data['step_1_else']['studentmonthlysub_uuid'])) {
    $monthlySubscriptionDetail = \app\models\StudentMonthlySubscription::find()->where(['uuid' => $step_data['step_1_else']['studentmonthlysub_uuid']])->one();
} elseif (!empty($step_data['step_1']['monthlysubscriptionfee_uuid'])) {
    $monthlySubscriptionDetail = MonthlySubscriptionFee::find()->where(['uuid' => $step_data['step_1']['monthlysubscriptionfee_uuid']])->one();
} else {
    return \yii\web\Controller::redirect(['index']);
}

$this->registerJsFile('https://js.stripe.com/v2/');
$this->registerJs(
        '
            //delete card 
            $(document).on("click",".delete_credit_card", function(){ 
                if(confirm("Are you sure, you want to delete this card ?")){
                var element = $(this); 
                    $.ajax({
                        type: "POST",
                        url: "' . Url::to(['/monthly-subscription/delete/']) . '",
                        data: { "id": $(this).data("uuid")},
                        beforeSend: function() { blockBody(); },
                        success:function(result){ 
                            if(result.code == 200){

                                $(element).closest("div.card").remove();
                                $("#gostep-remove-card").submit();
                                
                            }else{
                                showErrorMessage(result.message);
                            }
                            unblockBody();
                        },
                        error:function(e){ 
                            unblockBody();
                            showErrorMessage(e.responseText);
                        }
                    });
                }
            }); 
            
            //card cvv validation
            $(document).on("input","#studentcreditcard-cvv", function(){
                    match = (/(\d{0,4})[^]*((?:\.\d{0,0})?)/g).exec(this.value.replace(/[^\d.]/g, ""));
                    this.value = match[1] + match[2];
            });
            
            /*********************************/
            
            //set your publishable key
            Stripe.setPublishableKey("' . Yii::$app->params['stripe']['publishable_key'] . '");

            //callback to handle the response from stripe
                function stripeResponseHandler(status, response) {
                    if (response.error) {

                        $("#payBtn").removeAttr("disabled");
                        unblockBody();
                        $(".payment-errors").html(response.error.message);
                        showSuccess(response.error.message);
                    } else {
                        var form$ = $("#frm_step_2_else");

                        var token = response["id"];
                        
                        form$.append("<input type=\"hidden\" name=\"stripeToken\" value=\""+token+"\" />");

                        form$.get(0).submit();
                    }
                }

             function submitform()
                {
                    $("#payBtn").attr("disabled", "disabled");
                    blockBody();
                    var dateText = $("#studentcreditcard-expire_date").val(); 
                    var pieces = dateText.split("-");
                    var year = (pieces[0] != undefined && pieces[0] > 0) ? pieces[0] : "1970"; 
                    var month = (pieces[1] != undefined && pieces[1] > 0) ? pieces[1] : "00";
                    
                    var token = Stripe.createToken({
                        number: $("#studentcreditcard-number").val(),
                        cvc: $("#studentcreditcard-cvv").val(),
                        exp_month: month,
                        exp_year: year,
                    }, stripeResponseHandler);
                }
                $("#frm_step_2_else").submit(function (event) {
                    //submit from callback
                    return false;
                });
                
                $("#gostep1_else").submit(function (event) {
                    blockBody();
                });
                
                $(document).bind("cut copy paste","#studentcreditcard-number",function(e){
                    e.preventDefault();
                });
            
', View::POS_END
);
?>
<section class="content">
    <div class="row mar-bottom-10">
        <div class="col-xs-12">
            <div class="box">
                <div class="col-md-12 box-panel-header ">
                    <h4 class="col-md-6">Select Monthly Subscription &nbsp;<i class="fa fa-check"></i></h4>
                    <div class="col-md-6 ">
                        <?php echo Html::beginForm(['index'], 'post', ['id' => 'gostep1_else', 'class' => 'form-inline d-block']) ?>

                        <input type="hidden" name="step" value="1">
                        <input type="hidden" name="prev" value="1">
                        <a href="javascript:$('#gostep1_else').submit();" class="box-panel-header-right-btn pull-right">
                            Change
                        </a>
                        <?php echo Html::endForm() ?>
                        <p class="box-panel-header-right-p text-white pull-right"> <?= $monthlySubscriptionDetail->name; ?>&nbsp;</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row mar-bottom-10">
        <div class="col-xs-12">
            <div class="box">
                <div class="col-md-12 box-panel-header border-bottom-radious-none">
                    <h4 class="col-md-6">Payment</h4>
                    <div class="col-md-6 ">

                        <a  class="box-panel-header-right-btn pull-right delete_credit_card" data-uuid="<?= $creditCard['uuid']; ?>">Remove Card</a>
                    </div>
                </div>
                <div class="box-body box-panel-body">
                    <div class="row vertical-center-box vertical-center-box-tablet" >
                        <?php
                        $form = ActiveForm::begin([
                                    'action' => ['index'],
                                    'id' => 'frm_step_2_else',
                                    'enableClientValidation' => true,
//                              'enableAjaxValidation' => true,
                                    'validateOnChange' => true,
                                    'options' => ['class' => 'form-horizontal']]);
                        ?>
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <div class="row" style="margin-top:10px;">
                                    <input type="hidden" name="step_else" value="2">
                                    <input type="hidden" name="studentmonthlysub_uuid" value="<?= $monthlySubscriptionDetail->uuid; ?>">
                                    <div class="payment-errors text-danger"></div>
                                    <br>
                                   
                                </div>
                                <div class="form-group">
                                    <span><label class="control-label" for="studentcreditcard-name">Card Holder Name*</label></span>
                                    <span type="text" class="form-control" disabled="disabled"><?= $creditCard['name']; ?></span>
                                </div>
                                <div class="form-group">
                                    <span><label class="control-label" for="studentcreditcard-number">Card Number*</label></span>
                                    <span type="text" class="form-control bg-gray-field" disabled="disabled">
                                        <?php
                                        $number = General::decrypt_str($creditCard['number']);
                                        echo General::ccMasking($number);
                                        ?>
                                    </span>
                                </div>
                                <div class="form-group">
                                <div class=" col-md-6">
                                    <?=
                                            $form->field($model, 'cvv', ['template' => '<span>{label}</span>{input}{error}', 'inputOptions' => ['autocomplete' => 'new-password']])
                                            ->passwordInput(['class' => 'form-control', 'maxlength' => true,])
                                            ->label($model->getAttributeLabel('cvv') . '*')
                                    ?>
                                </div>
                                <div class=" col-md-6 ">
                                    <span><label class="control-label" for="studentcreditcard-cc_month_year">Expiry*</label></span>
                                    <span type="text" class="form-control" disabled="disabled"><?= date("Y-m", strtotime($creditCard['expire_date'])); ?></span>
                                </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="checkbox icheck">
                                            <?php
                                            echo $form->field($model, 'term_condition_checkbox', ['template' => ' <label>{input} I agree to Musiconns <a data-toggle="modal" data-target="#terms_condition_model"><b>Terms and Conditions</b></a></label>{error} '])->checkbox([
                                                'class' => 'skin-square-red float-left '], false);
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-md-4 pull-right">
                                        <?= Html::submitButton(Yii::t('app', 'Proceed Securely'), ['class' => 'btn btn-primary payment-btn', 'id' => 'payBtn', 'onclick' => 'submitform()', 'style' => '']) ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                            
                        <div class="hidden_inputs">
                            <?= $form->field($model, 'uuid')->hiddenInput(['value' => $creditCard['uuid']])->label(false); ?>
                            <?= $form->field($model, 'name')->hiddenInput(['value' => $creditCard['name']])->label(false); ?>
                            <?= $form->field($model, 'number')->hiddenInput(['value' => General::decrypt_str($creditCard['number'])])->label(false); ?>
                            <?php
                            $exp_date = date('Y-m', strtotime($creditCard['expire_date']));
                            echo $form->field($model, 'expire_date')->hiddenInput(['value' => $exp_date])->label(false);
                            ?>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php echo Html::beginForm(['index'], 'post', ['id' => 'gostep-remove-card', 'class' => 'form-inline d-block']) ?>
        <input type="hidden" name="step" value="2">
        <input type="hidden" name="prev" value="2">
        <input type="hidden" name="remove_card" value="yes">
        <input type="hidden" name="monthlysubscriptionfee_uuid" value="<?= $monthlySubscriptionDetail->uuid; ?>">
        <?php echo Html::endForm() ?>
    </div>
    <a class="btn btn-primary btn-sm black_button_in" href="<?= Yii::$app->getUrlManager()->createUrl('/student/profile'); ?>">RETURN TO PROFILE</a>
</section>

<!-- modal start -->
<div class="modal fade" id="terms_condition_model" tabindex="-1" role="dialog" aria-labelledby="ultraModal-Label" aria-hidden="true">
    <div class="modal-dialog animated zoomIn">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Terms and Conditions</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <?= \app\models\Settings::getSettingsByName('payment_terms_and_conditions'); ?>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-primary" type="button">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- modal end -->

