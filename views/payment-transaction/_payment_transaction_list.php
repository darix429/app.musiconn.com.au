<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">

    <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Transaction Number</th>
                <th>Order ID</th>
                <th>Booking ID</th>
                <th>Subscription</th>
                <th>Musicoin Plan</th>
                <th>Student</th>
                <th>Amount</th>
<!--                <th>Payment Method</th>
                <th>Reference Number</th>-->
                <th>Payment Date</th>
                <th>Status</th>
                
            </tr>
        </thead>

        <tbody>
            <?php
            if (!empty($paymentTransactionList)) {
                foreach ($paymentTransactionList as $key => $value) { //echo "<pre>"; print_r($value); exit;
                $student_name = $value['studentUu']['first_name']. ' ' . $value['studentUu']['last_name'];   
                    ?>
                    <tr>
                        <td><?= $key + 1; ?></td>
                        <td><?= (!empty($value['transaction_number'])) ? "#".$value['transaction_number'] : ''; ?></td>
                        <td><?= (!empty($value['orderUu']['order_id'])) ? '<a target="_blank" href="'.Yii::$app->params['media']['invoice']['url'] . $value['orderUu']['invoice_file'].'">#'.$value['orderUu']['order_id'].'</a>' : ''; ?></td>
                        <td><?= (!empty($value['studentTuitionBookingUu']['booking_id'])) ?  '<a href="'.Url::to(['/book-music-tution/view/', 'id' => $value['studentTuitionBookingUu']['uuid']]).'">#'.$value['studentTuitionBookingUu']['booking_id'].'</a>'  : ''; ?></td>
			<td><?= (!empty($value['studentMonthlySubscriptionUu']['name'])) ? $value['studentMonthlySubscriptionUu']['name'] : ''; ?></td>
			<td><?= (!empty($value['studentMusicoinPackageUu']['name'])) ? $value['studentMusicoinPackageUu']['name'] : ''; ?></td>
                        <td><?= (!empty($student_name)) ? $student_name : ''; ?></td>
                        <td><?= $value['amount']; ?></td>
<!--                        <td><?= $value['payment_method']; ?></td>
                        <td><?= $value['reference_number']; ?></td>-->
                        <td><?= \app\libraries\General::displayDateTime($value['payment_datetime']); ?></td>
                        <td>
                            <?php
                         
                            if ($value['status'] == 'succeeded') {
                                $e_class = "badge-primary";
				//$pointer = 'style="cursor:pointer;color:#fff"';
                                $e_link_class = "coupon_disabled_link";
                            } elseif ($value['status'] == 'canceled') {
                                $e_class = "badge-secondary";
				//$pointer = 'style="cursor:pointer;color:#fff"';
                                $e_link_class = "coupon_enabled_link";
                            } else {
                                $e_class = "badge-danger";
				//$pointer = '';                                
                                $e_link_class = "";
                            }
                            ?>
                            <span class="badge badge-pill <?= $e_class ?> <?= $e_link_class ?>"  ><?= strtoupper($value['status']); ?></span>
                        </td>
                        
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>

</div>
