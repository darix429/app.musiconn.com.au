<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use app\models\User;

$this->title = 'Dashboard';

$User = new User();
$enabledTutorCount = $User->enabledTutorCount();
$disabledTutorCount = $User->disabledTutorCount();
$enabledStudentCount = $User->enabledStudentCount();
$disabledStudentCount = $User->disabledStudentCount();
$scheduleTutionCount = $User->scheduleTutionCount();
$completedTutionCount = $User->completedTutionCount();
$publishedTutionCount = $User->publishedTutionCount();
$ongoingTutionCount = $User->ongoingTutionCount();
$publishedVideoCount = $User->publishedVideoCount();
$uploadedVideoCount = $User->uploadedVideoCount();
$isFreeVideoCount = $User->isFreeVideoCount();
$inactiveStudentCount = $User->inactiveStudentCount();
?>



<div class="col-xl-12">
    <section class="box nobox">
        <div class="content-body">
            <div class="row">
                
                <div class="col-xl-3 col-lg-4 col-12 col-md-6">
                    <div class="tile-counter bg-primary">
                        <div class="content">
                            <i class='fa fa-user icon-lg'></i>
                            <h2><?= $enabledTutorCount; ?></h2>
                            <div class="clearfix"></div>
                            <span>Active Tutors</span>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-12 col-md-6">
                    <div class="tile-counter bg-secondary">
                        <div class="content">
                            <i class='fa fa-user icon-lg'></i>
                            <h2><?= $disabledTutorCount; ?></h2>
                            <div class="clearfix"></div>
                            <span>Inactive Tutors</span>
                        </div>
                    </div>
                </div>
                
                <div class="col-xl-3 col-lg-4 col-12 col-md-6">
                    <div class="tile-counter bg-orange">
                        <div class="content">
                            <i class='fa fa-users icon-lg'></i>
                            <h2><?= $enabledStudentCount; ?></h2>
                            <div class="clearfix"></div>
                            <span>Active Students</span>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-12 col-md-6">
                    <div class="tile-counter bg-info">
                        <div class="content">
                            <i class='fa fa-users icon-lg'></i>
                            <h2><?= $inactiveStudentCount; ?></h2>
                            <div class="clearfix"></div>
                            <span>Inactive Students</span>
                        </div>
                    </div>
                </div>
                
                <div class="col-xl-3 col-lg-4 col-12 col-md-6">
                    <div class="tile-counter bg-purple">
                        <div class="content">
                            <i class='fa fa-video-camera icon-lg'></i>
                            <h2><?= $scheduleTutionCount; ?></h2>
                            <div class="clearfix"></div>
                            <span>Scheduled Lessons</span>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-12 col-md-6">
                    <div class="tile-counter bg-warning">
                        <div class="content">
                            <i class='fa fa-video-camera icon-lg'></i>
                            <h2><?= $ongoingTutionCount; ?></h2>
                            <div class="clearfix"></div>
                            <span>On Going Lessons</span>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-12 col-md-6">
                    <div class="tile-counter bg-success">
                        <div class="content">
                            <i class='fa fa-video-camera icon-lg'></i>
                            <h2><?= $publishedTutionCount; ?></h2>
                            <div class="clearfix"></div>
                            <span>Recorded Videos</span>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-12 col-md-6">
                    <div class="tile-counter bg-dark">
                        <div class="content">
                            <i class='fa fa-film icon-lg text-light'></i>
                            <h2><?= $isFreeVideoCount; ?></h2>
                            <div class="clearfix"></div>
                            <span>Free Videos</span>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-12 col-md-6">
                    <div class="tile-counter bg-orange">
                        <div class="content">
                            <i class='fa fa-film icon-lg'></i>
                            <h2><?= $publishedVideoCount; ?></h2>
                            <div class="clearfix"></div>
                            <span>Publish Videos</span>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-12 col-md-6">
                    <div class="tile-counter bg-info">
                        <div class="content">
                            <i class='fa fa-film icon-lg'></i>
                            <h2><?= $uploadedVideoCount; ?></h2>
                            <div class="clearfix"></div>
                            <span>Uploaded Videos</span>
                        </div>
                    </div>
                </div>
                
            </div>
            
        </div>
    </section>
    
</div>
