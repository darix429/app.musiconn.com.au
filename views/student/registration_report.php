<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Student Registrations Report');
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs(
        '
        
        $(document).on("click","#search_btn", function(){        
            $.ajax({
                type: "GET",
                url: "' . Url::to(['/student/registration-report/']) . '",
                data: $("#search_frm").serialize(),
                beforeSend:function(){ blockBody(); },
                success:function(result){
                    unblockBody();
                    $("#student_table_parent_div").html(result);
                    window.ULTRA_SETTINGS.dataTablesInit();
                },
                error:function(e){ 
                    unblockBody();
                    showErrorMessage(e.responseText);
                }
            });
        });
        
        $(document).on("click","#search_reset_btn", function(){        
            $.ajax({
                type: "GET",
                url: "' . Url::to(['/student/registration-report/']) . '",
                data: {},
                beforeSend:function(){ blockBody(); },
                success:function(result){
                    unblockBody();
                    $("#student_table_parent_div").html(result);
                    window.ULTRA_SETTINGS.dataTablesInit();
                },
                error:function(e){ 
                    unblockBody();
                    showErrorMessage(e.responseText);
                }
            });
        });
    '
);
?>


<div class="col-xl-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Registration List</h2>
            <div class="actions panel_actions float-right">
                <i class="fa fa-search icon-primary icon-xs icon-orange mail_head_icon custom-toggle" data-togglediv="datatable-search-div" title="Advance Search"></i>
            </div>
        </header>
        <div class="content-body">  
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12 ">
                    <div class="datatable-search-div " style="display: none;">

                        <?php
                        $form = ActiveForm::begin([
                                    'method' => 'get',
                                    'options' => [
                                        'id' => 'search_frm',
                                        'class' => 'form-inline'
                                    ],
                        ]);
                        ?>
                        <div class="col-md-3 m-0 pr-0">
                            <?= $form->field($searchModel, 'created_at')->textInput(['class' => 'form-control daterange  add-date-ranges', 'maxlength' => true, 'placeholder' => 'Registration Date', 'data-format' => 'DD-MM-YYYY' ])->label(false) ?>
		        </div>
                        <div class="col-md-2 m-0 pr-0">
                            <?= $form->field($searchModel, 'first_name')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'First Name', 'style' => 'width:100%'])->label(false) ?>
                        </div>
                        <div class="col-md-3 m-0 pr-0">
                            <?= $form->field($searchModel, 'email')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $searchModel->getAttributeLabel('email'), 'style' => 'width:100%'])->label(false) ?>
                        </div>
                        <div class="col-md-2 m-0 ">
                            <?= $form->field($searchModel, 'status')->dropDownList(['ENABLED' => 'ENABLED', 'DISABLED' => 'DISABLED', 'DELETED' => 'DELETED','INACTIVE'=>'INACTIVE'], ['prompt' => 'Select Status','class' => 'form-control', 'style' => 'width:100%'])->label(false) ?>
                        </div>    
                        
                            <?= Html::Button(Yii::t('app', 'Search'), ['class' => 'btn btn-primary mb-0', 'id' => 'search_btn']) ?>
                            <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-purple mb-0', 'id'=>'search_reset_btn']) ?>
                        

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>

            </div>
            <br>
            <div class="clearfix"></div>
            <div class="row" id="student_table_parent_div">
                <?php echo Yii::$app->controller->renderPartial('_registration_report_list', ['studentList' => $studentList]); ?>
            </div>
        </div>
    </section>
</div>

