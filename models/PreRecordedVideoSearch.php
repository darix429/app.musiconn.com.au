<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PreRecordedVideo;

/**
 * PreRecordedVideoSearch represents the model behind the search form of `app\models\PreRecordedVideo`.
 */
class PreRecordedVideoSearch extends PreRecordedVideo {

    /**
     * {@inheritdoc}
     */
    public $uploaded_by, $published_by;

    public function rules() {
        return [
                [['uuid', 'title', 'description', 'file', 'instrument_uuid', 'instrument_category_uuid', 'status', 'uploaded_date', 'publish_date', 'expire_date', 'rejected_reason', 'created_at', 'updated_at', 'user_uuid', 'is_free', 'uploaded_by', 'published_by'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $modeDatatable = false, $statusnot = '', $where = '') {
        $query = PreRecordedVideo::find()->joinWith(['instrumentCategoryUu']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($where)) {
            $query->andWhere($where);
        }

        if (!empty($statusnot)) {
            if (is_array($statusnot)) {

                $query->andWhere(['not in', 'pre_recorded_video.status', $statusnot]);
            } else {

                $query->andWhere(['<>', 'pre_recorded_video.status', $statusnot]);
            }
        }
        $query->andFilterWhere(['status' => $this->status]);

        // grid filtering conditions
        $query->andFilterWhere([
            "to_char(uploaded_date, 'DD-MM-YYYY')" => $this->uploaded_date,
            "to_char(publish_date, 'DD-MM-YYYY')" => $this->publish_date,
            'expire_date' => $this->expire_date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'CAST(instrument_uuid AS TEXT)' => $this->instrument_uuid,
                // 'CAST(instrument_category_uuid AS TEXT)'=> $this->instrument_category_uuid,
        ]);

        if (!empty($this->instrument_category_uuid)) {
            $query->andWhere('CAST(instrument_category_uuid AS TEXT) = \'' . $this->instrument_category_uuid . '\' OR CAST(instrument_category.parent_id AS TEXT) =  \'' . $this->instrument_category_uuid . '\' ');
        }

        if (Yii::$app->user->identity->role == 'TUTOR') {
            $query->andFilterWhere(['user_uuid' => Yii::$app->user->identity->id]);
        }

        $query->andFilterWhere(['ilike', 'uuid', $this->uuid])
                ->andFilterWhere(['ilike', 'title', $this->title])
                ->andFilterWhere(['ilike', 'description', $this->description])
                ->andFilterWhere(['ilike', 'file', $this->file])
                ->andFilterWhere(['ilike', 'status', $this->status])
                ->andFilterWhere(['ilike', 'rejected_reason', $this->rejected_reason]);

        if ($modeDatatable) {
            //echo $query->createCommand()->getRawSql();exit;
            $result = $query->asArray()->orderBy('updated_at')->all();
            return $result;
        }
        return $dataProvider;
    }

    public function searchReport($params, $modeDatatable = false, $statusnot = '', $statusIn = '') {
        
        $query = PreRecordedVideo::find();
        $query->joinWith(['instrumentCategoryUu']);
        $query->joinwith(['userUu']);
        $query->joinwith(['adminUu']);
        $query->joinwith(['tutorUu']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($where)) {
            $query->andWhere($where);
        }

        if (!empty($statusnot)) {
            if (is_array($statusnot)) {

                $query->andWhere(['not in', 'pre_recorded_video.status', $statusnot]);
            } else {

                $query->andWhere(['<>', 'pre_recorded_video.status', $statusnot]);
            }
        }

        if (!empty($statusIn)) {
            if (is_array($statusIn)) {

                $query->andWhere(['in', 'pre_recorded_video.status', $statusIn]);
            }
        }

        if ($this->status !== 'ALL') {
            $query->andFilterWhere(['pre_recorded_video.status' => $this->status]);
        }

        // grid filtering conditions
//        $published = explode("_", $this->published_by);
//        $published_by = [];
//        if ($published[0]) {
//            $published_by = $published[1];
//        } else {
//            $published_by = '';
//        }

       

        $query->andFilterWhere([
            "to_char(uploaded_date, 'DD-MM-YYYY')" => $this->uploaded_date,
            "to_char(publish_date, 'DD-MM-YYYY')" => $this->publish_date,
            'expire_date' => $this->expire_date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'CAST(instrument_uuid AS TEXT)'=> $this->instrument_uuid,
                // 'CAST(instrument_category_uuid AS TEXT)'=> $this->instrument_category_uuid,
        ]);
        
        if(!empty($this->published_by)){
            $query->andFilterWhere(['pre_recorded_video.published_by_uuid' => $this->published_by]);
        }
        
        if(!empty($this->uploaded_by)){
            $uploaded_array = explode("_", $this->uploaded_by);
            if(!empty($uploaded_array[0]) && !empty($uploaded_array[1])){
                if($uploaded_array[1] == 'TUTOR'){                    
                    $query->andFilterWhere(['pre_recorded_video.tutor_uuid' => $uploaded_array[0]]);
                }else{                    
                    $query->andFilterWhere(['pre_recorded_video.admin_uuid' => $uploaded_array[0]]);
                }
            }
        }

        if (!empty($this->instrument_category_uuid)) {
            $query->andWhere('CAST(instrument_category_uuid AS TEXT) = \'' . $this->instrument_category_uuid . '\' OR CAST(instrument_category.parent_id AS TEXT) =  \'' . $this->instrument_category_uuid . '\' ');
        }

        if (Yii::$app->user->identity->role == 'TUTOR') {
            $query->andFilterWhere(['user_uuid' => Yii::$app->user->identity->id]);
        }

        $query->andFilterWhere(['ilike', 'uuid', $this->uuid])
                ->andFilterWhere(['ilike', 'title', $this->title])
                ->andFilterWhere(['ilike', 'description', $this->description])
                ->andFilterWhere(['ilike', 'file', $this->file])
//            ->andFilterWhere(['ilike', 'status', $this->status])
                ->andFilterWhere(['ilike', 'CAST(is_free AS TEXT)', $this->is_free])
                ->andFilterWhere(['ilike', 'rejected_reason', $this->rejected_reason]);

        if ($modeDatatable) {
            //echo $query->createCommand()->getRawSql();exit;
            $result = $query->asArray()->orderBy('updated_at')->all();
            return $result;
        }
        return $dataProvider;
    }


    public function searchCategoryList($params, $modeDatatable = false, $statusnot = '', $where = '') {
        $query = PreRecordedVideo::find()->joinWith(['instrumentCategoryUu']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($where)) {
            $query->andWhere($where);
        }

        if (!empty($statusnot)) {
            if (is_array($statusnot)) {

                $query->andWhere(['not in', 'pre_recorded_video.status', $statusnot]);
            } else {

                $query->andWhere(['<>', 'pre_recorded_video.status', $statusnot]);
            }
        }
        // $query->andFilterWhere(['status' => $this->status]);
        $query->andFilterWhere(['instrument_category_uuid' => $this->instrument_category_uuid]);

        // grid filtering conditions
        $query->andFilterWhere([
            "to_char(uploaded_date, 'DD-MM-YYYY')" => $this->uploaded_date,
            "to_char(publish_date, 'DD-MM-YYYY')" => $this->publish_date,
            'expire_date' => $this->expire_date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'CAST(instrument_uuid AS TEXT)' => $this->instrument_uuid,
                // 'CAST(instrument_category_uuid AS TEXT)'=> $this->instrument_category_uuid,
        ]);

        if (!empty($this->instrument_category_uuid)) {
            $query->andWhere('CAST(instrument_category_uuid AS TEXT) = \'' . $this->instrument_category_uuid . '\' OR CAST(instrument_category.parent_id AS TEXT) =  \'' . $this->instrument_category_uuid . '\' ');
        }

        if (Yii::$app->user->identity->role == 'TUTOR') {
            $query->andFilterWhere(['user_uuid' => Yii::$app->user->identity->id]);
        }

        $query->andFilterWhere(['ilike', 'uuid', $this->uuid])
                ->andFilterWhere(['ilike', 'title', $this->title])
                ->andFilterWhere(['ilike', 'description', $this->description])
                ->andFilterWhere(['ilike', 'file', $this->file])
                ->andFilterWhere(['ilike', 'status', $this->status])
                ->andFilterWhere(['ilike', 'rejected_reason', $this->rejected_reason]);

        if ($modeDatatable) {
            // echo $query->createCommand()->getRawSql();exit;
            $result = $query->asArray()->orderBy('updated_at')->all();
            return $result;
        }
        return $dataProvider;
    }

}
