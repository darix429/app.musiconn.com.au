<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;
use app\models\Tutor;
use yii\helpers\ArrayHelper;

$this->title = Yii::t('app', 'Pre Recorded Videos View History Detail');
$this->params['breadcrumbs'][] = $this->title;

$tutors = Tutor::find()->where(['status' => 'ENABLED'])->asArray()->all();
$tutorList = ArrayHelper::map($tutors, 'uuid', function($model) {
            return $model['first_name'] . ' ' . $model['last_name'];
        });

$this->registerJs(
        '
        $(document).on("click","#search_btn", function(){        
            $.ajax({
                type: "GET",
                url: "' . Url::to(['/student-view-pre-recorded-lesson/view/']) . '",
                data: $("#search_frm").serialize(),
                beforeSend:function(){ blockBody(); },
                success:function(result){
                    unblockBody();
                    $("#lesson_table_parent_div_detail").html(result);
                    window.ULTRA_SETTINGS.dataTablesInit();
                },
                error:function(e){ 
                    unblockBody();
                    showErrorMessage(e.responseText);
                }
            });
        });
$(document).on("click","#search_reset_btn", function(){        
    $.ajax({
        type: "GET",
        url: "' . Url::to(['/student-view-pre-recorded-lesson/view/', 'id' => isset($_GET['id'])?$_GET['id']:'']) . '",
        data: {},
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            $("#lesson_table_parent_div_detail").html(result);
            window.ULTRA_SETTINGS.dataTablesInit();
        },
        error:function(e){ 
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});
    
    
', View::POS_END
);
?>

<div class="col-xl-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left"><?= $this->title; ?></h2>
            <div class="actions panel_actions float-right">
                <i class="fa fa-search icon-primary icon-xs icon-orange mail_head_icon custom-toggle" data-togglediv="datatable-search-div" title="Advance Search"></i>
                <?php if(isset($_GET['id'])) { ?>
                <a href="<?= Yii::$app->getUrlManager()->createUrl(['/student-view-pre-recorded-lesson/view/']) ?>" class="btn btn-purple btn-icon"><span>All Pre Recorded Lessons</span></a>
                <?php } ?>
                <a href="<?= Yii::$app->getUrlManager()->createUrl(['/student-view-pre-recorded-lesson/index/']) ?>" class="btn btn-info btn-icon "><i class="fa fa-arrow-left text-white"></i> <span>Back</span></a>
            </div>
        </header>
        <div class="content-body"> 
            <div class="row ">
                <div class="col-lg-12 col-md-12 col-12 ">
                    <div class="datatable-search-div " style="display: none;">

                        <?php
                        $form = ActiveForm::begin([
                                    'method' => 'get',
                                    'options' => [
                                        'id' => 'search_frm',
                                        'class' => 'form-inline'
                                    ],
                        ]);
                        ?>
                        <!--                   	    <div class="col-md-3 mb-0">
                        <?php //echo  $form->field($searchModel,'title')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Title', 'style' => 'width:100%'])->label(false)   ?>
                                                        </div>-->
                        <div class="col-md-3 mb-0">
                            <?php echo $form->field($searchModel, 'datetime')->textInput(['class' => 'form-control daterange  add-date-ranges', 'maxlength' => true, 'placeholder' => 'Date', 'data-format' => 'DD-MM-YYYY'])->label(false) ?>
                        </div>

                        <div class="col-md-3 mb-0">
                            <?php echo $form->field($searchModel, 'studentName')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Student', 'style' => 'width:100%'])->label(false) ?>
                        </div>

                        <div class="col-md-3 mb-0">
                            <?php echo $form->field($searchModel, 'studentId')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'StudentID', 'style' => 'width:100%'])->label(false) ?>
                        </div>
                        <?php if(!isset($_GET['id']) || $_GET['id'] == '') { ?>
                        <div class="col-md-3 mb-0">
                            <?=
                            $form->field($searchModel, 'tutor_uuid')->dropDownList(
                                    $tutorList, ['prompt' => 'Select Tutor..','class'=>'form-control myselect2']
                            )->label(false);
                            ?>  
                        </div>

                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'instrumentName')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Instrument', 'style' => 'width:100%'])->label(false)   ?>
                        </div>
                        <?php } ?>

                        <?= Html::Button(Yii::t('app', 'Search'), ['class' => 'btn btn-primary mb-0', 'id' => 'search_btn', 'style' => 'margin-left: 17px;']) ?>
                        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-purple mb-0', 'id' => 'search_reset_btn']) ?>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
            <br>
            <div class=""></div>
            <div class="clearfix"></div>
            <div class="row" id="lesson_table_parent_div_detail">
                <?php echo Yii::$app->controller->renderPartial('_lesson_detail_list', ['lessonList' => $lessonList]); ?>
            </div>
        </div>
    </section>
</div>
