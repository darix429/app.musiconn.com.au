function showErrorMessage(msg) {
    
    var msg1 = (typeof msg === "undefined" || msg == '') ? 'Sorry, something wrong' : msg;
    
    Messenger({
        extraClasses: 'messenger-fixed messenger-on-right messenger-on-top',
        theme: 'flat'
    }).post({
        message: msg1,
        type: 'error',
        showCloseButton: true
    });
}

function progressMessage() {
    var i = 0;
    Messenger({
        extraClasses: 'messenger-fixed messenger-on-right messenger-on-top',
        theme: 'flat'
    }).run({
        errorMessage: 'Error destroying alien planet. Retrying...',
        successMessage: 'Alien planet destroyed!',
        action: function(opts) {
            if (++i < 2) {
                return opts.error({
                    status: 500,
                    readyState: 0,
                    responseText: 0
                });
            } else {
                return opts.success();
            }
        }
    });
}

function showSuccess(msg) {
    var msg1 = (typeof msg === "undefined" || msg == '') ? 'Sorry, something wrong' : msg;
    Messenger({
        extraClasses: 'messenger-fixed messenger-on-right messenger-on-top',
        theme: 'flat'
    }).post({
            message: msg1,
        showCloseButton: true
    });
    //}).post(msg);
}
