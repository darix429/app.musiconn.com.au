<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentTransaction */

$this->title = $model->uuid;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Payment Transactions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-transaction-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->uuid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->uuid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'uuid',
            'student_uuid',
            'transaction_number:ntext',
            'book_uuid',
            'type:ntext',
            'payment_datetime',
            'amount',
            'payment_method:ntext',
            'reference_number:ntext',
            'status:ntext',
            'subscription_uuid',
            'response:ntext',
        ],
    ]) ?>

</div>
