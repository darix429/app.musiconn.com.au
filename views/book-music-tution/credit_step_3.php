<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Url;
use app\models\Instrument;
use app\models\CreditSession;
use app\models\Tutor;

$this->title = Yii::t('app', 'BOOK CREDITED MUSIC LESSON');
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_icon'] = 'my-lesson-tag.png';

$available_tutorial_type_code = CreditSession::getStudentCreditSessionTutorialType(Yii::$app->user->identity->student_uuid);

$tutor_uuid = $step_data['step_2']['tutor_uuid'];
$student_uuid = Yii::$app->user->identity->student_uuid;
$tutorDetail = Tutor::findOne($tutor_uuid);


//echo "<pre>";
//print_r($available_tutorial_type_code);
//echo "</pre>";
if (isset($step_data['step_3']) && !empty($step_data['step_3'])) {
    $model->tutorial_type_code = $step_data['step_3']['tutorial_type_code'];
}


if (!empty($step_data['step_1']['instrument_uuid'])) {
    $instrumentDetail = Instrument::find()->where(['uuid' => $step_data['step_1']['instrument_uuid']])->one();
} else {
    return \yii\web\Controller::redirect(['credit-schedule']);
}

$this->registerJs(
        '
$(document).on("ifClicked","input[name=\"StudentTuitionBooking[tutorial_type_code_checkbox]\"]",function (event) {

    $("#studenttuitionbooking-tutorial_type_code").val(this.value);
    $(".tutorial_select_btn").addClass("d-none");
    $(this).closest("div.onlinetutorial_tr").find(".tutorial_select_btn").removeClass("d-none");

});


$( "#frm_step_3,#gostep1" ).submit(function( event ) {
       // blockBody();
    });
', View::POS_END
);
?>
<section class="content">
    <label class="warning-info mar-bottom-10">For issues concerning tutor availability, please contact <a href="mailto:<?= Yii::$app->params['adminEmail']; ?>"><?= Yii::$app->params['adminEmail']; ?></a> for assistance.</label>
    <div class="row mar-bottom-10">
        <div class="col-xs-12">
            <div class="box">
                <div class="col-md-12 box-panel-header ">
                    <h4 class="col-md-6">Select Instrument &nbsp;<i class="fa fa-check"></i></h4>
                    <div class="col-md-6 ">
                        <?php echo Html::beginForm(['credit-schedule'], 'post', ['id' => 'gostep1', 'class' => 'form-inline d-block']) ?>
                        <input type="hidden" name="step" value="1">
                        <input type="hidden" name="prev" value="1">
                        <a href="javascript:$('#gostep1').submit();" class="box-panel-header-right-btn pull-right">
                            Change
                        </a>
                        <p class="box-panel-header-right-p text-white pull-right"> <?= $instrumentDetail->name; ?>&nbsp;</p>
                        <?php echo Html::endForm() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
   <div class="row mar-bottom-10">
        <div class="col-xs-12">
            <div class="box">
                <div class="col-md-12 box-panel-header ">
                    <h4 class="col-md-6">Select Tutor &nbsp;<i class="fa fa-check"></i></h4>
                    <div class="col-md-6 ">
                        <?php echo Html::beginForm(['credit-schedule'], 'post', ['id' => 'gostep2', 'class' => 'form-inline d-block']) ?>
                        <input type="hidden" name="step" value="2">
                        <input type="hidden" name="prev" value="2">
                        <a href="javascript:$('#gostep2').submit();" class="box-panel-header-right-btn pull-right">
                            Change
                        </a>
                        <p class="box-panel-header-right-p text-white pull-right"> <?= $tutorDetail->first_name . ' ' . $tutorDetail->last_name; ?>&nbsp;</p>
                        <?php echo Html::endForm() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row mar-bottom-10">
        <div class="col-xs-12">
            <div class="box">
                <div class="col-md-12 box-panel-header border-bottom-radious-none">
                    <h4 class="col-md-6">Select Tutorial Type</h4>
                    <div class="col-md-6 "> </div>
                </div>
                <div class="box-body box-panel-body" style="max-height: 500px;overflow-x: hidden;overflow-y: scroll;border-top-left-radius: 0px;border-top-right-radius: 0px;">
                    <div class="row vertical-center-box vertical-center-box-tablet"  style="">
                        <?php
                        $form = ActiveForm::begin([
                                    'action' => ['credit-schedule'],
                                    'id' => 'frm_step_3',
                                    'enableClientValidation' => true,
//                                'enableAjaxValidation' => true,
                                    'validateOnChange' => true,
                                    'options' => ['class' => 'form-horizontal']]);
                        ?>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <div class="row" style="">
                                    <input type="hidden" name="step" value="3">
                                    <?= $form->field($model, 'tutorial_type_code')->hiddenInput(['class' => 'form-control', 'maxlength' => true,])->label(false) ?>
                                </div>

                                <?php if (!empty($available_tutorial_type_code)) { ?>
                                    <?php
                                    foreach ($available_tutorial_type_code as $key => $value) {

                                        $displayClass = ($model->tutorial_type_code == $value['code']) ? "" : "d-none";
                                        ?>

                                        <div class="row pakage-list onlinetutorial_tr" >
                                            <div class="col-md-2 text-center">
                                                <input tabindex="5" type="radio"  class="skin-square-red float-left tutorial_type_checkbox" name="StudentTuitionBooking[tutorial_type_code_checkbox]" value="<?= $value['code'] ?>" <?= ($model->tutorial_type_code == $value['code']) ? "checked" : "" ?>>
                                            </div>

                                            <div class="col-md-8">

                                                <p class="">
                                                    <?= $value['min'] . " Minutes" ?>
                                                </p>
                                            </div>
                                            <div class="col-md-2">
                                                <?= Html::submitButton('Select', ['class' => "btn btn-primary btn-md tutorial_select_btn $displayClass"]) ?>

                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php
                                } else {
                                    echo "No record found.";
                                }
                                ?>
                            </div>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
 
    <div class="row mar-bottom-10">
        <div class="col-xs-12">
            <div class="box">
                <div class="col-md-12 box-panel-header ">
                    <h4 class="col-md-6">Select Date</h4>
                    <div class="col-md-6 "></div>
                </div>
            </div>
        </div>
    </div>
    <a class="btn btn-primary btn-sm black_button_in" href="<?= Yii::$app->getUrlManager()->createUrl('/booked-session/my-lessons'); ?>">RETURN TO MY LESSONS</a>
</section>