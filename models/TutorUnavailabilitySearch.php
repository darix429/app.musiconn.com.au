<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TutorUnavailability;

/**
 * TutorUnavailabilitySearch represents the model behind the search form of `app\models\TutorUnavailability`.
 */
class TutorUnavailabilitySearch extends TutorUnavailability
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'tutor_uuid', 'start_datetime', 'end_datetime', 'reason', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TutorUnavailability::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'start_datetime' => $this->start_datetime,
            'end_datetime' => $this->end_datetime,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['ilike', 'uuid', $this->uuid])
            ->andFilterWhere(['ilike', 'tutor_uuid', $this->tutor_uuid])
            ->andFilterWhere(['ilike', 'reason', $this->reason]);

        return $dataProvider;
    }
}
