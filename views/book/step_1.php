<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Url;
use app\libraries\General;
use app\models\StudentTuitionBooking;

$this->title = Yii::t('app', 'BOOK A MUSIC LESSON');
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_icon'] = 'my-lesson-tag.png';

$instrumentList = StudentTuitionBooking::getInstrumentList();


if (isset($step_data['step_1']['instrument_uuid']) && $step_data['step_1']['instrument_uuid'] != "") {
    $model->instrument_uuid = $step_data['step_1']['instrument_uuid'];
    $model->instrumentcheckbox = $model->instrument_uuid;
}

$this->registerJs(
        '
    $("input[name=\"Book[instrumentcheckbox]\"]").on("click", function (event) {
        $("#book-instrument_uuid").val(this.value);
        $(".instrument_select_btn").addClass("d-none")
        $(this).closest("li.instrument_tr").find(".instrument_select_btn").removeClass("d-none")
    });

    $( "#frm_step_1" ).submit(function( event ) {
        blockBody();
    });

$(".new_when_startdate").datetimepicker({
                inline: true,
                sideBySide: true
            });
        ', View::POS_END
);
?>

<?= Yii::$app->controller->renderPartial('step_prev_booking', ['model' => $model]); ?>


<!--<div class="new_when_startdate"></div>-->

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-12"><strong>BOOK A NEW MUSIC LESSON</strong></div><br>
            <div class="panel-group box-header with-border" id="accordion" role="tablist" aria-multiselectable="true">
                
                <!-- SELECT INSTRUMENT -->
                <div class="panel panel-default">

                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseInstrument" aria-expanded="false" aria-controls="collapseTwo">
                                SELECT INSTRUMENT 
                                <!--<i class="fa fa-check" aria-hidden="true"></i>-->
                            </a>
                            <!--<span class="pull-right">Ukulele <a href="#"><div class="label label-warning hm-1 mr-2 siba">CHANGE</div></a></span>-->
                        </h4>
                    </div>
                    <div class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                        <?php
//                        echo "<pre>";
//                        print_r($model->getErrors());
//                        echo "</pre>";
                        
                        $form = ActiveForm::begin([
                                    'action' => ['index'],
                                    'id' => 'frm_step_1',
                                    'enableClientValidation' => true,
                                    'validateOnChange' => true,
                                    'options' => ['class' => 'form-horizontal']]);
                        ?>
                        <div class="panel-body">
                            <?php
                            $instrument_array = \yii\helpers\ArrayHelper::map($instrumentList, 'uuid', function($e) {

                                $inst_img_dataUri = \app\libraries\General::getInstrumentImage($e->uuid);
                                $h = '<img class="sel_i" src="' . $inst_img_dataUri . '">' . $e->name;
                                return $h;
                            });
                            
                            $model->instrumentcheckbox = $model->instrument_uuid;
                            $model_instrument_uuid = $model->instrumentcheckbox;
                            
                            echo $form->field($model, 'instrumentcheckbox', ['template' => '{input}', 'options' => ['tag' => false]])
                                    ->radioList(
                                            $instrument_array, [
                                        'item' => function($index, $label, $name, $checked, $value) use($model_instrument_uuid) {

                                        $displayClass = ($model_instrument_uuid == $value) ? "" : "d-none";
                                        $checked_val = ($model_instrument_uuid == $value) ? 'checked="checked"' : "";
					$btn = ' <button style="cursor: pointer; padding-top: 4px;" class="label label-warning custom-hm-1 instrument_select_btn ' . $displayClass . '">NEXT</button>';
                                        //$btn = '<button class="label label-warning hm-1 mr-2 black1 instrument_select_btn '.$displayClass.'">NEXT</button>';
                                        
                                            $html = '<div class="col-md-4 ">';
                                            $html .= '<ul class="products-list product-list-in-box">';
                                            $html .= '<li class="item item_tb instrument_tr">';
                                            $html .= '<label class="contai_r">';
                                            $html .= '<input type="radio" value="' . $value . '" name="' . $name . '" '.$checked_val.'>';
                                            $html .= '<span class="checkmark"></span>';
                                            $html .= '</label>';
                                            $html .= $label . $btn;
                                            $html .= '</li>';
                                            $html .= '</ul>';
                                            $html .= '</div>';
                                            return $html;
                                        }
                                            ]
                                    )
                                    ->label(false);
                            ?>

                            <div class="col-md-12" style="">
                                <input type="hidden" name="step" value="1">
                                <?= $form->field($model, 'instrument_uuid')->hiddenInput(['class' => 'form-control', 'maxlength' => true,])->label(false) ?>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
                <!-- END SELECT INSTRUMENT -->
                
                <!-- SELECT TUTOR -->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTutor" aria-expanded="false" aria-controls="collapseThree">
                                SELECT TUTOR
                            </a>
                        </h4>
                    </div>
                </div>
                <!-- END SELECT TUTOR -->
                
                <!-- SELECT LENGTH OF LESSON -->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingfour">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseLength" aria-expanded="false" aria-controls="collapsefour">
                                SELECT LENGTH OF LESSON
                            </a>
                        </h4>
                    </div>
                </div>
                <!--END SELECT LENGTH OF LESSON -->
                
                <!-- SELECT DATE AND TIME -->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingfive">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseDate" aria-expanded="false" aria-controls="collapsefive">
                                SELECT DATE AND TIME
                            </a>
                        </h4>
                    </div>
                </div>
                <!--END SELECT DATE AND TIME -->
                
                <!-- CONFIRM BOOKING AND PAYMENT -->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingsix">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsePayment" aria-expanded="false" aria-controls="collapsesix">
                                CONFIRM BOOKING AND PAYMENT
                            </a>
                        </h4>
                    </div>
                </div>
                <!--END CONFIRM BOOKING AND PAYMENT -->
                
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <a class="btn btn-primary btn-sm black_button_in" href="<?= Yii::$app->getUrlManager()->createUrl('/student/home'); ?>">RETURN TO HOME</a>
        </div>
    </div>
</section>
