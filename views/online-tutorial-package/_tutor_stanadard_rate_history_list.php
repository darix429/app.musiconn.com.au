<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\TutorialType;
use app\models\PaymentTerm;
use app\models\OnlineTutorialPackageType;
?>
<div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">

    <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Tutor</th>
                <th>Instrument</th>
                <th style="text-align: center;">Changed Category</th>
                <th style="text-align: center;">Changed Option</th>
                <th>Date Time</th>

            </tr>
        </thead>

        <tbody>
            <?php
            if (!empty($historyList)) {
                foreach ($historyList as $key => $value) {
                    ?>
                    <tr>
                        <td><?= $key + 1; ?></td>
                        <td><?= $value['tutorUu']['first_name'] . ' ' . $value['tutorUu']['last_name']; ?></td>
                        <td><?= $value['instrumentUu']['name']; ?></td>
                        <td align="center">

                            <?php
                            ?>


        <?php
        if (!empty($value['old_online_tutorial_package_type_uuid']) && $value['old_online_tutorial_package_type_uuid'] != 'NULL') {
            $type_model = OnlineTutorialPackageType::find()->where(['uuid' => $value['old_online_tutorial_package_type_uuid']])->one();
            $old_category = $type_model->display_name;

            if ($type_model->name == 'PREMIUM') {
                $e_class = "badge-success";
                $pointer = 'style="color:#fff"';
            } else {
                $e_class = "badge-warning";
                $pointer = 'style="color:#fff"';
            }

            if ($value['onlineTutorialPackageTypeUu']['name'] == 'PREMIUM') {
                $e2_class = "badge-success";
                $pointer2 = 'style="color:#fff"';
            } else {
                $e2_class = "badge-warning";
                $pointer2 = 'style="color:#fff"';
            }
            ?><span class="badge badge-pill <?= $e_class ?>"  data-uuid="<?= $value['uuid']; ?>" <?= $pointer; ?>><?= strtoupper($old_category); ?></span> => <span class="badge badge-pill <?= $e2_class ?>"  data-uuid="<?= $value['uuid']; ?>" <?= $pointer2; ?>><?= strtoupper($value['onlineTutorialPackageTypeUu']['display_name']); ?></span>

                            <?php
                            } else {

                                if ($value['onlineTutorialPackageTypeUu']['name'] == 'PREMIUM') {
                                    $e2_class = "badge-success";
                                    $pointer2 = 'style="color:#fff"';
                                } else {
                                    $e2_class = "badge-warning";
                                    $pointer2 = 'style="color:#fff"';
                                }
                                ?>
                                <span class="badge badge-pill <?= $e2_class ?>"  data-uuid="<?= $value['uuid']; ?>" <?= $pointer2; ?>><?= strtoupper($value['onlineTutorialPackageTypeUu']['display_name']); ?></span>
                            <?php }
                            ?>
                        </td>
                        <td align="center">

                            <?php
                            if ((!empty($value['old_option'])) && ($value['old_option'] != 'NULL' || $value['old_option'] != 0) && (!empty($value['option'])) && ($value['option'] != 'NULL' || $value['option'] != 0)) {

                                echo $value['old_option'] . " => " . $value['option'];
                            } else {
                                if ((!empty($value['option'])) && ($value['option'] != 'NULL' || $value['option'] != 0)) {
                                    echo $value['option'];
                                }
                            }
                            ?>

                        </td>
                        <td ><?= \app\libraries\General::displayDateTime($value['datetime']); ?></td>


                    </tr>
                            <?php
                        }
                    }
                    ?>
        </tbody>
    </table>

</div>
