<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\libraries\General;

$timezone = General::getAllTimeZones();
if ($userModel->timezone == '') {
    $userModel->timezone = Yii::$app->params['timezone'];
}

$this->registerJs('

$("#student-country").change(function(){
                if($(this).val() == "other"){
                    $(".state_dd").hide();
                    $(".state_text").show();
                }else{
                    $(".state_text").hide();
                    $(".state_dd").show();
                }
            })
            
$(".birthdate").datepicker({
        minViewMode:  0,
        format: "dd-mm-yyyy",
        /* startDate: getValue($this, "startDate", ""),
        endDate: getValue($this, "endDate", ""),*/
        /* daysOfWeekDisabled: getValue($this, "disabledDays", ""),*/
        startView: 2,
        autoclose:true
    });
    
//$(".myselect2").select2();

');
?>

<div class="modal-body">
    <div class="row vertical-center-box vertical-center-box-tablet">
        <?php $form = ActiveForm::begin(['id' => 'form-profile-update']); ?>
        <div class="row ">
            <div class="col-sm-6 col-md-6 col-lg-6">
                <?=
                        $form->field($model, 'first_name', ['template' => '<span>{label}</span>{input}{error}'])
                        ->textInput(['class' => 'form-control', 'maxlength' => true,])
                        ->label($model->getAttributeLabel('first_name') . '*')
                ?>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-6">
                <?=
                        $form->field($model, 'last_name', ['template' => '<span>{label}</span>{input}{error}'])
                        ->textInput(['class' => 'form-control', 'maxlength' => true,])
                        ->label($model->getAttributeLabel('last_name') . '*')
                ?>
            </div>
        </div>
        <div class="row ">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <?=
                        $form->field($userModel, 'email', ['template' => '<span>{label}</span>{input}{error}'])
                        ->textInput(['class' => 'form-control', 'maxlength' => true,])
                        ->label($userModel->getAttributeLabel('email') . '*')
                ?>
            </div>
        </div>

        <div class="row ">
            <div class="col-sm-6 col-md-6 col-lg-6">
                <?php $model->birthdate = \app\libraries\General::displayDate($model->birthdate); ?>
                <?=
                        $form->field($model, 'birthdate', ['template' => '<span>{label}</span>{input}{error}'])
                        ->textInput(['autofocus' => false, 'class' => 'form-control birthdate', 'placeholder' => "DD-MM-YYYY",'readonly'=>'true'])
                        ->label($model->getAttributeLabel('birthdate') . '*')
                ?>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-6">
                <?=
                        $form->field($userModel, 'timezone', ['template' => '<span>{label}</span>{input}{error}'])
                        ->dropDownList($timezone, ['class' => 'form-control myselect2', 'maxlength' => true, 'prompt' => 'Select Timezone'])
                        ->label($userModel->getAttributeLabel('timezone') . '*')
                ?>                
            </div>            
        </div>

        <div class="row ">
            <?php
            if ($model->country == 'other') {
                $display_state_dd = 'none';
                $display_state_text = 'block';
            } else {
                $display_state_dd = 'block';
                $display_state_text = 'none';
            }
            ?>
            <div class="col-sm-6 col-md-6 col-lg-6">
                <?=
                        $form->field($model, 'country', ['template' => '<span>{label}</span>{input}{error}'])
                        ->dropDownList(['AUS' => 'Australia', 'other' => 'Other'], ['class' => 'form-control', 'maxlength' => true,])
                        ->label($model->getAttributeLabel('country') . '*')
                ?>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-6 state_dd" style="display:<?= $display_state_dd; ?>">
                <?=
                        $form->field($model, 'state', ['template' => '<span>{label}</span>{input}{error}'])
                        ->dropDownList(['New South Wales' => '	New South Wales', 'Queensland' => 'Queensland', 'South Australia' => 'South Australia', 'Tasmania' => 'Tasmania', 'Victoria' => 'Victoria', 'Western Australia' => 'Western Australia', 'Australian Capital Territory' => 'Australian Capital Territory', 'Northern Territory' => 'Northern Territory'], ['class' => 'form-control', 'maxlength' => true, 'prompt' => "Select " . $model->getAttributeLabel('state')])
                        ->label($model->getAttributeLabel('state') . '*')
                ?>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-6 state_text" style="display:<?= $display_state_text; ?>">
                <?=
                        $form->field($model, 'state_text', ['template' => '<span>{label}</span>{input}{error}'])
                        ->textInput(['class' => 'form-control state_text', 'maxlength' => true,])
                        ->label($model->getAttributeLabel('state_text') . '*')
                ?>
            </div>
        </div>
        
        <div class="row ">
            <div class="col-sm-6 col-md-6 col-lg-6">
                <?=
                        $form->field($model, 'street', ['template' => '<span>{label}</span>{input}{error}'])
                        ->textInput(['class' => 'form-control', 'maxlength' => true,])
                        ->label($model->getAttributeLabel('street') . '*')
                ?>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-6">
                <?=
                        $form->field($model, 'city', ['template' => '<span>{label}</span>{input}{error}'])
                        ->textInput(['class' => 'form-control', 'maxlength' => true,])
                        ->label($model->getAttributeLabel('city') . '*')
                ?>
            </div>
        </div>

        <div class="row ">
            <div class="col-sm-6 col-md-6 col-lg-6">
                <?php
//                        $form->field($model, 'postal_code', ['template' => '<span>{label}</span>{input}{error}'])
//                        ->widget(\yii\widgets\MaskedInput::className(['plceholder' => '']), ['mask' => '9999', 'clientOptions' => ['removeMaskOnSubmit' => true], 'options' => ['placeholder' => 'xxxx', 'class' => 'form-control']])
//                        ->label($model->getAttributeLabel('postal_code') . '*')
                ?>
                <?=
                        $form->field($model, 'postal_code', ['template' => '<span>{label}</span>{input}{error}'])
                        ->textInput(['class' => 'form-control', 'maxlength' => true,])
                        ->label($model->getAttributeLabel('postal_code') . '*')
                ?>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-6">
                <?=
                        $form->field($model, 'phone_number', ['template' => '<span>{label}</span>{input}{error}'])
                        ->widget(\yii\widgets\MaskedInput::className(['plceholder' => '']), ['mask' => '9999 999 999', 'clientOptions' => ['removeMaskOnSubmit' => true], 'options' => ['placeholder' => 'XXXX XXX XXX', 'class' => 'form-control']])
                        ->label($model->getAttributeLabel('phone_number') . '*')
                ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<div class="modal-footer">
    <button type="button" id="form-profile-update-btn" class="btn btn-primary">Save</button>
</div>
