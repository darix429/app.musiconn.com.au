<?php

use yii\helpers\Html;

?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['tutor_name']; ?>,</p>
            
            <p><?= $content['student_name']; ?> <?= $content['student_last_name']; ?> has submitted a request to change a tutorial booking (booking change reference #<?= $content['reschedule_number']; ?>) and is awaiting your approval. Please follow <a href="<?= $content['link']; ?>">this link</a> to review and approve the booking change request:</p>
            
            <p>The details of the booking change request are as follows:</p>
            
            <p><strong>From:</strong></p>
            
            <p><strong>Tutor Name :</strong> <?= $content['tutor_name']; ?></p>
            <p><strong>Instrument :</strong> <?= $content['instrument_name']; ?></p>
            <p><strong>Previous Tutorial Date :</strong> <?= $content['from_date']; ?></p>
            <p><strong>Previous Tutorial Time :</strong> <?= $content['from_time']; ?></p>
            
            <p><strong>To:</strong></p>
            
            <p><strong>Tutor Name :</strong> <?= $content['tutor_name']; ?></p>
            <p><strong>Instrument :</strong> <?= $content['instrument_name']; ?></p>
            <p><strong>Requested Tutorial Date :</strong> <?= $content['to_date']; ?></p>
            <p><strong>Requested Tutorial Time :</strong> <?= $content['to_time']; ?></p>
            
            <p>This is an automated message from the Musiconn Pty Ltd Booking System.  Please do not reply to this unattended mailbox.  Any queries can be directed to admin@musiconn.com.au.</p>
            
            
        </td>
    </tr>
</table>

