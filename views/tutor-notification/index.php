<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\helpers\ArrayHelper;
?>
<style>
    .field-itemmessage{
        min-width: 80px;
    }
    .div-field-from{
        display: block;
    }
    .notification-header {
        padding-top: 2px;
        padding-left: 10px;
        margin-bottom: 5px;
        background-color: rgba(0,0,0,.03);
        border-bottom: 1px solid rgba(0,0,0,.125);
    }
    .form-group {
        margin-bottom: 5px;
    }
    .form-control-drop {
        display: block;
        width: 80%;
        color: #495057;
        background-color: #fff;
        background-clip: padding-box;
        transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    }
    .has-success .form-control-drop {
        border-color: rgba(102, 189, 120, 1.0);
    }
    .has-error .form-control-drop {
        border-color: rgba(240, 80, 80, 1.0);
        box-shadow: none;
    }
    .form-control-sml {
        padding: .25rem .5rem;
        font-size: .875rem;
        line-height: 1.5;
    }
</style>

<?php $this->registerJs('

    no_record_label();

    function no_record_label(){
        if($(".item").length == 0){
            $(".norecord").show();
        }else{
            $(".norecord").hide();
        }
    }

    $(".dynamicform_wrapper").on("beforeDelete", function(e, item) {
        if (! confirm("Are you sure you want to delete this item?")) {
            return false;
        }
        return true;
    });

    $(".dynamicform_wrapper").on("afterDelete", function(e, item) {
        no_record_label();
    });

    $(".dynamicform_wrapper").on("afterInsert", function(e, item) {
        no_record_label();
        $(".tutor_uuid").val("' . $model->uuid . '");
    });

'); ?>

<div class="graph-2 general">
    <div class="tutor-notification-index">
        <p class="norecord pull-left" style="<?php echo (empty($modelsNotifications)) ? "display:block" : "display:none" ?>">No Record found.</p>
        <div class="but_list float-right">
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
            
        <div class="notification-form">
            <?php
            $form = ActiveForm::begin([
                'id' => 'dynamic-form',
                'validationUrl' => ['validation'],
                'enableClientValidation' => true,
                'enableAjaxValidation' => true, 
                'validateOnChange' => true,]);
            ?>

            <?php echo $form->field($model, 'email')->hiddenInput(['maxlength' => true, 'readonly' => true])->label(false) ?>
            


            <div class="panel panel-primary vn-panel">
                <div class="panel-body multiple-form-clon-parent">
                    <?php
                    DynamicFormWidget::begin([
                        'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                        'widgetBody' => '.container-items', // required: css class selector
                        'widgetItem' => '.item', // required: css class
                        'limit' => 999, // the maximum times, an element can be cloned (default 999)
                        'min' => 0, // 0 or 1 (default 1)
                        'insertButton' => '.add-item', // css class
                        'deleteButton' => '.remove-item', // css class
                        'model' => $modelsNotifications[0],
                        'formId' => 'dynamic-form',
                        'formFields' => [
                            'tutor_uuid',
                            'type',
                            'unit_value',
                            'time_unit',
                        ],
                    ]);
                    ?>

                    <div class="container-items multiple-form-clon-item-container">
                        <?php foreach ($modelsNotifications as $i => $modelAddress) { ?>
                        <div class="item panel panel-default">
                            <div class="notification-header">
                                <div class="pull-right" style="float: right !important; margin:5px;">
                                    <span class="remove-item"><i class="fa fa-close"></i></span>
                                </div>
                                <?= $form->field($modelAddress, "[{$i}]tutor_uuid")
                                    ->hiddenInput(['value' => $model->uuid, 'class' => 'form-control-drop form-control-sml tutor_uuid'])->label(false) ?>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?=
                                            $form->field($modelAddress, "[{$i}]type")
                                            ->dropDownList(['EMAIL' => 'EMAIL','SMS' => 'SMS'], ['class' => 'form-control-drop form-control-sml'])
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?=
                                            $form->field($modelAddress, "[{$i}]unit_value")->textInput(['type' => 'number', 'min' => 0,'class' => 'form-control-drop form-control-sml']);
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?=
                                            $form->field($modelAddress, "[{$i}]time_unit")
                                            ->dropDownList(['MINUTES' => 'MINUTES','HOURS' => 'HOURS','DAYS' => 'DAYS','WEEKS' => 'WEEKS'], ['prompt'=>'Select','class' => 'form-control-drop form-control-sml'])
                                            ?>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <?php
                            }
                        ?>

                    </div>
                    <?= Html::Button('Add', ['class' => 'add-item btn btn-success']) ?>
                    <?= Html::Button('Save', ['class' => 'btn btn-primary', 'id' => 'TutorAddNotification']) ?>
                   <?php DynamicFormWidget::end(); ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
        

    </div>
</div>
