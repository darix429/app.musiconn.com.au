<?php

use yii\helpers\Html;
use app\models\BookedSession;
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['student_first_name'] . ' ' . $content['student_last_name']; ?>,</p>

            <p>This is an automated reminder that a lesson with <?= $content['tutor_name']; ?> will start on <?= $content['start_datetime'] ?> on Musiconn's platform.</p>
            
            <p><strong>Session Link:</strong> <a style="background:#ff6f3d;min-height:46px;margin:10px 0;padding:0 40px;line-height:46px;border-radius:4px;font-size:18px;color:#fff;display:inline-block;" href="<?= BookedSession::getStartSessionLink($content['uuid'], 'student'); ?>">Click Here</a></p>
            <!--<p><strong>Your Session PIN is :</strong> <?php //echo $content['session_pin']; ?></p>-->
            <p>Do not reply to this unattended mailbox.  Any queries can be directed to admin@musiconn.com.au, or 1300 068 742.</p>

            <p>Regards,<br>
               System Administrators</p>
        </td>
    </tr>
</table>
