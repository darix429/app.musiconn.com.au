<?php

use yii\helpers\Html;
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['student_name']; ?>,</p>

            <p>Your request for the cancellation lesson has been rejected by tutor.</p>

            <p> If any query for your account then please contact to administrator.</p>

            <p>Sincerely,</p>
            <p>System Administrators</p>
        </td>
    </tr>
</table>
