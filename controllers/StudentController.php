<?php

namespace app\controllers;

use Yii;
use app\models\Student;
use app\models\StudentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\User;
use app\libraries\General;
use yii\helpers\Url;
use yii\filters\AccessControl;
use app\models\ChangePassword;
use app\models\StudentCreditCard;
use app\models\Tutor;
use app\models\BookedSession;
use app\models\CreditSession;

/**
 * StudentController implements the CRUD actions for Student model.
 */
class StudentController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                        [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['profile'],
                        'allow' => Yii::$app->user->can('/student/profile'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['change-password'],
                        'allow' => Yii::$app->user->can('/student/change-password'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['account-enabled'],
                        'allow' => Yii::$app->user->can('/student/account-enabled'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['account-disabled'],
                        'allow' => Yii::$app->user->can('/student/account-disabled'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['send-activation-link'],
                        'allow' => Yii::$app->user->can('/student/send-activation-link'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['credit-card'],
                        'allow' => Yii::$app->user->can('/student/credit-card'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['monthly-subscription-cancel'],
                        'allow' => Yii::$app->user->can('/student/monthly-subscription-cancel'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['remove-account'],
                        'allow' => Yii::$app->user->can('/student/remove-account'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['registration-report'],
                        'allow' => Yii::$app->user->can('/student/registration-report'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['credit-lesson-admin'],
                        'allow' => Yii::$app->user->can('/student/credit-lesson-admin'),
                        'roles' => ['@'],
                    ],
					          [
                        'actions' => ['delete'],
                        'allow' => Yii::$app->user->can('/student/delete'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['profile-update'],
                        'allow' => Yii::$app->user->can('/student/profile-update'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['profile_old'],
                        'allow' => true,
                        'roles' => ['STUDENT'],
                    ],
		    [
                        'actions' => ['apps'],
                        'allow' => Yii::$app->user->can('/student/apps'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['tutor_available'],
                        'allow' => true,
                        'roles' => ['STUDENT'],
                    ],
                    [
                        'actions' => ['get_tutor'],
                        'allow' => true,
                        'roles' => ['STUDENT'],
                    ],
                    [
                        'actions' => ['tutor_availability_calender_events'],
                        'allow' => true,
                        'roles' => ['STUDENT'],
                    ],
                    [
                        'actions' => ['home'],
                        'allow' => true,
                        'roles' => ['STUDENT'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                 //   'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Student models.
     * @return mixed
     */
    public function actionIndex() {
        // When admin switch to the student account and click on browser back button
        if(Yii::$app->user->identity->role == "STUDENT") {
            return $this->redirect('/index.php/booked-session/upcoming');
        }
        $searchModel = new StudentSearch();
        $searchModel->status = "ENABLED";
        $studentList = $searchModel->search(Yii::$app->request->queryParams, true);

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_student_list', [
                        'studentList' => $studentList,
            ]);
        } else {
            return $this->render('index', [
                        //'inactiveAdmin' => $inactiveAdmin,
                        'studentList' => $studentList,
                        'searchModel' => $searchModel,
            ]);
        }
    }

    /**
     * Displays a single Student model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Student model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Student();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->student_uuid]);
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Student model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->student_uuid]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Student model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete() {

        $id = Yii::$app->getRequest()->post('id');

        if ((($model = Student::findOne($id)) !== null) && (($modelUser = User::find()->where(['student_uuid' => $id])->one()) !== null)) {

            $model->delete();

            //Remove current role:
            $manager = Yii::$app->authManager;
            $item = $manager->getRole($modelUser->role);
            $item = $item ?: $manager->getPermission($modelUser->role);
            $manager->revoke($item, $modelUser->id);
            
            $modelUser->delete();

            $return = ['code' => 200, 'message' => 'Student account deleted successfully.'];
        } else {

            $return = ['code' => 404, 'message' => 'The requested page does not exist.'];
        }

        Yii::$app->response->format = 'json';
        return $return;
    }

    /**
     * Finds the Student model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Student the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Student::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionSendActivationLink($id) {

        if (($student = Student::findOne($id)) !== null) {

            if ($student->status == "INACTIVE") {

                $uuid_enc = General::encrypt_str($student->student_uuid);
                $token = Student::generateActivationToken();

                $student->activation_link_token = $token;
                $student->save(false);

                $content['name'] = $student->first_name;
                $content['link'] = Url::to(['site/student-account-activation', 't' => $uuid_enc, 'token' => $token], true);

                $owner = General::getOwnersEmails(['OWNER', 'ADMIN']);

                Yii::$app->mailer->compose('account_enabled', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                        ->setFrom(Yii::$app->params['supportEmail'])
                        ->setTo($student->email)
                        ->setBcc($owner)
                        ->setSubject('Account Activation mail - ' . Yii::$app->name)
                        ->send();

                $return = ['code' => 200, 'message' => 'Activation link sent successfully.'];
            } else {
                $return = ['code' => 421, 'message' => 'Student account already enabled'];
            }
        } else {
            $return = ['code' => 404, 'message' => 'Record does not exist.'];
        }
        Yii::$app->response->format = 'json';
        return $return;
    }

    public function actionAccountDisabled($id) {

        if (($student = Student::findOne($id)) !== null) {

            if ($student->status == "ENABLED") {

                $user = \app\models\User::find()->where(['student_uuid' => $student->student_uuid])->one();
                if (!empty($user)) {

                    $user->status = 0;
                    $user->save(false);

                    $student->status = 'DISABLED';
                    $student->save(false);

                    $return = ['code' => 200, 'message' => 'Student account disabled successfully.'];
                } else {
                    $return = ['code' => 421, 'message' => 'User record does not exist'];
                }
            } else {
                $return = ['code' => 421, 'message' => 'Student account not enabled.'];
            }
        } else {
            $return = ['code' => 404, 'message' => 'Record does not exist.'];
        }

        Yii::$app->response->format = 'json';
        return $return;
    }

    public function actionAccountEnabled($id) {

        if (($student = Student::findOne($id)) !== null) {

            if ($student->status == "DISABLED") {

                $user = \app\models\User::find()->where(['student_uuid' => $student->student_uuid])->one();
                if (!empty($user)) {

                    $user->status = 10;
                    $user->save(false);

                    $student->status = 'ENABLED';
                    $student->save(false);

                    $return = ['code' => 200, 'message' => 'Student account enabled successfully.'];
                } else {
                    $return = ['code' => 421, 'message' => 'User record does not exist'];
                }
            } else {
                $return = ['code' => 421, 'message' => 'Student account not disabled.'];
            }
        } else {
            $return = ['code' => 404, 'message' => 'Record does not exist.'];
        }

        Yii::$app->response->format = 'json';
        return $return;
    }

    public function actionProfile_old() {

        //echo Yii::$app->user->identity->student_uuid;exit;
        $model = $this->findModel(Yii::$app->user->identity->student_uuid);
        $model->state_text = $model->state;
        $modelCP = new ChangePassword();
        $modelCC = new StudentCreditCard();
        $userModel = \app\models\User::findOne(Yii::$app->user->identity->id);
        $userModel->scenario = 'student_profile';

        $model->attributes = Yii::$app->getRequest()->post('Student');
        $userModel->attributes = Yii::$app->getRequest()->post('User');


        $model->email = $userModel->username = $userModel->email;
        $model->updated_at = $userModel->updated_at = time();
        //$model->birthdate = date('Y-m-d', strtotime($model->birthdate));
//        echo "<pre>";print_r($model->attributes);exit;

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $userModel->load(Yii::$app->request->post()) && $userModel->validate()) {
            $userModel->first_name = $model->first_name;
            $userModel->last_name = $model->last_name;
            $userModel->phone = $model->phone_number;

            $model->state = ($model->country == 'other') ? $model->state_text : $model->state;
            $model->birthdate = date('Y-m-d', strtotime($model->attributes['birthdate']));

            $model->save(false);
            $userModel->save(false);

            Yii::$app->getSession()->setFlash('success', 'You have successfully updated your profile.');

            return $this->redirect(['profile']);
        }

        return $this->render('old/profile', [
                    'model' => $model,
                    'userModel' => $userModel,
                    'modelCP' => $modelCP,
                    'modelCC' => $modelCC,
        ]);
    }
    
    public function actionProfile() {

        $this->layout = "student_layout";
        
        $model = $this->findModel(Yii::$app->user->identity->student_uuid);
        $model->state_text = $model->state;
        $modelCP = new ChangePassword();
        $modelCC = StudentCreditCard::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid])->one();
        if(!empty($modelCC)) {
            $modelCC = $modelCC ;        
        }else{ 
            $modelCC = new StudentCreditCard();
            $modelCC->scenario = 'step_2';
        }
        $userModel = \app\models\User::findOne(Yii::$app->user->identity->id);
        $userModel->scenario = 'student_profile';

        $model->attributes = Yii::$app->getRequest()->post('Student');
        $userModel->attributes = Yii::$app->getRequest()->post('User');


        $model->email = $userModel->username = $userModel->email;
        $model->updated_at = $userModel->updated_at = time();
        //$model->birthdate = date('Y-m-d', strtotime($model->birthdate));
//        echo "<pre>";print_r($model->attributes);exit;

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $userModel->load(Yii::$app->request->post()) && $userModel->validate()) {
            $userModel->first_name = $model->first_name;
            $userModel->last_name = $model->last_name;
            $userModel->phone = $model->phone_number;

            $model->state = ($model->country == 'other') ? $model->state_text : $model->state;
            $model->birthdate = date('Y-m-d', strtotime($model->attributes['birthdate']));

            $model->save(false);
            $userModel->save(false);

            Yii::$app->getSession()->setFlash('success', 'Your profile is updated successfully.');

            return $this->redirect(['profile']);
        }

        return $this->render('profile', [
                    'model' => $model,
                    'userModel' => $userModel,
                    'modelCP' => $modelCP,
                    'modelCC' => $modelCC,
        ]);
    }
    
    public function actionProfileUpdate() {
        if (!Yii::$app->request->isAjax) {
            Yii::$app->response->format = 'json';
            return ['code' => 505, 'message' => 'You are not authorised to access this page'];
        }
        
        $model = $this->findModel(Yii::$app->user->identity->student_uuid);
        $model->state_text = $model->state;
        
        $userModel = \app\models\User::findOne(Yii::$app->user->identity->id);
        $userModel->scenario = 'student_profile';

        $model->attributes = Yii::$app->getRequest()->post('Student');
        $userModel->attributes = Yii::$app->getRequest()->post('User');


        $model->email = $userModel->username = $userModel->email;
        $model->updated_at = $userModel->updated_at = time();
        
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $userModel->load(Yii::$app->request->post()) && $userModel->validate()) {
            $userModel->first_name = $model->first_name;
            $userModel->last_name = $model->last_name;
            $userModel->phone = $model->phone_number;

            $model->state = ($model->country == 'other') ? $model->state_text : $model->state;
            $model->birthdate = date('Y-m-d', strtotime($model->attributes['birthdate']));

            $model->save(false);
            $userModel->save(false);

            Yii::$app->getSession()->setFlash('success', 'Your profile is updated successfully.');

            Yii::$app->response->format = 'json';
            return ['code' => 200, 'message' => 'Your profile updated successfully.'];
        }

        return $this->renderAjax('_profile_detail', [
                    'model' => $model,
                    'userModel' => $userModel
        ]);
    }

    public function actionChangePassword() {
        if (!Yii::$app->request->isAjax) {
            Yii::$app->response->format = 'json';
            return ['code' => 505, 'message' => 'You are not authorised to access this page'];
        }

        $modelCP = new ChangePassword();
        if ($modelCP->load(Yii::$app->getRequest()->post()) && $modelCP->validate()) {

            $modelCP->change();
            Yii::$app->response->format = 'json';
            return ['code' => 200, 'message' => 'Your password changed successfully.'];
        }

        return $this->renderAjax('change-password', [
                    'modelCP' => $modelCP,
        ]);
    }

    public function actionCreditCard() {

        if (!Yii::$app->request->isAjax) {
            Yii::$app->response->format = 'json';
            return ['code' => 505, 'message' => 'You are not authorised to access this page'];
        }

        $creditCard = StudentCreditCard::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid])->one();

        if (empty($creditCard)) {

            $modelCC = new StudentCreditCard();
            $modelCC->scenario = 'step_2';

            
            if ($modelCC->load(Yii::$app->getRequest()->post()) && $modelCC->validate()) {

                if(isset($_POST['stripeToken'])  && !empty($_POST['stripeToken']) ){

                    //include Stripe PHP library
                    require Yii::$app->basePath . '/vendor/stripe/stripe-php/init.php';
                    //set api key
                    $stripe = array(
                        "secret_key" => Yii::$app->params['stripe']['secret_key'],
                        "publishable_key" => Yii::$app->params['stripe']['publishable_key']
                    );
                    try {
                        \Stripe\Stripe::setApiKey($stripe['secret_key']);
                        $customer = \Stripe\Customer::create(array(
                                    'email' => Yii::$app->user->identity->email,
                                    'source' => $_POST['stripeToken']
                        ));
                        
                        
                        
                         $customerJson = $customer->jsonSerialize();

                    if(isset($customerJson['id']) && !empty($customerJson['id'])){

                        $modelCC->student_uuid = Yii::$app->user->identity->student_uuid;
                        $modelCC->number = General::encrypt_str($modelCC->number);
                        $modelCC->cvv = General::encrypt_str($modelCC->cvv);
                        $modelCC->status = 'ENABLED';
                        $modelCC->expire_date = date('Y-m-t', strtotime(Yii::$app->request->post('StudentCreditCard')['cc_month_year']));
                        $modelCC->created_at = $modelCC->updated_at = date('Y-m-d H:i:s', time());
                        $modelCC->stripe_customer_id = $customerJson['id'];
                        if ($modelCC->save(false)) {
                            Yii::$app->response->format = 'json';
                            return ['code' => 200, 'message' => 'Your Credit Card is updated successfully.'];
                        }
                    } else {
                        
                        
                         Yii::$app->response->format = 'json';
                            return ['code' => 404, 'message' => 'This Card is declined from the Stripe.'];
                    }
                        
                    } catch (\Stripe\Error\ApiConnection $e) {
                        Yii::$app->response->format = 'json';
                            return ['code' => 404, 'message' => 'Network problem, perhaps try again.'];
                            
                    } catch (\Stripe\Error\InvalidRequest $e) {
                        Yii::$app->response->format = 'json';
                            return ['code' => 404, 'message' => 'Your request is invalid.'];
                            
                    } catch (\Stripe\Error\Api $e) {
                        Yii::$app->response->format = 'json';
                            return ['code' => 404, 'message' => 'Stripe servers are down!'];
                            
                    } catch (\Stripe\Error\Card $e) {
                         Yii::$app->response->format = 'json';
                            return ['code' => 404, 'message' => 'This Card is Declined.'];
                    }

                   
                }

            }
            return $this->renderAjax('add_credit_card', [
                        'modelCC' => $modelCC,
            ]);
        } else {
            $student_uuid = Yii::$app->user->identity->student_uuid;
            $modelCC = StudentCreditCard::find()->where(['student_uuid' => $student_uuid])->one();

            return $this->renderAjax('update_credit_card', [
                        'modelCC' => $modelCC,
                        'creditCard' => $creditCard
            ]);
        }
    }

    public function actionMonthlySubscriptionCancel($id) {
        if (!Yii::$app->request->isAjax) {
            Yii::$app->response->format = 'json';
            return ['code' => 505, 'message' => 'You are not authorised to access this page'];
        }
        //$student = \app\models\StudentSubscription::find()->joinWith(['studentUu'])->where(['student_subscription.student_uuid' => $id, 'student.isCancelMonthlySubscriptionRequest' => 1])->one();

        $student = \app\models\StudentSubscription::find()->where(['student_uuid' => $id])->one();

        if ($student !== null) {
            $studentSubscription = \app\models\StudentMonthlySubscription::find()->where(['uuid' => $student->subscription_uuid])->one();
            if (!empty($studentSubscription)) {
                $studentSubscription->is_cancel = 1;
                $studentSubscription->cancel_date = date('Y-m-d H:i:s', time());
                $studentSubscription->status = 'CANCELLED';

                $studentModel = $this->findModel($id);
                //print_r($studentModel);
                $studentModel->isMonthlySubscription = 0;
                $studentModel->isCancelMonthlySubscriptionRequest = 0;
                $studentModel->save(false);
                if ($studentSubscription->save(false)) {
                    //self::MonthlySubscriptionCancelApproveMail($studentModel);
                    //print_r($studentSubscription);
                    Yii::$app->getSession()->setFlash('success', 'Student monthly subscription plan cancel successfully.');
                    $return = ['code' => 200, 'message' => 'Student monthly subscription plan cancel successfully.'];
                } else {
                    $return = ['code' => 404, 'message' => 'Something went wrong.'];
                }
            } else {
                $return = ['code' => 421, 'message' => 'Student Subscription not found.'];
            }
        } else {
            $return = ['code' => 404, 'message' => 'Student monthly subscription does not exist.'];
        }

        Yii::$app->response->format = 'json';
        return $return;
    }

    public function MonthlySubscriptionCancelApproveMail($student) {
        $content['student_name'] = $student->first_name . ' ' . $student->last_name;
        $owner = General::getOwnersEmails(['OWNER', 'ADMIN']);
        return Yii::$app->mailer->compose('monthly_subscription_cancel_approve_admin', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                        ->setFrom(Yii::$app->params['supportEmail'])
                        ->setTo($student->email)
                        ->setBcc($owner)
                        ->setSubject('Cancel Monthly Subscription - ' . Yii::$app->name)
                        ->send();
    }

    public function actionRemoveAccount($id) {
        if (!Yii::$app->request->isAjax) {
            Yii::$app->response->format = 'json';
            return ['code' => 505, 'message' => 'You are not authorised to access this page'];
        }

        //Check for available booked session
        $tutionAvailable = Student::CheckBookedTution($id);
        if (isset($tutionAvailable) && !empty($tutionAvailable)) {
            // print_r($tutionAvailable);
            foreach ($tutionAvailable as $key => $value) {
                //Cancel Booked Tutions
                $tutionCancel = Student::CancelBookedTution($value->uuid);
                if (!$tutionCancel) {
                    Yii::$app->response->format = 'json';
                    return ['code' => 505, 'message' => 'Delete account process interupted due to techinal issue. Please try after some time.'];
                }
            }
        }

        $modelUser = \app\models\User::findOne(Yii::$app->user->identity->id);
        if (!empty($modelUser)) {
            $student = Student::findOne($modelUser->student_uuid);
            $student->status = 'DELETED';
            $student->updated_at = time();
            $student->save(false);

            Yii::$app->authManager->revoke(Yii::$app->authManager->getRole('STUDENT'), $modelUser->getPrimaryKey());
            if ($modelUser->delete()) {
                self::DeleteAccountMailAdmin($student);
                Yii::$app->response->format = 'json';
                return ['code' => 200, 'message' => 'Your account deleted successfully.'];
            } else {
                Yii::$app->response->format = 'json';
                return ['code' => 505, 'message' => 'Delete account process interupted due to techinal issue. Please try after some time or contact your administrator.'];
            }
        }
    }

    public function DeleteAccountMailAdmin($student) {
        $content['student_name'] = $student->first_name . ' ' . $student->last_name;
        $content['student_email'] = $student->email;
        $content['student_id'] = $student->enrollment_id;
        $owner = General::getOwnersEmails(['OWNER']);
        $admin = General::getOwnersEmails(['ADMIN', 'SUBADMIN']);

        return Yii::$app->mailer->compose('student_delete_notify_admin', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                        ->setFrom(Yii::$app->params['supportEmail'])
                        ->setTo($admin)
                        ->setBcc($owner)
                        ->setSubject('Student Account Termination of ' . $content['student_name'] . ' - ' . Yii::$app->name)
                        ->send();
    }

    public function actionRegistrationReport()
    {
        $searchModel = new StudentSearch();
        $searchModel->created_at = date('01-m-Y').' - '.date('t-m-Y');

        $studentList = $searchModel->searchRegistrationReport(Yii::$app->request->queryParams, true);
        //echo "<pre>";print_r($tuitionBookingList);exit;
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_registration_report_list', [
                        'studentList' => $studentList,
            ]);
        } else {
            return $this->render('registration_report', [
                        'studentList' => $studentList,
                        'searchModel' => $searchModel,
            ]);
        }
    }

    public function actionCreditLessonAdmin() {
        $model = new \app\models\CreditSession();

        $model->attributes = Yii::$app->getRequest()->Post('CreditSession');

        if ($model->load(Yii::$app->request->post())) {

            $model->save(false);
            Yii::$app->getSession()->setFlash('success', 'You have successfully added credit lesson for admin.');
            return $this->redirect(['index']);
        }
        return $this->render('admin_credit_lesson_form', [
            'model' => $model,
        ]);
    }
    
    public function actionApps() {
 	$this->layout = "student_layout";
        return $this->render('apps_view');
    }

    public function actionTutor_available() {
        if (Yii::$app->request->isAjax) {
            $this->layout = "student_layout";
            $tutor_uuid = Yii::$app->getRequest()->get('tutor_uuid');
            if(empty($tutor_uuid) || $tutor_uuid == "") {
                Yii::$app->response->format = 'json';
                return ['code' => 500, 'message' => 'Please select appropriate instrument and Tutor.'];
            }
            $modelTutor = \app\models\Tutor::findOne($tutor_uuid);
            if(!empty($tutor_uuid) && !empty($modelTutor)){

                return $this->renderAjax('_tutor_availability_calender', [
                            'tutor_uuid' => $tutor_uuid,
                            'modelTutor' => $modelTutor,
                ]);
            } else {
                Yii::$app->response->format = 'json';
                return ['code' => 404, 'message' => 'Tutor not available'];
            }
        }else{
            $this->layout = "student_layout";
            return $this->render('tutor_availablility', [
            ]);
        }

    }

    public function actionGet_tutor() {
        if (Yii::$app->request->isAjax) {
            $this->layout = "student_layout";
            $instrument_uuid = Yii::$app->getRequest()->get('id');
            if(empty($instrument_uuid) || $instrument_uuid == "") {
                $html = '<select class="form-control myselect2" id="tutor_select">';
                $html .= '<option value="" selected>Select Tutor</option>';
                $html .= '</select>';

                return $html;
            }

            $modelTutor = (new \yii\db\Query())
                    ->select(['first_name', 'last_name','tutor.uuid'])
                    ->from('tutor')
                    ->join( 'LEFT OUTER JOIN','tutor_instrument','tutor_instrument.tutor_uuid =tutor.uuid AND tutor.status = \'ENABLED\'')
                    ->where(['tutor_instrument.instrument_uuid' => $instrument_uuid])
                    ->all(); 

            if(!empty($instrument_uuid) && !empty($modelTutor)){

                $html = '<select class="form-control myselect2" id="tutor_select">';
                $html .= '<option value="" selected>Select Tutor</option>';
                            
                    if(count($modelTutor)>0){
                        foreach ($modelTutor as $key => $value) {
                            $html .= '<option value="'.$value['uuid'].'" >'.$value['first_name'].' '.$value['last_name'].'</option>';
                        }
                    }
                $html .= '</select>';

                return $html;

            } else {
                $html = '<select class="form-control myselect2" id="tutor_select">';
                $html .= '<option value="" selected>Select Tutor</option>';
                $html .= '</select>';

                return $html;
            }
        }else{
            Yii::$app->response->format = 'json';
            return ['code' => 404, 'message' => 'Tutor not available.'];
        }

    }

    public function actionTutor_availability_calender_events() {
        if (!Yii::$app->request->isAjax) {
            throw new \yii\web\HttpException(403, 'The action you have requested is not allowed.');
        }
        $events = [];

        $tutor_uuid = Yii::$app->getRequest()->get('tutor_uuid');
        $user_timezone = Yii::$app->user->identity->timezone;
        $modelTutor = Tutor::findOne($tutor_uuid);

        $tutor = User::find()->where(['tutor_uuid' => $tutor_uuid])->one();
        $tutor_timezone = $tutor['timezone'];

        if(!empty($tutor_uuid) && !empty($modelTutor)){

            $start_date = date('Y-m-d', Yii::$app->request->get('start'));
            $end_date = date('Y-m-d', Yii::$app->request->get('end') );

            $event_1 = Student::prepareTutorUnavailabilityEvents($tutor_uuid, $start_date, $end_date);
            $event_2 = Student::prepareTutorWeekDayNoneWorkingEvents_booking($tutor_uuid, $start_date, $end_date, $tutor_timezone, $user_timezone);
            $all_1 = array_merge($event_1, $event_2);

            $event_3 = Student::prepareTutorScheduleAppoinmentEvents($tutor_uuid, $start_date, $end_date);
            $event_4 = Student::prepareStudentScheduleAppoinmentEvents($tutor_uuid, $start_date, $end_date);
            $all_2 = array_merge($event_3, $event_4);

            $all_3 = array_merge($all_1, $all_2);
            $event_5 = Student::prepareTutorRescheduleAppoinmentEvents($tutor_uuid, $start_date, $end_date);
            $events = array_merge($all_3, $event_5);
        }

        Yii::$app->response->format = 'json';
        return $events;
    }
    
    /*     * *****
     * After new booking process this page is Home page     * 
     * ******* */
    public function actionHome() {

        $this->layout = "student_layout";
        
        $model = $this->findModel(Yii::$app->user->identity->student_uuid);
        
        $statusIn = ['SCHEDULE','INPROCESS'];
        $upcomingList = BookedSession::find()
                ->joinWith(['studentUu','tutorUu','studentBookingUu','creditSessionUu'])
                ->where(['booked_session.student_uuid' => Yii::$app->user->identity->student_uuid])
                ->andWhere(['in', 'booked_session.status', $statusIn])
                ->asArray()->orderBy('booked_session.start_datetime asc')->limit(3)->all();
        
        $completedList = BookedSession::find()
                ->joinWith(['studentUu','tutorUu','studentBookingUu','creditSessionUu'])
                ->where(['booked_session.student_uuid' => Yii::$app->user->identity->student_uuid])
                ->andWhere(['in', 'booked_session.status', ['COMPLETED','PUBLISHED']])
                ->asArray()->orderBy('booked_session.start_datetime desc')->limit(3)->all();
        
        $cancelledList = BookedSession::find()
                ->joinWith(['studentUu','tutorUu','studentBookingUu','creditSessionUu'])
                ->where(['booked_session.student_uuid' => Yii::$app->user->identity->student_uuid])
                ->andWhere(['in', 'booked_session.status', ['CANCELLED']])
                ->asArray()->orderBy('booked_session.start_datetime desc')->limit(3)->all();
        
        
        $group_by = ['credit_session.uuid','tutorial_type_code'];
        $creditedList = CreditSession::find()
                ->where(['student_uuid' => Yii::$app->user->identity->student_uuid])
                ->andWhere(['in', 'status', ['PENDING']])
                ->asArray()
                ->orderBy('')
                ->groupBy($group_by)
                ->limit(3)->all();

//        echo "<pre>";
//        print_r($creditedList);
//        exit;

        return $this->render('student_home', [                    
                    'model' => $model,
                    'upcomingList' => $upcomingList,
                    'completedList' => $completedList,
                    'cancelledList' => $cancelledList,
                    'creditedList' => $creditedList,
        ]);
    }
}
