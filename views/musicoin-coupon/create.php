<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MusicoinCoupon */

$this->title = Yii::t('app', 'Create Musicoin Promo Code');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Musicoin Promo Codes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?= $this->render('_form', [
        'model' => $model,
    ]) ?>
