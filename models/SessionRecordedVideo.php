<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%booked_session}}".
 *
 * @property string $uuid
 * @property string $booking_uuid
 * @property string $credit_session_uuid
 * @property string $student_uuid
 * @property string $tutor_uuid
 * @property string $start_datetime
 * @property string $end_datetime
 * @property string $filename
 * @property int $session_min
 * @property string $session_pin
 * @property string $status
 * @property string $cancel_by
 * @property string $cancel_user_uuid
 * @property string $cancel_reason
 * @property string $cancel_datetime
 * @property string $created_at
 * @property string $updated_at
 * @property int $student_login_flag
 * @property int $tutor_login_flag
 * @property string $publish_date
 *
 * @property Student $studentUu
 * @property Tutor $tutorUu
 */
class SessionRecordedVideo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%booked_session}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'student_uuid', 'tutor_uuid', 'start_datetime', 'end_datetime', 'session_min'], 'required'],
            [['uuid', 'booking_uuid', 'credit_session_uuid', 'student_uuid', 'tutor_uuid', 'filename', 'status', 'cancel_user_uuid', 'cancel_reason'], 'string'],
            [['start_datetime', 'end_datetime', 'cancel_datetime', 'created_at', 'updated_at', 'publish_date'], 'safe'],
            [['session_min', 'student_login_flag', 'tutor_login_flag'], 'default', 'value' => null],
            [['session_min', 'student_login_flag', 'tutor_login_flag'], 'integer'],
            [['session_pin'], 'string', 'max' => 4],
            [['cancel_by'], 'string', 'max' => 10],
            [['uuid'], 'unique'],
            [['student_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['student_uuid' => 'student_uuid']],
            [['tutor_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Tutor::className(), 'targetAttribute' => ['tutor_uuid' => 'uuid']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'booking_uuid' => Yii::t('app', 'Booking Uuid'),
            'credit_session_uuid' => Yii::t('app', 'Credit Session Uuid'),
            'student_uuid' => Yii::t('app', 'Student Uuid'),
            'tutor_uuid' => Yii::t('app', 'Tutor Uuid'),
            'start_datetime' => Yii::t('app', 'Start Datetime'),
            'end_datetime' => Yii::t('app', 'End Datetime'),
            'filename' => Yii::t('app', 'Filename'),
            'session_min' => Yii::t('app', 'Session Min'),
            'session_pin' => Yii::t('app', 'Session Pin'),
            'status' => Yii::t('app', 'Status'),
            'cancel_by' => Yii::t('app', 'Cancel By'),
            'cancel_user_uuid' => Yii::t('app', 'Cancel User Uuid'),
            'cancel_reason' => Yii::t('app', 'Cancel Reason'),
            'cancel_datetime' => Yii::t('app', 'Cancel Datetime'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'student_login_flag' => Yii::t('app', 'Student Login Flag'),
            'tutor_login_flag' => Yii::t('app', 'Tutor Login Flag'),
            'publish_date' => Yii::t('app', 'Publish Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentUu()
    {
        return $this->hasOne(Student::className(), ['student_uuid' => 'student_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTutorUu()
    {
        return $this->hasOne(Tutor::className(), ['uuid' => 'tutor_uuid']);
    }
}
