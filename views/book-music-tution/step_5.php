<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Url;
use app\models\Instrument;
use app\models\OnlineTutorialPackage;
use app\models\StudentTuitionBooking;
use app\models\Tutor;
use app\models\StudentCreditCard;
use app\models\MonthlySubscriptionFee;
use app\libraries\General;
use app\models\Student;
use app\models\StandardPlanOptionRate;
use app\models\OnlineTutorialPackageType;
use app\models\OnlineTutorialPackageTutorwise;
use app\models\StudentTuitionStandardBookingLockingPeriod;

$this->title = Yii::t('app', 'BOOK MUSIC LESSONS');
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_icon'] = 'my-lesson-tag.png';

if (!empty($step_data['step_1']['instrument_uuid'])) {
    $instrumentDetail = Instrument::find()->where(['uuid' => $step_data['step_1']['instrument_uuid']])->one();
} else {
    return \yii\web\Controller::redirect(['index']);
}

$student_detail = Student::findOne(Yii::$app->user->identity->student_uuid);
$tutorDetail = Tutor::findOne($step_data['step_2']['tutor_uuid']);

$package_type_model = OnlineTutorialPackageType::find()->where(['uuid' => $tutorDetail->online_tutorial_package_type_uuid])->one();
$package_type = $package_type_model->name;

//$planDetail = OnlineTutorialPackage::find()->where(['uuid' => $step_data['step_3']['onlinetutorial_uuid']])->one();
$query = new \yii\db\Query;
$planDetail = $query->select(['o.uuid as package_uuid', 'p.uuid', 'p.name', 'p.price', 'p.gst', 'p.total_price', 'p.status', 'p.default_option', 'p.online_tutorial_package_type_uuid']
        )
        ->from('online_tutorial_package as o')
        ->join('RIGHT JOIN', 'premium_standard_plan_rate as p', 'o.uuid = p.online_tutorial_package_uuid')
        ->where(['p.uuid' => $step_data['step_3']['onlinetutorial_uuid']])
        //->asArray()
        ->one();

$planOptionDetail = StandardPlanOptionRate::find()->where(['option' => $step_data['step_3']['option'], 'premium_standard_plan_rate_uuid' => $step_data['step_3']['onlinetutorial_uuid']])->one();

if ($package_type == "PREMIUM") {
    $plantotal_price = $planDetail['total_price'];
    $planprice = $planDetail['price'];
    $plangst = $planDetail['gst'];
} else {
    $plantotal_price = $planOptionDetail->total_price;
    $planprice = $planOptionDetail->price;
    $plangst = $planOptionDetail->gst;
    $plan_option = $planOptionDetail->option;

//Get id of tutorwise paln 
    $tutorwise_model = OnlineTutorialPackageTutorwise::find()->where(['premium_standard_plan_rate_uuid' => $step_data['step_3']['onlinetutorial_uuid'], 'tutor_uuid' => $step_data['step_2']['tutor_uuid'], 'instrument_uuid' => $step_data['step_1']['instrument_uuid']])->one();
    $online_tutorial_package_tutorwise_uuid = $tutorwise_model->uuid;

    //Standard tutor loking period
    $locking_per_model_count = StudentTuitionStandardBookingLockingPeriod::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid, 'tutor_uuid' => $step_data['step_2']['tutor_uuid'], 'instrument_uuid' => $step_data['step_1']['instrument_uuid']])->count();

    if ($locking_per_model_count > 0) {

        $locking_per_model = StudentTuitionStandardBookingLockingPeriod::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid, 'tutor_uuid' => $step_data['step_2']['tutor_uuid'], 'instrument_uuid' => $step_data['step_1']['instrument_uuid']])->one();
        $locking_end_datetime = $locking_per_model->locking_end_datetime;
        $locking_start_datetime = $locking_per_model->locking_start_datetime;
        $locking_option = $locking_per_model->option;



        $current_date = date('Y-m-d H:i:s');

        if (($plan_option < $locking_option) && ($current_date <= $locking_end_datetime)) {

            $lockingplanDetail = StandardPlanOptionRate::find()->where(['premium_standard_plan_rate_uuid' => $step_data['step_3']['onlinetutorial_uuid'], 'option' => $locking_option])->one();
            $plantotal_price = $lockingplanDetail->total_price;

            $planprice = $lockingplanDetail->price;
            $plangst = $lockingplanDetail->gst;
        }
    }
}




$start_hour_array = StudentTuitionBooking::start_hour_array($step_data['step_3']['onlinetutorial_uuid']);
$saved_cc_detail = StudentCreditCard::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid, 'status' => 'ENABLED'])->one();
//$monthly_package = MonthlySubscriptionFee::find()->where(['with_tutorial' => 1, 'status' => 'ENABLED'])->one();//change by pooja for comman price for manthly subscription
$monthly_package = MonthlySubscriptionFee::find()->where(['status' => 'ENABLED'])->one();

//echo "<pre>";
//print_r($step_data);
//print_r($monthly_package);
//exit;

$this->registerJsFile('https://js.stripe.com/v2/');
$this->registerJs(
        '
$( "#gostep1,#gostep2,#gostep3,#gostep4" ).submit(function( event ) {
        //blockBody();
});

$(document).on("click",".delete_credit_card",function(){
    if(confirm("Are you sure, you want to delete this card ?")){
        $.ajax({
            type: "POST",
            url: "' . Url::to(['/book-music-tution/remove-card/']) . '",
            data: { "id": $(this).data("uuid")},
            beforeSend: function() {
                blockBody();
            },
            success:function(result){
                if(result.code == 200){

                    $("#gostep-reload-5").submit();

                }else{
                    showErrorMessage(result.message);
                }
                unblockBody();
            },
            error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
    }
})

function cart_refresh(loader_hide = true){

    var monthly_sub_check = ($("#studenttuitionbooking-monthly_sub_checkbox").is(":checked")) ? "yes" : "no";
    var monthly_sub_uuid = $("#studenttuitionbooking-monthly_sub_uuid").val();
    var coupon_code = $("#studenttuitionbooking-coupon_code").val();
    $.ajax({
        type: "GET",
        url: "' . Url::to(['/book-music-tution/refresh-cart/']) . '",
        data: { "monthly_sub_check": monthly_sub_check,"monthly_sub_uuid":monthly_sub_uuid,"coupon_code":coupon_code},
        beforeSend: function() {
            blockBody();
        },
        success:function(result){

            if(result.code == 200){

                $("#studenttuitionbooking-cart_total").val(result.result.cart_total);
                $("#cart_total_text").html(result.result.cart_total);

                $("#studenttuitionbooking-coupon_uuid").val(result.result.coupon.coupon_uuid);
                $("#studenttuitionbooking-discount").val(result.result.coupon.discount);
                $("#discount_text").html(result.result.coupon.discount);

                if(result.result.coupon.coupon_code != ""){
                    $(".promo-code-msg").text(result.result.coupon.message);
                }
                if(result.result.coupon.is_available){
                    $(".promo-code-msg").removeClass("text-danger");
                    $(".promo-code-msg").addClass("text-success");
                }else{
                    $(".promo-code-msg").removeClass("text-success");
                    $(".promo-code-msg").addClass("text-danger");
                }

                if(result.result.monthly_sub_check != "yes"){
                    $("#monthly_sub_price_text_parent").hide();
                }else{
                    $("#monthly_sub_price_text_parent").show();
                }
                if(loader_hide){
                    unblockBody();
                }
            }else{
                unblockBody();
                showErrorMessage(result.message);
            }
        },
        error:function(e){
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
}

$(document).on("ifChanged","input[name=\"StudentTuitionBooking[monthly_sub_checkbox]\"]",function (event) {
    cart_refresh();
});

cart_refresh();

/*********************************/

//set your publishable key
Stripe.setPublishableKey("' . Yii::$app->params['stripe']['publishable_key'] . '");

//callback to handle the response from stripe
    function stripeResponseHandler(status, response) {
        if (response.error) {
            $("#payBtn").removeAttr("disabled");
            $(".payment-errors").html(response.error.message);
            unblockBody();
            showSuccess(response.error.message);
        } else {
            var form$ = $("#frm_step_5");

            // var token = response["id"];
            var token = response.id;

            form$.append("<input type=\"hidden\" name=\"stripeToken\" value=\""+token+"\" />");
            blockBody("Submiting...");
            form$.get(0).submit();
	    blockBody("Please waiting...");
        }
    }

function beforeValidation(){

    $("#frm_step_5" ).find("input").each( //console.log()
        function(index) {
                idnya = $(this).attr("id");
                $("#frm_step_5").yiiActiveForm("validateAttribute", idnya);
                err = $(this).parent().has("has-error");
                msg = $(this).parent().find(".help-block").html();
               // alert(idnya+"=="+msg);

        }
    );
}

 function submitform()
    {
        cart_refresh(false);
        beforeValidation();

        $("#payBtn").attr("disabled", "disabled");
        var dateText = $("#studenttuitionbooking-cc_month_year").val();
        var pieces = dateText.split("-");
        var year = (pieces[0] != undefined && pieces[0] > 0) ? pieces[0] : "1970";
        var month = (pieces[1] != undefined && pieces[1] > 0) ? pieces[1] : "00";

        var token = Stripe.createToken({
            number: $("#studenttuitionbooking-cc_number").val(),
            cvc: $("#studenttuitionbooking-cc_cvv").val(),
            exp_month: month,
            exp_year: year,
        }, stripeResponseHandler);
    }
    
//    $("#frm_step_5").submit(function (event) {
//    console.log(event)
//        //submit from callback
//        alert("submit")
//        blockBody();
//        return false;
//    });

    $(document).bind("cut copy paste","#studenttuitionbooking-cc_number",function(e){
        e.preventDefault();
    });
    
$(".custom_expire_month_year").datepicker( {
        format: "yyyy-mm",
        viewMode: "months", 
        minViewMode: "months",
        startDate:new Date(),
        startView:2
    });

        ', View::POS_END
);
?>
<style>
    .field-studenttuitionbooking-cc_checkbox{
        margin-top: 15px;
    }
    .form-group{
        /*margin-bottom: 0px;*/
    }
    .field-studenttuitionbooking-cc_checkbox{
        margin-top: 0px;
        margin-bottom: 0px;
    }
    .field-studenttuitionbooking-monthly_sub_uuid,
    .field-studenttuitionbooking-monthly_sub_total,
    .field-studenttuitionbooking-discount,
    .field-studenttuitionbooking-cart_total,
    .coupon_parent_div,
    .field-studenttuitionbooking-coupon_code,
    .field-studenttuitionbooking-cc_uuid,
    .field-studenttuitionbooking-cc_name,
    .field-studenttuitionbooking-cc_number,
    .field-studenttuitionbooking-cc_month_year,
    .field-studenttuitionbooking-cc_cvv
    {
        margin-bottom: 0px !important;

    }

    .my_input_border{
        border: 1px solid #bbb8b8 !important;
    }
</style>

<section class="content">

    <div class="row mar-bottom-10">
        <div class="col-xs-12">
            <div class="box">
                <div class="col-md-12 box-panel-header ">
                    <h4 class="col-md-6">Select Instrument &nbsp;<i class="fa fa-check"></i></h4>
                    <div class="col-md-6 ">
                        <?php echo Html::beginForm(['index'], 'post', ['id' => 'gostep1', 'class' => 'form-inline d-block']) ?>
                        <input type="hidden" name="step" value="1">
                        <input type="hidden" name="prev" value="1">
                        <a href="javascript:$('#gostep1').submit();" class="box-panel-header-right-btn pull-right">
                            Change
                        </a>
                        <p class="box-panel-header-right-p text-white pull-right"> <?= $instrumentDetail->name; ?>&nbsp;</p>

                        <?php echo Html::endForm() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row mar-bottom-10">
        <div class="col-xs-12">
            <div class="box">
                <div class="col-md-12 box-panel-header ">
                    <h4 class="col-md-6">Select Plan &nbsp;<i class="fa fa-check"></i></h4>
                    <div class="col-md-6 ">                        
                        <?php echo Html::beginForm(['index'], 'post', ['id' => 'gostep2', 'class' => 'form-inline d-block']) ?>
                        <input type="hidden" name="step" value="2">
                        <input type="hidden" name="prev" value="2">
                        <a href="javascript:$('#gostep2').submit();" class="box-panel-header-right-btn pull-right">
                            Change
                        </a>
                        <p class="box-panel-header-right-p text-white pull-right"> <?= $planDetail['name'] . " Total : $" . $step_data['step_3']['option_total_price']; ?>&nbsp;</p>
                        <?php echo Html::endForm() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row mar-bottom-10">
        <div class="col-xs-12">
            <div class="box">
                <div class="col-md-12 box-panel-header ">
                    <h4 class="col-md-6">Select Tutor &nbsp;<i class="fa fa-check"></i></h4>
                    <div class="col-md-6 ">                        
                        <?php echo Html::beginForm(['index'], 'post', ['id' => 'gostep3', 'class' => 'form-inline d-block']) ?>
                        <input type="hidden" name="step" value="3">
                        <input type="hidden" name="prev" value="3">
                        <a href="javascript:$('#gostep3').submit();" class="box-panel-header-right-btn pull-right">
                            Change
                        </a>
                        <p class="box-panel-header-right-p text-white pull-right"> <?= $tutorDetail->first_name . ' ' . $tutorDetail->last_name; ?>&nbsp;</p>
                        <?php echo Html::endForm() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row mar-bottom-10">
        <div class="col-xs-12">
            <div class="box">
                <div class="col-md-12 box-panel-header ">
                    <h4 class="col-md-6">Select Date&nbsp;<i class="fa fa-check"></i></h4>
                    <div class="col-md-6 ">                        
                        <?php echo Html::beginForm(['index'], 'post', ['id' => 'gostep4', 'class' => 'form-inline d-block']) ?>
                        <input type="hidden" name="step" value="4">
                        <input type="hidden" name="prev" value="4">
                        <a href="javascript:$('#gostep4').submit();" class="box-panel-header-right-btn pull-right">
                            Change
                        </a>
                        <p class="box-panel-header-right-p text-white pull-right"> <?= $step_data['step_4']['all_dates']; ?>&nbsp;</p>
                        <?php echo Html::endForm() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row mar-bottom-10">
        <div class="col-xs-12">
            <div class="box">
                <div class="col-md-12 box-panel-header border-bottom-radious-none">
                    <h4 class="col-md-6">Confirm and Payment</h4>
                    <div class="col-md-6 "> </div>
                </div>

                <div class="box-body box-panel-body " style="border-top-left-radius: 0px;border-top-right-radius: 0px;">
                    <div class="row vertical-center-box vertical-center-box-tablet"  style="">
                        <?php
                        $form = ActiveForm::begin([
                                    'action' => ['index'],
                                    'id' => 'frm_step_5',
                                    'enableClientValidation' => true,
                                    //'enableAjaxValidation' => true,
                                    'validateOnChange' => true,
                                    'options' => ['class' => 'form-horizontal']]);
                        ?>
                        <div class="row">
                            <div class="col-md-12 Ccol-md-offset-1">
                                <div class="row " style="">
                                    <input type="hidden" name="step" value="5">
                                </div>

                                <div class="row  " >
                                    <div class="col-md-12">
                                        <div class="col-md-8">
                                            <div class="card " >
                                                <div class="my-card-body">
                                                    <div class="row ">
                                                        <div class="col-md-7" >
                                                            <?php if ($student_detail->isMonthlySubscription != 1 && !empty($monthly_package)) { ?>
                                                                <div class="col-md-12 mb-0">
                                                                    <!--<label class="control-label" for="" >Monthly Subscription </label>-->
                                                                    <?php
                                                                    $ms_label = "Monthly Subscription to Access Online Video Library";
                                                                    echo $form->field($model, 'monthly_sub_checkbox')->checkbox([
                                                                        //'label' => $monthly_package->name . ' $' . $monthly_package->total_price,
                                                                        'label' => $ms_label. ' $' . $monthly_package->total_price,
                                                                        'template' => '<p class="pull-left"><label class="icheck-label form-label" for="save subcription"> {input}  </label> <p>',
                                                                        'class' => 'skin-square-red float-left monthly_sub_checkbox',])->label(false);
                                                                    ?>

                                                                </div>
                                                            <?php } ?>

                                                            <div class="row mb-0">
                                                                <div class="col-md-12 creative_kids_voucher_code_div">
                                                                    <div class="col-md-12">
                                                                        <?php /*
                                                                          $form->field($model, 'creative_kids_voucher_code', ['inputOptions' => ['autocomplete' => 'off'], 'template' => ''
                                                                          . '<div class="col-md-12 creative_kids_voucher_code_div">'
                                                                          . '<div class="col-md-12">{input}</div>'
                                                                          . '</div>'
                                                                          . '<div class="col-md-12">{error}</div>'])
                                                                          ->textInput(['class' => 'form-control my_input_border pull-left', 'maxlength' => true, 'placeholder' => 'NSW Creative Kids Voucher Code'])
                                                                         */
                                                                        ?>

                                                                        <?=
                                                                                $form->field($model, 'creative_kids_voucher_code', ['inputOptions' => ['autocomplete' => 'off']])
                                                                                ->textInput(['class' => 'form-control my_input_border pull-left', 'maxlength' => true, 'placeholder' => 'NSW Creative Kids Voucher Code'])
                                                                        ?>
                                                                    </div>
                                                                </div> 
                                                            </div>

                                                            <div class="row mb-0">
                                                                <div class="col-md-12 kids_name_div">
                                                                    <div class="col-md-12">
                                                                        <?php /*
                                                                          $form->field($model, 'kids_name', ['inputOptions' => ['autocomplete' => 'off'], 'template' => ''
                                                                          . '<div class="col-md-12 kids_name_div">'
                                                                          . '<div class="col-md-12">{input}<font class="help-block" style="font-size:11px;margin-left:3%;">Please enter the kid\'s name exactly as shown on the Creative Kids Voucher</font></div>'
                                                                          . '</div>'
                                                                          . '<div class="col-md-12">{error}</div>'])
                                                                          ->textInput(['class' => 'form-control my_input_border pull-left', 'maxlength' => true, 'placeholder' => 'Kid\'s Name'])
                                                                         */ ?>

                                                                        <?=
                                                                                $form->field($model, 'kids_name', ['inputOptions' => ['autocomplete' => 'off']
                                                                                ])
                                                                                ->textInput(['class' => 'form-control my_input_border pull-left', 'maxlength' => true, 'placeholder' => 'Kid\'s Name'])
                                                                        ?>
                                                                        <font class="help-block" style="margin-top: -8%;margin-bottom: 0;margin-left: -3%;font-size: 11px; color: red; weight:bold;"><strong>Please enter the kid's name exactly as shown on the Creative Kids voucher.</strong></font>
                                                                    </div>
                                                                </div> 
                                                            </div>

                                                            <div class="row mb-0">
                                                                <div class="col-md-12 kids_dob_div">
                                                                    <div class="col-md-12">
                                                                        <?=
                                                                        $form->field($model, 'kids_dob')->textInput(['readonly' => true, 'class' => 'form-control custom_date_picker', 'maxlength' => true, 'placeholder' => 'DD-MM-YYYY']);

                                                                        /* $form->field($model, 'kids_dob', ['inputOptions' => ['autocomplete' => 'off'], 'template' => ''
                                                                          . '<div class="col-md-12 kids_dob_div">'
                                                                          . '<div class="col-md-12">{input}</div>'
                                                                          . '</div>'
                                                                          . '<div class="col-md-12">{error}</div>'])
                                                                          ->textInput(['class' => 'form-control my_input_border pull-left', 'maxlength' => true, 'placeholder' => 'Kid\'s Date of Birth']) */
                                                                        ?></div>
                                                                </div>    

                                                            </div>


                                                            <div class="row mb-0">

                                                                <?=
                                                                        $form->field($model, 'coupon_code', ['inputOptions' => ['autocomplete' => 'off'], 'template' => ''
                                                                            . '<div class="col-md-12 coupon_parent_div">'
                                                                            . '<div class="col-md-8">{input}</div>'
                                                                            . '<div class="">'
                                                                            . '<button type="button" id="coupon_btn" class="btn btn-md btn-primary" onclick="cart_refresh();">Apply</button>'
                                                                            . '</div>'
                                                                            . '</div>'
                                                                            . '<div class="col-md-12">{error}</div>'])
                                                                        ->textInput(['class' => 'form-control my_input_border pull-left', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('coupon_code')])
                                                                ?>
                                                                <div class="col-md-12"><p class="promo-code-msg "></p></div>
                                                            </div>



                                                        </div>
                                                        <div class="col-md-5" style="border-left: 1px solid #bbb8b8;">
                                                            <div class=" has-static">
                                                                <label class="form-label">Plan Price : <strong>$<span id="plan_price_text"><?= $planprice; ?></span></strong></label>
                                                            </div>
                                                            <div class=" has-static">
                                                                <label class="form-label">Plan GST : <strong>$<span id="plan_gst_text"><?= $plangst; ?></span></strong></label>
                                                            </div>
                                                            <div class=" has-static">
                                                                <label class="form-label">Plan Total : <strong>$<span id="plan_total_text"><?= $plantotal_price; ?></span></strong></label>
                                                            </div>
                                                            <?php if (!empty($monthly_package)) { ?>
                                                                <div class=" has-static" id="monthly_sub_price_text_parent" style="<?php echo ($model->monthly_sub_checkbox) ? "display:block" : "display:none" ?>">
                                                                    <label class="form-label">Monthly Subscription : <strong>$<span id="monthly_sub_price_text"><?= $monthly_package->total_price; ?></span></strong></label>
                                                                </div>
                                                            <?php } ?>
                                                            <div class=" has-static">
                                                                <label class="form-label">Discount : <strong>$<span id="discount_text"><?= $model->discount; ?></span></strong></label>
                                                            </div>
                                                            <div class=" has-static">
                                                                <label class="form-label">Amount Payable : <strong>$<span id="cart_total_text"><?= $model->cart_total; ?></span></strong></label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="hidden_inputs">

                                                        <?= $form->field($model, 'monthly_sub_uuid', ['template' => '{input}{error}'])->hiddenInput(['value' => (!empty($monthly_package)) ? $monthly_package->uuid : ''])->label(false); ?>
                                                        <?= $form->field($model, 'monthly_sub_total', ['template' => '{input}{error}'])->hiddenInput(['value' => (!empty($monthly_package)) ? $monthly_package->total_price : 0])->label(false); ?>

                                                        <?= $form->field($model, 'discount', ['template' => '{input}{error}'])->hiddenInput()->label(false); ?>
                                                        <?= $form->field($model, 'cart_total', ['template' => '{input}{error}'])->hiddenInput()->label(false); ?>
                                                        <?= $form->field($model, 'coupon_uuid', ['template' => '{input}{error}'])->hiddenInput()->label(false); ?>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="spacer"></div>

                                            <div class="card " >





                                                <?php if (!empty($saved_cc_detail)) { ?>
                                                    <div class="my-card-body" style="padding-top: 0px">
                                                        <div class="row " >
                                                            <div class="col-md-12 " style="padding: 10px;background-color: #000;border: 1px solid; border-radius: 20px 15px 0px 0px;" >

                                                                <button type="button" class="btn btn-primary btn-sm pull-right delete_credit_card" style="border:1px solid #fff;" data-uuid="<?= $saved_cc_detail['uuid']; ?>">Remove Card</button>

                                                            </div>
                                                        </div>

                                                        <div class="hidden_inputs">
                                                            <?= $form->field($model, 'cc_uuid', ['template' => '{input}{error}'])->hiddenInput(['value' => $saved_cc_detail['uuid']])->label(false); ?>
                                                            <?= $form->field($model, 'cc_name', ['template' => '{input}{error}'])->hiddenInput(['value' => $saved_cc_detail['name']])->label(false); ?>
                                                            <?= $form->field($model, 'cc_number', ['template' => '{input}{error}'])->hiddenInput(['value' => General::decrypt_str($saved_cc_detail['number'])])->label(false); ?>
                                                            <?php
                                                            $exp_date = date('Y-m', strtotime($saved_cc_detail['expire_date']));
                                                            echo $form->field($model, 'cc_month_year', ['template' => '{input}{error}'])->hiddenInput(['value' => $exp_date])->label(false);
                                                            ?>
                                                        </div>

                                                        <div class="row">
                                                            <div class=" col-md-10 col-md-offset-1">
                                                                <div class=" col-md-12 ">                                                                    
                                                                    <div class="form-group">
                                                                        <span><label class="control-label" for="studentcreditcard-name">Card Holder Name*</label></span>
                                                                        <span type="text" class="form-control" disabled="disabled"><?= $saved_cc_detail['name']; ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class=" col-md-10 col-md-offset-1">
                                                                <div class=" col-md-12 ">                                                                    
                                                                    <div class="form-group">
                                                                        <span><label class="control-label" for="studentcreditcard-number">Card Number*</label></span>
                                                                        <span type="text" class="form-control bg-gray-field" disabled="disabled">
                                                                            <?php
                                                                            $number = General::decrypt_str($saved_cc_detail['number']);
                                                                            echo General::ccMasking($number);
                                                                            ?>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class=" col-md-10 col-md-offset-1">
                                                                <div class=" col-md-5 ">
                                                                    <?=
                                                                            $form->field($model, 'cc_cvv', ['template' => '<span>{label}</span>{input}{error}', 'inputOptions' => ['autocomplete' => 'new-password']])
                                                                            ->passwordInput(['class' => 'form-control my_input_border', 'maxlength' => true,])
                                                                            ->label('CVV*')
                                                                    ?>
                                                                </div>
                                                                <div class=" col-md-2 "></div>
                                                                <div class=" col-md-5 ">
                                                                    <span><label class="control-label" for="studentcreditcard-cc_month_year">Expiry*</label></span>
                                                                    <span type="text" class="form-control" disabled="disabled"><?= date("Y-m", strtotime($saved_cc_detail['expire_date'])); ?></span>
                                                                </div>
                                                            </div>
                                                        </div>



                                                        <div class="row">
                                                            <div class="col-md-10 col-md-offset-1  term_condition_checkbox_parent_col">
                                                                <div class="checkbox icheck">
                                                                    <?php
                                                                    echo $form->field($model, 'term_condition_checkbox', ['template' => ' {input} I agree to Musiconns <a data-toggle="modal" data-target="#terms_condition_model"><b>Terms and Conditions</b></a>{error} '])->checkbox([
                                                                        'class' => 'skin-square-red float-left '], false);
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                <?php } else { ?>
                                                    <div class="my-card-body" >
                                                        <div class="row">
                                                            <div class=" col-md-10 col-md-offset-1">
                                                                <div class=" col-md-12 ">
                                                                    <?=
                                                                            $form->field($model, 'cc_name', ['template' => '<span>{label}</span>{input}{error}'])
                                                                            ->textInput(['class' => 'form-control demo', 'maxlength' => true,])
                                                                            ->label($model->getAttributeLabel('cc_name') . '*')
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class=" col-md-10 col-md-offset-1">
                                                                <div class=" col-md-12 ">
                                                                    <?=
                                                                            $form->field($model, 'cc_number', ['template' => '<span>{label}</span>{input}{error}', 'inputOptions' => ['autocomplete' => 'new-password']])
                                                                            ->textInput(['class' => 'credit-card-number form-control', 'maxlength' => true,])
                                                                            ->label($model->getAttributeLabel('cc_number') . '*')
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class=" col-md-10 col-md-offset-1">
                                                                <div class=" col-md-5 ">
                                                                    <?=
                                                                            $form->field($model, 'cc_cvv', ['template' => '<span>{label}</span>{input}{error}', 'inputOptions' => ['autocomplete' => 'new-password']])
                                                                            ->passwordInput(['class' => 'form-control my_input_border ', 'maxlength' => 5,])
                                                                            ->label($model->getAttributeLabel('cc_cvv') . '*')
                                                                    ?>
                                                                </div>
                                                                <div class=" col-md-2 "></div>
                                                                <div class=" col-md-5 ">
                                                                    <?=
                                                                            $form->field($model, 'cc_month_year', ['template' => '<span>{label}</span>{input}{error}'])
                                                                            ->textInput(['class' => 'form-control custom_expire_month_year ', 'data-min-view-mode' => 'months', 'data-start-view' => '2', 'data-format' => 'yyyy-mm', 'maxlength' => true,])
                                                                            ->label($model->getAttributeLabel('cc_month_year') . '*')
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-10 col-md-offset-1 cc_ckeckbx_parent_col">
                                                                <?php
                                                                echo $form->field($model, 'cc_checkbox')->checkbox([
                                                                    'template' => '<p class="save_future pull-left"><label class="icheck-label form-label" for="save"> {input}  </label> <p>',
                                                                    'class' => 'skin-square-red float-left save_future'])->label(false);
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-10 col-md-offset-1 term_condition_checkbox_parent_col">
                                                                <?php
                                                                echo $form->field($model, 'term_condition_checkbox', ['template' => ' {input} I agree to Musiconns <a data-toggle="modal" data-target="#terms_condition_model"><b>Terms and Conditions</b></a>{error} '])->checkbox([
                                                                    'class' => 'skin-square-red float-left '], false);
                                                                ?>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class=" col-md-10 col-md-offset-1">

                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>

                                            <div class="spacer"></div>

                                            <div class="card">

                                                <div class="payment-errors text-danger mar-left-10"></div>

                                                <?= Html::button(Yii::t('app', 'Proceed Securely'), ['class' => 'btn btn-primary mar-right-10', 'id' => 'payBtn', 'onclick' => 'submitform()', 'style' => 'float: right;']) ?>
                                            </div>

                                        </div>

                                        <div class="col-md-4">
                                            <div class="card " >
                                                <div class="my-card-body">
                                                    <div class="card-body">
                                                        <p class="text-primary"><strong>Instrument :</strong> <?= $instrumentDetail->name; ?></p>
                                                        <p class="text-primary"><strong>Plan :</strong> <?= $planDetail['name']; ?></p>
                                                        <p class="text-primary"><strong>Tutor :</strong> <?= $tutorDetail->first_name . ' ' . $tutorDetail->last_name; ?></p>
                                                        <p class="text-primary"><strong>Session Time:</strong> <?= General::displayTimeFormat($step_data['step_4']['start_hour'], 'h:i A'); ?></p>
                                                        <p class="text-primary"><strong>Session Dates:</strong> <?= $step_data['step_4']['all_dates']; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <?php ActiveForm::end(); ?>  
                    </div>
                </div>
                <?php echo Html::beginForm(['index'], 'post', ['id' => 'gostep-reload-5', 'class' => 'form-inline d-block']) ?>
                <input type="hidden" name="step" value="5">
                <input type="hidden" name="prev" value="5">
                <input type="hidden" name="remove_card" value="yes">
                <?php echo Html::endForm() ?>

            </div>
        </div>
    </div>

    <a class="btn btn-primary btn-sm black_button_in" href="<?= Yii::$app->getUrlManager()->createUrl('/booked-session/my-lessons'); ?>">RETURN TO MY LESSONS</a>

</section>





<!-- modal start -->
<div class="modal fade" id="terms_condition_model" tabindex="-1" role="dialog" aria-labelledby="ultraModal-Label" aria-hidden="true">
    <div class="modal-dialog animated zoomIn">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Terms and Conditions</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <?= \app\models\Settings::getSettingsByName('payment_terms_and_conditions'); ?>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-primary" type="button">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- modal end -->
