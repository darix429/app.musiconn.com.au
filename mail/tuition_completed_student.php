<?php

use yii\helpers\Html;
//echo $content['name']; exit;
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['student_name'] ?>,</p>

	    <p>We hope that the lesson you've just completed went well.  The recording of your sesson will be made available for viewing shortly.</p>

            <p style="text-decoration: underline;"><strong>Session Details</strong></p>
	    <p><strong>Tutor: </strong> <?= $content['tutor_name']; ?></p>
	    <p><strong>Lesson Date/Time: </strong> <?= $content['time']; ?></p>
            
            <p>To view your recorded session, follow the video link below:</p>
            <p><a href="<?= $content['link'] ?>" target="_blank">Video Link</a></p> 

            <p>Please do not reply to this unattended mailbox.  Any queries or feedback can be directed to admin@musiconn.com.au, or call us on 1300 068 742.</p>
            
            <p>Regards,<br>
            System Administrators</p>
        </td>
    </tr>
</table>

