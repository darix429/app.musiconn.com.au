<?php

namespace app\models;

use Yii;
use app\libraries\General;
use kartik\mpdf\Pdf;
use yii\helpers\FileHelper;
/**
 * This is the model class for table "{{%student_tuition_booking}}".
 *
 * @property string $uuid
 * @property string $booking_id
 * @property string $booking_datetime
 * @property string $student_uuid
 * @property string $tutor_uuid
 * @property string $instrument_uuid
 * @property string $tutorial_name
 * @property string $tutorial_description
 * @property string $tutorial_type_code
 * @property int $tutorial_min
 * @property string $payment_term_code
 * @property int $total_session
 * @property int $session_length_day
 * @property string $price
 * @property string $gst
 * @property string $gst_description
 * @property string $total_price
 * @property string $status
 * @property string $created_at
 *
 * @property Instrument $instrumentUu
 * @property Student $studentUu
 * @property Tutor $tutorUu
 */
class StudentTuitionBooking extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return '{{%student_tuition_booking}}';
    }

    public $instrumentcheckbox;
    public $onlinetutorial_uuid;
    public $onlinetutorialcheckbox;
    public $onlinetutorialtype;
    public $tutorcheckbox;
    public $session_week_day;
    public $session_when_start;
    public $start_hour;
    public $all_dates;
    public $session_text_info;
    public $cc_name;
    public $cc_number;
    public $cc_cvv;
    public $cc_month_year;
    public $cc_uuid;
    public $cc_checkbox;
    public $monthly_sub_uuid;
    public $monthly_sub_total;
    public $monthly_sub_checkbox;
    //public $discount;
    public $cart_total;
    public $coupon_code;
    public $tutorial_type_code_checkbox;
    public $term_condition_checkbox;
    public $AVAILABILITIES_TYPE_FIXED = 'fixed';

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
                [['instrument_uuid'], 'required', 'on' => 'step_1', 'message' => 'Instrument cannot be blank. Select any one Instrument.'],
                [['instrument_uuid'], 'required', 'on' => 'credit_step_1', 'message' => 'Instrument cannot be blank. Select any one Instrument.'],
                [['tutorial_type_code'], 'required', 'on' => 'credit_step_2', 'message' => 'Tutorial type cannot be blank. Select any one type.'],
                [['onlinetutorial_uuid'], 'required', 'on' => 'step_2', 'message' => 'Online Tutorial cannot be blank. Select any one Online Tutorial.'],
                [['tutor_uuid'], 'required', 'on' => 'step_3', 'message' => 'Tutor cannot be blank. Select any one Tutor.'],
                [['tutor_uuid'], 'required', 'on' => 'credit_step_3', 'message' => 'Tutor cannot be blank. Select any one Tutor.'],
                [['session_week_day'], 'required', 'on' => 'step_4', 'message' => 'Select any one week day.'],
                [['start_hour'], 'required', 'on' => 'step_4', 'message' => 'Select hour.'],
                [['all_dates'], 'required', 'on' => 'step_4', 'message' => ''],
                [['session_text_info'], 'safe', 'on' => 'step_4'],
            
                [['session_week_day'], 'required', 'on' => 'credit_step_4', 'message' => 'Select any one week day.'],
                [['start_hour'], 'required', 'on' => 'credit_step_4', 'message' => 'Select hour.'],
                [['all_dates'], 'required', 'on' => 'credit_step_4', 'message' => ''],
                [['session_text_info'], 'safe', 'on' => 'credit_step_4'],
                [['session_when_start'], 'required', 'on' => 'credit_step_4', 'message' => 'Select start date.'],
            
                [['cc_checkbox', 'cc_uuid', 'monthly_sub_uuid', 'monthly_sub_total', 'discount', 'monthly_sub_checkbox', 'coupon_code'], 'safe', 'on' => 'step_5'],
                [['cc_name', 'cc_number', 'cc_cvv', 'cc_month_year', 'cart_total'], 'required', 'on' => 'step_5',],
            //[['cc_month'],'number','min'=>date("m"), 'on' => 'step_5', ],
            [['cc_month_year'], 'date', 'format' => 'php:Y-m', 'on' => 'step_5',],
                [['term_condition_checkbox'], 'required', 'on' => 'step_5','requiredValue' => 1,'message' => 'Accept Terms and Condition'],
                
                [['cc_number'], 'integer', 'on' => 'step_5',],
                [['cc_number'], 'string', 'max' => 16, 'on' => 'step_5',],
                [['cc_number'], 'string', 'min' => 14, 'on' => 'step_5',],
                [['cc_cvv'], 'string', 'min' => 3, 'on' => 'step_5',],
                [['cc_cvv'],'string', 'max'=>5,  'on' => 'step_5', ],
            ['discount', 'default', 'value' => 0],
                ['cart_total', 'default', 'value' => 0],
                [['session_when_start'], 'required', 'on' => 'step_4', 'message' => 'Select start date.'],
                [['uuid', 'booking_id', 'student_uuid', 'tutor_uuid', 'instrument_uuid', 'tutorial_name', 'tutorial_description', 'gst_description', 'status'], 'string'],
                [['booking_id', 'student_uuid', 'tutorial_name', 'tutorial_description', 'tutorial_type_code', 'tutorial_min', 'payment_term_code', 'total_session', 'session_length_day', 'gst_description'], 'required'],
                [['booking_datetime', 'created_at', 'instrumentcheckbox', 'onlinetutorialcheckbox', 'onlinetutorialtype', 'tutorial_type_code_checkbox', 'tutorcheckbox', 'session_week_day'], 'safe'],
                [['tutorial_min', 'total_session', 'session_length_day'], 'default', 'value' => null],
                [['tutorial_min', 'total_session', 'session_length_day'], 'integer'],
                [['price', 'gst', 'total_price'], 'number'],
                [['tutorial_type_code'], 'string', 'max' => 4],
                [['payment_term_code'], 'string', 'max' => 1],
                [['instrument_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Instrument::className(), 'targetAttribute' => ['instrument_uuid' => 'uuid']],
                [['student_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['student_uuid' => 'student_uuid']],
                [['tutor_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Tutor::className(), 'targetAttribute' => ['tutor_uuid' => 'uuid']],
                [['order_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_uuid' => 'uuid']],
                [['student_monthly_subscription_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => StudentMonthlySubscription::className(), 'targetAttribute' => ['student_monthly_subscription_uuid' => 'uuid']],
                [['coupon_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Coupon::className(), 'targetAttribute' => ['coupon_uuid' => 'uuid']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'booking_id' => Yii::t('app', 'Booking ID'),
            'booking_datetime' => Yii::t('app', 'Booking Date'),
            'student_uuid' => Yii::t('app', 'Student Name'),
            'tutor_uuid' => Yii::t('app', 'Tutor Name'),
            'instrument_uuid' => Yii::t('app', 'Instrument'),
            'tutorial_name' => Yii::t('app', 'Tutorial Name'),
            'tutorial_description' => Yii::t('app', 'Tutorial Description'),
            'tutorial_type_code' => Yii::t('app', 'Tutorial Type Code'),
            'tutorial_min' => Yii::t('app', 'Tutorial Min'),
            'payment_term_code' => Yii::t('app', 'Payment Term Code'),
            'total_session' => Yii::t('app', 'Total Session'),
            'session_length_day' => Yii::t('app', 'Session Length Day'),
            'price' => Yii::t('app', 'Price'),
            'gst' => Yii::t('app', 'Gst'),
            'gst_description' => Yii::t('app', 'Gst Description'),
            'total_price' => Yii::t('app', 'Total Price'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'cc_checkbox' => 'Save for future use',
            'cc_name' => 'Name',
            'cc_number' => 'Card Number',
            'cc_cvv' => 'CVV',
            'cc_month' => 'Month',
            'cc_year' => 'Year',
            'cc_month_year' => 'Expire Month',
            'order_uuid' => Yii::t('app', 'Order ID'),
            'student_monthly_subscription_uuid' => Yii::t('app', 'Subscription Name'),
            'term_condition_checkbox' => 'Term and condition',
            'coupon_uuid' => 'Coupon',
            'coupon_code' => 'Promo Code',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstrumentUu() {
        return $this->hasOne(Instrument::className(), ['uuid' => 'instrument_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentUu() {
        return $this->hasOne(Student::className(), ['student_uuid' => 'student_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTutorUu() {
        return $this->hasOne(Tutor::className(), ['uuid' => 'tutor_uuid']);
    }
    
    public function getStudentMonthlySubscriptionUu()
    {
        return $this->hasOne(StudentMonthlySubscription::className(), ['uuid' => 'student_monthly_subscription_uuid']);
    }

    public function getOrderUu()
    {
        return $this->hasOne(Order::className(), ['uuid' => 'order_uuid']);
    }
    
    public function getCouponUu()
    {
        return $this->hasOne(Coupon::className(), ['uuid' => 'coupon_uuid']);
    }

    public function setSession($_id, $key, $value) {
        if (!isset(Yii::$app->session[$_id])) {
            Yii::$app->session[$_id] = serialize(array($key => $value));
        } else {
            $session = unserialize(Yii::$app->session[$_id]);
            $session[$key] = $value;
            Yii::$app->session[$_id] = serialize($session);
        }
    }

    public static function getSession($_id, $key = NULL) {
        $session = unserialize(Yii::$app->session[$_id]);

        if ($key === NULL) {
            return $session;
        } else {
            return isset($session[$key]) ? $session[$key] : array();
        }
    }
    
    public static function unsetSession($token)
    {
        unset(Yii::$app->session[$token]);
    }

    public function getInstrumentList() {
        return Instrument::find()->where(['status' => 'ACTIVE'])->all();
    }

    public function getOnlineTutorialList($type = '') {
        if (!empty($type)) {
            $where = ['payment_term.term' => $type, 'online_tutorial_package.status' => 'ENABLED'];
        } else {
            $where = ['online_tutorial_package.status' => 'ENABLED'];
        }
        return OnlineTutorialPackage::find()->joinWith(['paymentTermUu'])->where($where)->all();
    }

    public function getInstrumentWiseTutorList($instrument_uuid = '') {

        $sql = "select tutor.*,tutor_instrument.instrument_uuid,tutor_instrument.uuid as tutor_instrument_table_uuid from tutor LEFT JOIN tutor_instrument ON tutor_instrument.tutor_uuid = tutor.uuid 
            where 1=1 and tutor.status = 'ENABLED' ";
        if ($instrument_uuid != "") {
            $sql .= " AND tutor_instrument.instrument_uuid = '$instrument_uuid' ";
        }

        $command = Yii::$app->db->createCommand($sql);
        $result = $command->queryAll();
        return $result;
    }

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['step_1'] = ['instrument_uuid', 'instrumentcheckbox'];
        $scenarios['step_2'] = ['onlinetutorial_uuid', 'onlinetutorialcheckbox', 'onlinetutorialtype'];
        $scenarios['step_3'] = ['tutorcheckbox', 'tutor_uuid'];
        $scenarios['step_4'] = ['session_week_day', 'session_when_start', 'start_hour', 'all_dates', 'session_text_info'];
        $scenarios['step_5'] = ['cc_name', 'cc_number', 'cc_cvv', 'cc_month_year', 'cc_checkbox', 'cc_uuid', 'monthly_sub_uuid', 'monthly_sub_total', 'discount', 'cart_total', 'monthly_sub_checkbox', 'coupon_code','term_condition_checkbox'];
        $scenarios['credit_step_1'] = ['instrument_uuid', 'instrumentcheckbox'];
        $scenarios['credit_step_2'] = ['tutorial_type_code','tutorial_type_code_checkbox'];
        $scenarios['credit_step_3'] = ['tutorcheckbox', 'tutor_uuid'];
        $scenarios['credit_step_4'] = ['session_week_day', 'session_when_start', 'start_hour', 'all_dates', 'session_text_info'];
        return $scenarios;
    }
    
    public function hoursRange($lower = 0, $upper = 86400, $step = 1800, $format = '') {

        $times = array();
        if (empty($format)) {
            $format = 'h:i A';
        }
        foreach (range($lower, $upper, $step) as $increment) {
            $increment = gmdate('H:i', $increment);
            list( $hour, $minutes ) = explode(':', $increment);
            $date = new \DateTime($hour . ':' . $minutes);
            $times[(string) $increment] = $date->format($format);
        }
        return $times;
    }

    public function start_hour_array() {

        $formatter = function ($time) {
            if ($time % 3600 == 0) { 
                return date('H:i', $time);
                //return date('ga', $time);
            } else {
                return date('H:i', $time);
                //return date('g:ia', $time);
            }
        };
        $halfHourSteps = range(0, 47 * 1800, 1800);
        
        $res = array_map($formatter, $halfHourSteps);
        $resDisplay = array_map(function ($a) { return date('h:i A', strtotime($a)); }, $res);
        
        $result = array_combine($res, $resDisplay);
        
        return $result;
    }

    public function disabled_week_day($day = '') {
        $array = ['sunday' => 0, 'monday' => 1, 'tuesday' => 2, 'wednesday' => 3, 'thursday' => 4, 'friday' => 5, 'saturday' => 6];
        if ($day != '') {
            unset($array[$day]);
        }
        return implode(',', array_values($array));
    }

    public function convert_session_text($sessionDetail) {
        $text = '';
        $day = '';
        $start = '';
        $end = '';
        $date = [];
        if (!empty($sessionDetail['session_array'])) {

            foreach ($sessionDetail['session_array'] as $key => $value) {
                $day = ucfirst($value['day']);
                $start = ucfirst($value['start']);
                $end = ucfirst($value['end']);
                $date[] = $value['date'];
            }
            $date_txt = '';
            if (count($date) > 0) {
                $date_txt = '<p class="uibadges">';
                foreach ($date as $k => $v) {
                    $date_txt .= '<span class="badge badge-pill badge-info">' . General::displayDate($v) . '</span>';
                }
                $date_txt .= '</p>';
            }
            $text = '<strong>Every ' . $day . ', ' . General::displayTime($start) . ' - ' . General::displayTime($end) . ', untill ' . end($date) . ", " . count($date) . " occurrence(s)</strong> " . $date_txt;
        }
        return $text;
    }
    
    public function credit_convert_session_text($sessionDetail) {
        $text = '';
        $day = '';
        $start = '';
        $end = '';
        $date = [];
        if (!empty($sessionDetail['session_array'])) {

            foreach ($sessionDetail['session_array'] as $key => $value) {
                $day = ucfirst($value['day']);
                $start = ucfirst($value['start']);
                $end = ucfirst($value['end']);
                $date[] = $value['date'];
            }
            $date_txt = '';
            if (count($date) > 0) {
                $date_txt = '<p class="uibadges">';
                foreach ($date as $k => $v) {
                    $date_txt .= '<span class="badge badge-pill badge-info">' . General::displayDate($v) . '</span>';
                }
                $date_txt .= '</p>';
            }
            $text = '<strong>' . $day . ', ' . General::displayTime($start) . ' - ' . General::displayTime($end) . '</strong> ' . $date_txt;
        }
        return $text;
    }
    
    public function credit_time_between_day_working_period($tutor_uuid, $day = 'sunday', $selected_time = "00:00", $tutorial_code) {

        $flag = false;

        $tutorDetail = Tutor::findOne($tutor_uuid);
        
        $tutorialType = TutorialType::find()->where(['code' => $tutorial_code])->asArray()->one();
        
        if (!empty($tutorDetail) && !empty($tutorialType)) {

            $tutor_working_plan = json_decode($tutorDetail->working_plan, true);
            $ses_min = $tutorialType['min'];

            if (!empty($tutor_working_plan[$day])) {

                $start = date('Y-m-d') . " " . $tutor_working_plan[$day]['start'];
                $end = date('Y-m-d') . " " . $tutor_working_plan[$day]['end'];

                $period_start = new \DateTime($start);
                $period_end = new \DateTime($end);
                $selected_time_date_start = new \DateTime($selected_time);

                $period_end = new \DateTime(date('Y-m-d H:i:s', strtotime($period_end->format('Y-m-d H:i:s') . " +" . $ses_min . " minute")));

                if ($selected_time_date_start >= $period_start && $selected_time_date_start <= $period_end) {
                    $flag = TRUE;
                }
            }
        }
        return $flag;
    }
    
    public function credit_getDayChanges($tutor_uuid, $day = '', $tutorial_code, $selected_time, $selected_date = '',$student_uuid= '') {

        $session_array = [];
        $session_when_start = '';
        $return = ['session_week_day' => $day, 'session_when_start' => $session_when_start, 'session_array' => $session_array, 'message' => ''];
        if (!empty($day)) {

            $tutorDetail = Tutor::findOne($tutor_uuid);
            $tutorialType = TutorialType::find()->where(['code' => $tutorial_code])->asArray()->one();
            //$package = OnlineTutorialPackage::find()->joinWith(['paymentTermUu', 'tutorialTypeUu'])->where(['online_tutorial_package.uuid' => $online_package_uuid])->asArray()->one();

            if (!empty($tutorDetail) && !empty($tutorialType)) {

                $total_session = 1;
                $session_minute = $tutorialType['min'];
                $session_type = $tutorialType['type']; // W OR F                
                $tutor_working_plan = json_decode($tutorDetail->working_plan, true);
                $skip_count = 50;

                if (!empty($tutor_working_plan[$day])) {

                    $day_break_array = isset($tutor_working_plan[$day]['breaks']) ? $tutor_working_plan[$day]['breaks'] : [];

                    if ($selected_date != '') {
                        $next = false;
                        $start_date = $selected_date;
                    } 
                    /*else {
                     // if Today date include in booking then this section will enable
                        $next = true;
                        $start_date = date('Y-m-d');
                    }*/
                    
                    $start_date = date('Y-m-d', strtotime($start_date));

                    for ($i = 0; $i < $total_session; $i++) {

                        if ($next) {
                            $session_date = date('Y-m-d', strtotime($start_date . ' next ' . $day));
                        } else {
                            $session_date = $start_date;
                        }
                        $start_date = $session_date;
                        $next = true;

                        $start = $selected_time;
                        $end = date('H:i', strtotime($selected_time . " +" . $session_minute . " minute"));


                        $get_hours = self::get_available_hours($tutor_uuid, $tutor_working_plan, $session_date, $session_minute, $student_uuid);

                        $skip = (!in_array($selected_time, $get_hours)) ? true : false;
                        if ($skip) {
                            if ($skip_count == 0) {
                                //$return['message'] = "This time period is not available in ". ucfirst($day);
                                $return['message'] = "Your selected tutor is not available for one or more of the selected times/days. ". ucfirst($day). " ".General::displayTime($start);
                                break;
                            }
                            $skip_count--;
                            $i--;
                            continue;
                        }
                        $current_session = ['date' => General::displayDate($session_date), 'day' => $day, 'start' => $start, 'end' => $end, 'time_array' => $get_hours];
                        $session_array[] = $current_session;
                    }
                }
            }
        }
        $return['session_when_start'] = (!empty($session_array)) ? $session_array[0]['date'] : '';
        $return['session_array'] = $session_array;
        return $return;
    }

    public function time_between_day_working_period($tutor_uuid, $day = 'sunday', $selected_time = "00:00", $online_package_uuid) {

        $flag = false;

        $tutorDetail = Tutor::findOne($tutor_uuid);
        $package = OnlineTutorialPackage::find()->joinWith(['paymentTermUu', 'tutorialTypeUu'])->where(['online_tutorial_package.uuid' => $online_package_uuid])->asArray()->one();
        if (!empty($tutorDetail) && !empty($package)) {

            $tutor_working_plan = json_decode($tutorDetail->working_plan, true);
            $ses_min = $package['tutorialTypeUu']['min'];

            if (!empty($tutor_working_plan[$day])) {

                $start = date('Y-m-d') . " " . $tutor_working_plan[$day]['start'];
                $end = date('Y-m-d') . " " . $tutor_working_plan[$day]['end'];

                $period_start = new \DateTime($start);
                $period_end = new \DateTime($end);
                $selected_time_date_start = new \DateTime($selected_time);

                $period_end = new \DateTime(date('Y-m-d H:i:s', strtotime($period_end->format('Y-m-d H:i:s') . " +" . $ses_min . " minute")));

                if ($selected_time_date_start >= $period_start && $selected_time_date_start <= $period_end) {
                    $flag = TRUE;
                }
            }
        }
        return $flag;
    }

    public function getDayChanges($tutor_uuid, $day = '', $online_package_uuid, $selected_time, $selected_date = '',$student_uuid= '') {

        $session_array = [];
        $session_when_start = '';
        $return = ['session_week_day' => $day, 'session_when_start' => $session_when_start, 'session_array' => $session_array, 'message' => ''];
        if (!empty($day)) {

            $tutorDetail = Tutor::findOne($tutor_uuid);
            $package = OnlineTutorialPackage::find()->joinWith(['paymentTermUu', 'tutorialTypeUu'])->where(['online_tutorial_package.uuid' => $online_package_uuid])->asArray()->one();

            if (!empty($tutorDetail) && !empty($package)) {

                $total_session = $package['duration'];
                $session_minute = $package['tutorialTypeUu']['min'];
                $session_type = $package['tutorialTypeUu']['type']; // W OR F                
                $tutor_working_plan = json_decode($tutorDetail->working_plan, true);
                $skip_count = 50;

                if (!empty($tutor_working_plan[$day])) {

                    $day_break_array = isset($tutor_working_plan[$day]['breaks']) ? $tutor_working_plan[$day]['breaks'] : [];

                    if ($selected_date != '') {
                        $next = false;
                        $start_date = $selected_date;
                    } 
                    /*else {
                     // if Today date include in booking then this section will enable
                        $next = true;
                        $start_date = date('Y-m-d');
                    }*/
                    
                    $start_date = date('Y-m-d', strtotime($start_date));

                    for ($i = 0; $i < $total_session; $i++) {

                        if ($next) {
                            $session_date = date('Y-m-d', strtotime($start_date . ' next ' . $day));
                        } else {
                            $session_date = $start_date;
                        }
                        $start_date = $session_date;
                        $next = true;

                        $start = $selected_time;
                        $end = date('H:i', strtotime($selected_time . " +" . $session_minute . " minute"));


                        $get_hours = self::get_available_hours($tutor_uuid, $tutor_working_plan, $session_date, $session_minute, $student_uuid);
//echo "<pre>";print_r($get_hours);
                        $skip = (!in_array($selected_time, $get_hours)) ? true : false;
                        if ($skip) {
                            if ($skip_count == 0) {
                                //$return['message'] = "This time period is not available in ". ucfirst($day);
                                $return['message'] = "Your selected tutor is not available for one or more of the selected times/days. ". ucfirst($day). " ".General::displayTime($start);
                                break;
                            }
                            $skip_count--;
                            $i--;
                            continue;
                        }
                        $current_session = ['date' => General::displayDate($session_date), 'day' => $day, 'start' => $start, 'end' => $end, 'time_array' => $get_hours];
                        $session_array[] = $current_session;
                    }
                }
            }
        }
        $return['session_when_start'] = (!empty($session_array)) ? $session_array[0]['date'] : '';
        $return['session_array'] = $session_array;
        return $return;
    }

    public function is_tutor_available_perticular_date_time($tutor_uuid, $tutor_working_plan, $session_date, $session_minute, $selected_time = "00:00") {

        // Check if the provider is available for the requested date.
        $empty_periods = self::get_tutor_available_time_period($tutor_uuid, $tutor_working_plan, $session_date);

        $available_hours = self::_calculate_available_hours($empty_periods, $session_date, $session_minute, FALSE, 'flexible');

        return (in_array($selected_time, $selected_time)) ? true : false;
    }

    public function get_available_hours($tutor_uuid, $tutor_working_plan, $session_date, $session_minute, $student_uuid='') {

        $session_date = date('Y-m-d', strtotime($session_date));
        $empty_periods = self::get_tutor_available_time_period($tutor_uuid, $tutor_working_plan, $session_date, $student_uuid);
        $available_hours = self::_calculate_available_hours($empty_periods, $session_date, $session_minute, filter_var(false, FILTER_VALIDATE_BOOLEAN), 'flexible');
        // If the selected date is today, remove past hours. It is important  include the timeout before
        // booking that is set in the back-office the system. Normally we might want the customer to book
        // an appointment that is at least half or one hour from now. The setting is stored in minutes.
        if (date('Y-m-d', strtotime($session_date)) === date('Y-m-d')) {
            $book_advance_timeout = Yii::$app->params['book_advance_timeout'];

            foreach ($available_hours as $index => $value) {
                $available_hour = strtotime($value);
                $current_hour = strtotime('+' . $book_advance_timeout . ' minutes', strtotime('now'));
                if ($available_hour <= $current_hour) {
                    unset($available_hours[$index]);
                }
            }
        }

        $available_hours = array_values($available_hours);
        sort($available_hours, SORT_STRING);
        $available_hours = array_values($available_hours);

        return $available_hours;
    }

    public function get_tutor_available_time_period($tutor_uuid, $working_plan, $selected_date,$student_uuid='') {

        $selected_date_working_plan = $working_plan[strtolower(date('l', strtotime($selected_date)))];
        $selected_date_working_plan['breaks'] = (isset($selected_date_working_plan['breaks'])) ? $selected_date_working_plan['breaks'] : [];

        $periods = [];
        if (isset($selected_date_working_plan['breaks']) && isset($selected_date_working_plan['start'])) {
            $periods[] = [
                'start' => $selected_date_working_plan['start'],
                'end' => $selected_date_working_plan['end']
            ];

            $day_start = new \DateTime($selected_date_working_plan['start']);
            $day_end = new \DateTime($selected_date_working_plan['end']);

            // Split the working plan to available time periods that do not contain the breaks in them.
            foreach ($selected_date_working_plan['breaks'] as $index => $break) {
                $break_start = new \DateTime($break['start']);
                $break_end = new \DateTime($break['end']);

                if ($break_start < $day_start) {
                    $break_start = $day_start;
                }

                if ($break_end > $day_end) {
                    $break_end = $day_end;
                }

                if ($break_start >= $break_end) {
                    continue;
                }

                foreach ($periods as $key => $period) {
                    $period_start = new \DateTime($period['start']);
                    $period_end = new \DateTime($period['end']);

                    $remove_current_period = FALSE;

                    if ($break_start > $period_start && $break_start < $period_end && $break_end > $period_start) {
                        $periods[] = [
                            'start' => $period_start->format('H:i'),
                            'end' => $break_start->format('H:i')
                        ];

                        $remove_current_period = TRUE;
                    }

                    if ($break_start < $period_end && $break_end > $period_start && $break_end < $period_end) {
                        $periods[] = [
                            'start' => $break_end->format('H:i'),
                            'end' => $period_end->format('H:i')
                        ];

                        $remove_current_period = TRUE;
                    }
                    
                    if ($break_start == $period_start && $break_end == $period_end ) {
                        $periods[] = [
                            'start' => $period_start->format('H:i'),
                            'end' => $break_start->format('H:i')
                        ];

                        $remove_current_period = TRUE;
                    }

                    if ($remove_current_period) {
                        unset($periods[$key]);
                    }
                }
            }
            
        }
        // Break the empty periods with the reserved booked session.
        $tutor_appointments = self::get_tutor_booked_appointments($tutor_uuid);
        $tutor_unavailability = self::get_tutor_unavailability($tutor_uuid);
        $student_appointments = (!empty($student_uuid)) ? self::get_student_booked_appointments($student_uuid) : [];
        
        $provider_appointments_unavailability = array_merge($tutor_appointments,$tutor_unavailability);
        $provider_appointments = array_merge($provider_appointments_unavailability,$student_appointments);
        //echo "<pre>provider_appointments";print_r($provider_appointments);exit;
        /*
          $provider_appointments = Array
          (
          [0] => Array
          (
          [id] => 10
          [book_datetime] => 2018-08-11 16:39:53
          [start_datetime] => 2018-08-17 16:00:00
          [end_datetime] => 2018-08-17 17:00:00
          [notes] => Custom break
          [hash] =>
          [is_unavailable] => 1
          [id_users_provider] => 2
          [id_users_customer] =>
          [id_services] =>
          [id_google_calendar] =>
          )

          )
         */
        foreach ($provider_appointments as $provider_appointment) {
            foreach ($periods as $index => &$period) {

                //Alpesh After session rest minute include
                $appointment_between_gap_min = (isset($provider_appointment['is_tutor_unavailability']) && $provider_appointment['is_tutor_unavailability'] == 'Yes') ? 0 : 30;
                //$appointment_between_gap_min = 30;
                $provider_appointment['end_datetime'] = date('Y-m-d H:i:s', strtotime($provider_appointment['end_datetime'] . " +" . $appointment_between_gap_min . " minutes"));

                $appointment_start = new \DateTime($provider_appointment['start_datetime']);
                $appointment_end = new \DateTime($provider_appointment['end_datetime']);
                $period_start = new \DateTime($selected_date . ' ' . $period['start']);
                $period_end = new \DateTime($selected_date . ' ' . $period['end']);

                if ($appointment_start <= $period_start && $appointment_end <= $period_end && $appointment_end <= $period_start) {
                    // The appointment does not belong in this time period, so we  will not change anything.
                } else {
                    if ($appointment_start <= $period_start && $appointment_end <= $period_end && $appointment_end >= $period_start) {
                        // The appointment starts before the period and finishes somewhere inside. We will need to break
                        // this period and leave the available part.
                        $period['start'] = $appointment_end->format('H:i');
                    } else {
                        if ($appointment_start >= $period_start && $appointment_end < $period_end) {
                            // The appointment is inside the time period, so we will split the period into two new
                            // others.
                            unset($periods[$index]);

                            $periods[] = [
                                'start' => $period_start->format('H:i'),
                                'end' => $appointment_start->format('H:i')
                            ];

                            $periods[] = [
                                'start' => $appointment_end->format('H:i'),
                                'end' => $period_end->format('H:i')
                            ];
                        } else if ($appointment_start == $period_start && $appointment_end == $period_end) {
                            unset($periods[$index]); // The whole period is blocked so remove it from the available periods array.
                        } else {
                            if ($appointment_start >= $period_start && $appointment_end >= $period_start && $appointment_start <= $period_end) {
                                // The appointment starts in the period and finishes out of it. We will need to remove
                                // the time that is taken from the appointment.
                                $period['end'] = $appointment_start->format('H:i');
                            } else {
                                if ($appointment_start >= $period_start && $appointment_end >= $period_end && $appointment_start >= $period_end) {
                                    // The appointment does not belong in the period so do not change anything.
                                } else {
                                    if ($appointment_start <= $period_start && $appointment_end >= $period_end && $appointment_start <= $period_end) {
                                        // The appointment is bigger than the period, so this period needs to be removed.
                                        unset($periods[$index]);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $result = array_values($periods);
        //echo "<pre>provider_appointments";print_r($result);//exit;
        // $result = [['start' => "09:00",'end'=>'18:00']];
        return $result;
    }

    protected function _calculate_available_hours(
    array $empty_periods, $selected_date, $service_duration, $manage_mode = FALSE, $availabilities_type = 'flexible'
    ) {


        $available_hours = [];

        foreach ($empty_periods as $period) {
            $start_hour = new \DateTime($selected_date . ' ' . $period['start']);
            $end_hour = new \DateTime($selected_date . ' ' . $period['end']);
            $interval = $availabilities_type === Yii::$app->params['availabilities_type_fixed'] ? (int) $service_duration : 30;

            $current_hour = $start_hour;
            $diff = $current_hour->diff($end_hour);

            while (($diff->h * 60 + $diff->i) >= intval($service_duration)) {
                $available_hours[] = $current_hour->format('H:i');
                $current_hour->add(new \DateInterval('PT' . $interval . 'M'));
                $diff = $current_hour->diff($end_hour);
            }
        }

        return $available_hours;
    }

    public function get_tutor_booked_appointments($tutor_uuid) {

        $model = BookedSession::find()->where(['tutor_uuid' => $tutor_uuid, 'status' => 'SCHEDULE'])->asArray()->all();
        if (!empty($model)) {
            return $model;
        } else {
            return [];
        }
    }
    
    public function get_tutor_unavailability($tutor_uuid, $only_future = true) {
        
        $where = ($only_future) ? ' AND ((to_char(start_datetime, \'YYYY-MM-DD\') >= \''.date('Y-m-d').'\') OR  (to_char(end_datetime, \'YYYY-MM-DD\') >= \''.date('Y-m-d').'\'))' : "";
        $query = 'SELECT *, \'Yes\' AS "is_tutor_unavailability" FROM "tutor_unavailability" WHERE ("tutor_uuid"= \''.$tutor_uuid.'\') '.$where;
        $command = Yii::$app->db->createCommand($query);
        $model = $command->queryAll();
        
        if (!empty($model)) {
            return $model;
        } else {
            return [];
        }
    }

    public function get_student_booked_appointments($student_uuid) {

        $model = BookedSession::find()->where(['student_uuid' => $student_uuid, 'status' => 'SCHEDULE'])->asArray()->all();
        if (!empty($model)) {
            return $model;
        } else {
            return [];
        }
    }

    public function check_coupon_available($coupon_code = "", $student_uuid = "") {

        $return = ['is_available' => false, 'message' => 'This Promo code not available.', 'discount' => 0,'coupon_code' => $coupon_code, 'coupon_uuid' => ''];
        $model = Coupon::find()->where(['code' => $coupon_code, 'status' => 'ENABLED'])->one();
        if (!empty($model)) {
            $today = date('Y-m-d');
            $start = date('Y-m-d', strtotime($model->start_date));
            $end = date('Y-m-d', strtotime($model->end_date));
            $amount = $model->amount;

            if ($today >= $start && $today <= $end) {
                $return = ['is_available' => true, 'message' => 'This Coupon apply successfully.', 'discount' => $amount, 'coupon_uuid' => $model->uuid];
            }else{
                $return['message'] = "This coupon is expired or not available.";
            }
        }
        return $return;
    }

    public function make_cart_submit_array($postdata = []) {
        
        $stripe_array = [];
        $student_detail = Student::findOne(Yii::$app->user->identity->student_uuid);
        if (isset($postdata['StudentTuitionBooking'])) {
            
            $stripe_array = ['stripeToken' => $postdata['stripeToken']];
            $step_data = StudentTuitionBooking::getSession(Yii::$app->user->identity->student_uuid);
            $cart_total = 0;
            $discount = 0;
            $monthly_sub_total = 0;
            
            if($student_detail->isMonthlySubscription == 1){
                $monthly_sub_check = '0';
            }else{
                $monthly_sub_check = $postdata['StudentTuitionBooking']['monthly_sub_checkbox'];
            }
            
            $monthly_sub_uuid = $postdata['StudentTuitionBooking']['monthly_sub_uuid'];
            $coupon_code = $postdata['StudentTuitionBooking']['coupon_code'];

            $planDetail = OnlineTutorialPackage::find()->where(['uuid' => $step_data['step_2']['onlinetutorial_uuid']])->one();
            $plan_price = $planDetail->total_price;

            if ($monthly_sub_check == '1') {

                $monthly_model = MonthlySubscriptionFee::find()->where(['with_tutorial' => 1, 'status' => 'ENABLED'])->one();
                if (!empty($monthly_model)) {
                    $monthly_sub_total = $monthly_model->total_price;
                }
            }

            $model_coupon = StudentTuitionBooking::check_coupon_available($coupon_code);
            if ($model_coupon['is_available']) {
                $discount = $model_coupon['discount'];
            }

            $cart_total = (($planDetail->total_price + $monthly_sub_total ) - $discount);

            $stripe_array['monthly_sub_checkbox'] = $monthly_sub_check;
            $stripe_array['coupon_code'] = $coupon_code;
            $stripe_array['coupon_uuid'] = $postdata['StudentTuitionBooking']['coupon_uuid'];
            $stripe_array['monthly_sub_uuid'] = $monthly_sub_uuid;
            $stripe_array['monthly_sub_total'] = $monthly_sub_total;
            $stripe_array['discount'] = $discount;
            $stripe_array['cart_total'] = $cart_total;
            $stripe_array['card_name'] = $postdata['StudentTuitionBooking']['cc_name'];
            $stripe_array['card_number'] = $postdata['StudentTuitionBooking']['cc_number'];
            $stripe_array['card_cvv'] = $postdata['StudentTuitionBooking']['cc_cvv'];
            $stripe_array['card_month'] = date('m', strtotime($postdata['StudentTuitionBooking']['cc_month_year']));
            $stripe_array['card_year'] = date('Y', strtotime($postdata['StudentTuitionBooking']['cc_month_year']));
            $stripe_array['cc_checkbox'] = (isset($postdata['StudentTuitionBooking']['cc_checkbox'])) ? $postdata['StudentTuitionBooking']['cc_checkbox'] : 0;
            $stripe_array['plan_name'] = $planDetail->name;
            $stripe_array['plan_price'] = $plan_price;
        }
        return $stripe_array;
    }

    public function stripePayment($stripe_array) {

        $return = ['code' => 421, 'message' => 'Something went wrong.'];
        $student = Student::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid])->one();
        $step_data = StudentTuitionBooking::getSession(Yii::$app->user->identity->student_uuid);

        if (!empty($stripe_array['stripeToken'])) {

            $token = $stripe_array['stripeToken'];
            $email = $student->email;

            $dollar_amount = (int) $stripe_array['cart_total'];
            $cents_amount = $dollar_amount * 100;   // 1 AUD = 100 cents
            //include Stripe PHP library
            require Yii::$app->basePath . '/vendor/stripe/stripe-php/init.php';

            //set api key
            $stripe = array(
                "secret_key" => Yii::$app->params['stripe']['secret_key'],
                "publishable_key" => Yii::$app->params['stripe']['publishable_key']
            );

            try {
                \Stripe\Stripe::setApiKey($stripe['secret_key']);
                $customer = \Stripe\Customer::create(array(
                            'email' => $email,
                            'source' => $token
                ));

                //item information
                $itemName = $stripe_array['plan_name'];
                $itemDescription = ($stripe_array['monthly_sub_checkbox'] == '1') ? $stripe_array['plan_name'] . " with Monthly Subscription" : $stripe_array['plan_name'];
                $itemPrice = $cents_amount;
                $currency = "aud";
//                $orderID = mt_rand(100000, 999999);
                $orderID = General::generateTransactionID();

                //charge a credit or a debit card
                $charge = \Stripe\Charge::create(array(
                            'customer' => $customer->id,
                            'amount' => $itemPrice,
                            'currency' => $currency,
                            'description' => $itemDescription,
                            'metadata' => array(
                                'order_id' => $orderID
                            )
                ));
            } catch (\Stripe\Error\ApiConnection $e) {
                return $return = ['code' => 421, 'message' => 'Network problem, perhaps try again.'];
                //Yii::$app->session->setFlash('danger', Yii::t('app', 'Network problem, perhaps try again.'));
                //return \yii\web\Controller::redirect(['index']);
            } catch (\Stripe\Error\InvalidRequest $e) {
                return $return = ['code' => 421, 'message' => 'Your request is invalid.'];
                //Yii::$app->session->setFlash('danger', Yii::t('app', 'Your request is invalid.'));
                //return \yii\web\Controller::redirect(['index']);
            } catch (\Stripe\Error\Api $e) {
                return $return = ['code' => 421, 'message' => 'Stripe servers are down!'];
                //Yii::$app->session->setFlash('danger', Yii::t('app', 'Stripe servers are down!'));
                //return \yii\web\Controller::redirect(['index']);
            } catch (\Stripe\Error\Card $e) {
                return $return = ['code' => 421, 'message' => 'This Card is Declined.'];
                //Yii::$app->session->setFlash('danger', Yii::t('app', 'This Card is Declined.'));
                //return \yii\web\Controller::redirect(['index']);
            }

            $chargeJson = $charge->jsonSerialize();
            
//echo "<pre>chargeJson";
//print_r($chargeJson);exit;

            if ($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1) 
            {
                
                $planDetail = OnlineTutorialPackage::find()->joinWith(['paymentTermUu', 'tutorialTypeUu'])->where(['online_tutorial_package.uuid' => $step_data['step_2']['onlinetutorial_uuid']])->one();
                $booking_uuid = NULL;
                $save_m_s_flag = false;
                
                //Transaction_payment
                $modelPayment = new PaymentTransaction();
                $modelPayment->student_uuid = Yii::$app->user->identity->student_uuid;
                $modelPayment->transaction_number = $orderID;
                $modelPayment->payment_datetime = date("Y-m-d H:i:s");
                $modelPayment->amount = $chargeJson['amount'] / 100;
                $modelPayment->payment_method = 'Stripe';
                $modelPayment->reference_number = $chargeJson['balance_transaction'];
                $modelPayment->status = $chargeJson['status'];
                $modelPayment->book_uuid = $booking_uuid;
                $modelPayment->response = json_encode($chargeJson);
                $modelPayment->save(false);
                
                //student_tuition_booking table entry
                $modelSTBooking = new StudentTuitionBooking();
                $modelSTBooking->booking_id = General::generateBookingID();  
                $modelSTBooking->booking_datetime = date('Y-m-d H:i:s');
                $modelSTBooking->student_uuid = Yii::$app->user->identity->student_uuid;  
                $modelSTBooking->tutor_uuid = $step_data['step_3']['tutor_uuid'];  
                $modelSTBooking->instrument_uuid = $step_data['step_1']['instrument_uuid'];  
                $modelSTBooking->tutorial_name = $planDetail->name;  
                $modelSTBooking->tutorial_description = $planDetail->description;  
                $modelSTBooking->tutorial_type_code = $planDetail->tutorialTypeUu->code; 
                $modelSTBooking->tutorial_min = $planDetail->tutorialTypeUu->min; 
                $modelSTBooking->payment_term_code = $planDetail->paymentTermUu->code; 
                $modelSTBooking->total_session = $planDetail->duration; 
                $modelSTBooking->session_length_day = $planDetail->tutorialTypeUu->days;  
                $modelSTBooking->price = $planDetail->price;  
                $modelSTBooking->gst = $planDetail->gst;  
                $modelSTBooking->gst_description = $planDetail->gst_description;  
                $modelSTBooking->total_price = $planDetail->total_price;  
                $modelSTBooking->status = "BOOKED";  
                $modelSTBooking->monthly_subscription_price = $stripe_array['monthly_sub_total'];  
                $modelSTBooking->student_monthly_subscription_uuid =  NULL;  
                $modelSTBooking->coupon_uuid = (!empty($stripe_array['coupon_uuid'])) ? $stripe_array['coupon_uuid'] : NULL ;  
                $modelSTBooking->discount = $stripe_array['discount'];  
                $modelSTBooking->grand_total = $chargeJson['amount'] / 100; 
                if($modelSTBooking->save(false)){
                 
                    //Booked Session entry
                    $session_date_array = explode(',', $step_data['step_4']['all_dates']);
                    
                    //update Transaction Booking id
                    $modelPaymentUpdate = PaymentTransaction::find()->where(['uuid' => $modelPayment->getPrimaryKey()])->one();
                    $modelPaymentUpdate->book_uuid = $modelSTBooking->getPrimaryKey();
                    $modelPaymentUpdate->save(false);
                    
                    if($modelSTBooking->total_session > 0){
                         
                        for($i=0; $i < $modelSTBooking->total_session; $i++){
                            
                            
                            $start_hour = $step_data['step_4']['start_hour'];
                            $start_datetime = date('Y-m-d H:i:s', strtotime($session_date_array[$i]." ".$start_hour));
                            
                            $end_datetime = date('Y-m-d H:i:s', strtotime($start_datetime." +". $modelSTBooking->tutorial_min ." minutes"));
                            $digits = 4;
                            $pin = str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);
                            
                            $modelBookedSession = new BookedSession();
                            $modelBookedSession->booking_uuid = $modelSTBooking->getPrimaryKey();
                            $modelBookedSession->student_uuid = $modelSTBooking->student_uuid;
                            $modelBookedSession->tutor_uuid = $modelSTBooking->tutor_uuid;
                            $modelBookedSession->instrument_uuid = $modelSTBooking->instrument_uuid;
                            $modelBookedSession->tutorial_type_code = $modelSTBooking->tutorial_type_code;
                            $modelBookedSession->start_datetime = $start_datetime;
                            $modelBookedSession->end_datetime = $end_datetime;
                            $modelBookedSession->session_min = $modelSTBooking->tutorial_min;
                            $modelBookedSession->session_pin = $pin;
                            $modelBookedSession->status = 'SCHEDULE';
                            $modelBookedSession->save(false);
                        }
                    }
                    
                    
                    //cc save or not
                    if($stripe_array['cc_checkbox'] == 1){
                        $modelCC = StudentCreditCard::find()->where(['student_uuid' => $modelSTBooking->student_uuid, 'status' => 'ENABLED'])->one();
                        if(empty($modelCC)){
                            $modelCC = new StudentCreditCard();
                        }
                            $modelCC->student_uuid = $modelSTBooking->student_uuid;
                            $modelCC->number = General::encrypt_str($stripe_array['card_number']);
                            $modelCC->cvv = General::encrypt_str($stripe_array['card_cvv']);
                            $modelCC->expire_date = date('Y-m-t', strtotime($stripe_array['card_year']."-".$stripe_array['card_month']."-01"));
                            $modelCC->name = $stripe_array['card_name'];
                            $modelCC->status = 'ENABLED';
                            $modelCC->stripe_customer_id = $chargeJson['customer'];
                            $modelCC->save(false);
                    }
                    
                    //if Monthly subscription plan then entry in it.
                    if($stripe_array['monthly_sub_checkbox'] == 1){
                        
                        $MonthlySub = MonthlySubscriptionFee::find()->where(['uuid' => $stripe_array['monthly_sub_uuid'] ])->one();
                        
                        $modelStudentSubscription = StudentSubscription::find()->where(['student_uuid' => $modelSTBooking->student_uuid])->one();
                        
                        $start_date = date('Y-m-d H:i:s');
                        if($student->isMonthlySubscription == 1 ){
                            if(!empty($modelStudentSubscription)){
                                $start_date = date('Y-m-d H:i:s', strtotime($modelStudentSubscription->next_renewal_date));
                            }
                        }
                        
                        //$start_date = (!empty($modelStudentSubscription)) ? date('Y-m-d H:i:s', strtotime($modelStudentSubscription->next_renewal_date)) : date('Y-m-d H:i:s');
                        $end_date = date('Y-m-d H:i:s', strtotime($start_date." +30 days"));
                        $next_renewal_date = date('Y-m-d', strtotime($end_date." +1 days"));
                        
                        $modelStudentMonthlySub = new StudentMonthlySubscription();
                        $modelStudentMonthlySub->student_uuid = $modelSTBooking->student_uuid;
                        $modelStudentMonthlySub->name = $MonthlySub->name;
                        $modelStudentMonthlySub->price = $MonthlySub->price;
                        $modelStudentMonthlySub->tax = $MonthlySub->tax;
                        $modelStudentMonthlySub->total_price = $MonthlySub->total_price;
                        $modelStudentMonthlySub->description = $MonthlySub->description;
                        $modelStudentMonthlySub->with_tutorial = $MonthlySub->with_tutorial;
                        $modelStudentMonthlySub->status = 'ENABLED';
                        $modelStudentMonthlySub->start_datetime = $start_date;
                        $modelStudentMonthlySub->end_datetime = $end_date;
                        if($modelStudentMonthlySub->save(false)){
                            
                            $up_st_booking = StudentTuitionBooking::findOne($modelSTBooking->getPrimaryKey());
                            if(!empty($up_st_booking)){
                                
                                $up_st_booking->student_monthly_subscription_uuid = $modelStudentMonthlySub->uuid;
                                $up_st_booking->save(false);
                                
                                $modelSTBooking->student_monthly_subscription_uuid =  $modelStudentMonthlySub->uuid;
                            }
                            
                            $save_m_s_flag = true;                            
                        }
                        $monthly_item = [ 'item' => $modelStudentMonthlySub->name, 'booking_uuid' => NULL, 'student_monthly_subscription_uuid' => $modelStudentMonthlySub->uuid,'tax' => $modelStudentMonthlySub->tax,'price'=>$modelStudentMonthlySub->price,'total' => $modelStudentMonthlySub->total_price];
                        
                        if(empty($modelStudentSubscription)){
                            $modelStudentSubscription = new StudentSubscription();
                        }
                            $modelStudentSubscription->student_uuid = $modelSTBooking->student_uuid;
                            $modelStudentSubscription->subscription_uuid = $modelStudentMonthlySub->uuid;
                            $modelStudentSubscription->next_renewal_date = $next_renewal_date;
                            $modelStudentSubscription->save(false);
                            
                            //update student monthly sub flag
                            $student->isMonthlySubscription = 1;
                            $student->isCancelMonthlySubscriptionRequest = 0;
                            $student->save(false);
                            
                            
                    }
                    
                    $item_array[0] = [ 'item' => $modelSTBooking->tutorial_name, 'booking_uuid' => $modelSTBooking->uuid, 'student_monthly_subscription_uuid' => NULL,'tax' => $modelSTBooking->gst,'price'=>$modelSTBooking->price,'total' => $modelSTBooking->total_price];
                    $order_total = $item_array[0]['total'];
                    if($stripe_array['monthly_sub_checkbox'] == 1 && isset($monthly_item)){
                        $item_array[1] = $monthly_item;
                        $order_total = $order_total + $monthly_item['total'];
                    }
                    $order_array['student_uuid'] = $modelSTBooking->student_uuid;
                    $order_array['payment_uuid'] = $modelPayment->getPrimaryKey();
                    $order_array['discount'] = $modelSTBooking->discount;
                    $order_array['item'] = $item_array;
                    $order_array['total'] = $order_total;
                    $order_array['grand_total'] = $order_total - $order_array['discount'];
                    
                    //create order
                    $order_result = StudentTuitionBooking::generate_order($order_array);
                                        
                    self::bookedTutionMail($modelSTBooking->student_uuid,$modelSTBooking->getPrimaryKey(), $order_result, $save_m_s_flag);
                    
                    if($save_m_s_flag){
                        //StudentMonthlySubscription::MonthlySubscriptionBuyMail($modelStudentMonthlySub,$modelSTBooking->student_uuid);
                    }
                }
                
                return $return = ['code' => 200, 'message' => 'success'];
                
            } else {
                return $return = ['code' => 421, 'message' => 'Transaction has been failed.'];
                //Yii::$app->session->setFlash('success', Yii::t('app', 'Transaction has been failed.'));
                //return \yii\web\Controller::redirect(['index']);
            }
        } else {
            return $return = ['code' => 421, 'message' => 'Form submission error.'];
            //Yii::$app->session->setFlash('success', Yii::t('app', 'Form submission error.'));
            //return \yii\web\Controller::redirect(['index']);
        }
        
        return $return;
    }

    public function bookedTutionMail($student_uuid,$booking_uuid,$order_array,$ismonthlysubscription = false) {
        
        if(!empty($student_uuid) && !empty($booking_uuid)){
            
            $modelSTBooking = StudentTuitionBooking::find()->where(['uuid' => $booking_uuid])->one();
            $modelStudent = Student::find()->where(['student_uuid' => $student_uuid])->one();
            if(!empty($modelSTBooking)){
                
                $modelTutor = Tutor::find()->where(['uuid' => $modelSTBooking->tutor_uuid])->one();
                $modelInstrument = Instrument::find()->where(['uuid' => $modelSTBooking->instrument_uuid])->one();
                
                $session_dates = [];
                $session_time = '';
                $modelAllTusion = BookedSession::find()->where(['booking_uuid' => $booking_uuid])->all();
                if(!empty($modelAllTusion)){
                    foreach ($modelAllTusion as $key => $value) {
                        $session_dates[] = General::displayDate($value->start_datetime);
                        $session_time = General::displayTime($value->start_datetime).' - '.General::displayTime($value->end_datetime);
                    }
                }
                
                $tutor_name = $modelTutor->first_name;
                $student_name = $modelStudent->first_name;
                $package_payment_term = '';
                if($modelSTBooking->payment_term_code == 'Q'){
                    $package_payment_term = 'Quarterly';
                } elseif($modelSTBooking->payment_term_code == 'M'){
                    $package_payment_term = 'Monthly';
                } elseif($modelSTBooking->payment_term_code == 'F'){
                    $package_payment_term = 'Fortnightly';
                }
                
                $content['student_name'] = $student_name;
                $content['booking_id'] = $modelSTBooking->booking_id;
                $content['tutor_name'] = $tutor_name;
                $content['tutorial_name'] = $modelSTBooking->tutorial_name;
                $content['package_payment_term'] = $package_payment_term;
                $content['instrument'] = $modelInstrument->name;
                $content['tution_duration'] = $modelSTBooking->tutorial_min;
                $content['time'] = $session_time;
                $content['session_dates'] = $session_dates;
                $content['monthly_subscription_price'] = $modelSTBooking->monthly_subscription_price;
                $content['student_monthly_subscription_uuid'] = $modelSTBooking->student_monthly_subscription_uuid;
                $content['grand_total'] = $modelSTBooking->grand_total;
                $content['total_price'] = $modelSTBooking->total_price;
                $content['discount'] = $modelSTBooking->discount;
                $content['order_id'] = (isset($order_array['order_id'])) ? "#".$order_array['order_id'] : "";
                $isInvoiceFile = (isset($order_array['order_file'])) ? $order_array['order_file'] : "";
        
                $content_tutor['tutor_name'] = $tutor_name;
                $content_tutor['student_name'] = $student_name;
                $content_tutor['booking_id'] = $modelSTBooking->booking_id;
                $content_tutor['tutorial_name'] = $modelSTBooking->tutorial_name;
                $content_tutor['tution_duration'] = $modelSTBooking->tutorial_min;
                $content_tutor['instrument'] = $modelInstrument->name;
                $content_tutor['time'] = $session_time;
                $content_tutor['session_dates'] = $session_dates;
                $content_tutor['student_monthly_subscription_uuid'] = $modelSTBooking->student_monthly_subscription_uuid;
                
                $ownersEmail = General::getOwnersEmails(['OWNER','ADMIN']);
                
                $mail = Yii::$app->mailer->compose('booked_tution_student_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                        ->setFrom(Yii::$app->params['supportEmail'])
                        ->setTo($modelStudent->email)
                        ->setBcc($ownersEmail)
                        ->setSubject(ucfirst(strtolower(Yii::$app->name)) . ' Notification:  Confirmation of Payment and Booking – Receipt '.$content['order_id'] );
                
                        if (is_file($isInvoiceFile)) {
                            $mail->attach($isInvoiceFile);
                        }
                        $mail->send();
                
                
                Yii::$app->mailer->compose('booked_tution_tutor_mail', ['content' => $content_tutor], ['htmlLayout' => 'layouts/html'])
                        ->setFrom(Yii::$app->params['supportEmail'])
                        ->setTo($modelTutor->email)
                        ->setBcc($ownersEmail)
                        ->setSubject('New Tutorial Booking' )
                        ->send();
                
            }
        }
        
        return true;
    }
    
    public function generate_order($order_array) {
        
        if(!empty($order_array)){
            
            $modelOrder = new Order();
            $modelOrder->order_id = General::generateOrderID();
            $modelOrder->student_uuid = $order_array['student_uuid'];
            $modelOrder->payment_uuid = $order_array['payment_uuid'];
            $modelOrder->order_datetime = date('Y-m-d H:i:s');
            $modelOrder->total = $order_array['total'];
            $modelOrder->discount = $order_array['discount'];
            $modelOrder->grand_total = $order_array['grand_total'];
            if($modelOrder->save(false)){
                
                if(!empty($order_array['item'])){
                    foreach ($order_array['item'] as $key => $item) { 
                        
                        $modelOrderItem = new OrderItem();
                        $modelOrderItem->order_uuid = $modelOrder->getPrimaryKey();
                        $modelOrderItem->item = $item['item'];
                        $modelOrderItem->booking_uuid = $item['booking_uuid'];
                        $modelOrderItem->student_monthly_subscription_uuid = $item['student_monthly_subscription_uuid'];
                        $modelOrderItem->price = $item['price'];
                        $modelOrderItem->tax = $item['tax'];
                        $modelOrderItem->total = $item['total'];
                        $modelOrderItem->save(false);
                        
                        if(!empty($item['booking_uuid'])){
                            $modelSTBooking = StudentTuitionBooking::find()->where(['uuid' => $item['booking_uuid']])->one();
                            if(!empty($modelSTBooking)){
                                $modelSTBooking->order_uuid = $modelOrder->getPrimaryKey();
                                $modelSTBooking->save(false);
                            }
                        }
                        
                        if(!empty($item['student_monthly_subscription_uuid'])){
                            $modelStudentMonthlySub = StudentMonthlySubscription::find()->where(['uuid' => $item['student_monthly_subscription_uuid'] ])->one();
                            if(!empty($modelStudentMonthlySub)){
                                $modelStudentMonthlySub->order_uuid = $modelOrder->getPrimaryKey();
                                $modelStudentMonthlySub->save(false);
                            }
                        }
                    }
                }
            }
            
            $filename = 'Invoice_'.$modelOrder->order_id.'.pdf';
            $pdf_res = self::create_pdf($filename,$modelOrder->getPrimaryKey());
            
            $modelStudent = Student::find()->where(['student_uuid' => $order_array['student_uuid']])->one();
            
            $content['student_name'] = $modelStudent->first_name;
            $content['order_id'] = $modelOrder->order_id;
            $content['order_array'] = $order_array;
            $content['order_date'] = General::displayDate($modelOrder->order_datetime);
            $content['order_file'] = $pdf_res['file'];
            
            /*$ownersEmail = General::getOwnersEmails(['OWNER']);
                
            $mail = Yii::$app->mailer->compose('new_order', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                    ->setFrom(Yii::$app->params['supportEmail'])
                    ->setTo($modelStudent->email)
                    ->setBcc($ownersEmail)
                    ->setSubject('New Order - ' . Yii::$app->name);
            
                    if (is_file($pdf_res['file'])) {
                        $mail->attach($pdf_res['file']);
                    }
                    $mail->send();*/
                    
             return $content;
        }
        return [];
    }
    
    /**
    * This function use generate PDF file for Invoice.
    * 
    */
    public function create_pdf($filename = '', $order_uuid) {

        $file = Yii::$app->params['media']['invoice']['path'] . $filename;
        
        $model = \app\models\Order::findOne($order_uuid);
            
        if (!empty($model)) {

            $order_item = \app\models\OrderItem::find()->where(['order_uuid' => $order_uuid])->asArray()->all();

            $content = Yii::$app->controller->renderPartial('//order/invoice_pdf', ['model' => $model, 'order_item' => $order_item]);
            
            $filename = ( $filename == '') ? 'Invoice_' . $model->order_id . '.pdf' : $filename;
            $file = Yii::$app->params['media']['invoice']['path'] . $filename;
            
            if (!is_dir(Yii::$app->params['media']['invoice']['path'])) {
                FileHelper::createDirectory(Yii::$app->params['media']['invoice']['path']);
            }
            
            $css_content = file_get_contents(Yii::$app->params['theme_assets']['url'].'plugins/bootstrap/css/bootstrap.min.css' );
            $css_content .= file_get_contents(Yii::$app->params['theme_assets']['url'].'css/style.css' );
            $css_content .= ".invoice-title{width: 20%;}.invoice-head-info{width: 20%;font-size: 14px !important;}..invoice-head-info span{font-size: 14px !important;} .invoice-head-info{width:20%;font-family: 'Open Sans', Arial, Helvetica, sans-serif;line-height: 23px;font-style: normal;font-weight: normal;font-size: 10px !important;} .invoice-logo{max-width: 20%;}";
            
            $params = [
                'mode' => Pdf::MODE_CORE,
                'format' => Pdf::FORMAT_A4,
                'orientation' => Pdf::ORIENT_PORTRAIT,
                'destination' => Pdf::DEST_FILE,
                'content' => $content,
                'filename' => Yii::$app->params['media']['invoice']['path'] . $filename,
                'tempPath' => 'mpdf/',
//                'cssFile' => '@web/theme_assets/css/style.css',
                'cssInline' => '.kv-heading-1{font-size:12px}',
                'cssInline' => $css_content,
                'options' => ['title' => 'Invoice'],
                'methods' => [
                    'SetHeader' => ['MUSICONN'],
                    'SetFooter' => ['{PAGENO}'],
                ]
            ];
            $pdf = new Pdf($params);
            $pdf->render();
            
            if (is_file($file)) {
                
                $model->invoice_file = $filename;
                $model->save(false);
                
                return ['status' => '1', 'file' => $file, 'filename' => $filename];
            } else {
                return ['status' => '0', 'file' => $file, 'filename' => $filename];
            }
        } else {
            return ['status' => '0', 'file' => $file, 'filename' => $filename];
        }
    }
    
    public function bookCreditTution($step_data = [], $student_uuid = '') {
        
        $return = ['code' => 421, 'message' => 'Something went wrong.'];
        if( !empty($step_data['step_1']['instrument_uuid'])
            && !empty($step_data['step_2']['tutorial_type_code'])
            && !empty($step_data['step_3']['tutor_uuid'])
            && !empty($step_data['step_4']['all_dates'])
            && !empty($step_data['step_4']['start_hour'])
            && !empty($student_uuid)
        ){
            
            $modelCreditTution = CreditSession::find()->where(['student_uuid' => $student_uuid, 'tutorial_type_code' => $step_data['step_2']['tutorial_type_code'], 'status' => 'PENDING'])->one();
            
            if(!empty($modelCreditTution)){
                
                $TutorialType = TutorialType::find()->where(['code' => $step_data['step_2']['tutorial_type_code'], 'status' => 'ENABLED'])->one();
            
                if(!empty($TutorialType)){
                    
                    $modelStudent = Student::findOne($student_uuid);
                    $modelTutor = Tutor::findOne($step_data['step_3']['tutor_uuid']);
                    $modelInstrument = Instrument::findOne($step_data['step_1']['instrument_uuid']);
                    
                    $session_date_array = explode(',', $step_data['step_4']['all_dates']);
                    $start_hour = $step_data['step_4']['start_hour'];
                    $start_datetime = date('Y-m-d H:i:s', strtotime($session_date_array[0]." ".$start_hour));

                    $end_datetime = date('Y-m-d H:i:s', strtotime($start_datetime." +". $TutorialType->min ." minutes"));
                    $digits = 4;
                    $pin = str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);

                    $modelBookedSession = new BookedSession();
                    $modelBookedSession->booking_uuid = $modelCreditTution->booking_uuid;
                    $modelBookedSession->credit_session_uuid = $modelCreditTution->getPrimaryKey();
                    $modelBookedSession->student_uuid = $student_uuid;
                    $modelBookedSession->tutor_uuid = $step_data['step_3']['tutor_uuid'];
                    $modelBookedSession->instrument_uuid = $step_data['step_1']['instrument_uuid'];
                    $modelBookedSession->tutorial_type_code = $step_data['step_2']['tutorial_type_code'];
                    $modelBookedSession->start_datetime = $start_datetime;
                    $modelBookedSession->end_datetime = $end_datetime;
                    $modelBookedSession->session_min = $TutorialType->min;
                    $modelBookedSession->session_pin = $pin;
                    $modelBookedSession->status = 'SCHEDULE';
                    $modelBookedSession->save(false);
                    
                    //update credit session entry
                    $modelCreditTution->status = 'BOOKED';
                    $modelCreditTution->updated_at = date('Y-m-d H:i:s');
                    $modelCreditTution->save(false);
                    
                    // return array set
                    $return = ['code' => 200, 'message' => 'success'];
                    
                    //send mail
                    
                    $student_name = $modelStudent->first_name.' '.$modelStudent->last_name;
                    $tutor_name = $modelTutor->first_name.' '.$modelTutor->last_name;
                    $session_time = General::displayTime($start_datetime) . ' - ' . General::displayTime($end_datetime);
                    $session_dates[0] = General::displayDate($start_datetime);
                    
                    $content['student_name'] = $student_name;
                    $content['tutor_name'] = $tutor_name;
                    $content['instrument'] = $modelInstrument->name;
                    $content['tution_duration'] = $TutorialType->min;
                    $content['time'] = $session_time;
                    $content['session_dates'] = $session_dates;

                    $content_tutor['tutor_name'] = $tutor_name;
                    $content_tutor['student_name'] = $student_name;
                    $content_tutor['instrument'] = $modelInstrument->name;
                    $content_tutor['tution_duration'] = $TutorialType->min;
                    $content_tutor['time'] = $session_time;
                    $content_tutor['session_dates'] = $session_dates;
                    
                    $ownersEmail = General::getOwnersEmails(['OWNER']);

                    Yii::$app->mailer->compose('booked_credit_tution_student', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                            ->setFrom(Yii::$app->params['supportEmail'])
                            ->setTo($modelStudent->email)
                            ->setBcc($ownersEmail)
                            ->setSubject('Booked New Credited Lesson - ' . Yii::$app->name)
                            ->send();


                    Yii::$app->mailer->compose('booked_credit_tution_tutor', ['content' => $content_tutor], ['htmlLayout' => 'layouts/html'])
                            ->setFrom(Yii::$app->params['supportEmail'])
                            ->setTo($modelTutor->email)
                            ->setBcc($ownersEmail)
                            ->setSubject('Booked New Lesson - ' . Yii::$app->name)
                            ->send();
                    
                } else {
                    $return = ['code' => 421, 'message' => 'This tutorial type is not available.'];
                }
            } else {
                $return = ['code' => 421, 'message' => 'You have not credit Tuion for this tutorial type.'];
            }
            
        }
        return $return;
    }
    
    public function bookingFirstLastSession($booking_uuid) {
        
        $return = ['start' => '', 'end' => ''];
        $model = BookedSession::find()->where(['booking_uuid' => $booking_uuid])->asArray()->orderBy('start_datetime ASC')->all();
        
        if (!empty($model)) {
            $end_array = end($model);
            $return['start'] = (isset($model[0]['start_datetime'])) ? General::displayDate($model[0]['start_datetime']) : '';
            $return['end'] = (isset($end_array['start_datetime'])) ? General::displayDate($end_array['start_datetime']) : '';
        }
        return $return;
    }

}
