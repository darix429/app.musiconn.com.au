<?php
 
use yii\helpers\Html;
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['name']; ?>,</p>

            <p>According to our records, your Working with Children certificate has expired on <?= $content['child_certificate_expiry']; ?> which will impact your available to deliver lessons with Musiconn Pty Ltd.</p>
            
	    <p>Please contact admin@musiconn.com.au if you have already renewed your Working with Children certification to arrange updating of our records.</p>

            <p>Sincerely,<br>
            Admin Team – Musiconn Pty Ltd</p>
        </td>
    </tr>
</table>

