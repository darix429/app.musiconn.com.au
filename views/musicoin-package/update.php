<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MusicoinPackage */

$this->title = Yii::t('app', 'Musicoin Package: {name}', [
    'name' => $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Musicoin Packages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<?= $this->render('_form', [
        'model' => $model,
    ]) ?>