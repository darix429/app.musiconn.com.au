<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BookedSession;

/**
 * BookedSessionSearch represents the model behind the search form of `app\models\BookedSession`.
 */
class BookedSessionSearch extends BookedSession
{
    public $instrument_uuid,$student_name,$tutor_name,$cancellation_daterange,$start_daterange;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'booking_uuid', 'credit_session_uuid', 'student_uuid', 'tutor_uuid', 'start_datetime', 'end_datetime', 'filename', 'session_pin', 'status', 'cancel_by', 'cancel_user_uuid', 'cancel_reason', 'cancel_datetime', 'created_at', 'updated_at','instrument_uuid','student_name','tutor_name','cancellation_daterange','start_daterange'], 'safe'],
            [['session_min'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $modeDatatable = false, $statusnot = '', $statusIn = '',$orderBy = 'start_datetime asc')
    { //echo "<pre>"; print_r($statusnot);  print_r($statusIn); exit;
        $query = BookedSession::find()->joinWith(['studentUu','tutorUu','studentBookingUu','creditSessionUu','instrumentUu']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        if (!empty($statusnot)) {
            if (is_array($statusnot)) { 

                $query->andWhere(['not in', 'booked_session.status', $statusnot]);
            }else { 

                $query->andWhere(['<>', 'booked_session.status', $statusnot]);
            }
        }
        
        if (!empty($statusIn)) { 
            if (is_array($statusIn)) {

                $query->andWhere(['in', 'booked_session.status', $statusIn]);
            } 
        }
        if($this->status !== 'ALL'){
            $query->andFilterWhere(['booked_session.status' => $this->status]);
        }
        $query->andFilterWhere(['ilike', "CONCAT(student.first_name,' ',student.last_name)", $this->student_name]);
        $query->andFilterWhere(['ilike', "CONCAT(tutor.first_name,' ',tutor.last_name)", $this->tutor_name]);
        $query->andFilterWhere(["student_tuition_booking.instrument_uuid"=> $this->instrument_uuid]);
        
        // grid filtering conditions
        $query->andFilterWhere([
            //'start_datetime' => $this->start_datetime,
            //"to_char(start_datetime, 'YYYY-MM-DD')" => $this->start_datetime,
            "to_char(booked_session.start_datetime, 'DD-MM-YYYY')" => $this->start_datetime,
            "to_char(booked_session.end_datetime, 'DD-MM-YYYY')" => $this->end_datetime,
            //'end_datetime' => $this->end_datetime,
            'session_min' => $this->session_min,
            'cancel_datetime' => $this->cancel_datetime,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'CAST(booked_session.tutor_uuid AS TEXT)'=> $this->tutor_uuid,
        ]);
        
        if (Yii::$app->user->identity->role == 'STUDENT') { 
            $query->andFilterWhere(['booked_session.student_uuid' => Yii::$app->user->identity->student_uuid]);
        }

        if (!is_null($this->start_daterange) && 
            strpos($this->start_daterange, ' - ') !== false ) {
            list($start_date, $end_date) = explode(' - ', $this->start_daterange);
            $query->andFilterWhere(['>=', '"booked_session"."start_datetime"', date('Y-m-d 00:00:00', strtotime($start_date))]);
            $query->andFilterWhere(['<=', '"booked_session"."start_datetime"', date('Y-m-d 23:59:59', strtotime($end_date))]);
        }
        
        if (!is_null($this->cancellation_daterange) && 
            strpos($this->cancellation_daterange, ' - ') !== false ) {
            list($start_date, $end_date) = explode(' - ', $this->cancellation_daterange);
            $query->andFilterWhere(['>=', '"booked_session"."cancel_datetime"', date('Y-m-d 00:00:00', strtotime($start_date))]);
            $query->andFilterWhere(['<=', '"booked_session"."cancel_datetime"', date('Y-m-d 23:59:59', strtotime($end_date))]);
        }
        

        $query->andFilterWhere(['ilike', 'uuid', $this->uuid])
            ->andFilterWhere(['ilike', 'CAST(booked_session.booking_uuid AS TEXT)', $this->booking_uuid])
            ->andFilterWhere(['ilike', 'credit_session_uuid', $this->credit_session_uuid])
            //->andFilterWhere(['ilike', 'student_uuid', $this->student_uuid])
            //->andFilterWhere(['ilike', 'tutor_uuid', $this->tutor_uuid])
            ->andFilterWhere(['ilike', 'filename', $this->filename])
            ->andFilterWhere(['ilike', 'session_pin', $this->session_pin])
            //->andFilterWhere(['ilike', 'status', $this->status])
            ->andFilterWhere(['ilike', 'cancel_by', $this->cancel_by])
            ->andFilterWhere(['ilike', 'cancel_user_uuid', $this->cancel_user_uuid])
            ->andFilterWhere(['ilike', 'cancel_reason', $this->cancel_reason]);
            

        if ($modeDatatable) {
            $result = $query->asArray()->orderBy($orderBy)->all();
           //echo $query->createCommand()->getRawSql();  exit;
            //echo "<pre>"; print_r($result); 
            return $result;
        }
        return $dataProvider;
    }


    public function searchReport($params, $modeDatatable = false, $statusnot = '', $statusIn = '',$orderBy = 'start_datetime asc')
    { //echo "<pre>"; print_r($statusnot);  print_r($statusIn); exit;
        $query = BookedSession::find()->joinWith(['studentUu','tutorUu','studentBookingUu']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        if (!empty($statusnot)) {
            if (is_array($statusnot)) { 

                $query->andWhere(['not in', 'booked_session.status', $statusnot]);
            }else { 

                $query->andWhere(['<>', 'booked_session.status', $statusnot]);
            }
        }
        
        if (!empty($statusIn)) { 
            if (is_array($statusIn)) {

                $query->andWhere(['in', 'booked_session.status', $statusIn]);
            } 
        }
        if($this->status !== 'ALL'){
            $query->andFilterWhere(['booked_session.status' => $this->status]);
        }
        $query->andFilterWhere(['ilike', "CONCAT(student.first_name,' ',student.last_name)", $this->student_name]);
        $query->andFilterWhere(['ilike', "CONCAT(tutor.first_name,' ',tutor.last_name)", $this->tutor_name]);
        $query->andFilterWhere(["student_tuition_booking.instrument_uuid"=> $this->instrument_uuid]);
        
        // grid filtering conditions
        $query->andFilterWhere([
            //'start_datetime' => $this->start_datetime,
            //"to_char(start_datetime, 'YYYY-MM-DD')" => $this->start_datetime,
            "to_char(booked_session.start_datetime, 'DD-MM-YYYY')" => $this->start_datetime,
            "to_char(booked_session.end_datetime, 'DD-MM-YYYY')" => $this->end_datetime,
            //'end_datetime' => $this->end_datetime,
            'session_min' => $this->session_min,
            'cancel_datetime' => $this->cancel_datetime,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'CAST(booked_session.tutor_uuid AS TEXT)'=> $this->tutor_uuid,
        ]);
        
        if (Yii::$app->user->identity->role == 'STUDENT') {
            $query->andFilterWhere(['booked_session.student_uuid' => Yii::$app->user->identity->student_uuid]);
        } else if (Yii::$app->user->identity->role == 'TUTOR') {
            $query->andFilterWhere(['booked_session.tutor_uuid' => Yii::$app->user->identity->tutor_uuid]);
        }
        
        if (!is_null($this->start_daterange) && 
            strpos($this->start_daterange, ' - ') !== false ) {
            list($start_date, $end_date) = explode(' - ', $this->start_daterange);
            $query->andFilterWhere(['>=', '"booked_session"."start_datetime"', date('Y-m-d 00:00:00', strtotime($start_date))]);
            $query->andFilterWhere(['<=', '"booked_session"."start_datetime"', date('Y-m-d 23:59:59', strtotime($end_date))]);
        }
        

        $query->andFilterWhere(['ilike', 'uuid', $this->uuid])
            ->andFilterWhere(['ilike', 'CAST(booking_uuid AS TEXT)', $this->booking_uuid])
            ->andFilterWhere(['ilike', 'credit_session_uuid', $this->credit_session_uuid])
            //->andFilterWhere(['ilike', 'student_uuid', $this->student_uuid])
            //->andFilterWhere(['ilike', 'tutor_uuid', $this->tutor_uuid])
            ->andFilterWhere(['ilike', 'filename', $this->filename])
            ->andFilterWhere(['ilike', 'session_pin', $this->session_pin])
            //->andFilterWhere(['ilike', 'status', $this->status])
            ->andFilterWhere(['ilike', 'cancel_by', $this->cancel_by])
            ->andFilterWhere(['ilike', 'cancel_user_uuid', $this->cancel_user_uuid])
            ->andFilterWhere(['ilike', 'cancel_reason', $this->cancel_reason]);
            

        if ($modeDatatable) {
            $result = $query->asArray()->orderBy($orderBy)->all();
           //echo $query->createCommand()->getRawSql();  exit;
            //echo "<pre>"; print_r($result); 
            return $result;
        }
        return $dataProvider;
    }
}
