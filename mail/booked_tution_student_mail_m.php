<?php

use yii\helpers\Html;
?>
<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['student_name']; ?>,</p>

            <p>Below are the details of your booking:</p>

            <p><strong>Booking Reference:</strong> <?= "#" . $content['booking_id']; ?></p>
            <?php 
            $tutor_list = array();
            $lesson_list = array();
            if (is_array($content['session_dates'])) { ?>
                <?php
                foreach ($content['session_dates'] as $key => $value) {
                    $tutor_list[] = $content['tutor_name'][$key]['first_name'] . " " . $content['tutor_name'][$key]['last_name'];
                                        
                    foreach ($value as $keyy => $values) {
                        $lesson_list[] = $values. ' '.$content['time'][$key][$keyy];
                    }
                }
                echo "<p><strong>Tutor Name: </strong>" . implode(", ", $tutor_list) . "</p>";
                ?>                    
            <?php } ?>
            
            <p><strong>Instrument:</strong> <?= $content['instrument']; ?></p>

            <p><strong>Scheduled lessons:</strong></p>
            <ul>
            <?php 
            if (!empty($lesson_list)) { 
                
                foreach ($lesson_list as $v) {
                    echo "<li>" . $v . "</li>";
                }
                
            } ?>
                </ul>
            

            <p>You can find the above lesson details by logging onto our <a target="_blank" href="https://app.musiconn.com.au/">booking system</a>.</p>

            <p>If you need to re-schedule a lesson with your tutor, please refer to the student’s manual for instructions. The manual can be access on left menu of your dashboard.</p>
            
            <p>For terms and conditions of your booking, please refer to the <a target="_blank" href="https://www.musiconn.com.au/terms-and-conditions">Musiconn website</a>.</p>

            <p>Please do not reply to this unattended mailbox.  Any queries can be directed to admin@musiconn.com.au, or call us on 1300 068 742.</p>


            <p>Regards,<br>
                Julian Fung <br>
                CEO – Musiconn Pty Ltd</p>
        </td>
    </tr>
</table>
