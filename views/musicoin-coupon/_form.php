<?php

use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MusicoinCoupon */
/* @var $form yii\widgets\ActiveForm */
//echo "<pre>";
//print_r($model->getErrors());
//echo "</pre>";
//exit;
$this->registerJs(
        '

', View::POS_END
);
?>
<style>
    .input-group .form-control{
        width: 285px;
    }
    /*#per_amount_div {
    display: none;
}*/
</style>
<div class="col-xl-12 col-lg-12 col-12 col-md-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Please Fill Information</h2>
            <div class="actions panel_actions float-right">
                <a href="<?= Yii::$app->getUrlManager()->createUrl(['/musicoin-coupon/index/']) ?>" class="btn btn-info btn-icon "><i class="fa fa-arrow-left text-white"></i> <span>Back</span></a>
            </div>
        </header>
        <div class="content-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12">
                    <?php
//                    echo "<pre>";
//                    print_r($model->getErrors());
//                    echo "</pre>";
                    ?>
                    <?php
                    $form = ActiveForm::begin([
                                'action' => (!$model->isNewRecord) ? Yii::$app->getUrlManager()->createUrl(['/musicoin-coupon/update/', 'id' => $model->uuid]) : ['create'],
                                'enableClientValidation' => true,
                                //'enableAjaxValidation' => true,
                                'validateOnChange' => true,
                    ]);
                    ?>
                    <div class="form-row">
                        <div class="col-md-4 mb-0">
                            <?= $form->field($model, 'code')->textInput(['class' => 'form-control code', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('code')]) ?>
                        </div>
                        <div class="col-md-4 mb-0">
                            <?= $form->field($model, 'coins')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('coins')]) ?>
                        </div>

<!--                        <div class="col-md-4 mb-0">

                            <?php // $form->field($model, 'min_amount', ['template' => '{label}<div class="input-group"><div class="input-group-prepend" ><span class="input-group-text">$</span></div>{input}{error}</div>'])->textInput(['class' => 'form-control common', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('min_amount'), 'aria-label' => 'Amount (to the nearest dollar)', 'style' => 'text-align:right; width: auto;']) ?>
                        </div>-->
                        <div class="col-md-4 mb-0">
                            <?= $form->field($model, 'status')->dropDownList(['ENABLED' => 'ENABLED', 'DISABLED' => 'DISABLED'], ['class' => 'form-control']) ?>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-4 mb-0">
                            <?php
                            $model->start_date = \app\libraries\General::displayDate($model->start_date);
                            echo $form->field($model, 'start_date')->textInput(['readonly' => true, 'class' => 'form-control custom_date_picker', 'maxlength' => true, 'placeholder' => 'DD-MM-YYYY'])
                            ?>
                        </div>
                        <div class="col-md-4 mb-0">
                            <?php
                            $model->end_date = \app\libraries\General::displayDate($model->end_date);
                            echo $form->field($model, 'end_date')->textInput(['readonly' => true, 'class' => 'form-control custom_date_picker', 'maxlength' => true, 'placeholder' => 'DD-MM-YYYY'])
                            ?>
                        </div>
                        
                    </div>
                    <div class="form-row">
                        <div class="col-md-12 mb-0">
                            <?= $form->field($model, 'description')->textarea(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('description')]) ?>
                        </div>

                    </div>

                    <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary', 'style' => 'float: right;']) ?>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>

        </div>
    </section>
</div>
