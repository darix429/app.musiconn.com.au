<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%order}}".
 *
 * @property string $uuid
 * @property string $order_id
 * @property string $student_uuid
 * @property string $order_datetime
 * @property string $total
 * @property string $discount
 * @property string $grand_total
 * @property string $invoice_file
 *
 * @property Student $studentUu
 * @property OrderItem[] $orderItems
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%order}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'order_id', 'student_uuid'], 'required'],
            [['uuid', 'order_id', 'student_uuid', 'invoice_file'], 'string'],
            [['order_datetime'], 'safe'],
            [['total', 'discount', 'grand_total'], 'number'],
            [['uuid'], 'unique'],
            [['student_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['student_uuid' => 'student_uuid']],
            [['payment_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentTransaction::className(), 'targetAttribute' => ['payment_uuid' => 'uuid']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'order_id' => Yii::t('app', 'Order ID'),
            'student_uuid' => Yii::t('app', 'Student Uuid'),
            'order_datetime' => Yii::t('app', 'Order Datetime'),
            'total' => Yii::t('app', 'Total'),
            'discount' => Yii::t('app', 'Discount'),
            'grand_total' => Yii::t('app', 'Grand Total'),
            'invoice_file' => Yii::t('app', 'Invoice File'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentTransactionUu()
    {
        return $this->hasOne(PaymentTransaction::className(), ['uuid' => 'payment_uuid']);
    }
    
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems()
    {
        return $this->hasMany(OrderItem::className(), ['order_uuid' => 'uuid']);
    }
}
