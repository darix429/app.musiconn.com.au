<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%monthly_subscription_fee}}".
 *
 * @property string $uuid
 * @property string $name
 * @property string $price
 * @property string $tax
 * @property string $total_price
 * @property string $description
 * @property int $with_tutorial
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 */
class MonthlySubscriptionFee extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%monthly_subscription_fee}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //html tag filter
            [['name','description'], 'match', 'pattern' => \app\libraries\General::validTextPattern(['@','$','(',')','\[','\]','\/',]), 'message' => 'Invalid charecters used.'], 
            
	    [[ 'name', 'price', 'tax'], 'required'],
            [['uuid', 'name', 'description', 'status'], 'string'],
            [['name'], 'required'],
            //[['price', 'tax', 'total_price'],'match','pattern' => '/^([0-9])+(\.[0-9]{1,5})?$/','message'=>'Only allow decimal 5 digit.'],
	    [['price', 'tax'],'match','pattern' => '/^\d{0,10}(\.\d{0,2})?$/'/*,'message'=>'Only allow decimal 5 digit.'*/],
	    
            [['with_tutorial'], 'default', 'value' => null],
            [['with_tutorial'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
	    //[['price', 'tax'],'number'],
            ['tax', 'compare', 'compareAttribute' => 'price', 'operator' => '<','enableClientValidation' => false],
            ['price', 'compare', 'compareAttribute' => 'tax', 'operator' => '>','enableClientValidation' => false],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'name' => Yii::t('app', 'Name'),
            'price' => Yii::t('app', 'Price'),
            'tax' => Yii::t('app', 'Tax'),
            'total_price' => Yii::t('app', 'Total Price'),
            'description' => Yii::t('app', 'Description'),
            'with_tutorial' => Yii::t('app', 'With Tutorial'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /*public function getamount()
    {
	
    	$match = '(/(\d{0,10})[^.]*((?:\.\d{0,5})?)/g)';
    	
 	 
    }*/
    
    public function studentRecomandedMonthlySubscription($student_uuid){
        
        
        $student = StudentMonthlySubscription::find()->where(['student_uuid' => $student_uuid])->one();
        
        if(!empty($student)){
            $monthlyPlan = MonthlySubscriptionFee::find()->where(['with_tutorial' => '0','status' => 'ENABLED'])->one();
        }else{
            $monthlyPlan = MonthlySubscriptionFee::find()->where(['with_tutorial' => '1','status' => 'ENABLED'])->one();
            
        }
        return $monthlyPlan;
    }
}
