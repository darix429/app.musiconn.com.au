<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\InstrumentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Instruments');
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs(
        '
        $(document).on("click",".instrument_inactive_link", function(){    
            if(confirm("Are you sure you want to deactivate this instrument?")){
            var element = $(this);
                $.ajax({
                    type: "GET",
                     url: "' . Url::to(['/instrument/instrument-inactive/']) . '",
                    data: { "id": $(this).data("uuid")},
                    //beforeSend:function(){ $(".local-spinner").show(); },
                    success:function(result){
                        if(result.code == 200){
                            $(element).removeClass("badge-primary");
                            $(element).addClass("badge-secondary");
                            $(element).removeClass("instrument_inactive_link");
                            $(element).addClass("instrument_active_link");
                            $(element).text("INACTIVE");
                            showSuccess(result.message);
                        }else{
                            showErrorMessage(result.message);
                        }
                    },
                    error:function(e){ 
                        showErrorMessage("Sorry, something wrong.");
                    }
                });
            }
        });
    
        $(document).on("click",".instrument_active_link", function(){ 
            if(confirm("Are you sure you want to active this instrument?")){
            var element = $(this);
                $.ajax({
                    type: "GET",
                    url: "' . Url::to(['/instrument/instrument-active/']) . '",
                    data: { "id": $(this).data("uuid")},
                    //beforeSend:function(){ $(".local-spinner").show(); },
                    success:function(result){
                        if(result.code == 200){
                            $(element).removeClass("badge-secondary");
                            $(element).addClass("badge-primary");
                            $(element).removeClass("instrument_active_link");
                            $(element).addClass("instrument_inactive_link");
                            $(element).text("ACTIVE");
                            showSuccess(result.message);
                        }else{
                            showErrorMessage(result.message);
                        }
                    },
                    error:function(e){ 
                        showErrorMessage("Sorry, something wrong.");
                    }
                });
            }
        });
        
    $(document).on("click",".delete_admin_link", function(){
        if(confirm("Are you sure you want to delete this Instrument?")){
        var element = $(this);
            $.ajax({
                type: "POST",
                url: "' . Url::to(['/instrument/delete/']) . '",
                data: { "id": $(this).data("uuid")},
                beforeSend:function(){ blockBody(); },
                success:function(result){
                    if(result.code == 200){
                        unblockBody();
                        $(element).closest("div.music_genre").remove();
                        showSuccess(result.message);
                    }else{
                        unblockBody();
                        showErrorMessage(result.message);
                    }
                },
                error:function(e){ 
                    unblockBody();
                    showErrorMessage(e.responseText);
                }
            });
        }
    });
        $(document).on("click","#search_btn", function(){        
            $.ajax({
                type: "GET",
                url: "' . Url::to(['/instrument/index/']) . '",
                data: $("#search_frm").serialize(),
                beforeSend:function(){ blockBody(); },
                success:function(result){
                    unblockBody();
                    $("#student_table_parent_div").html(result);                    
                },
                error:function(e){
                    unblockBody();
                    showErrorMessage(e.responseText);
                }
            });
        });
        $(document).on("click","#search_reset_btn", function(){        
            $.ajax({
                type: "GET",
                url: "' . Url::to(['/instrument/index/']) . '",
                data: {},
                beforeSend:function(){ blockBody(); },
                success:function(result){
                    unblockBody();
                    $("#student_table_parent_div").html(result);                    
                },
                error:function(e){
                    unblockBody();
                    showErrorMessage(e.responseText);
                }
            });
        });
', View::POS_END
);
?>

<div class="col-xl-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">INSTRUMENTS List</h2>
            <div class="actions panel_actions float-right">
                <i class="fa fa-search icon-primary icon-xs icon-orange mail_head_icon custom-toggle" data-togglediv="datatable-search-div" title="Advance Search"></i>
                <a title='Add Instrument Category' href="<?= Yii::$app->getUrlManager()->createUrl(['/instrument/create/']) ?>" class="btn btn-primary "><i class="fa fa-plus text-white"></i></a>
            </div>
        </header>
        <div class="content-body">  
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12 ">
                    <div class="datatable-search-div " style="display: none;">

                        <?php
                        $form = ActiveForm::begin([
                                    'method' => 'get',
                                    'options' => [
                                        'id' => 'search_frm',
                                        'class' => 'form-inline'
                                    ],
                        ]);
                        ?>

                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'name')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Name', 'style' => 'width:100%'])->label(false) ?>
                        </div>
                        <!--div class="col-md-4 mb-0">
                        <!--?= $form->field($searchModel, 'email')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $searchModel->getAttributeLabel('email'), 'style' => 'width:100%'])->label(false) ?>
                    </div-->
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'status')->dropDownList(['ACTIVE' => 'ACTIVE', 'INACTIVE' => 'INACTIVE'], ['class' => 'form-control', 'style' => 'width:100%'])->label(false) ?>
                        </div>    
                        
                            <?= Html::Button(Yii::t('app', 'Search'), ['class' => 'btn btn-primary mb-0', 'id' => 'search_btn']) ?>
                            <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-purple mb-0', 'id'=>'search_reset_btn']) ?>
                        

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>

            </div>
            <br>
            <div class="clearfix"></div>
            <div class="row" id="student_table_parent_div">
                <?php echo Yii::$app->controller->renderPartial('_instrument_list', ['instrumentList' => $instrumentList]); ?>
            </div>
        </div>
    </section>
</div>
