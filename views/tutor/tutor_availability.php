<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;

$this->title = Yii::t('app', 'Tutors');
$this->params['breadcrumbs'][] = $this->title;


$this->registerJs(
        '
    $(document).on("click",".tutor_disabled_link", function(){
        if(confirm("Are you sure, you want to disable this account?")){
        var element = $(this);
            $.ajax({
                type: "GET",
                url: "' . Url::to(['/tutor/account-disabled/']) . '",
                data: { "id": $(this).data("uuid")},
                //beforeSend:function(){ $(".local-spinner").show(); },
                success:function(result){
                    if(result.code == 200){
                        $(element).removeClass("badge-primary");
                        $(element).addClass("badge-secondary");
                        
                        $(element).removeClass("tutor_disabled_link");
                        $(element).addClass("tutor_enabled_link");
                        $(element).text("DISABLED");
                        showSuccess(result.message);
                    }else{
                        showErrorMessage(result.message);
                    }
                },
                error:function(e){ 
                    showErrorMessage(e.responseText);
                }
            });
        }
    });
    
    $(document).on("click",".tutor_enabled_link", function(){
        if(confirm("Are you sure, you want to enable this account?")){
        var element = $(this);
            $.ajax({
                type: "GET",
                url: "' . Url::to(['/tutor/account-enabled/']) . '",
                data: { "id": $(this).data("uuid")},
                //beforeSend:function(){ $(".local-spinner").show(); },
                success:function(result){
                    if(result.code == 200){
                        $(element).removeClass("badge-secondary");
                        $(element).addClass("badge-primary");
                        
                        $(element).removeClass("tutor_enabled_link");
                        $(element).addClass("tutor_disabled_link");
                        
                        $(element).text("ENABLED");
                        showSuccess(result.message);
                    }else{
                        showErrorMessage(result.message);
                    }
                },
                error:function(e){ 
                    showErrorMessage(e.responseText);
                }
            });
        }
    });
    
//delete code 
    $(document).on("click",".delete_tutor_link", function(){
        if(confirm("Are you sure, you want to delete this account?")){
        var element = $(this);
            $.ajax({
                type: "POST",
                url: "' . Url::to(['/tutor/delete/']) . '",
                data: { "id": $(this).data("uuid")},
                success:function(result){
                    if(result.code == 200){
                    
                        $(element).closest("tr").remove();
                        
                        showSuccess(result.message);
                    }else{
                        showErrorMessage(result.message);
                    }
                },
                error:function(e){ 
                    showErrorMessage(e.responseText);
                }
            });
        }
    });
    
$(document).on("click","#search_btn", function(){        
    $.ajax({
        type: "GET",
        url: "' . Url::to(['/tutor/index/']) . '",
        data: $("#search_frm").serialize(),
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            $("#tutor_table_parent_div").html(result);
            window.ULTRA_SETTINGS.dataTablesInit();
        },
        error:function(e){
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});
    
    
', View::POS_END
);
?>

<div class="col-xl-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Tutor List</h2>
            <div class="actions panel_actions float-right">
                <i class="fa fa-search icon-primary icon-xs icon-orange mail_head_icon custom-toggle" data-togglediv="datatable-search-div" title="Advance Search"></i>
                <a href="<?= Yii::$app->getUrlManager()->createUrl(['/tutor/create/']) ?>" class="btn btn-primary "><i class="fa fa-plus text-white"></i></a>
            </div>
        </header>
        <div class="content-body"> 
            <div class="row ">
                <div class="col-lg-12 col-md-12 col-12 ">
                    <div class="datatable-search-div " style="display: none;">

                        <?php
                        $form = ActiveForm::begin([
                                    'method' => 'get',
                                    'options' => [
                                        'id' => 'search_frm',
                                        'class' => 'form-inline'
                                    ],
                        ]);
                        ?>

                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'first_name')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Name'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'email')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $searchModel->getAttributeLabel('email')])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'status')->dropDownList(['ENABLED' => 'ENABLED', 'DISABLED' => 'DISABLED', 'DELETED' => 'DELETED'], ['class' => 'form-control', 'style' => 'width:100%'])->label(false) ?>
                        </div>                            
                        <?= Html::Button(Yii::t('app', 'Search'), ['class' => 'btn btn-primary mb-0', 'id' => 'search_btn']) ?>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
            <br>
            <div class=""></div>
            <div class="clearfix"></div>
            <div class="row" id="tutor_table_parent_div">
                <?php echo Yii::$app->controller->renderPartial('_tutor_list', ['tutorList' => $tutorList]); ?>
            </div>
        </div>
    </section>
</div>


