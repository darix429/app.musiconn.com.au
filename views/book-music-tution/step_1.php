<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Url;

$this->title = Yii::t('app', 'BOOK MUSIC LESSONS');
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_icon'] = 'my-lesson-tag.png';

if (isset($step_data['step_1']['instrument_uuid']) && $step_data['step_1']['instrument_uuid'] != "") {
    $model->instrument_uuid = $step_data['step_1']['instrument_uuid'];
    $model->instrumentcheckbox = $model->instrument_uuid;
}
//echo "<pre>";
//print_r($step_data);
//echo "</pre>";
$this->registerJs(
        '
    $("input[name=\"StudentTuitionBooking[instrumentcheckbox]\"]").on("ifClicked", function (event) {
        $("#studenttuitionbooking-instrument_uuid").val(this.value);

        $(".instrument_select_btn").addClass("d-none")
        $(this).closest("div.instrument_tr").find(".instrument_select_btn").removeClass("d-none")
    });

    $( "#frm_step_1" ).submit(function( event ) {
        blockBody();
    });

        ', View::POS_END
);
?>

<section class="content">
    <label class="warning-info mar-bottom-10">For issues concerning tutor availability, please contact <a href="mailto:<?= Yii::$app->params['adminEmail']; ?>"><?= Yii::$app->params['adminEmail']; ?></a> for assistance.</label>
    <div class="row mar-bottom-10">
        <div class="col-xs-12">
            <div class="box">
                <div class="col-md-12 box-panel-header border-bottom-radious-none">
                    <h4 class="col-md-6">Select Instrument</h4>
                    <div class="col-md-6 "> </div>
                </div>
                <div class="box-body box-panel-body" style="max-height: 500px;overflow-x: hidden;overflow-y: scroll;border-top-left-radius: 0px;border-top-right-radius: 0px;">
                    <div class="row vertical-center-box vertical-center-box-tablet"  style="">
                        <?php
                        $form = ActiveForm::begin([
                                    'action' => ['index'],
                                    'id' => 'frm_step_1',
                                    'enableClientValidation' => true,
//                              'enableAjaxValidation' => true,
                                    'validateOnChange' => true,
                                    'options' => ['class' => 'form-horizontal']]);
                        ?>

                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <div class="row" style="">
                                    <input type="hidden" name="step" value="1">
                                    <?= $form->field($model, 'instrument_uuid')->hiddenInput(['class' => 'form-control', 'maxlength' => true,])->label(false) ?>
                                </div>
                                <?php
                                if (!empty($instrumentList)) {
                                    foreach ($instrumentList as $key => $value) {
                                        ob_start();
                                        fpassthru($value['image']);
                                        $contents = ob_get_contents();
                                        ob_end_clean();
                                        $dataUri = "data:image/jpg;base64," . base64_encode($contents);

                                        $model->instrumentcheckbox = $model->instrument_uuid;
                                        $displayClass = ($model->instrument_uuid == $value->uuid) ? "" : "d-none";

                                        $totalInstrumentsTutor = \app\models\TutorInstrument::find()->joinWith(['tutor'])->where(['instrument_uuid' => $value->uuid, 'tutor.status' => 'ENABLED'])->count();
                                        ?>
                                        <div class="row pakage-list instrument_tr" >
                                            <div class="col-md-2 text-center">
                                                <?php
                                                echo $form->field($model, 'instrumentcheckbox')->radio([
                                                    'label' => '',
                                                    'template' => '{input}{error} ',
                                                    'class' => 'skin-square-red float-left instrument_checkbox', 'value' => $value->uuid])->label('')
                                                ?>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="pull-left mar-right-10">
                                                    <img class="" style="width: 30px;" src="<?= $dataUri; ?>">
                                                </div>
                                                <p class="">
                                                    <?= $value->name ?> (<?= $totalInstrumentsTutor; ?>)
                                                </p>
                                            </div>
                                            <div class="col-md-2">
                                                <?= Html::submitButton('Select', ['class' => "btn btn-primary btn-md instrument_select_btn $displayClass"]) ?>
                                            </div>
                                        </div>

                                        <?php
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row mar-bottom-10">
        <div class="col-xs-12">
            <div class="box">
                <div class="col-md-12 box-panel-header ">
                    <h4 class="col-md-6">Select Plan</h4>
                    <div class="col-md-6 "></div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row mar-bottom-10">
        <div class="col-xs-12">
            <div class="box">
                <div class="col-md-12 box-panel-header ">
                    <h4 class="col-md-6">Select Tutor</h4>
                    <div class="col-md-6 "></div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row mar-bottom-10">
        <div class="col-xs-12">
            <div class="box">
                <div class="col-md-12 box-panel-header ">
                    <h4 class="col-md-6">Select Date</h4>
                    <div class="col-md-6 "></div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row mar-bottom-10">
        <div class="col-xs-12">
            <div class="box">
                <div class="col-md-12 box-panel-header ">
                    <h4 class="col-md-6">Confirm and Payment</h4>
                    <div class="col-md-6 "></div>
                </div>
            </div>
        </div>
    </div>
    
    <a class="btn btn-primary btn-sm black_button_in" href="<?= Yii::$app->getUrlManager()->createUrl('/booked-session/my-lessons'); ?>">RETURN TO MY LESSONS</a>
</section>

<!--==============-->
<style>
/*    .field-studenttuitionbooking-instrumentcheckbox{
        margin-bottom: 0px !important;
    }*/
</style>