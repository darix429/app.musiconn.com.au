<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\ArrayHelper;
use app\models\TutorUnavailability;
use app\models\Tutor;
use app\models\Instrument;

$this->title = Yii::t('app', 'Tutor Availability');
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_icon'] = 'my-profiles.png';

$instrumentList = Instrument::find()->where(['status' => 'ACTIVE'])->asArray()->all();
$tutorList = Tutor::find()->where(['status' => 'ENABLED'])->asArray()->all();

$this->registerJs(
        '
    $(document).on("click","#tutor_search_btn",function(){
        if( $("#tutor_select").val() != ""){

            $.ajax({
                type: "GET",
                url: "'.Url::to(['/student/tutor_available/']).'",
                data: { "tutor_uuid": $("#tutor_select").val()},
                beforeSend:function(){
                    blockBody();
                },
                success:function(result){

                    if(result.code == 404){
                        showErrorMessage(result.message);
                    }else{
                        $("#student_tutor_calendar_parent_div").show();
                        $("#student_tutor_calendar_parent_div").html(result);
                    }
                    unblockBody();
                },
                error:function(e){
                    unblockBody();
                    showErrorMessage(e.responseText);
                }
            });

        }else{
            $("#student_tutor_calendar_parent_div").hide();
        }
    });

    function getTutor(instrument_id)
    {
        $.ajax({
            type: "GET",
            url: "'.Url::to(['/student/get_tutor/']).'",
            data: { "id": instrument_id},
            beforeSend:function(){
                blockBody();
            },
            success:function(result){

                if(result.code == 404){
                    showErrorMessage(result.message);
                }else{
                    $("#tutor_select_div").html(result);
                }
                unblockBody();
            },
            error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
    }


', View::POS_END
);

$this->registerJs('
    

    ');
?>


<div class="col-xl-12">
    <section class="content">
        <div class="container-fluid video_categry-bg-white box box-primary">
            <div class="col-lg-12 col-md-12 col-12">
                <div class="form-group">
                    <div class="col-md-4">
                        <select class="form-control myselect2" id="instrument_select" onchange="getTutor(this.value)">
                            <option value="" selected>Select Instrument</option>
                            <?php
                            if(count($instrumentList)>0){
                                foreach ($instrumentList as $key => $value) {
                                    echo '<option value="'.$value['uuid'].'" >'.$value['name'].'</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>  
                    <div class="col-md-4" id="tutor_select_div">
                        <select class="form-control myselect2" id="tutor_select">
                            <option value="" selected>Select Tutor</option>
                            <?php
                            /*if(count($tutorList)>0){
                                foreach ($tutorList as $key => $value) {
                                    echo '<option value="'.$value['uuid'].'" >'.$value['first_name'].' '.$value['last_name'].'</option>';
                                }
                            }*/
                            ?>
                        </select>
                    </div>  
                    <div class="col-md-4">
                        <button class="btn btn-orange" id="tutor_search_btn" type="button">Search</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-12" id="student_tutor_calendar_parent_div" style="display:none">
            <div class="container-fluid video_categry-bg-white box box-primary">
                <?php //echo Yii::$app->controller->renderPartial('_tutor_availability_calender', []); ?>
            </div>
        </div>
    </section>
</div>


