<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
?>
<style>
    .custom-box{
        height: 141px;
        width: 211px;
        text-align: center;
        font-size: 20px;
        margin-top:93px;
    }
    .music_genre .thumb {
        height:96%;
    }
    .gallery-parent-div {
        border: 0px solid;
        padding: 0;
    }
</style>

<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="row">
        <?php
        if (!empty($videoList)) {
            foreach ($videoList as $key => $value) {       
                $thumb_url = is_file(Yii::$app->params['media']['pre_recorded_video']['thumbnail']['path'] . $value['uuid'].'.jpg') ? 
                                    Yii::$app->params['media']['pre_recorded_video']['thumbnail']['url'] . $value['uuid'].'.jpg' :
                                    Yii::$app->params['theme_assets']['url'] . "images/thumbnail.jpeg";
                ?>

                <div class="col-lg-3 col-sm-6 col-md-4 music_genre">
                    <div class="team-member gallery-parent-div">
                        <div class="thumb">
                            <a href="<?= Url::to(['/video/review/', 'id' => $value['uuid']]) ?>">
                                <img class="video_thumb_image" src="<?= $thumb_url; ?>">
                            </a>
                            <div class="overlay">                                
                                <a href="<?= Url::to(['/video/review/', 'id' => $value['uuid']]) ?>" class="overlay">
                                    <i class="fa fa-eye"></i>
                                </a>
                            </div>
                        </div>

                        <div class="team-info ">
                            <h4>&nbsp;
                                <?= $value['title']; ?>
                            </h4>
                        </div>

                        <div class="" align="center">
                            <span><a href="<?= Url::to(['/video/review/', 'id' => $value['uuid']]) ?>" style="color: rgba(147, 149, 150, 1);" title="Review"><i class="box_toggle fa fa-eye" style="font-size:18px;"></i></a></span>&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>

                    </div>
                </div>
                <?php
            }
        } else {
            ?>
            <h5 style="margin-left: 25px;"><b>No video found</b></h5>
        <?php }
        ?>

        <?php
        if (!empty($CategoryList)) {

            foreach ($CategoryList as $key => $value) {
                ?>
                <div class="hidden1" style="display: none;">
                    <h4 class="category_count"><?= count($CategoryList); ?></h4>
                </div>
                <div class="hidden" style="margin-right: 30px; display: none;">
                    <h4 class="category_name"><?= $value['name']; ?></h4>
                    <h4 class="category_uuid"><?= $value['uuid']; ?></h4>
                    <h4 class="category_parent_id"><?= $value['parent_id']; ?></h4>
                </div>
                <?php
            }
        } ?>
    </div>
</div>