<?php

use yii\helpers\Html;
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['subadmin_name'] ?>,</p>
            
            <p><?= $content['tutor_name'] ?> has successfully uploaded a video file '<?= $content['video_title'] ?>' into the video library.</p>

            <p>Please do not reply to this unattended mailbox. Any queries can be directed to admin@musiconn.com.au.</p>
            
            <p>Regards,</p>
            <p>Musiconn Booking System</p>
        </td>
    </tr>
</table>

