<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\helpers\Url;
use app\models\StudentTuitionBooking;
use app\models\Tutor;
use app\models\User;
use app\models\RescheduleSession;
use app\models\OnlineTutorialPackage;
use app\libraries\General;

$this->title = Yii::t('app', 'Reschedule Lesson');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Lessons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$model->tutor_uuid = $modelBS->tutor_uuid;
$model->student_uuid = $modelBS->student_uuid;
$tutor_uuid = $modelBS->tutor_uuid;
$student_uuid = $modelBS->student_uuid;
$tutorDetail = Tutor::findOne($tutor_uuid);
$tutor_working_plan = json_decode($tutorDetail->working_plan, true);
$tutor_user_model = User::find()->where(['tutor_uuid' => $tutor_uuid])->one();
$student_user_model = User::find()->where(['student_uuid' => $student_uuid])->one();
$tutor_timezone = (!empty($tutor_user_model)) ? $tutor_user_model->timezone : Yii::$app->params['timezone'];
$user_timezone = (!empty($student_user_model)) ? $student_user_model->timezone : Yii::$app->params['timezone'];
$model->tutorial_type_code = $modelBS->tutorial_type_code;
// $package = OnlineTutorialPackage::find()->joinWith(['paymentTermUu', 'tutorialTypeUu'])->where(['online_tutorial_package.uuid' => $step_data['step_2']['onlinetutorial_uuid']])->asArray()->one();
$session_minute = $modelBS->session_min;
// $start_hour_array = StudentTuitionBooking::hoursRange();
$start_hour_array = [];

$rescheduled_awaiting_entry = RescheduleSession::find()->where(['booked_session_uuid' => $modelBS->uuid, 'status' => 'AWAITING'])->one();


$this->registerJs('

$("document").ready(function(){
    $(".btn-secondary").removeAttr("onclick");
    $("#studenttuitionbooking-start_hour").css({"display":"none"});
});

function load_daywise_changes(dy,tm = "00:00",whendate = ""){

    //if(dy !== undefined){
    if(whendate !== ""){
    $.ajax({
            type: "GET",
            url: "' . Url::to(['book-music-tution/reschedule-get-day-changes']) . '",
            data: {"day":dy,"selected_time":tm,"selected_when_date":whendate,"tutor_uuid":$("#studenttuitionbooking-tutor_uuid").val(),"student_uuid":$("#studenttuitionbooking-student_uuid").val(),"tutorial_type_code":$("#studenttuitionbooking-tutorial_type_code").val()},
            beforeSend: function() {
                blockBody();
            },
            success:function(result){

                $("#studenttuitionbooking-session_when_start").datepicker("remove");


                if(result.code == 200){
                    $("#studenttuitionbooking-start_hour").css({"display":"inline"});
                    $("#studenttuitionbooking-session_when_start").val(result.session.session_when_start);


                    if(result.message != "success"){
                        showErrorMessage(result.message);
                    }

                }else{
                    $("#studenttuitionbooking-session_when_start").val(result.session_when_start);
                    // showErrorMessage(result.message);
                }
                if(result.all_dates != ""){
                    $(".next_btn").show();
                }else{
                    $(".next_btn").hide();
                }


                $("#session_text_info").html(result.session_text_info);
                $("#studenttuitionbooking-session_text_info").val(result.session_text_info);
                $("#studenttuitionbooking-all_dates").val(result.all_dates);

                $("#studenttuitionbooking-session_when_start").datepicker({startDate:"'.General::displayDate(date('Y-m-d')).'",daysOfWeekDisabled: result.disabled_day });
                ULTRA_SETTINGS.iCheck();

                if($("#studenttuitionbooking-session_when_start").val() != ""){
                        $(".field-studenttuitionbooking-session_when_start").removeClass("has-error");
                        $(".field-studenttuitionbooking-session_when_start").find(".help-block").html("");
                    }
                unblockBody();
            },error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
    }
}

function get_day_start_time(dy){
    if(dy !== undefined){
    $.ajax({
            type: "GET",
            url: "' . Url::to(['book-music-tution/reschedule-get-day-start-time']) . '",
            data: {"day":dy,"tutor_uuid":$("#studenttuitionbooking-tutor_uuid").val(),"student_uuid":$("#studenttuitionbooking-student_uuid").val(),"tutorial_type_code":$("#studenttuitionbooking-tutorial_type_code").val()},
            async:false,
            success:function(result){
                $("#studenttuitionbooking-session_when_start").datepicker("remove");
               $("#studenttuitionbooking-start_hour").val(result.time);
               $("#studenttuitionbooking-session_when_start").val(result.session_when_start);
               $("#studenttuitionbooking-session_when_start").datepicker({startDate:result.session_when_start,daysOfWeekDisabled: result.disabled_day,format:"dd-mm-yyyy" });
               ULTRA_SETTINGS.iCheck();
            },error:function(e){
                showErrorMessage(e.responseText);
            }
        });
    }
}


function get_daywise_time(dy,date,typ){
    if(dy !== undefined){
    // $("#studenttuitionbooking-start_hour").css({"display":"inline"});
    $.ajax({
            type: "GET",
            url: "' . Url::to(['book-music-tution/get-tutor-available-hours']) . '?tutor_uuid='.$tutor_uuid.'&session_minute='.$session_minute.'",
            data: {"day":dy,"date":date,"typ":typ},
            async:false,
            success:function(result){
                if(result.time == ""){
                    $("#studenttuitionbooking-start_hour").css({"display":"none"});
                }
                $("#studenttuitionbooking-session_when_start").datepicker("remove");
                $("#studenttuitionbooking-start_hour").html("");
                $.each(result.time, function (key, value) {

                    $("#studenttuitionbooking-start_hour").append("<option value=\'"+key+"\'>"+value+"</option>");
                });
                $("#studenttuitionbooking-session_when_start").val(result.session_when_start);
                // $("#studenttuitionbooking-session_when_start").datepicker({startDate:result.session_when_start,daysOfWeekDisabled: result.disabled_day,datesOfWeekDisabled: result.disabled_date,format:"dd-mm-yyyy" });
                $("#studenttuitionbooking-session_when_start").datepicker({startDate:date,daysOfWeekDisabled: result.disabled_day,datesOfWeekDisabled: result.disabled_date,format:"dd-mm-yyyy" });
                ULTRA_SETTINGS.iCheck();
            },error:function(e){
                showErrorMessage(e.responseText);
            }
        });
    }
}

function week_day_change(dy){
    // get_day_start_time(dy);
    var date = $("#studenttuitionbooking-session_when_start").val();
    get_daywise_time(dy,date,"day");
    var tm = $("#studenttuitionbooking-start_hour").val();
    var whendate = $("#studenttuitionbooking-session_when_start").val();
    load_daywise_changes(dy,tm,whendate);
}

function hour_change(){
    var dayvalue = $("input[name=\"StudentTuitionBooking[session_week_day]\"]:checked").val();
    var tm = $("#studenttuitionbooking-start_hour").val();
    var whendate = $("#studenttuitionbooking-session_when_start").val();

    load_daywise_changes(dayvalue,tm,whendate);
}

var send=true;
function date_change(obj){
    var weekday=new Array(7);
    weekday[0]="monday";
    weekday[1]="tuesday";
    weekday[2]="wednesday";
    weekday[3]="thursday";
    weekday[4]="friday";
    weekday[5]="saturday";
    weekday[6]="sunday";

    if(send){

    var date = $(obj).datepicker("getDate");
    var dayOfWeek = weekday[date.getUTCDay()];

    var w_str = "week_day_"+dayOfWeek;
    $("."+w_str).prop("checked", true);
    $("."+w_str).parent("label").addClass("active");

    var dayvalue = $("input[name=\"StudentTuitionBooking[session_week_day]\"]:checked").val();

    var tm = $("#studenttuitionbooking-start_hour").val();
    var date_new = convert(date);
    var whendate = $("#studenttuitionbooking-session_when_start").val();

    send=false;
    get_daywise_time(dayOfWeek,date_new,"date");
    load_daywise_changes(dayOfWeek,tm,whendate);

    }
    setTimeout(function(){send=true;},200);
}

function convert(str) {
    var date = new Date(str),
        mnth = ("0" + (date.getMonth()+1)).slice(-2),
        day  = ("0" + date.getDate()).slice(-2);
    return [day, mnth, date.getFullYear()].join("-");
}

function save_reschedule_approve_form(){

            $.ajax({
            type: "POST",
            url: $("#reschedule_approve_form_action").val(),
            data: {"is_approval": "yes"},
            beforeSend: function() {
                blockBody();
            },
            success:function(result){
              console.log(result);
                if(result.code == 200){
                  console.log("if", result);
                    $("#reschedule_approval_form_parent").html("");
                    window.location.href= "'.Url::to(['/booked-session/upcoming/']).'";
                    showSuccess(result.message);
                }else{
                  console.log("else", result);
                    $("#reschedule_approval_form_parent").html(result);
                    ULTRA_SETTINGS.iCheck();
                }
                unblockBody();
            },
            error:function(e){
              console.log(e.responseText);
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
}

function save_reschedule_reject_form(){

            $.ajax({
            type: "POST",
            url: $("#reschedule_approve_form_action").val(),
            data: {"is_approval": "no"},
            beforeSend: function() {
                blockBody();
            },
            success:function(result){
                if(result.code == 200){
                    $("#reschedule_approval_form_parent").html("");
                    window.location.href= "'.Url::to(['/booked-session/upcoming/']).'";
                    showSuccess(result.message);
                }else{
                    $("#reschedule_approval_form_parent").html(result);
                    ULTRA_SETTINGS.iCheck();
                }
                unblockBody();
            },
            error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
}

function reschedlue_lesson_request_cancel(id){
    if(confirm("Are you sure, you want to cancel this lesson reschedulled request?")){
        $.ajax({
            type: "POST",
            url: "' . Url::to(['booked-session/student-reschedule-lesson-request-cancel']) . '?id="+id,
            data: {"is_cancel": "yes"},
            beforeSend: function() {
                blockBody();
            },
            success:function(result){
                console.log(result);
                if(result.code == 200){
                    $("#reschedlue_lesson_request_cancel_parent").html("");
                    window.location.href= "'.Url::to(['/booked-session/upcoming/']).'";
                    showSuccess(result.message);
                }else{
                    $("#reschedlue_lesson_request_cancel_parent").html(result);
                    ULTRA_SETTINGS.iCheck();
                }
                unblockBody();
            },
            error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
        blockBody();
    } else {
        unblockBody();
        return false;
    }

}

    ', View::POS_END);
?>
<div class="row margin-0">
    <div class="col-xl-4 col-lg-4 col-md-12 col-12">
        <section class="box ">
            <ul class="list-group">
                <li class="list-group-item active"><h5 class="m-0 text-white" style="font-weight:600;">Lesson Information</h5></li>
                <li class="list-group-item list-group-item-danger"><span class="">Lesson Date</span><span class=" bg-danger float-right"><?= General::displayDate($modelBS->start_datetime); ?></span> </li>
                <li class="list-group-item list-group-item-success" ><span class="">Lesson Time</span><span class=" bg-success float-right"><?= General::displayTime($modelBS->start_datetime) . ' - ' . General::displayTime($modelBS->end_datetime); ?></span> </li>
                <li class="list-group-item " style="background-color: #dccde6;" ><span class="">Lesson Duration</span><span class=" bg-purple float-right"><?= $modelBS->session_min . ' Minutes'; ?></span> </li>
                <li class="list-group-item list-group-item-info" ><span class="">Instrument</span><span class=" bg-info float-right"><?= $modelBS->instrumentUu->name; ?></span> </li>
                <li class="list-group-item " style="background-color: #f3d7cf;"><span class="">Student Name</span><span class=" bg-orange float-right"><?= $modelBS->studentUu->first_name; ?></span> </li>
            </ul>
        </section>
    </div>

    <div class="col-xl-8 col-lg-8 col-md-12 col-12">

        <?php if(!empty($rescheduled_awaiting_entry)) { ?>

            <section class="box ">
                <header class="panel_header">
                    <h2 class="title float-left">Rescheduled Lesson Request</h2>
                    <div class="actions panel_actions float-right">
                        <a href="<?= Yii::$app->getUrlManager()->createUrl(['/booked-session/upcoming/']) ?>" class="btn btn-info btn-icon "><i class="fa fa-arrow-left text-white"></i> <span>Back</span></a>
                    </div>
                </header>
                <div class="content-body">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-12">
                            <div class="row">

                                <?php $div_class = (Yii::$app->user->identity->role == 'STUDENT') ? "col-lg-12 col-md-12 col-12" : "col-lg-6 col-md-6 col-6"; ?>
                                <div class="<?= $div_class ?>">
                                    <table class="table table table-bordered">
                                        <thead>
                                            <tr>
                                                <th colspan="2" class="bg-danger">Reschedule Request Information</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td style="width:40%">Lesson Date</td>
                                                <td style="width:60%"><?= General::displayDate($rescheduled_awaiting_entry->to_start_datetime); ?></td>
                                            </tr>
                                            <tr>
                                                <td>Lesson Time</td>
                                                <td><?= General::displayTime($rescheduled_awaiting_entry->to_start_datetime) . ' - ' . General::displayTime($rescheduled_awaiting_entry->to_end_datetime); ?></td>
                                            </tr>
                                            <tr>
                                                <td>Requested Date</td>
                                                <td><?= General::displayDateTime($rescheduled_awaiting_entry->created_at); ?></td>
                                            </tr>
                                            <tr>
                                                <td>Requested By</td>
                                                <td><?= $rescheduled_awaiting_entry->request_by; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-lg-5 col-md-5 col-5">

                                    <div id="reschedule_approval_form_parent">
                                        <?php if(Yii::$app->user->identity->role != 'STUDENT') { ?>
                                        <?=
                                        $this->render('_reschedule_approval_form', [
                                            'model' => $rescheduled_awaiting_entry,
                                        ])
                                        ?>
                                        <?php } ?>
                                    </div>
                                </div>

                                <?php if(Yii::$app->user->identity->role == 'STUDENT') { ?>
                                    <div class="col-lg-6 col-md-6 col-6">

                                        <div id="reschedlue_lesson_request_cancel_parent">
                                            <button type="button" class="btn btn-danger" onclick="reschedlue_lesson_request_cancel('<?= $modelBS->uuid ?>');">Cancel Reschedule Lesson Request</button>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            </div>
                    </div>
                </div>
            </section>

        <?php } else { ?>

            <section class="box ">
                <header class="panel_header">
                    <h2 class="title float-left">Reschedule Lesson</h2>
                    <div class="actions panel_actions float-right">
                        <a title="Availability" class="btn btn-orange btn-icon tutor_availability_calender_btn" data-id="<?= $modelBS->tutorUu->uuid?>" data-tutorname="<?= $modelBS->tutorUu->first_name. ' '.$modelBS->tutorUu->last_name ?>"><i class="fa fa-calendar text-white"></i></a>
                        <a href="<?= Yii::$app->getUrlManager()->createUrl(['/booked-session/upcoming/']) ?>" class="btn btn-info btn-icon"><i class="fa fa-arrow-left text-white"></i> <span>Back</span></a>
                    </div>
                </header>
                <div class="content-body">
                    <div class="row">
                         <div class="col-lg-12 col-md-12 col-12">
                        <?php

                        $action = (Yii::$app->user->identity->role == 'STUDENT') ? ['reschedule-request','id' => $modelBS->uuid] : ['reschedule','id' => $modelBS->uuid];
                        $form = ActiveForm::begin([
                                    'action' => $action,
                                    'id' => 'frm_step_4',
                                    'enableClientValidation' => true,
                                    //'enableAjaxValidation' => true,
                                    'validateOnChange' => true,
                                    'options' => ['class' => 'form-horizontal']]);
                        ?>

                        <input type="hidden" name="step" value="4">
                        <?= $form->field($model, 'all_dates')->hiddenInput(['class' => 'form-control', 'maxlength' => true,])->label(false) ?>
                        <?= $form->field($model, 'session_text_info')->hiddenInput(['class' => 'form-control', ])->label(false) ?>

                        <div class="my-inline-button" >
                            <div class=" text-center" >
                                <ul class="my-inline-button-filter list-inline btn-group btn-group-toggle " data-toggle="buttons">
                                    <li>
                                        <label class="btn <?= ($tutor_working_plan['sunday'] == '') ? "btn-secondary" : "btn-primary" ?> <?= ($model->session_week_day == 'sunday') ? "active" : "" ?>" onclick="week_day_change('sunday')" >
                                        <input type="radio" name="StudentTuitionBooking[session_week_day]" class="session_week_day week_day_sunday" autocomplete="off" value="sunday" <?= ($model->session_week_day == 'sunday') ? "checked" : "" ?> >Sun
                                        </label>
                                    </li>
                                    <li>
                                        <label class="btn <?= ($tutor_working_plan['monday'] == '') ? "btn-secondary" : "btn-primary" ?> <?= ($model->session_week_day == 'monday') ? "active" : "" ?>" onclick="week_day_change('monday')" >
                                        <input type="radio" name="StudentTuitionBooking[session_week_day]" class="session_week_day week_day_monday" autocomplete="off" value="monday" <?= ($model->session_week_day == 'monday') ? "checked" : "" ?> >Mon
                                    </label>
                                    </li>
                                    <li>
                                        <label class="btn <?= ($tutor_working_plan['tuesday'] == '') ? "btn-secondary" : "btn-primary" ?> <?= ($model->session_week_day == 'tuesday') ? "active" : "" ?>" onclick="week_day_change('tuesday')" >
                                        <input type="radio" name="StudentTuitionBooking[session_week_day]" class="session_week_day week_day_tuesday" autocomplete="off" value="tuesday" <?= ($model->session_week_day == 'tuesday') ? "checked" : "" ?> >Tue
                                    </label>
                                    </li>
                                    <li>
                                        <label class="btn <?= ($tutor_working_plan['wednesday'] == '') ? "btn-secondary" : "btn-primary" ?> <?= ($model->session_week_day == 'wednesday') ? "active" : "" ?>" onclick="week_day_change('wednesday')" >
                                        <input type="radio" name="StudentTuitionBooking[session_week_day]" class="session_week_day week_day_wednesday" autocomplete="off" value="wednesday" <?= ($model->session_week_day == 'wednesday') ? "checked" : "" ?> >Wed
                                    </label>
                                    </li>
                                    <li>
                                        <label class="btn <?= ($tutor_working_plan['thursday'] == '') ? "btn-secondary" : "btn-primary" ?> <?= ($model->session_week_day == 'thursday') ? "active" : "" ?>" onclick="week_day_change('thursday')" >
                                        <input type="radio" name="StudentTuitionBooking[session_week_day]"  class="session_week_day week_day_thursday" autocomplete="off" value="thursday" <?= ($model->session_week_day == 'thursday') ? "checked" : "" ?> >Thu
                                    </label>
                                    </li>
                                    <li>
                                        <label class="btn <?= ($tutor_working_plan['friday'] == '') ? "btn-secondary" : "btn-primary" ?> <?= ($model->session_week_day == 'friday') ? "active" : "" ?>" onclick="week_day_change('friday')" >
                                        <input type="radio" name="StudentTuitionBooking[session_week_day]" class="session_week_day week_day_friday" autocomplete="off" value="friday" <?= ($model->session_week_day == 'friday') ? "checked" : "" ?> >Fri
                                    </label>
                                    </li>
                                    <li>
                                        <label class="btn <?= ($tutor_working_plan['saturday'] == '') ? "btn-secondary" : "btn-primary" ?> <?= ($model->session_week_day == 'saturday') ? "active" : "" ?>" onclick="week_day_change('saturday')" >
                                        <input type="radio" name="StudentTuitionBooking[session_week_day]" class="session_week_day week_day_saturday" autocomplete="off" value="saturday" <?= ($model->session_week_day == 'saturday') ? "checked" : "" ?> >Sat
                                    </label>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-12 booking_search_data">
                                <div class="tab-pane fade show active " >

                                    <div id="session_list_parent" class="text-centre">
                                        <div class="card " >
                                            <div class="card-body">

                                                <div class="form-row align-items-center">
                                                    <div class="col-lg-2 col-md-2 col-2"></div>
                                                    <div class="col-auto">
                                                        <label class="" style="margin-top: -10px;vertical-align: top;" for="inlineFormInput">When</label>
                                                    </div>
                                                    <div class="col-auto">
                                                        <?php $model->session_when_start = General::displayDate($model->session_when_start); ?>
                                                        <?=
                                                        $form->field($model, 'session_when_start', ['template' => "{label}\n{input}\n{hint}\n{error}",
                                                            'labelOptions' => ['class' => 'control-label']])->textInput(
                                                                ['class' => 'form-control when-datepicker', 'data-date-format' =>'dd-mm-yyyy','data-disabled-days' => '','data-start-date' => '0d','onchange' => 'date_change(this);','readonly' => true])->label(false)
                                                        ?>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="col-auto">
                                                        <?=
                                                        $form->field($model, 'start_hour', ['template' => "{input}{error}",
                                                            'labelOptions' => ['class' => 'control-label1']])->dropDownList(
                                                                $start_hour_array, ['class' => 'form-control select2','onchange' => 'hour_change();'])->label(false)
                                                        ?>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div id="session_text_info" style="width: 100%;text-align: center;"><?= $model->session_text_info; ?></div>

                                                        <?=
                                                        $form->field($model, 'tutor_uuid', ['template' => "{input}\n{error}",
                                                            'labelOptions' => ['class' => 'control-label']])->hiddenInput(['class' => 'form-control', ])->label(false)
                                                        ?>
                                                        <?=
                                                        $form->field($model, 'student_uuid', ['template' => "{input}\n{error}",
                                                            'labelOptions' => ['class' => 'control-label']])->hiddenInput(['class' => 'form-control', ])->label(false)
                                                        ?>
                                                        <?=
                                                        $form->field($model, 'tutorial_type_code', ['template' => "{input}\n{error}",
                                                            'labelOptions' => ['class' => 'control-label']])->hiddenInput(['class' => 'form-control', ])->label(false)
                                                        ?>

                                                    <?php $displayStyle = ($model->all_dates != "") ? "display:block;" : "display:none;"; ?>
                                                    <div class="col-lg-12 col-md-12 col-12 text-center">
                                                        <?= Html::submitButton('Save Lesson', ['class' => "btn btn-orange btn-md next_btn pull-right", "style" => "$displayStyle"]) ?>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                    </div>
                </div>
            </section>
        <?php } ?>
    </div>
</div>
