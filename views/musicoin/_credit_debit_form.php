<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\web\View;
use yii\helpers\ArrayHelper;
use app\models\Student;

/* @var $this yii\web\View */
/* @var $model app\models\MusicoinLog */
/* @var $form yii\widgets\ActiveForm */

$studentArray = ArrayHelper::map(Student::find()->where(['status' => 'ENABLED'])->orderBy('first_name,last_name asc')->all(), 'student_uuid', function ($s) {
            return $s->first_name . ' ' . $s->last_name;
        });
$this->registerJs(
        ' 
            $(document).ready(function() {
                $(".myselect2_modal").select2({
                        placeholder: "Select...",
                        allowClear: true,
                        dropdownParent: $("#credit_debit_modal .modal-content")

                }).on("select2-open", function() {
                // Adding Custom Scrollbar
                //$(this).data("select2").results.addClass("overflow-hidden").perfectScrollbar();
                $(this).data("select2").results.addClass("overflow-hidden");
                const pssel2ex1 = new PerfectScrollbar(".select2-results", {
                            suppressScrollX: true
                });
            });
            });
');
?>

<style>
/*.select2-container--open{
        z-index:9999999         
    }*/

/*.select2-drop{
    z-index: 10000;
}*/
    </style>

<?php $form = ActiveForm::begin(['options' => ['id' => 'credit_debit_form', 'enableClientValidation' => true, 'enableAjaxValidation' => true, 'validateOnChange' => true,]]); ?>

<?= $form->field($model, 'student_uuid')->dropDownList($studentArray, ['class' => 'form-control1 myselect2_modal', 'prompt' => 'Select Student', 'style' => 'width: 100%']) ?>

<?= $form->field($model, 'type')->dropDownList(['CREDIT' => 'CREDIT', 'DEBIT' => 'DEBIT'], ['class' => 'form-control ', 'prompt' => 'Select Type', 'style' => 'width: 100%']) ?>

<?= $form->field($model, 'coins')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('coins')]) ?>

<?= $form->field($model, 'summary')->textArea(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('summary')]) ?>

<?php ActiveForm::end(); ?>
        
