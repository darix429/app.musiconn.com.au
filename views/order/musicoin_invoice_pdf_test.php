<?php

use yii\helpers\Html;
use app\libraries\General;
use app\models\Student;
?>

<section class="box ">
    <div class="content-body">    
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-12" >
                        <div class="invoice-head"  >
                            <div style="float:left" >
                                <img src="<?= Yii::$app->params['theme_assets']['url'] . 'images/pdf_logo.png' ?>"  width="150px">
                            </div>
                        </div>
                        
                    </div>
                    <div class="clearfix"></div>
                    <div class="spacer"></div>
                </div>

                <div class="row cmp_info_row middle_row">
                    <div class="col-lg-1 col-md-1 col-1" ></div>                        
                    <div class="col-lg-10 col-md-10 col-10">                        
                        <table class="info_tbl">                                
                            <tr >
                                <td class="info_tbl_td_bold">Musiconn Pty Ltd</td>
                            </tr>
                            <tr >
                                <td class="info_tbl_td">ABN:  72 614 882 003</td>
                            </tr>
                            <tr >
                                <td class="info_tbl_td">408/27 Lonsdale Street</td>
                            </tr>
                            <tr >
                                <td class="info_tbl_td">Braddon  ACT  2612</td>
                            </tr>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                    <div class="spacer"></div>
                </div>

                <div class="row cust_row middle_row">
                    <div class="col-lg-1 col-md-1 col-1" ></div>                        
                    <div class="col-lg-10 col-md-10 col-10">                        
                        <table class="cust_tbl">                                
                            <tr >
                                <td class="cust_tbl_td" colspan="2">&lt;Customer Full Name&gt;</td>
                            </tr>
                            <tr >
                                <td class="cust_tbl_td" colspan="2">&lt;Student ID&gt;</td>
                            </tr>
                            <tr >
                                <td class="cust_tbl_td" colspan="2">&lt;Customer Address Line 1&gt;</td>
                            </tr>
                            <tr >
                                <td class="cust_tbl_td">&lt;City&gt; &lt;State&gt; &lt;Postcode&gt;</td>
                                <td class="cust_tbl_td_big_title">Recipient Created Tax Invoice</td>                                
                            </tr>
                            <tr >
                                <td class="cust_tbl_td_right" colspan="2">Invoice # &lt;Insert Invoice No.&gt;</td>
                            </tr>
                            <tr >
                                <td class="cust_tbl_td_right" colspan="2">Invoice Date # &lt;Insert Invoice Date.&gt;</td>
                            </tr>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                    <div class="spacer"></div>
                </div>


                <div class="row item_row middle_row">
                    <div class="col-lg-1 col-md-1 col-1" ></div>                        
                    <div class="col-lg-10 col-md-10 col-10">                        
                        <table class="item_tbl">
                            <thead> 
                                <tr >
                                    <td class="item_tbl_th text-center" width="110px">Transaction ID</td>
                                    <td class="item_tbl_th">Description</td>
                                    <td class="item_tbl_th text-right" width="80px">Total</td>
                                </tr>
                            </thead>
                            <tbody>                    
                                <tr class="cls_table_tr">
                                    <td class="item_tbl_td text-center" >TRN0123</td>
                                    <td class="item_tbl_td">Musicoins Package:  200 Purchased Coins + 5 Bonus Coins</td>
                                    <td class="item_tbl_td text-right" >$ 100.00</td>
                                </tr>
                                <tr class="cls_table_tr">
                                    <td class="item_tbl_td text-center" >TRN0123</td>
                                    <td class="item_tbl_td">Musicoins Package:  200 Purchased Coins + 5 Bonus Coins</td>
                                    <td class="item_tbl_td text-right" >$ 100.00</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                    <div class="spacer"></div>
                </div>

                <div class="row total_row middle_row">
                    <div class="col-lg-1 col-md-1 col-1" ></div>                        
                    <div class="col-lg-10 col-md-10 col-10">                        
                        <table class="total_tbl">
                            <tbody>                    
                                <tr class="">
                                    <td width="110px"></td>
                                    <td class="total_tbl_td">Price includes:</td>
                                    <td width="80px"></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td class="total_tbl_td" style="padding-left:50px;">10% GST</td>
                                    <td class="total_tbl_td text-right total_price_td">9.09</td>
                                </tr>
                                <tr class="">
                                    <td></td>
                                    <td class="total_tbl_td" style="padding-left:50px;">Discount Applied in Bonus Coins</td>
                                    <td class="total_tbl_td text-right total_price_td" >2.05</td>
                                </tr>
                            </tbody>
                        </table>
                        
<!--                        <table class="total_tbl">
                            <tbody>                    
                                <tr class="">
                                    <td width="110px"></td>
                                    <td class="total_tbl_td text-right" >Subtotal Musicoins</td>
                                    <td class="total_tbl_td text-right total_price_td" width="80px">200</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td class="total_tbl_td text-right" >Discount Musicoins</td>
                                    <td class="total_tbl_td text-right total_price_td">50</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td class="total_tbl_td text-right" >Total Musicoins</td>
                                    <td class="total_tbl_td text-right total_price_td">150</td>
                                </tr>
                                
                            </tbody>
                        </table>-->
                    </div>
                    <div class="clearfix"></div>
                    <div class="spacer"></div>
                </div>



            </div>
        </div>
    </div>
</section>

