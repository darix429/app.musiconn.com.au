<?php

namespace app\controllers;

use Yii;
use app\models\StudentViewRecordedLesson;
use app\models\StudentViewRecordedLessonSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * StudentViewRecordedLessonController implements the CRUD actions for StudentViewRecordedLesson model.
 */
class StudentViewRecordedLessonController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => Yii::$app->user->can('/student-view-recorded-lesson/index'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['view'],
                        'allow' => Yii::$app->user->can('/student-view-recorded-lesson/view'),
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all StudentViewRecordedLesson models.
     * @return mixed
     */
    public function actionIndex() {

        $searchModel = new StudentViewRecordedLessonSearch();
        $lessonList = $searchModel->search(Yii::$app->request->queryParams, true);
        $searchModel->load(Yii::$app->request->queryParams);
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_recorded_lesson_list', [
                        'lessonList' => $lessonList,
                'searchModel' => $searchModel,
            ]);
        } else {
            return $this->render('index', [
                        //'inactiveAdmin' => $inactiveAdmin,
                        'lessonList' => $lessonList,
                        'searchModel' => $searchModel,
            ]);
        }
    }

    /**
     * Displays a single StudentViewRecordedLesson model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id = '') {

        $searchModel = new StudentViewRecordedLessonSearch();
        if(isset($id) || !empty($id))
        {
        $searchModel->booked_session_uuid = $id;
        }
        $lessonList = $searchModel->searchDetail(Yii::$app->request->queryParams, true);

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_lesson_detail_list', [
                        'lessonList' => $lessonList,
            ]);
        } else {
            return $this->render('lesson_detail', [
                        //'inactiveAdmin' => $inactiveAdmin,
                        'lessonList' => $lessonList,
                        'searchModel' => $searchModel,
            ]);
        }
    }

    /**
     * Finds the StudentViewRecordedLesson model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return StudentViewRecordedLesson the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = StudentViewRecordedLesson::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

}
