<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SessionRecordedVideo;

/**
 * SessionRecordedVideoSearch represents the model behind the search form of `app\models\SessionRecordedVideo`.
 */
class SessionRecordedVideoSearch extends SessionRecordedVideo
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'booking_uuid', 'credit_session_uuid', 'student_uuid', 'tutor_uuid', 'start_datetime', 'end_datetime', 'filename', 'session_pin', 'status', 'cancel_by', 'cancel_user_uuid', 'cancel_reason', 'cancel_datetime', 'created_at', 'updated_at', 'publish_date'], 'safe'],
            [['session_min', 'student_login_flag', 'tutor_login_flag'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $modeDatatable = false, $statusnot = '', $where = '')
    {
        $query = SessionRecordedVideo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        if(!empty($where)){
            $query->andWhere($where);
        }

	if (!empty($statusnot)) {
            if (is_array($statusnot)) {
                $query->andWhere(['not in', 'status', $statusnot]);
            } else {
                $query->andWhere(['<>', 'status', $statusnot]);
            }
        }
	$query->andFilterWhere(['status' => $this->status]);
        

        // grid filtering conditions
        $query->andFilterWhere([
            'start_datetime' => $this->start_datetime,
            'end_datetime' => $this->end_datetime,
            'session_min' => $this->session_min,
            'cancel_datetime' => $this->cancel_datetime,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'student_login_flag' => $this->student_login_flag,
            'tutor_login_flag' => $this->tutor_login_flag,
            'publish_date' => $this->publish_date,
        ]);

        $query->andFilterWhere(['ilike', 'uuid', $this->uuid])
            ->andFilterWhere(['ilike', 'booking_uuid', $this->booking_uuid])
            ->andFilterWhere(['ilike', 'credit_session_uuid', $this->credit_session_uuid])
            ->andFilterWhere(['ilike', 'student_uuid', $this->student_uuid])
            ->andFilterWhere(['ilike', 'tutor_uuid', $this->tutor_uuid])
            ->andFilterWhere(['ilike', 'filename', $this->filename])
            ->andFilterWhere(['ilike', 'session_pin', $this->session_pin])
           // ->andFilterWhere(['ilike', 'status', $this->status])
            ->andFilterWhere(['ilike', 'cancel_by', $this->cancel_by])
            ->andFilterWhere(['ilike', 'cancel_user_uuid', $this->cancel_user_uuid])
            ->andFilterWhere(['ilike', 'cancel_reason', $this->cancel_reason]);

        if ($modeDatatable) {
            //echo $query->createCommand()->getRawSql();exit;
            $result = $query->asArray()->orderBy('updated_at')->all();
            return $result;
        }
        return $dataProvider;
    }
}
