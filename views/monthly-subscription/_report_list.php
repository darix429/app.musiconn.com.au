<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">

    <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <!--<th>Subscription</th>-->
                <th>Customer ID</th>
                <th>Customer Name</th>
                <th>Order ID</th>
                <th>Purchase Date</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Subscription Payment($)</th>
                <th>GST($)</th>
                <th>Total Payment($)</th>
                <th>With Tutorial</th>
            </tr>
        </thead>

        <tbody>
            <?php
            if (!empty($monthlySubList)) {
                foreach ($monthlySubList as $key => $value) {
                    //StudentMonthlySubscription::getOrderID();
                    ?>
                    <tr>
                        <td><?= $key + 1; ?></td>
                        <!--<td><?= $value['name']; ?></td>-->
                        <td><?= $value['studentUu']['enrollment_id']; ?></td>
                        <td><?= $value['studentUu']['first_name'] . " " . $value['studentUu']['last_name']; ?></td>
        <!--                        <td><?= (!empty($value['orderUu']['order_id'])) ? "#" . $value['orderUu']['order_id'] : ''; ?></td>-->
                        <td><a target="_blank" href="<?php echo Yii::$app->params['media']['invoice']['url'] . $value['orderUu']['invoice_file']; ?>"><?= (!empty($value['orderUu']['order_id'])) ? "#" . $value['orderUu']['order_id'] : ''; ?></a></td>
                        <td><?= \app\libraries\General::displayDate($value['created_at']); ?></td>
                        <td><?= \app\libraries\General::displayDate($value['start_datetime']); ?></td>
                        <td><?= \app\libraries\General::displayDate($value['end_datetime']); ?></td>
                        <td><?= $value['price']; ?></td>
                        <td><?= $value['tax']; ?></td>
                        <td><?= $value['total_price']; ?></td>
                        <td><?= ($value['with_tutorial'] == '1') ? "Yes" : "No"; ?></td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>

</div>
