<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cancel_session".
 *
 * @property string $uuid
 * @property string $booked_session_uuid
 * @property string $student_uuid
 * @property string $tutor_uuid
 * @property string $cancel_by
 * @property string $canceler_uuid
 * @property string $approved_by
 * @property string $approver_uuid
 * @property string $approve_datetime
 * @property string $rejected_by
 * @property string $rejecter_uuid
 * @property string $reject_datetime
 * @property string $status
 * @property string $cancel_note
 * @property string $created_at
 *
 * @property BookedSession $bookedSessionUu
 * @property Student $studentUu
 * @property Tutor $tutorUu
 */
class CancelSession extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cancel_session';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'booked_session_uuid', 'student_uuid', 'tutor_uuid'], 'required'],
            [['uuid', 'booked_session_uuid', 'student_uuid', 'tutor_uuid', 'cancel_by', 'canceler_uuid', 'approved_by', 'approver_uuid', 'rejected_by', 'rejecter_uuid', 'status', 'cancel_note'], 'string'],
            [['approve_datetime', 'reject_datetime', 'created_at'], 'safe'],
            [['uuid'], 'unique'],
            [['booked_session_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => BookedSession::className(), 'targetAttribute' => ['booked_session_uuid' => 'uuid']],
            [['student_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['student_uuid' => 'student_uuid']],
            [['tutor_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Tutor::className(), 'targetAttribute' => ['tutor_uuid' => 'uuid']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uuid' => 'Uuid',
            'booked_session_uuid' => 'Booked Session Uuid',
            'student_uuid' => 'Student Uuid',
            'tutor_uuid' => 'Tutor Uuid',
            'cancel_by' => 'Cancel By',
            'canceler_uuid' => 'Canceler Uuid',
            'approved_by' => 'Approved By',
            'approver_uuid' => 'Approver Uuid',
            'approve_datetime' => 'Approve Datetime',
            'rejected_by' => 'Rejected By',
            'rejecter_uuid' => 'Rejecter Uuid',
            'reject_datetime' => 'Reject Datetime',
            'status' => 'Status',
            'cancel_note' => 'Cancel Note',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookedSessionUu()
    {
        return $this->hasOne(BookedSession::className(), ['uuid' => 'booked_session_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentUu()
    {
        return $this->hasOne(Student::className(), ['student_uuid' => 'student_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTutorUu()
    {
        return $this->hasOne(Tutor::className(), ['uuid' => 'tutor_uuid']);
    }
}
