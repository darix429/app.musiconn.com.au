<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\ArrayHelper;
use app\models\InstrumentCategory;
use app\models\Instrument;
use yii\helpers\Url;
use app\models\TutorInstrument;

$instrumentCategoryArray = ArrayHelper::map(InstrumentCategory::find()->asArray()->all(), 'uuid', 'name');

if(in_array(Yii::$app->user->identity->role, ['OWNER', 'ADMIN', 'SUBADMIN'])){
    $instrumentArray = ArrayHelper::map(Instrument::find()->where(['status' => 'ACTIVE'])->asArray()->all(), 'uuid', 'name');
}else{
    $tutorInstrumentModel = \app\models\TutorInstrument::find()->where(['tutor_uuid' => Yii::$app->user->identity->tutor_uuid])->asArray()->all();
    $old_t_ins = array_column($tutorInstrumentModel, 'instrument_uuid');

    $instrumentArray = ArrayHelper::map(Instrument::find()->where(['in', 'uuid', $old_t_ins])->asArray()->all(), 'uuid', 'name');
}
?>

<style>
    .video-box{
        border-radius: 6px;
        border: 1px dashed #b5aaaa; 
        width: fit-content;
        margin-left: 281px;
        text-align: center;
    }

    #prerecordedvideo-file{
        width: 294px;
        height: 271px;
        cursor: pointer;
    }
    .user-img{
        margin-top:21px;
    }
    .fileUpload {
        position: relative;
        overflow: hidden;
        margin: 10px;
        border: 2px dotted;
        height:262px;
    }
    .fileUpload input.upload {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }
</style>

<?php

$form = ActiveForm::begin([
            'action' => ['upload'],
//                'validationUrl' => ['uploadvalidation'],
            'id' => 'upload_form',
//                'enctype' => 'multipart/form-data',
//                'enableClientValidation' => true,
//                'enableAjaxValidation' => true,
//                'validateOnChange' => true,
            'options' => ['class' => 'form-horizontal']]);
?>


<div class="row margin-0">
    <div class="col-xl-12 col-lg-12 col-md-12 col-12">
        <section class="box ">

            <div class="content-body">    
                <div class="row">
                    <div class="col-lg-4 col-md-12 col-12 "  style="text-align: center">


                        <div class="fileUpload">
                            <div class="user-img">
                                <img src="/theme_assets/images/video-upload.jpg" alt="user-image" class="rounded-circle img-inline" style="width:104px;">
                            </div>
                            <h5>Drag and drop to</h5>
                            <h5>Upload</h5>
                            <?= $form->field($model, 'file')->fileInput(['class' => 'upload'])->label(false) ?>
                            <span class="text-primary" id="uploaded-video"></span>
                        </div>

                        <div class="progress progress-md">
                            <div id="progress-bar" class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                <span class="sr-only" id="progress-status">0%</span>
                            </div>
                        </div>
                        <div class="msg"></div>
                        <div class="spacer"></div>
                    </div>

                    <div class="col-lg-8 col-md-12 col-12">
                        <div class="form-row">
                            <div class="col-md-12 mb-0">
                                <?= $form->field($model, 'title')->textInput(['class' => 'form-control code', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('title')]) ?>

                            </div>

                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-0">
                                <?= $form->field($model, 'instrument_uuid')->dropDownList($instrumentArray, ['class' => 'form-control', 'prompt' => 'Select Instrument']) ?>
                            </div>
                            <div class="col-md-6 mb-0">
                                <?= $form->field($model, 'instrument_category_uuid')->dropDownList($instrumentCategoryArray, ['class' => 'form-control', 'prompt' => 'Select Instrument Category']) ?>
                            </div>
                        </div>
                        <!--<div class="form-row">
                            
                        <!--<div class="col-md-6 mb-0">
                        <?= $form->field($model, 'file')->fileInput(['class' => 'form-control']) ?>
                        </div>-->
                        <!--</div>-->
                        <div class="form-row">
                            <div class="col-md-12 mb-0">
                                <?= $form->field($model, 'description')->textarea(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('description')]) ?>
                            </div>
                        </div>

                        <?= Html::Button(Yii::t('app', 'Create'), ['class' => 'btn btn-primary', 'id' => 'savebutton', 'style' => 'float: right;']) ?>

                    </div>


                </div>
            </div>

        </section>
    </div>

</div>

</form>
<?php ActiveForm::end(); ?>

