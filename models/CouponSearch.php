<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Coupon;

/**
 * CouponSearch represents the model behind the search form of `app\models\Coupon`.
 */
class CouponSearch extends Coupon
{
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'code', 'description', 'type', 'code_type', 'start_date', 'end_date', 'status', 'created_at', 'updated_at'], 'safe'],
            [['amount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $modeDatatable = false, $statusnot = '')
    {
        $query = Coupon::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

	if (!empty($statusnot)) {
            if (is_array($statusnot)) {

                $query->andWhere(['not in', 'status', $statusnot]);
            } else {

                $query->andWhere(['<>', 'status', $statusnot]);
            }
        }
	$query->andFilterWhere(['status' => $this->status]);
	
        // grid filtering conditions
        $query->andFilterWhere([
            //'amount' => $this->amount,
            //"to_char(start_date, 'YYYY-MM-DD')" => date('Y-m-d', strtotime($this->start_date)),
            //"to_char(end_date, 'YYYY-MM-DD')" => date('Y-m-d', strtotime($this->end_date)),
            "to_char(start_date, 'DD-MM-YYYY')" => $this->start_date,
            "to_char(end_date, 'DD-MM-YYYY')" => $this->end_date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'uuid', $this->uuid])
	    ->andFilterWhere(['ilike', 'CAST(amount AS TEXT)', $this->amount])
            ->andFilterWhere(['ilike', 'code', $this->code])
            ->andFilterWhere(['ilike', 'description', $this->description])
            ->andFilterWhere(['ilike', 'type', $this->type])
            ->andFilterWhere(['ilike', 'code_type', $this->code_type]);
           /// ->andFilterWhere(['ilike', 'status', $this->status]);
//echo $query->createCommand()->getRawSql();exit;


	if ($modeDatatable) {
            $result = $query->asArray()->orderBy('code')->all();
            return $result;
        }
        return $dataProvider;
    }
}
