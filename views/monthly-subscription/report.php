<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;

$this->title = Yii::t('app', 'Monthly Subscriptions Report');
$this->params['breadcrumbs'][] = $this->title;


$this->registerJs(
        '
    
$(document).on("click","#search_btn", function(){        
    $.ajax({
        type: "GET",
        url: "' . Url::to(['/monthly-subscription/report/']) . '",
        data: $("#search_frm").serialize(),
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            $("#coupon_table_parent_div").html(result);
            window.ULTRA_SETTINGS.dataTablesInit();
        },
        error:function(e){
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});
    
$(document).on("click","#search_reset_btn", function(){        
    $.ajax({
        type: "GET",
        url: "' . Url::to(['/monthly-subscription/report/']) . '",
        data: {},
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            $("#coupon_table_parent_div").html(result);
            window.ULTRA_SETTINGS.dataTablesInit();
        },
        error:function(e){
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});
    
    $(".datefilter").daterangepicker({
      autoUpdateInput: false,
      locale: {
          cancelLabel: "Clear",
          format: "DD-MM-YYYY"
      },ranges : {
                    "Today": [moment(), moment()],
                    "Yesterday": [moment().subtract("days", 1), moment().subtract("days", 1)],
                    "Last 7 Days": [moment().subtract("days", 6), moment()],
                    "Last 30 Days": [moment().subtract("days", 29), moment()],
                    "This Month": [moment().startOf("month"), moment().endOf("month")],
                    "Last Month": [moment().subtract("month", 1).startOf("month"), moment().subtract("month", 1).endOf("month")]
                }
  });

  $(".datefilter").on("apply.daterangepicker", function(ev, picker) {
      $(this).val(picker.startDate.format("DD-MM-YYYY") + " - " + picker.endDate.format("DD-MM-YYYY"));
  });

  $(".datefilter").on("cancel.daterangepicker", function(ev, picker) {
      $(this).val("");
  });
', View::POS_END
);
?>

<div class="col-xl-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Monthly Subscriptions History</h2>
            <div class="actions panel_actions float-right">
                <i class="fa fa-search icon-primary icon-xs icon-orange mail_head_icon custom-toggle" data-togglediv="datatable-search-div" title="Advance Search"></i>

            </div>
        </header>
        <div class="content-body"> 
            <div class="row ">
                <div class="col-lg-12 col-md-12 col-12 ">
                    <div class="datatable-search-div " style="display: none;">

                        <?php
                        $form = ActiveForm::begin([
                                    'method' => 'get',
                                    'options' => [
                                        'id' => 'search_frm',
                                        'class' => 'form-inline'
                                    ],
                        ]);
                        ?>

                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'student_name')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Customer Name'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'price')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Subscription Payment'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'tax')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'GST'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'total_price')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Total Payment'])->label(false) ?>
                        </div>

                        <!--div class="col-md-3 mb-0">
                        <?= $form->field($searchModel, 'start_datetime')->textInput(['class' => 'form-control custom_date_picker', 'maxlength' => true, 'placeholder' => 'Start Date'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                        <?= $form->field($searchModel, 'end_datetime')->textInput(['class' => 'form-control custom_date_picker', 'maxlength' => true, 'placeholder' => 'End Date'])->label(false) ?>
                        </div-->
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'start_daterange')->textInput(['class' => 'form-control datefilter', 'maxlength' => true, 'placeholder' => 'Purchase Date'])->label(false) ?>
                        </div><br><br>
                        <!--		                <div class="col-md-3 mb-0">
                        <?= $form->field($searchModel, 'status')->dropDownList(['ENABLED' => 'ENABLED', 'DISABLED' => 'DISABLED'], ['class' => 'form-control', 'style' => 'width:100%'])->label(false) ?>
                                                        </div>  -->

                        <?= Html::Button(Yii::t('app', 'Search'), ['class' => 'btn btn-primary mb-0', 'id' => 'search_btn', 'style' => 'margin-left: 17px;']) ?>
                        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-purple mb-0', 'id' => 'search_reset_btn']) ?>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
            <br>
            <div class=""></div>
            <div class="clearfix"></div>
            <div class="row" id="coupon_table_parent_div">
                <?php echo Yii::$app->controller->renderPartial('_report_list', ['monthlySubList' => $monthlySubList]); ?>
            </div>
        </div>
    </section> 
</div>

