<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;
use app\models\Tutor;
use app\models\BookedSession;
use app\libraries\General;


$this->title = Yii::t('app', 'Missed Lessons');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">
    <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Student</th>
                <th>Date</th>
                <th>Time</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (!empty($expiredList)) {
                foreach ($expiredList as $key => $value) {
                    $now = date('Y-m-d H:i:s');
                        ?>
                        <tr>
                            <td><?= $key + 1; ?></td>
                            <td><?= $value['studentUu']['first_name']. ' ' .$value['studentUu']['last_name']; ?></td>
                            <td><?= General::displayDate($value['start_datetime']); ?></td>
                            <td><?= General::displayTime($value['start_datetime']) ." - ".General::displayTime($value['end_datetime']); ?></td>
                        </tr>
                        <?php
                }
            }
            ?>
        </tbody>
    </table>
</div>