<?php

namespace app\controllers;

use Yii;
use app\models\Admin;
use app\models\AdminSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\User;
use app\libraries\General;
use app\libraries\Browser;
use yii\helpers\Url;
use yii\filters\AccessControl;
use app\models\ChangePassword;
use app\models\Test;
use app\models\Timezone;
use yii\web\UploadedFile;
use app\models\InstrumentCategory;
use app\models\PreRecordedVideo;
use app\models\PreRecordedVideoSearch;
use app\libraries\Mobile_Detect;
use app\libraries\MailChimpApi;
use kartik\mpdf\Pdf;
use yii\helpers\FileHelper;

/**
 * TestController implements the CRUD actions for Admin model.
 */
class TestController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                        [
                        'actions' => ['getvideo', 'set-old-lesson-tutor-rate'],
                        'allow' => true,
                        'roles' => ['OWNER', 'ADMIN', 'SUBADMIN'],
                    ],
                        [
                        'actions' => ['test-check-device', 'test-datepicker', 'movefile', 'existing-student-add-into-mailchimp', 'test-mail', 'test-invoice'],
                        'allow' => true,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                //'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex() {

        $model = Test::find()->asArray()->all();
        return $this->render('index', ['model' => $model]);
    }

    public function actionCreate() {
        // phpinfo();
        ini_set('memory_limit', '-1');
        ini_set('post_max_size', '15000M');
        ini_set('upload_max_filesize', '15000M');
        ini_set('max_input_time', '20000');
        $media_path = '/var/www/html/media/5/';

        try {
            $model = new Test();
            if (Yii::$app->request->isPost) {

                $model->attributes = Yii::$app->request->post('Test');

                $model->file = UploadedFile::getInstance($model, 'file');

                if ($model->file && $model->validate()) {

                    $path = General::CreateDir($media_path);
                    if ($path) {
                        $flag = $model->file->saveAs($media_path . $model->file->baseName . '.' . $model->file->extension);
                        if ($flag) {
                            $model->save(false);
                            return $this->redirect(['index']);
                        }
                    }
                }
            }
        } catch (\Exception $exception) {
            echo $exception->getMessage();
            exit;
        }

        return $this->render('create', ['model' => $model]);
    }

    public function actionGetLessonVideoListUsingUuid($uuid) {
        $res = General::getLessonVideoListUsingUuid($uuid);
        print_r($res);
    }

    /* public function actionAddtimezone() {
      $OptionsArray = timezone_identifiers_list();
      foreach ($OptionsArray as $key => $value) {
      $date = new \DateTime(date('Y-m-d H:i:s'), new \DateTimeZone($value));
      $offset = $date->format('P');
      $timezone = $date->format('T');
      $model = new Timezone();
      $model->name = $value;
      $model->timezone = $timezone;
      $model->offset = $offset;
      $model->status = "ACTIVE";
      }
      } */

    public function actionTest() {

        self::tutorWorkingPlanConvertTimezone();
        $date = '00:00';
        $from = 'America/Los_Angeles';
        $to = 'Australia/Sydney';
        $format = "H:i";
        $res = General::convertTimezone($date, $from, $to, $format);

        print_r($res);
        exit;
    }

    public function tutorWorkingPlanConvertTimezone() {

        $working_plan = '{"sunday":{"start":"09:00","end":"18:00","breaks":[]},"monday":{"start":"09:00","end":"18:00","breaks":[]},"tuesday":{"start":"09:00","end":"18:00","breaks":[]},"wednesday":{"start":"09:00","end":"18:00","breaks":[]},"thursday":{"start":"09:00","end":"18:00","breaks":[]},"friday":{"start":"09:00","end":"18:00","breaks":[]},"saturday":{"start":"09:00","end":"18:00","breaks":[]}}';

        $working_plan_array = json_decode($working_plan, true);
        $from = 'America/Los_Angeles';
        $to = 'Australia/Sydney';
        $return = [];
        if (!empty($working_plan_array)) {
            foreach ($working_plan_array as $key => $value) {
                $e = $value;
                $e['start'] = General::convertTimezone($value['start'], $from, $to, 'H:i');
                $e['end'] = General::convertTimezone($value['end'], $from, $to, 'H:i');
                $e['start_from'] = $value['start'];
                $e['end_from'] = $value['end'];
                $e['from'] = $from;
                $e['to'] = $to;
                $return[$key] = $e;
            }
        }

        echo "<pre>";
        print_r($working_plan_array);
        echo '=========';
        print_r($return);

        exit;
    }

    public function actionTestMail() {
        Yii::$app->mailer->compose('test', ['content' => []], ['htmlLayout' => 'layouts/html'])
                ->setFrom(Yii::$app->params['supportEmail'])
                ->setTo('alpesh@vindaloovoip.com')
//                ->setTo('mehul@mailinator.com')
                ->setSubject('Test Mail-' . Yii::$app->name)
                ->send();
    }

    public function actionGetvideo() {
        $id = Yii::$app->getRequest()->get('id');
        $searchModel = new PreRecordedVideoSearch();
        $searchModel->status = 'PUBLISHED';
        $searchModel->instrument_category_uuid = $id;
        $videoList = $searchModel->searchCategoryList(Yii::$app->request->queryParams, true);

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_get_video_list', [
                        'videoList' => $videoList,
                        'mode' => 'UPLOADED',
            ]);
        } else {
            return $this->render('get_video', [
                        'videoList' => $videoList,
                        'searchModel' => $searchModel,
            ]);
        }
    }

    public function server_agent() {

        $json_safari = '{
            "server_data": {
                "HTTPS": "on",
                "SSL_TLS_SNI": "sandbox.musiconn.com.au",
                "SSL_SERVER_S_DN_OU": "Domain Control Validated",
                "SSL_SERVER_S_DN_CN": "*.musiconn.com.au",
                "SSL_SERVER_I_DN_C": "BE",
                "SSL_SERVER_I_DN_O": "GlobalSign nv-sa",
                "SSL_SERVER_I_DN_CN": "AlphaSSL CA - SHA256 - G2",
                "SSL_SERVER_SAN_DNS_0": "*.musiconn.com.au",
                "SSL_SERVER_SAN_DNS_1": "musiconn.com.au",
                "SSL_VERSION_INTERFACE": "mod_ssl\/2.4.25",
                "SSL_VERSION_LIBRARY": "OpenSSL\/1.0.2s",
                "SSL_PROTOCOL": "TLSv1.2",
                "SSL_SECURE_RENEG": "true",
                "SSL_COMPRESS_METHOD": "NULL",
                "SSL_CIPHER": "ECDHE-RSA-AES256-GCM-SHA384",
                "SSL_CIPHER_EXPORT": "false",
                "SSL_CIPHER_USEKEYSIZE": "256",
                "SSL_CIPHER_ALGKEYSIZE": "256",
                "SSL_CLIENT_VERIFY": "NONE",
                "SSL_SERVER_M_VERSION": "3",
                "SSL_SERVER_M_SERIAL": "6FB89885F2DCEA4548C5BE5F",
                "SSL_SERVER_V_START": "Feb 28 06:08:53 2019 GMT",
                "SSL_SERVER_V_END": "Feb 28 06:08:53 2021 GMT",
                "SSL_SERVER_S_DN": "CN=*.musiconn.com.au,OU=Domain Control Validated",
                "SSL_SERVER_I_DN": "CN=AlphaSSL CA - SHA256 - G2,O=GlobalSign nv-sa,C=BE",
                "SSL_SERVER_A_KEY": "rsaEncryption",
                "SSL_SERVER_A_SIG": "sha256WithRSAEncryption",
                "SSL_SESSION_ID": "369606791d1c9d28aaa556f57acb3df5df59d55d14a824387c29b752d6dc8742",
                "SSL_SESSION_RESUMED": "Initial",
                "HTTP_HOST": "sandbox.musiconn.com.au",
                "HTTP_ACCEPT": "text\/html,application\/xhtml+xml,application\/xml;q=0.9,*\/*;q=0.8",
                "HTTP_COOKIE": "__stripe_mid=4fc64c31-a7fa-4700-a51a-71c412973707; _identity=9dd66bd0424dc99333d6daa45916322d3a02e72863fd69dddde0f49327274ab1a%3A2%3A%7Bi%3A0%3Bs%3A9%3A%22_identity%22%3Bi%3A1%3Bs%3A47%3A%22%5B71%2C%22hwYLm6_tPZ2jIFkALGjrAZX8az1fmObv%22%2C2592000%5D%22%3B%7D; _ga=GA1.3.462021513.1573989271",
                "HTTP_USER_AGENT": "Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_15) AppleWebKit\/605.1.15 (KHTML, like Gecko) Version\/13.0.1 Safari\/605.1.15",
                "HTTP_ACCEPT_LANGUAGE": "en-au",
                "HTTP_ACCEPT_ENCODING": "gzip, deflate, br",
                "HTTP_CONNECTION": "keep-alive",
                "PATH": "\/usr\/local\/sbin:\/usr\/local\/bin:\/usr\/sbin:\/usr\/bin:\/sbin:\/bin",
                "SERVER_SIGNATURE": "Apache\/2.4.25 (Debian) Server at sandbox.musiconn.com.au Port 443",
                "SERVER_SOFTWARE": "Apache\/2.4.25 (Debian)",
                "SERVER_NAME": "sandbox.musiconn.com.au",
                "SERVER_ADDR": "174.138.23.65",
                "SERVER_PORT": "443",
                "REMOTE_ADDR": "119.18.0.80",
                "DOCUMENT_ROOT": "\/var\/www\/html\/musiconn\/web",
                "REQUEST_SCHEME": "https",
                "CONTEXT_PREFIX": "",
                "CONTEXT_DOCUMENT_ROOT": "\/var\/www\/html\/musiconn\/web",
                "SERVER_ADMIN": "webmaster@localhost",
                "SCRIPT_FILENAME": "\/var\/www\/html\/musiconn\/web\/index.php",
                "REMOTE_PORT": "1068",
                "GATEWAY_INTERFACE": "CGI\/1.1",
                "SERVER_PROTOCOL": "HTTP\/1.1",
                "REQUEST_METHOD": "GET",
                "QUERY_STRING": "",
                "REQUEST_URI": "\/index.php\/site\/test-check-device",
                "SCRIPT_NAME": "\/index.php",
                "PATH_INFO": "\/site\/test-check-device",
                "PATH_TRANSLATED": "\/var\/www\/html\/musiconn\/web\/site\/test-check-device",
                "PHP_SELF": "\/index.php\/site\/test-check-device",
                "REQUEST_TIME_FLOAT": "1574328977.526",
                "REQUEST_TIME": "1574328977"
            },
            "get_browser": {
                "userAgent": "Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_15) AppleWebKit\/605.1.15 (KHTML, like Gecko) Version\/13.0.1 Safari\/605.1.15",
                "name": "Apple Safari",
                "version": "13.0.1",
                "platform": "mac",
                "pattern": "#(?Version|Safari|other)[\/ ]+(?[0-9.|a-zA-Z.]*)#"
            }
        }';

        $json_chrome = '{
            "server_data": {
                "HTTPS": "on",
                "SSL_TLS_SNI": "sandbox.musiconn.com.au",
                "SSL_SERVER_S_DN_OU": "Domain Control Validated",
                "SSL_SERVER_S_DN_CN": "*.musiconn.com.au",
                "SSL_SERVER_I_DN_C": "BE",
                "SSL_SERVER_I_DN_O": "GlobalSign nv-sa",
                "SSL_SERVER_I_DN_CN": "AlphaSSL CA - SHA256 - G2",
                "SSL_SERVER_SAN_DNS_0": "*.musiconn.com.au",
                "SSL_SERVER_SAN_DNS_1": "musiconn.com.au",
                "SSL_VERSION_INTERFACE": "mod_ssl\/2.4.25",
                "SSL_VERSION_LIBRARY": "OpenSSL\/1.0.2s",
                "SSL_PROTOCOL": "TLSv1.2",
                "SSL_SECURE_RENEG": "true",
                "SSL_COMPRESS_METHOD": "NULL",
                "SSL_CIPHER": "ECDHE-RSA-AES256-GCM-SHA384",
                "SSL_CIPHER_EXPORT": "false",
                "SSL_CIPHER_USEKEYSIZE": "256",
                "SSL_CIPHER_ALGKEYSIZE": "256",
                "SSL_CLIENT_VERIFY": "NONE",
                "SSL_SERVER_M_VERSION": "3",
                "SSL_SERVER_M_SERIAL": "6FB89885F2DCEA4548C5BE5F",
                "SSL_SERVER_V_START": "Feb 28 06:08:53 2019 GMT",
                "SSL_SERVER_V_END": "Feb 28 06:08:53 2021 GMT",
                "SSL_SERVER_S_DN": "CN=*.musiconn.com.au,OU=Domain Control Validated",
                "SSL_SERVER_I_DN": "CN=AlphaSSL CA - SHA256 - G2,O=GlobalSign nv-sa,C=BE",
                "SSL_SERVER_A_KEY": "rsaEncryption",
                "SSL_SERVER_A_SIG": "sha256WithRSAEncryption",
                "SSL_SESSION_ID": "5cca634ea3519cb4568ed93f1ae996c387ef540fea7ea7320d7122108c3bedb7",
                "SSL_SESSION_RESUMED": "Resumed",
                "HTTP_HOST": "sandbox.musiconn.com.au",
                "HTTP_ACCEPT": "text\/html,application\/xhtml+xml,application\/xml;q=0.9,*\/*;q=0.8",
                "HTTP_COOKIE": "__stripe_mid=54fd7fa9-897a-4235-aebd-ab4a1eeb71ec; _identity=9dd66bd0424dc99333d6daa45916322d3a02e72863fd69dddde0f49327274ab1a%3A2%3A%7Bi%3A0%3Bs%3A9%3A%22_identity%22%3Bi%3A1%3Bs%3A47%3A%22%5B71%2C%22hwYLm6_tPZ2jIFkALGjrAZX8az1fmObv%22%2C2592000%5D%22%3B%7D",
                "HTTP_USER_AGENT": "Mozilla\/5.0 (iPad; CPU OS 13_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) CriOS\/78.0.3904.84 Mobile\/15E148 Safari\/604.1",
                "HTTP_ACCEPT_LANGUAGE": "en-au",
                "HTTP_ACCEPT_ENCODING": "gzip, deflate, br",
                "HTTP_CONNECTION": "keep-alive",
                "PATH": "\/usr\/local\/sbin:\/usr\/local\/bin:\/usr\/sbin:\/usr\/bin:\/sbin:\/bin",
                "SERVER_SIGNATURE": "Apache\/2.4.25 (Debian) Server at sandbox.musiconn.com.au Port 443",
                "SERVER_SOFTWARE": "Apache\/2.4.25 (Debian)",
                "SERVER_NAME": "sandbox.musiconn.com.au",
                "SERVER_ADDR": "174.138.23.65",
                "SERVER_PORT": "443",
                "REMOTE_ADDR": "119.18.0.80",
                "DOCUMENT_ROOT": "\/var\/www\/html\/musiconn\/web",
                "REQUEST_SCHEME": "https",
                "CONTEXT_PREFIX": "",
                "CONTEXT_DOCUMENT_ROOT": "\/var\/www\/html\/musiconn\/web",
                "SERVER_ADMIN": "webmaster@localhost",
                "SCRIPT_FILENAME": "\/var\/www\/html\/musiconn\/web\/index.php",
                "REMOTE_PORT": "1124",
                "GATEWAY_INTERFACE": "CGI\/1.1",
                "SERVER_PROTOCOL": "HTTP\/1.1",
                "REQUEST_METHOD": "GET",
                "QUERY_STRING": "",
                "REQUEST_URI": "\/index.php\/site\/test-check-device",
                "SCRIPT_NAME": "\/index.php",
                "PATH_INFO": "\/site\/test-check-device",
                "PATH_TRANSLATED": "\/var\/www\/html\/musiconn\/web\/site\/test-check-device",
                "PHP_SELF": "\/index.php\/site\/test-check-device",
                "REQUEST_TIME_FLOAT": "1574329278.409",
                "REQUEST_TIME": "1574329278"
            },
            "get_browser": {
                "userAgent": "Mozilla\/5.0 (iPad; CPU OS 13_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) CriOS\/78.0.3904.84 Mobile\/15E148 Safari\/604.1",
                "name": "Google Chrome",
                "version": "78.0.3904.84",
                "platform": "mac",
                "pattern": "#(?Version|CriOS|other)[\/ ]+(?[0-9.|a-zA-Z.]*)#"
            }
        }';

        return json_decode($json_safari, true);
    }

    public function actionTestCheckDevice() {

//        $data = [
//            'server_data' => $_SERVER,
//            'get_browser' => General::getBrowser(),
//            ];
//        echo "<pre>";
//        print_r($data);
//        echo "</pre>";
//        exit;

        $tablet_browser = 0;
        $mobile_browser = 0;
        $ipad_browser = 0;
        $device = '';
        $MY_SERVER = $_SERVER;
        $GET_BROWSER = General::getBrowser();
        $GET_OS = General::getOS();
        $IS_MOBILE = General::isMobile();

        //for debuging start
        /* $json_array = \app\controllers\TestController::server_agent();
          echo "<pre>";
          print_r($json_array);
          echo "</pre>";

          $MY_SERVER = $json_array['server_data'];
          $GET_BROWSER = $json_array['get_browser'];
         */
        //for debuging end

        $return = array(
            'success' => '',
            'browser' => '',
            'browser_version' => '',
            'device' => '',
            'msg' => 'Sorry! No data found.'
        );

        //echo "<pre>";
        //print_r($MY_SERVER['HTTP_USER_AGENT']);

        if (preg_match('/(tablet)|(android)/i', strtolower($MY_SERVER['HTTP_USER_AGENT']))) {
            $tablet_browser++;
        }
        //if (preg_match('/(ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($MY_SERVER['HTTP_USER_AGENT']))) {
        if (preg_match('/(ipad|playbook)|(mobi|opera mini)/i', strtolower($MY_SERVER['HTTP_USER_AGENT']))) {

            $ipad_browser++;
        }

        if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($MY_SERVER['HTTP_USER_AGENT']))) {
            $mobile_browser++;
        }

        if ((strpos(strtolower($MY_SERVER['HTTP_ACCEPT']), 'application/vnd.wap.xhtml+xml') > 0) or ( (isset($MY_SERVER['HTTP_X_WAP_PROFILE']) or isset($MY_SERVER['HTTP_PROFILE'])))) {
            $mobile_browser++;
        }

        $mobile_ua = strtolower(substr($MY_SERVER['HTTP_USER_AGENT'], 0, 4));
        $mobile_agents = array(
            'w3c ', 'acs-', 'alav', 'alca', 'amoi', 'audi', 'avan', 'benq', 'bird', 'blac',
            'blaz', 'brew', 'cell', 'cldc', 'cmd-', 'dang', 'doco', 'eric', 'hipt', 'inno',
            'ipaq', 'java', 'jigs', 'kddi', 'keji', 'leno', 'lg-c', 'lg-d', 'lg-g', 'lge-',
            'maui', 'maxo', 'midp', 'mits', 'mmef', 'mobi', 'mot-', 'moto', 'mwbp', 'nec-',
            'newt', 'noki', 'palm', 'pana', 'pant', 'phil', 'play', 'port', 'prox',
            'qwap', 'sage', 'sams', 'sany', 'sch-', 'sec-', 'send', 'seri', 'sgh-', 'shar',
            'sie-', 'siem', 'smal', 'smar', 'sony', 'sph-', 'symb', 't-mo', 'teli', 'tim-',
            'tosh', 'tsm-', 'upg1', 'upsi', 'vk-v', 'voda', 'wap-', 'wapa', 'wapi', 'wapp',
            'wapr', 'webc', 'winw', 'winw', 'xda ', 'xda-');

        if (in_array($mobile_ua, $mobile_agents)) {
            $mobile_browser++;
        }

        if (strpos(strtolower($MY_SERVER['HTTP_USER_AGENT']), 'opera mini') > 0) {
            $mobile_browser++;
            //Check for tablets on opera mini alternative headers
            $stock_ua = strtolower(isset($MY_SERVER['HTTP_X_OPERAMINI_PHONE_UA']) ? $MY_SERVER['HTTP_X_OPERAMINI_PHONE_UA'] : (isset($MY_SERVER['HTTP_DEVICE_STOCK_UA']) ? $MY_SERVER['HTTP_DEVICE_STOCK_UA'] : ''));
            if (preg_match('/(tablet)|(android(?!.*mobile))/i', $stock_ua)) {
                $tablet_browser++;
            }
            if (preg_match('/(ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
                $ipad_browser++;
            }
        }


//        echo "<pre>";
//        print_r(($IS_MOBILE)?"true":"false");echo "<br>";
//        print_r($GET_OS);
//        echo "<br>";
//        print_r($mobile_browser);
//        print_r($tablet_browser);
//        print_r($ipad_browser);
//        echo "</pre>";
//        exit;
        if ($ipad_browser > 0 && in_array($GET_OS, array('iPad', 'iPhone'))) {

            // do something for tablet devices
            //print 'is tablet';
            $device = 'Ipad';
            $ua = $GET_BROWSER;
            $browser = $ua['name'];
            $browser_version = $ua['version'];
            $webrtc_supported_version = '12.1';
            if ($GET_OS == 'iPhone') {

                $device = 'iPhone';
                $return = array(
                    'success' => 'false',
                    'browser' => $browser,
                    'browser_version' => $browser_version,
                    'device' => $device,
                    'msg' => 'Settings in your mobile device is not compatible for lessons.'
                );
            } elseif ($browser != 'Apple Safari') {

                $return = array(
                    'success' => 'false',
                    'browser' => $browser,
                    'browser_version' => $browser_version,
                    'device' => $device,
                    'msg' => 'This product was built for use in the Safari browser.  Please log into Musiconn using Safari.'
                );
            } elseif ($browser == 'Apple Safari' && $browser_version < $webrtc_supported_version) {

                $return = array(
                    'success' => 'false',
                    'browser' => $browser,
                    'browser_version' => $browser_version,
                    'device' => $device,
                    'msg' => 'Please update your Safari browser for the lessons to run smoothly.'
                );
            } else {

                $return = array(
                    'success' => 'true',
                    'browser' => $browser,
                    'browser_version' => $browser_version,
                    'device' => $device,
                    'msg' => "Systems compatibility testing completed successfully. You're good to go!"
                );
            }
        } elseif ($tablet_browser > 0 && $IS_MOBILE == false) {

            // do something for tablet devices
            //print 'is tablet';
            $device = 'Tablet';
            $ua = $GET_BROWSER;
            $browser = $ua['name'];
            $browser_version = $ua['version'];
            $webrtc_supported_version = '28';

            if ($browser != 'Google Chrome') {

                $return = array(
                    'success' => 'false',
                    'browser' => $browser,
                    'browser_version' => $browser_version,
                    'device' => $device,
//                    'msg' => 'This product was built for use in Chrome browser only.'
                    'msg' => 'This product was built for use in Chrome browser for your device.  Please log in again using Chrome.  If you don’t have Chrome installed, follow this <a href="https://www.google.com.au/chrome/?brand=CHBD&gclid=EAIaIQobChMI2caQ7Prw5QIV2AorCh2zNgGgEAAYASABEgIhOPD_BwE&gclsrc=aw.ds">link</a> to download.'
                );
            } elseif ($browser == 'Google Chrome' && $browser_version <= $webrtc_supported_version) {

                $return = array(
                    'success' => 'false',
                    'browser' => $browser,
                    'browser_version' => $browser_version,
                    'device' => $device,
                    'msg' => 'Please update your Google Chrome browser for the lessons to run smoothly.'
                );
            } else {

                $return = array(
                    'success' => 'true',
                    'browser' => $browser,
                    'browser_version' => $browser_version,
                    'device' => $device,
                    'msg' => "Systems compatibility testing completed successfully. You're good to go!33"
                );
            }
        } else if ($mobile_browser > 0 && $IS_MOBILE == true) {

            $device = 'Mobile';
            $ua = $GET_BROWSER;
            $browser = $ua['name'];
            $browser_version = $ua['version'];

            /* if ($GET_OS == 'Android' && $browser == 'Google Chrome' ) {
              if($browser_version >= '50'){
              $return = array(
              'success' => 'true',
              'browser' => $browser,
              'browser_version' => $browser_version,
              'device' => $device,
              'msg' => "Systems compatibility testing completed successfully. You're good to go!"
              );
              }else{
              $return = array(
              'success' => 'true',
              'browser' => $browser,
              'browser_version' => $browser_version,
              'device' => $device,
              'msg' => "Please install latest version of Chrome."
              );
              }
              } else {
              // do something for mobile devices
              $return = array(
              'success' => 'false',
              'browser' => $browser,
              'browser_version' => $browser_version,
              'device' => $device,
              'msg' => 'Settings in your mobile device is not compatible for lessons.'
              );
              } */
            $return = array(
                'success' => 'false',
                'browser' => $browser,
                'browser_version' => $browser_version,
                'device' => $device,
                'msg' => 'Settings in your mobile device is not compatible for lessons.'
            );
        } else {
            // do something for everything else
            //print 'is desktop';
            $device = 'Desktop';
            $ua = $GET_BROWSER;
            $browser = $ua['name'];
            $browser_version = $ua['version'];
            $webrtc_supported_version = '28';

            $mac_browser_version = $ua['version'];
            $mac_webrtc_supported_version = '12.1';


            if ($ua['platform'] == 'mac') {
                //condition for MAC system - start
                if ($browser == 'Apple Safari') {
                    if ($mac_browser_version < $mac_webrtc_supported_version) {

                        $return = array(
                            'success' => 'false',
                            'browser' => $browser,
                            'browser_version' => $browser_version,
                            'device' => $device,
                            'msg' => 'Please update your Safari browser for the lessons to run smoothly.'
                        );
                    } else {
                        $return = array(
                            'success' => 'true',
                            'browser' => $browser,
                            'browser_version' => $browser_version,
                            'device' => $device,
                            'msg' => "Systems compatibility testing completed successfully. You're good to go!"
                        );
                    }
                } else {
                    $return = array(
                        'success' => 'false',
                        'browser' => $browser,
                        'browser_version' => $browser_version,
                        'device' => $device,
                        'msg' => 'This product was built for use in the Safari browser.  Please log into Musiconn using Safari.'
                    );
                }
                //condition for MAC system - end
            } elseif ($browser != 'Google Chrome') {

                $return = array(
                    'success' => 'false',
                    'browser' => $browser,
                    'browser_version' => $browser_version,
                    'device' => $device,
                    //'msg' => 'This product was built for use in Chrome browser only.'
                    'msg' => 'This product was built for use in Chrome browser for your device.  Please log in again using Chrome.  If you don’t have Chrome installed, follow this <a href="https://www.google.com.au/chrome/?brand=CHBD&gclid=EAIaIQobChMI2caQ7Prw5QIV2AorCh2zNgGgEAAYASABEgIhOPD_BwE&gclsrc=aw.ds">link</a> to download.'
                );
            } elseif ($browser == 'Google Chrome' && $browser_version <= $webrtc_supported_version) {

                $return = array(
                    'success' => 'false',
                    'browser' => $browser,
                    'browser_version' => $browser_version,
                    'device' => $device,
                    'msg' => 'Please update your Google Chrome browser for the lessons to run smoothly.'
                );
            } else {

                $return = array(
                    'success' => 'true',
                    'browser' => $browser,
                    'browser_version' => $browser_version,
                    'device' => $device,
                    'msg' => "Systems compatibility testing completed successfully. You're good to go!"
                );
            }
        }

        echo "<pre>";
        print_r($MY_SERVER);
        print_r($GET_BROWSER);
        print_r($return);
        echo "</pre>";
        exit;
    }

    public function actionTestDatepicker() {

        return $this->render('test_datepicker', [
        ]);
    }

    public function actionSetOldLessonTutorRate() {

        echo "Sorry.";
        exit;
        $query = \app\models\BookedSession::find()->joinWith(['studentUu', 'tutorUu', 'studentBookingUu', 'creditSessionUu', 'instrumentUu']);
        $orderBy = 'start_datetime asc';
//$query->where(['booked_session.uuid'=>'f4d6de11-4e51-4fbf-b8b8-bedde77aa009']);
        $result = $query->orderBy($orderBy)->all();
$a = [];
        if (!empty($result)) {
            foreach ($result as $value) {
echo "<br>=====>>>UUID:".$value['uuid']."==>>START";
                $tutor_fee = 0.00;
                $plan_model = \app\models\PremiumStandardPlanRate::find()->joinWith(['onlineTutorialPackageTypeUu'])->where(['premium_standard_plan_rate.uuid' => $value['studentBookingUu']['premium_standard_plan_rate_uuid']])->asArray()->one();
                $option = $value['studentBookingUu']['plan_rate_option'];
                $tutor_plan_cat = strtoupper($plan_model['onlineTutorialPackageTypeUu']['name']);
                if (!empty($value['studentBookingUu']['premium_standard_plan_rate_uuid']) && empty($value['credit_session_uuid']) ) {

                    $tutor_fee_model = \app\models\TutorFee::find()->where(['online_tutorial_package_type_uuid' => $plan_model['onlineTutorialPackageTypeUu']['uuid'], 'option' => $option, 'sesson_length' => $value['session_min']])->one();

                    if (isset($tutor_fee_model->tutor_fee)) {
                        $tutor_fee = $tutor_fee_model->tutor_fee;
                    }
                    $option = $plan_rate_option = ($value['studentBookingUu']['plan_rate_option'] != 0) ? $value['studentBookingUu']['plan_rate_option'] : '';
                    if ($plan_rate_option == 0) {
                        $plan_type_model = \app\models\OnlineTutorialPackageType::find()->where(['name' => 'PREMIUM'])->one();
                        $tutor_plan_cat = "PREMIUM";
                        }

                } else {
                    //This lessons rate are defined which lesson booked before option rate module implementation.
                    $before_rate_module_price_array = [30 => 37, 60 => 66];
                        $tutorList = \app\models\StudentTuitionBooking::getInstrumentWiseTutorListSingle($value['instrumentUu']['uuid'], $value['tutorUu']['uuid']);
                        if (!empty($tutorList)) {
                            //$TutorInstrumentPlanOptions = StudentTuitionBooking::getTutorInstrumentPlanOptions($tutorList[0]['online_tutorial_package_type_uuid'], $tutorList[0]['uuid'], $tutorList[0]['instrument_uuid']);
                            if (isset($tutorList) && $tutorList[0]['standard_plan_rate_option'] != 0) {
                                $TutorInstrumentPlanOptions = \app\models\StudentTuitionBooking::getTutorInstrumentPlanOptions($tutorList[0]['online_tutorial_package_type_uuid'], $tutorList[0]['uuid'], $tutorList[0]['instrument_uuid']);
                                $InstrumentWiseTutorPriceList = \app\models\StudentTuitionBooking::getInstrumentWiseTutorPriceList($tutorList[0]['online_tutorial_package_type_uuid'], $TutorInstrumentPlanOptions['standard_plan_rate_option']);
                                //echo "<pre>";print_r($InstrumentWiseTutorPriceList);echo "</pre>";
                                //echo "<pre>";print_r($tutor_fee_model);echo "</pre>";
                                if (date("Y-m-d", strtotime($value['created_at'])) > date('Y-m-d', strtotime('2019-10-25'))) {
                                    
                                    $tutor_fee_model = \app\models\TutorFee::find()->where(['online_tutorial_package_type_uuid' => $tutorList[0]['online_tutorial_package_type_uuid'], 'option' => $TutorInstrumentPlanOptions['standard_plan_rate_option'], 'sesson_length' => $value['session_min']])->one();
                                    //echo "<pre>";print_r($tutor_fee_model);echo "</pre>";
                                    $tutor_fee = $tutor_fee_model['tutor_fee'];
                                    $plan_rate_option = $TutorInstrumentPlanOptions['standard_plan_rate_option'];
                                    $plan_type_model = \app\models\OnlineTutorialPackageType::find()->where(['uuid' => $tutor_fee_model['online_tutorial_package_type_uuid']])->one();
                                    //echo "<pre>";print_r($plan_type_model->display_name);echo "</pre>";	
                                } else {
                    $tutor_fee = ($value['session_min'] == 30) ? $before_rate_module_price_array[30] : $before_rate_module_price_array[60];
                                    $plan_type_model = \app\models\OnlineTutorialPackageType::find()->where(['name' => 'PREMIUM'])->one();
                                    $plan_rate_option = ($value['studentBookingUu']['plan_rate_option'] != 0) ? $value['studentBookingUu']['plan_rate_option'] : '';
                }
                            } else {
                                if (date("Y-m-d", strtotime($value['created_at'])) > date('Y-m-d', strtotime('2019-10-25'))) {
                                    if (in_array($value['studentBookingUu']['plan_rate_option'], ['', '0'])) {
                                        $tutor_fee_model = \app\models\TutorFee::find()->where(['option' => '0', 'sesson_length' => $value['session_min']])->one();
                                        $tutor_fee = $tutor_fee_model['tutor_fee'];
                                    } else {
                                        $tutor_fee_model = \app\models\TutorFee::find()->where(['option' => $value['studentBookingUu']['plan_rate_option'], 'sesson_length' => $value['session_min']])->one();
                                        $tutor_fee = $tutor_fee_model['tutor_fee'];
                                    }
                                } else {
                                    $tutor_fee = ($value['session_min'] == 30) ? $before_rate_module_price_array[30] : $before_rate_module_price_array[60];
                                }
                                $plan_type_model = \app\models\OnlineTutorialPackageType::find()->where(['name' => 'PREMIUM'])->one();
                                $plan_rate_option = ($value['studentBookingUu']['plan_rate_option'] != 0) ? $value['studentBookingUu']['plan_rate_option'] : '';
                            }
                        } else {
                            //echo '</br>'.$value['studentBookingUu']['plan_rate_option'].'patel'.$value['created_at'];
                            if (date("Y-m-d", strtotime($value['created_at'])) > date('Y-m-d', strtotime('2019-10-25'))) {
                                if (in_array($value['studentBookingUu']['plan_rate_option'], ['', '0'])) {
                                    $tutor_fee_model = \app\models\TutorFee::find()->where(['option' => '0', 'sesson_length' => $value['session_min']])->one();
                                    $tutor_fee = $tutor_fee_model['tutor_fee'];
                                } else {
                                    $tutor_fee_model = \app\models\TutorFee::find()->where(['option' => $value['studentBookingUu']['plan_rate_option'], 'sesson_length' => $value['session_min']])->one();
                                    $tutor_fee = $tutor_fee_model['tutor_fee'];
                                }
                            } else {
                                $tutor_fee = ($value['session_min'] == 30) ? $before_rate_module_price_array[30] : $before_rate_module_price_array[60];
                            }
                            $plan_type_model = \app\models\OnlineTutorialPackageType::find()->where(['name' => 'PREMIUM'])->one();
                            $plan_rate_option = ($value['studentBookingUu']['plan_rate_option'] != 0) ? $value['studentBookingUu']['plan_rate_option'] : '';
                        }
                        $tutor_plan_cat = (!empty($plan_type_model)) ? strtoupper($plan_type_model->name) : "";

//This lessons rate are defined which lesson booked before option rate module implementation.
//                    $before_rate_module_price_array = [30 => 37, 60 => 66];
//                    $tutor_fee = ($value['session_min'] == 30) ? $before_rate_module_price_array[30] : $before_rate_module_price_array[60];
//                    $tutor_plan_cat = "PREMIUM";
//                    $option = 0;
                }

                $session_price = $session_gst = $session_total_price = 0;
                
                $plan_rate_option = (!empty($plan_rate_option))?$plan_rate_option:0;
                if (!empty($value['studentBookingUu']['booking_id'])) {
                    $price = $value['studentBookingUu']['price'];
                    $gst = $value['studentBookingUu']['gst'];
                    $discount = $value['studentBookingUu']['discount'];

                    $total_session = $value['studentBookingUu']['total_session'];

//                    $session_price = number_format(($price / $total_session), 2, '.', '');
//                    $session_gst = number_format(($gst / $total_session), 2, '.', '');
//                    $session_total_pricenumber_format = number_format(($session_price + $session_gst), 2, '.', '');

                    $session_price = ($price / $total_session);
                    $session_gst = ($gst / $total_session);
                    $session_total_price = ($session_price + $session_gst);
                }

                $a[] = [
                    'uuid' => $value['uuid'],
                    'Booking Id' => $value['studentBookingUu']['booking_id'],
                    'total_session' => $value['studentBookingUu']['total_session'],
                    'session_pin' => $value['session_pin'],
                    'tutor_plan_category' => $tutor_plan_cat,
                    'tutor_plan_option' => $plan_rate_option,
                    'tutor_pay_rate' => $tutor_fee,
                    'session_price' => $session_price,
                    'session_gst' => $session_gst,
                    'session_total_price' => $session_total_price,
                    'session_musicoins' => \app\libraries\General::priceToMusicoins($session_total_price),
                    'session_discount_musicoins' => 0,
                ];
                echo "==>>PREPARED";
                //update process
                $up_model = $value;
                $up_model->tutor_plan_category = $tutor_plan_cat;
                $up_model->tutor_plan_option = $plan_rate_option;
                $up_model->tutor_pay_rate = $tutor_fee;
                $up_model->session_price = $session_price;
                $up_model->session_gst = $session_gst;
                $up_model->session_total_price = $session_total_price;
                $up_model->session_musicoins = \app\libraries\General::priceToMusicoins($session_total_price);
                $up_model->session_discount_musicoins = 0;
                $up_model->save(false);
                echo "==>>SAVED";
//                echo "<pre>";
//                print_r($value);
//                print_r($a);
//                echo "</pre>";
//                exit;
            }
        }
        echo "<pre>";
        //print_r($a);
        print_r("Done");
        echo "</pre>";
        exit;
    }

    public function actionMovefile() {

        exit;
        $lesson_uuid = '1c6d357e-da8d-42de-8936-6a48fe6ccea9';
        $file_list = [];
        $jitsi_path = Yii::$app->params['jitsi']['video_path'];
        $glob_path = $jitsi_path . $lesson_uuid . '*.*';
        $file_glob_array = glob($glob_path);
        if (is_array($file_glob_array) && count($file_glob_array) > 0) {
            foreach ($file_glob_array as $k => $v) {

                $source_filename = basename($v);
                //move file into media folder - start
                $file_media_path = Yii::$app->params['media']['session_recorded_video']['path'] . $lesson_uuid;
                $path = \app\libraries\General::CreateDir($file_media_path);

                if (is_file($v) && is_dir($file_media_path)) {
                    if (copy($v, $file_media_path . '/' . $source_filename)) {

                        $file_list[] = $source_filename;
                        //delete source file
                        unlink($v);
                    }
                }
                //move file into media folder - end
            }
        }
    }

    /*
      Developer: Akash Shrimali
      Task: Add data in Mailchimp when student Signup
      Date: 12-03-2020
      Start
     */

    public function actionExistingStudentAddIntoMailchimp() {
        $model = User::find()->where(['not', ['student_uuid' => null]])->all();
        $return = [];
        if (!empty($model)) {
            foreach ($model as $user) {
                $mailchimp_user = MailChimpApi::insert($user->email, $user->first_name, $user->last_name);
                if (!empty($mailchimp_user) && isset($mailchimp_user['user'])) {
                    $return = array(
                        'success' => 'true',
                        'user' => $mailchimp_user['user'],
                        'msg' => 'Mailchimp user created successfully.'
                    );
                } else {
                    $return = array(
                        'success' => 'false',
                        'msg' => $mailchimp_user['msg'],
                        'code' => $mailchimp_user['code']
                    );
                }
            }
        }
        echo "<pre>";
        print_r($return);
        exit;
    }

    /*
      Developer: Akash Shrimali
      Task: Add data in Mailchimp when student Signup
      Date: 12-03-2020
      End
     */

    public function create_pdf($filename = '', $order_uuid) {

        $file = Yii::$app->params['media']['invoice']['path'] . $filename;

        $model = \app\models\Order::findOne($order_uuid);
        
        if (!empty($model)) {

            $order_item = \app\models\OrderItem::find()->where(['order_uuid' => $order_uuid])->asArray()->all();
            $transaction_model = \app\models\PaymentTransaction::find()->where(['uuid' => $model->payment_uuid])->asArray()->one();
            
            $content = Yii::$app->controller->renderPartial('//order/buy_musicoin_invoice_pdf', ['model' => $model, 'order_item' => $order_item,'transaction_model' => $transaction_model]);

            if ($filename == '') {
                if (!empty($transaction_model['transaction_number'])) {
                    $filename = 'Invoice_' . $transaction_model['transaction_number'] . '.pdf';
                } else {
                    $filename = 'Invoice_' . $model->order_id . '.pdf';
                }
            }
            $file = Yii::$app->params['media']['invoice']['path'] . $filename;

            if (!is_dir(Yii::$app->params['media']['invoice']['path'])) {
                FileHelper::createDirectory(Yii::$app->params['media']['invoice']['path']);
            }

            $css_content = file_get_contents(Yii::$app->params['theme_assets']['url'] . 'plugins/bootstrap/css/bootstrap.min.css');
            $css_content .= file_get_contents(Yii::$app->params['theme_assets']['url'] . 'css/new_pdf.css');
            $css_content .= 'body {background-image: url(\'' . Yii::$app->params['theme_assets']['url'] . 'images/pdf_bg.png' . '\'); background-position: center bottom;background-repeat: no-repeat;}';
            
            $footer_html = '<table class="footer_tbl"><tr><td>ADMIN@MUSICONN.COM.AU</td><td>1300 068 742</td><td>MUSICONN.COM.AU</td></tr></table>';

            $params = [
                'mode' => Pdf::MODE_CORE,
                'format' => Pdf::FORMAT_A4,
                'orientation' => Pdf::ORIENT_PORTRAIT,
                'destination' => Pdf::DEST_FILE,
                'content' => $content,
                'filename' => Yii::$app->params['media']['invoice']['path'] . $filename,
                'tempPath' => 'mpdf/',
                'cssInline' => '.kv-heading-1{font-size:12px;border:none;}',
                'cssInline' => $css_content,
                'options' => ['title' => 'Invoice'],
                'methods' => [
                    'SetHeader' => [''],
                    'SetFooter' => [$footer_html],
                ]
            ];
            $pdf = new Pdf($params);
            $pdf->render();

            if (is_file($file)) {

                $model->invoice_file = $filename;
                $model->save(false);

                return ['status' => '1', 'file' => $file, 'filename' => $filename];
            } else {
                return ['status' => '0', 'file' => $file, 'filename' => $filename];
            }
        } else {
            return ['status' => '0', 'file' => $file, 'filename' => $filename];
        }
    }

    public function actionTestInvoice() {
//       $res = self::create_pdf('alpesh_order.pdf','8a44941a-1342-403b-af2d-e9215e160a65');
//       echo "<pre>";
//       print_r($res);
//       echo "</pre>";
//       exit;
        
        
        $filename = 'alpesh_test.pdf';
        $content = Yii::$app->controller->renderPartial('//order/musicoin_invoice_pdf_test', []);

        $css_content = file_get_contents(Yii::$app->params['theme_assets']['url'] . 'plugins/bootstrap/css/bootstrap.min.css');
            $css_content .= file_get_contents(Yii::$app->params['theme_assets']['url'] . 'css/new_pdf.css');
            $css_content .= 'body {background-image: url(\'' . Yii::$app->params['theme_assets']['url'] . 'images/pdf_bg.png' . '\'); background-position: center bottom;background-repeat: no-repeat;}';
            
            $footer_html = '<table class="footer_tbl"><tr><td>ADMIN@MUSICONN.COM.AU</td><td>1300 068 742</td><td>MUSICONN.COM.AU</td></tr></table>';

            $params = [
                'mode' => Pdf::MODE_CORE,
                'format' => Pdf::FORMAT_A4,
                'orientation' => Pdf::ORIENT_PORTRAIT,
                'destination' => Pdf::DEST_FILE,
                'content' => $content,
                'filename' => Yii::$app->params['media']['invoice']['path'] . $filename,
                'tempPath' => 'mpdf/',
                'cssInline' => '.kv-heading-1{font-size:12px;border:none;}',
                'cssInline' => $css_content,
                'options' => ['title' => 'Invoice'],
                'methods' => [
                    'SetHeader' => [''],
                    'SetFooter' => [$footer_html],
                ]
            ];
        $pdf = new Pdf($params);
        $pdf->render();

        echo "<pre>";
        print_r($filename);
        echo "</pre>";
        exit;
    }

}
