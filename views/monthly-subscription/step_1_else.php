<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Url;
use app\models\StudentMonthlySubscription;
use app\models\StudentSubscription;
use app\libraries\General;

$this->title = Yii::t('app', 'MONTHLY SUBSCRIPTION');
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_icon'] = 'my-lesson-tag.png';

//$monthlySubscriptionDetail = StudentMonthlySubscription::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid])->one();
$studentSubscriptionDetail = StudentSubscription::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid])->one();

$subscriptionFeeList = StudentMonthlySubscription::getsubscriptionFeeList(Yii::$app->user->identity->student_uuid);

$this->registerJs(
        '
    
    $(document).on("click",".monthly_sub_fee_select_btn", function(){ 
        var id = $(".monthly_sub_fee_select_btn").data("id"); 
        //$(".payment").show(); 
        
    });
    $(document).on("click",".isCancelMonthlySubscriptionRequest", function(){ 
        if(confirm("Are you sure, you want to opt out from your current monthly subscription plan?")){
        var id = $(".isCancelMonthlySubscriptionRequest").data("id");
        var element = $(this);
            $.ajax({
                type: "GET",
                url: "' . Url::to(['/student/monthly-subscription-cancel/']) . '",
                data: { "id": id},
                //beforeSend:function(){ $(".local-spinner").show(); },
                success:function(result){
                    if(result.code == 200){
                        $(element).html("");
                        $(element).addClass("btn-primary");
                        $(element).addClass("disabled");
                        $(element).removeClass("isCancelMonthlySubscriptionRequest");
                        $(element).removeClass("btn-danger");
                        
                        //showSuccess(result.message);
                         window.location="'.Yii::$app->urlManager->createUrl('student/profile').'";
                    }else{
                        showErrorMessage(result.message);
                    }
                },
                error:function(e){ 
                    showErrorMessage(e.responseText);
                }
            });
        }
    });
        ', View::POS_END
);
?>

<section class="content">
    <div class="row ">
        <div class="col-xs-6 col-xs-offset-3">
            <div class="box">
                <div class="booked-sessions-header">
                    <h3 style="color:#fff;" class=" text-center"><?= $studentMonthlysubDetail['name']; ?></h3>
                </div>
                <div class="box-body box-panel-body" style="border-top-left-radius: 0px;border-top-right-radius: 0px;">
                    <?php
                    $form = ActiveForm::begin([
                                'action' => ['index'],
                                'id' => 'frm_step_1_else',
                                'enableClientValidation' => true,
//                              'enableAjaxValidation' => true,
                                'validateOnChange' => true,
                                'options' => ['class' => 'form-horizontal']]);
                    ?>
                    <input type="hidden" name="step_else" value="1">
                    <input type="hidden" name="studentmonthlysub_uuid" value="<?= $studentMonthlysubDetail['uuid']; ?>">
                    <div class="row vertical-center-box vertical-center-box-tablet ">
                        <span class="hr_line2 ">
                            <div class="text-center col-xs-6 mar-bot-15" style="border-right: 1px solid #bbb8b8;">
                                <h5>
                                    <b>Price is <span class="symbol">$</span><?= $subscriptionFeeList->price; ?></b>
                                </h5>
                                <h5>
                                    <b>Tax is <span class="symbol">$</span><?= $subscriptionFeeList->tax; ?></b>
                                </h5>

                                <?php if (!empty($studentSubscriptionDetail)) { ?>
                                    <h5>
                                        <b>Start on <?= General::displayDate($studentMonthlysubDetail->start_datetime); ?></b>
                                    </h5>

                                    <h5>
                                        <b>Renew on <?= General::displayDate(date('Y-m-d', strtotime('+1 days', strtotime($studentMonthlysubDetail->end_datetime)))); ?></b>
                                    </h5>

                                <?php } ?>
                            </div>
                            <div class="text-center col-xs-6 mar-bot-15">
                                <div class="price">
                                    <h3><span class="symbol">$</span><?= $subscriptionFeeList->total_price; ?></h3>
                                    <h4>per month</h4>
                                </div>
                                <?php
                                if (!empty($studentSubscriptionDetail)) {
                                    $next_renewal_date = strtotime($studentSubscriptionDetail['next_renewal_date']);
                                    $currentDate = date("Y-m-d");
                                    $days_ago = date('Y-m-d', strtotime('-10 days', $next_renewal_date));
                                    if ($currentDate >= $days_ago) {
                                        ?>
                                        <button class="btn btn-primary monthly_sub_fee_select_btn" data-id="<?= $studentMonthlysubDetail->uuid; ?>">RENEW</button>
                                        
                                        <?php
                                    }
                                } else {
                                    ?>
                                        <button class="btn btn-primary monthly_sub_fee_select_btn" data-id="<?= $subscriptionFeeList->uuid; ?>">BUY</button>    
                                    

                                <?php }
                                ?>
                            </div>
                        </span>
                    </div>
                    <div class="row vertical-center-box vertical-center-box-tablet " style="margin-top: 20px;">
                        <div class="col-md-12">

                            <div class="text-center" style="padding:20px; border-top: 1px solid #bbb8b8;">
                                
                                <?php if ($student_details->isMonthlySubscription && !$student_details->isCancelMonthlySubscriptionRequest) { ?>
                                
                                <a class="btn btn-danger btn-icon isCancelMonthlySubscriptionRequest" data-id="<?= $student_details->student_uuid; ?>">
                                        <span class="text-white">Cancel Subscription</span>
                                    </a>
                                <?php } elseif ($student_details->isMonthlySubscription && $student_details->isCancelMonthlySubscriptionRequest) { ?>
                                    <a class="btn btn-primary btn-icon disabled">
                                        <span class="text-white1">Pending approval of cancel subscription</span>
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <a class="btn btn-primary btn-sm black_button_in" href="<?= Yii::$app->getUrlManager()->createUrl('/student/profile'); ?>">RETURN TO PROFILE</a>
</section>
