<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentTerm */

$this->title = Yii::t('app', 'Update Payment Term: ' . $model->uuid, [
    'nameAttribute' => '' . $model->uuid,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Payment Terms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->uuid, 'url' => ['view', 'id' => $model->uuid]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="payment-term-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
