<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->registerJs(
        '  
        '
);
?>
<div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">

    <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Name</th>
                <th>Email</th>
                <th>Role</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>

        <tbody>
            <?php
            if (!empty($adminList)) {
                foreach ($adminList as $key => $value) {
                    ?>
                    <tr>
                        <td><?= $key + 1; ?></td>
                        <td><?= $value['first_name'] . ' ' . $value['last_name']; ?></td>
                        <td><?= $value['email']; ?></td>
                        <td><?= $value['admin_type']; ?></td>
                        <td>
                            <?php
                         
                            if ($value['status'] == 'ENABLED') {
                                $e_class = "badge-primary";
                                $e_link_class = "admin_disabled_link";
                            } elseif ($value['status'] == 'DISABLED') {
                                $e_class = "badge-secondary";
                                $e_link_class = "admin_enabled_link";
                            } elseif ($value['status'] == 'INACTIVE') {
                                $e_class = "badge-orange";
                                $e_link_class = "";
                            } else {
                                $e_class = "badge-danger";                                
                                $e_link_class = "";
                            }
                            ?>
                            <span class="badge badge-pill <?= $e_class ?> <?= $e_link_class ?>"  data-uuid="<?= $value['admin_uuid']; ?>" ><?= $value['status']; ?></span>
                        </td>
                        <td>
                            <?php if ($value['status'] != 'DELETED') { ?>
                                <?php if ($value['status'] == 'ENABLED') { ?>
                                    <a href="<?= Url::to(['/admins/switch-to-admin/', 'id' => $value['admin_uuid']]) ?>" class="badge badge-orange badge-md" title="Go To Account"><i class="fa fa-exchange"></i></a>
                                <?php } ?>
                                <a href="<?= Url::to(['/admins/update/', 'id' => $value['admin_uuid']]) ?>" class="badge badge-info badge-md loader" title="Update"><i class="fa fa-pencil"></i></a>
                                <a href="javascript:void(0)" data-uuid="<?= $value['admin_uuid']; ?>" class="delete_admin_link badge badge-danger badge-md" title="Delete"><i class="fa fa-trash"></i></a>
                                <?php } ?>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>

</div>
