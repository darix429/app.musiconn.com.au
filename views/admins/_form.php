<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\libraries\General;

/* @var $this yii\web\View */
/* @var $model app\models\Admin */
/* @var $form yii\widgets\ActiveForm */
// echo "<pre>";print_r($userModel);exit;

$timezone = General::getAllTimeZones();
if($userModel->timezone == '') {
 $userModel->timezone = Yii::$app->params['timezone'];
}

$this->registerJs(
 '

', yii\web\View::POS_END
);
?>

<div class="col-xl-12 col-lg-12 col-12 col-md-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Please Fill Information</h2>
            <div class="actions panel_actions float-right">
                <!--<i class="box_toggle fa fa-chevron-down"></i>-->
                <a href="<?= Yii::$app->getUrlManager()->createUrl(['/admins/index/'])?>" class="btn btn-info btn-icon "><i class="fa fa-arrow-left text-white"></i> <span>Back</span></a>
            </div>
        </header>
        <div class="content-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12">
                    <?php
//                    echo "<pre>";
//                    print_r($model->getErrors());
//                    print_r($userModel->getErrors());
//                    echo "</pre>";

                    ?>
                    <?php $form = ActiveForm::begin(['id' => 'myform']); ?>
                    <div class="form-row">
                        <div class="col-md-4 mb-0">
                            <?= $form->field($model, 'first_name')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('first_name')]) ?>
                        </div>
                        <div class="col-md-4 mb-0">
                            <?= $form->field($model, 'last_name')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('last_name')]) ?>
                        </div>
                        <div class="col-md-4 mb-0">

                            <?= $form->field($model, 'admin_type')->dropDownList(['ADMIN' => 'ADMIN', 'SUBADMIN' => 'SUBADMIN'], ['class' => 'form-control', 'maxlength' => true, 'prompt' => "Select " . $model->getAttributeLabel('admin_type')]) ?>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6 mb-0">
                            <?= $form->field($userModel, 'email')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $userModel->getAttributeLabel('email')]) ?>
                        </div>
                        <div class="col-md-6 mb-0">
                            <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), ['mask' => '9999 999 999', 'clientOptions' => ['removeMaskOnSubmit' => true],'options' => ['placeholder' => 'XXXX XXX XXX', 'class' => 'form-control']]) ?>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6 mb-0">
                            <?= $form->field($userModel, 'password_hash')->passwordInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $userModel->getAttributeLabel('password_hash')]) ?>
                        </div>
                        <div class="col-md-6 mb-0">
                            <?= $form->field($userModel, 'confirmPassword')->passwordInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $userModel->getAttributeLabel('confirmPassword')]) ?>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-4 mb-0">
                            <?= $form->field($model, 'company_position')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('company_position')]) ?>
                        </div>
                        <div class="col-md-4 mb-0">
                            <?= $form->field($model, 'status')->dropDownList(['ENABLED' => 'ENABLED', 'DISABLED' => 'DISABLED'], ['class' => 'form-control']) ?>
                        </div>
                        <div class="col-md-4 mb-0">
                            <?= $form->field($userModel, 'timezone')->dropDownList($timezone, ['class' => 'form-control myselect2', 'prompt' => 'Select Timezone', 'maxlength' => true]) ?>
                        </div>

                    </div>

                    <?= Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn btn-primary', 'style' => 'float: right;']) ?>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>

        </div>
    </section>
</div>
