<?php
/* @var $this yii\web\View */
$this->title = 'Dashboard';
?>


<div class="col-xl-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Section Box</h2>
            <div class="actions panel_actions float-right">
                <!--<i class="box_toggle fa fa-chevron-down"></i>-->
                <!--<i class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></i>-->
                <!--<i class="box_close fa fa-times"></i>-->
            </div>
        </header>
        <div class="content-body">    
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12">
                    <a class="btn btn-primary text-white tutor-unavailability" style="cursor:pointer" data-id="<?= $model->uuid; ?>"> Set Unavailability </a>
                </div>
            </div>
        </div>
    </section>                            
</div>