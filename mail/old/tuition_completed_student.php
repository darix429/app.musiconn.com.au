<?php

use yii\helpers\Html;
//echo $content['name']; exit;
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['student_name'] ?>,</p>

	    <p>Your Lesson has been Completed successfully in <?= Yii::$app->name; ?>.</p>

	    <p><strong>Tutor :</strong> <?= $content['tutor_name']; ?></p>
	    <p><strong>Lesson Time :</strong> <?= $content['time']; ?></p>
            <p><strong>Video Link :</strong> <?= $content['link'] ?></p> 

            <p> If any query for your account then please contact to administrator.</p>
            
            <p>Sincerely,</p>
            <p>System Administrators</p>
        </td>
    </tr>
</table>

