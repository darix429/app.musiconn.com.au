<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Url;
use app\models\Instrument;
use app\models\TutorialType;
use app\models\PaymentTerm;

$this->title = Yii::t('app', 'Book Music Lessons');
$this->params['breadcrumbs'][] = $this->title;

$activePaymentTerm = PaymentTerm::find()->where(['status'=>'ENABLED'])->asArray()->all();
if (isset($step_data['step_2'] ) && !empty($step_data['step_2']) ) {
    $model->onlinetutorial_uuid = $step_data['step_2']['onlinetutorial_uuid'];
    $model->onlinetutorialcheckbox = $step_data['step_2']['onlinetutorial_uuid'];
    $model->onlinetutorialtype = (!empty($step_data['step_2']['onlinetutorialtype'])) ? $step_data['step_2']['onlinetutorialtype'] : '';
}


if (!empty($step_data['step_1']['instrument_uuid'])) {
    $instrumentDetail = Instrument::find()->where(['uuid' => $step_data['step_1']['instrument_uuid']])->one();
} else {
    return \yii\web\Controller::redirect(['index']);
}

$this->registerJs(
        '
$(document).on("ifClicked","input[name=\"StudentTuitionBooking[onlinetutorialcheckbox]\"]",function (event) {

    $("#studenttuitionbooking-onlinetutorial_uuid").val(this.value);
    $(".onlinetutorial_select_btn").addClass("d-none");
    $(this).closest("tr.onlinetutorial_tr").find(".onlinetutorial_select_btn").removeClass("d-none");

});

function load_tutorial(typ=""){
    $.ajax({
            type: "GET",
            url: "' . Url::to(['book-music-tution/get-online-tutorial']) . '",
            data: {"type":typ},
            beforeSend: function() {
                blockBody();
            },
            success:function(result){
                $("#tutorial_list_parent").html(result);
                ULTRA_SETTINGS.iCheck();
                unblockBody();
            },error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
}

$("input[name=\"StudentTuitionBooking[onlinetutorialtype]\"]").on("change", function (event) {
    load_tutorial($(this).val());
});

$( "#frm_step_2,#gostep1" ).submit(function( event ) {
        blockBody();
    });
', View::POS_END
);

if (!empty($step_data['step_2']['onlinetutorialtype'])) {
    $this->registerJs(' load_tutorial("' . $step_data['step_2']['onlinetutorialtype'] . '");', View::POS_END);
}

?>
<div class="col-xl-12">
    <p class="bg-warning text-dark">For issues concerning tutor availability, please contact <a href="mailto:admin@musiconn.com.au">admin@muscionn.com.au</a> for assistance.</label>
    <nav class="navbar navbar-expand-lg navbar-light bg-primary text-light" style="">
        <h2 class="booking-nav-title float-left text-white" style="">Select Instrument &nbsp;<i class="fa fa-check"></i> </h2>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#change-step1" aria-controls="change-step1" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="change-step1">
            <?php echo Html::beginForm(['index'], 'post', ['id' => 'gostep1', 'class' => 'form-inline d-block']) ?>
            <input type="hidden" name="step" value="1">
            <input type="hidden" name="prev" value="1">
            <a href="javascript:$('#gostep1').submit();" class="btn btn-outline-success pull-right">
                Change
            </a>
            <p class="pull-right"> <?= $instrumentDetail->name; ?>&nbsp;</p>
            <?php echo Html::endForm() ?>

        </div>
    </nav>

    <section class="box ">
        <header class="panel_header bg-primary ">
            <h2 class="title float-left text-white">Select Plan</h2>
            <div class="actions panel_actions float-right">

            </div>
        </header>
        <div class="content-body padding-imp-30">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12">


                    <?php
                    $form = ActiveForm::begin([
                                'action' => ['index'],
                                'id' => 'frm_step_2',
                                'enableClientValidation' => true,
//                                'enableAjaxValidation' => true,
                                'validateOnChange' => true,
                                'options' => ['class' => 'form-horizontal']]);
                    ?>

                    <div class="col-lg-12 col-md-12 col-12 booking_search_data">

                        <div class="tab-pane fade show active " >

                            <div class=" text-center" >

                                <div class="btn-group btn-group-toggle " data-toggle="buttons">
                                <?php
                                    if (!empty($activePaymentTerm)) {
                                        foreach ($activePaymentTerm as $key_pt => $value_pt) {
                                            ?>
                                            <label class="btn btn-secondary <?= ($model->onlinetutorialtype == $value_pt['term']) ? "active" : "" ?>">
                                                <input type="radio" name="StudentTuitionBooking[onlinetutorialtype]" class="tutorial_type" autocomplete="off" value="<?= $value_pt['term']; ?>" <?= ($model->onlinetutorialtype == $value_pt['term']) ? "checked" : "" ?> > <?= $value_pt['term']; ?>
                                            </label>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>

                                <?php
                                     //$m_checked =  ($model->onlinetutorialtype == 'Monthly')  ? "checked" : "";
                                     //$q_checked =  ($model->onlinetutorialtype == 'Quarterly')  ? "checked" : "";
                                ?>

<!--                                <div class="btn-group btn-group-toggle " data-toggle="buttons">
                                    <label class="btn btn-secondary <?php // ($model->onlinetutorialtype == 'Monthly') ?"active":"" ?>">
                                        <input type="radio" name="StudentTuitionBooking[onlinetutorialtype]" class="tutorial_type" autocomplete="off" value="Monthly" <?php //$m_checked;?> > Monthly
                                    </label>
                                    <label class="btn btn-secondary <?php // ($model->onlinetutorialtype == 'Quarterly') ?"active":"" ?>">
                                        <input type="radio" name="StudentTuitionBooking[onlinetutorialtype]" class="tutorial_type" autocomplete="off" value="Quarterly" <?php //$q_checked;?>> Quarterly
                                    </label>
                                </div>-->
                                </div>

                            <input type="hidden" name="step" value="2">
                            <?= $form->field($model, 'onlinetutorial_uuid')->hiddenInput(['class' => 'form-control', 'maxlength' => true,])->label(false) ?>

                            <div id="tutorial_list_parent">
                                <?php
                                ?>
                            </div>


                        </div>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </section>


    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">SELECT Tutor</h2>
            <div class="actions panel_actions float-right">

            </div>
        </header>
    </section>
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Select Date</h2>
            <div class="actions panel_actions float-right">

            </div>
        </header>
    </section>
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Confirm and Payment</h2>
            <div class="actions panel_actions float-right">

            </div>
        </header>
    </section>
</div>
