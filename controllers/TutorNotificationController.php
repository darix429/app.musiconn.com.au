<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\models\TutorNotification;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Tutor;
use app\models\Modelmulti;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use \yii\base\Model;


/**
 * TutorNotificationController implements the CRUD actions for TutorNotification model.
 */
class TutorNotificationController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
        	'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => Yii::$app->user->can('/tutor-notification/index'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['validation'],
                        'allow' => Yii::$app->user->can('/tutor-notification/validation'),
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TutorNotification models.
     * @return mixed
     */
    public function actionIndex()
    {
        $id = Yii::$app->user->identity->tutor_uuid;
        $model = Tutor::findOne($id);
        $model->scenario = 'tutor_notification';
        
        $allNoti = TutorNotification::find()->where(['tutor_uuid' => $model->uuid])->all();
        if(!empty($allNoti)){
            foreach ($allNoti as $k => $v) {
                $modelsNotifications[] = $v;
            }
        } else {
            $modelsNotifications = [new TutorNotification];
        }

        if ($model->load(Yii::$app->request->post())) {
            
            $oldIDs = ArrayHelper::map($modelsNotifications, 'uuid', 'uuid');
            $modelsNotifications = Modelmulti::createMultiple(TutorNotification::classname(), $modelsNotifications);
            Model::loadMultiple($modelsNotifications, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsNotifications, 'uuid', 'uuid')));

            // ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
            }

            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($modelsNotifications) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        if (! empty($deletedIDs)) {
                            TutorNotification::deleteAll(['uuid' => $deletedIDs]);
                        }
                        foreach ($modelsNotifications as $modelAddress) {
                            $modelAddress->tutor_uuid = $model->uuid;
                            $modelAddress->type = $modelAddress->type;
                            $modelAddress->time_unit = $modelAddress->time_unit;
                            $modelAddress->unit_value = $modelAddress->unit_value;
                            if (! ($flag = $modelAddress->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        Yii::$app->response->format = 'json';
                        return ['code' => 200, 'message' => 'Notifications updated successfully.'];
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            } else {
                Yii::$app->response->format = 'json';
                $validatemulti = ArrayHelper::merge(
                    ActiveForm::validateMultiple($modelsNotifications), ActiveForm::validate($model)
                );
                return ['code' => 500, 'message' => $validatemulti];
            }
        }
        return $this->renderAjax('index', [
            'model' => $model,
            'modelsNotifications' => (empty($modelsNotifications)) ? [new TutorNotification] : $modelsNotifications
        ]);
    }

    public function actionValidation()
    {
        $id = Yii::$app->user->identity->tutor_uuid;
        $validateMulti = [];
        $model = Tutor::findOne($id);
        $model->scenario = 'tutor_notification';
        
        $allNoti = TutorNotification::find()->where(['tutor_uuid' => $model->uuid])->all();
        if(!empty($allNoti)){
            foreach ($allNoti as $k => $v) {
                $modelsNotifications[] = $v;
            }
        } else {
            $modelsNotifications = [new TutorNotification];
        }

        $model->attributes = Yii::$app->getRequest()->post('TutorNotification');

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
            $oldIDs = ArrayHelper::map($modelsNotifications, 'uuid', 'uuid');
            $modelsNotifications = Modelmulti::createMultiple(TutorNotification::classname(), $modelsNotifications);
            Model::loadMultiple($modelsNotifications, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsNotifications, 'uuid', 'uuid')));

            // ajax validation
            $validateMulti = ArrayHelper::merge(
                ActiveForm::validateMultiple($modelsNotifications), ActiveForm::validate($model)
            );
        }
        Yii::$app->response->format = 'json';
        return $validateMulti;
    }

    /**
     * Finds the TutorNotification model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return TutorNotification the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TutorNotification::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
