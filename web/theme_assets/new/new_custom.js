
var NEW_CUSTOM = window.NEW_CUSTOM || {};

/*--------------------------------
 DataTables
 --------------------------------*/
NEW_CUSTOM.dataTablesInit = function () {

    if ($.isFunction($.fn.dataTable)) {

        /*--- start ---*/
        
        $(".my-datatable").dataTable({
                responsive: true,
               "pageLength": dt_page_length,
                "dom": "<\"dataTables_wrapper\" <\"row\" <\"col-sm-12 col-md-4\"l><\"col-sm-12 col-md-4\"B><\"col-sm-12 col-md-4\"f>>  <\"row\" <\"col-sm-12\"t>> <\"row\" <\"col-sm-12 col-md-5\"i> <\"col-sm-12 col-md-7\"p>> >",
                buttons: [
                    "copy", "csv", "excel", "pdf", "print"
                ],
                aLengthMenu: [
                    [10, 25, 50, 100, -1],
                    [10, 25, 50, 100, "All"]
                ]
            });

        var tableex1 = $("#example-1").dataTable({
            responsive: true,
            aLengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "All"]
            ]
        });

        

//            $(".dataTables_filter").each(function() { 
//                $(this).closest('div.row').append('<div class="clearfix"></div> ');
//            });

        /*--- end ---*/

        /*--- start ---*/

        var tableex1 = $("#example-11").dataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

        /*--- end ---*/


        /*--- start ---*/

        $('#example-4').dataTable();

        /*--- end ---*/

        /*--- start ---*/

        // Setup - add a text input to each footer cell
        $('#example-3 tfoot th').each(function () {
            var title = $(this).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" />');
        });

        // DataTable
        var table = $('#example-3').DataTable();

        // Apply the search
        table.columns().every(function () {
            var that = this;

            $('input', this.footer()).on('keyup change', function () {
                if (that.search() !== this.value) {
                    that
                            .search(this.value)
                            .draw();
                }
            });
        });
        /*--- end ---*/
        /*--- start ---*/
        var table = $('#example-2').DataTable();

        $('#example-2 tbody')
                .on('mouseenter', 'td', function () {
                    var colIdx = table.cell(this).index().column;

                    $(table.cells().nodes()).removeClass('highlight');
                    $(table.column(colIdx).nodes()).addClass('highlight');
                });
        /*--- end ---*/



        var table5 = $('#example-5').DataTable();

        $('button.example-5-submit').click(function () {
            var data = table5.$('input, select').serialize();
            alert(
                    "The following data would have been submitted to the server: \n\n" +
                    data.substr(0, 120) + '...'
                    );
            return false;
        });

    }
};

/*--------------------------------
         iCheck
     --------------------------------*/
    NEW_CUSTOM.iCheck = function() {



        if ($.isFunction($.fn.iCheck)) {


            $('input[type="checkbox"].iCheck').iCheck({
                checkboxClass: 'icheckbox_minimal',
                radioClass: 'iradio_minimal',
                increaseArea: '20%'
            });


            var x;
            var colors = ["-green", "-red", "-yellow", "-blue", "-aero", "-orange", "-grey", "-pink", "-purple","-white"];

            for (x = 0; x < colors.length; x++) {

                if (x == 0) {
                    $('input.icheck-minimal').iCheck({
                        checkboxClass: 'icheckbox_minimal' + colors[x],
                        radioClass: 'iradio_minimal' + colors[x],
                        increaseArea: '20%'
                    });

                    $('input.skin-square').iCheck({
                        checkboxClass: 'icheckbox_square' + colors[x],
                        radioClass: 'iradio_square' + colors[x],
                        increaseArea: '20%'
                    });

                    $('input.skin-flat').iCheck({
                        checkboxClass: 'icheckbox_flat' + colors[x],
                        radioClass: 'iradio_flat' + colors[x],
                    });


                    $('input.skin-line').each(function() {
                        var self = $(this),
                            label = self.next(),
                            label_text = label.text();

                        label.remove();
                        self.iCheck({
                            checkboxClass: 'icheckbox_line' + colors[x],
                            radioClass: 'iradio_line' + colors[x],
                            insert: '<div class="icheck_line-icon"></div>' + label_text
                        });
                    });

                } // end x = 0

                $('input.icheck-minimal' + colors[x]).iCheck({
                    checkboxClass: 'icheckbox_minimal' + colors[x],
                    radioClass: 'iradio_minimal' + colors[x],
                    increaseArea: '20%'
                });


                $('input.skin-square' + colors[x]).iCheck({
                    checkboxClass: 'icheckbox_square' + colors[x],
                    radioClass: 'iradio_square' + colors[x],
                    increaseArea: '20%'
                });


                $('input.skin-flat' + colors[x]).iCheck({
                    checkboxClass: 'icheckbox_flat' + colors[x],
                    radioClass: 'iradio_flat' + colors[x],
                });


                $('input.skin-line' + colors[x]).each(function() {
                    var self = $(this),
                        label = self.next(),
                        label_text = label.text();

                    label.remove();
                    self.iCheck({
                        checkboxClass: 'icheckbox_line' + colors[x],
                        radioClass: 'iradio_line' + colors[x],
                        insert: '<div class="icheck_line-icon"></div>' + label_text
                    });
                });

            } // end for loop


        }
    };
    
    
    NEW_CUSTOM.bookingDateScript = function() { 
        //[Alpesh : For a Package Booking Process]
            $(".when-datepicker").each(function(i, e) {
                var $this = $(e),
                    options = {
                        minViewMode: getValue($this, 'minViewMode', 0),
                        format: getValue($this, 'format', 'dd-mm-yyyy'),
                        startDate: getValue($this, 'startDate', ''),
                        endDate: getValue($this, 'endDate', ''),
                        daysOfWeekDisabled: getValue($this, 'disabledDays', ''),
                        startView: getValue($this, 'startView', 0)
                    },
                    $nxt = $this.next(),
                    $prv = $this.prev();


                $this.datepicker(options);

                if ($nxt.is('.input-group-addon') && $nxt.has('a')) {
                    $nxt.on('click', function(ev) {
                        ev.preventDefault();
                        $this.datepicker('show');
                    });
                }

                if ($prv.is('.input-group-addon') && $prv.has('a')) {
                    $prv.on('click', function(ev) {
                        ev.preventDefault();

                        $this.datepicker('show');
                    });
                }
            });
    };







    // Element Attribute Helper
    function getValue($el, data_var, default_val) {
        if (typeof $el.data(data_var) != 'undefined') {
            return $el.data(data_var);
        }

        return default_val;
    }
    
    //buy_musicoin model start
    function buy_musicoin(){
        
            $.ajax({
            type: "GET",
            url: web_url + "musicoin/buy",
            data: {},
            beforeSend:function(){ blockBody(); },
            success: function (result) {
                
                $(".buy-musicoins-model-body").html(result);
                NEW_CUSTOM.iCheck();
                $("#buy-musicoins-model").modal({show: true});
                unblockBody();
            },
            error:function(e){
                unblockBody();
                $("#buy-musicoins-model").modal("hide");
                showErrorMessage(e.responseText);
            }
        });
    }
    
    function save_buy_musicoin(){
        $.ajax({
            type: "POST",
            url: web_url + "musicoin/buy",
            data: $("#frm_buycoin").serialize(),
            beforeSend:function(){ blockBody(); },
            success: function (result) {
                unblockBody();
                if(result.code == 200){
                    showSuccess(result.message);
                    $(".student_musicoin_balance_field").val(result.musicoin_balance);
                    $(".student_musicoin_balance_text").html(result.musicoin_balance);
                    $("#buy-musicoins-model").modal("hide");
                    location.reload();
                }else{
                    $(".buy-musicoins-model-body").html(result);
                    NEW_CUSTOM.iCheck();
                    $("#buy-musicoins-model").modal({show: true});
                }
                
                //$(".buy-musicoins-model-body").html(result);
                //NEW_CUSTOM.iCheck();
                 
            },
            error:function(e){
                unblockBody();
                $("#buy-musicoins-model").modal("hide");
                showErrorMessage(e.responseText);
            }
        });
    }
    
    //buy_musicoin model start
    function buy_musicoin_old(){
        
            $.ajax({
            type: "GET",
            url: web_url + "musicoin/buy",
            data: {},
            beforeSend:function(){ blockBody(); },
            success: function (result) {
                
                $(".buy-musicoins-model-body").html(result);
                NEW_CUSTOM.iCheck();
                $("#buy-musicoins-model").modal({show: true});
                unblockBody();
            },
            error:function(e){
                unblockBody();
                $("#buy-musicoins-model").modal("hide");
                showErrorMessage(e.responseText);
            }
        });
    }
    
    function save_step_1(){
        $.ajax({
            type: "POST",
            url: web_url + "musicoin/buy",
            data: $("#frm_step_1").serialize(),
            beforeSend:function(){ blockBody(); },
            success: function (result) {
                unblockBody();
                $(".buy-musicoins-model-body").html(result);
                NEW_CUSTOM.iCheck();
                $("#buy-musicoins-model").modal({show: true});
            },
            error:function(e){
                unblockBody();
                $("#buy-musicoins-model").modal("hide");
                showErrorMessage(e.responseText);
            }
        });
    }
    function save_step_2(){
        $.ajax({
            type: "POST",
            url: web_url + "musicoin/buy",
            data: $("#frm_step_2").serialize(),
            beforeSend:function(){ blockBody(); },
            success: function (result) {
                unblockBody();
                if(result.code == 200){
                    showSuccess(result.message);
                    $(".student_musicoin_balance_field").val(result.musicoin_balance);
                    $(".student_musicoin_balance_text").html(result.musicoin_balance);
                    $("#buy-musicoins-model").modal("hide");
                }else{
                    $(".buy-musicoins-model-body").html(result);
                    NEW_CUSTOM.iCheck();
                    $("#buy-musicoins-model").modal({show: true});
                }
                 
            },
            error:function(e){
                unblockBody();
                $("#buy-musicoins-model").modal("hide");
                showErrorMessage(e.responseText);
            }
        });
    }
    function refresh_step_2_after_card_remove(){
        $.ajax({
            type: "POST",
            url: web_url + "musicoin/buy",
            data: $("#gostep-reload-2").serialize(),
            beforeSend:function(){ blockBody(); },
            success: function (result) {
                unblockBody();
                $(".buy-musicoins-model-body").html(result);
                NEW_CUSTOM.iCheck();
                $("#buy-musicoins-model").modal({show: true});
            },
            error:function(e){
                unblockBody();
                $("#buy-musicoins-model").modal("hide");
                showErrorMessage(e.responseText);
            }
        });
    }
    function goto_step(){
        $.ajax({
            type: "POST",
            url: web_url + "musicoin/buy",
            data: $("#frm_gotostep").serialize(),
            beforeSend:function(){ blockBody(); },
            success: function (result) {
                unblockBody();
                
                $(".buy-musicoins-model-body").html(result);
                NEW_CUSTOM.iCheck();
                $("#buy-musicoins-model").modal({show: true});
            },
            error:function(e){
                unblockBody();
                $("#buy-musicoins-model").modal("hide");
                showErrorMessage(e.responseText);
            }
        });
    }
    
    function buycoin_cart_refresh(loader_hide = true){

    var coupon_code = $("#studentmusicoinpackage-coupon_code").val();
    var package_id = $("#studentmusicoinpackage-package_uuid").val();
    $.ajax({
        type: "GET",
        url: web_url + "musicoin/refresh-cart",
        data: { "coupon_code":coupon_code,"package_id":package_id},
        beforeSend: function() {
            blockBody();
        }, success:function(result){

            if(result.code == 200){

                $("#studentmusicoinpackage-cart_total").val(result.result.cart_total);
                $("#cart_total_text").html(result.result.cart_total);

                $("#studentmusicoinpackage-cart_total_coins").val(result.result.cart_total_coins);
                $("#cart_total_coins_text").html(result.result.cart_total_coins);

                $("#studentmusicoinpackage-promo_coins").val(result.result.promo_coins);
                $("#promo_coins_text").html(result.result.promo_coins);

                $("#studentmusicoinpackage-coupon_uuid").val(result.result.coupon_uuid);
                $("#studentmusicoinpackage-discount").val(result.result.discount);
                $("#discount_text").html(result.result.discount);
                
                $("#plan_price_text").html(result.result.price);
                $("#plan_gst_text").html(result.result.gst);
                $("#plan_total_price_text").html(result.result.total_price);

                if(result.result.coupon_code != ""){
                    $(".promo-code-msg").text(result.result.message);
                }
                
                if(result.result.is_available){
                    $(".promo-code-msg").removeClass("text-danger");
                    $(".promo-code-msg").addClass("text-success");
                }else{
                    $(".promo-code-msg").removeClass("text-success");
                    $(".promo-code-msg").addClass("text-danger");
                }

                if(loader_hide){
                    unblockBody();
                }
            }else{
                unblockBody();
                showErrorMessage(result.message);
            }
        },error:function(e){
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
}
    
    //buy_musicoin model end


/******************************
     initialize respective scripts 
     *****************************/
    $(document).ready(function() {
        NEW_CUSTOM.dataTablesInit();
        NEW_CUSTOM.iCheck();
        NEW_CUSTOM.bookingDateScript();
    });