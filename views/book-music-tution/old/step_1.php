<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Book Music Lessons');
$this->params['breadcrumbs'][] = $this->title;

if(isset($step_data['step_1']['instrument_uuid']) && $step_data['step_1']['instrument_uuid'] != ""){
    $model->instrument_uuid = $step_data['step_1']['instrument_uuid'];
    $model->instrumentcheckbox = $model->instrument_uuid;
}
//echo "<pre>";
//print_r($step_data);
//echo "</pre>";
$this->registerJs(
        '
    $("input[name=\"StudentTuitionBooking[instrumentcheckbox]\"]").on("ifClicked", function (event) {
        $("#studenttuitionbooking-instrument_uuid").val(this.value);

        $(".instrument_select_btn").addClass("d-none")
        $(this).closest("tr.instrument_tr").find(".instrument_select_btn").removeClass("d-none")
    });

    $( "#frm_step_1" ).submit(function( event ) {
        blockBody();
    });

        ', View::POS_END
);
?>
<style>
    .field-studenttuitionbooking-instrumentcheckbox{
        margin-bottom: 0px !important;
    }
</style>
<div class="col-xl-12">
    <p class="bg-warning text-dark">For issues concerning tutor availability, please contact <a href="mailto:admin@musiconn.com.au">admin@muscionn.com.au</a> for assistance.</label>
    <section class="box ">
        <header class="panel_header bg-primary ">
            <h2 class="title float-left text-white">Select Instrument</h2>
            <div class="actions panel_actions float-right">

            </div>
        </header>
        <div class="content-body padding-imp-30">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12">

                    <?php
                    $form = ActiveForm::begin([
                                'action' => ['index'],
                                'id' => 'frm_step_1',
                                'enableClientValidation' => true,
//                              'enableAjaxValidation' => true,
                                'validateOnChange' => true,
                                'options' => ['class' => 'form-horizontal']]);
                    ?>
                    <div class="col-lg-12 col-md-12 col-12 booking_search_data">
                        <div class="tab-pane fade show active " >
                            <input type="hidden" name="step" value="1">
                            <?= $form->field($model, 'instrument_uuid')->hiddenInput(['class' => 'form-control', 'maxlength' => true,])->label(false) ?>
                            <table class="table table-striped table-hover">
                                <?php
                                if (!empty($instrumentList)) {
                                    foreach ($instrumentList as $key => $value) {
                                        ob_start();
                                        fpassthru($value['image']);
                                        $contents = ob_get_contents();
                                        ob_end_clean();
                                        $dataUri = "data:image/jpg;base64," . base64_encode($contents);

                                        $model->instrumentcheckbox = $model->instrument_uuid;
                                        $displayClass = ($model->instrument_uuid == $value->uuid) ? "" : "d-none";

                                        $totalInstrumentsTutor = \app\models\TutorInstrument::find()->joinWith(['tutor'])->where(['instrument_uuid' => $value->uuid,'tutor.status' => 'ENABLED'])->count();

                                        ?>

                                        <tr class="unread instrument_tr" >
                                            <td class="" style="width: 10%">
                                                <?php
                                                echo $form->field($model, 'instrumentcheckbox')->radio([
                                                    'label' => '',
                                                    'template' => '{input}{error} ',
                                                    'class' => 'skin-square-red float-left instrument_checkbox', 'value' => $value->uuid])->label('')
                                                ?>
                                            </td>
                                            <td class=""style="width: 10%">
                                                <img class="" style="width: 30px;" src="<?= $dataUri; ?>">
                                            </td>
                                            <td class="open-view"><?= $value->name ?> (<?= $totalInstrumentsTutor; ?>)</td>

                                            <td class="open-view">
                                                <?= Html::submitButton('Select', ['class' => "btn btn-orange btn-md instrument_select_btn $displayClass"]) ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </table>
                        </div>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </section>

    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Select Plan</h2>
            <div class="actions panel_actions float-right">

            </div>
        </header>
    </section>
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">SELECT Tutor</h2>
            <div class="actions panel_actions float-right">

            </div>
        </header>
    </section>
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Select Date</h2>
            <div class="actions panel_actions float-right">

            </div>
        </header>
    </section>
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Confirm and Payment</h2>
            <div class="actions panel_actions float-right">

            </div>
        </header>
    </section>
</div>
