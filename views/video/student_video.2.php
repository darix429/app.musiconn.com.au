<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\ArrayHelper;
use app\models\InstrumentCategory;
use app\models\Instrument;

$this->title = Yii::t('app', 'VIDEO LIBRARY');
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_icon'] = 'VideoLibrary-gradient.png';

$this->params['page_title_custom'] = ''
        . '         <div class="col-md-12">
                        <div class="col-md-5">
                            <h3>
                                <img src="'. Yii::$app->request->baseUrl . '/theme_assets/new_book/img/'.$this->params['page_icon'] .'" style="width: 52px;position: relative;">'
            . '                 <strong class="title-hm">'.strtoupper($this->title).'</strong>
                            </h3>
                        </div>
                        <div class="col-md-7 text-right">
                            <a href="'.Url::to(['/booked-session/completed']).'" class="btn btn-primary btn-sm black_button_in" >MY RECORDED LESSONS</a>
                            <a href="'.Url::to(['/video/student-video-list/']).'" class="btn btn-primary btn-sm black_button_in" >VIDEO LIBRARY</a>
                        </div>
                    </div>
                    ';

$categoryArray = ArrayHelper::map(InstrumentCategory::find()->asArray()->all(), 'uuid', 'name');
$instrumentArray = ArrayHelper::map(Instrument::find()->where(['status' => 'ACTIVE'])->orderBy('name')->asArray()->all(), 'uuid', 'name');

$this->registerJs(
'

$(document).on("change",".instrument_btn", function(){  
    var page_no = 0;
    $("#page_no").val(page_no);
    $("#search_input").val("");
    $("#instrument_input").val($(this).data("bind"));

    var instrument = $(this).children("option:selected").val();
    
    $.ajax({
        type: "GET",
        url: "' . Url::to(['/video/student-video-list/']) . '",
        data: {"instrument_uuid" : instrument,"page_no" : page_no},
        beforeSend:function(){ blockBody(); },
        success:function(result){
        
            unblockBody();
           
            $("#pre_recorded_video_table_parent_div").html(result.dataHtml);
        },
        error:function(e){ 
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
}); 

$(document).on("click",".search_input_btn", function(){  
    var page_no = 0;
    $("#page_no").val(page_no);
    var search_input = $("#search_input").val();
    $("#instrument_input").val("");
    
    $.ajax({
        type: "POST",
        url: "' . Url::to(['/video/student-video-list/']) . '",
        data: {"search_input" : search_input,"page_no" : page_no},
        beforeSend:function(){ blockBody(); },
        success:function(result){
        
            unblockBody();
           
            $("#pre_recorded_video_table_parent_div").html(result.dataHtml);
        },
        error:function(e){ 
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
}); 


$(document).on("click","#load_more", function(){

    var page_no = $("#page_no").val();
    page_no = (page_no > 0) ? page_no : 0;
    page_no = parseInt(page_no) + 1;
    $("#page_no").val(page_no);
    
    var search_input = $("#search_input").val();
    
    $.ajax({
        type: "GET",
        url: "' . Url::to(['/video/student-video-list/']) . '",
        data: {"instrument_uuid" : $("#instrument_input").val(),"page_no" : page_no, "search_input" : search_input },
        beforeSend:function(){ blockBody(); },
        success:function(result){
        
            unblockBody();
            if(result.result){
                $("#pre_recorded_video_table_parent_div").append(result.dataHtml);
            } else {
                showErrorMessage("No more video found");
            }
        },
        error:function(e){ 
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});  
', View::POS_END
);
?>
<style>
    .instrument_btn{
        cursor: pointer;
    }
    .search_input_btn{
        cursor: pointer;
    }
</style>
<section class="content">

    <div class="row">
        <div class="col-md-12">

            <!-- Filters -->
            <div class="row">
                <div class="col-md-4">
                    <label>Instruments</label>
                    <select name="" id="" class="form-control instrument_btn">
                        <option value="">All</option>
                        <?php
                            foreach($instrumentArray as $k => $v) {
                                $instrument_video_count = \app\models\PreRecordedVideo::instrumentPreRecordedVideoCount($k);
                        ?>
                            <option value="<?= $k ?>"><?= " $v ($instrument_video_count)" ?></option>
                        <?php } ?>
                    
                    </select>
                </div>
                <div class="col-md-4">
                    <label>Tutor</label>
                    <select name="" id="" class="form-control">
                        <option value="">All</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <div class="form-group ">
                        <label for="">Search</label>
                        <div class="input-group">
                            <input type="text" name="search_input" id="search_input" class="form-control" value="<?= (!empty($search_input))?$search_input:""; ?>">
                            <span class="input-group-addon search_input_btn"><i class="fa fa-search"></i></span>
                        </div>
                        <input type="hidden" name="page_no" id="page_no" value="<?= (!empty($page_no))?$page_no:0; ?>">
                        <input type="hidden" name="instrument_input" id="instrument_input" value="<?= (!empty($instrument_uuid))?$instrument_uuid:""; ?>">
                    </div>
                </div>
            </div>

            <!-- LIST -->
            <div class="row">
                <div class="col-md-12">
                    <div>
                        <?= "search: $search_input" ?>
                    </div>

                    <div class="container-fluid video_categry-bg-white" id="pre_recorded_video_table_parent_div">
                        <!-- <?= $recodedVideoListHtml; ?> -->
                        <?php var_dump($collection) ?>
                    </div>
                    <div class="change_pass" style="margin-top: 10px;cursor: pointer;"><a class="" id="load_more">SEE MORE</a></div>
                </div>
            </div>
        
        </div>
    </div>
    
</section>
