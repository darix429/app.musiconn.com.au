<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;
use app\models\Tutor;
use app\models\BookedSession;
use app\libraries\General;

$this->title = Yii::t('app', 'CANCELLED LESSONS');
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_icon'] = 'my-lesson-tag.png';
?>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <?php /* <div class="row center-block box-search-area">
                        <div class="col-lg-12 col-md-12 col-12 ">
                            <div class="datatable-search-div " style="display: block; margin-bottom: 10px;">
                                <?php
                                $s_c_count = \app\models\CreditSession::getStudentCreditSessionCount(Yii::$app->user->identity->student_uuid);
                                ?>
                                <?= Html::Button(Yii::t('app', 'SCHEDULE A NEW LESSON'), ['class' => 'btn btn-primary mb-0', 'data-creditsession' => $s_c_count, 'id' => 'studentBookingOption_button',]) ?>

                                <label class="pull-right" style="cursor: default;" style="cursor: default;">Lesson Credits = <?= $s_c_count ?> </label>

                            </div>
                        </div>
                    </div> */ ?>
                    <div class="clearfix"></div>
                    <div class="row" >
                        <!---start---->
                        <div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">
                            <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Tutor</th>
                                        <th>Date</th>
                                        <th>Time</th>
                                        <th>Cancel Date</th>
                                        <!--<th>Reason</th>-->
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($canceledList)) {
                                        foreach ($canceledList as $key => $value) {
                                            $now = date('Y-m-d H:i:s');
                                            ?>
                                            <tr>
                                                <td><?= $key + 1; ?></td>
                                                <td><?= $value['tutorUu']['first_name'] . ' ' . $value['tutorUu']['last_name']; ?></td>
                                                <td><?= General::displayDate($value['start_datetime']); ?></td>
                                                <td><?= General::displayTime($value['start_datetime']) . " - " . General::displayTime($value['end_datetime']); ?></td>
                                                <td><?= General::displayDate($value['cancel_datetime']); ?></td>                                                                    
                                                <!--<td><?php //$value['cancel_reason'];  ?></td>-->                                                                    
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>

                        </div>
                        <!---end--->
                    </div>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <a class="btn btn-primary btn-sm black_button_in" href="<?= Yii::$app->getUrlManager()->createUrl('/student/home'); ?>">RETURN TO HOME</a>

</section>