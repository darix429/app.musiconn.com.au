<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Sign In';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs(
        '
//    $("input.skin-square-orange").iCheck({
//        checkboxClass: "icheckbox_square-orange",
//        increaseArea: "20%"
//    });

', \yii\web\View::POS_END
);

if((Yii::$app->session->get('registration_success') && Yii::$app->session->get('registration_success') == 'registration_success') || isset($verifyEmail)){
$this->registerJs(
        '
        $("#signup_success_model").modal({
            backdrop: "static",
            keyboard: false,
        }) 
        
', \yii\web\View::POS_END
);
Yii::$app->session->remove('registration_success');
}
?>


<div class="login-box">
    <!-- /.login-logo -->
    <div class="login-box-body">
        <?php
        $form = ActiveForm::begin([
                    'id' => 'login-form',
                    'options' => ['name' => 'loginform'],
        ]);
        ?>

        <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'class' => 'form-control']) ?>
        <?= $form->field($model, 'password')->passwordInput(['class' => 'form-control']) ?>
        <?php
        echo $form->field($model, 'rememberMe')->checkbox([
            'template' => '<p class="forgetmenot"><label class="icheck-label form-label" for="rememberme"> {input} Remember me</label> <p>',
            'class' => 'skin-square-orange '])->label(false)
        ?>

        <div class="row">

            <center>
                <?= Html::submitButton('Sign In', ['class' => 'login_btn btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>

            </center>
            <!-- /.col -->
        </div>
        <?php ActiveForm::end(); ?>
        <center>
            <?= Html::a('Forgot password?', ['site/forgot-password'], []) ?>
            <br>
            <?= Html::a('Not a member?', ['site/signup'], ['class' => 'text-center']) ?>
    	    <?php 
		//if (strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') == false) {
    			// User agent is Google Chrome
//			echo '<br/><br/><span class="text-center" style="padding-top:10px;">This product was built for use in Chrome browser only, for download <a href="https://www.google.com/chrome/" target="_blank">click here</a>.</span>';
			//2019-12-12 echo '<br/><br/><span class="text-center" style="padding-top:10px;font-size:17px;">This product was built for use in <font style="color:red;"><b>Chrome browser</b></font> only, for latest version download please <a href="https://www.google.com/chrome/" target="_blank" style="text-decoration: underline;">click here</a>.</span>';
		//}
            $response = \app\libraries\General::JitsiCheckDevice();
            if($response['success'] != 'true'){
                echo '<br/><br/><span class="text-center" style="padding-top:10px;font-size:17px;">'.$response['msg'].'</span>';
            }
//            echo "<pre>";
//print_r($response);
//echo "</pre>";
//exit;
	    ?>
        </center>
        <div class="login-logo">
            <a href="<?= Yii::$app->params['base_url']; ?>"><img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new/' ?>img/login-logo.png"></a>
            <center><p class="login_btm"><?= Yii::$app->params['copyright']; ?></p></center>
        </div>
    </div>
    <!-- /.login-box-body -->
</div>

<!-- modal-content-Signup Success-->
<div class="modal fade" id="signup_success_model">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center">Thank you for Registration!</h4>
            </div>
            <div id="signup_success_div">
                <div class="modal-body">
                    <div class="row vertical-center-box vertical-center-box-tablet" >
                        <div class=" col-xs-12 mar-bottom-10 text-center">
                            <img src="<?= Yii::$app->request->baseUrl . '/theme_assets/images/success.png'?>" width="70px">
                            <!--<h1><i class="fa fa-check-circle-o"></i></h1>-->
                        </div>
                        <div class=" col-xs-12 mar-bot-15">
                                <label class="text-justify cont_title_add">
                                    An email has been sent to your registration email address.  Please follow the link in the email to activate your Musiconn Student account.
                                </label>
                            </div>
                        
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- /.modal -->