<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Instrument;

/**
 * InstrumentSearch represents the model behind the search form of `app\models\Instrument`.
 */
class InstrumentSearch extends Instrument {

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
                [['uuid', 'name', 'description', 'image', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $modeDatatable = false, $statusnot = '') {
        $query = Instrument::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        if (!empty($statusnot)) {
            if (is_array($statusnot)) {
                $query->andWhere(['not in', 'status', $statusnot]);
            } else {
                $query->andWhere(['<>', 'status', $statusnot]);
            }
        }

        $query->andFilterWhere(['status' => $this->status]);

        // grid filtering conditions
        $query->andFilterWhere(['ilike', 'uuid', $this->uuid])
                ->andFilterWhere(['ilike', 'name', $this->name])
                ->andFilterWhere(['ilike', 'description', $this->description]);
        // ->andFilterWhere(['ilike', 'image', $this->image]);
        if ($modeDatatable) {
            $result = $query->asArray()->all();
            return $result;
        }
        return $dataProvider;
    }

}
