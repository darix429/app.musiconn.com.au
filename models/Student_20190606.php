<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "student".
 *
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $phone_number
 * @property string $skype
 * @property string $street
 * @property string $city
 * @property string $state
 * @property string $country
 * @property string $postal_code
 * @property string $status
 * @property string $enrollment_id
 * @property string $student_uuid
 */
class Student extends \yii\db\ActiveRecord {

    public $state_text;
    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'student';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
                [['first_name', 'last_name', 'email', 'phone_number', 'street', 'city', 'country', 'postal_code', 'birthdate'], 'required'],
                [['first_name', 'last_name', 'email', 'skype', 'street', 'city', 'state', 'country', 'postal_code', 'status', 'enrollment_id', 'student_uuid'], 'string'],
                //[['postal_code'], 'number'],
                [['student_uuid'], 'unique'],
                [['created_at', 'updated_at'], 'integer'],
                //[['phone_number'], 'match','pattern'=> '/^\d{10}$/','message' => 'Enter a 10 digit Phone Number.'],
                //[['phone_number'], 'match','pattern'=> '/^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$/','message' => 'Enter a valid Phone Number.'],
                //[['postal_code'], 'string', 'max' => 4, 'min' => 4],
            
                [['birthdate'], 'ckeckDateMaxCurrentdate'],
                [['activation_link_token','state','state_text'], 'safe'],
                [['activation_link_token'], 'string'],
                [['birthdate'], 'date', 'format' => 'php:d-m-Y'],
            
                [['phone_number'], 'match',
                    'pattern'=> '/^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$/',
                    'message' => 'Enter a valid Phone Number.',
                    'when' => function($model) {
                    return ($model->country != "other");
                }, 'whenClient' => "function (attribute, value) {
                    return ( $('#student-country').val() !== 'other')?true:false;
                }"],
                        
                [['phone_number'], 'match',
                    'pattern'=> '/^\(?\b([0-9]{4})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{3})\b$/',
                    'message' => 'Enter a 10 digit Phone Number.',
                    'when' => function($model) {
                    return ($model->country == "other");
                }, 'whenClient' => "function (attribute, value) {
                    return ( $('#student-country').val() === 'other')?true:false;
                }"],
                        
                [['postal_code'], 'match',
                    'pattern'=> '/^\d{4}$/',
                    'message' => 'Enter a 4 digit postal code.',
                    'when' => function($model) {
                    return ($model->country != "other");
                }, 'whenClient' => "function (attribute, value) {
                    return ( $('#student-country').val() !== 'other')?true:false;
                }"],
                        
                [['postal_code'], 'string', 'max' => 10,
                    'when' => function($model) {
                    return ($model->country == "other");
                }, 'whenClient' => "function (attribute, value) {
                    
                    return ( $('#student-country').val() === 'other')?true:false;
                }"],

                [['state'], 'required', 'when' => function($model) {
                    return ($model->country != "other");
                }, 'whenClient' => "function (attribute, value) {
                    return ( $('#student-country').val() !== 'other')?true:false;
                }"],
                [['state_text'], 'required', 'when' => function($model) {
                    return ($model->country == "other");
                },'whenClient' => "function (attribute, value) {
                    return ( $('#student-country').val() == 'other')?true:false;
                }"],
                [['birthdate','state','state_text'], 'safe','on'=>'student_notification'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'email' => Yii::t('app', 'Email'),
            'phone_number' => Yii::t('app', 'Phone Number'),
            'skype' => Yii::t('app', 'Skype'),
            'street' => Yii::t('app', 'Street'),
            'city' => Yii::t('app', 'City'),
            'state' => Yii::t('app', 'State/Territory'),
            'state_text' => Yii::t('app', 'State/Territory'),
            'country' => Yii::t('app', 'Country'),
            'postal_code' => Yii::t('app', 'Postal Code'),
            'status' => Yii::t('app', 'Status'),
            'enrollment_id' => Yii::t('app', 'Enrollment ID'),
            'student_uuid' => Yii::t('app', 'Student Uuid'),
            'birthdate' => Yii::t('app', 'Birthdate'),
            'created_at' => Yii::t('app', 'Join Date'),
            'activation_link_token' => 'Activation Link Token',
        ];
    }

    public function ckeckDateMaxCurrentdate($attribute) {
        $label = $this->getAttributeLabel($attribute);
        $currDate = date('Y-m-d');
        $attr_val = $attribute;
        $att_date = date("Y-m-d", strtotime($this->$attr_val));
        if ($att_date == $currDate) {
            $this->addError($attribute, $label . " must be less than today date.");
        } elseif ($att_date > $currDate) {
            $this->addError($attribute, $label . " can not allow to future date.");
        }
    }

    public static function isActivationTokenValid($token, $uuid) {
        $flag = false;
        if (empty($token)) {
            $flag = false;
        }
        if (!empty($uuid)) {
            $model = Student::find()->where(['student_uuid' => $uuid])->one();
            if (!empty($model)) {
                if ($model->activation_link_token == $token) {
                    $expire = Yii::$app->params['user.passwordResetTokenExpire'];
                    $parts = explode('_', $token);
                    $timestamp = (int) end($parts);
                    $flag = ($timestamp + $expire >= time());
                }
            }
        }

        return $flag;
    }

    public function generateActivationToken() {
        $token = Yii::$app->security->generateRandomString() . '_' . time();
        return $token;
    }

    public function CheckBookedTution($student_id) {
        $student = \app\models\BookedSession::find()->joinWith(['studentUu'])->where(['booked_session.student_uuid' => $student_id, 'booked_session.status' => 'SCHEDULE'])->all();
        if (!empty($student)) {
            return $student;
        } else {
            return array();
        }
    }

    public function CancelBookedTution($book_id) {
        $status = false;
        $studentSession = \app\models\BookedSession::find()->where(['booked_session.uuid' => $book_id, 'booked_session.status' => 'SCHEDULE'])->all();
        if (!empty($studentSession)) {
            foreach($studentSession as $key => $value){
                $value->status = 'CANCELLED';
                $value->cancel_by = 'STUDENT';
                $value->cancel_user_uuid = $value->student_uuid;
                $value->cancel_reason = 'ACCOUNT_DELETED';
                $value->cancel_datetime = date('Y-m-d H:i:s', time());
                if ($value->save(false)) {
                    $status = true;
                }else{
                    $status = false;
                }
            }
//            $value->-save();
            return $status;
        } else {
            return $status;
        }
    }

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['step_2'] = ['number', 'name', 'cvv', 'cc_month_year'];
        $scenarios['student_notification'] = ['student_uuid'];
        return $scenarios;
    }

}
