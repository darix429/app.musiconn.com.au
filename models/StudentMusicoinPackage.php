<?php

namespace app\models;

use Yii;
use app\libraries\General;
use kartik\mpdf\Pdf;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "student_musicoin_package".
 *
 * @property string $uuid
 * @property string $student_uuid
 * @property string $name
 * @property string $description
 * @property string $price
 * @property string $gst
 * @property string $total_price
 * @property string $coins
 * @property string $bonus_coins
 * @property string $total_coins
 * @property string $bonus_coins_amount
 * @property string $order_uuid
 * @property string $coupon_code
 * @property string $coupon_uuid
 * @property string $created_at
 */
class StudentMusicoinPackage extends \yii\db\ActiveRecord
{
    public $package_uuid;
    public $package_ckeck;
    public $saved_cc_ckeck;
    
    public $cc_name;
    public $cc_number;
    public $cc_cvv;
    public $cc_month_year;
    
    public $s_cc_name;
    public $s_cc_number;
    public $s_cc_cvv;
    public $s_cc_month_year;
    public $cc_uuid;
    public $cc_checkbox;
    public $term_condition_checkbox;
    public $term_condition_checkbox_1;
    public $discount;
    public $cart_total;
    public $cart_total_coins;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'student_musicoin_package';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'student_uuid', 'name', 'description', 'order_uuid', 'coupon_code', 'coupon_uuid'], 'string'],
            [['price', 'gst', 'total_price', 'coins', 'bonus_coins', 'total_coins', 'bonus_coins_amount','cart_total_coins','promo_coins'], 'number'],
            [['created_at','package_ckeck','saved_cc_ckeck'], 'safe'],
            
            [['package_uuid'], 'required', 'on' => 'step_1', 'message' => 'This field cannot be blank. Select any one Package.'],
            
            [['creative_kids_voucher_code','kids_name','kids_dob','kids_postal_code'],'creativeCodeCriteria' ,'on' => 'step_2'],
            [['cc_checkbox', 'cc_uuid', 'discount', 'coupon_code','creative_kids_voucher_code','kids_name','kids_dob','kids_postal_code','promo_coins'], 'safe', 'on' => 'step_2'],
            [['cc_name', 'cc_number', 'cc_cvv', 'cc_month_year', 'cart_total','cart_total_coins'], 'required', 'on' => 'step_2',],
            [['cc_month_year'], 'date', 'format' => 'php:Y-m', 'on' => 'step_2',],
            [['term_condition_checkbox'], 'required', 'on' => 'step_2', 'requiredValue' => 1, 'message' => 'Accept Terms and Condition'],
            [['cc_number'], 'integer', 'on' => 'step_2',],
            [['cc_number'], 'string', 'max' => 16, 'on' => 'step_2',],
            [['cc_number'], 'string', 'min' => 14, 'on' => 'step_2',],
            [['cc_cvv'], 'string', 'min' => 3, 'on' => 'step_2',],
            [['cc_cvv'], 'string', 'max' => 5, 'on' => 'step_2',],
            [['discount','cart_total'], 'default', 'value' => 0],
            [['coupon_code'], 'validate_coupon', 'on' => 'step_2',],
            
            [['package_uuid',], 'required', 'on' => 'buycoin_popup', 'message' => 'This field cannot be blank. Select any one Package.'],
            [['saved_cc_ckeck'], 'required', 'on' => 'buycoin_popup', 'message' => 'This field is required. Select Payment option.'],
            [['creative_kids_voucher_code','kids_name','kids_dob','kids_postal_code'],'creativeCodeCriteria' ,'on' => 'buycoin_popup'],
            [['created_at','package_ckeck','saved_cc_ckeck','cc_checkbox', 'cc_uuid', 'discount', 'coupon_code','creative_kids_voucher_code','kids_name','kids_dob','kids_postal_code','promo_coins'], 'safe', 'on' => 'buycoin_popup'],
            [['cc_month_year'], 'date', 'format' => 'php:Y-m', 'on' => 'buycoin_popup',],
            
            [['cc_number'], 'integer', 'on' => 'buycoin_popup',],
            [['cc_number'], 'string', 'max' => 16, 'on' => 'buycoin_popup',],
            [['cc_number'], 'string', 'min' => 14, 'on' => 'buycoin_popup',],
            [['cc_cvv','s_cc_cvv'], 'string', 'min' => 3, 'on' => 'buycoin_popup',],
            [['cc_cvv','s_cc_cvv'], 'string', 'max' => 5, 'on' => 'buycoin_popup',],
            [['discount','cart_total'], 'default', 'value' => 0],
            [['coupon_code'], 'validate_coupon', 'on' => 'buycoin_popup',],
            
            [['cart_total','cart_total_coins','saved_cc_ckeck'], 'required', 'on' => 'buycoin_popup',],
            [['term_condition_checkbox'], 'required', 'on' => 'buycoin_popup', 'requiredValue' => 1, 'message' => 'Accept Terms and Condition',
                'when' => function ($model){
                    return ($model->saved_cc_ckeck == 'other');
                },'whenClient' => 'function (attribute, value) { $("input[name=\"StudentMusicoinPackage[saved_cc_ckeck]\"]").val() == "other"; }',
                ],
            [['term_condition_checkbox_1'], 'required', 'on' => 'buycoin_popup', 'requiredValue' => 1, 'message' => 'Accept Terms and Condition',
                'when' => function ($model){
                    return (!empty($model->saved_cc_ckeck) && $model->saved_cc_ckeck != "other");
                },'whenClient' => 'function (attribute, value) { ($("input[name=\"StudentMusicoinPackage[saved_cc_ckeck]\"]").val() != "" &&  $("input[name=\"StudentMusicoinPackage[saved_cc_ckeck]\"]").val() !== "other" ); }',
                ],
            
            [['cc_name', 'cc_number', 'cc_cvv', 'cc_month_year', 'cart_total','cart_total_coins'], 'required', 
                'on' => 'buycoin_popup',
                'when' => function ($model){
                    return ($model->saved_cc_ckeck == 'other');
                },'whenClient' => 'function (attribute, value) { $("input[name=\"StudentMusicoinPackage[saved_cc_ckeck]\"]").val() == "other"; }',],
            [['s_cc_cvv'], 'required', 
                'on' => 'buycoin_popup',
                'when' => function ($model){
                    return (!empty($model->saved_cc_ckeck) && $model->saved_cc_ckeck != "other");
                },'whenClient' => 'function (attribute, value) { ($("input[name=\"StudentMusicoinPackage[saved_cc_ckeck]\"]").val() != "" &&  $("input[name=\"StudentMusicoinPackage[saved_cc_ckeck]\"]").val() !== "other" ); }',],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'student_uuid' => Yii::t('app', 'Student Uuid'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'price' => Yii::t('app', 'Price'),
            'gst' => Yii::t('app', 'Gst'),
            'total_price' => Yii::t('app', 'Total Price'),
            'coins' => Yii::t('app', 'Coins'),
            'bonus_coins' => Yii::t('app', 'Bonus Coins'),
            'total_coins' => Yii::t('app', 'Total Coins'),
            'bonus_coins_amount' => Yii::t('app', 'Bonus Coins Amount'),
            'order_uuid' => Yii::t('app', 'Order Uuid'),
            'coupon_code' => Yii::t('app', 'Promo Code'),
            'coupon_uuid' => Yii::t('app', 'Coupon Uuid'),
            'created_at' => Yii::t('app', 'Created At'),
            'creative_kids_voucher_code' => Yii::t('app', 'Voucher Code'),
            'kids_name' => Yii::t('app', 'Name'),
            'kids_dob' => Yii::t('app', 'Date of Birth'),
            'kids_postal_code' => Yii::t('app', 'Postal Code'),
            'cc_checkbox' => 'Save for future use',
            'cc_name' => 'Card Holder Name',
            'cc_number' => 'Card Number',
            'cc_cvv' => 'CVV',
            'cc_month_year' => 'Expire Month',
            's_cc_name' => 'Saved Card Holder Name',
            's_cc_number' => 'Saved Card Number',
            's_cc_cvv' => 'Saved Card CVV',
            's_cc_month_year' => 'Saved Card Expire Month',
            'promo_coins' => 'Promocode Coins',
            'cart_total_coins' => 'Total Coins',
        ];
    }
    
    public static function creativeCodeCriteria() {
        if (
                (!empty($this->creative_kids_voucher_code) && ( empty($this->kids_name) || empty($this->kids_dob) || empty($this->kids_postal_code))) || (!empty($this->kids_name) && ( empty($this->creative_kids_voucher_code) || empty($this->kids_dob) || empty($this->kids_postal_code))) || (!empty($this->kids_dob) && ( empty($this->kids_name) || empty($this->creative_kids_voucher_code) || empty($this->kids_postal_code))) || (!empty($this->kids_postal_code) && ( empty($this->kids_name) || empty($this->creative_kids_voucher_code) || empty($this->kids_dob)))
        ) {

            if (empty($this->creative_kids_voucher_code)) {
                $this->addError('creative_kids_voucher_code', 'Voucher Code Required.');
            }

            if (empty($this->kids_name)) {
                $this->addError('kids_name', 'Name Required.');
            }

            if (empty($this->kids_dob)) {
                $this->addError('kids_dob', 'Date of Birth Required.');
            }
            if (empty($this->kids_postal_code)) {
                $this->addError('kids_postal_code', 'Postal Code Required.');
            }
        }
    }
    
    public static function validate_coupon() {
        if (!empty($this->coupon_code)) {
            
            $res = $this->check_coupon_available($this->coupon_code);
            if ($res['is_available']) {
                //$this->coupon_uuid = $res['coupon_uuid'];
            } else{
                //$this->coupon_uuid = '';
                $this->addError('coupon_code', $res['message']);
            }
        }
    }
    
    public static function check_coupon_available($coupon_code = "", $student_uuid = "") {

        $return = ['is_available' => false, 'message' => 'This Promo code not available.', 'promo_coins' => 0, 'coupon_code' => $coupon_code, 'coupon_uuid' => ''];
        $model = \app\models\MusicoinCoupon::find()->where(['code' => $coupon_code, 'status' => 'ENABLED'])->one();

        if (!empty($model)) {
            $today = date('Y-m-d');
            $start = date('Y-m-d', strtotime($model->start_date));
            $end = date('Y-m-d', strtotime($model->end_date));
            
            $promo_coins = $model->coins;

            if ($today >= $start && $today <= $end) {
                $return = ['is_available' => true, 'message' => 'This Coupon apply successfully.', 'promo_coins' => $promo_coins, 'coupon_code' => $coupon_code, 'coupon_uuid' => $model->uuid];
            } else {
                $return['message'] = "This coupon is expired or not available.";
            }
        }
        return $return;
    }
    
    public static function make_cart_submit_array($step_data) {
        
        $stripe_array = [];
        $student_detail = Student::findOne(Yii::$app->user->identity->student_uuid);
        
        if (is_array($step_data) && !empty($step_data)) {
            
            $package = MusicoinPackage::find()->where(['uuid' => $step_data['package_uuid']])->one();
            $coupon_model = \app\models\MusicoinCoupon::find()->where(['code' => $step_data['coupon_code'], 'status' => 'ENABLED'])->one();
            
            $promo_coins = isset($coupon_model->coins)?$coupon_model->coins:0;
            $cart_total_coins = (int) ($promo_coins + $package->total_coins);
            
            $discount = 0;
            $cart_total = ($package->total_price - $discount);
            
            //if saved card
            if(!empty($step_data['saved_cc_ckeck']) && $step_data['saved_cc_ckeck'] != 'other'){
                
                $step_data['cc_name'] = $step_data['s_cc_name'];
                $step_data['cc_number'] = $step_data['s_cc_number'];
                $step_data['cc_cvv'] = $step_data['s_cc_cvv'];
                $step_data['cc_month_year'] = $step_data['s_cc_month_year'];
            }
            
            $stripe_array = [
                'stripeToken' => $step_data['stripeToken'],
                'creative_kids_voucher_code' => $step_data['creative_kids_voucher_code'],
                'kids_name' => $step_data['kids_name'],
                'kids_dob' => $step_data['kids_dob'],
                'kids_postal_code' => $step_data['kids_postal_code'],
                'coupon_code' => isset($coupon_model->code)?$coupon_model->code:null,
                'coupon_uuid' => isset($coupon_model->uuid)?$coupon_model->uuid:null,
                
                'coins' => $package->coins,
                'bonus_coins' => $package->bonus_coins,
                'total_coins' => $package->total_coins,
                'promo_coins' => $promo_coins,
                'cart_total_coins' => $cart_total_coins,
                
                'plan_uuid' => $package->uuid,
                'plan_name' => $package->name,
                'plan_description' => $package->description,
                'plan_bonus_coins_amount' => $package->bonus_coins_amount,
                'price' => $package->price,
                'gst' => $package->gst,
                'total_price' => $package->total_price,
                'discount' => $discount,
                'cart_total' => $cart_total,
                
                'card_name' => $step_data['cc_name'],
                'card_number' => $step_data['cc_number'],
                'card_cvv' => $step_data['cc_cvv'],
                'card_month' => date('m', strtotime($step_data['cc_month_year'])),
                'card_year' => date('Y', strtotime($step_data['cc_month_year'])),
                'cc_checkbox' => (isset($step_data['cc_checkbox'])) ? $step_data['cc_checkbox'] : 0,
                
                
                'student_uuid' => Yii::$app->user->identity->student_uuid,
                'student_ID' => $student_detail->enrollment_id,
                'student_name' => $student_detail->first_name . ' ' . $student_detail->last_name,
                'student_first_name' => $student_detail->first_name,
                'student_last_name' => $student_detail->last_name,
            ];
        }
        return $stripe_array;
    }
    public static function stripePayment($stripe_array) {
        
        $return = ['code' => 421, 'message' => 'Something went wrong.'];
        $student = Student::find()->where(['student_uuid' => $stripe_array['student_uuid']])->one();
        $step_data = StudentTuitionBooking::getSession("buy_coin_".$stripe_array['student_uuid']);
        
        if (!empty($stripe_array['stripeToken'])) {
            
            $dollar_amount = (float) $stripe_array['cart_total'];
            $cents_amount = $dollar_amount * 100;   // 1 AUD = 100 cents
            
            //include Stripe PHP library
            require Yii::$app->basePath . '/vendor/stripe/stripe-php/init.php';
            
            //set api key
            $stripe = array(
                "secret_key" => Yii::$app->params['stripe']['secret_key'],
                "publishable_key" => Yii::$app->params['stripe']['publishable_key']
            );
            try {
                \Stripe\Stripe::setApiKey($stripe['secret_key']);
                $customer = \Stripe\Customer::create(array(
                            'email' => $student->email,
                            'source' => $stripe_array['stripeToken']
                ));

                //item information
                $itemName = $stripe_array['plan_name'];
                $orderID = General::generateTransactionID();
		$itemDescription = $stripe_array['student_ID'] . ' - ' . $orderID.' - Musicoins Package Type Purchased';
		$item_title = $stripe_array['student_ID'] . ' - ' . $orderID.' - Musicoins '.$itemName.': ';
	        if(!empty($stripe_array['coins'])){
	            $item_title .= $stripe_array['coins']. ' Purchased Coins';
                    if($stripe_array['bonus_coins'] > 0){
                        $item_title .= ' plus '.$stripe_array['bonus_coins']. ' Bonus Coins';
                    }
                }
		$itemDescription = $item_title;
                $itemPrice = $cents_amount;
                $currency = "aud";
                
                //charge a credit or a debit card
                $charge = \Stripe\Charge::create(array(
                            'customer' => $customer->id,
                            'amount' => $itemPrice,
                            'currency' => $currency,
                            'description' => $itemDescription,
                            'metadata' => array(
                                'order_id' => $orderID
                            )
                ));
            } catch (\Stripe\Error\ApiConnection $e) {
                return $return = ['code' => 421, 'message' => 'Network problem, perhaps try again.'];
            } catch (\Stripe\Error\InvalidRequest $e) {
                return $return = ['code' => 421, 'message' => 'Your request is invalid.'];
            } catch (\Stripe\Error\Api $e) {
                return $return = ['code' => 421, 'message' => 'Stripe servers are down!'];
            } catch (\Stripe\Error\Card $e) {
                return $return = ['code' => 421, 'message' => 'This Card is Declined.'];
            }

            $chargeJson = $charge->jsonSerialize();
            
            if ($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1) {
                
                $student_musicoin_package_uuid = NULL;
                //Transaction_payment
                $modelPayment = new PaymentTransaction();
                $modelPayment->student_uuid = $stripe_array['student_uuid'];
                $modelPayment->transaction_number = $orderID;
                $modelPayment->payment_datetime = date("Y-m-d H:i:s");
                $modelPayment->amount = $chargeJson['amount'] / 100;
                $modelPayment->payment_method = 'Stripe';
                $modelPayment->reference_number = $chargeJson['balance_transaction'];
                $modelPayment->status = $chargeJson['status'];
                $modelPayment->student_musicoin_package_uuid = $student_musicoin_package_uuid;
                $modelPayment->response = json_encode($chargeJson);
                $modelPayment->save(false);
                
                //student_musicoin_package table entry
                $modelSMP = new StudentMusicoinPackage();
                $modelSMP->student_uuid = $stripe_array['student_uuid'];
                $modelSMP->name = $stripe_array['plan_name'];
                $modelSMP->description = $stripe_array['plan_description'];
                $modelSMP->price = $stripe_array['price'];
                $modelSMP->gst = $stripe_array['gst'];
                $modelSMP->total_price = $stripe_array['total_price'];
                $modelSMP->discount = $stripe_array['discount'];
                $modelSMP->grand_total = $chargeJson['amount'] / 100;
                $modelSMP->bonus_coins_amount =$stripe_array['plan_bonus_coins_amount'];
                
                $modelSMP->coins = $stripe_array['coins'];
                $modelSMP->bonus_coins = $stripe_array['bonus_coins'];
                $modelSMP->total_coins = $stripe_array['total_coins'];
                $modelSMP->promo_coins = $stripe_array['promo_coins'];
                $modelSMP->grand_total_coins = $stripe_array['cart_total_coins'];
                $modelSMP->order_uuid = NULL;
                $modelSMP->coupon_code = $stripe_array['coupon_code'];
                $modelSMP->coupon_uuid = $stripe_array['coupon_uuid'];
                $modelSMP->created_at = date('Y-m-d H:i:s');
                
                $modelSMP->creative_kids_voucher_code = $stripe_array['creative_kids_voucher_code'];
                $modelSMP->kids_name = $stripe_array['kids_name'];
                $modelSMP->kids_dob = $stripe_array['kids_dob'];
                $modelSMP->kids_postal_code = $stripe_array['kids_postal_code'];
                if($modelSMP->save(false)){
                    
                    //update Transaction Booking id
                    $modelPaymentUpdate = PaymentTransaction::find()->where(['uuid' => $modelPayment->getPrimaryKey()])->one();
                    $modelPaymentUpdate->student_musicoin_package_uuid = $modelSMP->getPrimaryKey();
                    $modelPaymentUpdate->save(false);
                    
                    //cc save or not
                    if ($stripe_array['cc_checkbox'] == 1) {
                        $modelCC = StudentCreditCard::find()->where(['student_uuid' => $stripe_array['student_uuid'], 'status' => 'ENABLED'])->one();
                        if (empty($modelCC)) {
                            $modelCC = new StudentCreditCard();
                        }
                        $modelCC->student_uuid = $stripe_array['student_uuid'];
                        $modelCC->number = General::encrypt_str($stripe_array['card_number']);
                        $modelCC->cvv = General::encrypt_str($stripe_array['card_cvv']);
                        $modelCC->expire_date = date('Y-m-t', strtotime($stripe_array['card_year'] . "-" . $stripe_array['card_month'] . "-01"));
                        $modelCC->name = $stripe_array['card_name'];
                        $modelCC->status = 'ENABLED';
                        $modelCC->stripe_customer_id = $chargeJson['customer'];
                        $modelCC->save(false);
                    }
                    
                    //create order
                    $item_array[0] = [
                        'item' => $stripe_array['plan_name'],
                        'booking_uuid' => NULL,
                        'student_monthly_subscription_uuid' => NULL,
                        'student_musicoin_package_uuid' => $modelSMP->getPrimaryKey(),
                        'tax' => $modelSMP->gst,
                        'price' => $modelSMP->price,
                        'total' => $modelSMP->total_price
                    ];
                    
                    $order_array['student_uuid'] = $stripe_array['student_uuid'];
                    $order_array['payment_uuid'] = $modelPayment->getPrimaryKey();
                    $order_array['discount'] = $stripe_array['discount'];
                    $order_array['item'] = $item_array;
                    $order_array['total'] = $stripe_array['total_price'];
                    $order_array['grand_total'] = $modelSMP->grand_total;
                    
                    $order_result = self::generate_order($order_array);
                    
                    $order_id = (!empty($order_result['order_id'])) ? $order_result['order_id'] : "";
                    
                    //Musicoins credit into student account
                    $credit_coin_array = [
                        'student_uuid' => $stripe_array['student_uuid'],
                        'coins' => $stripe_array['total_coins'],
                        'summary' => "Buy plan: ".$stripe_array['plan_name'].", Order: #".$order_id,
                    ];
                    MusicoinLog::creditMusicoin($credit_coin_array);
                    
                    //Musicoin_coupon_log
                    if (!empty($stripe_array['coupon_uuid'])) {
                        
                        $coupon_log = new MusicoinCouponLog();
                        $coupon_log->student_uuid = $stripe_array['student_uuid'];
                        $coupon_log->student_musicoin_package_uuid = $modelSMP->getPrimaryKey();
                        $coupon_log->transaction_uuid = $modelPayment->getPrimaryKey();
                        $coupon_log->order_uuid = (!empty($order_result['order_uuid'])) ? $order_result['order_uuid'] : NULL;
                        $coupon_log->created_at = date('Y-m-d H:i:s');
                        $coupon_log->save(false);
                        
                        //Musicoins credit into student account on coupon code.
                        $coupon_credit_coin_array = [
                            'student_uuid' => $stripe_array['student_uuid'],
                            'coins' => $stripe_array['promo_coins'],
                            'summary' => "Added promocode coins on Order #".$order_id,
                        ];
                        MusicoinLog::creditMusicoin($coupon_credit_coin_array);
                    }
                    
                    //Send mail
                    $mail_data = [
                        'student_uuid' => $stripe_array['student_uuid'],
                        'student_name' => $stripe_array['student_name'],
                        'student_first_name' => $stripe_array['student_first_name'],
                        'student_last_name' => $stripe_array['student_last_name'],
                        'plan_name' => $stripe_array['plan_name'],
                        'coins' => $stripe_array['coins'],
                        'bonus_coins' => $stripe_array['bonus_coins'],
                        'total_coins' => $stripe_array['total_coins'],
                        'promo_coins' => $stripe_array['promo_coins'],
                        'grand_total_coins' => $stripe_array['cart_total_coins'],
                        'price' => $stripe_array['price'],
                        'gst' => $stripe_array['gst'],
                        'total_price' => $stripe_array['total_price'],
                        'coupon_code' => $stripe_array['coupon_code'],
                        'order_id' => $order_id,
                        'order_file' => (isset($order_result['order_file'])) ? $order_result['order_file'] : "",
                        'transaction_id' => $modelPayment->transaction_number,
                        'creative_kids_voucher_code' => $stripe_array['creative_kids_voucher_code'],
                    ];
                    self::buyMusicoinMail($mail_data);
                }
                
                $latest_student = Student::find()->where(['student_uuid' => $stripe_array['student_uuid']])->one();
                return $return = ['code' => 200, 'message' => 'success', 'musicoin_balance' => $latest_student->musicoin_balance];
                
            } else {
                return $return = ['code' => 421, 'message' => 'Transaction has been failed.'];
            }
            
        } else {
            return $return = ['code' => 421, 'message' => 'Form submission error.'];
        }

        return $return;
    }
    
    public static function generate_order($order_array) {

        if (!empty($order_array)) {

            $modelOrder = new Order();
            $modelOrder->order_id = General::generateOrderID();
            $modelOrder->student_uuid = $order_array['student_uuid'];
            $modelOrder->payment_uuid = $order_array['payment_uuid'];
            $modelOrder->order_datetime = date('Y-m-d H:i:s');
            $modelOrder->total = $order_array['total'];
            $modelOrder->discount = $order_array['discount'];
            $modelOrder->grand_total = $order_array['grand_total'];
            if ($modelOrder->save(false)) {

                if (!empty($order_array['item'])) {
                    foreach ($order_array['item'] as $key => $item) {

                        $modelOrderItem = new OrderItem();
                        $modelOrderItem->order_uuid = $modelOrder->getPrimaryKey();
                        $modelOrderItem->item = $item['item'];
                        $modelOrderItem->booking_uuid = $item['booking_uuid'];
                        $modelOrderItem->student_monthly_subscription_uuid = $item['student_monthly_subscription_uuid'];
                        $modelOrderItem->student_musicoin_package_uuid = (!empty($item['student_musicoin_package_uuid']))?$item['student_musicoin_package_uuid']:NULL;
                        $modelOrderItem->price = $item['price'];
                        $modelOrderItem->tax = $item['tax'];
                        $modelOrderItem->total = $item['total'];
                        $modelOrderItem->save(false);

                        if (!empty($item['booking_uuid'])) {
                            $modelSTBooking = StudentTuitionBooking::find()->where(['uuid' => $item['booking_uuid']])->one();
                            if (!empty($modelSTBooking)) {
                                $modelSTBooking->order_uuid = $modelOrder->getPrimaryKey();
                                $modelSTBooking->save(false);
                            }
                        }

                        if (!empty($item['student_monthly_subscription_uuid'])) {
                            $modelStudentMonthlySub = StudentMonthlySubscription::find()->where(['uuid' => $item['student_monthly_subscription_uuid']])->one();
                            if (!empty($modelStudentMonthlySub)) {
                                $modelStudentMonthlySub->order_uuid = $modelOrder->getPrimaryKey();
                                $modelStudentMonthlySub->save(false);
                            }
                        }
                        if (!empty($item['student_musicoin_package_uuid'])) {
                            $modelStudentMusicoinPackage = StudentMusicoinPackage::find()->where(['uuid' => $item['student_musicoin_package_uuid']])->one();
                            if (!empty($modelStudentMusicoinPackage)) {
                                $modelStudentMusicoinPackage->order_uuid = $modelOrder->getPrimaryKey();
                                $modelStudentMusicoinPackage->save(false);
                            }
                        }
                    }
                }
            }
            $transaction_model = \app\models\PaymentTransaction::find()->where(['uuid' => $modelOrder->payment_uuid])->asArray()->one();
            if (!empty($transaction_model['transaction_number'])) {
                $filename = 'Invoice_' . $transaction_model['transaction_number'] . '.pdf';
            } else {
                $filename = 'Invoice_' . $modelOrder->order_id . '.pdf';
            }
            
            $pdf_res = self::create_pdf($filename, $modelOrder->getPrimaryKey());

            $modelStudent = Student::find()->where(['student_uuid' => $order_array['student_uuid']])->one();

            $content['student_name'] = $modelStudent->first_name;
            $content['order_uuid'] = $modelOrder->getPrimaryKey();
            $content['order_id'] = $modelOrder->order_id;
            $content['order_array'] = $order_array;
            $content['order_date'] = General::displayDate($modelOrder->order_datetime);
            $content['order_file'] = $pdf_res['file'];

            return $content;
        }
        return [];
    }

    /**
     * This function use generate PDF file for Invoice.
     *
     */
    public static function create_pdf($filename = '', $order_uuid) {

        $file = Yii::$app->params['media']['invoice']['path'] . $filename;

        $model = \app\models\Order::findOne($order_uuid);

        if (!empty($model)) {

            $order_item = \app\models\OrderItem::find()->where(['order_uuid' => $order_uuid])->asArray()->all();
            $transaction_model = \app\models\PaymentTransaction::find()->where(['uuid' => $model->payment_uuid])->asArray()->one();

            $content = Yii::$app->controller->renderPartial('//order/buy_musicoin_invoice_pdf', ['model' => $model, 'order_item' => $order_item,'transaction_model' => $transaction_model]);

            if ($filename == '') {
                if (!empty($transaction_model['transaction_number'])) {
                    $filename = 'Invoice_' . $transaction_model['transaction_number'] . '.pdf';
                } else {
                    $filename = 'Invoice_' . $model->order_id . '.pdf';
                }
            }
            $file = Yii::$app->params['media']['invoice']['path'] . $filename;

            if (!is_dir(Yii::$app->params['media']['invoice']['path'])) {
                FileHelper::createDirectory(Yii::$app->params['media']['invoice']['path']);
            }

            $css_content = file_get_contents(Yii::$app->params['theme_assets']['url'] . 'plugins/bootstrap/css/bootstrap.min.css');
            $css_content .= file_get_contents(Yii::$app->params['theme_assets']['url'] . 'css/new_pdf.css');
            $css_content .= 'body {background-image: url(\'' . Yii::$app->params['theme_assets']['url'] . 'images/pdf_bg.png' . '\'); background-position: center bottom;background-repeat: no-repeat;}';
            
            $footer_html = '<table class="footer_tbl"><tr><td>ADMIN@MUSICONN.COM.AU</td><td>1300 068 742</td><td>MUSICONN.COM.AU</td></tr></table>';

            $params = [
                'mode' => Pdf::MODE_CORE,
                'format' => Pdf::FORMAT_A4,
                'orientation' => Pdf::ORIENT_PORTRAIT,
                'destination' => Pdf::DEST_FILE,
                'content' => $content,
                'filename' => Yii::$app->params['media']['invoice']['path'] . $filename,
                'tempPath' => 'mpdf/',
                'cssInline' => '.kv-heading-1{font-size:12px;border:none;}',
                'cssInline' => $css_content,
                'options' => ['title' => 'Invoice'],
                'methods' => [
                    'SetHeader' => [''],
                    'SetFooter' => [$footer_html],
                ]
            ];
            $pdf = new Pdf($params);
            $pdf->render();

            if (is_file($file)) {

                $model->invoice_file = $filename;
                $model->save(false);

                return ['status' => '1', 'file' => $file, 'filename' => $filename];
            } else {
                return ['status' => '0', 'file' => $file, 'filename' => $filename];
            }
        } else {
            return ['status' => '0', 'file' => $file, 'filename' => $filename];
        }
    }
    
    public static function buyMusicoinMail($data = []) {
        
        $modelStudent = Student::find()->where(['student_uuid' => $data['student_uuid']])->one();
        if(!empty($modelStudent)){
            $content = $data;
            $isInvoiceFile = (isset($data['order_file'])) ? $data['order_file'] : "";
            $owner = General::getOwnersEmails(['OWNER', 'ADMIN', 'SUBADMIN']);
            $mail = Yii::$app->mailer->compose('buy_musicoins_student_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                        ->setFrom(Yii::$app->params['supportEmail'])
                        ->setTo($modelStudent->email)
                        ->setBcc($owner)
                        ->setSubject(ucfirst(strtolower(Yii::$app->name)) . ' Notification: Confirmation of Musicoins Purchase');

            if (is_file($isInvoiceFile)) {
                $mail->attach($isInvoiceFile);
            }
            $mail->send();
        }
        return true;
    }
    
    /*public static function make_cart_submit_array_old() {
        
        $step_data = StudentTuitionBooking::getSession("buy_coin_".Yii::$app->user->identity->student_uuid);
        
        $stripe_array = [];
        $student_detail = Student::findOne(Yii::$app->user->identity->student_uuid);
        
        if (isset($step_data['step_1']) && isset($step_data['step_2'])) {
            
            $package = MusicoinPackage::find()->where(['uuid' => $step_data['step_1']['package_uuid']])->one();
            $coupon_model = \app\models\MusicoinCoupon::find()->where(['code' => $step_data['step_2']['coupon_code'], 'status' => 'ENABLED'])->one();
            
            $promo_coins = $coupon_model->coins;
            $cart_total_coins = (int) ($promo_coins + $package->total_coins);
            
            $discount = 0;
            $cart_total = ($package->total_price - $discount);
            
            $stripe_array = [
                'stripeToken' => $step_data['step_2']['stripeToken'],
                'creative_kids_voucher_code' => $step_data['step_2']['creative_kids_voucher_code'],
                'kids_name' => $step_data['step_2']['kids_name'],
                'kids_dob' => $step_data['step_2']['kids_dob'],
                'kids_postal_code' => $step_data['step_2']['kids_postal_code'],
                'coupon_code' => $coupon_model->code,
                'coupon_uuid' => $coupon_model->uuid,
                
                'coins' => $package->coins,
                'bonus_coins' => $package->bonus_coins,
                'total_coins' => $package->total_coins,
                'promo_coins' => $promo_coins,
                'cart_total_coins' => $cart_total_coins,
                
                'plan_uuid' => $package->uuid,
                'plan_name' => $package->name,
                'plan_description' => $package->description,
                'plan_bonus_coins_amount' => $package->bonus_coins_amount,
                'price' => $package->price,
                'gst' => $package->gst,
                'total_price' => $package->total_price,
                'discount' => $discount,
                'cart_total' => $cart_total,
                
                'card_name' => $step_data['step_2']['cc_name'],
                'card_number' => $step_data['step_2']['cc_number'],
                'card_cvv' => $step_data['step_2']['cc_cvv'],
                'card_month' => date('m', strtotime($step_data['step_2']['cc_month_year'])),
                'card_year' => date('Y', strtotime($step_data['step_2']['cc_month_year'])),
                'cc_checkbox' => (isset($step_data['step_2']['cc_checkbox'])) ? $step_data['step_2']['cc_checkbox'] : 0,
                
                
                'student_uuid' => Yii::$app->user->identity->student_uuid,
                'student_ID' => $student_detail->enrollment_id,
                'student_name' => $student_detail->first_name . ' ' . $student_detail->last_name,
            ];
        }
        return $stripe_array;
    }
    
    public static function stripePayment_old($stripe_array) {
        
        $return = ['code' => 421, 'message' => 'Something went wrong.'];
        $student = Student::find()->where(['student_uuid' => $stripe_array['student_uuid']])->one();
        $step_data = StudentTuitionBooking::getSession("buy_coin_".$stripe_array['student_uuid']);
        
        if (!empty($stripe_array['stripeToken'])) {
            
            $dollar_amount = (float) $stripe_array['cart_total'];
            $cents_amount = $dollar_amount * 100;   // 1 AUD = 100 cents
            
            //include Stripe PHP library
            require Yii::$app->basePath . '/vendor/stripe/stripe-php/init.php';
            
            //set api key
            $stripe = array(
                "secret_key" => Yii::$app->params['stripe']['secret_key'],
                "publishable_key" => Yii::$app->params['stripe']['publishable_key']
            );
            try {
                \Stripe\Stripe::setApiKey($stripe['secret_key']);
                $customer = \Stripe\Customer::create(array(
                            'email' => $student->email,
                            'source' => $stripe_array['stripeToken']
                ));

                //item information
                $itemName = $stripe_array['plan_name'];
                $orderID = General::generateTransactionID();
                $itemDescription = $stripe_array['student_ID'] . ' - ' . $orderID.' - Musicoins Package Type Purchased';
                $itemPrice = $cents_amount;
                $currency = "aud";
                
                //charge a credit or a debit card
                $charge = \Stripe\Charge::create(array(
                            'customer' => $customer->id,
                            'amount' => $itemPrice,
                            'currency' => $currency,
                            'description' => $itemDescription,
                            'metadata' => array(
                                'order_id' => $orderID
                            )
                ));
            } catch (\Stripe\Error\ApiConnection $e) {
                return $return = ['code' => 421, 'message' => 'Network problem, perhaps try again.'];
            } catch (\Stripe\Error\InvalidRequest $e) {
                return $return = ['code' => 421, 'message' => 'Your request is invalid.'];
            } catch (\Stripe\Error\Api $e) {
                return $return = ['code' => 421, 'message' => 'Stripe servers are down!'];
            } catch (\Stripe\Error\Card $e) {
                return $return = ['code' => 421, 'message' => 'This Card is Declined.'];
            }

            $chargeJson = $charge->jsonSerialize();
            
            if ($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1) {
                
                $student_musicoin_package_uuid = NULL;
                //Transaction_payment
                $modelPayment = new PaymentTransaction();
                $modelPayment->student_uuid = $stripe_array['student_uuid'];
                $modelPayment->transaction_number = $orderID;
                $modelPayment->payment_datetime = date("Y-m-d H:i:s");
                $modelPayment->amount = $chargeJson['amount'] / 100;
                $modelPayment->payment_method = 'Stripe';
                $modelPayment->reference_number = $chargeJson['balance_transaction'];
                $modelPayment->status = $chargeJson['status'];
                $modelPayment->student_musicoin_package_uuid = $student_musicoin_package_uuid;
                $modelPayment->response = json_encode($chargeJson);
                $modelPayment->save(false);
                
                //student_musicoin_package table entry
                $modelSMP = new StudentMusicoinPackage();
                $modelSMP->student_uuid = $stripe_array['student_uuid'];
                $modelSMP->name = $stripe_array['plan_name'];
                $modelSMP->description = $stripe_array['plan_description'];
                $modelSMP->price = $stripe_array['price'];
                $modelSMP->gst = $stripe_array['gst'];
                $modelSMP->total_price = $stripe_array['total_price'];
                $modelSMP->discount = $stripe_array['discount'];
                $modelSMP->grand_total = $chargeJson['amount'] / 100;
                $modelSMP->bonus_coins_amount =$stripe_array['plan_bonus_coins_amount'];
                
                $modelSMP->coins = $stripe_array['coins'];
                $modelSMP->bonus_coins = $stripe_array['bonus_coins'];
                $modelSMP->total_coins = $stripe_array['total_coins'];
                $modelSMP->promo_coins = $stripe_array['promo_coins'];
                $modelSMP->grand_total_coins = $stripe_array['cart_total_coins'];
                $modelSMP->order_uuid = NULL;
                $modelSMP->coupon_code = $stripe_array['coupon_code'];
                $modelSMP->coupon_uuid = $stripe_array['coupon_uuid'];
                $modelSMP->created_at = date('Y-m-d H:i:s');
                
                $modelSMP->creative_kids_voucher_code = $stripe_array['creative_kids_voucher_code'];
                $modelSMP->kids_name = $stripe_array['kids_name'];
                $modelSMP->kids_dob = $stripe_array['kids_dob'];
                $modelSMP->kids_postal_code = $stripe_array['kids_postal_code'];
                if($modelSMP->save(false)){
                    
                    //update Transaction Booking id
                    $modelPaymentUpdate = PaymentTransaction::find()->where(['uuid' => $modelPayment->getPrimaryKey()])->one();
                    $modelPaymentUpdate->student_musicoin_package_uuid = $modelSMP->getPrimaryKey();
                    $modelPaymentUpdate->save(false);
                    
                    //cc save or not
                    if ($stripe_array['cc_checkbox'] == 1) {
                        $modelCC = StudentCreditCard::find()->where(['student_uuid' => $stripe_array['student_uuid'], 'status' => 'ENABLED'])->one();
                        if (empty($modelCC)) {
                            $modelCC = new StudentCreditCard();
                        }
                        $modelCC->student_uuid = $stripe_array['student_uuid'];
                        $modelCC->number = General::encrypt_str($stripe_array['card_number']);
                        $modelCC->cvv = General::encrypt_str($stripe_array['card_cvv']);
                        $modelCC->expire_date = date('Y-m-t', strtotime($stripe_array['card_year'] . "-" . $stripe_array['card_month'] . "-01"));
                        $modelCC->name = $stripe_array['card_name'];
                        $modelCC->status = 'ENABLED';
                        $modelCC->stripe_customer_id = $chargeJson['customer'];
                        $modelCC->save(false);
                    }
                    
                    //create order
                    $item_array[0] = [
                        'item' => $stripe_array['plan_name'],
                        'booking_uuid' => NULL,
                        'student_monthly_subscription_uuid' => NULL,
                        'student_musicoin_package_uuid' => $modelSMP->getPrimaryKey(),
                        'tax' => $modelSMP->gst,
                        'price' => $modelSMP->price,
                        'total' => $modelSMP->total_price
                    ];
                    
                    $order_array['student_uuid'] = $stripe_array['student_uuid'];
                    $order_array['payment_uuid'] = $modelPayment->getPrimaryKey();
                    $order_array['discount'] = $stripe_array['discount'];
                    $order_array['item'] = $item_array;
                    $order_array['total'] = $stripe_array['total_price'];
                    $order_array['grand_total'] = $modelSMP->grand_total;
                    
                    $order_result = StudentTuitionBooking::generate_order($order_array);
                    
                    $order_id = (!empty($order_result['order_id'])) ? $order_result['order_id'] : "";
                    
                    //Musicoins credit into student account
                    $credit_coin_array = [
                        'student_uuid' => $stripe_array['student_uuid'],
                        'coins' => $stripe_array['total_coins'],
                        'summary' => "Buy plan: ".$stripe_array['plan_name'].", Order: #".$order_id,
                    ];
                    MusicoinLog::creditMusicoin($credit_coin_array);
                    
                    //Musicoin_coupon_log
                    if (!empty($stripe_array['coupon_uuid'])) {
                        
                        $coupon_log = new MusicoinCouponLog();
                        $coupon_log->student_uuid = $stripe_array['student_uuid'];
                        $coupon_log->student_musicoin_package_uuid = $modelSMP->getPrimaryKey();
                        $coupon_log->transaction_uuid = $modelPayment->getPrimaryKey();
                        $coupon_log->order_uuid = (!empty($order_result['order_uuid'])) ? $order_result['order_uuid'] : NULL;
                        $coupon_log->created_at = date('Y-m-d H:i:s');
                        $coupon_log->save(false);
                        
                        //Musicoins credit into student account on coupon code.
                        $coupon_credit_coin_array = [
                            'student_uuid' => $stripe_array['student_uuid'],
                            'coins' => $stripe_array['promo_coins'],
                            'summary' => "Added promocode coins on Order #".$order_id,
                        ];
                        MusicoinLog::creditMusicoin($coupon_credit_coin_array);
                    }
                    
                    //Send mail
                    $mail_data = [
                        'student_uuid' => $stripe_array['student_uuid'],
                        'student_name' => $stripe_array['student_name'],
                        'plan_name' => $stripe_array['plan_name'],
                        'coins' => $stripe_array['coins'],
                        'bonus_coins' => $stripe_array['bonus_coins'],
                        'total_coins' => $stripe_array['total_coins'],
                        'promo_coins' => $stripe_array['promo_coins'],
                        'grand_total_coins' => $stripe_array['cart_total_coins'],
                        'price' => $stripe_array['price'],
                        'gst' => $stripe_array['gst'],
                        'total_price' => $stripe_array['total_price'],
                        'coupon_code' => $stripe_array['coupon_code'],
                        'order_id' => $order_id,
                        'order_file' => (isset($order_result['order_file'])) ? $order_result['order_file'] : "",
                    ];
                    self::buyMusicoinMail($mail_data);
                }
                
                $latest_student = Student::find()->where(['student_uuid' => $stripe_array['student_uuid']])->one();
                return $return = ['code' => 200, 'message' => 'success', 'musicoin_balance' => $latest_student->musicoin_balance];
                
            } else {
                return $return = ['code' => 421, 'message' => 'Transaction has been failed.'];
            }
            
        } else {
            return $return = ['code' => 421, 'message' => 'Form submission error.'];
        }

        return $return;
    } */

}
