<?php
/* @var $this \yii\web\View */
/* @var $content string */
$this->registerJsFile(Yii::$app->request->baseUrl.'/theme_assets/js/jquery-3.2.1.min.js');
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\SignupAsset;

SignupAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
	<head>
		<meta charset="<?= Yii::$app->charset ?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<?= Html::csrfMetaTags() ?>
		<title><?= Html::encode($this->title) ?></title>
		<script src="https://browser.sentry-cdn.com/5.19.0/bundle.min.js" integrity="sha384-edPCPWtQrj57nipnV3wt78Frrb12XdZsyMbmpIKZ9zZRi4uAxNWiC6S8xtGCqwDG" crossorigin="anonymous">
</script>
<script>
Sentry.init({ dsn: 'https://817fc244e238470f990f3e2af587bc03@o399941.ingest.sentry.io/5257765' });
</script>
		<link rel="shortcut icon" href="/theme_assets/images/favicon.ico" type="image/x-icon" /><!-- Favicon -->
		<link rel="apple-touch-icon-precomposed" href="/theme_assets/images/apple-touch-icon-57-precomposed.png"><!-- For iPhone -->
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/theme_assets/images/apple-touch-icon-114-precomposed.png">   <!-- For iPhone 4 Retina display -->
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/theme_assets/images/apple-touch-icon-72-precomposed.png"><!-- For iPad -->
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/theme_assets/images/apple-touch-icon-144-precomposed.png"><!-- For iPad Retina display -->
		<?php $this->head() ?>
		<style>
			.has-success .control-label{
				color : rgb(31, 181, 172) !important;
			}
			
			.has-success .form-control{
				border-color : rgb(31, 181, 172) !important;
			}
			a i.test{
				color: rgb(170, 93, 228) !important;
    				text-shadow: 0 0 1px #000;
			}
			a:hover i.test
			{
				color: rgb(152, 81, 206) !important;
			}
		</style>
	</head>
	<body class="login_page">
		<?php $this->beginBody() ?>
		<?= $content ?>
		<p class="text-center text-orange "><?= Yii::$app->params["copyright"]; ?></p>
		<?php $this->endBody(); 

		?>
		
		<script src="/theme_assets/plugins/messenger/js/messenger.min.js" type="text/javascript"></script>
		<script src="/theme_assets/plugins/messenger/js/messenger-theme-future.js" type="text/javascript"></script>
		<script src="/theme_assets/plugins/messenger/js/messenger-theme-flat.js" type="text/javascript"></script>
		<script src="/theme_assets/js/messenger.js" type="text/javascript"></script>

		<script>
		    <?php
		    foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
		        if ($key == 'success') {
		            ?>
		                    showSuccess("<?= $message; ?>");

		        <?php } else { ?>
		                    showErrorMessage("<?= $message; ?>");
		        <?php } ?>

		    <?php } ?>
		</script>
		<script>
			$(".birthdate").datepicker({
				minViewMode:  0,
				format: 'yyyy-mm-dd',
				/* startDate: getValue($this, 'startDate', ''),
				endDate: getValue($this, 'endDate', ''),*/
				/* daysOfWeekDisabled: getValue($this, 'disabledDays', ''),*/
				startView: 2,
				autoclose:true
			});
		</script>
	</body>
</html>
<?php $this->endPage() ?>
