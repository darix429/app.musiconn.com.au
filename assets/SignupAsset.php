<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class SignupAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    	'theme_assets/plugins/jquery-ui/smoothness/jquery-ui.min.css',  
        'theme_assets/plugins/pace/pace-theme-flash.css',        
        'theme_assets/plugins/bootstrap/css/bootstrap.min.css',        
        'theme_assets/fonts/font-awesome/css/font-awesome.css',
        'theme_assets/css/animate.min.css',
        'theme_assets/plugins/perfect-scrollbar/perfect-scrollbar.css',
        'theme_assets/plugins/icheck/skins/all.css',
        'theme_assets/css/style.css',
        'theme_assets/css/responsive.css',
        'theme_assets/css/custom.css',
        'theme_assets/plugins/datepicker/css/datepicker.css',
    ];
    public $js = [
       // 'theme_assets/js/jquery-3.2.1.min.js',
        'theme_assets/js/popper.min.js',
        'theme_assets/js/jquery.easing.min.js',
        'theme_assets/plugins/bootstrap/js/bootstrap.min.js',
        'theme_assets/plugins/pace/pace.min.js',
        'theme_assets/plugins/viewport/viewportchecker.js',
        'theme_assets/plugins/autosize/autosize.min.js',
        'theme_assets/plugins/sparkline-chart/jquery.sparkline.min.js',
        'theme_assets/js/chart-sparkline.js',
        'theme_assets/plugins/jquery-ui/smoothness/jquery-ui.min.js',
        'theme_assets/plugins/datepicker/js/datepicker.js',       
    	//'theme_assets/js/scripts.js',
    ];
    public $depends = [
        
    ];
}
