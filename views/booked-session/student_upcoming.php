<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;
use app\models\Tutor;
use app\models\BookedSession;
use app\libraries\General;
use app\models\CreditSession;

$this->title = Yii::t('app', 'UPCOMING LESSONS');
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_icon'] = 'my-lesson-tag.png';

$this->registerJs(
        '
$(".my-datatable-student-upcoming").dataTable({
                responsive: true,
               "pageLength": dt_page_length,
                "dom": "<\"dataTables_wrapper\" <\"row\" <\"col-sm-12 col-md-4\"l><\"col-sm-12 col-md-4\"B><\"col-sm-12 col-md-4\"f>>  <\"row\" <\"col-sm-12\"t>> <\"row\" <\"col-sm-12 col-md-5\"i> <\"col-sm-12 col-md-7\"p>> >",
                buttons: [
                    "copy", {
			   extend: "csv",
			   footer: false,
			   exportOptions: {
                		columns: [0,1,2]
            		   }			  
		       }, 
		       {
			   extend: "excel",
			   footer: false,
			   exportOptions: {
                		columns: [0,1,2]
            		   }			  
		       },
			{
			   extend: "pdf",
			   footer: false,
			   exportOptions: {
                		columns: [0,1,2]
            		   }			  
		       },
			{
			   extend: "print",
			   footer: false,
			   exportOptions: {
                		columns: [0,1,2]
            		   }			  
		       }
                ],
                aLengthMenu: [
                    [10, 25, 50, 100, -1],
                    [10, 25, 50, 100, "All"]
                ]
            });

setInterval(function() {
	array = [];
	value = [];
	var d = new Date();
	var current_timestamp = Math.floor(d / 1000);
	d.setMinutes(d.getMinutes() + 5);
	var after_timestamp = Math.floor(d / 1000);

	$("tr.tution_tr").each(function(item){
		var uuid = array["uuid"] = $(this).attr("data-uuid");
		array["start_time"] = $(this).attr("data-stime");
		array["end_time"] = $(this).attr("data-etime");
		value.push(array);
		if(after_timestamp >= array["start_time"] && current_timestamp <= array["end_time"]) {
			$("#reschedulebutton_"+uuid).css({display:"none"});
			$("#cancelbutton_"+uuid).css({display:"none"});
		} else {
			$("#reschedulebutton_"+uuid).css({display:"inline"});
			$("#cancelbutton_"+uuid).css({display:"inline"});
			if(current_timestamp >= array["end_time"]) {
				var table = $(".my-datatable-student-upcoming").DataTable();
				table.row( $(".tr_"+uuid) ).remove().draw(true);
			}
		}
		
	});
}, 500 * 60 * 1);

function test(){
    $.ajax({
        type: "GET",
        url: "' . Url::to(['booked-session/active-start-button-student']) . '",
        success:function(data){
            $.each(data.start.id, function(k, v) {
                var b_id = "#startbutton_"+v;
                $(b_id).removeClass("btn-default");
                $(b_id).addClass("btn-primary");
                $(b_id).text(data.start.text[k]);
                $(b_id).prop("disabled", false);
            });

        },error:function(e){
            showErrorMessage(e.responseText);
        }
    });
}

$(document).on("click",".session_cancel_button",function(){
    if(confirm("Are you sure you want to cancel this lesson?")){
        window.location.href= $(this).data("url");
        blockBody();
    } else {
        unblockBody();
        return false;
    }
});

function cancel_lesson_request_cancel(id){
    if(confirm("Are you sure you want to cancel this lesson cancelled request?")){
        $.ajax({
            type: "POST",
            url: "' . Url::to(['booked-session/student-cancel-lesson-request-cancel']) . '?id="+id,
            data: {"is_cancel": "yes"},
            beforeSend: function() {
                blockBody();
            },
            success:function(result){
                
                if(result.code == 200){
                    window.location.href= "' . Url::to(['/booked-session/upcoming/']) . '";
                    showSuccess(result.message);
                }
                unblockBody();
            },
            error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
        blockBody();
    } else {
        unblockBody();
        return false;
    }

}

', View::POS_END
);
?>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <?php /*<div class="row center-block box-search-area">
                        <div class="col-lg-12 col-md-12 col-12 ">
                            <div class="datatable-search-div " style="display: block; margin-bottom: 10px;">
                                <?php
                                $s_c_count = \app\models\CreditSession::getStudentCreditSessionCount(Yii::$app->user->identity->student_uuid);
                                ?>
                                <?= Html::Button(Yii::t('app', 'SCHEDULE A NEW LESSON'), ['class' => 'btn btn-primary mb-0', 'data-creditsession' => $s_c_count, 'id' => 'studentBookingOption_button',]) ?>

                                <label class="pull-right" style="cursor: default;" style="cursor: default;">Lesson Credits = <?= $s_c_count ?> </label>

                            </div>
                        </div>
                    </div> */ ?>
                    <div class="clearfix"></div>
                    <div class="row" >
                        <!---start---->
                        <div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable ">
                            <table  class="table table-striped dt-responsive display my-datatable-student-upcoming" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <!-- <th>No.</th> -->
                                        <th>Tutor</th>
                                        <th>Date</th>
                                        <th>Time</th>
                                        <!--<th>Session Pin</th>-->
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($upcomingList)) {
                                        foreach ($upcomingList as $key => $value) {
                                            $uuid = $value["uuid"];
                                            $now = date('Y-m-d h:i A');
                                            $timestamp = strtotime($now);
                                            $timestamp_after_5 = date('Y-m-d h:i A', strtotime('+5 minutes', strtotime($now)));
                                            $cancelAllowed = true;
                                            ?>
                                            <tr class="tr_<?= $value['uuid']; ?> tution_tr" data-stime="<?= strtotime($value['start_datetime']); ?>" data-etime="<?= strtotime($value['end_datetime']); ?>" data-uuid="<?= $value['uuid']; ?>">
                                                <td width="20%"><?= $value['tutorUu']['first_name'] . ' ' . $value['tutorUu']['last_name']; ?></td>
                                                <td width="20%"><?= General::displayDate($value['start_datetime']); ?></td>
                                                <td width="20%"><?= General::displayTime($value['start_datetime']) . " - " . General::displayTime($value['end_datetime']); ?></td>
                                                <td width="30%">
                                                    <?php
                                                    $isStarting = strtotime($timestamp_after_5) >= strtotime($value['start_datetime']) && $timestamp <= strtotime($value['end_datetime']);
                                                    $reschedule_link = Url::to(['/book/reschedule-request', 'id' => $value['uuid']]);
                                                    $lesson_link = BookedSession::getStartSessionLink($value['uuid'], 'student');
                                                    echo "<button class='btn btn-xs tbl-btn-join session_start_button'
                                                            id='startbutton_$uuid'
                                                            data-uuid='$uuid'
                                                            data-url='$lesson_link'
                                                            ".($isStarting? "":"disabled title='This will be active 5 minutes before your lesson time.'")."
                                                            >
                                                        Join
                                                        </button>&nbsp;";

                                                    if ($value["is_reschedule_request"] == 1) {
                                                        echo "<a class='btn btn-xs btn-primary tbl-btn-success session_reschedule_button'
                                                                id='reschedulebutton_$uuid'
                                                                title='Reschedule initiated'
                                                                href='$reschedule_link'>
                                                            Reschedule Initiated
                                                        </a>";
                                                    } else if ($value['is_reschedule_request'] == 2) {
                                                        echo "<a class='btn btn-xs btn-primary tbl-btn-orange session_reschedule_button'
                                                                id='reschedulebutton_$uuid'
                                                                title='Reschedule initiated'
                                                                href='$reschedule_link'>
                                                            Pending Approval
                                                        </a>";
                                                    } else {
                                                        if (in_array($value["status"], ["SCHEDULE", "INPROCESS"])) {
                                                            echo "<a class='btn btn-xs btn-primary tbl-btn-warning session_reschedule_button'
                                                                id='reschedulebutton_$uuid'
                                                                title='Reschedule initiated'
                                                                href='$reschedule_link'>
                                                            Reschedule lesson
                                                            </a>";
                                                        } else {
                                                            echo $value["status"];
                                                            $cancelAllowed = false;
                                                        }
                                                    }

                                                    if ($cancelAllowed) {
                                                        $cancel_link = Url::to(['/booked-session/cancel-lesson-request', 'id' => $value['uuid']]);
                                                        echo "&nbsp;<button class='btn btn-xs btn-primary tbl-btn-danger session_cancel_button'
                                                                id='cancelbutton_$uuid'
                                                                data-url='$cancel_link'>
                                                                Cancel Lesson
                                                            </button>";
                                                    } 
                                                    ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>

                        </div>
                        <!---end--->
                    </div>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <a class="btn btn-primary btn-sm black_button_in" href="<?= Yii::$app->getUrlManager()->createUrl('/student/home'); ?>">RETURN TO HOME</a>

</section>
