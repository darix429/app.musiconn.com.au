<?php

use yii\helpers\Html;
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['tutor_name']['first_name']; ?>,</p>

            <p>A student has just booked lessons with you. Below are details of the booking:</p>

            <p><strong>Booking Reference:</strong> <?= "#" . $content['booking_id']; ?></p>
            <p><strong>Student Name:</strong> <?= $content['student_name'] . " " . $content['student_last_name']; ?></p>
            <p><strong>Instrument:</strong> <?= $content['instrument']; ?></p>

            
            <?php if (is_array($content['session_dates'])) { ?>

                <?php
                foreach ($content['session_dates'] as $key => $value) {
                    if ($content['tutor_uuid'] == $key) {
                        echo "<p><strong>Scheduled lessons :</strong><ul>";
                        foreach ($value as $keyy => $values) {
                            echo "<li>" . $values . " " . $content['time'][$key][$keyy] . "</li>";
                        }echo "</ul>";
                    }
                }
                ?>

            <?php } ?>
            
            <p>You can view the details of this booking by logging onto the <a target="_blank" href="https://app.musiconn.com.au/">Musiconn platform</a>.</p>

            <p>If you need to re-schedule a lesson with a student, please refer to the tutor's manual for instructions.</p>

            <p>Please do not reply to this unattended mailbox. Any queries can be directed to admin@musiconn.com.au, or 1300 068 742.</p>


            <p>Regards,<br>
                Admin Team – Musiconn Pty Ltd</p>
        </td>
    </tr>
</table>

