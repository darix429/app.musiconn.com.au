<?php

use yii\helpers\Html;
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear Admin,</p>

            <p><b><?= $content['student_name']; ?></b> have made request for cancel monthly subscription plan in <?= Yii::$app->name; ?>.</p>

            <p><strong>After your approvel student<b>(<?= $content['student_email']; ?>)</b> monthly subscription plan has been cancel.</strong></p>

            <p>Sincerely,</p>
            <p>System Administrators</p>
        </td>
    </tr>
</table>

