<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lesson_activity_log".
 *
 * @property string $uuid
 * @property string $booked_session_uuid
 * @property string $action
 * @property string $summary
 * @property string $user_agent
 * @property string $ip_address
 * @property string $browser
 * @property string $device
 * @property string $datetime
 */
class LessonActivityLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lesson_activity_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'booked_session_uuid', 'action', 'summary', 'user_agent', 'ip_address', 'browser', 'device'], 'string'],
            [['action', 'user_agent', 'datetime'], 'required'],
            [['datetime'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'booked_session_uuid' => Yii::t('app', 'Booked Session Uuid'),
            'action' => Yii::t('app', 'Action'),
            'summary' => Yii::t('app', 'Summary'),
            'user_agent' => Yii::t('app', 'User Agent'),
            'ip_address' => Yii::t('app', 'Ip Address'),
            'browser' => Yii::t('app', 'Browser'),
            'device' => Yii::t('app', 'Device'),
            'datetime' => Yii::t('app', 'Datetime'),
        ];
    }
}
