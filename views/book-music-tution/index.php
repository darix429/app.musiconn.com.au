<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\ArrayHelper;
use app\models\StudentMonthlySubscription;
use app\models\StudentTuitionBooking;
use app\models\Instrument;


$this->title = Yii::t('app', 'Lessons History');
$this->params['breadcrumbs'][] = $this->title;

$instrumentArray = ArrayHelper::map(Instrument::find()->where(['status' => 'ACTIVE'])->all(),'uuid','name');
$this->registerJs(
        '

$(document).on("click","#search_btn", function(){
    $.ajax({
        type: "GET",
        url: "' . Url::to(['book-music-tution/history']) . '",
        data: $("#search_frm").serialize(),
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            $("#payment_transaction_table_parent_div").html(result);
            window.ULTRA_SETTINGS.dataTablesInit();
        },
        error:function(e){
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});

$(document).on("click","#search_reset_btn", function(){
    $.ajax({
        type: "GET",
        url: "' . Url::to(['book-music-tution/history']) . '",
        data: {},
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            $("#payment_transaction_table_parent_div").html(result);
            window.ULTRA_SETTINGS.dataTablesInit();
        },
        error:function(e){
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});


', View::POS_END
);
?>

<div class="col-xl-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Book Lessons History</h2>
            <div class="actions panel_actions float-right">
                <i class="fa fa-search icon-primary icon-xs icon-orange mail_head_icon custom-toggle" data-togglediv="datatable-search-div" title="Advance Search"></i>

            </div>
        </header>
        <div class="content-body">
            <div class="row ">
                <div class="col-lg-12 col-md-12 col-12 ">
                    <div class="datatable-search-div " style="display: none;">

                        <?php
                        $form = ActiveForm::begin([
                                    'method' => 'get',
                                    'options' => [
                                        'id' => 'search_frm',
                                        'class' => 'form-inline'
                                    ],
                        ]);
                        ?>

                        <div class="col-md-3 mb-0">
		            <?= $form->field($searchModel, 'booking_id')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Booking ID'])->label(false) ?>
		        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'student_name')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Student'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'tutor_name')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Tutor'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'instrument_uuid')->dropDownList($instrumentArray, ['prompt' => 'Select Instrument', 'class' => 'form-control myselect2', 'style' => 'width: 100%'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
		            <?= $form->field($searchModel, 'order_id')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Order ID'])->label(false) ?>
		        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'discount')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Discount', 'type' => 'number'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'grand_total')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Grand Total', 'type' => 'number'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'booking_datetime')->textInput(['class' => 'form-control custom_date_picker', 'maxlength' => true, 'placeholder' => 'Booking Date'])->label(false) ?>
		        </div>

                        <?= Html::Button(Yii::t('app', 'Search'), ['class' => 'btn btn-primary mb-0', 'id' => 'search_btn', 'style' => 'margin-left: 17px;']) ?>
                        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-purple mb-0', 'id'=>'search_reset_btn']) ?>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
            <br>
            <div class=""></div>
            <div class="clearfix"></div>
            <div class="row" id="payment_transaction_table_parent_div">
                <?php echo Yii::$app->controller->renderPartial('_book_music_tuition_owner_list', ['tuitionBookingList' => $tuitionBookingList]); ?>
            </div>
        </div>
    </section>
</div>
