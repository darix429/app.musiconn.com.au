<?php
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;
use yii\web\View;
use app\models\Tutor;
use app\models\Student;
use richardfan\widget\JSRegister;

AppAsset::register($this);
if (in_array(Yii::$app->user->identity->role, ['OWNER', 'ADMIN', 'SUBADMIN'])) {
    $profile_url = Url::to(['admins/profile']);
} elseif (Yii::$app->user->identity->role == 'TUTOR') {
    $profile_url = Url::to(['tutor/profile']);
} elseif (Yii::$app->user->identity->role == 'STUDENT') {
    $profile_url = Url::to(['student/profile']);
}

$currentController = Yii::$app->controller->id;
$currentAction = Yii::$app->controller->action->id;
$websocket_url = Yii::$app->params["websocket_url"];
$user_role = Yii::$app->user->identity->role;
$user_uuid = null;
if ($user_role == "STUDENT") {
    $user_uuid = Yii::$app->user->identity->student_uuid;
} else if ($user_role == "TUTOR") {
    $user_uuid = Yii::$app->user->identity->tutor_uuid;
} else {
    $user_uuid = Yii::$app->user->identity->admin_uuid;
}

$attachment_url = Yii::$app->params["messages"]["attachment_url"];
$defaultAvatar = "data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAABC2wAAQtsBUNrbYgAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAABc7SURBVHja7Z15VJX3mcfjkphqzDSLc6bJONOmnjaTmWSm00wSE5tJ58TYSZzanp64ECMBVzR6YoyRWLVsbhi1ghoXgkajYlzqQhRFBQFBCYuAiggooFy8LO7gguI8T/JDQblw33vf/ff943OOiQnc+/ye7+e+931/ywO3b99+AFibdz8IeoR4gfgjMYGYQywkoog1xCZiB7GPSCOyieNEKeEkLgn4nw8TCcRm8f/zz5pMBBADiT7ES0QPoivqb21QBGsEvB3xFNGL8CWCia+JVBHg2wZSKuQSLl7br4nOGDcIAHge+GdEmPgTOI+oMzjkSrlFFBFbiemEj7hC6YTxhQDA/Z/uHI4xRAxRbrGwK6GeOCiuYF4lOqAHIADZAv8Q0ZP4lNhOnLNx4Nvigrg/MYL4KfoDArBr6LuIG2hbLHg5rycFRATxDtcMvQMBWP2Tvh+xjriCcCvmunhqMZZ4HD0FAVgh9B2I3kQ0cR4hVo1r4v7IW0R79BoEYLabeL3Ec3cnwqrLY8dg3DOAAIwO/qPiRl4pQmkIDcQeYhDxMHoSAtAr+DwhZzZxESE0DefEFdiv0KMQgFbBf1ZMzLmOwJmaeOI19CwEoFbwe4rHdw0Il+VE8Cp6GALw9MZeXyIZQbI8uyECCEBJ+P9EHEVwbMcuvppDj0MAroL/b8ReBMX2xBGvoOchgMbgP0ZEEjcRDulE8BwEIG/w2xOjiGqEQVpuEKEyL1OWNfyvi11xEALQuAjpDQjA/sHvLuaUo+lBS0TLtvBIluB3JKYQtWhy0Aa8nsMHArDX9loH0djAg5uEP4MArB3+wWK3WzQ08AS+YpzIV5AQgLWC35VYjQYGKsFXkN0hAGuE/2WiGE0LVKaKN3uBAMz9XH+y2HkWDQu02u6cbya3gwDMFf6nxWk2aFKgB7E8gxQCMEf4eePNGjQl0JmTdtiAxOrhn4R1+sBArhL+EIAxu+8uQgMCkxBl1X0JrRj+zuLMOTQeMBOZxD9CANqGvxtm9QETU8LHpkMA2oSfz6MvRJMBk1PBm8tAAOpP7qlCcwGLwE+lXoIA1HvMhwM1gdW4ZIU9Bswe/tFi9hUaClj1MeHbEIBn4f8IDQRssu1YfwhAWfiHYoIPsNkagqEQgHvh74/LftWb71J/v+CzA4eGlLw3Iix/SMDMnKFjwzNGfTwvjeE/87/jv+P/hv9b8R0W46Ae/IH2EQTQevjfFpdMaBgFUGCrfUfPzJ4wdXHiX5du3Lt9d2pa7vGTxWUVzgsV1TX1xG0PqeefwT+Lfyb/bP4d/Lv4d6L2HjEaAmg5/G+ImyZokta55v9heOb8JRsT96Vk5RSWnqn2IuBewb+bXwO/Fn5N/NowPm5dkfWDAJqH/yVs3eWaQcNCT34Wujxx264D6aUOZ61RgW8Lfm38Gvm18mvG2LmEH2u/DAHcPZILy3nvv6w/FzR7ZUJOPl3JmzTwbcGvnd8DvxeMaYs7DPWQWgBiem8FmuEuPsNCi5atik2i7951Vg3+vfB74ffE7w1j3Aye2t5NSgHwyimxeAKNQPiNmZ317Z6DGXYJvSv4PfJ7xZg323C0s1QC4LXTYvkkLvX9giu/3hCfbPfg38vqDbtT6L1jfccP8PL2DjIJIAqDHtQwYerifcWnHTWyhb+R4rLycx9PWbwfk76+Z5EUAuAtlGQf7AH+ISUJB7K/kzX497I3JfMw1eQMJBA0ydYC4E0UZX/W//7IGTknSs44EPzmFJwsqxw8csYxzBbUd46AnuF/TOykKu0Aj/x4XkpZhfMyAu9yHkHdyPFz07GXQNDTthIAH6Qg9lKXdmAnh0XtcVTV3ETQW8dRVX3rs9DlyZJLgM+4aG8nAUyReUA/mfrFPoRbGR9PWZwquQQm20IA9EZ6y7yqbOi4OQfLK6tvINTKOOOsujl0bHi2xAKo12O6sNbh7y7zXn4+w8PyS8rPXkSgPePkaccVqqHMswf5kNuulhQAvfCOkm/hffFoYclpBNk78gpO8lTxKxL30WqrCmCizN/hlqzcloAAq8PiFVtTJL8fMNhSAqAX/DOiVtYB8w2YmeOoqrmF8Kr2ZKBhSMDMPMl3GH7GSgKIk3iwrmfmFRQhuOqSkVvA34evS75oqKPpBUAv0kfmy7VxgZEpCKw2jAuMkP2rwBRTC4Be4OOEU+ZB2puSlY2wasOe5MwcyQXAX6u7m1kA0ZJv3VWIoGoLthoLijGlAMSmnlKv5opcvnkvQqotEcs27ceqwaDXTSUAekGdiALZN3osPu2oRkg134n4PHYfDspWa62AWgIIld3K4wIjExFQfRg7KSIVVwFBo0whAHohz+Ewj6Db2OBDP+KTMvIggCA+mOUxMwggDoMRdJ4aExN/9JsY1Li3vux9F2moAOgFvIJBCLo9JGBmJoKpL1Tzo+i9oJt8roaRAsCnP/Fp0FLc/deZidOWHEDvfc9eQwRAv7gniv8Dy1bHQgA6s2TlNgjgLn8yQgC7UPgfSDqYg9l/OrMvJSsfvXcH/jrUTjcB0C97FUW/+z2srMKJTT/03ywEJ0k3p6+eAtiNgt852ceJQBoD1f4ievAOyboIAJ/+9x/thTAaJoAL6MFm9NRDAPEoNARgEgHg2PHmbNFUAPQLXkORIQATCaAGPXjfyULPaikAfPpDAGYSQDV68D6iNBGAONcPBYYAzCSASvTg/VvSEU9pIYCFKC4EAAFYgtmqCoB+4MMEbrhAABCARc6kIB5VUwCDUFQIAAKwFJ+qKYA9KCgEAAFYilJ3pge7E/6fiscLKCoEAAFYi15qCCAYhYQAIABLstArAfDGg+JSAsWEACAA68FndHTwRgBvoYgQAARgaXp7I4AYFBACgAAsTbRHAhDHfF1DASEACMDaG9YSD3kigLEoHgQAAdiCfp4IYB8KBwFAALZgnSIB0P/Q5V25z2KHACAAO3GFM61EAO+gaBAABGArBioRQAQKBgFAAPbfLciVAApQMAgAArAVdS09DXA19x8FgwAgAAk2DW1JACNQKAgAApBjiXBLAtiEQkEAEIAt2d6qAHjhAIG91iEACMCenLt3jwAc+gEBQABy8UJrAsDafwgAArA3Y1oTwEEUCAKAAGxNTIsCoL/oRNSjQBAABGBryl0J4AUUxyMB4HRg4wTgRA96xDMtCcAHhVHO8tXf7kcYjWH56thk9KBH+LYkgOkojDLCI9ftQRCNZdaCtZCAcqJaEsBWFMZ9fANmZlMDNiCExuKoquGxOIKeVEReSwIoQmHc5ubBrGPHEUBzkJZ59ASPCfpS0cKgdncEQP/QmbiFwrhHwIT5aQieueAxQW8q4qmmAvg1CuI+MVv2pSJ05oLHBL2p/NSgRgH4oiBuc63U4axF6MwFjwl2sVb+JKBRAOEoiHsMGhZ6EoEzJzw26FG3CW4qgB0oiHv4fxieibCZEx4b9KjbfN1UADj/z03GBUamIGzmhMcGPeo2qd8LgP7QFcVwn48mL4QATAqPDXrUbZyNAuiBYkAAEICUPMICeAmFgAAgADk3B2EB9EEhIAAIQEr+yAIYiEJAABCAlExgAQSgEBAABCAlc1gAk1EICAACkJKFLIA5KAQEAAHIuS8ACyAKhYAAIAApWcMC2IxCQAAQgJRsYgEkoBAQAAQgJTtYAIdRCAgAApCSfVgIBAFAAPKSxgK4hEIoEkAywgYB2IRsFgBOA8YVAAQgJ8dZAA4UAgKAAKSkFNuBK94QJAICMCljJ0VAAAr3BGAB5KIQ7vPB6FmHETZz8v6oGTggRBmXWADYT10BA/xDyhE2c0Ljcxk9qoiLLIC9KIQibpVVOK8icObiyIlT5ehNxZxiAWxHIRQeDPI3HAxiNjbvSD6E3lRMJgtgPQqhdGvw2VkInbnwDZiZg95UzG4WQDQKoZiGLLrmRPDMwZadKenoSY+IYQEsRCGUQ584ueWVVfUIoNHHg1ff8hkeVoie9IjFOBbMC0LmfJWAEBoLjwF60WNCWQBBKITnXwWi1+7E2gCDmDZzRSJ60CvGswAmohDeMXVmdCJfiiKUel321zQEhizbj97zmiEsgDEohAoSmBG9D+HUPvibYpMODR45/Rh6ThX6sgB8UQjvGegfUoqQavqcP+O94WEn0Guq0pMF8BsUQh2yjpwoRFjV51R5RR3Vtw49pjq/ZAF0QyHUIXL5ZjwV0IBV3+w6iP7ShCcf4COC6Q/nUAzvGTZuTjoCqz5Dx4Zj30oNnmARHRoFkIqCqEJdWYWzDqFVj2NFpdW8AAu9pTrVnP1GAWA6sErExqdlILjqMX/JBmzyoQ2JTQUwCQVRhwlTFychuOrx3ogw7FilDQuaCqAfCqIal0sdzlqE13tSM44g/Nrh11QAz6Ig6vHV+l3YN1CdTT5xb0o7/rOpAB4k6lEU7BtoFvKLS6uoltfRT5rAWe90RwBCAsdRGPUesRw+VoSZgV4Q+vkqzPXXjrzG3DcVwFYURs2lwqswKchDSh3Ouv5+wZiboh2rWxLAbBRGPaiBnZgT4BlLVm5LQg9pyictCcAfhVGXiGWbEhFo5Sv+Bg0LPYX+0ZQ3WxLAqyiMJlcBeCSogDWb9hxA72hOt5YE0Jm4huKoy4KlG3EvwE1OOyuvDxwaUoa+0ZTyxsw3E4CQQDwKpPpVQCVdBVxBwNuGvzKhZzRnR2sC+AQFUp/5SzZit6A2OHnacZFkWYN+0ZwZrQngeRRIm6uAUofzMoLumuBw7O6rE39wKQAhAQeKpMFVwBcb9iLorpb8llRgxx9d4Ht8XdoSwEoUClcBejIuMAJLfg34/u9KAINQKG2Yu/ibeAS+Odt3p2agN3QjwB0BPCm2C0LB1L8KqCoqLa9C8O9s9lk7wD8EXzn1o3ubAhAS+A7F0oaxkyKwYYhgUtBSXPrrR3ZLWXclgDAUTDu2xqWkyR7+PcmZR3GlqSvBSgTwOgqmHQP8gx0nz1RckDX8ZRXO64OGhZaiF3TlRSUC4A1CLqFomu4duF9WAfx5+pfY6Ufn6b9EO7cFICSwBYXTdtOQuIT0TNnC/822xEyMve4sdZXz1gQwCoXTFl74UuI4K806gawjJ3ihz2WMve709UQAT2B1oPaM//OiZFl2+fEZji2+DYBnWP5IsQCEBL5GAbVnZUxcsv1n+0Vinb8xbGst420JAE8D9KH2jLPqhl3DH5eQno0xNgw/jwUgJJCPImpPWYXzml0FsCk26RDG2BDO80Y/3gpgPAoJAUAAlmRuW/l2RwC4GQgBQADWg09U/rnXAsDNQAgAArAkse5k210B4GYgBAABWIs+qgkANwMhAAjAUhS4mvrrjQBwMxACgACswTh3c61EALgZCAFAAOaHp1o/qroAcDMQAoAALMEiJZlWKgDcDIQAIABz8y+aCUBIACe3QgAQgDmJV5pnTwTQE4WGACAAU9JPcwFgsxAIAAIwJXlEe70E8BxxE0WHACAA0/A7T7LskQCEBKJRdAgAAjAFuzzNsTcC6E5cRfHV2Q+AgtJgVwFs252Kcya0XfTzgu4CEBL4HAPgPT7DwwrtvBtQRm5BMcZZM6K8ybC3AnicuIBB8I4RH809ZPNzAPhKEYeAqM8V4ieGCUBI4DMMhHdMDotKtPuegAP8Q8ox1qrzF2/zq4YAOhM44NEL1m9NsP1RYf4fzs7CWKsKZ66L4QIQEhiJAfEMPiasvLKq3u4CWBkTh4NA1cVfjeyqJYCOxAkMinLmLIxJkOFcgNPOyhv9/YIrMeaqkOPJpB/NBCAk8A4GRjFXC0vO1MhyMtCsBWuxjkQdequVW9UEICQQhcFxn0XRW6Q6IPREyenz/f1w6KyX7FQzs2oLoCtxCoPUNuMCI1NkPBl4e3xqlpi8gj5QDsvzGdMKQEjgDTzzbZ33Rkw/XlbhrJP1ePAFyzbhq4Bn+KmdV9UFICQwD4PVMu+PmpF3tLDEIWv4G+FDUdEPitioRVa1EsDDxDEMWjMa/jJrRaIMj/zcwVFV00BXAolYT+IWPInqccsIQEjgRaIeg/f9s/6z23enfofgt7xOYPDI6fiwaOWDg3hTq5xqJgAhgWDZd2idtWBtQqnDWYuwu4aviuZ98U1if7/gGgT+PuZpmVGtBcAThDIkHLQbgcHL9p8oOV2NgLtPSfnZK58vWg8R3CWX6GRZATTZPUia73mjP/lr6uFjhSUINETgJXwGx/Na51NzAQgJTLD7gH0wetbhpIM5RxBgiEAlxuuRTb0E0J5ItOVmHsNCi7bsTElHYDUVQa1kIoh/182z/SwhACGBbkSxne7sf7lmR1J5ZfVNhBQiUBF+b0/plUvdBCAk8Cxx3urTMcMjY3Bn33gR7LehCG5o+cjPcAEICfxWvFHLDc5nocsTZVq9BxHojq/eedRdAEICH1hpIsboiXxnv6gUoYMINGSaEVk0RABCAmGmv7M/ZlZ28qHcowgZRKAxXxqVQyMF0I5YZ9ZturfGHcCdfYhAD+J4wpx0AhAS6EQcMNGd/Yov1+5IdlRV30KQIAIdyOY9NIzMoKECEBJ4kigy+s4+780n8xp9iEB3yvR83GdaAQgJ/JI4Z8AgXJ+MO/sQgf7wYTr/aobsmUIATXYSuq7Xnf0xny44kJNfXIZgQAR6f+jwo3Cz5M40AhASGKD1HgJ+Y2ZnJ6fnHkMQgEEiGGymzJlKAEIC/6fF6kG+s79tF+7sA5ciSNJYBLyxx2iz5c10AmgyW/CyWifvRK/diTv7wEgR8MxXHzNmzZQCEBJ42Zsbg/39gi7OWbged/aB0SKoI942a85MKwAhgeeJCsV39sOiEgtLcWcfGC4Cvtvfy8wZM7UAhAR6ECXuFHzE+LmHco/jzj7QTARKrkjPEv9u9nyZXgBCAt2J460Uu3bZqtgkNCvQkqKy8guBwcv4UJObbYSfT8fqYYVsWUIAQgJ/L6ZONiv24BHT8zPzThSjQYFeHMrOL+SFYi7Cf5R42iq5sowAhAR+3GTtQMO0mSsSzjirrqMpgREHm/DuRPdcDRwinrBSpiwlACGBLoNHTt8Yl5CeiUYERrN7f0bOAL9gJ/XlHuIRq+XJcgJgqPAPE8vRgMAM5B4vXqb1/v0QQMsi8CeuogmBQVwjAqycIUsLQEjgV8RJNCPQmVPEi1bPj+UFICTwGBGLpgQ6sZ17zg7ZsYUAhATaEVMIzPkHWsFnQARyr9klN7YRQBMR9Caq0KxAZSqI/7ZbXmwnACGB7kQymhaoRALxD3bMii0F0OQrwRjiEhoYeMhlYizR3q45sa0A7rkawA1CoJRviX+yez5sL4AmIhhEVKKxQRtwj/jIkgtpBCAk8CSxGk0OXLCKeEKmTEglgCYi+B2Bs/5A00k9fWTMgpQCEBJ4hJhLYDWhvNQT84kusuZAWgE0EcE/EysxgUgqeKzXED1k73/pBdBEBM8Rf0M4bM9W4nn0PATgSgSviIkfCIu92EO8jB6HANwVQR8Cm45YnzTif9DTEICnswn7E0cQJMtxmPg9+hgCUEsGvyU2iRVhCJh57+pvIN5Az0IAWk4tno5ZhabiLBFCPI0ehQD0EkEn4n3iEAJoGCliiveD6EkIwEgZ/BfxldgjDsHUllqxIex/oPcgALOJ4MfiqmALNitVlauiplzbv0OvQQBWkEEX4l0iRqwrR5CVwfs4rBM17IKeggCsLAM+x+D3YsrxOYTbJXyi8wqiL99jQe9AAHaUwYPEW2IhUprki5FuEOliQc6bREf0CAQg49XBa8RE8T3XafPNNngu/iTiN8SP0AMQALhfCj2IIcQSIteik49uide+RLyXHhhbCAB4JoSOxM/FV4dRxBxis5juauSmp3VimjR/qs8jPiT+l/gF8RDGDgIA+giim1jF6CMOruCZiguIKPEUIlascPyOyCfKxM3IG4LzxBmigMgSW6rHERvF/IbFQjrTCF+iF/ETOx2QISv/D/94Y2Ny3YIVAAAAAElFTkSuQmCC";

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>

        <link rel="shortcut icon" href="/theme_assets/images/favicon.ico" type="image/x-icon" /><!-- Favicon -->
        <link rel="apple-touch-icon-precomposed" href="/theme_assets/images/apple-touch-icon-57-precomposed.png"><!-- For iPhone -->
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/theme_assets/images/apple-touch-icon-114-precomposed.png">   <!-- For iPhone 4 Retina display -->
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/theme_assets/images/apple-touch-icon-72-precomposed.png"><!-- For iPad -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/theme_assets/images/apple-touch-icon-144-precomposed.png"><!-- For iPad Retina display -->    
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
        <script src="https://browser.sentry-cdn.com/5.19.0/bundle.min.js" integrity="sha384-edPCPWtQrj57nipnV3wt78Frrb12XdZsyMbmpIKZ9zZRi4uAxNWiC6S8xtGCqwDG" crossorigin="anonymous"></script>
        <script>
            Sentry.init({ dsn: 'https://817fc244e238470f990f3e2af587bc03@o399941.ingest.sentry.io/5257765' });
        </script>
        
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>
        <!-- START TOPBAR -->
        
        <!-- Loader code -->
        <div id="preloader" style="display: block;">
            <div id="loader"></div>
            <p id="loader_message"></p>
        </div>

        
        <div class='page-topbar '>
            <a href="<?= Yii::$app->homeUrl; ?>"><div class='logo-area'> </div></a>
            <div class='quick-area'>
                <div class='float-left'>
                    <ul class="info-menu left-links list-inline list-unstyled">
                        <li class="sidebar-toggle-wrap list-inline-item">
                            <a href="#" data-toggle="sidebar" class="sidebar_toggle">
                                <i class="fa fa-bars"></i>
                            </a>
                        </li>
                        <li class="notify-toggle-wrapper list-inline-item">
                            <a href="#" data-toggle="dropdown" class="toggle d-none">
                                <i class="fa fa-bell"></i>
                                <span class="badge badge-pill badge-orange">3</span>
                            </a>
                            <ul class="dropdown-menu notifications animated fadeIn">
                                <li class="total dropdown-item">
                                    <span class="small">
                                        You have <strong>3</strong> new notifications.
                                        <a href="javascript:;" class="float-right">Mark all as Read</a>
                                    </span>
                                </li>
                                <li class="list dropdown-item">

                                    <ul class="dropdown-menu-list list-unstyled ps-scrollbar">
                                        <li class="unread available"> <!-- available: success, warning, info, error -->
                                            <a href="javascript:;">
                                                <div class="notice-icon">
                                                    <i class="fa fa-check"></i>
                                                </div>
                                                <div>
                                                    <span class="name">
                                                        <strong>Server needs to reboot</strong>
                                                        <span class="time small">15 mins ago</span>
                                                    </span>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="unread away"> <!-- available: success, warning, info, error -->
                                            <a href="javascript:;">
                                                <div class="notice-icon">
                                                    <i class="fa fa-envelope"></i>
                                                </div>
                                                <div>
                                                    <span class="name">
                                                        <strong>45 new messages</strong>
                                                        <span class="time small">45 mins ago</span>
                                                    </span>
                                                </div>
                                            </a>
                                        </li>
                                        <li class=" busy"> <!-- available: success, warning, info, error -->
                                            <a href="javascript:;">
                                                <div class="notice-icon">
                                                    <i class="fa fa-times"></i>
                                                </div>
                                                <div>
                                                    <span class="name">
                                                        <strong>Server IP Blocked</strong>
                                                        <span class="time small">1 hour ago</span>
                                                    </span>
                                                </div>
                                            </a>
                                        </li>
                                        <li class=" offline"> <!-- available: success, warning, info, error -->
                                            <a href="javascript:;">
                                                <div class="notice-icon">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                                <div>
                                                    <span class="name">
                                                        <strong>10 Orders Shipped</strong>
                                                        <span class="time small">5 hours ago</span>
                                                    </span>
                                                </div>
                                            </a>
                                        </li>
                                        <li class=" offline"> <!-- available: success, warning, info, error -->
                                            <a href="javascript:;">
                                                <div class="notice-icon">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                                <div>
                                                    <span class="name">
                                                        <strong>New Comment on blog</strong>
                                                        <span class="time small">Yesterday</span>
                                                    </span>
                                                </div>
                                            </a>
                                        </li>
                                        <li class=" available"> <!-- available: success, warning, info, error -->
                                            <a href="javascript:;">
                                                <div class="notice-icon">
                                                    <i class="fa fa-check"></i>
                                                </div>
                                                <div>
                                                    <span class="name">
                                                        <strong>Great Speed Notify</strong>
                                                        <span class="time small">14th Mar</span>
                                                    </span>
                                                </div>
                                            </a>
                                        </li>
                                        <li class=" busy"> <!-- available: success, warning, info, error -->
                                            <a href="javascript:;">
                                                <div class="notice-icon">
                                                    <i class="fa fa-times"></i>
                                                </div>
                                                <div>
                                                    <span class="name">
                                                        <strong>Team Meeting at 6PM</strong>
                                                        <span class="time small">16th Mar</span>
                                                    </span>
                                                </div>
                                            </a>
                                        </li>

                                    </ul>

                                </li>

                                <li class="external dropdown-item">
                                    <a href="javascript:;">
                                        <span>Read All Notifications</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="d-sm-none d-none searchform list-inline-item">
                            <div class="input-group">
                                <span class="input-group-addon input-focus">
                                    <i class="fa fa-search"></i>
                                </span>
                                <form action="search-page.html" method="post">
                                    <input type="text" class="form-control animated fadeIn" placeholder="Search & Enter">
                                    <input type='submit' value="">
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class='float-right'>
                    <ul class="info-menu right-links list-inline list-unstyled">

                        <?php if(Yii::$app->session->get('user.idbeforeswitch.owner') && Yii::$app->session->get('user.idbeforeswitch.owner') > 0 && in_array(Yii::$app->user->identity->role, ['ADMIN','SUBADMIN']) ){ ?>
                            <li class=" list-inline-item">
                                <a href="<?= Url::to(['/admins/back-to-owner/'])?>" class="btn btn-orange right15 " title="Back To Owner Portal"><i class="fa fa-level-up"></i> Back To Owner</a>
                            </li>
                        <?php } ?>

                        <?php if(Yii::$app->session->get('user.idbeforeswitch.admin') && Yii::$app->session->get('user.idbeforeswitch.admin') > 0 && in_array(Yii::$app->user->identity->role, ['STUDENT','TUTOR']) ){ ?>
                            <li class=" list-inline-item">
                                <a href="<?= Url::to(['/admins/back-to-admin/'])?>" class="btn btn-orange right15 " title="Back To <?= ucfirst(strtolower(Yii::$app->session->get('user.idbeforeswitch.admin.role')));?> Portal"><i class="fa fa-level-up"></i> Back To <?= ucfirst(strtolower(Yii::$app->session->get('user.idbeforeswitch.admin.role')));?></a>
                            </li>
                        <?php } ?>


                        <li class=" list-inline-item mr-3">
                            <a href="/index.php/message"><button class="btn message-btn" style="font-size: 14px;">Messages <span class="badge badge-danger unseen-msg-count"></span></button></a>
                        </li>                            
                        <li class="profile list-inline-item">

                            <a href="#" data-toggle="dropdown" class="toggle">
                                <?php
                                if (isset(Yii::$app->user->identity->tutor_uuid)) {
                                    $tutorProfile = Tutor::findOne(Yii::$app->user->identity->tutor_uuid);
                                    ob_start();
                                    fpassthru($tutorProfile->profile_image);
                                    $contents = ob_get_contents();
                                    ob_end_clean();
                                    //print_r($contents);
                                    $dataUri = "data:image/jpg;base64," . base64_encode($contents);
                                    echo "<img src='$dataUri' class='rounded-circle img-inline' id='menuImage'/>";
                                } else {
                                    ?>
                                    <img src="/data/profile/profile.png" alt="user-image" class="rounded-circle img-inline">
                                <?php } ?>
                                <span><?= Yii::$app->user->identity->first_name . ' ' . Yii::$app->user->identity->last_name ?> <i class="fa fa-angle-down"></i></span>
                            </a>
                            <ul class="dropdown-menu profile animated fadeIn">
                                <li class="dropdown-item">
                                    <a href="<?= $profile_url; ?>">
                                        <i class="fa fa-user"></i>
                                        Profile
                                    </a>
                                </li>
                                <?php
                                if (!Yii::$app->user->isGuest) {
                                    ?>
                                    <li class="last dropdown-item">
                                        <?php echo Html::beginForm(['/site/logout'], 'post', ['id' => 'logoutfrm']) ?>
                                        <a href="javascript:$('#logoutfrm').submit();">
                                            <i class="fa fa-lock"></i>
                                            Logout
                                        </a>
                                        <?php echo Html::endForm() ?>
                                    </li>
                                <?php } ?>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
        <!-- END TOPBAR -->

        <!-- START CONTAINER -->
        <div id="page-container" class="page-container row-fluid">

            <!-- SIDEBAR - START -->
            <div class="page-sidebar ">

                <!-- MAIN MENU - START -->
                <div class="page-sidebar-wrapper" id="main-menu-wrapper">

                    <!-- USER INFO - START -->
                    <div class="profile-info row">

                        <div class="profile-image col-lg-4 col-md-4 col-4">
                            <a href="<?= $profile_url; ?>" id="profileImage">
                                <?php
                                if (isset(Yii::$app->user->identity->tutor_uuid)) {
                                    $tutorProfile = Tutor::findOne(Yii::$app->user->identity->tutor_uuid);
                                    ob_start();
                                    fpassthru($tutorProfile->profile_image);
                                    $contents = ob_get_contents();
                                    ob_end_clean();
                                    //print_r($contents);
                                    $dataUri = "data:image/jpg;base64," . base64_encode($contents);
                                    echo "<img src='$dataUri' class='rounded-circle img-inline'/>";
                                } else {
                                    ?>
                                    <img src="/data/profile/profile.png" alt="user-image" class="rounded-circle img-inline">
                                <?php } ?>
                            </a>
                        </div>

                        <div class="profile-details col-lg-8 col-md-8 col-8">
                            <h3>
                                <a href="<?= $profile_url; ?>">
                                    <?= Yii::$app->user->identity->first_name . ' ' . Yii::$app->user->identity->last_name ?>
                                </a>
                                <!-- Available statuses: online, idle, busy, away and offline -->
                                <span class="profile-status online"></span>
                            </h3>
                            <p class="profile-title"><?= Yii::$app->user->identity->role; ?></p>
                        </div>

                    </div>
                    <!-- USER INFO - END -->



                    <ul class='wraplist'>

                        <?php if(empty(in_array(Yii::$app->user->identity->role, ['STUDENT','TUTOR']))){ ?>
                        <li class="<?= (in_array($currentController, ['site']) && $currentAction == 'index' ) ? 'open' : '' ?>">
                            <a href="<?= Yii::$app->homeUrl; ?>">
                                <i class="fa fa-dashboard"></i>
                                <span class="title">Dashboard</span>
                            </a>
                        </li>
                        <?php } ?>
                        <?php if (Yii::$app->user->identity->role == 'OWNER') { ?>

                            <li class="<?= (in_array($currentController, ['admins']) && $currentAction == 'index' ) ? 'open' : '' ?>">
                                <a href="<?= Yii::$app->getUrlManager()->createUrl('/admins/index'); ?>">
                                    <i class="fa fa-user"></i>
                                    <span class="title">Users</span>
                                </a>
                            </li>
                            <li class="<?= (in_array($currentController, ['tutor']) && $currentAction == 'index' ) ? 'open' : '' ?>">
                                <a href="<?= Yii::$app->getUrlManager()->createUrl('/tutor/index'); ?>">
                                    <i class="fa fa-user"></i>
                                    <span class="title">Tutors</span>
                                </a>
                            </li>
                            <li class="<?= (in_array($currentController, ['student','cancel-session']) && in_array($currentAction ,[ 'index','credit-lesson-admin','cancel-session']) ) ? 'open' : '' ?>">
                                <a href="javascript:;">
                                    <i class="fa fa-users"></i>
                                    <span class="title">Students</span>
                                    <span class="arrow "></span>
                                </a>
                                <ul class="sub-menu">
                                  <li>
                                      <a class="<?= (in_array($currentController, ['student']) && ($currentAction == 'index' || ($currentAction == 'student'))) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/student/index'); ?>">
                                          <span class="title">Student List</span>
                                      </a>
                                  </li>
                                  <li>
                                      <a class="<?= (in_array($currentController, ['cancel-session']) && ($currentAction == 'index')) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/cancel-session/index'); ?>">
                                          <span class="title">Cancel Lesson Request</span>
                                      </a>
                                  </li>
                                </ul>
                            </li>
                            <li class="<?= (in_array($currentController, ['video']) && in_array($currentAction, ['uploaded', 'published-video', 'review', 'upload']) ) ? 'open' : '' ?>">
                                <a href="javascript:;">
                                    <i class="fa fa-film"></i>
                                    <span class="title">Pre Recorded Videos</span>
                                    <span class="arrow "></span>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a class="<?= (in_array($currentController, ['video']) && ($currentAction == 'published-video' || ($currentAction == 'review' && Yii::$app->session['status'] == 'published')) ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/video/published-video'); ?>">
                                            <span class="title">Published Videos</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['video']) && ($currentAction == 'uploaded' || $currentAction == 'upload' || ($currentAction == 'review' && Yii::$app->session['status'] == 'uploaded') )) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/video/uploaded'); ?>">
                                            <span class="title">Uploaded Videos</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="<?= (in_array($currentController, ['video']) && in_array($currentAction, ['recorded', 'tution-view']) ) ? 'open' : '' ?>">
                                <a href="javascript:;">
                                    <i class="fa fa-video-camera"></i>
                                    <span class="title">Lesson Videos</span>
                                    <span class="arrow "></span>
                                </a>
                                <ul class="sub-menu" >
                                    <li>
                                        <a class="<?= (in_array($currentController, ['video']) && ($currentAction == 'recorded' || ($currentAction == 'tution-view' && Yii::$app->session['status'] == 'published') )) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/video/recorded'); ?>">
                                            <span class="title">Recorded Videos</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="<?= (in_array($currentController, ['instrument', 'instrument-category']) && in_array($currentAction, ['index']) ) ? 'open' : '' ?>">
                                <a href="javascript:;">
                                    <i class="fa fa-list"></i>
                                    <span class="title">Configuration</span>
                                    <span class="arrow "></span>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a class="<?= (in_array($currentController, ['instrument']) && $currentAction == 'index' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/instrument/index'); ?>">
                                            <span class="title">Instruments</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['instrument-category']) && $currentAction == 'index' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/instrument-category/index'); ?>">
                                            <span class="title">Categories</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="<?= ((in_array($currentController, ['tutorial-type', 'payment-term', 'online-tutorial-package', 'monthly-subscription-fee', 'coupon']) && in_array($currentAction, ['index','create','update']))
                                                || (in_array($currentController, ['musicoin-coupon', 'coupon','musicoin-package',]) && in_array($currentAction, ['index','create','update']))
                                                
                                                ) ? 'open' : '' ?>">
                                <a href="javascript:;">
                                    <i class="fa fa-money"></i>
                                    <span class="title">Billing Configuration</span>
                                    <span class="arrow "></span>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a class="<?= (in_array($currentController, ['tutorial-type']) && $currentAction == 'index' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/tutorial-type/index'); ?>">
                                            <span class="title">Tutorial Types</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['online-tutorial-package']) && ($currentAction == 'index' || $currentAction == 'create' || $currentAction == 'update') ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/online-tutorial-package/index'); ?>">
                                            <span class="title">Online Tutorial Package</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['monthly-subscription-fee']) && in_array($currentAction, ['index', 'create', 'update']) ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/monthly-subscription-fee/index'); ?>">
                                            <span class="title">Monthly Subscription Package</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['musicoin-package']) && in_array($currentAction, ['index','create','update']) ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/musicoin-package/index'); ?>">
                                            <span class="title">Musicoin Package</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['musicoin-coupon']) && in_array($currentAction, ['index','create','update']) ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/musicoin-coupon/index'); ?>">
                                            <span class="title">Musicoin Promo Code</span>
                                        </a>
                                    </li>
                                    
                                </ul>
                            </li>
                            <li class="<?= ( (in_array($currentController, ['booked-session', 'book-music-tution', 'payment-transaction','order','video','student-view-recorded-lesson','student-view-pre-recorded-lesson']) && in_array($currentAction, ['index', 'history','view','sales-representative-commissions','report'])) 
                                                || ($currentController == 'tutor' && in_array($currentAction, ['availability','report-tutor-list', 'tutor-plan-change-history']) ) 
                                                || ($currentController == 'student' && $currentAction == 'registration-report') 
                                                || ($currentController == 'monthly-subscription' && $currentAction == 'report') 
                                                || ($currentController == 'online-tutorial-package' && $currentAction == 'get-rate-option-history') 
                                                || (in_array($currentController, ['musicoin',]) && in_array($currentAction, ['index',]))
                                                ) ? 'open' : '' ?>">
                                <a href="javascript:;">
                                    <i class="fa fa-list"></i>
                                    <span class="title">Reports</span>
                                    <span class="arrow "></span>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a class="<?= (in_array($currentController, ['message']) && $currentAction == 'report' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/message/report'); ?>">
                                            <span class="title">Messaging</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['musicoin-purchases']) && $currentAction == 'index' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/musicoin-purchases/index'); ?>">
                                            <span class="title">Musicoin Purchases</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['student-view-pre-recorded-lesson']) && $currentAction == 'index' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/student-view-pre-recorded-lesson/index'); ?>">
                                            <span class="title">Pre Recorded videos View history</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['student-view-recorded-lesson']) && $currentAction == 'index' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/student-view-recorded-lesson/index'); ?>">
                                            <span class="title">Recorded Lessons View History</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['booked-session']) && $currentAction == 'index' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/booked-session/index'); ?>">
                                            <span class="title">All Lessions History</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['tutor']) && $currentAction == 'availability' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/tutor/availability'); ?>">
                                            <span class="title">Tutor Availability</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['book-music-tution']) && in_array($currentAction, ['history','view'] )) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/book-music-tution/history'); ?>">
                                            <span class="title">Lessons History</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['payment-transaction']) && $currentAction == 'index' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/payment-transaction/index'); ?>">
                                            <span class="title">Payment Transaction</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['order']) && $currentAction == 'history' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/order/history'); ?>">
                                            <span class="title">Order History</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['book-music-tution']) && $currentAction == 'sales-representative-commissions' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/book-music-tution/sales-representative-commissions'); ?>">
                                            <span class="title">Sales Report</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['student']) && $currentAction == 'registration-report' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/student/registration-report'); ?>">
                                            <span class="title">Registrations Report</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['monthly-subscription']) && $currentAction == 'report' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/monthly-subscription/report'); ?>">
                                            <span class="title">Subscriptions Report</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['video']) && $currentAction == 'report' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/video/report'); ?>">
                                            <span class="title">Uploaded Videos Report</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['tutor']) && $currentAction == 'report-tutor-list' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/tutor/report-tutor-list'); ?>">
                                            <span class="title">Tutors Listing</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['tutor']) && $currentAction == 'tutor-plan-change-history' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/tutor/tutor-plan-change-history'); ?>">
                                            <span class="title">Tutor Plan Changes History</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['musicoin']) && in_array($currentAction, ['index',]) ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/musicoin/index'); ?>">
                                            <span class="title">Musicoin History</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>


                        <?php } else if (Yii::$app->user->identity->role == 'STUDENT') { ?>

                            <li class="<?= (in_array($currentController, ['booked-session','video',]) && in_array($currentAction, ['upcoming','completed','tution-view','cancelled','credited'] ) ) ? 'open' : '' ?>">
                                <a href="<?= Yii::$app->getUrlManager()->createUrl('/booked-session/upcoming'); ?>">
                                    <i class="fa fa-laptop"></i>
                                    <span class="title">My Lessons</span>
                                </a>
                            </li>
                            <li class="<?= (in_array($currentController, ['monthly-subscription']) && in_array($currentAction, ['index']) ) ? 'open' : '' ?>">
                                <a href="<?= Yii::$app->getUrlManager()->createUrl('/monthly-subscription/index'); ?>">
                                    <i class="fa fa-money"></i>
                                    <span class="title">My Subscription</span>
                                </a>
                            </li>
                            <li class="<?= (in_array($currentController, ['video']) && in_array($currentAction, ['student-video-list','review']) ) ? 'open' : '' ?>">
                                <a href="<?= Yii::$app->getUrlManager()->createUrl('/video/student-video-list'); ?>">
                                    <i class="fa fa-video-camera"></i>
                                    <span class="title">Videos</span>
                                </a>
                            </li>
                            <li class="<?= (in_array($currentController, ['monthly-subscription', 'order','book-music-tution', 'booked-session']) && in_array($currentAction, ['history','view','expired']) ) ? 'open' : '' ?>">
                                <a href="javascript:;">
                                    <i class="fa fa-list"></i>
                                    <span class="title">Reports</span>
                                    <span class="arrow "></span>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a class="<?= (in_array($currentController, ['monthly-subscription']) && $currentAction == 'history' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/monthly-subscription/history'); ?>">
                                            <span class="title">Monthly Subscription History</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['book-music-tution']) && in_array($currentAction, ['history','view'] )) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/book-music-tution/history'); ?>">
                                            <span class="title">Lessons History</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['order']) && $currentAction == 'history' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/order/history'); ?>">
                                            <span class="title">Order History</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['booked-session']) && $currentAction == 'expired' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/booked-session/expired'); ?>">
                                            <span class="title">Missed Lessons</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                        <?php } else if (Yii::$app->user->identity->role == 'TUTOR') { ?>
                            <li class="<?= (in_array($currentController, ['booked-session','video']) && in_array($currentAction, ['upcoming','completed','tution-view','cancelled'] ) ) ? 'open' : '' ?>">
                                <a href="<?= Yii::$app->getUrlManager()->createUrl('/booked-session/upcoming'); ?>">
                                    <i class="fa fa-laptop"></i>
                                    <span class="title">My Lessons</span>
                                </a>
                            </li>
                            
                            <li class="<?= (in_array($currentController, ['tutor']) && in_array($currentAction, ['assign-plan'] ) ) ? 'open' : '' ?>">
                                <a href="<?= Yii::$app->getUrlManager()->createUrl(['/tutor/assign-plan','id' => Yii::$app->user->identity->tutor_uuid]); ?>">
                                    <i class="fa fa-dollar"></i>
                                    <span class="title">Plan Rate</span>
                                </a>
                            </li>

                            <li class="<?= (in_array($currentController, ['video']) && in_array($currentAction, ['uploaded', 'published', 'upload', 'update', 'review']) ) ? 'open' : '' ?>">
                                <a href="javascript:;">
                                    <i class="fa fa-film"></i>
                                    <span class="title">Pre Recorded Videos</span>
                                    <span class="arrow "></span>
                                </a>
                                <ul class="sub-menu" >
                                    <li>
                                        <a class="<?= (in_array($currentController, ['video']) && ($currentAction == 'published' || $currentAction == 'review') ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/video/published'); ?>">
                                            <span class="title">Published Videos</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['video']) && ($currentAction == 'uploaded' || $currentAction == 'upload' || $currentAction == 'update') ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/video/uploaded'); ?>">
                                            <span class="title">Uploaded Videos</span>
                                        </a>
                                    </li>


                                </ul>
                            </li>
			    <li class="<?= (($currentController == 'tutor' && in_array($currentAction ,['apps']) )) ? 'open' : '' ?>">
		                    <a href="<?= Yii::$app->getUrlManager()->createUrl('/tutor/apps'); ?>">
		                        <i class="fa fa-th"></i> <span class="title">APPS</span>
		                    </a>
		                </li>
                            <li class="<?= (in_array($currentController, ['tutor']) && $currentAction == 'working-plan' ) ? 'open' : '' ?>">
                                <a href="<?= Yii::$app->getUrlManager()->createUrl('/tutor/working-plan'); ?>">
                                    <i class="fa fa-calendar"></i>
                                    <span class="title">Availability</span>
                                </a>
                            </li>
                            <li class="<?= (in_array($currentController, ['booked-session']) && in_array($currentAction, ['expired']) ) ? 'open' : '' ?>">
                                <a href="javascript:;">
                                    <i class="fa fa-list"></i>
                                    <span class="title">Reports</span>
                                    <span class="arrow "></span>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a class="<?= (in_array($currentController, ['booked-session']) && $currentAction == 'expired' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/booked-session/expired'); ?>">
                                            <span class="title">Missed Lessons</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        <?php } else if (Yii::$app->user->identity->role == 'ADMIN') { ?>
                            <li class="<?= (in_array($currentController, ['tutor']) && $currentAction == 'index' ) ? 'open' : '' ?>">
                                <a href="<?= Yii::$app->getUrlManager()->createUrl('/tutor/index'); ?>">
                                    <i class="fa fa-user"></i>
                                    <span class="title">Tutors</span>
                                </a>
                            </li>
                            <li class="<?= (in_array($currentController, ['student','cancel-session']) && in_array($currentAction ,[ 'index','credit-lesson-admin','cancel-session']) ) ? 'open' : '' ?>">
                                <a href="javascript:;">
                                    <i class="fa fa-users"></i>
                                    <span class="title">Students</span>
                                    <span class="arrow "></span>
                                </a>
                                <ul class="sub-menu">
                                  <li>
                                      <a class="<?= (in_array($currentController, ['student']) && ($currentAction == 'index' || ($currentAction == 'student'))) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/student/index'); ?>">
                                          <span class="title">Student List</span>
                                      </a>
                                  </li>
                                  <li>
                                      <a class="<?= (in_array($currentController, ['cancel-session']) && ($currentAction == 'index')) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/cancel-session/index'); ?>">
                                          <span class="title">Cancel Lesson Request</span>
                                      </a>
                                  </li>
                                </ul>
                            </li>
                            <li class="<?= (in_array($currentController, ['video']) && in_array($currentAction, ['uploaded', 'published-video', 'review', 'upload']) ) ? 'open' : '' ?>">
                                <a href="javascript:;">
                                    <i class="fa fa-film"></i>
                                    <span class="title">Pre Recorded Videos</span>
                                    <span class="arrow "></span>
                                </a>
                                <ul class="sub-menu" >
                                    <li>
                                        <a class="<?= (in_array($currentController, ['video']) && ($currentAction == 'published-video' || ($currentAction == 'review' && Yii::$app->session['status'] == 'published')) ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/video/published-video'); ?>">
                                            <span class="title">Published Videos</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['video']) && ($currentAction == 'uploaded' || $currentAction == 'upload' || ($currentAction == 'review' && Yii::$app->session['status'] == 'uploaded') )) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/video/uploaded'); ?>">
                                            <span class="title">Uploaded Videos</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="<?= (in_array($currentController, ['video']) && in_array($currentAction, ['recorded', 'tution-view']) ) ? 'open' : '' ?>">
                                <a href="javascript:;">
                                    <i class="fa fa-video-camera"></i>
                                    <span class="title">Lesson Videos</span>
                                    <span class="arrow "></span>
                                </a>
                                <ul class="sub-menu" >

                                    <li>
                                        <a class="<?= (in_array($currentController, ['video']) && ($currentAction == 'recorded' || ($currentAction == 'tution-view' && Yii::$app->session['status'] == 'published') )) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/video/recorded'); ?>">
                                            <span class="title">Recorded Videos</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="<?= ((in_array($currentController, ['payment-term', 'online-tutorial-package']) && in_array($currentAction, ['index','create','update']))
                                            || (in_array($currentController, ['monthly-subscription-fee']) && in_array($currentAction, ['index','create','update']))    
                                            || (in_array($currentController, ['musicoin-coupon', 'coupon','musicoin-package']) && in_array($currentAction, ['index','create','update']))    
                                            
                                                ) ? 'open' : '' ?>">
                                <a href="javascript:;">
                                    <i class="fa fa-money"></i>
                                    <span class="title">Billing Configuration</span>
                                    <span class="arrow "></span>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a class="<?= (in_array($currentController, ['online-tutorial-package']) && in_array($currentAction, ['index','update','create']) ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/online-tutorial-package/index'); ?>">
                                            <span class="title">Online Tutorial Package</span>
                                        </a>
                                    </li>
                                     <li>
                                        <a class="<?= (in_array($currentController, ['monthly-subscription-fee']) && in_array($currentAction, ['index','create','update']) ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/monthly-subscription-fee/index'); ?>">
                                            <span class="title">Monthly Subscription Package</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['musicoin-package']) && in_array($currentAction, ['index','create','update']) ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/musicoin-package/index'); ?>">
                                            <span class="title">Musicoin Package</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['musicoin-coupon']) && in_array($currentAction, ['index','create','update']) ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/musicoin-coupon/index'); ?>">
                                            <span class="title">Musicoin Promo Code</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="<?= ((in_array($currentController, ['booked-session', 'book-music-tution', 'payment-transaction','student-view-pre-recorded-lesson','student-view-recorded-lesson']) && in_array($currentAction, ['index', 'history','sales-representative-commissions']) ) 
                                                || $currentController == 'tutor' && in_array($currentAction,['availability','report-tutor-list', 'tutor-plan-change-history']) 
                                                || ($currentController == 'student' && $currentAction == 'registration-report') 
                                                || ($currentController == 'monthly-subscription' && $currentAction == 'report')
                                                || ($currentController == 'video' && $currentAction == 'report')  
                                                || (in_array($currentController, ['musicoin']) && in_array($currentAction, ['index',]))    
                                                ) ? 'open' : '' ?>">
                                <a href="javascript:;">
                                    <i class="fa fa-list"></i>
                                    <span class="title">Reports</span>
                                    <span class="arrow "></span>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a class="<?= (in_array($currentController, ['message']) && $currentAction == 'report' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/message/report'); ?>">
                                            <span class="title">Messaging</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['musicoin-purchases']) && $currentAction == 'index' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/musicoin-purchases/index'); ?>">
                                            <span class="title">Musicoin Purchases</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['student-view-pre-recorded-lesson']) && $currentAction == 'index' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/student-view-pre-recorded-lesson/index'); ?>">
                                            <span class="title">Pre Recorded videos View history</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['student-view-recorded-lesson']) && $currentAction == 'index' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/student-view-recorded-lesson/index'); ?>">
                                            <span class="title">Recorded Lessons View History</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['booked-session']) && $currentAction == 'index' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/booked-session/index'); ?>">
                                            <span class="title">All Lessions History</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['tutor']) && $currentAction == 'availability' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/tutor/availability'); ?>">
                                            <span class="title">Tutor Availability</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['book-music-tution']) && $currentAction == 'history' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/book-music-tution/history'); ?>">
                                            <span class="title">Lessons History</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['book-music-tution']) && $currentAction == 'sales-representative-commissions' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/book-music-tution/sales-representative-commissions'); ?>">
                                            <span class="title">Sales Report</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['student']) && $currentAction == 'registration-report' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/student/registration-report'); ?>">
                                            <span class="title">Registrations Report</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['monthly-subscription']) && $currentAction == 'report' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/monthly-subscription/report'); ?>">
                                            <span class="title">Subscriptions Report</span>
                                        </a>
                                    </li>
				    <li>
                                        <a class="<?= (in_array($currentController, ['video']) && $currentAction == 'report' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/video/report'); ?>">
                                            <span class="title">Uploaded Videos Report</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['tutor']) && $currentAction == 'report-tutor-list' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/tutor/report-tutor-list'); ?>">
                                            <span class="title">Tutors Listing</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['tutor']) && $currentAction == 'tutor-plan-change-history' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/tutor/tutor-plan-change-history'); ?>">
                                            <span class="title">Tutor Plan Changes History</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['musicoin']) && in_array($currentAction, ['index',]) ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/musicoin/index'); ?>">
                                            <span class="title">Musicoin History</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                        <?php } else if (Yii::$app->user->identity->role == 'SUBADMIN') { ?>
                            <li class="<?= (in_array($currentController, ['tutor']) && $currentAction == 'index' ) ? 'open' : '' ?>">
                                <a href="<?= Yii::$app->getUrlManager()->createUrl('/tutor/index'); ?>">
                                    <i class="fa fa-user"></i>
                                    <span class="title">Tutors</span>
                                </a>
                            </li>
                            <li class="<?= (in_array($currentController, ['student','cancel-session']) && in_array($currentAction ,[ 'index','credit-lesson-admin','cancel-session']) ) ? 'open' : '' ?>">
                                <a href="javascript:;">
                                    <i class="fa fa-users"></i>
                                    <span class="title">Students</span>
                                    <span class="arrow "></span>
                                </a>
                                <ul class="sub-menu">
                                  <li>
                                      <a class="<?= (in_array($currentController, ['student']) && ($currentAction == 'index' || ($currentAction == 'student'))) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/student/index'); ?>">
                                          <span class="title">Student List</span>
                                      </a>
                                  </li>
                                  <li>
                                      <a class="<?= (in_array($currentController, ['cancel-session']) && ($currentAction == 'index')) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/cancel-session/index'); ?>">
                                          <span class="title">Cancel Lesson Request</span>
                                      </a>
                                  </li>
                                </ul>
                            </li>

                            <li class="<?= (in_array($currentController, ['video']) && in_array($currentAction, ['uploaded', 'published-video', 'review', 'upload']) ) ? 'open' : '' ?>">
                                <a href="javascript:;">
                                    <i class="fa fa-film"></i>
                                    <span class="title">Pre Recorded Videos</span>
                                    <span class="arrow "></span>
                                </a>
                                <ul class="sub-menu" >
                                    <li>
                                        <a class="<?= (in_array($currentController, ['video']) && ($currentAction == 'published-video' || ($currentAction == 'review' && Yii::$app->session['status'] == 'published')) ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/video/published-video'); ?>">
                                            <span class="title">Published Videos</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['video']) && ($currentAction == 'uploaded' || $currentAction == 'upload' || ($currentAction == 'review' && Yii::$app->session['status'] == 'uploaded') )) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/video/uploaded'); ?>">
                                            <span class="title">Uploaded Videos</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="<?= (in_array($currentController, ['video']) && in_array($currentAction, ['recorded', 'tution-view']) ) ? 'open' : '' ?>">
                                <a href="javascript:;">
                                    <i class="fa fa-video-camera"></i>
                                    <span class="title">Lesson Videos</span>
                                    <span class="arrow "></span>
                                </a>
                                <ul class="sub-menu" >
                                    <?php /*<li>
                                        <a class="<?= (in_array($currentController, ['video']) && ($currentAction == 'published-recorded' || ($currentAction == 'review-recorded' && Yii::$app->session['status'] == 'published-recorded')) ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/video/published-recorded'); ?>">
                                            <span class="title">Published Videos</span>
                                        </a>
                                    </li>
                                    */?>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['video']) && ($currentAction == 'recorded' || ($currentAction == 'tution-view' && Yii::$app->session['status'] == 'published') )) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/video/recorded'); ?>">
                                            <span class="title">Recorded Videos</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
			    <li class="<?= (in_array($currentController, ['instrument', 'instrument-category']) && in_array($currentAction, ['index']) ) ? 'open' : '' ?>">
                                <a href="javascript:;">
                                    <i class="fa fa-list"></i>
                                    <span class="title">Configuration</span>
                                    <span class="arrow "></span>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a class="<?= (in_array($currentController, ['instrument']) && $currentAction == 'index' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/instrument/index'); ?>">
                                            <span class="title">Instruments</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['instrument-category']) && $currentAction == 'index' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/instrument-category/index'); ?>">
                                            <span class="title">Categories</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="<?= ((in_array($currentController, ['booked-session', 'book-music-tution', 'payment-transaction','student-view-pre-recorded-lesson','student-view-recorded-lesson']) && in_array($currentAction, ['index', 'history'])) || ($currentController == 'tutor' && in_array($currentAction, ['availability','report-tutor-list', 'tutor-plan-change-history']) ) || ($currentController == 'video' && $currentAction == 'report') || ($currentController == 'online-tutorial-package' && $currentAction == 'get-rate-option-history') ) ? 'open' : '' ?>">
                                <a href="javascript:;">
                                    <i class="fa fa-list"></i>
                                    <span class="title">Reports</span>
                                    <span class="arrow "></span>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a class="<?= (in_array($currentController, ['student-view-pre-recorded-lesson']) && $currentAction == 'index' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/student-view-pre-recorded-lesson/index'); ?>">
                                            <span class="title">Pre Recorded videos View history</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['student-view-recorded-lesson']) && $currentAction == 'index' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/student-view-recorded-lesson/index'); ?>">
                                            <span class="title">Recorded Lessons View History</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['booked-session']) && $currentAction == 'index' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/booked-session/index'); ?>">
                                            <span class="title">All Lessions History</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['tutor']) && $currentAction == 'availability' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/tutor/availability'); ?>">
                                            <span class="title">Tutor Availability</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['book-music-tution']) && $currentAction == 'history' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/book-music-tution/history'); ?>">
                                            <span class="title">Lessons History</span>
                                        </a>
                                    </li>
			            <li>
                                        <a class="<?= (in_array($currentController, ['video']) && $currentAction == 'report' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/video/report'); ?>">
                                            <span class="title">Uploaded Videos Report</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['tutor']) && $currentAction == 'report-tutor-list' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/tutor/report-tutor-list'); ?>">
                                            <span class="title">Tutors Listing</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="<?= (in_array($currentController, ['tutor']) && $currentAction == 'tutor-plan-change-history' ) ? 'active' : '' ?>" href="<?= Yii::$app->getUrlManager()->createUrl('/tutor/tutor-plan-change-history'); ?>">
                                            <span class="title">Tutor Plan Changes History</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        <?php } ?>

                    </ul>

                </div>
                <!-- MAIN MENU - END -->



                <div class="project-info">

                    <div class="team-info"><span style="color: rgba(250, 133, 100, 1.0);  margin-left: 36px;"><i class="fa fa-clock-o"> </i> <?= \app\libraries\General::displayDateTime(date('d-m-Y H:i:s '),false,true); ?></span></div>



                    <!--                    <div class="block1">
                                            <div class="data">
                                                <span class='title'>New&nbsp;Orders</span>
                                                <span class='total'>2,345</span>
                                            </div>
                                            <div class="graph">
                                                <span class="sidebar_orders">...</span>
                                            </div>
                                        </div>

                                        <div class="block2">
                                            <div class="data">
                                                <span class='title'>Visitors</span>
                                                <span class='total'>345</span>
                                            </div>
                                            <div class="graph">
                                                <span class="sidebar_visitors">...</span>
                                            </div>
                                        </div>-->

                </div>



            </div>
            <!--  SIDEBAR - END -->

            <!-- START CONTENT -->
            <section id="main-content" class=" ">
                <section class="wrapper main-wrapper" style=''>

                    <!-- MIDDLE CONTENT START -->
                    <div class='col-xl-12 col-lg-12 col-md-12 col-12'>
                        <div class="page-title page-title-custom">

                            <div class="float-left">
                                <h1 class="title"><?= $this->title; ?></h1>
                            </div>
                            <!--        <div class="float-right d-block">
                                                <ol class="breadcrumb">
                                                    <li>
                                                        <a href="index.html"><i class="fa fa-home"></i>Home</a>
                                                    </li>
                                                    <li>
                                                        <a href="form-elements.html">Forms</a>
                                                    </li>
                                                    <li class="active">
                                                        <strong>Form Elements</strong>
                                                    </li>
                                                </ol>
                                            </div>-->
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    
                    <?php 
                    if(!empty(Yii::$app->session['custom_alert']) && !empty(Yii::$app->session['custom_alert']['message'])){ ?>
                    <div class='col-xl-12 col-lg-12 col-md-12 col-12 margin-top-10'>
                        <div class="alert alert-<?= Yii::$app->session['custom_alert']['type'];?>" role="alert">
                            <?= Yii::$app->session['custom_alert']['message']; ?>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    </div>
                    <?php unset(Yii::$app->session['custom_alert']); } ?>
                    
                    
                    <?= $content ?>
                    <!-- MIDDLE CONTENT END -->

                </section>
            </section>
            <!-- END CONTENT -->

            <div class="page-chatapi hideit">

                <div class="search-bar">
                    <input type="text" placeholder="Search" class="form-control">
                </div>

                <div class="chat-wrapper">
                    <h4 class="group-head">Groups</h4>
                    <ul class="group-list list-unstyled">
                        <li class="group-row">
                            <div class="group-status available">
                                <i class="fa fa-circle"></i>
                            </div>
                            <div class="group-info">
                                <h4><a href="#">Work</a></h4>
                            </div>
                        </li>
                        <li class="group-row">
                            <div class="group-status away">
                                <i class="fa fa-circle"></i>
                            </div>
                            <div class="group-info">
                                <h4><a href="#">Friends</a></h4>
                            </div>
                        </li>

                    </ul>


                    <h4 class="group-head">Favourites</h4>
                    <ul class="contact-list">

                        <li class="user-row" id='chat_user_1' data-user-id='1'>
                            <div class="user-img">
                                <a href="#"><img src="/data/profile/avatar-1.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Clarine Vassar</a></h4>
                                <span class="status available" data-status="available"> Available</span>
                            </div>
                            <div class="user-status available">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row" id='chat_user_2' data-user-id='2'>
                            <div class="user-img">
                                <a href="#"><img src="/data/profile/avatar-2.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Brooks Latshaw</a></h4>
                                <span class="status away" data-status="away"> Away</span>
                            </div>
                            <div class="user-status away">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row" id='chat_user_3' data-user-id='3'>
                            <div class="user-img">
                                <a href="#"><img src="/data/profile/avatar-3.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Clementina Brodeur</a></h4>
                                <span class="status busy" data-status="busy"> Busy</span>
                            </div>
                            <div class="user-status busy">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>

                    </ul>


                    <h4 class="group-head">More Contacts</h4>
                    <ul class="contact-list">

                        <li class="user-row" id='chat_user_4' data-user-id='4'>
                            <div class="user-img">
                                <a href="#"><img src="/data/profile/avatar-4.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Carri Busey</a></h4>
                                <span class="status offline" data-status="offline"> Offline</span>
                            </div>
                            <div class="user-status offline">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row" id='chat_user_5' data-user-id='5'>
                            <div class="user-img">
                                <a href="#"><img src="/data/profile/avatar-5.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Melissa Dock</a></h4>
                                <span class="status offline" data-status="offline"> Offline</span>
                            </div>
                            <div class="user-status offline">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row" id='chat_user_6' data-user-id='6'>
                            <div class="user-img">
                                <a href="#"><img src="/data/profile/avatar-1.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Verdell Rea</a></h4>
                                <span class="status available" data-status="available"> Available</span>
                            </div>
                            <div class="user-status available">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row" id='chat_user_7' data-user-id='7'>
                            <div class="user-img">
                                <a href="#"><img src="/data/profile/avatar-2.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Linette Lheureux</a></h4>
                                <span class="status busy" data-status="busy"> Busy</span>
                            </div>
                            <div class="user-status busy">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row" id='chat_user_8' data-user-id='8'>
                            <div class="user-img">
                                <a href="#"><img src="/data/profile/avatar-3.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Araceli Boatright</a></h4>
                                <span class="status away" data-status="away"> Away</span>
                            </div>
                            <div class="user-status away">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row" id='chat_user_9' data-user-id='9'>
                            <div class="user-img">
                                <a href="#"><img src="/data/profile/avatar-4.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Clay Peskin</a></h4>
                                <span class="status busy" data-status="busy"> Busy</span>
                            </div>
                            <div class="user-status busy">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row" id='chat_user_10' data-user-id='10'>
                            <div class="user-img">
                                <a href="#"><img src="/data/profile/avatar-5.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Loni Tindall</a></h4>
                                <span class="status away" data-status="away"> Away</span>
                            </div>
                            <div class="user-status away">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row" id='chat_user_11' data-user-id='11'>
                            <div class="user-img">
                                <a href="#"><img src="/data/profile/avatar-1.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Tanisha Kimbro</a></h4>
                                <span class="status idle" data-status="idle"> Idle</span>
                            </div>
                            <div class="user-status idle">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row" id='chat_user_12' data-user-id='12'>
                            <div class="user-img">
                                <a href="#"><img src="/data/profile/avatar-2.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Jovita Tisdale</a></h4>
                                <span class="status idle" data-status="idle"> Idle</span>
                            </div>
                            <div class="user-status idle">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>

                    </ul>
                </div>

            </div>
            <div class="chatapi-windows ">

            </div>

        </div>

        <!-- modal start -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="ultraModal-Label" aria-hidden="true">
            <div class="modal-dialog animated zoomIn">
                <div class="modal-content">
                    <div class="modal-header">
                        <div id="title-demo"></div>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div id="video-content"></div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <!--<button class="btn btn-success" type="button">Save changes</button>-->
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="tutorUnavailability" tabindex="-1" role="dialog" aria-labelledby="ultraModal-Label" aria-hidden="true">
            <div class="modal-dialog animated zoomIn">
                <div class="modal-content">
                    <div class="modal-header">
                        <div id="title-tutorUnavailability"></div>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div id="unavailability-content"></div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="tutorAvailabilityCalender" tabindex="-1" role="dialog" aria-labelledby="ultraModal-Label" aria-hidden="true">
            <style>#tutorAvailabilityCalender-content > section.box { margin: 0;}</style>
            <div class="modal-dialog animated zoomIn " style="max-width:90%;">
                <div class="modal-content border-0">
                    <div class="modal-header bg-primary">
                        <h4 id="title-tutorAvailabilityCalender" class="modal-title bg-primary"></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div id="tutorAvailabilityCalender-content"></div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="studentBookingOption" tabindex="-1" role="dialog" aria-labelledby="ultraModal-Label" aria-hidden="true">
            <div class="modal-dialog animated zoomIn">
                <div class="modal-content">
                    <div class="modal-header bg-orange" style="    border-radius: 0;">
                        <h4 class="text-white" id="title-studentBookingOption">Choose your booking option</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div id="studentBookingOption-content" class="modal-body">

                        <div class="card credit_card_custom_css">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-6 text-center" style="border-right: 1px solid #bbb8b8;">
                                        <p> </p>
                                        <a class="btn btn-purple btn-icon redirect_booking_btn" href="javascript:void(0)" data-href="<?= Yii::$app->params['base_url'] . 'book-music-tution'; ?>" >
                                            Book a New Lesson
                                        </a>
                                    </div>
                                    <div class="col-lg-6 text-center" >
                                        <p> </p>
                                        <a class="btn btn-orange btn-icon redirect_booking_btn" href="javascript:void(0)" data-href="<?= Yii::$app->params['base_url'] . 'book-music-tution/credit-schedule'; ?>" >
                                            Book a Credit Lesson
                                        </a>
                                        <!--<div class="clearfix"></div>-->
                                        <p>You have <span id="model_credit_session_count">0</span> credit Lessons.</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        
        <div class="modal fade " id="credit_debit_modal" tabindex="-1" role="dialog" aria-labelledby="ultraModal-Label" aria-hidden="true">
            <div class="modal-dialog animated zoomIn " >
                <div class="modal-content ">
                <div class="modal-header bg-info " style="border-radius: none;">
                    <h4 class="modal-title text-white">Credit/Debit  Musicoins</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" id="credit_debit_model_body"> </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default pull-left" type="button">Close</button>
                    <?= Html::Button(Yii::t('app', 'Save'), ['class' => 'btn btn-success', 'id' => 'credit_debit_save_btn']) ?>
                </div>
            </div>
            </div>     
        </div>

<!-- SCRIPTS -->

<?php 

// $this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/js/jquery-3.2.1.min.js?v='.date("dmHis"));
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/datatables/js/datatables.min.js?v='.date("dmHis"));
$this->registerJsFile("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js?v=".date("dmHis"));
$this->registerJsFile("https://cdn.datatables.net/plug-ins/1.10.11/sorting/datetime-moment.js?v=".date("dmHis"));
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/datatables/extensions/Buttons/js/dataTables.buttons.min.js?v='.date("dmHis"));
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/datatables/extensions/Buttons/js/buttons.flash.min.js?v='.date("dmHis"));
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/datatables/extensions/Buttons/js/buttons.html5.min.js?v='.date("dmHis"));
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/datatables/extensions/Buttons/js/buttons.print.min.js?v='.date("dmHis"));
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/messenger/js/messenger.min.js?v='.date("dmHis"));
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/messenger/js/messenger-theme-future.js?v='.date("dmHis"));
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/messenger/js/messenger-theme-flat.js?v='.date("dmHis"));
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/js/messenger.js?v='.date("dmHis"));
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/calendar/moment.min.js?v='.date("dmHis"));
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/jquery-ui/smoothness/jquery-ui.min.js?v='.date("dmHis"));
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/calendar/fullcalendar.min.js?v='.date("dmHis"));
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/js/jquery.form.js?v='.date("dmHis"));
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/jquery-validation/js/jquery.validate.min.js?v='.date("dmHis"));
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js?v='.date("dmHis"));
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/js/form-validation.js?v='.date("dmHis"));
$this->registerJsFile("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js?v=".date("dmHis"));

foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
    if ($key == 'success') {
        $this->registerJs("showSuccess('$message');");
    } else {
        $this->registerJs("showErrorMessage('$message');");
    }
}

$page_length = (Yii::$app->params['datatable']['page_length'] > 0 ) ? Yii::$app->params['datatable']['page_length'] : 10;
$this->registerJs("
        var dt_page_length = " . $page_length . ";
        var web_url = " . Url::to(['/']) . ";
       ", View::POS_HEAD);

JSRegister::begin(); ?>
<script>
$("form").on("beforeValidate", function (event, messages, deferreds) {
    blockBody("Loading...");
}).on("afterValidate", function (event, messages,errorAttributes) {
    if (errorAttributes.length) {
        unblockBody();
    }
}).on("beforeSubmit", function () {
    blockBody("Loading...");
});


// websocket
var wsStatus = "OFFLINE";
WS = new WebSocket('<?= $websocket_url ?>?id=<?= $user_uuid ?>&role=<?= $user_role ?>');
WS.onopen = function(e) {
    wsStatus = "ONLINE";
    // console.log("WS CONNECTED", wsStatus);
};

WS.onmessage = function(e) {
    var message = JSON.parse(e.data);
    // console.log(message);
    if (message.type == "message"){
        appendBalloon(message.content, false);
    } else if (message.type == "attachment") {
        appendAttachmentBalloon(message.content, false)
    } else if (message.type == "unseen") {
        $(".message-btn .badge").html(message.count);
    }
};

WS.onclose = function() {
    wsStatus = "OFFLINE";
    // console.log("WS DISCONNECTED", wsStatus);
}

$(document).ready(function(){
    $(".balloons-container").animate({
        scrollTop: 99999
    }, 100);
})


// send message
$("#message-form").on("submit",function(e){
    e.preventDefault();
});
$(".message-input").on("keypress", function(e){
    if (e.keyCode === 10 || e.keyCode === 13) {
        e.preventDefault();
        var form = $("#message-form");
        var formData = form.serialize();
        var url = form.attr("action");
        var content = $(this).val().trim();
        if (content.length > 0) {
            // console.log("WS STATUS", wsStatus);
            if(wsStatus == "OFFLINE"){
                // send api
                sendOffline(url, formData);
            } else {
                // send ws
                appendBalloon($(this).val(),true);
                WS.send(JSON.stringify(formData));
                $(".message-input").val("");
                unblockBody();
            }
        }
        $(this).val("");
    }
});


function sendOffline(url, formData) {
    $.ajax({
        url: url,
        type: "POST",
        data: formData,
        processData: false,
        enctype: 'multipart/form-data',
        success: function(data) {
            // console.log("SENT", data);
            unblockBody();
        },
        error: function(e) {
            console.error("ERROR",e);
            unblockBody();
        }
    });
}

function escapeHtml(text) {
  var map = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#039;'
  };
  
  return text.replace(/[&<>"']/g, function(m) { return map[m]; });
}

function appendBalloon(content, self) {
    let params = new URLSearchParams(window.location.search);
    if (params.has("to")) {
        content = escapeHtml(content);
        var avatar = "<img src='<?= $defaultAvatar ?>' class='avatar'/>";
        var balloon = "<div class='message-wrapper "+(self? 'self':'')+"'><div class='balloon'>"+(self?'':avatar)+" "+(content)+"</div></div>";
        $(".balloons-container").append(balloon).animate({
            scrollTop: 99999
        }, 100);
    }
}

function appendAttachmentBalloon(content, self) {
    let params = new URLSearchParams(window.location.search);
    if (params.has("to")) {
        var avatar = "<img src='<?= $defaultAvatar ?>' class='avatar'/>";
        var balloon = "<div class='message-wrapper "+(self? 'self':'')+"'><div class='balloon'>"+(self?'':avatar)+" <a target='_blank' href='<?= $attachment_url ?>"+content+"'>"+content+"</a></div></div>";
        $(".balloons-container").append(balloon).animate({
            scrollTop: 99999
        }, 100);
    }
}
 
setInterval(() => {
    if (wsStatus == "ONLINE") {
        var data = {
            Message: {
                type: "getUnseen",
                role: "<?= $user_role ?>",
                id: "<?= $user_uuid ?>"
            }
        };
        WS.send($.param(data));
    }
}, 5000);
</script>
<?php JSRegister::end(); ?>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
