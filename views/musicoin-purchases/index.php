<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Musicoin Purchases');
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs('', View::POS_END);
?>


<div class="col-xl-12">
    <section class="box ">

        <header class="panel_header">
        <h2 class="title float-left">Musicoin Purchases</h2>
        </header>

        <div class="content-body"> 
            <div class="row" id="coupon_table_parent_div">
                <div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">

                    <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Student ID</th>
                                <th>Student Name</th>
                                <th>Transaction ID</th>
                                <th>Transaction Date</th>
                                <th>Package</th>
                                <th class="text-right">Income</th>
                                <th class="text-right">GST</th>
                                <th class="text-right">Transaction Total</th>
                                <th>Musicoins</th>
                                <th>Bonus Coins</th>
                                <th class="text-right">Discount</th>
                                <th>Creative Kids Voucher Code</th>
                                <th>Voucher Kid's Name</th>
                                <th>Voucher Kid's Date of Birth</th>
                                <th>Voucher Kid's Post Code</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($collection as $data){ 
                                $transactionDate = date("Y-m-d", strtotime($data["payment_datetime"]));
                                $discount = $data->studentMusicoinPackageUu["bonus_coins"] * 0.5;
                                $discount = number_format($discount, 2);
                            ?>
                                <tr>
                                    <td><?= $data->studentUu->enrollment_id ?></td>
                                    <td><?= $data->studentUu->first_name . ' ' . $data->studentUu->last_name ?></td>
                                    <td><?= $data["transaction_number"]?></td>
                                    <td><?= $transactionDate ?></td>
                                    <td><?= $data->studentMusicoinPackageUu["name"] ?></td>
                                    <td class="text-right">$<?= $data->studentMusicoinPackageUu["price"] ?></td>
                                    <td class="text-right">$<?= $data->studentMusicoinPackageUu["gst"] ?></td>
                                    <td class="text-right">$<?= $data->studentMusicoinPackageUu["total_price"] ?></td>
                                    <td><?= $data->studentMusicoinPackageUu["coins"] ?></td>
                                    <td><?= $data->studentMusicoinPackageUu["bonus_coins"] ?></td>
                                    <td class="text-right">$<?= $discount ?></td>
                                    <td><?= $data->studentMusicoinPackageUu["creative_kids_voucher_code"] ?></td>
                                    <td><?= $data->studentMusicoinPackageUu["kids_name"] ?></td>
                                    <td><?= $data->studentMusicoinPackageUu["kids_dob"] ?></td>
                                    <td><?= $data->studentMusicoinPackageUu["kids_postal_code"] ?></td>
                                </tr>
                            <?php } ?>                        
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </section>
</div>