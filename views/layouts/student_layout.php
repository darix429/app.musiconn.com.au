<?php
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\NewAppAsset;
use yii\helpers\Url;
use yii\web\View;
use app\models\Tutor;
use app\models\Student;
use richardfan\widget\JSRegister;

NewAppAsset::register($this);

$page_length = (Yii::$app->params['datatable']['page_length'] > 0 ) ? Yii::$app->params['datatable']['page_length'] : 10;
$this->registerJs("
        var dt_page_length = " . $page_length . ";
        var web_url = '" . Url::to(['/']) . "';
", View::POS_HEAD);

$profile_url = Url::to(['student/profile']);
$currentController = Yii::$app->controller->id;
$currentAction = Yii::$app->controller->action->id;

$websocket_url = Yii::$app->params["websocket_url"];
$user_role = Yii::$app->user->identity->role;
$user_uuid = null;
if ($user_role == "STUDENT") {
    $user_uuid = Yii::$app->user->identity->student_uuid;
} else if ($user_role == "TUTOR") {
    Yii::$app->user->identity->tutor_uuid;
}

$attachment_url = Yii::$app->params["messages"]["attachment_url"];
$defaultAvatar = "data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAABC2wAAQtsBUNrbYgAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAABc7SURBVHja7Z15VJX3mcfjkphqzDSLc6bJONOmnjaTmWSm00wSE5tJ58TYSZzanp64ECMBVzR6YoyRWLVsbhi1ghoXgkajYlzqQhRFBQFBCYuAiggooFy8LO7gguI8T/JDQblw33vf/ff943OOiQnc+/ye7+e+931/ywO3b99+AFibdz8IeoR4gfgjMYGYQywkoog1xCZiB7GPSCOyieNEKeEkLgn4nw8TCcRm8f/zz5pMBBADiT7ES0QPoivqb21QBGsEvB3xFNGL8CWCia+JVBHg2wZSKuQSLl7br4nOGDcIAHge+GdEmPgTOI+oMzjkSrlFFBFbiemEj7hC6YTxhQDA/Z/uHI4xRAxRbrGwK6GeOCiuYF4lOqAHIADZAv8Q0ZP4lNhOnLNx4Nvigrg/MYL4KfoDArBr6LuIG2hbLHg5rycFRATxDtcMvQMBWP2Tvh+xjriCcCvmunhqMZZ4HD0FAVgh9B2I3kQ0cR4hVo1r4v7IW0R79BoEYLabeL3Ec3cnwqrLY8dg3DOAAIwO/qPiRl4pQmkIDcQeYhDxMHoSAtAr+DwhZzZxESE0DefEFdiv0KMQgFbBf1ZMzLmOwJmaeOI19CwEoFbwe4rHdw0Il+VE8Cp6GALw9MZeXyIZQbI8uyECCEBJ+P9EHEVwbMcuvppDj0MAroL/b8ReBMX2xBGvoOchgMbgP0ZEEjcRDulE8BwEIG/w2xOjiGqEQVpuEKEyL1OWNfyvi11xEALQuAjpDQjA/sHvLuaUo+lBS0TLtvBIluB3JKYQtWhy0Aa8nsMHArDX9loH0djAg5uEP4MArB3+wWK3WzQ08AS+YpzIV5AQgLWC35VYjQYGKsFXkN0hAGuE/2WiGE0LVKaKN3uBAMz9XH+y2HkWDQu02u6cbya3gwDMFf6nxWk2aFKgB7E8gxQCMEf4eePNGjQl0JmTdtiAxOrhn4R1+sBArhL+EIAxu+8uQgMCkxBl1X0JrRj+zuLMOTQeMBOZxD9CANqGvxtm9QETU8LHpkMA2oSfz6MvRJMBk1PBm8tAAOpP7qlCcwGLwE+lXoIA1HvMhwM1gdW4ZIU9Bswe/tFi9hUaClj1MeHbEIBn4f8IDQRssu1YfwhAWfiHYoIPsNkagqEQgHvh74/LftWb71J/v+CzA4eGlLw3Iix/SMDMnKFjwzNGfTwvjeE/87/jv+P/hv9b8R0W46Ae/IH2EQTQevjfFpdMaBgFUGCrfUfPzJ4wdXHiX5du3Lt9d2pa7vGTxWUVzgsV1TX1xG0PqeefwT+Lfyb/bP4d/Lv4d6L2HjEaAmg5/G+ImyZokta55v9heOb8JRsT96Vk5RSWnqn2IuBewb+bXwO/Fn5N/NowPm5dkfWDAJqH/yVs3eWaQcNCT34Wujxx264D6aUOZ61RgW8Lfm38Gvm18mvG2LmEH2u/DAHcPZILy3nvv6w/FzR7ZUJOPl3JmzTwbcGvnd8DvxeMaYs7DPWQWgBiem8FmuEuPsNCi5atik2i7951Vg3+vfB74ffE7w1j3Aye2t5NSgHwyimxeAKNQPiNmZ317Z6DGXYJvSv4PfJ7xZg323C0s1QC4LXTYvkkLvX9giu/3hCfbPfg38vqDbtT6L1jfccP8PL2DjIJIAqDHtQwYerifcWnHTWyhb+R4rLycx9PWbwfk76+Z5EUAuAtlGQf7AH+ISUJB7K/kzX497I3JfMw1eQMJBA0ydYC4E0UZX/W//7IGTknSs44EPzmFJwsqxw8csYxzBbUd46AnuF/TOykKu0Aj/x4XkpZhfMyAu9yHkHdyPFz07GXQNDTthIAH6Qg9lKXdmAnh0XtcVTV3ETQW8dRVX3rs9DlyZJLgM+4aG8nAUyReUA/mfrFPoRbGR9PWZwquQQm20IA9EZ6y7yqbOi4OQfLK6tvINTKOOOsujl0bHi2xAKo12O6sNbh7y7zXn4+w8PyS8rPXkSgPePkaccVqqHMswf5kNuulhQAvfCOkm/hffFoYclpBNk78gpO8lTxKxL30WqrCmCizN/hlqzcloAAq8PiFVtTJL8fMNhSAqAX/DOiVtYB8w2YmeOoqrmF8Kr2ZKBhSMDMPMl3GH7GSgKIk3iwrmfmFRQhuOqSkVvA34evS75oqKPpBUAv0kfmy7VxgZEpCKw2jAuMkP2rwBRTC4Be4OOEU+ZB2puSlY2wasOe5MwcyQXAX6u7m1kA0ZJv3VWIoGoLthoLijGlAMSmnlKv5opcvnkvQqotEcs27ceqwaDXTSUAekGdiALZN3osPu2oRkg134n4PHYfDspWa62AWgIIld3K4wIjExFQfRg7KSIVVwFBo0whAHohz+Ewj6Db2OBDP+KTMvIggCA+mOUxMwggDoMRdJ4aExN/9JsY1Li3vux9F2moAOgFvIJBCLo9JGBmJoKpL1Tzo+i9oJt8roaRAsCnP/Fp0FLc/deZidOWHEDvfc9eQwRAv7gniv8Dy1bHQgA6s2TlNgjgLn8yQgC7UPgfSDqYg9l/OrMvJSsfvXcH/jrUTjcB0C97FUW/+z2srMKJTT/03ywEJ0k3p6+eAtiNgt852ceJQBoD1f4ievAOyboIAJ/+9x/thTAaJoAL6MFm9NRDAPEoNARgEgHg2PHmbNFUAPQLXkORIQATCaAGPXjfyULPaikAfPpDAGYSQDV68D6iNBGAONcPBYYAzCSASvTg/VvSEU9pIYCFKC4EAAFYgtmqCoB+4MMEbrhAABCARc6kIB5VUwCDUFQIAAKwFJ+qKYA9KCgEAAFYilJ3pge7E/6fiscLKCoEAAFYi15qCCAYhYQAIABLstArAfDGg+JSAsWEACAA68FndHTwRgBvoYgQAARgaXp7I4AYFBACgAAsTbRHAhDHfF1DASEACMDaG9YSD3kigLEoHgQAAdiCfp4IYB8KBwFAALZgnSIB0P/Q5V25z2KHACAAO3GFM61EAO+gaBAABGArBioRQAQKBgFAAPbfLciVAApQMAgAArAVdS09DXA19x8FgwAgAAk2DW1JACNQKAgAApBjiXBLAtiEQkEAEIAt2d6qAHjhAIG91iEACMCenLt3jwAc+gEBQABy8UJrAsDafwgAArA3Y1oTwEEUCAKAAGxNTIsCoL/oRNSjQBAABGBryl0J4AUUxyMB4HRg4wTgRA96xDMtCcAHhVHO8tXf7kcYjWH56thk9KBH+LYkgOkojDLCI9ftQRCNZdaCtZCAcqJaEsBWFMZ9fANmZlMDNiCExuKoquGxOIKeVEReSwIoQmHc5ubBrGPHEUBzkJZ59ASPCfpS0cKgdncEQP/QmbiFwrhHwIT5aQieueAxQW8q4qmmAvg1CuI+MVv2pSJ05oLHBL2p/NSgRgH4oiBuc63U4axF6MwFjwl2sVb+JKBRAOEoiHsMGhZ6EoEzJzw26FG3CW4qgB0oiHv4fxieibCZEx4b9KjbfN1UADj/z03GBUamIGzmhMcGPeo2qd8LgP7QFcVwn48mL4QATAqPDXrUbZyNAuiBYkAAEICUPMICeAmFgAAgADk3B2EB9EEhIAAIQEr+yAIYiEJAABCAlExgAQSgEBAABCAlc1gAk1EICAACkJKFLIA5KAQEAAHIuS8ACyAKhYAAIAApWcMC2IxCQAAQgJRsYgEkoBAQAAQgJTtYAIdRCAgAApCSfVgIBAFAAPKSxgK4hEIoEkAywgYB2IRsFgBOA8YVAAQgJ8dZAA4UAgKAAKSkFNuBK94QJAICMCljJ0VAAAr3BGAB5KIQ7vPB6FmHETZz8v6oGTggRBmXWADYT10BA/xDyhE2c0Ljcxk9qoiLLIC9KIQibpVVOK8icObiyIlT5ehNxZxiAWxHIRQeDPI3HAxiNjbvSD6E3lRMJgtgPQqhdGvw2VkInbnwDZiZg95UzG4WQDQKoZiGLLrmRPDMwZadKenoSY+IYQEsRCGUQ584ueWVVfUIoNHHg1ff8hkeVoie9IjFOBbMC0LmfJWAEBoLjwF60WNCWQBBKITnXwWi1+7E2gCDmDZzRSJ60CvGswAmohDeMXVmdCJfiiKUel321zQEhizbj97zmiEsgDEohAoSmBG9D+HUPvibYpMODR45/Rh6ThX6sgB8UQjvGegfUoqQavqcP+O94WEn0Guq0pMF8BsUQh2yjpwoRFjV51R5RR3Vtw49pjq/ZAF0QyHUIXL5ZjwV0IBV3+w6iP7ShCcf4COC6Q/nUAzvGTZuTjoCqz5Dx4Zj30oNnmARHRoFkIqCqEJdWYWzDqFVj2NFpdW8AAu9pTrVnP1GAWA6sErExqdlILjqMX/JBmzyoQ2JTQUwCQVRhwlTFychuOrx3ogw7FilDQuaCqAfCqIal0sdzlqE13tSM44g/Nrh11QAz6Ig6vHV+l3YN1CdTT5xb0o7/rOpAB4k6lEU7BtoFvKLS6uoltfRT5rAWe90RwBCAsdRGPUesRw+VoSZgV4Q+vkqzPXXjrzG3DcVwFYURs2lwqswKchDSh3Ouv5+wZiboh2rWxLAbBRGPaiBnZgT4BlLVm5LQg9pyictCcAfhVGXiGWbEhFo5Sv+Bg0LPYX+0ZQ3WxLAqyiMJlcBeCSogDWb9hxA72hOt5YE0Jm4huKoy4KlG3EvwE1OOyuvDxwaUoa+0ZTyxsw3E4CQQDwKpPpVQCVdBVxBwNuGvzKhZzRnR2sC+AQFUp/5SzZit6A2OHnacZFkWYN+0ZwZrQngeRRIm6uAUofzMoLumuBw7O6rE39wKQAhAQeKpMFVwBcb9iLorpb8llRgxx9d4Ht8XdoSwEoUClcBejIuMAJLfg34/u9KAINQKG2Yu/ibeAS+Odt3p2agN3QjwB0BPCm2C0LB1L8KqCoqLa9C8O9s9lk7wD8EXzn1o3ubAhAS+A7F0oaxkyKwYYhgUtBSXPrrR3ZLWXclgDAUTDu2xqWkyR7+PcmZR3GlqSvBSgTwOgqmHQP8gx0nz1RckDX8ZRXO64OGhZaiF3TlRSUC4A1CLqFomu4duF9WAfx5+pfY6Ufn6b9EO7cFICSwBYXTdtOQuIT0TNnC/822xEyMve4sdZXz1gQwCoXTFl74UuI4K806gawjJ3ihz2WMve709UQAT2B1oPaM//OiZFl2+fEZji2+DYBnWP5IsQCEBL5GAbVnZUxcsv1n+0Vinb8xbGst420JAE8D9KH2jLPqhl3DH5eQno0xNgw/jwUgJJCPImpPWYXzml0FsCk26RDG2BDO80Y/3gpgPAoJAUAAlmRuW/l2RwC4GQgBQADWg09U/rnXAsDNQAgAArAkse5k210B4GYgBAABWIs+qgkANwMhAAjAUhS4mvrrjQBwMxACgACswTh3c61EALgZCAFAAOaHp1o/qroAcDMQAoAALMEiJZlWKgDcDIQAIABz8y+aCUBIACe3QgAQgDmJV5pnTwTQE4WGACAAU9JPcwFgsxAIAAIwJXlEe70E8BxxE0WHACAA0/A7T7LskQCEBKJRdAgAAjAFuzzNsTcC6E5cRfHV2Q+AgtJgVwFs252Kcya0XfTzgu4CEBL4HAPgPT7DwwrtvBtQRm5BMcZZM6K8ybC3AnicuIBB8I4RH809ZPNzAPhKEYeAqM8V4ieGCUBI4DMMhHdMDotKtPuegAP8Q8ox1qrzF2/zq4YAOhM44NEL1m9NsP1RYf4fzs7CWKsKZ66L4QIQEhiJAfEMPiasvLKq3u4CWBkTh4NA1cVfjeyqJYCOxAkMinLmLIxJkOFcgNPOyhv9/YIrMeaqkOPJpB/NBCAk8A4GRjFXC0vO1MhyMtCsBWuxjkQdequVW9UEICQQhcFxn0XRW6Q6IPREyenz/f1w6KyX7FQzs2oLoCtxCoPUNuMCI1NkPBl4e3xqlpi8gj5QDsvzGdMKQEjgDTzzbZ33Rkw/XlbhrJP1ePAFyzbhq4Bn+KmdV9UFICQwD4PVMu+PmpF3tLDEIWv4G+FDUdEPitioRVa1EsDDxDEMWjMa/jJrRaIMj/zcwVFV00BXAolYT+IWPInqccsIQEjgRaIeg/f9s/6z23enfofgt7xOYPDI6fiwaOWDg3hTq5xqJgAhgWDZd2idtWBtQqnDWYuwu4aviuZ98U1if7/gGgT+PuZpmVGtBcAThDIkHLQbgcHL9p8oOV2NgLtPSfnZK58vWg8R3CWX6GRZATTZPUia73mjP/lr6uFjhSUINETgJXwGx/Na51NzAQgJTLD7gH0wetbhpIM5RxBgiEAlxuuRTb0E0J5ItOVmHsNCi7bsTElHYDUVQa1kIoh/182z/SwhACGBbkSxne7sf7lmR1J5ZfVNhBQiUBF+b0/plUvdBCAk8Cxx3urTMcMjY3Bn33gR7LehCG5o+cjPcAEICfxWvFHLDc5nocsTZVq9BxHojq/eedRdAEICH1hpIsboiXxnv6gUoYMINGSaEVk0RABCAmGmv7M/ZlZ28qHcowgZRKAxXxqVQyMF0I5YZ9ZturfGHcCdfYhAD+J4wpx0AhAS6EQcMNGd/Yov1+5IdlRV30KQIAIdyOY9NIzMoKECEBJ4kigy+s4+780n8xp9iEB3yvR83GdaAQgJ/JI4Z8AgXJ+MO/sQgf7wYTr/aobsmUIATXYSuq7Xnf0xny44kJNfXIZgQAR6f+jwo3Cz5M40AhASGKD1HgJ+Y2ZnJ6fnHkMQgEEiGGymzJlKAEIC/6fF6kG+s79tF+7sA5ciSNJYBLyxx2iz5c10AmgyW/CyWifvRK/diTv7wEgR8MxXHzNmzZQCEBJ42Zsbg/39gi7OWbged/aB0SKoI942a85MKwAhgeeJCsV39sOiEgtLcWcfGC4Cvtvfy8wZM7UAhAR6ECXuFHzE+LmHco/jzj7QTARKrkjPEv9u9nyZXgBCAt2J460Uu3bZqtgkNCvQkqKy8guBwcv4UJObbYSfT8fqYYVsWUIAQgJ/L6ZONiv24BHT8zPzThSjQYFeHMrOL+SFYi7Cf5R42iq5sowAhAR+3GTtQMO0mSsSzjirrqMpgREHm/DuRPdcDRwinrBSpiwlACGBLoNHTt8Yl5CeiUYERrN7f0bOAL9gJ/XlHuIRq+XJcgJgqPAPE8vRgMAM5B4vXqb1/v0QQMsi8CeuogmBQVwjAqycIUsLQEjgV8RJNCPQmVPEi1bPj+UFICTwGBGLpgQ6sZ17zg7ZsYUAhATaEVMIzPkHWsFnQARyr9klN7YRQBMR9Caq0KxAZSqI/7ZbXmwnACGB7kQymhaoRALxD3bMii0F0OQrwRjiEhoYeMhlYizR3q45sa0A7rkawA1CoJRviX+yez5sL4AmIhhEVKKxQRtwj/jIkgtpBCAk8CSxGk0OXLCKeEKmTEglgCYi+B2Bs/5A00k9fWTMgpQCEBJ4hJhLYDWhvNQT84kusuZAWgE0EcE/EysxgUgqeKzXED1k73/pBdBEBM8Rf0M4bM9W4nn0PATgSgSviIkfCIu92EO8jB6HANwVQR8Cm45YnzTif9DTEICnswn7E0cQJMtxmPg9+hgCUEsGvyU2iRVhCJh57+pvIN5Az0IAWk4tno5ZhabiLBFCPI0ehQD0EkEn4n3iEAJoGCliiveD6EkIwEgZ/BfxldgjDsHUllqxIex/oPcgALOJ4MfiqmALNitVlauiplzbv0OvQQBWkEEX4l0iRqwrR5CVwfs4rBM17IKeggCsLAM+x+D3YsrxOYTbJXyi8wqiL99jQe9AAHaUwYPEW2IhUprki5FuEOliQc6bREf0CAQg49XBa8RE8T3XafPNNngu/iTiN8SP0AMQALhfCj2IIcQSIteik49uide+RLyXHhhbCAB4JoSOxM/FV4dRxBxis5juauSmp3VimjR/qs8jPiT+l/gF8RDGDgIA+giim1jF6CMOruCZiguIKPEUIlascPyOyCfKxM3IG4LzxBmigMgSW6rHERvF/IbFQjrTCF+iF/ETOx2QISv/D/94Y2Ny3YIVAAAAAElFTkSuQmCC";

?>


<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>

        <link rel="shortcut icon" href="/theme_assets/images/favicon.ico" type="image/x-icon" /><!-- Favicon -->
        <link rel="apple-touch-icon-precomposed" href="/theme_assets/images/apple-touch-icon-57-precomposed.png"><!-- For iPhone -->
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/theme_assets/images/apple-touch-icon-114-precomposed.png">   <!-- For iPhone 4 Retina display -->
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/theme_assets/images/apple-touch-icon-72-precomposed.png"><!-- For iPad -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/theme_assets/images/apple-touch-icon-144-precomposed.png"><!-- For iPad Retina display -->
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
        <script src="https://browser.sentry-cdn.com/5.19.0/bundle.min.js" integrity="sha384-edPCPWtQrj57nipnV3wt78Frrb12XdZsyMbmpIKZ9zZRi4uAxNWiC6S8xtGCqwDG" crossorigin="anonymous"></script>
        <script>
            Sentry.init({ dsn: 'https://817fc244e238470f990f3e2af587bc03@o399941.ingest.sentry.io/5257765' });
        </script>

        <?php $this->head() ?>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <?php $this->beginBody() ?>
        
        <div class="wrapper">
            <?php echo Html::beginForm(['/site/logout'], 'post', ['id' => 'logoutfrm']) ?>
            <?php echo Html::endForm() ?>
            
            <!-- Loader code -->
            <div id="preloader" style="display: block;">
                <div id="loader"></div>
                <p id="loader_message"></p>
            </div>
            
            <header class="main-header">
                <nav class="navbar navbar-static-top">
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button"><img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new/'?>img/toggle.png"></a>
                    <span class="welcome_title"><?= substr(Yii::$app->user->identity->first_name . ' ' . Yii::$app->user->identity->last_name,0,20) ?></span>
                    <div class="navbar-custom-menu">
                        
                        <ul class="nav navbar-nav" style="vertical-align: middle;">
                            
                            <?php if(Yii::$app->session->get('user.idbeforeswitch.admin') && Yii::$app->session->get('user.idbeforeswitch.admin') > 0 && in_array(Yii::$app->user->identity->role, ['STUDENT']) ){ ?>
                            
                            <li style="vertical-align: middle;">
                                <a href="<?= Url::to(['/admins/back-to-admin/'])?>">
                                    <img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new/'?>img/back_to_admin.png">
                                    Back To <?= ucfirst(strtolower(Yii::$app->session->get('user.idbeforeswitch.admin.role')));?>
                                </a>
                            </li>
                            <?php } ?>                          
                            <li style="vertical-align: middle;">
                                <a style="cursor:pointer" onclick="buy_musicoin();" type="button" >
                                    <img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new_book/img/instrument/Musicoins.png'?>" height="28" width="28"> 
                                    My Musicoins <span class="student_musicoin_balance_text"><?= \app\libraries\General::getStudentMusicoin(Yii::$app->user->identity->student_uuid); ?></span>
                                </a>
                            </li>
                            <li style="vertical-align: middle;">
                                <a style="cursor: pointer; margin-top: 3px;" type="button" class="message-btn" href="/index.php/message">
                                    <img height="20" src="<?= Yii::$app->request->baseUrl . '/theme_assets/new_book/img/message.png'?>"> 
                                    Messages  <span class="badge badge-danger unseen-msg-count"></span>
                                </a>
                            </li>  
                            <li style="vertical-align: middle;">
                                <a href="javascript:$('#logoutfrm').submit();">
                                    <img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new/'?>img/logout.png"> Logout
                                </a>
                            </li>
                        </ul>
                        
                    </div>
                </nav>
            </header>
            
            <aside class="main-sidebar">
                <section class="sidebar">
                    <div class="user-panel">
                        <div class="pull-left image">
                            <span class="profile-charecter" onclick="window.location.href = '<?= Yii::$app->getUrlManager()->createUrl('/student/home'); ?>'"><?= \app\models\User::loginUserProfileCharecter(); ?></span>
                            <!--<img onclick="window.location.href = '<?= Yii::$app->getUrlManager()->createUrl('/student/profile'); ?>'" src="<?= Yii::$app->request->baseUrl . '/theme_assets/new/'?>img/profile.png" class="img-circle" alt="User Image">-->
                        </div><br>
                        <div class="pull-left info">
                            <p><?= substr(Yii::$app->user->identity->first_name . ' ' . Yii::$app->user->identity->last_name,0,20) ?></p>
                            <a href="#"><?= Yii::$app->user->identity->role; ?></a>
                        </div>
                    </div>

                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="<?= ( 
                                        ($currentController == 'booked-session' && in_array($currentAction ,['my-lessons','upcoming','completed','cancelled','credited']) )
                                    ||  ($currentController == 'book-music-tution' && in_array($currentAction ,['index','credit-schedule','reschedule-request']) )
                                    ||  ($currentController == 'student' && in_array($currentAction ,['home',]) )
                                    ||  ($currentController == 'book' && in_array($currentAction ,['index',]) )
                                ) ? 'active' : '' ?>">
                            <a href="<?= Yii::$app->getUrlManager()->createUrl('/student/home'); ?>">
                                <img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new_book/'?>img/home.png"> <span>HOME</span>
                            </a>
                        </li>
                        <li class="<?= ( 
                                        ($currentController == 'student' && in_array($currentAction ,['profile',]) )
                                    ||  ($currentController == 'monthly-subscription' && in_array($currentAction ,['index','history']) )
                                    ||  ($currentController == 'book-music-tution' && in_array($currentAction ,['history','view']) )
                                    ||  ($currentController == 'order' && in_array($currentAction ,['history']) )
                                    ||  ($currentController == 'booked-session' && in_array($currentAction ,['expired']) )
                                ) ? 'active' : '' ?>">
                            <a href="<?= Yii::$app->getUrlManager()->createUrl('/student/profile'); ?>">
                                <img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new_book/'?>img/my-profile.png"> <span>MY PROFILE</span>
                            </a>
                        </li>
<!--                        <li class="<?= ( 
                                        ($currentController == 'booked-session' && in_array($currentAction ,['my-lessons','upcoming','completed','cancelled','credited']) )
                                    ||  ($currentController == 'book-music-tution' && in_array($currentAction ,['index','credit-schedule','reschedule-request']) )
                                ) ? 'active' : '' ?>">
                            <a href="<?= Yii::$app->getUrlManager()->createUrl('/booked-session/my-lessons'); ?>">
                                <img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new/'?>img/my-lessons.png"> <span>MY LESSONS</span>
                            </a>
                        </li>-->
                        <li class="<?= ( 
                                        ($currentController == 'video' && in_array($currentAction ,['student-video-list', 'review']) )
                                ) ? 'active' : '' ?>">
                            <a href="<?= Yii::$app->getUrlManager()->createUrl('/video/student-video-list'); ?>">
                                <img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new_book/'?>img/video-library.png"> <span>VIDEO LIBRARY</span>
                            </a>
                        </li>
			 <li class="<?= ( 
                                        ($currentController == 'student' && in_array($currentAction ,['apps']) )
                                ) ? 'active' : '' ?>">
                            <a href="<?= Yii::$app->getUrlManager()->createUrl('/student/apps'); ?>">
                                <img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new_book/'?>img/Apps-white.png"> <span>APPS</span>
                            </a>
                        </li>
<!--                        <li class="<?= ( 
                                        ($currentController == 'student' && in_array($currentAction ,['tutor_available']) )
                                ) ? 'active' : '' ?>">
                            <a href="<?= Yii::$app->getUrlManager()->createUrl('/student/tutor_available'); ?>">
                                <img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new/'?>img/calendar.png"> <span>TUTOR AVAILABILITY</span>
                            </a>
                        </li>-->
                    </ul>
                    <span class="side_logo"><img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new_book/'?>img/logo.png"></span>
                </section>
            </aside>
            
            <!-- START CONTAINER -->
            <div class="content-wrapper">
                <section class="box-header">
                    <?php if(!empty($this->params['page_title_custom'])) {
                        echo $this->params['page_title_custom'];
                    } else {?>
                    <div class="col-md-12">
                        <?php $page_icon = (!empty($this->params['page_icon'])) ? $this->params['page_icon'] : 'my-profile.png'; ?>
                        <h3><img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new_book/img/'.$page_icon; ?>" style="width: 52px;position: relative;"><strong class="title-hm"><?= strtoupper($this->title); ?></strong></h3>
                    </div>
                    <?php } ?>
                </section>
                
                <?php if(!empty(Yii::$app->session['custom_alert']) && !empty(Yii::$app->session['custom_alert']['message'])){ ?>
                    <div class="col-md-12 margin-10">
                        <div class="alert alert-<?= Yii::$app->session['custom_alert']['type'];?>" role="alert">
                            <?= Yii::$app->session['custom_alert']['message']; ?>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    </div>
                <?php unset(Yii::$app->session['custom_alert']); } ?>
                
                <?= $content ?>
            </div>
            <!-- END CONTAINER -->
            
            <footer class="main-footer">
                <?= Yii::$app->params['copyright']; ?>
            </footer>
            <div class="control-sidebar-bg"></div>
            
        </div>
        <!-- END .wrapper -->
        

        <!-- modal start -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="ultraModal-Label" aria-hidden="true">
            <div class="modal-dialog animated zoomIn">
                <div class="modal-content">
                    <div class="modal-header">
                        <div id="title-demo"></div>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div id="video-content"></div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <!--<button class="btn btn-success" type="button">Save changes</button>-->
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="tutorUnavailability" tabindex="-1" role="dialog" aria-labelledby="ultraModal-Label" aria-hidden="true">
            <div class="modal-dialog animated zoomIn">
                <div class="modal-content">
                    <div class="modal-header">
                        <div id="title-tutorUnavailability"></div>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div id="unavailability-content"></div>
                </div>
            </div>
        </div>
        <!-- modal-content- LESSON BOOK SECTION AVAIBILITY START-->
        <div class="modal fade" id="tutorAvailabilityCalender">
            <div class="modal-dialog" style="width:90%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="title-tutorAvailabilityCalender"></h4>
                    </div>
                    <div id="tutorAvailabilityCalender-content" style="padding: 10px"></div>

                </div>
            </div>
        </div>
        <!-- modal-content- LESSON BOOK SECTION AVAIBILITY END-->
        
        
      
        <!-- modal-content- LESSON BOOK OPTION -->
        <div class="modal fade" id="studentBookingOption">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">CHOOSE YOUR BOOKING OPTION</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-6 text-center" style="border-right: 1px solid #bbb8b8;">
                                <p> </p>
                                <a class="btn btn-primary btn-icon redirect_booking_btn" href="javascript:void(0)" data-href="<?= Yii::$app->params['base_url'] . 'book-music-tution'; ?>" >
                                    BOOK A NEW LESSON
                                </a>
                            </div>
                            <div class="col-lg-6 text-center" >
                                <p> </p>
                                <a class="btn btn-primary btn-icon redirect_booking_btn" href="javascript:void(0)" data-href="<?= Yii::$app->params['base_url'] . 'book-music-tution/credit-schedule'; ?>" >
                                    BOOK A CREDIT LESSON
                                </a>
                                <!--<div class="clearfix"></div>-->
                                <p>You have <span id="model_credit_session_count">0</span> credit Lessons.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal -->
        
        <!-- modal-content- Buy Musicoins-->
        <div class="modal fade" id="buy-musicoins-model-old" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content buy-musicoins-model-body-old">  
                    
                </div>
            </div>
        </div>
        
        <div class="modal fade" id="buy-musicoins-model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content buy-musicoins-model-body">
                    
                </div>
            </div>
        </div>
        <!-- /.modal -->
        
        <!-- modal start -->
        <div class="modal fade" id="terms_condition_model" style="z-index: 1051; height: 50%;" tabindex="-1" role="dialog" aria-labelledby="ultraModal-Label" aria-hidden="true">
    <div class="modal-dialog animated zoomIn">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Terms and Conditions</h4>
            </div>

            <div class="modal-body">
                <?= \app\models\Settings::getSettingsByName('payment_terms_and_conditions'); ?>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-primary" type="button">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- modal end -->
        

<?php 

// $this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/js/jquery-3.2.1.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/datatables/js/datatables.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/datatables/extensions/Buttons/js/dataTables.buttons.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/datatables/extensions/Buttons/js/buttons.flash.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/datatables/extensions/Buttons/js/buttons.html5.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/datatables/extensions/Buttons/js/buttons.print.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/messenger/js/messenger.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/messenger/js/messenger-theme-future.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/messenger/js/messenger-theme-flat.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/js/messenger.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/calendar/moment.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/jquery-ui/smoothness/jquery-ui.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/calendar/fullcalendar.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/js/jquery.form.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/jquery-validation/js/jquery.validate.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/theme_assets/js/form-validation.js');
$this->registerJsFile('https://js.stripe.com/v2/');

foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
    if ($key == 'success') {
        $this->registerJs("showSuccess('$message');");
    } else {
        $this->registerJs("showErrorMessage('$message');");
    }
}
?>

<!-- SCRIPTS -->

<?php JSRegister::begin(); ?>
<script>
$("form").on("beforeValidate", function (event, messages, deferreds) {
    blockBody("Loading...");
}).on("afterValidate", function (event, messages,errorAttributes) {
    if (errorAttributes.length) {
        unblockBody();
    }
}).on("beforeSubmit", function () {
    blockBody("Loading...");
});


// websocket
var wsStatus = "OFFLINE";
WS = new WebSocket('<?= $websocket_url ?>?id=<?= $user_uuid ?>&role=<?= $user_role ?>');
WS.onopen = function(e) {
    wsStatus = "ONLINE";
    // console.log("WS CONNECTED", wsStatus);
};

WS.onmessage = function(e) {
    var message = JSON.parse(e.data);
    // console.log(message);
    if (message.type == "message"){
        appendBalloon(message.content, false);
    } else if (message.type == "attachment") {
        appendAttachmentBalloon(message.content, false)
    } else if (message.type == "unseen") {
        $(".message-btn .badge").html(message.count);
    }
};

WS.onclose = function() {
    wsStatus = "OFFLINE";
    // console.log("WS DISCONNECTED", wsStatus);
}

$(document).ready(function(){
    $(".balloons-container").animate({
        scrollTop: 99999
    }, 100);
})


// send message
$("#message-form").on("submit",function(e){
    e.preventDefault();
});
$(".message-input").on("keypress", function(e){
    if (e.keyCode === 10 || e.keyCode === 13) {
        e.preventDefault();
        var form = $("#message-form");
        var formData = form.serialize();
        var url = form.attr("action");
        var content = $(this).val().trim();
        if (content.length > 0) {
            // console.log("WS STATUS", wsStatus);
            if(wsStatus == "OFFLINE"){
                // send api
                sendOffline(url, formData);
            } else {
                // send ws
                appendBalloon($(this).val(),true);
                WS.send(JSON.stringify(formData));
                $(".message-input").val("");
                unblockBody();
            }
        }
        $(this).val("");
    }
});


function sendOffline(url, formData) {
    $.ajax({
        url: url,
        type: "POST",
        data: formData,
        processData: false,
        enctype: 'multipart/form-data',
        success: function(data) {
            // console.log("SENT", data);
            unblockBody();
        },
        error: function(e) {
            console.error("ERROR",e);
            unblockBody();
        }
    });
}

function escapeHtml(text) {
  var map = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#039;'
  };
  
  return text.replace(/[&<>"']/g, function(m) { return map[m]; });
}

function appendBalloon(content, self) {
    let params = new URLSearchParams(window.location.search);
    if (params.has("to")) {
        content = escapeHtml(content);
        var avatar = "<img src='<?= $defaultAvatar ?>' class='avatar'/>";
        var balloon = "<div class='message-wrapper "+(self? 'self':'')+"'><div class='balloon'>"+(self?'':avatar)+" "+(content)+"</div></div>";
        $(".balloons-container").append(balloon).animate({
            scrollTop: 99999
        }, 100);
    }
}

function appendAttachmentBalloon(content, self) {
    let params = new URLSearchParams(window.location.search);
    if (params.has("to")) {
        var avatar = "<img src='<?= $defaultAvatar ?>' class='avatar'/>";
        var balloon = "<div class='message-wrapper "+(self? 'self':'')+"'><div class='balloon'>"+(self?'':avatar)+" <a target='_blank' href='<?= $attachment_url ?>"+content+"'>"+content+"</a></div></div>";
        $(".balloons-container").append(balloon).animate({
            scrollTop: 99999
        }, 100);
    }
}
 
setInterval(() => {
    if (wsStatus == "ONLINE") {
        var data = {
            Message: {
                type: "getUnseen",
                role: "<?= $user_role ?>",
                id: "<?= $user_uuid ?>"
            }
        };
        WS.send($.param(data));
    }
}, 5000);
</script>
<?php JSRegister::end(); ?>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
