<?php

use yii\helpers\Html;

?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['tutor_name'].' '.$content['tutor_last_name']; ?>,</p>
            
            <p>A booking change to an upcoming lesson with <?= $content['student_name'] . ' '. $content['student_last_name']; ?> are now reflected in the booking system.</p>
            
            <p>The details of the processed booking change are as follows:</p>
            
            <p><strong>From:</strong></p>
            
            <p><strong>Student Name :</strong> <?= $content['student_name'].' '.$content['student_last_name']; ?></p>
            <p><strong>Instrument :</strong> <?= $content['instrument_name']; ?></p>
            <p><strong>Previous Tutorial Date :</strong> <?= \app\libraries\General::convertTimezone($content['from_start_datetime'],Yii::$app->params['timezone'],$content['tutor_timezone'],'d-m-Y T'); ?></p>
            <p><strong>Previous Tutorial Time :</strong> <?= \app\libraries\General::convertTimezone($content['from_start_datetime'],Yii::$app->params['timezone'],$content['tutor_timezone'],'h:i A'); ?> - <?= \app\libraries\General::convertTimezone($content['from_end_datetime'],Yii::$app->params['timezone'],$content['tutor_timezone'],'h:i A' ); ?></p>
            
            <p><strong>To:</strong></p>
            
            <p><strong>Student Name :</strong> <?= $content['student_name'].' '.$content['student_last_name']; ?></p>
            <p><strong>Instrument :</strong> <?= $content['instrument_name']; ?></p>
            <p><strong>Requested Tutorial Date :</strong> <?= \app\libraries\General::convertTimezone($content['to_start_datetime'],Yii::$app->params['timezone'],$content['tutor_timezone'],'d-m-Y T'); ?></p>
            <p><strong>Requested Tutorial Time :</strong> <?= \app\libraries\General::convertTimezone($content['to_start_datetime'],Yii::$app->params['timezone'],$content['tutor_timezone'],'h:i A'); ?> - <?= \app\libraries\General::convertTimezone($content['to_end_datetime'],Yii::$app->params['timezone'],$content['tutor_timezone'],'h:i A' ); ?></p>
            
            <p><strong style="text-decoration: underline;color:red;">IMPORTANT:</strong> You should have first made contact with your student to arrange a mutually suitable date/time for a re-scheduled lesson prior to processing the above booking change.  If this has not occurred, please contact admin@musiconn.com.au.</p>
            
            <p>You can log in to the <a href="https://app.musiconn.com.au/" target="_blank">booking system</a> to view details of your upcoming lessons.</p>
            
            <p>This is an automated message from the Musiconn Pty Ltd Booking System.  Please do not reply to this unattended mailbox.  Any queries can be directed to admin@musiconn.com.au.</p>
            
            
        </td>
    </tr>
</table>

