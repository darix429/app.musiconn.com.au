<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">

    <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Booking ID</th>
                <th>Tutor Name</th>
                <th>Instrument</th>
                <th>Order ID</th>
                <th>Subscription Name</th>
<!--                <th>Tutorial Name</th>
                <th>Tutorial Type Code</th>
                <th>Tutorial Min</th>
                <th>Payment Term Code</th>
                <th>Total Session</th>
                <th>Session Length Day</th>
                <th>Price</th>
                <th>GST</th>
                <th>Total Price</th>
                <th>Subscription Price</th>-->
                <th>Discount</th>
                <th>Grand Total</th>
                <th>Booking Date</th>
                <th>Action</th>
            </tr>
        </thead>

        <tbody>
            <?php
            if (!empty($tuitionBookingList)) {
                foreach ($tuitionBookingList as $key => $value) {  //echo "<pre>"; print_r($value); exit;
                $tutor_name = $value['tutorUu']['first_name']. ' ' .$value['tutorUu']['last_name']; 
                    ?>
                    <tr>
                        <td><?= $key + 1; ?></td>
                        <td><?= "#".$value['booking_id'] ?></td>
                        <td><?= (!empty($tutor_name)) ? $tutor_name : ''; ?></td>
                        <td><?= (!empty($value['instrumentUu']['name'])) ? $value['instrumentUu']['name'] : ''; ?></td>
                        <td><?= (!empty($value['orderUu']['order_id'])) ? "#".$value['orderUu']['order_id'] : ''; ?></td>
                        <td><?= (!empty($value['studentMonthlySubscriptionUu']['name'])) ? $value['studentMonthlySubscriptionUu']['name'] : ''; ?></td>
<!--                        <td><?= $value['tutorial_name'] ?></td>
                        <td><?= $value['tutorial_type_code'] ?></td>
                        <td><?= $value['tutorial_min'] ?></td>
                        <td><?= $value['payment_term_code'] ?></td>
                        <td><?= $value['total_session'] ?></td>
                        <td><?= $value['session_length_day'] ?></td>
                        <td><?= $value['price'] ?></td>
                        <td><?= $value['gst'] ?></td>
                        <td><?= $value['total_price'] ?></td>
                        <td><?= $value['monthly_subscription_price'] ?></td>-->
                        <td><?= $value['discount'] ?></td>
                        <td><?= $value['grand_total'] ?></td>
                        <td><?= \app\libraries\General::displayDate($value['booking_datetime']); ?></td>
                        <td>
                            <a href="<?= Url::to(['/book-music-tution/view/', 'id' => $value['uuid']]) ?>" class="badge badge-info badge-md" title="View"><i class="fa fa-eye"></i></a>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>

</div>
