<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TutorialType;

/**
 * TutorialTypeSearch represents the model behind the search form of `app\models\TutorialType`.
 */
class TutorialTypeSearch extends TutorialType
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'code', 'description', 'type', 'status'], 'safe'],
            [['min', 'days', 'unit'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $modeDatatable = false, $statusnot = '')
    {
        $query = TutorialType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

	if (!empty($statusnot)) {
            if (is_array($statusnot)) {

                $query->andWhere(['not in', 'status', $statusnot]);
            } else {

                $query->andWhere(['<>', 'status', $statusnot]);
            }
        }

        $query->andFilterWhere(['status' => $this->status]);

        // grid filtering conditions
        $query->andFilterWhere([
            'min' => $this->min,
            'days' => $this->days,
            'unit' => $this->unit,
        ]);

        $query->andFilterWhere(['ilike', 'code', $this->code])
            ->andFilterWhere(['ilike', 'description', $this->description])
            ->andFilterWhere(['ilike', 'type', $this->type]);

	if ($modeDatatable) {
            $result = $query->asArray()->orderBy('code')->all();
	    return $result;
        }
        return $dataProvider;
    }
}
