<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "timezone".
 *
 * @property int $id
 * @property string $name
 * @property string $timezone
 * @property string $offset
 * @property string $status
 */
class Timezone extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'timezone';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'timezone'], 'required'],
            [['name', 'timezone', 'offset', 'status'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'timezone' => 'Timezone',
            'offset' => 'Offset',
            'status' => 'Status',
        ];
    }
}
