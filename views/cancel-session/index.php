<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CancelSessionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Cancel Session Request');
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs(
        '
        $(document).on("click","#search_btn", function(){
            $.ajax({
                type: "GET",
                url: "' . Url::to(['/cancel-session/index/']) . '",
                data: $("#search_frm").serialize(),
                beforeSend:function(){ blockBody(); },
                success:function(result){
                  console.log(result);
                    unblockBody();
                    $("#cancel_session_table_parent_div").html(result);
                    window.ULTRA_SETTINGS.dataTablesInit();
                },
                error:function(e){
                    unblockBody();
                    showErrorMessage(e.responseText);
                }
            });
        });

        $(document).on("click","#search_reset_btn", function(){
            $.ajax({
                type: "GET",
                url: "' . Url::to(['/cancel-session/index/']) . '",
                data: {},
                beforeSend:function(){ blockBody(); },
                success:function(result){
                    unblockBody();
                    $("#cancel_session_table_parent_div").html(result);
                    window.ULTRA_SETTINGS.dataTablesInit();
                },
                error:function(e){
                    unblockBody();
                    showErrorMessage(e.responseText);
                }
            });
        });
    '
);
?>


<div class="col-xl-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Cancel Session Request List</h2>
            <!-- <div class="actions panel_actions float-right">
                <i class="fa fa-search icon-primary icon-xs icon-orange mail_head_icon custom-toggle" data-togglediv="datatable-search-div" title="Advance Search"></i>
            </div> -->
        </header>
        <div class="content-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12 ">
                    <!-- <div class="datatable-search-div " style="display: none;">

                        <?php
                        $form = ActiveForm::begin([
                                    'method' => 'get',
                                    'options' => [
                                        'id' => 'search_frm',
                                        'class' => 'form-inline'
                                    ],
                        ]);
                        ?>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'approve_datetime')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Date', 'style' => 'width:100%'])->label(false) ?>
                        </div>
                        <div class="col-md-4 mb-0">
                            <?= $form->field($searchModel, 'student_uuid')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Time', 'style' => 'width:100%'])->label(false) ?>
                        </div>
                        <div class="col-md-4 mb-0">
                            <?= $form->field($searchModel, 'tutor_uuid')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Student Name', 'style' => 'width:100%'])->label(false) ?>
                        </div>
                        <div class="col-md-4 mb-0">
                            <?= $form->field($searchModel, 'student_uuid')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Tutor Name', 'style' => 'width:100%'])->label(false) ?>
                        </div>
                        <div class="col-md-4 mb-0">
                            <?= $form->field($searchModel, 'student_uuid')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Request Date', 'style' => 'width:100%'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'status')->dropDownList(['AWAITING' => 'AWAITING', 'APPROVE' => 'APPROVE', 'REJECTED' => 'REJECTED'], ['class' => 'form-control', 'style' => 'width:100%'])->label(false) ?>
                        </div>

                            <?= Html::Button(Yii::t('app', 'Search'), ['class' => 'btn btn-primary mb-0', 'id' => 'search_btn']) ?>
                            <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-purple mb-0', 'id'=>'search_reset_btn']) ?>


                        <?php ActiveForm::end(); ?>
                    </div> -->
                </div>

            </div>
            <br>
            <div class="clearfix"></div>
            <div class="row" id="cancel_session_table_parent_div">
                <?php echo Yii::$app->controller->renderPartial('_cancel_session_list', ['cancelSessionList' => $cancelSessionList]); ?>
            </div>
        </div>
    </section>
</div>
