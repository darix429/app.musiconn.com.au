<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\ArrayHelper;
use app\models\InstrumentCategory;
use app\models\Instrument;
use app\models\User;
use yii\helpers\Url;
use app\libraries\General;


if ($model->status == 'UPLOADED') {
    $this->title = Yii::t('app', 'Uploaded Video : ' . $model->title);
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Uploaded Videos'), 'url' => (isset($back_url)) ? $back_url : ['index']];
    $this->params['breadcrumbs'][] = $this->title;
} else {
    $this->title = Yii::t('app', 'Published Video : ' . $model->title);
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Published Videos'), 'url' => (isset($back_url)) ? $back_url : ['index']];
    $this->params['breadcrumbs'][] = $this->title;
}

$this->registerJs(
        '  
$(document).on("change","#prerecordedvideo-status", function(){        
    if($(this).val() == "REJECTED"){
        $(".rejected-reason-parent").show();
    } else {
        $(".rejected-reason-parent").hide();
    }
    
    if($(this).val() == "PUBLISHED"){
        $(".expire_date_parent").show();
    } else {
        $(".expire_date_parent").hide();
    }
});

  
$(document).on("click","#savebutton",function(){ //alert(1);

    var file_data = $("#prerecordedvideo-update_file").prop("files")[0];   
    var form_data = new FormData($("form")[2]);                  
    form_data.append("update_file", file_data);
    $(".msg").text("Uploading in progress..."); 
    $.ajax({
            type: "POST",
            url: $( "#upload_form" ).attr("action"),
            data:  form_data,
            processData: false,
            contentType: false,
            beforeSubmit: function() {
                blockBody();
                $("#progress-bar").width("0%");
              },
            beforeSend:function(){ blockBody(); },
            xhr: function () { 
                    blockBody();
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        percentComplete = parseInt(percentComplete * 100);                       
                        
                        $("#progress-bar").width(percentComplete + "%");
			$("#progress-status").html(percentComplete + "%");
                    }
                }, false);
                return xhr;
            },
            success: function (result) { //alert(result.files.url); 
                
                if(result.code == 200){
                    unblockBody();
                    $(".msg").text(result.message);
                    
                    if($("#progress-status").html() == "100%"){
                    
                        $("#progress-bar").width("0%");
                        $( ".form-open" ).hide();
                        $(".msg").hide(); 
                        window.location.reload();
                    }
                } else {
                    unblockBody();
                    $("#uploadFormDiv").html(result);
                    $("#progress-bar").width("0%");
                }
                unblockBody();
            }, error:function(e){ 
                unblockBody();
                $("#progress-bar").width("0%");
                showErrorMessage(e.responseText);
            }
        });
    
});


$(document).on("click","#editCategoryBtn",function(){
    $.ajax({
        type: "GET",
        url: "' . Url::to(['/video/categoryupdate/','id' => $model->uuid]) . '",
        beforeSend:function(){ blockBody(); },
        success:function(result){
            $("#categoryUpdateDiv").html(result);
            unblockBody();
        },
        error:function(e){ 
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});

$(document).on("click","#categorySubmitbutton",function(){
    $.ajax({
        type: "POST",
        url: "' . Url::to(['/video/categoryupdate/','id' => $model->uuid]) . '",
        data: $("#category_form").serialize(),
        beforeSend:function(){ blockBody(); },
        success:function(result){
            $("#categoryUpdateDiv").html(result);
            unblockBody();
            window.location.reload();
        },
        error:function(e){ 
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});

//published-video    
$(document).on("click",".published-enabled", function(){ 
        if(confirm("Are you sure, you want to published this video?")){
        var id = $(this).attr("data-id"); 
        
            $.ajax({
                type: "GET",
                url: "' . Url::to(['/video/published-enabled/']) . '",
                data: { "id": id},
                beforeSend:function(){ blockBody(); },
                success:function(result){ 
                    if(result.code == 200){
                        unblockBody();
                        showSuccess(result.message);
                        window.location.reload();
                    }else{
                        unblockBody();
                        showErrorMessage(result.message);
                    }
                },
                error:function(e){ 
                    unblockBody();
                    showErrorMessage(e.responseText);
                }
            });
        }
    });
    
    $(document).on("click",".edit",function(){ 
        //$( document ).ready( function() {
           // $( "#edit" ).click( function() { 
                $( ".form-open" ).show();
                $("html,body").animate({
                    scrollTop: $(".form-open").offset().top},
                    "slow");
                    
           // });
            
        });
        
        //is-free    
        $(document).on("click",".isfree_disabled_link", function(e){ 
                if(confirm("Are you sure, you want to is free disabled this video?")){
                var element = $(this);
                
                    $.ajax({
                        type: "GET",
                        url: "' . Url::to(['/video/is-free-disabled/']) . '",
                        data: { "id": $(this).attr("data-id")},
                        beforeSend:function(){ blockBody(); },
                        success:function(result){ 
                            if(result.code == 200){ 
                                unblockBody();
                                $(element).removeClass("isfree_disabled_link");
                                $(element).addClass("isfree_enabled_link");
                                
                                showSuccess(result.message);
                            }else{
                                unblockBody();
                                showErrorMessage(result.message);
                            }
                        },
                        error:function(e){
                            unblockBody();
                            showErrorMessage(e.responseText);
                        }
                    });
                }
                else{
                    e.preventDefault();
                }
            });
            
            //not_free
            $(document).on("click",".isfree_enabled_link", function(e){ 
                if(confirm("Are you sure, you want to is free enabled this  video?")){
                var element = $(this);
                
                    $.ajax({
                        type: "GET",
                        url: "' . Url::to(['/video/is-free-enabled/']) . '",
                        data: { "id": $(this).attr("data-id")},
                        beforeSend:function(){ blockBody(); },
                        success:function(result){ 
                            if(result.code == 200){
                                unblockBody();
                                $(element).removeClass("isfree_enabled_link");
                                $(element).addClass("isfree_disabled_link");
                                
                                showSuccess(result.message);
                            }else{
                                unblockBody();
                                showErrorMessage(result.message);
                            }
                        },
                        error:function(e){
                            unblockBody();
                            showErrorMessage(e.responseText);
                        }
                    });
                }else{
                e.preventDefault();
                }
            });

//deleted video
$(document).on("click","#deleteVideoBtn",function(){
    $.ajax({
        type: "POST",
        url: "' . Url::to(['/video/delete-video/','id' => $model->uuid]) . '",
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            if(result.status == "published") {
                window.location.assign("' . Url::to(['/video/published-video']) .'");
            } else {
                window.location.assign("' . Url::to(['/video/uploaded']) .'");
            }
            showSuccess(result.message);
        },
        error:function(e){ 
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});
', View::POS_END
);

if ($model['tutor_uuid'] != NULL) {
    $user = \app\models\Tutor::find()->where(['uuid' => $model['tutor_uuid']])->one();
} else {
    $user = \app\models\Admin::find()->where(['admin_uuid' => $model['admin_uuid']])->one();
}
$instrument = Instrument::find()->where(['uuid' => $model->instrument_uuid])->one();
$instrumentCategory = InstrumentCategory::find()->where(['uuid' => $model->instrument_category_uuid])->one();

$file_path = Yii::$app->params['media']['pre_recorded_video']['path'] . $model->file;
$poster = (is_file($file_path)) ? '' : Yii::$app->params['theme_assets']['url'] . "images/novideo.png";
?>

<script>
    function video_reset(video_id) {
            video_tag = document.getElementById('video_'+video_id);
            video_tag.pause();
            video_tag.currentTime = 0;
    }
</script>
<style>
    .video-js {
        width: 985px;
        height: 430px;
    }
    .edit{
        margin-top: 11px;
    }
    .download{
        margin-top: 11px;
    }
    .small-size{
        width: 100px;
        height: 90px;
    }
    .search_result_more_video{
        /*margin: 0 0px -20px 0;*/
        margin: 0;
        padding: 0;
    }
    .title-details{
        margin-top: 16px;
    }
    .published-enabled{
        margin-top: 11px;
    }
    .custom-download{
        margin-top: -9px;    
    }

</style>

<div class="col-xl-12 col-lg-12 col-12 col-md-12">
    <section class="box ">
        <header class="panel_header">
            <?php if ($model->status == 'UPLOADED') { ?>
                <h2 class="title float-left">Uploaded Videos Details</h2>
            <?php } else { ?>
                <h2 class="title float-left">Published Videos Details</h2>
            <?php } ?>


            <div class="actions panel_actions float-right">
                <?php if ($model->status !== 'PUBLISHED') { ?>
                    <a href="<?= Yii::$app->getUrlManager()->createUrl(['/video/uploaded/']) ?>" class="btn btn-info btn-icon "><i class="fa fa-arrow-left text-white"></i> <span>Back</span></a>
                <?php } else { ?>
                    <?php if (Yii::$app->user->identity->role == 'OWNER') { ?>
                        <a href="<?= Yii::$app->getUrlManager()->createUrl(['/video/published-video/']) ?>" class="btn btn-info btn-icon "><i class="fa fa-arrow-left text-white"></i> <span>Back</span></a>
                    <?php } else if (Yii::$app->user->identity->role == 'ADMIN') { ?>
                        <a href="<?= Yii::$app->getUrlManager()->createUrl(['/video/published-video/']) ?>" class="btn btn-info btn-icon "><i class="fa fa-arrow-left text-white"></i> <span>Back</span></a>
                    <?php } else if (Yii::$app->user->identity->role == 'SUBADMIN') { ?>
                        <a href="<?= Yii::$app->getUrlManager()->createUrl(['/video/published-video/']) ?>" class="btn btn-info btn-icon "><i class="fa fa-arrow-left text-white"></i> <span>Back</span></a>
                    <?php } else { ?>
                        <a href="<?= Yii::$app->getUrlManager()->createUrl(['/video/published/']) ?>" class="btn btn-info btn-icon "><i class="fa fa-arrow-left text-white"></i> <span>Back</span></a>
                    <?php } ?>
                <?php } ?>
            </div>
        </header>
        <div class="content-body">
            <div class="row">

                <div class="col-md-12 mb-0" >
                    <video id="video_<?= $model->uuid?>" class="video-js resize" controls preload="none" controlsList="nodownload"  poster="<?= $poster; ?>" onended="video_reset('<?= $model->uuid?>')">
                        <source src="<?= Yii::$app->getUrlManager()->createUrl(['/video/play/','id' => $model->uuid]) ?>" type="video/mp4" > 
                    </video>
                </div>

                <div class="col-lg-8 col-md-12 col-12" style="border-right-style: ridge;">
                    <div class="form-row video-details">
                        <div class="col-md-12 mb-0">

                            <h4 id ="title" onchange="myFunction()"><b><?= $model->title; ?> </b></h4>
                            <p id ="created_at"><small class="text-muted"><i class="fa fa-clock-o"></i> <?= \app\libraries\General::displayDateTime($model->created_at); ?>&nbsp;<a class="text-primary more" data-id="<?= $model->uuid; ?>"> More </a></small></p>

                            <p class="text-primary" id ="tutor_name"><i class="fa fa-smile-o" aria-hidden="true" style="font-size:26px;"></i>&nbsp;<b style="font-size:17px;"><?= $user->first_name . ' ' . $user->last_name; ?> </b></p>

                            <?php if ($model->status !== 'PUBLISHED') { ?>
                                <button class="btn btn-purple  btn-icon bottom15 right15 edit">
                                    <i class="fa fa-pencil"></i> &nbsp; <span>Edit</span>
                                </button>
                                   
                                <button class="btn btn-danger  btn-icon bottom15 right15 download" id="deleteVideoBtn">
                                        <i class="fa fa-trash"></i> &nbsp; <span>Delete</span>
                                    </button> 
                                <?php
                            }
                            
                            if(Yii::$app->user->identity->role !== 'TUTOR'){
                                if ($model->status !== 'PUBLISHED' ) {
                                    ?>
                                    <a href="<?= Yii::$app->getUrlManager()->createUrl(['/video/download/','id' => $model->uuid]) ?>" download>
                                        <button class="btn btn-purple  btn-icon bottom15 right15 download">
                                            <i class="fa fa-download"></i> &nbsp; <span>Download</span>
                                        </button>
                                    </a>
                                <?php } else { ?>
                                    <!-- <a href="<?= Yii::$app->getUrlManager()->createUrl(['/video/download/','id' => $model->uuid]) ?>" download>
                                        <button class="btn btn-purple  btn-icon bottom15 right15 custom-download">
                                            <i class="fa fa-download"></i> &nbsp; <span>Download</span>
                                        </button>
                                    </a> -->
                                    <button class="btn btn-purple  btn-icon bottom15 right15 custom-download" id="editCategoryBtn">
                                        <i class="fa fa-pencil"></i> &nbsp; <span>Edit</span>
                                    </button>

                                    <button class="btn btn-danger  btn-icon bottom15 right15 custom-download" id="deleteVideoBtn">
                                        <i class="fa fa-trash"></i> &nbsp; <span>Delete</span>
                                    </button>
                                    <?php
                                }
                            }

                            if ($model->status !== 'PUBLISHED') {
                                ?>
                                <button class="btn btn-purple  btn-icon bottom15 right15 published-enabled" data-id="<?= $model->uuid; ?>">
                                    <i class="fa fa-check-square-o" aria-hidden="true"></i> &nbsp; <span> Published </span>
                                </button>
                                <?php
                            }

                            if ($model->status !== 'UPLOADED' && Yii::$app->user->identity->role !== 'TUTOR' && Yii::$app->user->identity->role !== 'STUDENT') {
                                if ($model->is_free == '1') {
                                    $e_link_class = "isfree_disabled_link";
                                    ?>
                                    <input type="checkbox" checked ="" class="iswitch iswitch-lg iswitch-purple switch-is-free <?= $e_link_class ?>" data-id="<?= $model->uuid; ?>" title="Is Free"> 
                                    <?php
                                } else {
                                    $e_link_class = "isfree_enabled_link";
                                    ?>
                                    <input type="checkbox"  class="iswitch iswitch-lg iswitch-purple switch-is-free <?= $e_link_class ?>" data-id="<?= $model->uuid; ?>" title="Is Free"> 
                                    <?php
                                }
                            }

                            if (!empty($model->instrument_uuid)) {
                                ?>
                                <p id="instrument"><b>Instrument :  </b><?= $instrument->name; ?></p>
                                <?php
                            }
                            if (!empty($model->instrument_category_uuid)) {
                                ?>
                                <p id="instrumentCategory"><b>Category :  </b><?= $instrumentCategory->name; ?></p>
                                <?php
                            }
                            if (!empty($model->description)) {
                                ?>
                                <h5><b>Description</b></h5>
                                <p><?= $model->description; ?></p>
                            <?php } ?>

                        </div>
                    </div>
                    <br><br>
                    <div class="row form-open" style="display:none; border-top-style: ridge;">
                        <div class="col-lg-12 col-md-12 col-12">
                            <div class="form-row">
                                <h3 class="title float-left"><b>Update Recorded Video</b></h3>
                                <div class="col-md-12 mb-0">

                                    <div id="uploadFormDiv">
                                        <?=
                                        $this->render('_admin_update_form', [
                                            'model' => $model,
                                        ])
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div id="categoryUpdateDiv" >
                        
                    </div>

                </div>

                <div class="col-lg-4 col-md-12 col-12">
                    <div class="col-lg-12">
                        <h4><b> More Videos </b></h4>
                        <!--<p>In design, space is concerned with the area <del>deep within the moment</del> of designated design.</p>-->

                        <div class="tab-pane fade show active" id="web-1">
                            <?php
                            if (Yii::$app->user->identity->role == 'TUTOR') {
                                $tutor_uuid = Yii::$app->user->identity->tutor_uuid;
                                if ($model->status !== 'PUBLISHED') {
                                    $recordData = app\models\PreRecordedVideo::find()->where(['<>', 'uuid', $model->uuid])->andWhere(['tutor_uuid' => $tutor_uuid, 'status' => 'UPLOADED'])->asArray()->limit(5)->orderBy('updated_at')->all();
                                } else {
                                    $recordData = app\models\PreRecordedVideo::find()->where(['<>', 'uuid', $model->uuid])->andWhere(['tutor_uuid' => $tutor_uuid, 'status' => 'PUBLISHED'])->asArray()->limit(5)->orderBy('updated_at')->all();
                                }
                            } else {
                                if ($model->status !== 'PUBLISHED') {
                                    $recordData = app\models\PreRecordedVideo::find()->where(['<>', 'uuid', $model->uuid])->andWhere(['status' => 'UPLOADED'])->asArray()->limit(5)->orderBy('updated_at')->all();
                                } else {
                                    $recordData = app\models\PreRecordedVideo::find()->where(['<>', 'uuid', $model->uuid])->andWhere(['status' => 'PUBLISHED'])->asArray()->limit(5)->orderBy('updated_at')->all();
                                }
                            }


                            if (!empty($recordData)) {
                                foreach ($recordData as $key => $value) {
                                    $thumb_url = is_file(Yii::$app->params['media']['pre_recorded_video']['thumbnail']['path'] . $value['uuid'].'.jpg') ? 
                                                        Yii::$app->params['media']['pre_recorded_video']['thumbnail']['url'] . $value['uuid'].'.jpg' :
                                                        Yii::$app->params['theme_assets']['url'] . "images/thumbnail.jpeg";
                                    ?>
                                    <div class="search_result_more_video">
                                        <div class="float-left col-md-5 col-3 related-video">
                                            <a href="<?= Url::to(['/video/review/', 'id' => $value['uuid']]) ?>">
                                                <img class="small-size" src="<?= $thumb_url; ?>">
                                            </a>
                                        </div>
                                        <div class="float-left col-md-7 col-9 title-details" >

                                            <h6><a href="<?= Url::to(['/video/review/', 'id' => $value['uuid']]) ?>"><?= $value['title']; ?></a></h6>

                                            <p><?php
                                                if ($value['tutor_uuid'] != NULL) {
                                                    $username = \app\models\Tutor::find()->where(['uuid' => $value['tutor_uuid']])->one();
                                                    echo $username['first_name'] . ' ' . $username['last_name'];
                                                } else {
                                                    $username = \app\models\Admin::find()->where(['admin_uuid' => $value['admin_uuid']])->one();
                                                    echo $username['first_name'] . ' ' . $username['last_name'];
                                                }
                                                ?>
                                            </p>
                                        </div>
                                    </div>
                                    <?php 
                                }
                            } else {
                                ?>
                                <h6>No video found</h6>
                            <?php }
                            ?>

                        </div>

                    </div>

                </div>

            </div>
        </div>
    </section>
</div>


