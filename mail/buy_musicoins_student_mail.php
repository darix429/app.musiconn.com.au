<?php

use yii\helpers\Html;

?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['student_first_name']; ?>,</p>
            
            <p>Thank you for your payment.  Below are the details of your purchase:</p>
                        
                       
            <p><strong>Transaction ID:</strong> <?= "#".$content['transaction_id']; ?></p>
            <p><strong>Musicoins purchased:</strong> <?= $content['coins']; ?></p>
            <?php if($content['bonus_coins'] > 0){ ?>
                <p><strong>Bonus Musicoins:</strong> <?= $content['bonus_coins']; ?></p>
            <?php } ?>
            <?php if($content['promo_coins'] > 0){ ?>
                <p><strong>Promocode Musicoins:</strong> <?= $content['promo_coins']; ?></p>
            <?php } ?>
            <p><strong>Total Musicoins credited to account:</strong> <?= $content['grand_total_coins']; ?></p>
            
            <?php if(!empty($content['coupon_code'])){ ?>
                <p>You can used the Promo code is <strong><?= $content['coupon_code']; ?></strong></p>
            <?php } ?>
            <p><strong>Creative Kids Voucher submitted:</strong> <?= (!empty($content['creative_kids_voucher_code'])) ? "Yes" : "No"; ?></p>
            
            <p>Attached is an invoice as record of your payment.  You can also find this invoice by logging onto our <a target="_blank" href="https://app.musiconn.com.au/">booking system</a>.</p>
            
            <p>You can now use the purchased Musicoins to book lessons with a tutor on our platform.</p>
            
            <p>For terms and conditions of your purchase, please refer to the <a target="_blank" href="https://www.musiconn.com.au/">Musiconn website</a>.</p>
            
            <p>Please do not reply to this unattended mailbox. Any queries can be directed to admin@musiconn.com.au, or call us on 1300 068 742.</p>
            
            
            <p>Regards,<br>
            Julian Fung <br>
            CEO – Musiconn Pty Ltd</p>
        </td>
    </tr>
</table>

