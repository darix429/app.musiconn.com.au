<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "student_tuition_standard_booking_locking_period".
 *
 * @property string $uuid
 * @property string $student_uuid
 * @property string $tutor_uuid
 * @property string $premium_standard_plan_rate_uuid
 * @property string $price
 * @property string $gst
 * @property string $total_price
 * @property string $booking_datetime
 * @property string $online_tutorial_package_tutorwise_uuid
 * @property string $locking_start_datetime
 * @property string $locking_end_datetime
 */
class StudentTuitionStandardBookingLockingPeriod extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'student_tuition_standard_booking_locking_period';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'student_uuid', 'tutor_uuid', 'premium_standard_plan_rate_uuid', 'online_tutorial_package_tutorwise_uuid', 'locking_start_datetime', 'locking_end_datetime'], 'required'],
            [['uuid', 'student_uuid', 'tutor_uuid', 'premium_standard_plan_rate_uuid', 'online_tutorial_package_tutorwise_uuid'], 'string'],
            [['price', 'gst', 'total_price'], 'number'],
            [['booking_datetime', 'locking_start_datetime', 'locking_end_datetime'], 'safe'],
            [['uuid'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'student_uuid' => Yii::t('app', 'Student Uuid'),
            'tutor_uuid' => Yii::t('app', 'Tutor Uuid'),
            'premium_standard_plan_rate_uuid' => Yii::t('app', 'Premium Standard Plan Rate Uuid'),
            'price' => Yii::t('app', 'Price'),
            'gst' => Yii::t('app', 'Gst'),
            'total_price' => Yii::t('app', 'Total Price'),
            'booking_datetime' => Yii::t('app', 'Booking Datetime'),
            'online_tutorial_package_tutorwise_uuid' => Yii::t('app', 'Online Tutorial Package Tutorwise Uuid'),
            'locking_start_datetime' => Yii::t('app', 'Locking Start Datetime'),
            'locking_end_datetime' => Yii::t('app', 'Locking End Datetime'),
        ];
    }
}
