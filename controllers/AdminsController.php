<?php

namespace app\controllers;

use Yii;
use app\models\Admin;
use app\models\AdminSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\User;
use app\libraries\General;
use yii\helpers\Url;
use yii\filters\AccessControl;
use app\models\ChangePassword;

/**
 * AdminController implements the CRUD actions for Admin model.
 */
class AdminsController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                        [
                        'actions' => ['index'],
                        'allow' => Yii::$app->user->can('/admins/index'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['create'],
                        'allow' => Yii::$app->user->can('/admins/create'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['account-disabled'],
                        'allow' => Yii::$app->user->can('/admins/account-disabled'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['account-enabled'],
                        'allow' => Yii::$app->user->can('/admins/account-enabled'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['profile'],
                        'allow' => Yii::$app->user->can('/admins/profile'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['change-password'],
                        'allow' => Yii::$app->user->can('/admins/change-password'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['update'],
                        'allow' => Yii::$app->user->can('/admins/update'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['delete'],
                        'allow' => Yii::$app->user->can('/admins/delete'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['switch-to-admin'],
                        'allow' => Yii::$app->user->can('/admins/switch-to-admin'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['back-to-owner'],
                        'allow' => Yii::$app->user->can('/admins/back-to-owner'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['switch-to-student'],
                        'allow' => Yii::$app->user->can('/admins/switch-to-student'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['back-to-admin'],
                        'allow' => Yii::$app->user->can('/admins/back-to-admin'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['switch-to-tutor'],
                        'allow' => Yii::$app->user->can('/admins/switch-to-tutor'),
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                //'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
        if ($action->id == 'delete') {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    /**
     * Lists all Admin models.
     * @return mixed
     */
    public function actionIndex() {

        /* $searchModelInactive = new AdminSearch();
          $searchModelInactive->status = 'INACTIVE';
          $inactiveAdmin = $searchModelInactive->search(Yii::$app->request->queryParams, true); */

        $searchModel = new AdminSearch();
        $searchModel->status = "ENABLED";
        $adminList = $searchModel->search(Yii::$app->request->queryParams, true);

//        $searchDeleted = new AdminSearch();
//        $searchDeleted->status = "DELETED";
//        $adminDeleted = $searchDeleted->search(Yii::$app->request->queryParams, true);

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_admin_list', [
                        'adminList' => $adminList,
            ]);
        } else {
            return $this->render('index', [
                        //'inactiveAdmin' => $inactiveAdmin,
                        'adminList' => $adminList,
                        'searchModel' => $searchModel,
            ]);
        }
    }

    /**
     * Displays a single Admin model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Admin model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Admin();
        $userModel = new \app\models\User();
        $userModel->scenario = 'admin_create';

        $model->attributes = Yii::$app->getRequest()->post('Admin');
        $userModel->attributes = Yii::$app->getRequest()->post('User');

        $model->email = $userModel->username = $userModel->email;
        $userModel->role = $model->admin_type;
        $userModel->auth_key = Yii::$app->security->generateRandomString();
        $userModel->created_at = $userModel->updated_at = $model->created_at = $model->updated_at = time();

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $userModel->load(Yii::$app->request->post()) && $userModel->validate()) {

            if ($model->save(false)) {

                $password_string = $userModel->password_hash;
                $userModel->password_hash = Yii::$app->security->generatePasswordHash($userModel->password_hash);
                $userModel->admin_uuid = $model->admin_uuid;
                $userModel->first_name = $model->first_name;
                $userModel->last_name = $model->last_name;
                $userModel->phone = $model->phone;
                $userModel->status = 10;
                $userModel->save(false);

                Yii::$app->authManager->assign(Yii::$app->authManager->getRole($userModel->role), $userModel->getPrimaryKey());

                $content = [
                    'name' => $model->first_name . ' ' . $model->last_name,
                    'email' => $userModel->email,
                    'username' => $userModel->username,
                    'password' => $password_string,
                    'admin_type' => $model->admin_type,
                ];
                Yii::$app->mailer->compose('admin_account_create', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                        ->setFrom(Yii::$app->params['supportEmail'])
                        ->setTo($userModel->email)
                        ->setBcc(Yii::$app->user->identity->email)
                        ->setSubject('New Account Creation mail - ' . Yii::$app->name)
                        ->send();

                Yii::$app->getSession()->setFlash('success', 'You have successfully created Admin account.');
            } else {
                Yii::$app->getSession()->setFlash('danger', 'Something wrong. Admin account does not created.');
            }
            return $this->redirect(['index']);
        }

        return $this->render('create', [
                    'model' => $model,
                    'userModel' => $userModel,
        ]);
    }

    /**
     * Updates an existing Admin model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        try {
            $model = $this->findModel($id);
            if($model->status == "DELETED")
            {
                throw new \yii\web\HttpException(505,'This record already deleted.');
            }
            $userModel = \app\models\User::find()->where(['admin_uuid' => $model->admin_uuid])->one();
            $userModel->scenario = 'admin_update';
            $old_admin_type = $model->admin_type;


            $model->attributes = Yii::$app->getRequest()->post('Admin');
            $userModel->attributes = Yii::$app->getRequest()->post('User');

            $model->email = $userModel->username = $userModel->email;
            $userModel->updated_at = $model->updated_at = time();

            if ($model->load(Yii::$app->request->post()) && $model->validate() && $userModel->load(Yii::$app->request->post()) && $userModel->validate()) {

                $userModel->first_name = $model->first_name;
                $userModel->last_name = $model->last_name;
                $userModel->phone = $model->phone;

                $model->save(false);
                $userModel->save(false);

                if ($old_admin_type != $model->admin_type) {

                    //Remove current role:
                    $manager = Yii::$app->authManager;
                    $item = $manager->getRole($old_admin_type);
                    $item = $item ?: $manager->getPermission($old_admin_type);
                    $manager->revoke($item, $userModel->id);

                    //and again update new role to user:
                    $authorRole = $manager->getRole($model->admin_type);
                    $manager->assign($authorRole, $userModel->id);
                }

                Yii::$app->getSession()->setFlash('success', 'You have successfully updated user account.');
                return $this->redirect(['index']);
            }

            return $this->render('update', [
                        'model' => $model,
                        'userModel' => $userModel,
            ]);
        } catch (\yii\base\Exception $exception) {
            throw new \yii\web\HttpException(505, $exception->getMessage());
        }
    }

    /**
     * Deletes an existing Admin model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete() {

        $id = Yii::$app->getRequest()->post('id');

        if ((($model = Admin::findOne($id)) !== null) && (($modelUser = User::find()->where(['admin_uuid' => $id])->one()) !== null)) {

            $model->updated_at = time();
            $model->status = 'DELETED';
            $model->save(false);

            //Remove current role:
            $manager = Yii::$app->authManager;
            $item = $manager->getRole($modelUser->role);
            $item = $item ?: $manager->getPermission($modelUser->role);
            $manager->revoke($item, $modelUser->id);

            $modelUser->delete();

            $return = ['code' => 200, 'message' => 'Admin account deleted successfully.'];
        } else {

            $return = ['code' => 404, 'message' => 'The requested page does not exist.'];
        }

        Yii::$app->response->format = 'json';
        return $return;
    }

    /**
     * Finds the Admin model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Admin the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Admin::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionSendActivationLink($id) {

        if (($admin = Admin::findOne($id)) !== null) {

            if ($admin->status == "INACTIVE") {

                $uuid_enc = General::encrypt_str($admin->admin_uuid);
                $email_enc = General::encrypt_str($admin->email);

                $content['name'] = $admin->first_name . ' ' . $admin->last_name;
                $content['link'] = Url::to(['site/account-activation', 't' => $uuid_enc, 'u' => $email_enc], true);

                Yii::$app->mailer->compose('account_enabled', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                        ->setFrom(Yii::$app->params['supportEmail'])
                        ->setTo($admin->email)
                        ->setSubject('Account Activation mail - ' . Yii::$app->name)
                        ->send();

                $return = ['code' => 200, 'message' => 'Activation link sent successfully.'];
            } else {
                $return = ['code' => 421, 'message' => 'Admin account already enabled'];
            }
        } else {
            $return = ['code' => 404, 'message' => 'Record does not exist.'];
        }
        Yii::$app->response->format = 'json';
        return $return;
    }

    public function actionAccountDisabled($id) {

        if (($admin = Admin::findOne($id)) !== null) {

            if ($admin->status == "ENABLED") {

                $user = \app\models\User::find()->where(['admin_uuid' => $admin->admin_uuid])->one();
                if (!empty($user)) {

                    $user->status = 0;
                    $user->save(false);

                    $admin->status = 'DISABLED';
                    $admin->save(false);

                    $return = ['code' => 200, 'message' => 'Admin account disabled successfully.'];
                } else {
                    $return = ['code' => 421, 'message' => 'User record does not exist'];
                }
            } else {
                $return = ['code' => 421, 'message' => 'Admin account not enabled.'];
            }
        } else {
            $return = ['code' => 404, 'message' => 'Record does not exist.'];
        }

        Yii::$app->response->format = 'json';
        return $return;
    }

    public function actionAccountEnabled($id) {

        if (($admin = Admin::findOne($id)) !== null) {

            if ($admin->status == "DISABLED") {

                $user = \app\models\User::find()->where(['admin_uuid' => $admin->admin_uuid])->one();
                if (!empty($user)) {

                    $user->status = 10;
                    $user->save(false);

                    $admin->status = 'ENABLED';
                    $admin->save(false);

                    $return = ['code' => 200, 'message' => 'Admin account enabled successfully.'];
                } else {
                    $return = ['code' => 421, 'message' => 'User record does not exist'];
                }
            } else {
                $return = ['code' => 421, 'message' => 'Admin account not disabled.'];
            }
        } else {
            $return = ['code' => 404, 'message' => 'Record does not exist.'];
        }

        Yii::$app->response->format = 'json';
        return $return;
    }

    /**
     * Profile an existing Admin model.
     * Only Admin table users access this page. Logged Admin access this page
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionProfile() {

        $model = $this->findModel(Yii::$app->user->identity->admin_uuid);
        $modelCP = new ChangePassword();
        $userModel = \app\models\User::findOne(Yii::$app->user->identity->id);
        $userModel->scenario = 'admin_profile';

        $model->attributes = Yii::$app->getRequest()->post('Admin');
        $userModel->attributes = Yii::$app->getRequest()->post('User');

        $model->email = $userModel->username = $userModel->email;
        $userModel->updated_at = $model->updated_at = time();

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $userModel->load(Yii::$app->request->post()) && $userModel->validate()) {

            $userModel->first_name = $model->first_name;
            $userModel->last_name = $model->last_name;
            $userModel->phone = $model->phone;

            $model->save(false);
            $userModel->save(false);

            Yii::$app->getSession()->setFlash('success', 'You have successfully updated your profile.');

            return $this->redirect(['profile']);
        }

        return $this->render('profile', [
                    'model' => $model,
                    'userModel' => $userModel,
                    'modelCP' => $modelCP,
        ]);
    }

    /**
     * Admin Change password
     * @return string
     */
    public function actionChangePassword() {
        if (!Yii::$app->request->isAjax) {
            Yii::$app->response->format = 'json';
            return ['code' => 505, 'message' => 'You are not authorised to access this page'];
        }

        $modelCP = new ChangePassword();
        if ($modelCP->load(Yii::$app->getRequest()->post()) && $modelCP->validate()) {

            $modelCP->change();
            Yii::$app->response->format = 'json';
            return ['code' => 200, 'message' => 'Password change successfully.'];
        }

        return $this->renderAjax('change-password', [
                    'modelCP' => $modelCP,
        ]);
    }

    /**
     * Owner Go TO (Switch To) Admin's Account
     * @return string
     */
    public function actionSwitchToAdmin($id) {

        $initialId = Yii::$app->user->getId();
        if (($user = User::find()->joinWith(['admin'])->where(" \"user\".\"admin_uuid\" = '$id' AND ( \"user\".\"role\" = 'ADMIN' OR \"user\".\"role\" = 'SUBADMIN' ) AND \"admin\".\"status\" ='ENABLED' ")->one()) !== null) {

            if ($user->id == $initialId) {
                Yii::$app->session->setFlash('warning', 'Sorry! Same account do not switch.');
            } elseif (empty($user)) {
                Yii::$app->session->setFlash('danger', 'Account does not exist.');
            } elseif ($user->status != 10) {
                Yii::$app->session->setFlash('danger', 'Account does not Active.');
            } else {

                $duration = 0;
                $logger_identity = \mdm\admin\models\User::findIdentity($user->id);

                Yii::$app->user->switchIdentity($logger_identity, $duration); //Change the current user.
                Yii::$app->session->set('user.idbeforeswitch.owner', $initialId); //Save in the session the id of your admin user.
                Yii::$app->session->set('user.idbeforeswitchurl.owner', Yii::$app->request->referrer);
                return $this->redirect(['/']);
            }
        } else {
            Yii::$app->session->setFlash('danger', 'Account does not exist.');
        }
        return $this->redirect(['index']);
    }

    /**
     * Owner Back TO (Switch To) his Account
     * @return string
     */
    public function actionBackToOwner() {

        $originalId = Yii::$app->session->get('user.idbeforeswitch.owner');
        $backURL = (Yii::$app->session->get('user.idbeforeswitchurl.owner') != '') ? Yii::$app->session->get('user.idbeforeswitchurl.owner') : ['/'];
        if ($originalId) {

            $logger_identity = \mdm\admin\models\User::findIdentity($originalId);

            Yii::$app->user->switchIdentity($logger_identity, 0);
            Yii::$app->session->remove('user.idbeforeswitch.owner');
            Yii::$app->session->remove('user.idbeforeswitchurl.owner');
        }else{
            Yii::$app->session->setFlash('warning','Sorry! Do not set main account.');
        }
        return $this->redirect($backURL);
    }

    /**
     * Owner/Admin/Subadmin Go TO (Switch To) Student's Account
     * @return string
     */
    public function actionSwitchToStudent($id) {

        $initialId = Yii::$app->user->getId();
        if (($user = User::find()->joinWith(['student'])->where(['user.student_uuid' => $id, 'user.role' => 'STUDENT','student.status' => 'ENABLED'])->one()) !== null) {

            if ($user->id == $initialId) {
                Yii::$app->session->setFlash('warning', 'Sorry! Same account do not switch.');
            } elseif (empty($user)) {
                Yii::$app->session->setFlash('danger', 'Account does not exist.');
            } elseif ($user->status != 10) {
                Yii::$app->session->setFlash('danger', 'Account does not Active.');
            } else {

                $current_role = Yii::$app->user->identity->role;
                $duration = 0;
                $logger_identity = \mdm\admin\models\User::findIdentity($user->id);

                Yii::$app->user->switchIdentity($logger_identity, $duration); //Change the current user.
                Yii::$app->session->set('user.idbeforeswitch.admin', $initialId); //Save in the session the id of your admin user.
                Yii::$app->session->set('user.idbeforeswitchurl.admin', Yii::$app->request->referrer);
                Yii::$app->session->set('user.idbeforeswitch.admin.role', $current_role);
                return $this->redirect(['/']);
            }
        } else {
            Yii::$app->session->setFlash('danger', 'Account does not exist.');
        }
        return $this->redirect(['index']);
    }

    /**
     * Admin Back TO (Switch To) his Account
     * @return string
     */
    public function actionBackToAdmin() {

        $originalId = Yii::$app->session->get('user.idbeforeswitch.admin');
        $backURL = (Yii::$app->session->get('user.idbeforeswitchurl.admin') != '') ? Yii::$app->session->get('user.idbeforeswitchurl.admin') : ['/'];
        if ($originalId) {

            $logger_identity = \mdm\admin\models\User::findIdentity($originalId);

            Yii::$app->user->switchIdentity($logger_identity, 0);
            Yii::$app->session->remove('user.idbeforeswitch.admin');
            Yii::$app->session->remove('user.idbeforeswitchurl.admin');
            Yii::$app->session->remove('user.idbeforeswitch.admin.role');
        }else{
            Yii::$app->session->setFlash('warning','Sorry! Do not set main account.');
        }
        return $this->redirect($backURL);
    }
    
     /**
     * Owner/Admin/Subadmin Go TO (Switch To) Tutor's Account
     * @return string
     */
    public function actionSwitchToTutor($id) {

        $initialId = Yii::$app->user->getId();
        if (($user = User::find()->joinWith(['tutor'])->where(['user.tutor_uuid' => $id, 'user.role' => 'TUTOR','tutor.status' => 'ENABLED'])->one()) !== null) {

            if ($user->id == $initialId) {
                Yii::$app->session->setFlash('warning', 'Sorry! Same account do not switch.');
            } elseif (empty($user)) {
                Yii::$app->session->setFlash('danger', 'Account does not exist.');
            } elseif ($user->status != 10) {
                Yii::$app->session->setFlash('danger', 'Account does not Active.');
            } else {

                $current_role = Yii::$app->user->identity->role;
                $duration = 0;
                $logger_identity = \mdm\admin\models\User::findIdentity($user->id);

                Yii::$app->user->switchIdentity($logger_identity, $duration); //Change the current user.
                Yii::$app->session->set('user.idbeforeswitch.admin', $initialId); //Save in the session the id of your admin user.
                Yii::$app->session->set('user.idbeforeswitchurl.admin', Yii::$app->request->referrer);
                Yii::$app->session->set('user.idbeforeswitch.admin.role', $current_role);
                return $this->redirect(['/']);
            }
        } else {
            Yii::$app->session->setFlash('danger', 'Account does not exist.');
        }
        return $this->redirect(['index']);
    }

}
