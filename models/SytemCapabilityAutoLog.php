<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sytem_capability_auto_log".
 *
 * @property string $uuid
 * @property string $tutor_uuid
 * @property string $student_uuid
 * @property string $datetime
 * @property string $status
 * @property string $message
 */
class SytemCapabilityAutoLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sytem_capability_auto_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'datetime', 'status', 'message'], 'required'],
            [['uuid', 'tutor_uuid', 'student_uuid', 'status', 'message'], 'string'],
            [['datetime'], 'safe'],
            [['uuid'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'tutor_uuid' => Yii::t('app', 'Tutor Uuid'),
            'student_uuid' => Yii::t('app', 'Student Uuid'),
            'datetime' => Yii::t('app', 'Datetime'),
            'status' => Yii::t('app', 'Status'),
            'message' => Yii::t('app', 'Message'),
        ];
    }
}
