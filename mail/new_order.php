<?php

use yii\helpers\Html;
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['student_name']; ?>,</p>
            
            <p>You have create a new order in <?= Yii::$app->name; ?>.</p>
            
            <p><strong>Order ID :</strong> #<?= $content['order_id']; ?></p>
            <p><strong>Order Date :</strong> <?= $content['order_date']; ?></p>
            <p><strong>Subtotal :</strong> $<?= $content['order_array']['total']; ?></p>
            <p><strong>Discount :</strong> $<?php echo $content['order_array']['discount']; ?></p>
            <p><strong>Total :</strong> $<?php echo $content['order_array']['grand_total']; ?></p>
                       
            <p> If any query for your account then please contact to administrator.</p>

            <p>Sincerely,</p>
            <p>System Administrators</p>
        </td>
    </tr>
</table>

