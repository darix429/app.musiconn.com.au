<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;
use app\models\OnlineTutorialPackageTutorwise;

$this->title = $model['first_name'] . ' ' . $model['last_name'] . Yii::t('app', '\'s Instrument Plan');
$this->params['breadcrumbs'][] = $this->title;


$this->registerJs(
        '
    
    
', View::POS_END
);
?>

<div class="col-xl-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left"><?= $model['first_name'] . ' ' . $model['last_name']; ?>'s Instrument Plan</h2>
            <div class="actions panel_actions float-right">
                <?php if (Yii::$app->user->identity->role != 'TUTOR') { ?>
                <a href="<?= Yii::$app->getUrlManager()->createUrl(['/tutor/index/']) ?>" class="btn btn-info btn-icon "><i class="fa fa-arrow-left text-white"></i> <span>Back</span></a>
                <?php } ?>
            </div>
        </header>
        <div class="content-body"> 
            <div class="row ">

            </div>
            <br>
            <div class=""></div>
            <div class="clearfix"></div>
            <div class="row" id="tutor_table_parent_div">
                <div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">

                    <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Instrument</th>
                                <th>Option</th>                
                                <th>Action</th>

                            </tr>
                        </thead>

                        <tbody>
                            <?php
                            if (!empty($instrumentList)) {

                                foreach ($instrumentList as $key => $value) {


                                    $option = OnlineTutorialPackageTutorwise::find()->where(['tutor_uuid' => $model['uuid'], 'instrument_uuid' => $value['instrument_uuid'], 'online_tutorial_package_type_uuid' => $model['online_tutorial_package_type_uuid']])->one();
                                    ?>
                                    <tr>
                                        <td><?= $key + 1; ?></td>
                                        <td><?= $value['instrument']['name']; ?></td>
                                        <td><?= $option->standard_plan_rate_option; ?></td>

                                        <td>
                                            <a href="<?= Url::to(['/tutor/assign-instrument-plan/', 'tutor_uuid' => $model['uuid'], 'instrument_uuid' => $value['instrument_uuid']]) ?>" class="badge badge-info badge-md" title="Update"><i class="fa fa-pencil"></i></a>



                                        </td>

                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>

                </div>            
            </div>
        </div>
    </section>
</div>

