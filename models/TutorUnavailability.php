<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tutor_unavailability}}".
 *
 * @property string $uuid
 * @property string $tutor_uuid
 * @property string $start_datetime
 * @property string $end_datetime
 * @property string $reason
 * @property string $created_at
 *
 * @property Tutor $tutorUu
 */
class TutorUnavailability extends \yii\db\ActiveRecord {
    public $dateError;
    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return '{{%tutor_unavailability}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
                //html tag filter
                [['reason', ], 'match', 'pattern' => \app\libraries\General::validTextPattern(), 'message' => 'Invalid charecters used.'], 
            
                [['uuid', 'tutor_uuid', 'start_datetime', 'end_datetime', 'reason'], 'required'],
                [['uuid', 'tutor_uuid', 'reason'], 'string'],
                [['start_datetime', 'end_datetime', 'created_at','dateError'], 'safe'],
                [['uuid'], 'unique'],
                [['tutor_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Tutor::className(), 'targetAttribute' => ['tutor_uuid' => 'uuid']],
                [['start_datetime', 'end_datetime', 'reason'], 'required', 'on' => ['unavailability']],
                [['start_datetime','end_datetime'], 'checkDateTimeExist'],
                [['start_datetime'], 'compare','compareAttribute'=>'end_datetime','operator' => '<', 'message'=>'Start date time must be less than End date time'],
                [['end_datetime'], 'compare','compareAttribute'=>'start_datetime','operator' => '>', 'message'=>'End date time must be greter than Start date time'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'tutor_uuid' => Yii::t('app', 'Tutor Uuid'),
            'start_datetime' => Yii::t('app', 'Start Datetime'),
            'end_datetime' => Yii::t('app', 'End Datetime'),
            'reason' => Yii::t('app', 'Reason'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTutorUu() {
        return $this->hasOne(Tutor::className(), ['uuid' => 'tutor_uuid']);
    }

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['unavailability'] = ['start_datetime', 'end_datetime', 'reason'];
        return $scenarios;
    }

    public function checkDateTimeExist($attribute) {

        $sdate = \app\libraries\General::convertUserToSystemTimezone($this->start_datetime, 'Y-m-d H:i:00');
        $edate = \app\libraries\General::convertUserToSystemTimezone($this->end_datetime, 'Y-m-d H:i:00');
        
        //$sdate = date("Y-m-d H:i:00",strtotime($this->start_datetime));
        //$edate = date("Y-m-d H:i:00",strtotime($this->end_datetime));

        $arrayunavailability = $this->find()->where(['tutor_uuid'=> Yii::$app->user->identity->tutor_uuid])
                            ->andWhere("('".$sdate."' = start_datetime AND '".$edate."' = end_datetime) "
                            . "or ('".$sdate."' >= start_datetime AND '".$sdate."' < end_datetime) "
                            . "or ('".$edate."' > start_datetime AND '".$edate."' <= end_datetime) "
                            . "or (start_datetime > '".$sdate."' AND end_datetime < '".$edate."')")
                            ->asArray()->all();
        
        $array = \app\models\BookedSession::find()->where(['tutor_uuid'=> Yii::$app->user->identity->tutor_uuid,'status'=>'SCHEDULE'])
                            ->andWhere("('".$sdate."' = start_datetime AND '".$edate."' = end_datetime) "
                            . "or ('".$sdate."' >= start_datetime AND '".$sdate."' < end_datetime) "
                            . "or ('".$edate."' > start_datetime AND '".$edate."' <= end_datetime) "
                            . "or (start_datetime > '".$sdate."' AND end_datetime < '".$edate."')")
                            ->asArray()->all();


        if(count($arrayunavailability)> 0){
            $this->addError('dateError', 'Already set unavailability mention above duration.');
        }elseif (count($array) > 0) {
            $this->addError('dateError', 'Assign duration has already booked tution.');
        }
    }

}
