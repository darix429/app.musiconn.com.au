<?php
namespace app\assets;

use yii\web\AssetBundle;

//New booking process
class NewAppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        
        'theme_assets/new_book/css/bootstrap.min.css',
        'theme_assets/new_book/css/font-awesome.min.css',
        'theme_assets/new_book/css/ionicons.min.css',
        'theme_assets/plugins/calendar/fullcalendar.css',
        'theme_assets/plugins/messenger/css/messenger.css',
        'theme_assets/plugins/messenger/css/messenger-theme-flat.css',
        'theme_assets/plugins/icheck/skins/all.css',
        'theme_assets/new_book/css/style.css',
        'theme_assets/new_book/css/dataTables.bootstrap.min.css',
        'theme_assets/plugins/datepicker/css/datepicker.css',
        'theme_assets/plugins/timepicker/css/bootstrap-timepicker.css',
        'theme_assets/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css',
        'theme_assets/plugins/select2/select2.css',
        'theme_assets/plugins/multi-select/css/multi-select.css',
	    'theme_assets/plugins/daterangepicker/css/daterangepicker-bs3.css',
        'theme_assets/new/custom.css',
	
    ];
    public $js = [
        'theme_assets/js/popper.min.js',
        'theme_assets/new_book/js/bootstrap.min.js',
        'theme_assets/plugins/pace/pace.js',
        'theme_assets/new_book/js/jquery.meanmenu.js',
        'theme_assets/new_book/js/owl.carousel.min.js',
        'theme_assets/new_book/js/jquery.sticky.js',
        'theme_assets/new_book/js/jquery.scrollUp.min.js',
        'theme_assets/new_book/js/adminlte.min.js',
        'theme_assets/new_book/js/main.js',
        'theme_assets/plugins/datepicker/js/datepicker.js',
        'theme_assets/plugins/daterangepicker/js/moment.min.js',
        'theme_assets/plugins/daterangepicker/js/daterangepicker.js',
        'theme_assets/plugins/timepicker/js/bootstrap-timepicker.min.js',
        'theme_assets/plugins/datetimepicker/js/bootstrap-datetimepicker.min.js',
        'theme_assets/plugins/icheck/icheck.min.js',
        'theme_assets/plugins/select2/select2.min.js',
        'theme_assets/plugins/multi-select/js/jquery.multi-select.js',
        'theme_assets/new/new_custom.js',
        'theme_assets/js/custom.js',
    ];
    public $depends = [
        
    ];
}
