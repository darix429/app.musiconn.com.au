<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Admin;

/**
 * AdminSearch represents the model behind the search form of `app\models\Admin`.
 */
class AdminSearch extends Admin
{
    public $fullname;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['admin_uuid', 'admin_type', 'first_name', 'last_name', 'email', 'phone', 'company_position', 'status','fullname'], 'safe'],
            //[['updated_at', 'created_at'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$modeDatatable = false, $statusnot = '' )
    {
        $query = Admin::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $query->andFilterWhere(['<>', 'admin_uuid', Yii::$app->user->identity->admin_uuid]);
        /*if(Yii::$app->user->identity->role == 'OWNER'){
            $query->andFilterWhere(['<>', 'admin_uuid', Yii::$app->user->identity->admin_uuid]);
        }*/

        // grid filtering conditions
//        $query->andWhere([
//            'updated_at' => $this->updated_at,
//            'created_at' => $this->created_at,
//        ]);
        
        if( !empty($statusnot)){
            if(is_array($statusnot)){
                
                $query->andWhere(['not in','status',$statusnot]);
            }else{
                
                $query->andWhere(['<>','status' , $statusnot ]);
            }
        }
        
        $query->andFilterWhere(['status' => $this->status]); 
        
        if(!empty($this->fullname)){
            $query->andFilterWhere(['ilike', "concat(first_name,' ',last_name)", $this->fullname]); 
        }
        
        $query->andFilterWhere(['ilike', 'admin_uuid', $this->admin_uuid])
            ->andFilterWhere(['ilike', 'admin_type', $this->admin_type])
            ->andFilterWhere(['ilike', 'first_name', $this->first_name])
            ->andFilterWhere(['ilike', 'last_name', $this->last_name])
            ->andFilterWhere(['ilike', 'email', $this->email])
            ->andFilterWhere(['ilike', 'phone', $this->phone])
            ->andFilterWhere(['ilike', 'company_position', $this->company_position]);
        
//        echo $query->createCommand()->getRawSql();exit;
        
        if($modeDatatable){
            $result = $query->asArray()->all();
            return $result;
        }
        return $dataProvider;
    }
}
