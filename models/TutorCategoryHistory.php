<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tutor_category_history".
 *
 * @property string $uuid
 * @property string $tutor_uuid
 * @property string $online_tutorial_package_type_uuid
 * @property string $changed_by_uuid
 * @property string $changed_by_role
 * @property string $datetime
 * @property string $old_online_tutorial_package_type_uuid
 * @property string $action ADD or EDIT
 *
 * @property OnlineTutorialPackageType $onlineTutorialPackageTypeUu
 * @property Tutor $tutorUu
 * @property TutorStandardRateHistory[] $tutorStandardRateHistories
 */
class TutorCategoryHistory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tutor_category_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'tutor_uuid', 'online_tutorial_package_type_uuid', 'changed_by_uuid', 'changed_by_role', 'old_online_tutorial_package_type_uuid'], 'required'],
            [['uuid', 'tutor_uuid', 'online_tutorial_package_type_uuid', 'changed_by_uuid', 'changed_by_role', 'old_online_tutorial_package_type_uuid', 'action'], 'string'],
            [['datetime'], 'safe'],
            [['uuid'], 'unique'],
            [['online_tutorial_package_type_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => OnlineTutorialPackageType::className(), 'targetAttribute' => ['online_tutorial_package_type_uuid' => 'uuid']],
            [['tutor_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Tutor::className(), 'targetAttribute' => ['tutor_uuid' => 'uuid']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'tutor_uuid' => Yii::t('app', 'Tutor Uuid'),
            'online_tutorial_package_type_uuid' => Yii::t('app', 'Online Tutorial Package Type Uuid'),
            'changed_by_uuid' => Yii::t('app', 'Changed By Uuid'),
            'changed_by_role' => Yii::t('app', 'Changed By Role'),
            'datetime' => Yii::t('app', 'Datetime'),
            'old_online_tutorial_package_type_uuid' => Yii::t('app', 'Old Online Tutorial Package Type Uuid'),
            'action' => Yii::t('app', 'Action'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOnlineTutorialPackageTypeUu()
    {
        return $this->hasOne(OnlineTutorialPackageType::className(), ['uuid' => 'online_tutorial_package_type_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTutorUu()
    {
        return $this->hasOne(Tutor::className(), ['uuid' => 'tutor_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTutorStandardRateHistories()
    {
        return $this->hasMany(TutorStandardRateHistory::className(), ['tutor_category_history_uuid' => 'uuid']);
    }
}
