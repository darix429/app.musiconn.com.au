<?php

namespace app\models;
use Yii;
use app\libraries\General;

/**
 * This is the model class for table "{{%booked_session}}".
 *
 * @property string $uuid
 * @property string $booking_uuid
 * @property string $credit_session_uuid
 * @property string $student_uuid
 * @property string $tutor_uuid
 * @property string $start_datetime
 * @property string $end_datetime
 * @property string $filename
 * @property int $session_min
 * @property string $session_pin
 * @property string $status
 * @property string $cancel_by
 * @property string $cancel_user_uuid
 * @property string $cancel_reason
 * @property string $cancel_datetime
 * @property string $created_at
 * @property string $updated_at
 * @property string $is_cancelled_request
 *
 * @property Student $studentUu
 * @property Tutor $tutorUu
 */
class BookedSession extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $update_file;
    public static function tableName()
    {
        return '{{%booked_session}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'booking_uuid', 'credit_session_uuid', 'student_uuid', 'tutor_uuid', 'filename', 'status', 'cancel_user_uuid', 'cancel_reason', 'instrument_uuid', 'tutorial_type_code', 'is_cancelled_request'], 'string'],
            [['student_uuid', 'tutor_uuid', 'start_datetime', 'end_datetime', 'session_min', 'instrument_uuid', 'tutorial_type_code'], 'required'],
            [['start_datetime', 'end_datetime', 'cancel_datetime', 'created_at', 'updated_at','student_login_flag','tutor_login_flag'], 'safe'],
            [['session_min'], 'default', 'value' => null],
            [['session_min'], 'integer'],
            [['session_pin'], 'string', 'max' => 4],
            [['cancel_by'], 'string', 'max' => 10],
            [['student_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['student_uuid' => 'student_uuid']],
            [['tutor_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Tutor::className(), 'targetAttribute' => ['tutor_uuid' => 'uuid']],
            [['instrument_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Instrument::className(), 'targetAttribute' => ['instrument_uuid' => 'uuid']],
            [['update_file'], 'safe', 'on' => 'UPDATE_VIDEO'],
            [['update_file'], 'file', 'extensions' => 'mp4', 'on' => 'UPDATE_VIDEO'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'booking_uuid' => Yii::t('app', 'Booking Uuid'),
            'credit_session_uuid' => Yii::t('app', 'Credit Session Uuid'),
            'student_uuid' => Yii::t('app', 'Student Uuid'),
            'instrument_uuid' => Yii::t('app', 'Instrument Uuid'),
            'tutor_uuid' => Yii::t('app', 'Tutor Uuid'),
            'start_datetime' => Yii::t('app', 'Start Datetime'),
            'end_datetime' => Yii::t('app', 'End Datetime'),
            'filename' => Yii::t('app', 'Filename'),
            'session_min' => Yii::t('app', 'Session Min'),
            'session_pin' => Yii::t('app', 'Session Pin'),
            'status' => Yii::t('app', 'Status'),
            'cancel_by' => Yii::t('app', 'Cancel By'),
            'cancel_user_uuid' => Yii::t('app', 'Cancel User Uuid'),
            'cancel_reason' => Yii::t('app', 'Cancel Reason'),
            'cancel_datetime' => Yii::t('app', 'Cancel Datetime'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentUu()
    {
        return $this->hasOne(Student::className(), ['student_uuid' => 'student_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTutorUu()
    {
        return $this->hasOne(Tutor::className(), ['uuid' => 'tutor_uuid']);
    }

    public function getInstrumentUu()
    {
        return $this->hasOne(Instrument::className(), ['uuid' => 'instrument_uuid']);
    }

    public function getStudentBookingUu()
    {
        return $this->hasOne(StudentTuitionBooking::className(), ['uuid' => 'booking_uuid']);
    }

    public function getCreditSessionUu()
    {
        return $this->hasOne(CreditSession::className(), ['uuid' => 'credit_session_uuid']);
    }

    public function getCancleLessonRequest($uuid)
    {
	$return = [];
	if (($model = BookedSession::findOne($uuid)) !== null) {
		$cancel_lesson_request = CancelSession::find()->where(['booked_session_uuid' => $uuid])->asArray()->all();
		foreach($cancel_lesson_request as $key => $value) {
			$return['cancel_by'] = $value['cancel_by'];
			$return['canceler_uuid'] = $value['canceler_uuid'];
		}	
	}
	return $return;
    }

    public static function getStartSessionLink($uuid,$user_type = 'student') {

        if (($model = BookedSession::findOne($uuid)) !== null) {

            if($user_type == 'tutor'){
                $model_user = Tutor::findOne($model->tutor_uuid);
                $user_uuid = $model_user->uuid;
            }else{
                $model_user = Student::findOne($model->student_uuid);
                $user_uuid = $model_user->student_uuid;
            }

            $url_string = $model->uuid."|".$user_uuid."|".$user_type;

            $url = Yii::$app->params['verto']['url']."?u=".urlencode(General::encrypt_str($url_string));

            return $url;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));

    }

    //cron booked session mail
    public function StartBookedSessionNotification($bookedSessionDetails) {

        $content['tutor_first_name'] = $bookedSessionDetails['tutor_first_name'];
        $content['tutor_last_name'] = $bookedSessionDetails['tutor_last_name'];
        $content['student_first_name'] = $bookedSessionDetails['student_first_name'];
        $content['student_last_name'] = $bookedSessionDetails['student_last_name'];
        $content['start_datetime'] = $bookedSessionDetails['start_datetime'];
        $content['session_pin'] = $bookedSessionDetails['session_pin'];
        $content['uuid'] = $bookedSessionDetails['uuid'];
        $content['tutor_start_datetime'] = $bookedSessionDetails['tutor_start_datetime'];

        Yii::$app->mailer->compose('booked_session_start_cron_student_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
            ->setFrom(Yii::$app->params['supportEmail'])
            ->setTo($bookedSessionDetails['student_email'])
            ->setSubject("Musiconn Reminder: A Scheduled Lesson is due to Start")
            ->send();

        Yii::$app->mailer->compose('booked_session_start_cron_tutor_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
            ->setFrom(Yii::$app->params['supportEmail'])
            ->setTo($bookedSessionDetails['tutor_email'])
            ->setSubject("Musiconn Reminder: A Scheduled Lesson is due to Start")
            ->send();

        return true;
    }

    public function cancelSession($id) {

        $return = ['code' => 421, 'message' => 'Sorry upcoming sesion not found.'];

        if (($model = BookedSession::find()->joinWith(['tutorUu','studentUu'])->where(['booked_session.uuid' => $id, 'booked_session.status' => 'SCHEDULE'])->one()) !== null) {

            $modelCreditSession = new CreditSession;
            $modelCreditSession->booked_session_uuid = $model->uuid;
            $modelCreditSession->student_uuid = $model->student_uuid;
            $modelCreditSession->tutor_uuid = $model->tutor_uuid;
            $modelCreditSession->instrument_uuid = $model->instrument_uuid;
            $modelCreditSession->status = 'PENDING';
            $modelCreditSession->tutor_uuid = $model->tutor_uuid;
            $modelCreditSession->tutorial_type_code = $model->tutorial_type_code;
            $modelCreditSession->booking_uuid = $model->booking_uuid;
            if($modelCreditSession->save(false)){

		//Cancel_session table entry
		    $modelCS = new CancelSession();
		    if(in_array(Yii::$app->user->identity->role,["OWNER","ADMIN","SUBADMIN"])){
		      $canceler_uuid = Yii::$app->user->identity->admin_uuid;
		    } else if(in_array(Yii::$app->user->identity->role,["TUTOR"])){
		      $canceler_uuid = Yii::$app->user->identity->tutor_uuid;
		    }else{
		      $canceler_uuid = Yii::$app->user->identity->student_uuid;
		    }
		    $cancel_reason = 'TUTOR CANCELLED LESSON';

		    $modelCS->booked_session_uuid = $model->uuid;
		    $modelCS->student_uuid = $model->student_uuid;
		    $modelCS->tutor_uuid = $model->tutor_uuid;
		    $modelCS->cancel_by = Yii::$app->user->identity->role;
		    $modelCS->canceler_uuid = $canceler_uuid;
		    $modelCS->created_at = Yii::$app->formatter->asDate(time(), 'php:Y-m-d H:i:s');
		    $modelCS->approved_by = Yii::$app->user->identity->role;
		    $modelCS->approver_uuid = $canceler_uuid;
		    $modelCS->approve_datetime = Yii::$app->formatter->asDate(time(), 'php:Y-m-d H:i:s');
		    $modelCS->cancel_note = Yii::$app->user->identity->role." APPROVE CANCELLED LESSON";
		    $modelCS->status = "APPROVED";
		    $modelCS->save(false);

                //cancel status existing session
                $model->status = 'CANCELLED';
                $model->cancel_by = Yii::$app->user->identity->role;
                $model->cancel_user_uuid = $canceler_uuid;
                $model->cancel_reason = $cancel_reason;
                $model->cancel_datetime = date('Y-m-d H:i:s');
                $model->is_cancelled_request = 0;
                $model->save(false);

                $instrument_name = '';
                if (($modelInstrument = Instrument::findOne($modelCreditSession->instrument_uuid)) !== null) {
                    $instrument_name = $modelInstrument->name;
                }

                $content_tutor['tutor_name'] = $model->tutorUu->first_name ." ".$model->tutorUu->last_name;
                $content_tutor['student_name'] = $model->studentUu->first_name ." ".$model->studentUu->last_name;
                $content_tutor['session_date'] = General::displayDate($model->start_datetime);
                $content_tutor['session_time'] = General::displayTime($model->start_datetime)." To ".General::displayTime($model->end_datetime);
                $content_tutor['instrument'] = $instrument_name;
                $content_tutor['tution_duration'] = $model->session_min;

                $content_student['tutor_name'] = $model->tutorUu->first_name ." ".$model->tutorUu->last_name;
                $content_student['student_name'] = $model->studentUu->first_name ." ".$model->studentUu->last_name;
                $content_student['session_date'] = General::displayDate($model->start_datetime);
                $content_student['session_time'] = General::displayTime($model->start_datetime)." To ".General::displayTime($model->end_datetime);
                $content_student['instrument'] = $instrument_name;
                $content_student['tution_duration'] = $model->session_min;

                $ownersEmail_tutor = General::getOwnersEmails(['OWNER','ADMIN','SUBADMIN']);

                Yii::$app->mailer->compose('cancel_booked_session_tutor', ['content' => $content_tutor], ['htmlLayout' => 'layouts/html'])
                        ->setFrom(Yii::$app->params['supportEmail'])
                        ->setTo($model->tutorUu->email)
                        ->setBcc($ownersEmail_tutor)
                        ->setSubject('Cancel Booked Lesson - ' . Yii::$app->name)
                        ->send();

                Yii::$app->mailer->compose('cancel_booked_session_student', ['content' => $content_student], ['htmlLayout' => 'layouts/html'])
                        ->setFrom(Yii::$app->params['supportEmail'])
                        ->setTo($model->studentUu->email)
                        ->setBcc($ownersEmail_tutor)
                        ->setSubject('Cancel Booked Lesson - ' . Yii::$app->name)
                        ->send();

                $return = ['code' => 200, 'message' => 'Your booked Lesson successfully cancelled.'];
            }

        }

        return $return;
    }
    
    public function lessonCompletedMail($booked_session_uuid,$f = "==") {

        if (($model = BookedSession::find()->joinWith(['studentUu','tutorUu'])->where(['booked_session.uuid' => $booked_session_uuid])->one() ) !== null) {
            
            $student_name = $model->studentUu->first_name.' '.$model->studentUu->last_name;
            $tutor_name = $model->tutorUu->first_name.' '.$model->tutorUu->last_name;
            
            $student_user_model = User::find()->where(['student_uuid' => $model->student_uuid])->one();
            $tutor_user_model = User::find()->where(['tutor_uuid' => $model->tutor_uuid])->one();
            $student_tz = (!empty($student_user_model))? $student_user_model->timezone : Yii::$app->params['timezone'];
            $tutor_tz = (!empty($tutor_user_model))? $tutor_user_model->timezone : Yii::$app->params['timezone'];
            
            $student_time = General::convertTimezone($model->start_datetime,Yii::$app->params['timezone'],$student_tz,'d-m-Y H:i T').' - '.General::convertTimezone($model->end_datetime,Yii::$app->params['timezone'],$student_tz,'d-m-Y H:i T');
            $tutor_time = General::convertTimezone($model->start_datetime,Yii::$app->params['timezone'],$tutor_tz,'d-m-Y H:i T').' - '.General::convertTimezone($model->end_datetime,Yii::$app->params['timezone'],$tutor_tz,'d-m-Y H:i T');
            
            $link = Yii::$app->params['base_url'].'video/tution-view&id='.$booked_session_uuid;
            $duration = $model->session_min;

            $content['student_name'] = $student_name;
            $content['tutor_name'] = $tutor_name;
            $content['time'] = $student_time;
            $content['link'] = $link;
            $content['tution_duration'] = $duration;

            $content_tutor['student_name'] = $student_name;
            $content_tutor['tutor_name'] = $tutor_name;
            $content_tutor['time'] = $tutor_time;
            $content_tutor['link'] = $link;
            $content_tutor['tution_duration'] = $duration;

            $ownersEmail = General::getOwnersEmails(['OWNER', 'ADMIN']);

            if($model->studentUu->isMonthlySubscription == 1){
                Yii::$app->mailer->compose('tuition_completed_student', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                        ->setFrom(Yii::$app->params['supportEmail'])
                        ->setTo($model->studentUu->email)
                        ->setBcc($ownersEmail)
                        ->setSubject('Lesson Completed:  Recording of your lesson will be available for viewing soon!')
                        ->send();
            }

            Yii::$app->mailer->compose('tuition_completed_tutor', ['content' => $content_tutor], ['htmlLayout' => 'layouts/html'])
                    ->setFrom(Yii::$app->params['supportEmail'])
                    ->setTo($model->tutorUu->email)
                    ->setBcc(General::getOwnersEmails(['OWNER']))
                    ->setSubject('Lesson Completed - ' . Yii::$app->name)
                    ->send();
        }

        return true;
    }
    
    public function instrumentCompletedLessonCount($id = '') {
        
        $where = '1=1';
        if(!empty(Yii::$app->user->identity->student_uuid)){
            $where = " (student_uuid = '".Yii::$app->user->identity->student_uuid."')";
        }
        $statusIn = ['COMPLETED','PUBLISHED'];
        $res = BookedSession::find()->where(['instrument_uuid' => $id,])->andWhere($where)
                ->andWhere(['in', 'booked_session.status', $statusIn])->count();
        return $res;
    }
}
