<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">

    <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Item Name</th>
                <th>Price</th>
                <th>Tax</th>
                <th>Total Price</th>
                <th>Order ID</th>
                <th>Start Date</th>
		<th>End Date</th>
		
            </tr>
        </thead>

        <tbody>
            <?php
            if (!empty($monthlySubList)) {
                foreach ($monthlySubList as $key => $value) {
                    //StudentMonthlySubscription::getOrderID();
                    ?>
                    <tr>
                        <td><?= $key + 1; ?></td>
                        <td><?= $value['name']; ?></td>
                        <td><?= $value['price']; ?></td>
			<td><?= $value['tax']; ?></td>
			<td><?= $value['total_price']; ?></td>
                        <td><?= (!empty($value['orderUu']['order_id']))?"#".$value['orderUu']['order_id']:''; ?></td>
                        <td><?= \app\libraries\General::displayDate($value['start_datetime']); ?></td>
			<td><?= \app\libraries\General::displayDate($value['end_datetime']); ?></td>
                        
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>

</div>
