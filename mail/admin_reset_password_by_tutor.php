<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Hello <?= $content['name'] ?>,</p>
            <p>Admin have reset your account password.</p>

            <p><strong>Username :</strong> <?= $content['username']; ?></p>
            <p><strong>Password :</strong> <?= $content['password']; ?></p>

            <p>Sincerely,</p>
            <p>System Administrators</p>
        </td>
    </tr>
</table>
