<?php

use yii\helpers\Html;

?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['student_name']; ?>,</p>
            
            <p>Your request <?= $content['status']; ?>  for <strong>Reschedule Reference :</strong> <?= "#".$content['reschedule_number']; ?>.</p>
            
            
            
            
            <p>Regards,<br>
            Admin Team – Musiconn Pty Ltd</p>
        </td>
    </tr>
</table>

