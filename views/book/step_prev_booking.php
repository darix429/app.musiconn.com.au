<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Url;
use app\libraries\General;

$prev_booking_list = \app\models\StudentTuitionBooking::find()->joinWith(['studentUu','instrumentUu','tutorUu'])->where(["student_tuition_booking.student_uuid" => $model->student_uuid])->asArray()->orderBy(['booking_id' => SORT_DESC])->limit(3)->all();
$this->registerJs(
        '

        ', View::POS_END
);
?>

<?php if(!empty($prev_booking_list)){ ?>
<section class="content">
    <div class="row">
        <div class="col-md-12">
        <label class="warning-info mar-bottom-5">For issues concerning tutor availability, please contact <a href="mailto:<?= Yii::$app->params['adminEmail']; ?>"><?= Yii::$app->params['adminEmail']; ?></a> for assistance.</label>

            <div class="panel-group box-header with-border" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">PREVIOUS BOOKINGS</h4>
                    </div>
                    <div id="" class="">
                        <div class="panel-body">
                            <?php foreach ($prev_booking_list as $prev_book) { ?>
                            
                                <div class="col-md-4">
                                    <div class="">
                                        <ul class="products-list product-list-in-box">
                                            <li class="item">
                                                <div class="product-img">
                                                    <?php
                                                    $inst_img_dataUri = \app\libraries\General::getInstrumentImage($prev_book['instrument_uuid']);
                                                    echo "<img src='$inst_img_dataUri'/>";
                                                    ?>
                                                </div>
                                                <div class="product-info">
                                                    <div><a  class="product-title"><strong>Instrument:</strong> <?= $prev_book['instrumentUu']['name']; ?></a></div>
                                                    <div><a  class="product-title"><strong>Tutor:</strong> <?= $prev_book['tutorUu']['first_name']." ".$prev_book['tutorUu']['last_name']; ?></a></div>
                                                    <div><a  class="product-title"><strong>Length of lesson:</strong> <?= $prev_book['tutorial_min']; ?> Minutes</a></div><br>
                                                    <a href="<?= Yii::$app->getUrlManager()->createUrl(['/book/book-again','id'=> $prev_book['uuid'] ]); ?>"><div class="label label-warning hm-1 mr-2">BOOK AGAIN</div></a>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            <?php } ?>
                            
<!--                            <div class="col-md-3">
                                <div class="see_mr">
                                    <ul class="products-list product-list-in-box">
                                        <li class="item">
                                            <div class="product-info">
                                                <a href="<?= Yii::$app->getUrlManager()->createUrl('/student/home'); ?>"><div class="label label-warning hm-1 mr-2">SEE MORE</div></a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php } ?>