<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CancelSession */

$this->title = 'Update Cancel Session: ' . $model->uuid;
$this->params['breadcrumbs'][] = ['label' => 'Cancel Sessions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->uuid, 'url' => ['view', 'id' => $model->uuid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cancel-session-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
