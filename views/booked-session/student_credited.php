<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;
use app\models\Tutor;
use app\models\BookedSession;
use app\libraries\General;
use app\models\CreditSession;

$this->title = Yii::t('app', 'CREDITED LESSONS');
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_icon'] = 'my-lesson-tag.png';
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <?php /*<div class="row center-block box-search-area">
                        <div class="col-lg-12 col-md-12 col-12 ">
                            <div class="datatable-search-div " style="display: block; margin-bottom: 10px;">
                                <?php
                                $s_c_count = \app\models\CreditSession::getStudentCreditSessionCount(Yii::$app->user->identity->student_uuid);
                                ?>
                                <?= Html::Button(Yii::t('app', 'SCHEDULE A NEW LESSON'), ['class' => 'btn btn-primary mb-0','data-creditsession' => $s_c_count, 'id' => 'studentBookingOption_button', ]) ?>
                                
                                <label class="pull-right" style="cursor: default;" style="cursor: default;">Lesson Credits = <?= $s_c_count ?> </label>

                            </div>
                        </div>
                    </div> */ ?>
                    <div class="clearfix"></div>
                    <div class="row" >
                        <!---start---->
                        <div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">
                            <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Type</th>
                                        <th>Total Credit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $all_type_code_array = array_column($creditedList, 'tutorial_type_code');
                                    $type_code_array = array_count_values($all_type_code_array);

                                    if (!empty($type_code_array)) {
                                        $row = 0;
                                        foreach ($type_code_array as $key => $value) {
                                            $row++;
                                            $TutorialType = \app\models\TutorialType::find()->where(['code' => $key])->one();
                                            ?>
                                            <tr>
                                                <td><?= $row; ?></td>
                                                <td><?= (!empty($TutorialType)) ? $TutorialType->description : $key; ?></td>
                                                <td><?= $value; ?></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>

                        </div>
                        <!---end--->
                    </div>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <a class="btn btn-primary btn-sm black_button_in" href="<?= Yii::$app->getUrlManager()->createUrl('/student/home'); ?>">RETURN TO HOME</a>

</section>