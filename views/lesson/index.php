<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Url;
use app\libraries\General;
use app\models\StudentTuitionBooking;

$this->title = Yii::t('app', 'BOOK A MUSIC LESSON');

$this->registerJs(
        '
   
        ', View::POS_END
);
?>
<style>
    .conference_frame{
        width: 100%;
    }
    .lesson_message_div a{
	color: red;	
    }
</style>
<script
    src="https://code.jquery.com/jquery-3.4.1.js"
    integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
crossorigin="anonymous"></script>

<?php
if (!empty($result['success']) && $result['success'] == true) {
    
    if (!$reload_flag) {
        
        $this->registerJs(
                ' 
                var header_height = 5;
                var height1 = screen.height;
                //var frm_height = (height1 - header_height);
		var body_height = $(window).height();
		var frm_height = (body_height - header_height);

                $(".ifra_test").css("height", frm_height + "px");
                
                //close current window
                function main_wind_close(){
                    window.top.close();
                }
        ', View::POS_END
        );
        ?>
        <iframe class="conference_frame ifra_test" id="conference_frame" src="<?= $frame_url; ?>" allow="microphone; camera;"></iframe>

        <?php
    } else {

        $this->registerJs(
                '
            setInterval(function() {
                  window.location.reload();
                }, 10000);
        ', View::POS_END
        );
        ?>
        <div class="row pull-left lesson_logo_asign">
            <a href="#">
                <img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new/img/login-logo.png' ?>">
            </a>
        </div>
        <div class="lesson_message_div">
            <div class="login-box">
                <div class="login-box-body lesson_hi">
                    <?php
                    $name = (!empty($result['userData']['first_name'])) ? $result['userData']['first_name'] . ' ' . $result['userData']['last_name'] : '';
                    $msg = 'Please wait, Tutor will be join soon..';

                    echo '<h4>Hi ' . $name . '</h4>';
                    echo '<br>';
                    echo '<h4>' . $msg . '</h4>';
                    ?>
                </div>
            </div>
        </div>
    <?php } ?>
<?php } else { ?>

    <div class="row pull-left lesson_logo_asign">
        <a href="#">
            <img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new/img/login-logo.png' ?>">
        </a>
    </div>
    <div class="lesson_message_div">
        <div class="login-box">
            <div class="login-box-body lesson_hi">
                <?php
                $name = (!empty($result['userData']['first_name'])) ? $result['userData']['first_name'] . ' ' . $result['userData']['last_name'] : '';
                $msg = (!empty($result['message'])) ? $result['message'] : '';

                echo '<h4>Hi ' . $name . '</h4>';
                echo '<br>';
                echo '<h4>' . $msg . '</h4>';
                ?>
            </div>
        </div>
    </div>
<?php } ?>