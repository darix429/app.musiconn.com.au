<?php

namespace app\controllers;

use Yii;
use app\models\MusicoinPackage;
use app\models\MusicoinPackageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * MusicoinPackageController implements the CRUD actions for MusicoinPackage model.
 */
class MusicoinPackageController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                        [
                        'actions' => ['index'],
                        'allow' => Yii::$app->user->can('/musicoin-package/index'),
                        'roles' => ['@'],
                    ],
			[
                        'actions' => ['package-enabled'],
                        'allow' => Yii::$app->user->can('/musicoin-package/package-enabled'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['package-disabled'],
                        'allow' => Yii::$app->user->can('/musicoin-package/package-disabled'),
                        'roles' => ['@'],
                    ],
			[
                        'actions' => ['create'],
                        'allow' => Yii::$app->user->can('/musicoin-package/create'),
                        'roles' => ['@'],
                    ],
			[
                        'actions' => ['update'],
                        'allow' => Yii::$app->user->can('/musicoin-package/update'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['delete'],
                        'allow' => Yii::$app->user->can('/musicoin-package/delete'),
                        'roles' => ['@'],
                    ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MusicoinPackage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MusicoinPackageSearch();
        $searchModel->status = 'ENABLED';
        $packageList = $searchModel->search(Yii::$app->request->queryParams,true);

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_packages_list', [
                        'packageList' => $packageList,
                        'searchModel' => $searchModel,
            ]);
        } else {
            return $this->render('index', [
                        'packageList' => $packageList,
                        'searchModel' => $searchModel,
            ]);
        }
    }

    /**
     * Displays a single MusicoinPackage model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MusicoinPackage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MusicoinPackage();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
            $model->created_at = $model->updated_at = date('Y-m-d H:i:s', time());
            $model->save(false);
            Yii::$app->session->setFlash('success', Yii::t('app', 'Musicoin package created successfully.'));
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MusicoinPackage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
            $model->updated_at = date('Y-m-d H:i:s', time());
            $model->save(false);
            Yii::$app->session->setFlash('success', Yii::t('app', 'Musicoin package updated successfully.'));
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing MusicoinPackage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete()
    {
        $id = Yii::$app->getRequest()->post('id');
        $model = $this->findModel($id);
        if(!empty($model)){
            $model->delete();
            $return = ['code' => 200, 'message' => 'Musicoin package deleted successfully.'];
        }else {
            $return = ['code' => 404, 'message' => 'The requested page does not exist.'];
        }

        Yii::$app->response->format = 'json';
        return $return;
    }
    
    public function actionPackageDisabled($id) {

        if (($model = MusicoinPackage::findOne($id)) !== null) {

            if ($model->status == "ENABLED") {
                
                    $model->status = 'DISABLED';
                    $model->save(false);

                    $return = ['code' => 200, 'message' => 'Musicoin package disabled successfully.'];
            } else {
                $return = ['code' => 421, 'message' => 'Musicoin package not disabled.'];
            }
        } else {
            $return = ['code' => 404, 'message' => 'Record does not exist.'];
        }

        Yii::$app->response->format = 'json';
        return $return;
    }

    public function actionPackageEnabled($id) {

        if (($model = MusicoinPackage::findOne($id)) !== null) {

            if ($model->status == "DISABLED") {
                
                    $model->status = 'ENABLED';
                    $model->save(false);

                    $return = ['code' => 200, 'message' => 'Musicoin package enabled successfully.'];
            } else {
                $return = ['code' => 421, 'message' => 'Musicoin package not enabled.'];
            }
        } else {
            $return = ['code' => 404, 'message' => 'Record does not exist.'];
        }

        Yii::$app->response->format = 'json';
        return $return;
    }

    /**
     * Finds the MusicoinPackage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return MusicoinPackage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MusicoinPackage::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
