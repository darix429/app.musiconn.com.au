<?php
/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
use yii\helpers\Url;
$this->title = $name;
?>
<div class="col-xl-12">
    <section class="box nobox">
        <div class="content-body">    <div class="row">
                <div class="col-lg-12 col-md-12 col-12">

                    <h1 class="page_error_code text-purple"><?= $exception->statusCode; ?></h1>
                    <h1 class="page_error_info"><?= (Html::encode($message)) ?></h1>

                    <div class="col-12">
                        <div class="row justify-content-center">
                            <form action="javascript:;" method="post" class="page_error_search" style="width: 60%;">
                                <!--                                        <div class="input-group transparent">
                                                                            <span class="input-group-addon">
                                                                                <i class="fa fa-search icon-purple"></i>
                                                                            </span>
                                                                            <input type="text" class="form-control" placeholder="Try a new search">
                                                                            <input type='submit' value="">
                                                                        </div>-->
                                <div class="text-center page_error_btn">
                                    <a class="btn btn-purple btn-lg" href='<?= Url::to(['/'])?>'><i class='fa fa-location-arrow'></i> &nbsp; Back to Home</a>
                                </div>
                            </form>
                        </div></div>

                </div>
            </div>
        </div>
    </section>
</div>
