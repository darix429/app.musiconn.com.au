<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CancelSessionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cancel-session-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'uuid') ?>

    <?= $form->field($model, 'booked_session_uuid') ?>

    <?= $form->field($model, 'student_uuid') ?>

    <?= $form->field($model, 'tutor_uuid') ?>

    <?= $form->field($model, 'cancel_by') ?>

    <?php // echo $form->field($model, 'canceler_uuid') ?>

    <?php // echo $form->field($model, 'approved_by') ?>

    <?php // echo $form->field($model, 'approver_uuid') ?>

    <?php // echo $form->field($model, 'approve_datetime') ?>

    <?php // echo $form->field($model, 'rejected_by') ?>

    <?php // echo $form->field($model, 'rejecter_uuid') ?>

    <?php // echo $form->field($model, 'reject_datetime') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'cancel_note') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
