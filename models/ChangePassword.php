<?php

namespace app\models;

use Yii;
use app\models\User;
use yii\base\Model;

/**
 * Description of ChangePassword
 *
 * @author Misbahul D Munir <misbahuldmunir@gmail.com>
 * @since 1.0
 */
class ChangePassword extends Model
{
    public $oldPassword;
    public $newPassword;
    public $retypePassword;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['oldPassword', 'newPassword', 'retypePassword'], 'required'],
            [['oldPassword'], 'validatePassword'],
            [['newPassword'], 'string', 'min' => 6, 'max' => 64],
            [['retypePassword'], 'compare', 'compareAttribute' => 'newPassword'],
            [['newPassword'], 'match', 'pattern' => '/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d!@#$%^&*+_-]{6,}$/', 'message' => 'Password must contain upper and lower case letters and at least 1 number. (eg. TeSt&12)'],
            
        ];
    }
    
    public function attributeLabels() {
        return [
            'oldPassword' => Yii::t('app', 'Current Password'),
            'newPassword' => Yii::t('app', 'New Password'),
            'retypePassword' => Yii::t('app', 'Retype New Password'),
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     */
    public function validatePassword()
    {
        /* @var $user User */
        $user = Yii::$app->user->identity;
        if (!$user || !$user->validatePassword($this->oldPassword)) {
            $this->addError('oldPassword', 'Incorrect current password.');
        }
    }

    /**
     * Change password.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function change()
    {
        if ($this->validate()) {
            /* @var $user User */
            $user = Yii::$app->user->identity; 
            $user->setPassword($this->newPassword);
            $user->generateAuthKey();
            if ($user->save()) {
                return true;
            }
        }

        return false;
    }
    
    public function tutorChangePasswordByAdmin($record)
    {
        $user = \app\models\User::find()->where(['email' => $record->email])->one(); 
        
            $user->password_hash = Yii::$app->security->generatePasswordHash($this->newPassword);
            if ($user->save()) {
                
                $content['name'] = $user->first_name . ' ' . $user->last_name;
                $content['username'] = $user->username;
                $content['password'] = $this->newPassword;
                
                Yii::$app->mailer->compose('admin_reset_password_by_tutor', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                                ->setFrom(Yii::$app->params['supportEmail'])
                                ->setTo($user->email)
                                ->setSubject('Admin Reset Your Password - ' . Yii::$app->name)
                                ->send();
                
                return true;
            }
        
        return false;
    }
}
