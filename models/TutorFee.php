<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tutor_fee".
 *
 * @property string $uuid
 * @property string $online_tutorial_package_type_uuid
 * @property string $option
 * @property int $sesson_length
 * @property string $tutor_fee
 */
class TutorFee extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tutor_fee';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'online_tutorial_package_type_uuid', 'sesson_length'], 'required'],
            [['uuid', 'online_tutorial_package_type_uuid'], 'string'],
            [['option', 'tutor_fee'], 'number'],
            [['sesson_length'], 'default', 'value' => null],
            [['sesson_length'], 'integer'],
            [['uuid'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'online_tutorial_package_type_uuid' => Yii::t('app', 'Online Tutorial Package Type Uuid'),
            'option' => Yii::t('app', 'Option'),
            'sesson_length' => Yii::t('app', 'Sesson Length'),
            'tutor_fee' => Yii::t('app', 'Tutor Fee'),
        ];
    }
}
