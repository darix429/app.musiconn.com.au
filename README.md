Command line instructions


Create a new repository
git clone http://git.vindaloovoip.com/musiconn/web-application.git
cd web-application
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder
cd existing_folder
git init
git remote add origin http://git.vindaloovoip.com/musiconn/web-application.git
git add .
git commit -m "Initial commit"
git push -u origin master

Existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin http://git.vindaloovoip.com/musiconn/web-application.git
git push -u origin --all
git push -u origin --tags