<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;
use app\models\User;
use app\models\Admin;

$this->title = Yii::t('app', 'Uploaded Video Report');
$this->params['breadcrumbs'][] = $this->title;

$uploadedResult = [];
$publishedResult = [];

$uploaded_by_admin = User::find()->joinWith(['admin'])->where(['admin.status' => 'ENABLED'])->asArray()->all();
$uploaded_by_tutor = User::find()->joinWith(['tutor'])->where(['tutor.status' => 'ENABLED'])->asArray()->all();
$uploaded_by = array_merge($uploaded_by_admin, $uploaded_by_tutor);
foreach ($uploaded_by as $key => $value) {
    if ($value['role'] == 'ADMIN' || $value['role'] == 'SUBADMIN' || $value['role'] == 'OWNER') {
        $uploadedResult[$value['admin_uuid'] . '_' . $value['role']] = $value['first_name'] . " " . $value['last_name'] . '(' . $value['role'] . ')';
    } else if ($value['role'] == 'TUTOR') {
        $uploadedResult[$value['tutor_uuid'] . '_' . $value['role']] = $value['first_name'] . " " . $value['last_name'] . '(' . $value['role'] . ')';
    } else {
        $uploadedResult[$value['student_uuid'] . '_' . $value['role']] = $value['first_name'] . " " . $value['last_name'] . '(' . $value['role'] . ')';
    }
}

$published_by = User::find()->joinWith(['admin'])->where(['admin.status' => 'ENABLED'])->asArray()->all();
foreach ($published_by as $key => $value) {
    $publishedResult[$value['admin_uuid']] = $value['first_name'] . " " . $value['last_name'] . '(' . $value['role'] . ')';
}

$this->registerJs(
        '

$(document).on("click","#search_btn", function(){
    $.ajax({
        type: "GET",
        url: "' . Url::to(['/video/report/']) . '",
        data: $("#search_frm").serialize(),
        beforeSend:function(){ blockBody(); },
        success:function(result){
            console.log(result);
            unblockBody();
            $("#coupon_table_parent_div").html(result);
            window.ULTRA_SETTINGS.dataTablesInit();
        },
        error:function(e){
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});

$(document).on("click","#search_reset_btn", function(){
    $.ajax({
        type: "GET",
        url: "' . Url::to(['/video/report/']) . '",
        data: {},
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            $("#coupon_table_parent_div").html(result);
            window.ULTRA_SETTINGS.dataTablesInit();
        },
        error:function(e){
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});


', View::POS_END
);
?>

<div class="col-xl-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Uploaded Video History</h2>
            <div class="actions panel_actions float-right">
                <i class="fa fa-search icon-primary icon-xs icon-orange mail_head_icon custom-toggle" data-togglediv="datatable-search-div" title="Advance Search"></i>

            </div>
        </header>
        <div class="content-body">
            <div class="row ">
                <div class="col-lg-12 col-md-12 col-12 ">
                    <div class="datatable-search-div " style="display: none;">
                        <?php
                        $form = ActiveForm::begin([
                                    'method' => 'get',
                                    'options' => [
                                        'id' => 'search_frm',
                                        'class' => 'form-inline'
                                    ],
                        ]);
                        ?>

                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'title')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Title'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'uploaded_date')->textInput(['class' => 'form-control custom_date_picker', 'maxlength' => true, 'placeholder' => 'Uploaded Date'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'publish_date')->textInput(['class' => 'form-control custom_date_picker', 'maxlength' => true, 'placeholder' => 'Publish Date'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'is_free')->dropDownList([0 => 'No', 1 => 'Yes'], ['prompt' => 'Select isFree', 'class' => 'form-control select2', 'style' => 'width: 100%'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'status')->dropDownList(['ALL' => 'ALL', 'PUBLISHED' => 'PUBLISHED', 'UPLOADED' => 'UPLOADED'], ['class' => 'form-control', 'style' => 'width:100%'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'uploaded_by')->dropDownList($uploadedResult, ['prompt' => 'Select Uploaded By', 'class' => 'form-control myselect2', 'style' => 'width: 100%'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'published_by')->dropDownList($publishedResult, ['prompt' => 'Select Published By', 'class' => 'form-control myselect2', 'style' => 'width: 100%'])->label(false) ?>
                        </div>

                        <?= Html::Button(Yii::t('app', 'Search'), ['class' => 'btn btn-primary mb-0', 'id' => 'search_btn', 'style' => 'margin-left: 17px;']) ?>
                        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-purple mb-0', 'id' => 'search_reset_btn']) ?>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
            <br>
            <div class=""></div>
            <div class="clearfix"></div>
            <div class="row" id="coupon_table_parent_div">
                <?php echo Yii::$app->controller->renderPartial('_report_list', ['report' => $report]); ?>
            </div>
        </div>
    </section>
</div>
