<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%reschedule_session}}".
 *
 * @property string $uuid
 * @property string $booked_session_uuid
 * @property string $student_uuid
 * @property string $tutor_uuid
 * @property string $from_start_datetime
 * @property string $from_end_datetime
 * @property string $to_start_datetime
 * @property string $to_end_datetime
 * @property string $request_by
 * @property string $requester_uuid
 * @property string $approved_by
 * @property string $approver_uuid
 * @property string $approve_datetime
 * @property string $status
 * @property string $rejected_by
 * @property string $rejecter_uuid
 * @property string $reject_datetime
 * @property string $referance_number
 * @property string $created_at
 *
 * @property BookedSession $bookedSessionUu
 * @property Student $studentUu
 * @property Tutor $tutorUu
 */
class RescheduleSession extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%reschedule_session}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'booked_session_uuid', 'student_uuid', 'tutor_uuid', 'from_start_datetime', 'from_end_datetime', 'to_start_datetime', 'to_end_datetime', 'request_by', 'requester_uuid', 'referance_number'], 'required'],
            [['uuid', 'booked_session_uuid', 'student_uuid', 'tutor_uuid', 'request_by', 'requester_uuid', 'approved_by', 'approver_uuid', 'status', 'rejected_by', 'rejecter_uuid', 'referance_number'], 'string'],
            [['from_start_datetime', 'from_end_datetime', 'to_start_datetime', 'to_end_datetime', 'approve_datetime', 'reject_datetime', 'created_at'], 'safe'],
            [['uuid'], 'unique'],
            [['booked_session_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => BookedSession::className(), 'targetAttribute' => ['booked_session_uuid' => 'uuid']],
            [['student_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['student_uuid' => 'student_uuid']],
            [['tutor_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Tutor::className(), 'targetAttribute' => ['tutor_uuid' => 'uuid']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'booked_session_uuid' => Yii::t('app', 'Booked Session Uuid'),
            'student_uuid' => Yii::t('app', 'Student Uuid'),
            'tutor_uuid' => Yii::t('app', 'Tutor Uuid'),
            'from_start_datetime' => Yii::t('app', 'From Start Datetime'),
            'from_end_datetime' => Yii::t('app', 'From End Datetime'),
            'to_start_datetime' => Yii::t('app', 'To Start Datetime'),
            'to_end_datetime' => Yii::t('app', 'To End Datetime'),
            'request_by' => Yii::t('app', 'Request By'),
            'requester_uuid' => Yii::t('app', 'Requester Uuid'),
            'approved_by' => Yii::t('app', 'Approved By'),
            'approver_uuid' => Yii::t('app', 'Approver Uuid'),
            'approve_datetime' => Yii::t('app', 'Approve Datetime'),
            'status' => Yii::t('app', 'Status'),
            'rejected_by' => Yii::t('app', 'Rejected By'),
            'rejecter_uuid' => Yii::t('app', 'Rejecter Uuid'),
            'reject_datetime' => Yii::t('app', 'Reject Datetime'),
            'referance_number' => Yii::t('app', 'Referance Number'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookedSessionUu()
    {
        return $this->hasOne(BookedSession::className(), ['uuid' => 'booked_session_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentUu()
    {
        return $this->hasOne(Student::className(), ['student_uuid' => 'student_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTutorUu()
    {
        return $this->hasOne(Tutor::className(), ['uuid' => 'tutor_uuid']);
    }
}
