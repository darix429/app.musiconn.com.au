<?php

namespace app\controllers;

use Yii;
use app\models\TutorialType;
use app\models\TutorialTypeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
//use app\models\User;
use app\libraries\General;
use yii\helpers\Url;
use yii\filters\AccessControl;
use app\models\ChangePassword;

/**
 * TutorialTypeController implements the CRUD actions for TutorialType model.
 */
class TutorialTypeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
	    'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                        [
                        'actions' => ['index'],
                        'allow' => Yii::$app->user->can('/tutorial-type/index'),
                        'roles' => ['@'],
                    ],
			[
                        'actions' => ['enabled'],
                        'allow' => Yii::$app->user->can('/tutorial-type/enabled'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['disabled'],
                        'allow' => Yii::$app->user->can('/tutorial-type/disabled'),
                        'roles' => ['@'],
                    ],
                        
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TutorialType models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TutorialTypeSearch();
        $searchModel->status = "ENABLED";
	$tutorialTypeList = $searchModel->search(Yii::$app->request->queryParams, true);
	if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_tutorial_type_list', [
                        'tutorialTypeList' => $tutorialTypeList,
            ]);
        } else {
            return $this->render('index', [
                        //'inactiveAdmin' => $inactiveAdmin,
                        'tutorialTypeList' => $tutorialTypeList,
                        'searchModel' => $searchModel,
            ]);
        }
    }

    /**
     * Displays a single TutorialType model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TutorialType model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    /*public function actionCreate()
    {
        $model = new TutorialType();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->uuid]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }*/

    /**
     * Updates an existing TutorialType model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    /*public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->uuid]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }*/

    /**
     * Deletes an existing TutorialType model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    /*public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }*/

    /**
     * Finds the TutorialType model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return TutorialType the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TutorialType::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionDisabled($id) { 

        if (($tutorialType = TutorialType::findOne($id)) !== null) {

            if ($tutorialType->status == "ENABLED") { 

                    $tutorialType->status = 'DISABLED';
                    $tutorialType->save(false);

                    $return = ['code' => 200, 'message' => 'Tutorial Type disabled successfully.'];
            } else {
                $return = ['code' => 421, 'message' => 'Tutorial Type not enabled.'];
            }
        } else {
            $return = ['code' => 404, 'message' => 'Record does not exist.'];
        }

        Yii::$app->response->format = 'json';
        return $return;
    }

    public function actionEnabled($id) { 

        if (($tutorialType = TutorialType::findOne($id)) !== null) { 

            if ($tutorialType->status == "DISABLED") {

                    $tutorialType->status = 'ENABLED';
                    $tutorialType->save(false);

                    $return = ['code' => 200, 'message' => 'Tutorial Type enabled successfully.'];
            } else {
                $return = ['code' => 421, 'message' => 'Tutorial Type not disabled.'];
            }
        } else {
            $return = ['code' => 404, 'message' => 'Record does not exist.'];
        }

        Yii::$app->response->format = 'json';
        return $return;
    }
}
