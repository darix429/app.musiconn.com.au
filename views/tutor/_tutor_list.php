<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use app\libraries\General;
use app\models\OnlineTutorialPackageType;

?>
<div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">

    <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Name</th>
                <th>Email</th>
                <th>Join Date</th>
                <th>Contract Expiry</th>
                <th>Category</th>
                <th>Status</th>
                <th>Action</th>
                <th>Go To</th>
            </tr>
        </thead>

        <tbody>
            <?php
            if (!empty($tutorList)) {
                foreach ($tutorList as $key => $value) {
                                        $plantype = OnlineTutorialPackageType::find()->where(['uuid' => $value['online_tutorial_package_type_uuid']])->one();

                    ?>
                    <tr>
                        <td><?= $key + 1; ?></td>

                        <td><?php
                            ob_start();
                            fpassthru($value['profile_image']);
                            $contents = ob_get_contents();
                            ob_end_clean();
                            // print_r(base64_encode($contents));
                            $dataUri = "data:image/jpg;base64," . base64_encode($contents);
                            echo "<img width=30 height=30 class='img-fluid' style='' src='$dataUri'/>";
                            ?> <?= $value['first_name'].' '.$value['last_name'] ; ?>
                        </td>
                        <td><?= $value['email']; ?></td>
                        <td><?= General::displayDateTime($value['join_date'],false,true); ?></td>
                        <td><?= General::displayDate($value['contract_expiry']); ?></td>
<td>
                            <?php
                            if ($plantype['name'] == 'PREMIUM') {
                                $e_class = "badge-success";
                                $pointer = 'style="color:#fff"';
                            } else {
                                $e_class = "badge-warning";
                                $pointer = 'style="color:#fff"';
                            }
                            ?>
                            <span class="badge badge-pill <?= $e_class ?>"  data-uuid="<?= $value['uuid']; ?>" <?= $pointer; ?>><?= strtoupper($plantype['display_name']); ?></span>

                        </td>
                        <td>
                            <?php
                            if ($value['status'] == 'ENABLED') {
                                $e_class = "badge-primary";
                                $e_link_class = "tutor_disabled_link";
                            } elseif ($value['status'] == 'DISABLED') {
                                $e_class = "badge-secondary";
                                $e_link_class = "tutor_enabled_link";
                            } elseif ($value['status'] == 'INACTIVE') {
                                $e_class = "badge-orange";
                                $e_link_class = "";
                            } else {
                                $e_class = "badge-danger";
                                $e_link_class = "";
                            }
                            ?>
                            <span class="badge badge-pill <?= $e_class ?> <?= $e_link_class ?>"  data-uuid="<?= $value['uuid']; ?>" ><?= $value['status']; ?></span>
                        </td>
                        <td>
                            <?php if ($value['status'] != 'DELETED') { ?>
                                <a href="<?= Url::to(['/tutor/update/', 'id' => $value['uuid']]) ?>" class="badge badge-info badge-md" title="Update"><i class="fa fa-pencil"></i></a>
                                <a href="<?= Url::to(['/tutor/working-plan/', 'id' => $value['uuid']]) ?>" class="icon-lg icon-rounded icon-orange badge badge-primary badge-md" title="Work Plan"><i class="fa fa-calendar"></i></a>
                                <a href="<?= Url::to(['/tutor/reset-password/', 'id' => $value['uuid']]) ?>" class="badge badge-primary badge-md" title="Reset Password"><i class="fa fa-undo"></i></a>
                                <?php if (Yii::$app->user->identity->role == 'ADMIN' || Yii::$app->user->identity->role == 'OWNER' || Yii::$app->user->identity->role == 'SUBADMIN') { ?>
                                <a href="<?= Url::to(['/tutor/assign-plan/', 'id' => $value['uuid']]) ?>" class="badge badge-primary badge-md" title="Assign Plan"><i class="fa fa-usd"></i></a>
                            <?php } ?>
                                

                            <?php } ?>
                        </td>
                         <td class="list-unstyled list-inline">
                            <?php if ($value['status'] == 'ENABLED') { ?>
                                <a href="<?= Url::to(['/admins/switch-to-tutor/', 'id' => $value['uuid']]) ?>" class="badge badge-orange badge-md" title="Go To Account"><i class="fa fa-exchange"></i></a>
                            <?php } ?>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>

</div>
