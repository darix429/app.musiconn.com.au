<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \mdm\admin\models\form\PasswordResetRequest */

$this->title = 'Forgot Pasword';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="login-box">
    <!-- /.login-logo -->
    <div class="login-box-body">
        <h2 class="text-center"><?= $this->title; ?></h2>
        <?php
        $form = ActiveForm::begin([
                    'id' => 'request-password-reset-form',
                    'options' => ['name' => 'forgotpasswordform'],
        ]);
        ?>

        <?= $form->field($model, 'email')->textInput(['autofocus' => true, 'class' => 'form-control'])->label('Email*') ?>
        <p>Please fill out your email. A link to reset password will be sent there.</p>

        <div class="row">
            <center>
                <?= Html::submitButton('Send', ['class' => 'login_btn btn btn-primary btn-block btn-flat', 'name' => 'signup-button']) ?>
            </center>
            <!-- /.col -->
        </div>
        <?php ActiveForm::end(); ?>
        <center>
            <?= Html::a('Go to Sign In', ['site/login'], []) ?>
        </center>
        <div class="login-logo">
            <a href="<?= Yii::$app->params['base_url']; ?>"><img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new/' ?>img/login-logo.png"></a>
            <center><p class="login_btm"><?= Yii::$app->params['copyright']; ?></p></center>
        </div>
    </div>
    <!-- /.login-box-body -->
</div>