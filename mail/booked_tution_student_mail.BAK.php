<?php

use yii\helpers\Html;

?>
<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['student_name']; ?>,</p>
            
            <p>Thank you for your payment. Below are the details of your purchase:</p>
                        
            <p style="text-decoration: underline;"><strong>Scheduled tutorials</strong></p>
            
            <p><strong>Booking Reference :</strong> <?= "#".$content['booking_id']; ?></p>
            <p><strong>Tutor Name :</strong> <?= $content['tutor_name']." ".$content['tutor_last_name']; ?></p>
            <p><strong>Instrument :</strong> <?= $content['instrument']; ?></p>
            
            <p><strong>Scheduled Lessons :</strong> 
                <?php if(is_array($content['session_dates'])){ ?>
                <ul>
                    <?php foreach ($content['session_dates'] as $key => $value) {
                        echo "<li>".$value." ".$content['time']."</li>";
                    } ?>
                </ul>
                <?php } ?>
            </p>
            <p><strong>Subscription to online contents :</strong> <?= (!empty($content['student_monthly_subscription_uuid']) ) ? "Yes" : "No"; ?></p>
            
            <p><strong>Creative Kids Voucher submitted :</strong> <?= (!empty($content['creative_kids_voucher_code']) ) ? "Yes" : "No"; ?></p>
            
            <p>You can find the invoice as record of your payment by logging onto our <a target="_blank" href="https://app.musiconn.com.au/">booking system</a>.</p>
            
            <p>If you need to re-schedule a lesson with your tutor, please refer to the student’s manual for instructions.  The manual can be found on the <a target="_blank" href="https://www.musiconn.com.au/terms-and-conditions">Musiconn website</a>.</p>
            
            <p>For terms and conditions of your purchase, please refer to the <a target="_blank" href="https://www.musiconn.com.au/">Musiconn website</a>.</p>
            
            <p>Please do not reply to this unattended mailbox.  Any queries can be directed to admin@musiconn.com.au, or call us on 1300 068 742.</p>
            
            
            <p>Regards,<br>
            Julian Fung <br>
            CEO – Musiconn Pty Ltd</p>
        </td>
    </tr>
</table>