<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<div class="modal-body">
    <div class="row vertical-center-box vertical-center-box-tablet" >
        <?php $form = ActiveForm::begin(['id' => 'form-change']); ?>

        <?=
                $form->field($modelCP, 'oldPassword', ['template' => '<span>{label}</span>{input}{error}'])
                ->passwordInput(['class' => 'form-control', 'maxlength' => true,'autocomplete' => 'new-password'])
                ->label('Current Password*')
        ?>
        <span class="hr_line">
        </span>
        <?=
                $form->field($modelCP, 'newPassword', ['template' => '<span>{label}</span>{input}{error}'])
                ->passwordInput(['class' => 'form-control', 'maxlength' => true,])
                ->label('New Password*')
        ?>
        <?=
                $form->field($modelCP, 'retypePassword', ['template' => '<span>{label}</span>{input}{error}'])
                ->passwordInput(['class' => 'form-control', 'maxlength' => true,])
                ->label('Retype New Password*')
        ?>
        
        <?php ActiveForm::end(); ?>
    </div>
</div>
<div class="modal-footer">
    <?= Html::Button('Save', ['class' => 'btn btn-primary', 'name' => 'change-button', 'id' => 'password-change-btn']) ?>
</div>
