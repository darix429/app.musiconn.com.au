<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CancelSession */

$this->title = 'Create Cancel Session';
$this->params['breadcrumbs'][] = ['label' => 'Cancel Sessions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cancel-session-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
