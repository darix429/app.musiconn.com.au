<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Url;
use app\models\Instrument;
use app\models\StudentTuitionBooking;

$step_data = StudentTuitionBooking::getSession(Yii::$app->user->identity->student_uuid);
?>
<?php
if (!empty($onlineTutorialList)) {
    foreach ($onlineTutorialList as $key => $value) {
        
        $displayClass = (isset($step_data['step_2']['onlinetutorial_uuid']) && $step_data['step_2']['onlinetutorial_uuid'] == $value['premium_stanadrd_plan_uuid']) ? "" : "d-none";
        $checked = (isset($step_data['step_2']['onlinetutorial_uuid']) && $step_data['step_2']['onlinetutorial_uuid'] == $value['premium_stanadrd_plan_uuid']) ? "checked" : "";
        ?>
        <div class="row pakage-list onlinetutorial_tr" >
            <div class="col-md-2 text-center">
                <input tabindex="5" type="radio"  class="skin-square-red float-left onlinetutorial_checkbox" name="StudentTuitionBooking[onlinetutorialcheckbox]" value="<?= $value['premium_stanadrd_plan_uuid']; ?>" <?= $checked; ?> data-option="<?= $value['tutor_option'];?>" data-price="<?= $value['tutor_plan_total_price'];?>">
            </div>
            <div class="col-md-6">
                <p class="">
                    <?= $value['name'] ?>
                </p>
            </div>
            <div class="col-md-2">
                <p class="">
                    $<?= $value['tutor_plan_total_price']; ?>
                </p>
            </div>
            <div class="col-md-2">
                <?= Html::submitButton('Select', ['class' => "btn btn-primary btn-md onlinetutorial_select_btn $displayClass"]) ?>
            </div>
        </div>
        <?php
    }
} else {
    echo "No plan available.";
}
?>