<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;

$this->title = Yii::t('app', 'Settings');
$this->params['breadcrumbs'][] = $this->title;


$this->registerJs(
   '
    
    
    
$(document).on("click","#search_btn", function(){        
    $.ajax({
        type: "GET",
        url: "'.Url::to(['/settings/index/']).'",
        data: $("#search_frm").serialize(),
        success:function(result){
            $("#setting_table_parent_div").html(result);
            window.ULTRA_SETTINGS.dataTablesInit();
        },
        error:function(e){ 
            showErrorMessage(e.responseText);
        }
    });
});
    
    
',View::POS_END
);
?>

<div class="col-xl-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Settings List</h2>
            <div class="actions panel_actions float-right">
                <i class="fa fa-search icon-primary icon-xs icon-orange mail_head_icon custom-toggle" data-togglediv="datatable-search-div" title="Advance Search"></i>
                <!--<a href="<?= Yii::$app->getUrlManager()->createUrl(['/settings/create/'])?>" class="btn btn-primary "><i class="fa fa-plus text-white"></i></a>-->
            </div>
        </header>
        <div class="content-body"> 
            <div class="row ">
                <div class="col-lg-12 col-md-12 col-12 ">
                    <div class="datatable-search-div " style="display: none;">
                    
                    <?php $form = ActiveForm::begin([
                                'method' => 'get',
                                'options' => [
                                    'id' => 'search_frm',
                                    'class' => 'form-inline'
                                ],
                            ]); ?>
                   	    
		                <div class="col-md-3 mb-0">
		                    <?= $form->field($searchModel, 'name')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Name'])->label(false) ?>
		                </div>
				<div class="col-md-3 mb-0">
		                    <?= $form->field($searchModel, 'value')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Value'])->label(false) ?>
		                </div>

				               
                        <?= Html::Button(Yii::t('app', 'Search'), ['class' => 'btn btn-primary mb-0', 'id'=>'search_btn','style'=>'margin-left: 17px;']) ?>
                    
                    <?php ActiveForm::end(); ?>
                </div>
                </div>
            </div>
            <br>
            <div class=""></div>
            <div class="clearfix"></div>
            <div class="row" id="setting_table_parent_div">
                <?php echo Yii::$app->controller->renderPartial('_setting_list', ['settingList' => $settingList]); ?>
            </div>
        </div>
    </section> 
</div>














