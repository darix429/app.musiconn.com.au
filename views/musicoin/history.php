<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'MUSICOIN HISTORY');
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_icon'] = 'my-lesson-tag.png';

$this->registerJs(
        '
    
function refresh_history(){
    $.ajax({
        type: "GET",
        url: "' . Url::to(['/musicoin/history/']) . '",
        data: $("#search_frm").serialize(),
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            $("#table_parent_div").html(result);
            window.NEW_CUSTOM.dataTablesInit();
        },
        error:function(e){ 
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
}

$(document).on("click","#search_btn", function(){        
    refresh_history();
});
$(document).on("click","#search_reset_btn", function(){        
    $.ajax({
        type: "GET",
        url: "' . Url::to(['/musicoin/history/']) . '",
        data: {},
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            $("#table_parent_div").html(result);
            window.NEW_CUSTOM.dataTablesInit();
        },
        error:function(e){ 
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});
    

$(".datefilter").daterangepicker({
      autoUpdateInput: false,
      locale: {
          cancelLabel: "Clear",
          format: "DD-MM-YYYY"
      },ranges : {
                    "Today": [moment(), moment()],
                    "Yesterday": [moment().subtract("days", 1), moment().subtract("days", 1)],
                    "Last 7 Days": [moment().subtract("days", 6), moment()],
                    "Last 30 Days": [moment().subtract("days", 29), moment()],
                    "This Month": [moment().startOf("month"), moment().endOf("month")],
                    "Last Month": [moment().subtract("month", 1).startOf("month"), moment().subtract("month", 1).endOf("month")]
                }
  });

  $(".datefilter").on("apply.daterangepicker", function(ev, picker) {
      $(this).val(picker.startDate.format("DD-MM-YYYY") + " - " + picker.endDate.format("DD-MM-YYYY"));
  });

  $(".datefilter").on("cancel.daterangepicker", function(ev, picker) {
      $(this).val("");
  });
    
', View::POS_END
);
?>


<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row center-block box-search-area">
                        <div class="col-lg-12 col-md-12 col-12 ">
                            <div class="actions panel_actions pull-right custom-toggle" data-togglediv="datatable-search-div" title="Advance Search" style="margin-bottom: 5px; cursor: pointer">
                                <span>
                                    <i class="fa fa-search " ></i>
                                    Advance Search
                                </span>
                            </div>
                            <div class="datatable-search-div " style="display: none;">
                                <?php
                                $form = ActiveForm::begin([
                                            'method' => 'get',
                                            'options' => [
                                                'id' => 'search_frm',
                                                'class' => 'form-inline'
                                            ],
                                ]);
                                ?>

                                <?= $form->field($searchModel, 'created_at')->textInput([ 'class' => 'form-control datefilter', 'maxlength' => true, 'placeholder' => 'Select Date', 'data-format' => 'DD-MM-YYYY'])->label(false) ?>
                                <?php //echo $form->field($searchModel, 'created_at')->textInput(['class' => 'form-control daterange  add-date-ranges', 'maxlength' => true, 'placeholder' => 'Date', 'data-format' => 'DD-MM-YYYY'])->label(false) ?>
                                <?= $form->field($searchModel, 'summary')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Summary'])->label(false) ?>
                                
                                <?= Html::Button(Yii::t('app', 'Search'), ['class' => 'btn btn-primary mb-0', 'id' => 'search_btn', 'style' => 'margin-left: 17px;']) ?>
                                <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-primary mb-0', 'id' => 'search_reset_btn']) ?>

                                <?php ActiveForm::end(); ?>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row" id="table_parent_div">
                        <?php echo Yii::$app->controller->renderPartial('_history_list', ['recordList' => $recordList,'searchModel'=>$searchModel]); ?>
                    </div>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <a class="btn btn-primary btn-sm black_button_in" href="<?= Yii::$app->getUrlManager()->createUrl('/student/profile'); ?>">RETURN TO PROFILE</a>

</section>