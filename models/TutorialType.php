<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tutorial_type}}".
 *
 * @property string $uuid
 * @property string $code
 * @property string $description
 * @property string $type
 * @property int $min
 * @property int $days
 * @property int $unit
 */
class TutorialType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tutorial_type}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'code', 'description', 'type', 'min', 'days', 'unit'], 'required'],
            [['uuid', 'status'], 'string'],
            [['min', 'days', 'unit'], 'default', 'value' => null],
            [['min', 'days', 'unit'], 'integer'],
            [['code'], 'string', 'max' => 4],
            [['description'], 'string', 'max' => 60],
            [['type'], 'string', 'max' => 1],
            [['code'], 'unique'],
            [['uuid'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'code' => Yii::t('app', 'Code'),
            'description' => Yii::t('app', 'Description'),
            'type' => Yii::t('app', 'Type'),
            'min' => Yii::t('app', 'Min'),
            'days' => Yii::t('app', 'Days'),
            'unit' => Yii::t('app', 'Unit'),
	    'status' => Yii::t('app', 'Status'),
        ];
    }
    
    public function getTutorialTypeByCode($code = '') {
        
        $model = TutorialType::find()->where(['code' => $code])->asArray()->one();
        if (!empty($model)) {
            return $model;
        } else {
            return [];
        }
    }
}
