<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Payment Terms');
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs(
        '
        $(document).on("click",".paymentTerm_disabled_link", function(){  
            if(confirm("Are you sure, you want to disabled?")){
            var element = $(this);
                $.ajax({
                    type: "GET",
                    url: "' . Url::to(['/payment-term/disabled/']) . '",
                    data: { "id": $(this).data("uuid")},
                    beforeSend:function(){ blockBody(); },
                    success:function(result){
                        unblockBody();
                        if(result.code == 200){
                            $(element).removeClass("badge-primary");
                            $(element).addClass("badge-secondary");
                            $(element).removeClass("paymentTerm_disabled_link");
                            $(element).addClass("paymentTerm_enabled_link");
                            $(element).text("DISABLED");
                            showSuccess(result.message);
                        }else{
                            showErrorMessage(result.message);
                        }
                    },
                    error:function(e){
                        unblockBody();
                        showErrorMessage("Sorry, something wrong.");
                    }
                });
            }
        });
    
        $(document).on("click",".paymentTerm_enabled_link", function(){ 
            if(confirm("Are you sure, you want to enabled?")){
            var element = $(this);
                $.ajax({
                    type: "GET",
                    url: "' . Url::to(['/payment-term/enabled/']) . '",
                    data: { "id": $(this).data("uuid")},
                    beforeSend:function(){ blockBody(); },
                    success:function(result){
                        unblockBody();
                        if(result.code == 200){
                            $(element).removeClass("badge-secondary");
                            $(element).addClass("badge-primary");
                            $(element).removeClass("paymentTerm_enabled_link");
                            $(element).addClass("paymentTerm_disabled_link");
                            $(element).text("ENABLED");
                            showSuccess(result.message);
                        }else{
                            showErrorMessage(result.message);
                        }
                    },
                    error:function(e){
                        unblockBody();
                        showErrorMessage("Sorry, something wrong.");
                    }
                });
            }
        }); 
    
       
        $(document).on("click","#search_btn", function(){        
            $.ajax({
                type: "GET",
                url: "' . Url::to(['/payment-term/index/']) . '",
                data: $("#search_frm").serialize(),
                beforeSend:function(){ blockBody(); },
                success:function(result){
                    unblockBody();
                    $("#payment_term_table_parent_div").html(result);
                    window.ULTRA_SETTINGS.dataTablesInit();
                },
                error:function(e){
                    unblockBody();
                    showErrorMessage(e.responseText);
                }
            });
        });
       
        $(document).on("click","#search_reset_btn", function(){        
            $.ajax({
                type: "GET",
                url: "' . Url::to(['/payment-term/index/']) . '",
                data: {},
                beforeSend:function(){ blockBody(); },
                success:function(result){
                    unblockBody();
                    $("#payment_term_table_parent_div").html(result);
                    window.ULTRA_SETTINGS.dataTablesInit();
                },
                error:function(e){
                    unblockBody();
                    showErrorMessage(e.responseText);
                }
            });
        });
    '
);
?>

<div class="col-xl-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Payment Terms List</h2>
            <div class="actions panel_actions float-right">
                <i class="fa fa-search icon-primary icon-xs icon-orange mail_head_icon custom-toggle" data-togglediv="datatable-search-div" title="Advance Search"></i>
            </div>
        </header>
        <div class="content-body">   
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12 ">
                    <div class="datatable-search-div " style="display: none;">

                        <?php 
                        $form = ActiveForm::begin([
                                    'method' => 'get',
                                    'options' => [
                                        'id' => 'search_frm',
                                        'class' => 'form-inline'
                                    ],
                        ]);
                        ?>
			
                        <div class="col-md-2 mb-0">
                            <?= $form->field($searchModel, 'code')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Code', 'style' => 'width:100%'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'term')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $searchModel->getAttributeLabel('term'), 'style' => 'width:100%'])->label(false) ?>
                        </div>
			<div class="col-md-2 mb-0">
                            <?= $form->field($searchModel, 'unit')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $searchModel->getAttributeLabel('unit'), 'type' => 'number', 'style' => 'width:100%'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'status')->dropDownList(['ENABLED' => 'ENABLED', 'DISABLED' => 'DISABLED'], ['class' => 'form-control', 'style' => 'width:100%'])->label(false) ?>
                        </div>  
                        
                            <?= Html::Button(Yii::t('app', 'Search'), ['class' => 'btn btn-primary mb-0', 'id' => 'search_btn']) ?>
                            <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-purple mb-0', 'id'=>'search_reset_btn']) ?>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>

            </div>
            <br>
            <div class="clearfix"></div>
            <div class="row" id="payment_term_table_parent_div">
                <?php echo Yii::$app->controller->renderPartial('_payment_term_list', ['paymentTermList' => $paymentTermList]); ?>
            </div>
        </div>
    </section>
</div>






















