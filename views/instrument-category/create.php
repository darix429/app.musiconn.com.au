<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\InstrumentCategory */

$this->title = Yii::t('app', 'Create Instrument Category');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Instrument Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<!--<div class="instrument-category-create">

    <h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

<!--</div>-->
