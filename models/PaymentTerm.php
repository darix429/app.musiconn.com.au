<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%payment_term}}".
 *
 * @property string $uuid
 * @property string $code
 * @property string $term
 * @property int $unit
 */
class PaymentTerm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%payment_term}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'code', 'term', 'unit'], 'required'],
            [['uuid', 'status'], 'string'],
            [['unit'], 'default', 'value' => null],
            [['unit'], 'default'],
            [['code'], 'string', 'max' => 1],
            [['term'], 'string', 'max' => 20],
            [['code'], 'unique'],
            [['uuid'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'code' => Yii::t('app', 'Code'),
            'term' => Yii::t('app', 'Term'),
            'unit' => Yii::t('app', 'Unit'),
	    'status' => Yii::t('app', 'Status'),
        ];
    }
    
    public function getPaymentTermByCode($code = '') {
        
        $model = PaymentTerm::find()->where(['code' => $code])->asArray()->one();
        if (!empty($model)) {
            return $model;
        } else {
            return [];
        }
    }
    
    public function getPaymentTermWiseTotalSessonCount($payment_term_code = '',$tutorial_type_code = '') {
        
        $unit = 0;
        
        if (!empty($tutorial_type_code) && !empty($payment_term_code) ) {
            
            switch ($payment_term_code) {
                
                case 'M':
                    if($tutorial_type_code == 'W'){
                        $unit = 4;
                    }elseif($tutorial_type_code == 'F'){
                        $unit = 2;
                    }elseif($payment_term_code == 'M'){
                        $unit = 1;
                    }

                    break;
                
                case 'Q':
                    if($tutorial_type_code == 'W'){
                        $unit = 12;
                    }elseif($tutorial_type_code == 'F'){
                        $unit = 6;
                    }elseif($payment_term_code == 'M'){
                        $unit = 3;
                    }

                    break;
                
                case 'F':
                    if($tutorial_type_code == 'W'){
                        $unit = 2;
                    }elseif($tutorial_type_code == 'F'){
                        $unit = 1;
                    }elseif($payment_term_code == 'M'){
                        $unit = 0;
                    }

                    break;
                
                    
                default:
                    break;
            }
            
            /*switch ($tutorial_type_code) {
                case 'W':
                    if($payment_term_code == 'M'){
                        $unit = 4;
                    }elseif($payment_term_code == 'Q'){
                        $unit = 12;
                    }elseif($payment_term_code == 'F'){
                        $unit = 2;
                    }

                    break;
                case 'Q':
                    if($payment_term_code == 'M'){
                        $unit = 3;
                    }elseif($payment_term_code == 'Q'){
                        $unit = 1;
                    }elseif($payment_term_code == 'F'){
                        $unit = 6;
                    }

                    break;
                case 'F':
                    if($payment_term_code == 'M'){
                        $unit = 2;
                    }elseif($payment_term_code == 'Q'){
                        $unit = 6;
                    }elseif($payment_term_code == 'F'){
                        $unit = 1;
                    }

                    break;

                default:
                    break;
            }*/
        }
//        echo $payment_term_code."<br>";
//        echo $tutorial_type_code."<br>";
//        echo $unit."<br>";
//        exit;
        return $unit;
    }
}
