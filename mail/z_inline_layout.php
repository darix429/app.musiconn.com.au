<?php

use yii\helpers\Html;
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['name'] ?>,</p>
            
            <p>Thank you for registering with <?= Yii::$app->name; ?>.</p>

            <p>Lorium ipsum</p>
            

            <p><a style="background:#ff6f3d;min-height:46px;margin:10px 0;padding:0 40px;line-height:46px;border-radius:4px;font-size:18px;color:#fff;display:inline-block;" href="">Activate account</a></p>
            <p>If the button does not work, please try copying this URL in your browser's address bar: </p>
            <p></p>
            
            <p> Once your <?= Yii::$app->name; ?> Account has been activated, you can use <?= Yii::$app->name; ?> services.</p>

            <p>Sincerely,</p>
            <p>System Administrators</p>
        </td>
    </tr>
</table>

