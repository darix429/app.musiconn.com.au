<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\ArrayHelper;
use app\models\InstrumentCategory;
use app\models\Instrument;

//Yii::$app->session['status'] = 'published';

$this->title = Yii::t('app', 'VIDEO LIBRARY');
$this->params['breadcrumbs'][] = $this->title;
//$this->params['page_icon'] = 'video-icon.png';
$this->params['page_icon'] = 'VideoLibrary-gradient.png';
/*$this->params['page_title_custom'] = '<div class="col-md-7">
                        <h1>
                            <img src="' . Yii::$app->request->baseUrl . '/theme_assets/new/img/video-icon.png' . '">VIDEO LIBRARY 
                        </h1>
                    </div>
                    <div class="col-md-5">
                        <a href="'.Url::to(['/booked-session/completed']).'" class="btn btn-primary btn-sm black_button_in" >MY RECORDED LESSONS</a>
                        <a href="'.Url::to(['/video/student-video-list/']).'" class="btn btn-primary btn-sm black_button_in" >VIDEO LIBRARY</a>
                    </div>';*/

$this->params['page_title_custom'] = ''
        . '         <div class="col-md-12">
                        <div class="col-md-5">
                            <h3>
                                <img src="'. Yii::$app->request->baseUrl . '/theme_assets/new_book/img/'.$this->params['page_icon'] .'" style="width: 52px;position: relative;">'
            . '                 <strong class="title-hm">'.strtoupper($this->title).'</strong>
                            </h3>
                        </div>
                        <div class="col-md-7 text-right">
                            <a href="'.Url::to(['/booked-session/completed']).'" class="btn btn-primary btn-sm black_button_in" >MY RECORDED LESSONS</a>
                            <a href="'.Url::to(['/video/student-video-list/']).'" class="btn btn-primary btn-sm black_button_in" >VIDEO LIBRARY</a>
                        </div>
                    </div>
                    ';

$categoryArray = ArrayHelper::map(InstrumentCategory::find()->asArray()->all(), 'uuid', 'name');
$instrumentArray = ArrayHelper::map(Instrument::find()->where(['status' => 'ACTIVE'])->asArray()->all(), 'uuid', 'name');

$this->registerJs(
'

$(document).on("click",".instrument_btn", function(){  
    var page_no = 0;
    $("#page_no").val(page_no);
    $("#search_input").val("");
    $("#instrument_input").val($(this).data("bind"));
    
    $.ajax({
        type: "GET",
        url: "' . Url::to(['/video/student-video-list/']) . '",
        data: {"instrument_uuid" : $(this).data("bind"),"page_no" : page_no},
        beforeSend:function(){ blockBody(); },
        success:function(result){
        
            unblockBody();
           
            $("#pre_recorded_video_table_parent_div").html(result.dataHtml);
        },
        error:function(e){ 
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
}); 

$(document).on("click",".search_input_btn", function(){  
    var page_no = 0;
    $("#page_no").val(page_no);
    var search_input = $("#search_input").val();
    $("#instrument_input").val("");
    
    $.ajax({
        type: "GET",
        url: "' . Url::to(['/video/student-video-list/']) . '",
        data: {"search_input" : search_input,"page_no" : page_no},
        beforeSend:function(){ blockBody(); },
        success:function(result){
        
            unblockBody();
           
            $("#pre_recorded_video_table_parent_div").html(result.dataHtml);
        },
        error:function(e){ 
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
}); 


$(document).on("click","#load_more", function(){

    var page_no = $("#page_no").val();
    page_no = (page_no > 0) ? page_no : 0;
    page_no = parseInt(page_no) + 1;
    $("#page_no").val(page_no);
    
    var search_input = $("#search_input").val();
    
    $.ajax({
        type: "GET",
        url: "' . Url::to(['/video/student-video-list/']) . '",
        data: {"instrument_uuid" : $("#instrument_input").val(),"page_no" : page_no, "search_input" : search_input },
        beforeSend:function(){ blockBody(); },
        success:function(result){
        
            unblockBody();
            if(result.result){
                $("#pre_recorded_video_table_parent_div").append(result.dataHtml);
            } else {
                showErrorMessage("No more video found");
            }
        },
        error:function(e){ 
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});  
', View::POS_END
);
?>
<style>
    .instrument_btn{
        cursor: pointer;
    }
    .search_input_btn{
        cursor: pointer;
    }
</style>
<section class="content" style='display:flex' >
    <div class="col-md-2">
        <div class="white_bg">
            <div class="text-center categry-bg-white">
                <?php if (!empty($instrumentArray)) { ?>
                <?php
                foreach ($instrumentArray as $key => $value) {
                $instrument_video_count = \app\models\PreRecordedVideo::instrumentPreRecordedVideoCount($key);
                ?>

                <div class=""><a  class="instrument_btn" data-bind="<?= $key ?>"><?= $value . ' (' . $instrument_video_count . ')' ?></a></div>
                <?php } ?>
                <?php } ?>
            </div>
            <div class="form-group video_search_btm">
                <label for="">Search</label>
                <div class="input-group">
                    <input type="text" name="search_input" id="search_input" class="form-control" value="<?= (!empty($search_input))?$search_input:""; ?>">
                    <span class="input-group-addon search_input_btn"><i class="fa fa-search"></i></span>
                </div>
                <input type="hidden" name="page_no" id="page_no" value="<?= (!empty($page_no))?$page_no:0; ?>">
                <input type="hidden" name="instrument_input" id="instrument_input" value="<?= (!empty($instrument_uuid))?$instrument_uuid:""; ?>">
            </div>

        </div>
    </div>
    <div class="col-md-10">
        <div class="container-fluid video_categry-bg-white" id="pre_recorded_video_table_parent_div">
            <?= $recodedVideoListHtml; ?>
        </div>
        <a class="" id="load_more"><div class="change_pass" style="margin-top: 10px;cursor: pointer;">SEE MORE</div></a>
    </div>
    
</section>
