<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\ArrayHelper;
use app\models\InstrumentCategory;
use app\models\Instrument;


if($searchModel->status !== 'PUBLISHED'){
    $this->title = Yii::t('app', 'Lesson Videos');
    $this->params['breadcrumbs'][] = $this->title;
}else{
    $this->title = Yii::t('app', 'Recorded Videos');
    $this->params['breadcrumbs'][] = $this->title;
}

$categoryArray = ArrayHelper::map(InstrumentCategory::find()->asArray()->all(), 'uuid', 'name');
$instrumentArray = ArrayHelper::map(Instrument::find()->where(['status' => 'ACTIVE'])->asArray()->all(), 'uuid', 'name');

$this->registerJs(
   '
//delete code
    $(document).on("click",".delete_pre_recorded_video_link", function(){
        if(confirm("Are you sure, you want to delete this video?")){
        var element = $(this);
        var id = $(this).data("uuid");
            $.ajax({
                type: "POST",
                // url: "'.Url::to(['/video/delete/']).'",
                url: "'.Url::to(['/video/delete-video/']).'?id="+id,
                success:function(result){
                    if(result.code == 200){

                        $(element).closest("div.music_genre").remove();

                        showSuccess(result.message);
                    }else{
                        showErrorMessage(result.message);
                    }
                },
                error:function(e){
                    showErrorMessage(e.responseText);
                }
            });
        }
    });


$(document).on("click","#search_btn", function(){
    $.ajax({
        type: "GET",
        url: "'.Url::to(['/video/recorded/']).'",
        data: $("#search_frm").serialize(),
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            $("#pre_recorded_video_table_parent_div").html(result);
            window.ULTRA_SETTINGS.dataTablesInit();
        },
        error:function(e){
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});
$(document).on("click","#search_reset_btn", function(){
    $.ajax({
        type: "GET",
        url: "'.Url::to(['/video/recorded/']).'",
        data: {},
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            $("#pre_recorded_video_table_parent_div").html(result);
            window.ULTRA_SETTINGS.dataTablesInit();
        },
        error:function(e){
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});

',View::POS_END
);
?>

<div class="col-xl-12">
    <section class="box ">
        <header class="panel_header">

            <h2 class="title float-left">Lesson Recorded Videos List</h2>

            <div class="actions panel_actions float-right">
                <i class="fa fa-search icon-primary icon-xs icon-orange mail_head_icon custom-toggle" data-togglediv="datatable-search-div" title="Advance Search"></i>
                <!--<a href="<?= Yii::$app->getUrlManager()->createUrl(['/video/upload/'])?>" class="btn btn-primary "><i class="fa fa-plus text-white"></i></a>-->
            </div>
        </header>
        <div class="content-body">
            <div class="row ">
                <div class="col-lg-12 col-md-12 col-12 ">
                   <div class="datatable-search-div " style="display: none;">

                        <?php
                        $form = ActiveForm::begin([
                                    'method' => 'get',
                                    'options' => [
                                        'id' => 'search_frm',
                                        'class' => 'form-inline'
                                    ],
                        ]);
                        //echo "<pre>";print_r($searchModel);
                        ?>

                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'student_name')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Student Name'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'tutor_name')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Tutor Name'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'instrument_uuid')->dropDownList($instrumentArray, ['class' => 'form-control myselect2', 'prompt' => 'Select Instrument', 'style' => 'width: 100%'])->label(false) ?>
                        </div>


                        <?= Html::Button(Yii::t('app', 'Search'), ['class' => 'btn btn-primary mb-0', 'id' => 'search_btn', 'style' => 'margin-left: 17px;']) ?>
                       <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-purple mb-0', 'id'=>'search_reset_btn']) ?>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
            <br>
            <div class=""></div>
            <div class="clearfix"></div>
            <div class="row" id="pre_recorded_video_table_parent_div">
                <?php echo Yii::$app->controller->renderPartial('_recorded_list', ['videoList' => $videoList]); ?>
            </div>
        </div>
    </section>
</div>
