<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\BookedSession;
use app\models\Student;
use app\models\Tutor;
use app\libraries\General;

class LessonController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'test'],
                'rules' => [
                        [
                        'actions' => ['index','test'],
                        'allow' => true,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                //'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Conference URL validate and load.
     *
     * @return string
     */
    public function actionIndex($u) {

        $this->layout = "lesson";

        $result = self::check_lesson($u);
        $res_check_device = General::JitsiCheckDevice();
        
        if(isset($res_check_device['success']) && $res_check_device['success'] != 'true'){
            $result['success'] = false;
            $result['message'] = $res_check_device['msg'];
        }
        
        $reload_flag = false;
        
        $frame_url = Yii::$app->params['verto']['lesson_frame_base_url'] . '';
        if (!empty($result['success']) && $result['success'] == true) {
            $frame_url_param = "room=" . base64_encode($result['lesson_uuid']) . "&nick_name=" . $result['userData']['first_name'];
            $frame_url = $frame_url . "?" . urlencode($frame_url_param);

            if (!empty($result['user_type']) && $result['user_type'] == "student" && isset($result['tutor_login_flag']) && $result['tutor_login_flag'] != 1) {
                $reload_flag = true;
            }
        }

        return $this->render('index', [
                    'result' => $result,
                    'frame_url' => $frame_url,
                    'reload_flag' => $reload_flag,
        ]);
        Yii::$app->end();
    }

    public function check_lesson($u = '') {

        $decryptData = explode('|', urldecode(General::decrypt_str($u)));

        $student_uuid = (!empty($decryptData[1]) && !empty($decryptData[2]) && $decryptData[2] == 'student' ) ? $decryptData[1] : NULL;
        $tutor_uuid = (!empty($decryptData[1]) && !empty($decryptData[2]) && $decryptData[2] == 'tutor' ) ? $decryptData[1] : NULL;
        $lesson_uuid = (!empty($decryptData[0]) ) ? $decryptData[0] : NULL;
        $user_type = (!empty($decryptData[2]) ) ? $decryptData[2] : NULL;
        $response = array('success' => false, 'message' => 'Your lesson URL is incorrect.');

        if (!General::is_uuid($lesson_uuid)) {
            return $response;
        } elseif (!in_array($user_type, ['student', 'tutor'])) {
            return $response;
        } elseif ($user_type == "student" && !General::is_uuid($student_uuid)) {
            return $response;
        } elseif ($user_type == "tutor" && !General::is_uuid($tutor_uuid)) {
            return $response;
        }

        if (count($decryptData) > 2) {

            if (strtoupper($user_type) == 'STUDENT') {
                $bookedResult = BookedSession::find()->where(['uuid' => $lesson_uuid, 'student_uuid' => $student_uuid])->andWhere(['in', 'booked_session.status', ['SCHEDULE', 'INPROCESS']])->one();
                $mStudent = Student::findOne($student_uuid);

                if (!empty($bookedResult) && !empty($mStudent)) {

                    $student_timezone = General::getStudentTimezone($bookedResult->student_uuid);

                    $userData = ['first_name' => $mStudent->first_name, 'last_name' => $mStudent->last_name, 'email' => $mStudent->email];
                    $currentDate = date('Y-m-d H:i:s');
                    $conferenceStartDate = date('Y-m-d H:i:s', strtotime($bookedResult->start_datetime));
                    $conferenceEndDate = date('Y-m-d H:i:s', strtotime($bookedResult->end_datetime));
                    $conferenceStartDate_before5minute = date('Y-m-d H:i:s', strtotime($bookedResult->start_datetime . " -5 minute"));

                    $display_start_time = General::convertTimezone($conferenceStartDate, Yii::$app->params['timezone'], $student_timezone, 'd M Y h:i A T');

                    if ($conferenceStartDate_before5minute <= $currentDate && $conferenceEndDate >= $currentDate) {

                        $mBS_UP = BookedSession::findOne($bookedResult->uuid);
                        $mBS_UP->student_login_flag = 1;
                        $mBS_UP->status = 'INPROCESS';
                        $mBS_UP->save(false);

                        $response = array('success' => true, 'message' => 'Your lesson verified successfully.', 'user_type' => $user_type, 'userData' => $userData, 'tutor_login_flag' => $mBS_UP->tutor_login_flag);
                    } elseif ($conferenceStartDate > $currentDate) {
                        $response = array('success' => false, 'message' => 'Your lesson will start at ' . $display_start_time, 'user_type' => $user_type, 'userData' => $userData,);
                    } else {
                        $response = array('success' => false, 'message' => 'Your lesson has been expired.', 'user_type' => $user_type, 'userData' => $userData);
                    }
                } else {
                    $response = array('success' => false, 'message' => 'Lesson not available.');
                }
            } else {
                //Tutor
                $bookedResult = BookedSession::find()->where(['uuid' => $lesson_uuid, 'tutor_uuid' => $tutor_uuid])->andWhere(['in', 'booked_session.status', ['SCHEDULE', 'INPROCESS']])->one();
                $mTutor = Tutor::findOne($tutor_uuid);
                if (!empty($bookedResult) && !empty($mTutor)) {

                    $tutor_timezone = General::getTutorTimezone($bookedResult->tutor_uuid);

                    $userData = ['first_name' => $mTutor->first_name, 'last_name' => $mTutor->last_name, 'email' => $mTutor->email];
                    $currentDate = date('Y-m-d H:i:s');
                    $conferenceStartDate = date('Y-m-d H:i:s', strtotime($bookedResult->start_datetime));
                    $conferenceEndDate = date('Y-m-d H:i:s', strtotime($bookedResult->end_datetime));
                    $conferenceStartDate_before5minute = date('Y-m-d H:i:s', strtotime($bookedResult->start_datetime . " -5 minute"));

                    $display_start_time = General::convertTimezone($conferenceStartDate, Yii::$app->params['timezone'], $tutor_timezone, 'd M Y h:i A T');

                    if ($conferenceStartDate_before5minute <= $currentDate && $conferenceEndDate >= $currentDate) {
                        $mBS_UP = BookedSession::findOne($bookedResult->uuid);
                        $mBS_UP->tutor_login_flag = 1;
                        $mBS_UP->status = 'INPROCESS';
                        $mBS_UP->save(false);

                        $response = array('success' => true, 'message' => 'Your lesson verified successfully.', 'user_type' => $user_type, 'userData' => $userData);
                    } elseif ($conferenceStartDate > $currentDate) {
                        $response = array('success' => false, 'message' => 'Your lesson will start at ' . $display_start_time, 'user_type' => $user_type, 'userData' => $userData);
                    } else {
                        $response = array('success' => false, 'message' => 'Your lesson has been expired.', 'user_type' => $user_type, 'userData' => $userData);
                    }
                } else {
                    $response = array('success' => false, 'message' => 'This lesson is not available.');
                }
            }
        } else {
            $response = array('success' => false, 'message' => 'Your lesson URL is incorrect.');
        }

        $response['lesson_uuid'] = $lesson_uuid;
        //$response['success'] = false;
        return $response;
    }

    public function actionTest() {

        $this->layout = "lesson";

        
        return $this->render('test', [
        ]);
        Yii::$app->end();
    }
}
