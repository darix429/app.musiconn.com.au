<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\helpers\Url;
use app\models\StudentTuitionBooking;
use app\models\Tutor;
use app\models\User;
use app\models\RescheduleSession;
use app\models\OnlineTutorialPackage;
use app\libraries\General;

$this->title = Yii::t('app', 'Reschedule Lesson');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Lessons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$tutor_uuid = $modelBS->tutor_uuid;
$student_uuid = $modelBS->student_uuid;
//$tutorDetail = Tutor::findOne($tutor_uuid);
//$tutor_working_plan = json_decode($tutorDetail->working_plan, true);
$tutor_tz = General::getTutorTimezone($tutor_uuid);
$student_tz = General::getStudentTimezone($student_uuid);
$rescheduled_awaiting_entry = RescheduleSession::find()->where(['booked_session_uuid' => $modelBS->uuid, 'status' => 'AWAITING'])->one();

$model_start_hour = $model->start_hour;
$start_hour_json = '{}';
$selecteddate = General::convertSystemToUserTimezone($modelBS->start_datetime, 'Y-m-d');//date("Y-m-d");
$model->session_when_start = $selecteddate;
$rescheduled_awaiting_entry = RescheduleSession::find()->where(['booked_session_uuid' => $modelBS->uuid, 'status' => 'AWAITING'])->one();


$this->registerJs('

var todaydate = "' . date("Y-m-d") . '";
var selecteddate = "' . $selecteddate . '";
    
$("#new_when_startdate_div").datepicker({
    inline: false,
    //todayBtn: true,
    format: "yyyy-mm-dd",
    viewSelect: "Default",
    startDate:todaydate,
    todayHighlight:false
}).on("changeDate", function(e) {
    $("#book-session_when_start").val( e.format() );
    clear_selected_data();
    get_timeslots();
    $("#new_when_startdate_div").find(".datepicker").find("div.datepicker-inline").hide();
});

$("#new_when_startdate_div").datepicker("update", selecteddate);
get_timeslots();
function get_timeslots(){
    var whendate = $("#book-session_when_start").val();
    if(whendate !== ""){
        $.ajax({
            type: "GET",
            url: "' . Url::to(['book/get-timeslots-reschedule']) . '",
            data: {"selected_when_date":whendate,"bs_uuid":"' . $modelBS->uuid . '"},
            beforeSend: function() {
                blockBody();
            },
            success:function(result){
                $("#book-start_hour").html(result.html);
                $("#start_hour_array").val(JSON.stringify(result.result));
                ULTRA_SETTINGS.iCheck();
                unblockBody();
            },error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
    }
}

function select_time(tm){
    if(tm !== ""){
        $.ajax({
            type: "POST",
            url: "' . Url::to(['book/select-time-reschedule']) . '",
            data: {"selected_time":tm,"student_uuid":"' . $student_uuid . '","bs_uuid":"' . $modelBS->uuid . '"},
            beforeSend: function() {
                blockBody();
            },
            success:function(result){

                $("#selected_date_data_parent").html(result.html);
                $(".selected_sesson_html").val(result.html);
                $("#book-all_lesson_data").val(JSON.stringify(result.all_lesson_data));
                $("#btn_next_btn").show();
                unblockBody();
            },error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
    }
}

function clear_selected_data(){
    $("#selected_date_data_parent").html("<div><strong>No Lesson Available</strong></div>");
    $(".selected_sesson_html").val("<div><strong>No Lesson Available</strong></div>");
    $("#book-all_lesson_data").val("");
    $("#btn_next_btn").hide();
}

$(document).on("ifClicked","input[name=\"Book[start_hour]\"]",function (event) {

    select_time(this.value);

});

function save_reschedule_approve_form(){

            $.ajax({
            type: "POST",
            url: $("#reschedule_approve_form_action").val(),
            data: {"is_approval": "yes"},
            beforeSend: function() {
                blockBody();
            },
            success:function(result){
              console.log(result);
                if(result.code == 200){
                  console.log("if", result);
                    $("#reschedule_approval_form_parent").html("");
                    window.location.href= "'.Url::to(['/booked-session/upcoming/']).'";
                    showSuccess(result.message);
                }else{
                  console.log("else", result);
                    $("#reschedule_approval_form_parent").html(result);
                    ULTRA_SETTINGS.iCheck();
                }
                unblockBody();
            },
            error:function(e){
              console.log(e.responseText);
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
}

function save_reschedule_reject_form(){

            $.ajax({
            type: "POST",
            url: $("#reschedule_approve_form_action").val(),
            data: {"is_approval": "no"},
            beforeSend: function() {
                blockBody();
            },
            success:function(result){
                if(result.code == 200){
                    $("#reschedule_approval_form_parent").html("");
                    window.location.href= "'.Url::to(['/booked-session/upcoming/']).'";
                    showSuccess(result.message);
                }else{
                    $("#reschedule_approval_form_parent").html(result);
                    ULTRA_SETTINGS.iCheck();
                }
                unblockBody();
            },
            error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
}
function cancel_reschedule_request_form() {
    $.ajax({
        type: "POST",
        url: "'.Url::to(['/book/cancel-tutor-reschedule-request/','id' => $modelBS->uuid]).'",
        data: {"tutor_cancel_request": "true"},
        beforeSend: function() { blockBody(); },
        success:function(result){
            if(result.code == 200){
                $("#reschedule_approval_form_parent").html("");
                window.location.href= "'.Url::to(['/booked-session/upcoming/']).'";
                showSuccess(result.message);
            }else{
                $("#reschedule_approval_form_parent").html(result);
                ULTRA_SETTINGS.iCheck();
            }
            unblockBody();
        },
        error:function(e){
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
}

    ', View::POS_END);
?>
<style>
    #new_when_startdate_div>.datepicker>.datepicker-inline{display: none !important;}
    #new_when_startdate_div .datepicker-inline{
        border: 1px solid #ccc;
        padding: 7px 0px;
        border-radius: 30px;
    }
    .datepicker::before{content:none;}
    .list-unstyled{
        padding:0px 15px 0px 15px;
    }
    .list-unstyled li{
        /*display: inline;*/ 
        /*margin: 5px 10px 5px 10px;*/
        /*width: 50%;*/
    }
</style>
<div class="row margin-0">
    <div class="col-xl-4 col-lg-4 col-md-12 col-12">
        <section class="box ">
            <ul class="list-group">
                <li class="list-group-item active"><h5 class="m-0 text-white" style="font-weight:600;">Lesson Information</h5></li>
                <li class="list-group-item list-group-item-danger"><span class="">Lesson Date</span><span class=" bg-danger float-right"><?= General::displayDate($modelBS->start_datetime); ?></span> </li>
                <li class="list-group-item list-group-item-success" ><span class="">Lesson Time</span><span class=" bg-success float-right"><?= General::displayTime($modelBS->start_datetime) . ' - ' . General::displayTime($modelBS->end_datetime); ?></span> </li>
                <li class="list-group-item " style="background-color: #dccde6;" ><span class="">Lesson Duration</span><span class=" bg-purple float-right"><?= $modelBS->session_min . ' Minutes'; ?></span> </li>
                <li class="list-group-item list-group-item-info" ><span class="">Instrument</span><span class=" bg-info float-right"><?= $modelBS->instrumentUu->name; ?></span> </li>
                <li class="list-group-item " style="background-color: #f3d7cf;"><span class="">Student Name</span><span class=" bg-orange float-right"><?= $modelBS->studentUu->first_name; ?></span> </li>
            </ul>
        </section>
    </div>

    <div class="col-xl-8 col-lg-8 col-md-12 col-12">

        <?php if (!empty($rescheduled_awaiting_entry)) { ?>

            <section class="box ">
                <header class="panel_header">
                    <h2 class="title float-left">Rescheduled Lesson Request</h2>
                    <div class="actions panel_actions float-right">
                        <a href="<?= Yii::$app->getUrlManager()->createUrl(['/booked-session/upcoming/']) ?>" class="btn btn-info btn-icon "><i class="fa fa-arrow-left text-white"></i> <span>Back</span></a>
                    </div>
                </header>
                <div class="content-body">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-12">
                            <div class="row">

                                <?php $div_class = (Yii::$app->user->identity->role == 'STUDENT') ? "col-lg-12 col-md-12 col-12" : "col-lg-6 col-md-6 col-6"; ?>
                                <div class="<?= $div_class ?>">
                                    <table class="table table table-bordered">
                                        <thead>
                                            <tr>
                                                <th colspan="2" class="bg-danger">Reschedule Request Information</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td style="width:40%">Lesson Date</td>
                                                <td style="width:60%"><?= General::displayDate($rescheduled_awaiting_entry->to_start_datetime); ?></td>
                                            </tr>
                                            <tr>
                                                <td>Lesson Time</td>
                                                <td><?= General::displayTime($rescheduled_awaiting_entry->to_start_datetime) . ' - ' . General::displayTime($rescheduled_awaiting_entry->to_end_datetime); ?></td>
                                            </tr>
                                            <tr>
                                                <td>Requested Date</td>
                                                <td><?= General::displayDateTime($rescheduled_awaiting_entry->created_at); ?></td>
                                            </tr>
                                            <tr>
                                                <td>Requested By</td>
                                                <td><?= $rescheduled_awaiting_entry->request_by; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-lg-5 col-md-5 col-5">
                                    <div id="reschedule_approval_form_parent">
                                        <?php if ($modelBS["is_reschedule_request"] == 2) { ?>
                                            <button type="button" class="btn btn-danger" onclick="cancel_reschedule_request_form()">Cancel Reschedule Lesson Request</button>
                                        <?php } else { ?>
                                            <?php if (Yii::$app->user->identity->role != 'STUDENT') { ?>
                                                <?=
                                                $this->render('_reschedule_approval_form', [
                                                    'model' => $rescheduled_awaiting_entry,
                                                ])
                                                ?>
                                            <?php } 
                                            }
                                        ?>
                                    </div>
                                </div>

                                
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        <?php } else { ?>

            <section class="box ">
                <header class="panel_header">
                    <h2 class="title float-left">Reschedule Lesson</h2>
                    <div class="actions panel_actions float-right">
                        <!--<a title="Availability" class="btn btn-orange btn-icon tutor_availability_calender_btn" data-id="<?= $modelBS->tutorUu->uuid ?>" data-tutorname="<?= $modelBS->tutorUu->first_name . ' ' . $modelBS->tutorUu->last_name ?>"><i class="fa fa-calendar text-white"></i></a>-->
                        <a href="<?= Yii::$app->getUrlManager()->createUrl(['/booked-session/upcoming/']) ?>" class="btn btn-info btn-icon"><i class="fa fa-arrow-left text-white"></i> <span>Back</span></a>
                    </div>
                </header>
                <div class="content-body">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-12">
                            <?php
                            $form = ActiveForm::begin([
                                        'action' => ['reschedule','id' => $modelBS->uuid],
                                        'id' => 'frm_step_4',
                                        'enableClientValidation' => true,
                                        //'enableAjaxValidation' => true,
                                        'validateOnChange' => true,
                                        'options' => ['class' => 'form-horizontal']]);
                            ?>
                            <div class="row">
                                <div class="col-md-6 pr-0">

                                    <div id="new_when_startdate_div"></div>
                                    <?php echo $form->field($model, 'session_when_start')->hiddenInput(['class' => 'form-control session_when_start', 'maxlength' => true,])->label(false) ?>
                                    
                                    <div style="border: 1px solid #ccc;padding: 7px 0px; border-radius: 30px;">
                                        <div class="col-md-12 text-center" style="margin: 10px 0px 10px 0px;">
                                            <?php
                                            $model->selected_sesson_html = (empty($model->selected_sesson_html)) ? "<div><strong>No Lesson Available</strong></div>" : $model->selected_sesson_html;
                                            echo $form->field($model, 'selected_sesson_html')->hiddenInput(['class' => 'form-control selected_sesson_html', 'maxlength' => true,])->label(false);
                                            $displayNextBtn = (!empty($model->start_hour)) ? "block" : "none";
                                            ?>
                                            <div class="" id="selected_date_data_parent">
                                                <?= $model->selected_sesson_html; ?>
                                            </div>
                                            <br>
                                            <button id="btn_next_btn" style="cursor: pointer; padding-top: 4px; display:<?= $displayNextBtn ?>;" class="btn btn-orange btn-md next_btn">SAVE LESSON</button>

                                        </div>
                                    </div>
                                    <?php echo $form->field($model, 'all_lesson_data')->hiddenInput(['class' => 'form-control all_lesson_data', 'maxlength' => true,])->label(false) ?>
                                </div>

                                <div class="col-md-6" >
                                    <div style="border: 1px solid #ccc;padding: 7px 0px; border-radius: 30px;">
                                        <div class="col-md-12 text-center">
                                            <div style="border: 1px solid #ccc;width: 100%;padding: 7px 5px;border-radius: 30px;text-align: center;">Select Start Time</div>
                                            <br>
                                        </div>
                                        <div class="col-md-12">
<!--                                            <ul class="list-unstyled">-->
                                                <?php
                                                $start_hour_array = json_decode($start_hour_json, true);
                                                $start_hour_array = (!empty($start_hour_array)) ? $start_hour_array : [0 => 'dummy'];
                                                echo $form->field($model, 'start_hour', ['template' => '{input}<div class="col-md-12">{error}</div>', 'options' => ['tag' => false]], ['class' => 'dasdasd'])
                                                        ->radioList(
                                                                $start_hour_array, [
                                                            'item' => function($index, $label, $name, $checked, $value) use($model_start_hour, $start_hour_array) {

                                                                if (count($start_hour_array) == 1 && $value == 'dummy') {
                                                                    return '<div class="col-md-12 text-center">Please select the date.</div>';
                                                                }
                                                                $checked_val = ($model_start_hour == $value) ? 'checked="checked"' : "";

                                                                $html = '<li>';
                                                                $html .= '<input tabindex="5" type="radio" id="square-radio-' . $index . '" class="skin-square-green" onchange="select_time(\'' . $value . '\');" value="' . $value . '" name="' . $name . '" ' . $checked_val . '>';
                                                                $html .= '<label class="iradio-label form-label" for="square-radio-' . $index . '">' . $label . '</label>';
                                                                $html .= '</li>';
                                                                return $html;
                                                            }
                                                                ]
                                                        )
                                                        ->label(false);
                                                ?>
<!--                                            </ul>-->
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </section>
        <?php } ?>
    </div>
</div>
