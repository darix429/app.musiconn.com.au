<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%pre_recorded_video}}".
 *
 * @property string $uuid
 * @property string $title
 * @property string $description
 * @property string $file
 * @property string $instrument_uuid
 * @property string $instrument_category_uuid
 * @property string $status
 * @property string $uploaded_date
 * @property string $publish_date
 * @property string $expire_date
 * @property string $rejected_reason
 * @property string $created_at
 * @property string $updated_at
 */
class PreRecordedVideo extends \yii\db\ActiveRecord {

    public $update_file;

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return '{{%pre_recorded_video}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
                //html tag filter
                [['title', 'description',], 'match', 'pattern' => \app\libraries\General::validTextPattern(), 'message' => 'Invalid charecters used.'], 
            
                [['uuid', 'title', 'description', 'instrument_uuid', 'instrument_category_uuid', 'status', 'rejected_reason'], 'string'],
                [['title'], 'required'],
                [['file'], 'required', 'on' => 'create'],
                [['file'], 'file', 'extensions' => 'mp4', 'maxSize'=>(1024*1024*1000)],
                [['uploaded_date', 'publish_date', 'expire_date', 'created_at', 'updated_at'], 'safe'],
            //['publish_date','compare','compareAttribute'=>'uploaded_date','operator'=>'>'],
            //['expire_date','compare','compareAttribute'=>'publish_date','operator'=>'>'],
            //[['file'], 'file', 'skipOnEmpty' => false, 'extensions' => 'ogv, mp4'],
            [['update_file'], 'safe', 'on' => 'UPDATE_VIDEO'],
                [['update_file'], 'file', 'extensions' => 'mp4', 'on' => 'UPDATE_VIDEO'],
//            [['expire_date'], 'required', 'on' => 'UPDATE_VIDEO', 'when' => function($model) {
//                    return ($model->status == "PUBLISH");
//                }, 'whenClient' => "function (attribute, value) {
//                    return ($('#prerecordedvideo-expire_date').val() == 'PUBLISH')?true:false;
//                }"],
            ['expire_date', 'compare', 'compareAttribute' => 'publish_date', 'operator' => '>=', 'on' => 'UPDATE_VIDEO',
                'when' => function($model) {
                    return $model->status == "PUBLISH";
                },
                'whenClient' => "function (attribute, value) { return $('#prerecordedvideo-expire_date').val() == 'PUBLISH'; }"
            ],
                [['tutor_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Tutor::className(), 'targetAttribute' => ['tutor_uuid' => 'uuid']],
                [['admin_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Admin::className(), 'targetAttribute' => ['admin_uuid' => 'admin_uuid']],
                [['user_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_uuid' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'file' => Yii::t('app', 'File'),
            'instrument_uuid' => Yii::t('app', 'Instrument'),
            'instrument_category_uuid' => Yii::t('app', 'Instrument Category'),
            'status' => Yii::t('app', 'Status'),
            'uploaded_date' => Yii::t('app', 'Uploaded Date'),
            'publish_date' => Yii::t('app', 'Publish Date'),
            'expire_date' => Yii::t('app', 'Expire Date'),
            'rejected_reason' => Yii::t('app', 'Rejected Reason'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'update_file' => Yii::t('app', 'File'),
        ];
    }

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['UPDATE_VIDEO'] = ['update_file', 'uuid', 'title', 'description', 'instrument_uuid', 'instrument_category_uuid', 'status', 'rejected_reason', 'uploaded_date', 'publish_date', 'expire_date', 'created_at', 'updated_at'];

        return $scenarios;
    }

    public function getTutorUu() {
        return $this->hasOne(Tutor::className(), ['uuid' => 'tutor_uuid']);
    }

    public function getAdminUu() {
        return $this->hasOne(Admin::className(), ['admin_uuid' => 'published_by_uuid']);
    }

    public function getUserUu() {
        return $this->hasOne(User::className(), ['id' => 'user_uuid']);
    }

    public function getInstrumentCategoryUu() {
        return $this->hasOne(InstrumentCategory::className(), ['uuid' => 'instrument_category_uuid']);
    }
    
    public function getInstrumentUu() {
        return $this->hasOne(Instrument::className(), ['uuid' => 'instrument_uuid']);
    }
    
    public function getAdminUploadUu() {
        return $this->hasOne(Admin::className(), ['admin_uuid' => 'admin_uuid']);
    }

    public function loginUserAuthorizeReviewVideo($id) {

        $result = false;
        $video = self::find()->where(['uuid' => $id])->one();
        if (Yii::$app->user->identity->role == 'TUTOR') {

            if (Yii::$app->user->identity->tutor_uuid == $video->tutor_uuid) {
                $result = ($video->status == 'PUBLISHED') ? true : false;
            } else {
                $result = false;
            }
        } elseif (Yii::$app->user->identity->role == 'STUDENT') {

            $result = ($video->status == 'PUBLISHED') ? true : false;
        } elseif (in_array(Yii::$app->user->identity->role, ['OWNER', 'ADMIN', 'SUBADMIN'])) {
            $result = true;
        }
        return $result;
    }

    public function loginUserAuthorizeUpdateVideo($id) {

        $result = false;
        $video = self::find()->where(['uuid' => $id])->one();
        if (Yii::$app->user->identity->role == 'TUTOR') {

            if (Yii::$app->user->identity->tutor_uuid == $video->tutor_uuid) {
                $result = ($video->status == 'UPLOADED') ? true : false;
            } else {
                $result = false;
            }
        }
        return $result;
    }

    public function loginUserAuthorizePlayVideo($id) {

        $result = false;
        if (Yii::$app->getUser()->isGuest) {
            return $result;
        }
        $video = PreRecordedVideo::find()->where(['uuid' => $id])->one();

        if (Yii::$app->user->identity->role == 'TUTOR') {

            $result = (Yii::$app->user->identity->tutor_uuid == $video->tutor_uuid) ? true : false;
        } elseif (Yii::$app->user->identity->role == 'STUDENT') {

            $student = Student::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid])->one();
            if ($student->isMonthlySubscription == '1') {

                $result = ( $video->status == 'PUBLISHED' || ($video->status == 'PUBLISHED' && $video->is_free == '1') ) ? true : false;
            } else {
                $result = ( $video->status == 'PUBLISHED' && $video->is_free == '1' ) ? true : false;
            }
        } elseif (in_array(Yii::$app->user->identity->role, ['OWNER', 'ADMIN', 'SUBADMIN'])) {
            $result = true;
        }
        return $result;
    }

    public function loginUserAuthorizePlayTutionVideo($id) {

        $result = false;
        if (Yii::$app->getUser()->isGuest) {
            return $result;
        }
        $video = BookedSession::find()->where(['uuid' => $id])->one();

        if (Yii::$app->user->identity->role == 'TUTOR') {

            $result = (Yii::$app->user->identity->tutor_uuid == $video->tutor_uuid) ? true : false;
        } elseif (Yii::$app->user->identity->role == 'STUDENT') {

            $student = Student::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid])->one();
            if ($student->isMonthlySubscription == '1') {

                $result = ( $video->status == 'PUBLISHED' && Yii::$app->user->identity->student_uuid == $video->student_uuid) ? true : false;
            } else {
                $result = false;
            }
        } elseif (in_array(Yii::$app->user->identity->role, ['OWNER', 'ADMIN', 'SUBADMIN'])) {
            $result = true;
        }
        return $result;
    }

    public function videoThumbnailGenerate($video_file_path = '', $filename = '') {

        if (!is_file($video_file_path)) {
            return false;
        }
        // Location where video thumbnail to store
        $ffmpeg_installation_path = Yii::$app->params['ffmpeg_installation_path'];
        $thumbnail_path = Yii::$app->params['media']['pre_recorded_video']['thumbnail']['path'];
        $second = 1;
        $thumbSize = '550x550';

        // Video file name without extension(.mp4 etc)
        $explode = explode('.', $filename);
        $videoname = (is_array($explode) && isset($explode[0])) ? $explode[0] : '';

        $thumbnail_path_full = $thumbnail_path . $videoname . '.jpg';

        // FFmpeg Command to generate video thumbnail

        $cmd = "{$ffmpeg_installation_path} -i {$video_file_path} -deinterlace -an -ss {$second} -t 00:00:01  -s {$thumbSize} -r 1 -y -vcodec mjpeg -f mjpeg {$thumbnail_path_full} 2>&1";

        exec($cmd, $output, $retval);

        return (!$retval) ? true : false;

        /* echo "<pre>";
          print_r($cmd);
          print_r($output);
          print_r($retval);exit;

          if ($retval)
          {
          echo 'error in generating video thumbnail';
          }
          else
          {
          echo 'Thumbnail generated successfully';
          echo $thumb_path = $thumbnail_path . $videoname . '.jpg';
          } */
    }

    public function instrumentPreRecordedVideoCount($id = '') {
        
        $where = '1=1';
        if(!empty(Yii::$app->user->identity->student_uuid)){
            $student = Student::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid])->one();
            if ($student->isMonthlySubscription == '1') {
                $where = " (is_free = 1 AND status = 'PUBLISHED') OR status = 'PUBLISHED' ";
            } else {
                $where = " (is_free = 1 AND status = 'PUBLISHED')";
            }
        }
        $res = PreRecordedVideo::find()->where(['instrument_uuid' => $id])->andWhere($where)->count();
        return $res;
    }
    public function getInstrumentPreRecordedVideo($id = '',$page = 0, $search_input = '') {
        $offset = 0;
        if($page > 0){
            $offset = Yii::$app->params['datatable']['page_length'] * $page;            
        }       
        $where = '1=1';
        if(!empty(Yii::$app->user->identity->student_uuid)){
            $student = Student::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid])->one();
            if ($student->isMonthlySubscription == '1') {
                $where = " (pre_recorded_video.is_free = 1 AND pre_recorded_video.status = 'PUBLISHED') OR pre_recorded_video.status = 'PUBLISHED' ";
            } else {
                $where = " (pre_recorded_video.is_free = 1 AND pre_recorded_video.status = 'PUBLISHED')";
            }
        }
        $model = PreRecordedVideo::find()
                //->select('tutor.uuid,tutor.first_name,tutor.last_name,admin.first_name,admin.last_name,instrument.uuid,instrument.name,pre_recorded_video.*')
                ->joinWith(['tutorUu','instrumentUu','adminUploadUu'])
                ->where($where);
        if(!empty($id)){   
            $model->andWhere(['pre_recorded_video.instrument_uuid' => $id]);
        }
        if(!empty($search_input)){
            
            $q = '("pre_recorded_video"."title" ILIKE \'%'.$search_input.'%\')';
            $q .= ' OR ("tutor"."first_name" ILIKE \'%'.$search_input.'%\')';
            $q .= ' OR ("tutor"."last_name" ILIKE \'%'.$search_input.'%\')';
            $q .= ' OR ("admin"."first_name" ILIKE \'%'.$search_input.'%\')';
            $q .= ' OR ("admin"."last_name" ILIKE \'%'.$search_input.'%\')';
            
            $model->andWhere($q);
//            $model->orFilterWhere(['ilike', 'pre_recorded_video.title', $search_input]);
//            $model->orFilterWhere(['ilike', 'tutor.first_name', $search_input]);
//            $model->orFilterWhere(['ilike', 'tutor.last_name', $search_input]);
//            $model->orFilterWhere(['ilike', 'admin.first_name', $search_input]);
//            $model->orFilterWhere(['ilike', 'admin.last_name', $search_input]);
        }
        $model->asArray()
                ->limit(Yii::$app->params['datatable']['page_length'])
                ->offset($offset)
                ->orderBy('pre_recorded_video.updated_at');
        //echo $model->createCommand()->getRawSql();exit;
        $res = $model->all();
        
        $html = self::student_video_html($res);
        
        return ['data' => $res,'html' => $html];
    }
    
    public function student_video_html($res){
        $html = '';
        if(!empty($res)){
            foreach ($res as $key => $value) {
                
                $thumb_url = is_file(Yii::$app->params['media']['pre_recorded_video']['thumbnail']['path'] . $value['uuid'] . '.jpg') ?
                Yii::$app->params['media']['pre_recorded_video']['thumbnail']['url'] . $value['uuid'] . '.jpg' :
                Yii::$app->params['theme_assets']['url'] . "images/thumbnail.jpeg";
                $r_url = \yii\helpers\Url::to(['/video/review/', 'id' => $value['uuid']]);
                
                $tname = (!empty($value['tutorUu']['first_name'])) ? $value['tutorUu']['first_name'].' '.$value['tutorUu']['last_name'] : '';
                $aname = (!empty($value['adminUploadUu']['first_name'])) ? $value['adminUploadUu']['first_name'].' '.$value['adminUploadUu']['last_name'] : '';
                $inst_name = (!empty($value['instrumentUu']['name'])) ? $value['instrumentUu']['name'] : '&nbsp;';
                
                $html .= '<div class="col-md-12" style="border: 1px solid #d2d2d2;bottom: 5px;padding: 5px;border-radius: 16px;margin-bottom:5px;">';
		     $html .= '<div class="box_video">';
			$html .= '<div class="col-md-3"><img class="video_library_img img-reponsive" onclick="window.location.href=\''.$r_url.'\'" src="'. $thumb_url.'"></div>';
			$html .= '<div class="col-md-9"><p class="orange video_content_side_bar"><b>'.$inst_name.' - '.$tname.$aname.'</b></p><p style="line-height: normal;"><b>'.$value['title'].'</b></p>';
			$html .='<p>'.$value['description'].'</p>';
		     $html .= '</div></div>';
		$html .= '</div></br>';
            }
        } else {
            $html .= '<div class="col-md-15 col-sm-3"><p>No video found.</p></div>';
        }
        return $html;
    }
    
  
}
