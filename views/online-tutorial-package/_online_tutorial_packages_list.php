<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\TutorialType;
use app\models\PaymentTerm;
use app\models\OnlineTutorialPackageType;
?>
<div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">

    <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Name</th>
                <th>Tutorial Type</th>
                <th>Payment Term</th>
                <th>Price</th>
                <th>GST</th>
                <th>Total Price</th>
                <th>Total Lesson</th>
                <th>Plan Type</th>
<!--                <th>Default Option</th>-->
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>

        <tbody>
            <?php
            if (!empty($tutorialPackageList)) {
                foreach ($tutorialPackageList as $key => $value) {
                    $type = TutorialType::find()->where(['uuid' => $value['tutorial_type_uuid']])->one();
                    $term = PaymentTerm::find()->where(['uuid' => $value['payment_term_uuid']])->one();
                    $plantype = OnlineTutorialPackageType::find()->where(['uuid' => $value['online_tutorial_package_type_uuid']])->one();
                    ?>
                    <tr>
                        <td><?= $key + 1; ?></td>
                        <td><?= $value['name']; ?></td>
                        <td><?= $type['description']; ?></td>
                        <td><?= $term['term']; ?></td>
                        <td align="right"><?= $value['price']; ?></td>
                        <td align="right"><?= $value['gst']; ?></td>
                        <td align="right"><?= $value['total_price']; ?></td>

                        <td><?= $value['duration']; ?></td>
                        <td>
                            <?php
                            if ($plantype['name'] == 'PREMIUM') {
                                $e_class = "badge-success";
                                $pointer = 'style="color:#fff"';
                            } else {
                                $e_class = "badge-warning";
                                $pointer = 'style="color:#fff"';
                            }
                            ?>
                            <span class="badge badge-pill <?= $e_class ?>"  data-uuid="<?= $value['uuid']; ?>" <?= $pointer; ?>><?= strtoupper($plantype['display_name']); ?></span>

                        </td>
<!--
                        <td ><?php //echo  ( $value['default_option'] != 0) ? $value['default_option'] : ''; ?></td>
-->
                        <td>
                            <?php
                            if ($value['status'] == 'ENABLED') {
                                $e_class = "badge-primary";
                                $pointer = 'style="cursor:pointer;color:#fff"';
                                $e_link_class = "tutorial_package_disabled_link";
                            } elseif ($value['status'] == 'DISABLED') {
                                $e_class = "badge-secondary";
                                $pointer = 'style="cursor:pointer;color:#fff"';
                                $e_link_class = "tutorial_package_enabled_link";
                            } else {
                                $e_class = "badge-danger";
                                $pointer = '';
                                $e_link_class = "";
                            }
                            ?>
                            <span class="badge badge-pill <?= $e_class ?> <?= in_array(Yii::$app->user->identity->role, ['OWNER']) ? $e_link_class : ''; ?>"  data-uuid="<?= $value['uuid']; ?>" <?= in_array(Yii::$app->user->identity->role, ['OWNER']) ? $pointer : ''; ?>><?= $value['status']; ?></span>
                        </td>

                        <td>
                            <?php if (in_array(Yii::$app->user->identity->role, ['OWNER', 'ADMIN'])) { ?>
                                <a href="<?= Url::to(['/online-tutorial-package/update/', 'id' => $value['uuid']]) ?>" class="badge badge-info badge-md" title="Update"><i class="fa fa-pencil"></i></a>
                            <?php } ?>
                            <?php if (in_array(Yii::$app->user->identity->role, ['OWNER'])) { ?>
                                <a href="javascript:void(0)" data-uuid="<?= $value['uuid']; ?>" class="delete_tutorial_package_link badge badge-danger badge-md" title="Delete"><i class="fa fa-trash"></i></a>
                                <?php } ?>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>

</div>
