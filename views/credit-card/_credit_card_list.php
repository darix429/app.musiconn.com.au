<?php

//echo "<pre>"; print_r($creditCardList); exit;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;

$this->registerJs(
        '
//        function myFunction() { 
//		var exp_month = $("#studentcreditcard-exp_month").val() 
//                $(".card-expiry-month").val(exp_month);
//                
//                var exp_year = $("#studentcreditcard-exp_year").val() 
//                $(".card-expiry-year").val(exp_year);
//                
//                var card_number = $("#studentcreditcard-number").val() 
//                $(".card-number").val(card_number);
//        }

        //stripe payment
        Stripe.setPublishableKey("pk_test_dpaQ0ewzisVIrBtMKX2wqxWX");

        function stripeResponseHandler(status, response) { 
            if (response.error) {
                $("#payBtn").removeAttr("disabled");

                $(".payment-errors").html(response.error.message);
            } else { 
                var form$ = $("#paymentFrm"); 
                var token = response["id"]; 

                form$.append("<input type=\"hidden\" name=\"stripeToken\" value=\" + token + \" />");
                form$.get(0).submit();
            }
        }
        $(document).ready(function() {
            //on form submit
            $("#paymentFrm").submit(function(event) {
                $("#payBtn").attr("disabled", "disabled");

                Stripe.createToken({
                    number: $("#studentcreditcard-number").val(),
                    cvc: $("#studentcreditcard-cvv").val(),
                    exp_month: $("#studentcreditcard-exp_month").val(),
                    exp_year: $("#studentcreditcard-exp_year").val(),
                    
                }, stripeResponseHandler);

                //submit from callback
                return false;
            });
        });


    ', View::POS_END
);
?>
<div class="col-lg-12 col-md-12 col-12 ">

    <span class="payment-errors"></span>

    <div class="row">

        <?php if (!empty($creditCardList)) { ?>

            <div class="col-lg-3">
            </div>
            <div class="col-lg-6">
                <?php
                $form = ActiveForm::begin([
                            'action' => ['create'],
                            'id' => 'paymentFrm',
                            'enableClientValidation' => true,
                            'enableAjaxValidation' => true,
                            'validateOnChange' => true,
                ]);
                ?>
                <div id="accordion" role="tablist" class="accordion-group">
                    <div class="card">
                        <div class="card-header" role="tab" >
                            <!--<input tabindex="5" type="checkbox" id="square-checkbox-2" class="skin-square-green" checked>
                            <label class="icheck-label form-label" for="square-checkbox-2"> ICICI </label>
                            <h5 class="mb-0"> ICICI </h5>-->
                            <button type="button" class="btn btn-purple btn-sm float-right delete_credit_card" data-uuid="<?= $creditCardList['uuid']; ?>">Remove Card</button>
                        </div>
                        <div class="card-body">
                            <!--  <img src="/theme_assets/images/visa_card.png"></img>-->
                            <h5 class="field-credit-card"><i class="fa fa-credit-card" aria-hidden="true" style="font-size: 25px;" ></i> &nbsp;<?= "xxxx xxxx xxxx " . substr($creditCardList['number'], -4); ?></h5>
                            <?= $form->field($model, 'uuid')->hiddenInput(['value' => $creditCardList['uuid']])->label(false); ?>
                            <?= $form->field($model, 'name')->hiddenInput(['value' => $creditCardList['name']])->label(false); ?>
                            <?= $form->field($model, 'number')->hiddenInput(['value' => $creditCardList['number']])->label(false); ?>
                            <?= $form->field($model, 'expire_date')->hiddenInput(['value' => $creditCardList['expire_date']])->label(false); ?>
                            <div class="form-row">
                                <div class="col-md-4 mb-0">
                                    <?= $form->field($model, 'cvv')->passwordInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('cvv')])->label(false) ?>
                                </div>  
                                <div class="col-md-8 mb-0">
                                    <?= Html::submitButton(Yii::t('app', 'Proceed Securely'), ['class' => 'btn btn-primary', 'id' => 'payBtn', 'style' => 'float: right;']) ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <div class="spacer"></div>
    <?php } else { ?>
        <h5 class="not-found" style="margin-left: 25px;"><b>No Credit Card Found</b></h5>
    <?php } ?>


    <div class="col-md-12">
        <div class="row credit-card-form" style="display:none;">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-10">
                <?php
                $form = ActiveForm::begin([
                            'action' => ['create'],
                            'id' => 'paymentFrm',
                            'enableClientValidation' => true,
                            'enableAjaxValidation' => true,
                            'validateOnChange' => true,
                ]);
                ?>
                <div id="accordion" role="tablist" class="accordion-group">
                    <div class="card">
                        <div class="card-header bg-primary" role="tab" >
                            <h4 class="title float-left text-white">Add credit card</h4>
                        </div>
                        <div class="card-body">
                            <!--<input type="hidden" name="exp_month" size="2" class="card-expiry-month" />
                            <input type="hidden" name="exp_year" size="4" class="card-expiry-year" />
                            <input type="hidden" name="card_num" size="20" autocomplete="off" class="card-number" />-->
                            <div class="form-row">
                                <div class="col-md-8 mb-0">
                                    <?= $form->field($model, 'name')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('name')]) ?>
                                </div>
                                <div class="col-md-4 mb-0">
                                    <?= $form->field($model, 'number')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('number')]) ?>
                                </div>

                            </div>
                            <div class="form-row">
                                <div class="col-md-4 mb-0">
                                    <?= $form->field($model, 'cvv')->passwordInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('cvv')]) ?>
                                </div>
                                <div class="col-md-4 mb-0">
                                    <?= $form->field($model, 'exp_month')->textInput(['class' => 'form-control custom_expire_month', 'data-min-view-mode' => 'months', 'data-start-view' => '1', 'data-format' => 'mm', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('exp_month')]) ?>
                                </div>
                                <div class="col-md-4 mb-0">  
                                    <?= $form->field($model, 'exp_year')->textInput(['class' => 'form-control custom_expire_year', 'data-min-view-mode' => 'years', 'data-start-view' => '2', 'data-format' => 'yyyy', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('exp_year')]) ?>
                                </div>
                            </div>

                            <?= Html::submitButton(Yii::t('app', 'Proceed Securely'), ['class' => 'btn btn-primary', 'id' => 'payBtn', 'style' => 'float: right;']) ?>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>

        </div>
    </div>

</div>

<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>








