<?php

use yii\helpers\Html;

?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear Musiconn Admin,</p>
            
            <p><?= $content['tutor_name'] . ' '. $content['tutor_last_name']; ?> has processed a booking change to an upcoming lesson with <?= $content['student_name'] . ' '. $content['student_last_name']; ?>.</p>
            
            <p>The details of the booking change are as follows:</p>
            
            <p><strong>From:</strong></p>
            
            <p><strong>Student Name :</strong> <?= $content['student_name'] . ' '. $content['student_last_name']; ?></p>
            <p><strong>Instrument :</strong> <?= $content['instrument_name']; ?></p>
            <p><strong>Previous Tutorial Date :</strong> <?= date('d-m-Y T',strtotime($content['from_start_datetime'])); ?></p>
            <p><strong>Previous Tutorial Time :</strong> <?= date('h:i A',strtotime($content['from_start_datetime'])); ?> - <?= date('h:i A',strtotime($content['from_end_datetime'])); ?></p>
            
            <p><strong>To:</strong></p>
            
            <p><strong>Student Name :</strong> <?= $content['student_name'] . ' '. $content['student_last_name']; ?></p>
            <p><strong>Instrument :</strong> <?= $content['instrument_name']; ?></p>
            <p><strong>Requested Tutorial Date :</strong> <?= date('d-m-Y T',strtotime($content['to_start_datetime'])); ?></p>
            <p><strong>Requested Tutorial Time :</strong> <?= date('h:i A',strtotime($content['to_start_datetime'])); ?> - <?= date('h:i A',strtotime($content['to_end_datetime'])); ?></p>
            
            <p>This is an automated message from the Musiconn Pty Ltd Booking System.</p>
            
            
        </td>
    </tr>
</table>

