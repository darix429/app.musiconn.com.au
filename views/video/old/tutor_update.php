<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
//rupal
use yii\web\View;
use yii\helpers\ArrayHelper;
use app\models\InstrumentCategory;
use app\models\Instrument;
use app\models\Tutor;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Admin */


if ($model['tutor_uuid'] != NULL) {
    $user = Tutor::find()->where(['uuid' => $model->tutor_uuid])->one();
}

$instrument = Instrument::find()->where(['uuid' => $model->instrument_uuid])->one();
$instrumentCategory = InstrumentCategory::find()->where(['uuid' => $model->instrument_category_uuid])->one();

$this->registerCssFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/ios-switch/css/switch.css');

$file_path = Yii::$app->params['media']['pre_recorded_video']['path'] . $model->file;
$poster = (is_file($file_path)) ? '' : Yii::$app->params['theme_assets']['url'] . "images/novideo.png";
?>

<?php
$this->registerJs(
        '
       
        $(document).on("click","#savebutton",function(){ 
            
            var file_data = $("#prerecordedvideo-file").prop("files")[0]; 
            var form_data = new FormData($("form")[2]);   
            form_data.append("file", file_data);
            $(".msg").text("Uploading in progress..."); 
            $.ajax({
                    type: "POST",
                    url: $( "#upload_form" ).attr("action"),
                    data:  form_data,
                    processData: false,
                    contentType: false,
                    beforeSubmit: function() {
                        blockBody();
                        $("#progress-bar").width("0%");
                    },
                    xhr: function () { 
                            blockBody();
                            var xhr = new window.XMLHttpRequest();
                            xhr.upload.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                percentComplete = parseInt(percentComplete * 100);                       

                                $("#progress-bar").width(percentComplete + "%");
                                $("#progress-status").html(percentComplete + "%");
                            }
                        }, false);
                        return xhr;
                    }, 
                    success: function (result) { 
                        
                        if(result.code == 200){ 
                            $(".msg").text(result.message);
                            $("#progress-bar").width("0%");
                            if($("#progress-status").html() == "100%"){
                                
//                                $("#progress-bar").width("0%");
//                                $( ".form-open" ).hide();
//                                $(".msg").hide();
//                                window.location.reload();
//                                showSuccess(result.message);
//                                
                                 $("#progress-bar").width("0%");
                                    $( ".form-open" ).hide();
                                    $(".msg").hide(); 
                                    window.location.reload();
                                  
                            }
                        } else {
                            $("#uploadFormDiv").html(result);
                            $("#progress-bar").width("0%");
                        }
                        unblockBody();
                    }, error:function(e){ 
                        unblockBody();
                        $("#progress-bar").width("0%");
                        showErrorMessage(e.responseText);
                    }
                });

        });

        $(document).on("click",".edit",function(){ 
                $( ".form-open" ).show();
                $("html,body").animate({
                    scrollTop: $(".form-open").offset().top},
                    "slow");
        
        });
        

    
', View::POS_END
);
?>
<style>
    .video-js {
        width: 985px;
        height: 430px;
    }
    .edit{
        margin-top: 11px;
    }
    .download{
        margin-top: 11px;
    }
    

    .title-details{
        margin-top: 16px;
    }
</style>

<div class="col-xl-12 col-lg-12 col-12 col-md-12">
    <section class="box scroll-test">
        <header class="panel_header">
            <h2 class="title float-left">Video Information</h2>
            <div class="actions panel_actions float-right">
                <?php if ($model->status !== 'PUBLISHED') { ?>
                    <a href="<?= Yii::$app->getUrlManager()->createUrl(['/video/uploaded/']) ?>" class="btn btn-info btn-icon "><i class="fa fa-arrow-left text-white"></i> <span>Back</span></a>
                <?php } else { ?>
                    <a href="<?= Yii::$app->getUrlManager()->createUrl(['/video/published/']) ?>" class="btn btn-info btn-icon "><i class="fa fa-arrow-left text-white"></i> <span>Back</span></a>
                <?php } ?>
            </div>
        </header>

        <div class="content-body">
            <div class="row">
                <div class="col-md-12 mb-0">
                    <video id="myVideo" class="video-js resize" controls preload="auto" controlsList="nodownload"  poster="<?= $poster; ?>">
                        <source src="<?= Yii::$app->getUrlManager()->createUrl(['/video/play/','id' => $model->uuid]) ?>" type="video/mp4" >
                    </video> 
                </div>

                <div class="col-lg-8 col-md-12 col-12" style="border-right-style: ridge;">
                    <div class="form-row">
                        <div class="col-md-12 mb-0">
                            <h4><b><?= $model->title; ?> </b></h4>
                            <p><small class="text-muted"><i class="fa fa-clock-o"></i> <?= \app\libraries\General::displayDate($model->created_at); ?>&nbsp;<a class="text-primary more" data-id="<?= $model->uuid; ?>"> More </a></small></p>

                            <p class="text-primary"><i class="fa fa-smile-o" aria-hidden="true" style="font-size:26px;"></i>&nbsp;<b style="font-size:17px;"><?= $user['first_name'] . ' ' . $user['last_name']; ?> </b></p>

                            <?php if ($model->status !== 'PUBLISHED') { ?>
                                <button class="btn btn-purple  btn-icon bottom15 right15 edit" >
                                    <i class="fa fa-pencil"></i> &nbsp; <span>Edit</span>
                                </button>
                            <?php } ?>
                            
                            <a href="<?= Yii::$app->getUrlManager()->createUrl(['/video/download/','id' => $model->uuid]) ?>" download>
                                <button class="btn btn-purple  btn-icon bottom15 right15 download">
                                    <i class="fa fa-download"></i> &nbsp; <span>Download</span>
                                </button>
                            </a>
                            <?php if (!empty($model->instrument_uuid)) { ?>
                                <p><b>Instrument :  </b><?= $instrument->name; ?></p>
                                <?php
                            }
                            if (!empty($model->instrument_category_uuid)) {
                                ?>
                                <p><b>Category :  </b><?= $instrumentCategory->name; ?></p>
                            <?php
                            }
                            if (!empty($model->description)) {
                                ?>
                                <h5><b>Description</b></h5>
                                <p><?= $model->description; ?></p>
<?php } ?>

                        </div>
                    </div>
                    <br><br>
                    <div class="row form-open" style="display:none; border-top-style: ridge;">
                        <div class="col-lg-12 col-md-12 col-12">
                            <div class="form-row">
                                <h3 class="title float-left"><b>Update Video</b></h3>
                                <div class="col-md-12 mb-0">

                                    <div id="uploadFormDiv">
                                        <?=
                                        $this->render('_tutor_update_form', [
                                            'model' => $model,
                                        ])
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-lg-4 col-md-12 col-12">
                    <div class="col-lg-12">
                        <h4><b> More Videos </b></h4>
                        <!--<p>In design, space is concerned with the area <del>deep within the moment</del> of designated design.</p>-->

                        <div class="tab-pane fade show active" id="web-1">
                            <?php
                            $tutor_uuid = Yii::$app->user->identity->tutor_uuid;
                            if ($model->status !== 'PUBLISHED' && $model->tutor_uuid == $tutor_uuid) {
                                $recordData = app\models\PreRecordedVideo::find()->where(['<>', 'uuid', $model->uuid])->andWhere(['status' => 'UPLOADED'])->andWhere(['tutor_uuid' => $user->uuid])->asArray()->limit(5)->orderBy('updated_at')->all();
                            } else {
                                $recordData = app\models\PreRecordedVideo::find()->where(['<>', 'uuid', $model->uuid])->andWhere(['status' => 'PUBLISHED'])->andWhere(['tutor_uuid' => $user->uuid])->asArray()->orderBy('updated_at')->all();
                            }

                            if (!empty($recordData)) {
                                foreach ($recordData as $key => $value) {
                                    $thumb_url = is_file(Yii::$app->params['media']['pre_recorded_video']['thumbnail']['path'] . $value['uuid'].'.jpg') ? 
                                                        Yii::$app->params['media']['pre_recorded_video']['thumbnail']['url'] . $value['uuid'].'.jpg' :
                                                        Yii::$app->params['theme_assets']['url'] . "images/thumbnail.jpeg";
                                    ?>
                                    <div class="search_result_more_video">
                                        <div class="float-left col-md-5 col-3 related-video">
                                            <a href="<?= Url::to(['/video/update/', 'id' => $value['uuid']]) ?>">
                                                <img class="small-size" src="<?= $thumb_url; ?>">
                                            </a>
                                        </div>
                                        <div class="float-left col-md-7 col-9 title-details" >

                                            <h6><a href="<?= Url::to(['/video/update/', 'id' => $value['uuid']]) ?>"><?= $value['title']; ?></a></h6>
                                            
                                            <p><?php
                                                if ($value['tutor_uuid'] != NULL) {
                                                    $username = \app\models\Tutor::find()->where(['uuid' => $value['tutor_uuid']])->one();
                                                    echo $username['first_name'] . ' ' . $username['last_name'];
                                                }
                                                ?>
                                            </p>
                                        </div>
                                    </div>
                                    <?php
                                }
                            } else {
                                ?>
                                <h6>No video found</h6>
                            <?php }
                            ?>

                        </div>

                    </div>

                </div>

            </div>  <!--end row-->
        </div>
    </section>

