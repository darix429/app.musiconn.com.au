<?php

use yii\helpers\Html;
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['student_name']; ?>,</p>

            <p>Your request for cancellation of a lesson has been approved by Musiconn Admin.</p>

            <p>The details of your cancelled lesson are as follows:</p>

            <p><strong>Student Name:</strong> <?= $content['student_name']." ".$content['student_last_name']; ?></p>
            <p><strong>Tutor Name:</strong> <?= $content['tutor_name']." ".$content['tutor_last_name']; ?></p>
            <p><strong>Lesson Date:</strong> <?= $content['from_date']; ?></p>
            <p><strong>Lesson Time:</strong> <?= $content['from_time']; ?></p>
            <p><strong>Musicoins added back to your account:</strong> <?= $content['credited_musicoins']; ?></p>

            <p>Please do not reply to this unatttended mailbox. Any queries can be directed to admin@musiconn.com.au, or 1300 068 742 if you have any queries.</p>

            <p>Regards,<br>
            System Administrators</p>
        </td>
    </tr>
</table>
