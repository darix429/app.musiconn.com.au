<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Students');
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs(
        '
        $(document).on("click",".student_active_link",function(){
            $.ajax({
                type: "GET",
                url: "' . Url::to(['/student/send-activation-link/']) . '",
                data: { "id": $(this).data("uuid")},
                //beforeSend:function(){ $(".local-spinner").show(); },
                success:function(result){
                    if(result.code == 200){
                        showSuccess(result.message);
                    }else{
                        showErrorMessage(result.message);
                    }
                },
                error:function(e){
                    showErrorMessage("Sorry, something wrong.");
                }
            });
        });

        $(document).on("click",".student_disabled_link", function(){
            if(confirm("Are you sure, you want to disable this account?")){
            var element = $(this);
                $.ajax({
                    type: "GET",
                    url: "' . Url::to(['/student/account-disabled/']) . '",
                    data: { "id": $(this).data("uuid")},
                    //beforeSend:function(){ $(".local-spinner").show(); },
                    success:function(result){
                        if(result.code == 200){
                            $(element).removeClass("badge-primary");
                            $(element).addClass("badge-secondary");
                            $(element).removeClass("student_disabled_link");
                            $(element).addClass("student_enabled_link");
                            $(element).text("DISABLED");
                            showSuccess(result.message);
                        }else{
                            showErrorMessage(result.message);
                        }
                    },
                    error:function(e){
                        showErrorMessage("Sorry, something wrong.");
                    }
                });
            }
        });

        $(document).on("click",".student_enabled_link", function(){
            if(confirm("Are you sure, you want to enable this account?")){
            var element = $(this);
                $.ajax({
                    type: "GET",
                    url: "' . Url::to(['/student/account-enabled/']) . '",
                    data: { "id": $(this).data("uuid")},
                    //beforeSend:function(){ $(".local-spinner").show(); },
                    success:function(result){
                        if(result.code == 200){
                            $(element).removeClass("badge-secondary");
                            $(element).addClass("badge-primary");
                            $(element).removeClass("student_enabled_link");
                            $(element).addClass("student_disabled_link");
                            $(element).text("ENABLED");
                            showSuccess(result.message);
                        }else{
                            showErrorMessage(result.message);
                        }
                    },
                    error:function(e){
                        showErrorMessage("Sorry, something wrong.");
                    }
                });
            }
        });
        $(document).on("click","#search_btn", function(){
            $.ajax({
                type: "GET",
                url: "' . Url::to(['/student/index/']) . '",
                data: $("#search_frm").serialize(),
                beforeSend:function(){ blockBody(); },
                success:function(result){
                    unblockBody();
                    $("#student_table_parent_div").html(result);
                    window.ULTRA_SETTINGS.dataTablesInit();
                },
                error:function(e){
                    unblockBody();
                    showErrorMessage(e.responseText);
                }
            });
        });

        $(document).on("click","#search_reset_btn", function(){
            $.ajax({
                type: "GET",
                url: "' . Url::to(['/student/index/']) . '",
                data: {},
                beforeSend:function(){ blockBody(); },
                success:function(result){
                    unblockBody();
                    $("#student_table_parent_div").html(result);
                    window.ULTRA_SETTINGS.dataTablesInit();
                },
                error:function(e){
                    unblockBody();
                    showErrorMessage(e.responseText);
                }
            });
        });

        $(document).on("click",".cancel_monthly_subscription_paln_student",function(){
            if(confirm("Are you sure, you want to approve cancellation request for student monthly subscription plan?")){
                var element = $(this);
                $.ajax({
                    type: "GET",
                    url: "' . Url::to(['/student/monthly-subscription-cancel/']) . '",
                    data: { "id": $(this).data("uuid")},
                    //beforeSend:function(){ $(".local-spinner").show(); },
                    success:function(result){
                        if(result.code == 200){
                            $(element).remove();
                            showSuccess(result.message);

                        }else{
                            showErrorMessage(result.message);
                        }
                    },
                    error:function(e){
                        showErrorMessage("Sorry, something wrong.");
                    }
                });
           	 }
        });
		
		$(document).on("click",".delete_student_link", function(){
			if(confirm("Are you sure, you want to delete this account?")){
				var element = $(this);
				console.log("adssf");
				console.log($(this).data("uuid"));
				$.ajax({
					type: "POST",
					url: "'.Url::to(['/student/delete/']).'",
					data: { "id": $(this).data("uuid")},
					success:function(result){
						if(result.code == 200){

							$(element).closest("tr").remove();

							showSuccess(result.message);
						}else{
							showErrorMessage(result.message);
						}
					},
					error:function(e){ 
						showErrorMessage(e.responseText);
					}
				});
			}
		});
    '
);
?>


<div class="col-xl-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Student List</h2>
            <div class="actions panel_actions float-right">
                <i class="fa fa-search icon-primary icon-xs icon-orange mail_head_icon custom-toggle" data-togglediv="datatable-search-div" title="Advance Search"></i>
                <!--<a class="btn btn-purple btn-icon" id="add_credit_btn" href="<?= Url::to(['/student/credit-lesson-admin/']) ?>">Add Credit</a>-->
                <a class="btn btn-purple btn-icon" onclick="openCreditDebitCoinForm();">Credit/Debit Musicoins</a>
            </div>
        </header>
        <div class="content-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12 ">
                    <div class="datatable-search-div " style="display: none;">

                        <?php
                        $form = ActiveForm::begin([
                                    'method' => 'get',
                                    'options' => [
                                        'id' => 'search_frm',
                                        'class' => 'form-inline'
                                    ],
                        ]);
                        ?>

                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'fullname')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Name', 'style' => 'width:100%'])->label(false) ?>
                        </div>
                        <div class="col-md-4 mb-0">
                            <?= $form->field($searchModel, 'email')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $searchModel->getAttributeLabel('email'), 'style' => 'width:100%'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'status')->dropDownList(['ENABLED' => 'ENABLED', 'DISABLED' => 'DISABLED', 'DELETED' => 'DELETED','INACTIVE'=>'INACTIVE'], ['class' => 'form-control', 'style' => 'width:100%'])->label(false) ?>
                        </div>

                            <?= Html::Button(Yii::t('app', 'Search'), ['class' => 'btn btn-primary mb-0', 'id' => 'search_btn']) ?>
                            <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-purple mb-0', 'id'=>'search_reset_btn']) ?>


                        <?php ActiveForm::end(); ?>
                    </div>
                </div>

            </div>
            <br>
            <div class="clearfix"></div>
            <div class="row" id="student_table_parent_div">
                <?php echo Yii::$app->controller->renderPartial('_student_list', ['studentList' => $studentList]); ?>
            </div>
        </div>
    </section>
</div>
