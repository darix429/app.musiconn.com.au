<?php

use yii\helpers\Html;

?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['student_name']; ?>,</p>
            
            <p>Thank you for submitting a request to change a tutorial booking. Your request is awaiting approval by your tutor and you will be notified by email once it’s been confirmed.</p>
            
            <p>The details of your booking change request #<?= $content['reschedule_number']; ?> are as follows:</p>
            
            <p><strong>From:</strong></p>
            
            <p><strong>Tutor Name:</strong> <?= $content['tutor_name'].' '.$content['tutor_last_name']; ?></p>
            <p><strong>Previous Tutorial Date:</strong> <?= \app\libraries\General::convertTimezone($content['from_start_datetime'],Yii::$app->params['timezone'],$content['student_timezone'],'Y-m-d T'); ?></p>
            <p><strong>Previous Tutorial Time:</strong> <?= \app\libraries\General::convertTimezone($content['from_start_datetime'],Yii::$app->params['timezone'],$content['student_timezone'],'h:i A'); ?> - <?= \app\libraries\General::convertTimezone($content['from_end_datetime'],Yii::$app->params['timezone'],$content['student_timezone'],'h:i A' ); ?></p>
            
            <p><strong>To:</strong></p>
            
            <p><strong>Tutor Name:</strong> <?= $content['tutor_name'].' '.$content['tutor_last_name']; ?></p>
            <p><strong>Requested Tutorial Date:</strong> <?= \app\libraries\General::convertTimezone($content['to_start_datetime'],Yii::$app->params['timezone'],$content['student_timezone'],'Y-m-d T'); ?></p>
            <p><strong>Requested Tutorial Time:</strong> <?= \app\libraries\General::convertTimezone($content['to_start_datetime'],Yii::$app->params['timezone'],$content['student_timezone'],'h:i A'); ?> - <?= \app\libraries\General::convertTimezone($content['to_end_datetime'],Yii::$app->params['timezone'],$content['student_timezone'],'h:i A' ); ?></p>
            
            <p>Please do not reply to this unattended mailbox. Any queries can be directed to admin@musiconn.com.au or 1300 068 742.</p>
            
            <p>Regards,<br>
            Admin Team – Musiconn Pty Ltd</p>
        </td>
    </tr>
</table>

