<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">

    <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Name</th>
                <th>Price</th>
                <th>Tax</th>
		<th>Total Price</th>
		<!--<th>Is Tutorial</th>-->
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>

        <tbody>
            <?php
            if (!empty($monthlyFeeList)) {  
                foreach ($monthlyFeeList as $key => $value) { 
                    ?>
                    <tr>
                        <td><?= $key + 1; ?></td>
                        <td><?= $value['name']; ?></td>
                        <td align='right'><?= $value['price']; ?></td>
                        <td align='right'><?= $value['tax']; ?></td>
			<td align='right'><?= $value['total_price']; ?></td>
			<!--<td>
				<?php 
				if($value['with_tutorial'] == 1){
					echo "YES"; 
				} else{
					echo "NO"; 
				}
				?>
			</td>-->
			<td>
                            <?php
                         
                            if ($value['status'] == 'ENABLED') {
                                $e_class = "badge-primary";
				$pointer = 'style="cursor:pointer;color:#fff"';
                                $e_link_class = "fee_disabled_link";
                            } elseif ($value['status'] == 'DISABLED') {
                                $e_class = "badge-secondary";
				$pointer = 'style="cursor:pointer;color:#fff"';
                                $e_link_class = "fee_enabled_link";
                            } else {
                                $e_class = "badge-danger";
				$pointer = '';                                
                                $e_link_class = "";
                            }
                            ?>
                            <span class="badge badge-pill <?= $e_class ?> <?= $e_link_class ?>"  data-uuid="<?= $value['uuid']; ?>" <?= $pointer ?>><?= $value['status']; ?></span>
                        </td>
                        <td>
                            	<a href="<?= Url::to(['/monthly-subscription-fee/update/', 'id' => $value['uuid']]) ?>" class="badge badge-info badge-md" title="Update"><i class="fa fa-pencil"></i></a>
                                <a href="javascript:void(0)" data-uuid="<?= $value['uuid']; ?>" class="delete_fee_link badge badge-danger badge-md" title="Delete"><i class="fa fa-trash"></i></a>
                                
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>

</div>
