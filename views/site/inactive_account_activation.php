<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = "Inactive Account Activation";
?>
<div class="col-xl-12">
    <section class="box nobox">
        <div class="content-body">    
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12">
                    <?php
                    if($return['code'] != 200){
                    ?>
                        <h1 class="page_error_code text-purple"><?= $return['code']; ?></h1>
                        <h1 class="page_error_info"><?= (Html::encode($return['message'])) ?></h1>
                    <?php }else{ ?>
                        <h1 class="page_error_info"><?= (Html::encode($return['message'])) ?></h1>
                    <?php } ?>

                    <div class="col-12">
                        <div class="row justify-content-center">
                            <form action="javascript:;" method="post" class="page_error_search" style="width: 60%;">
                                <div class="text-center page_error_btn">
                                    <a class="btn btn-purple btn-lg" href='<?= Url::to(['student/send-activation-link','id' => $uuid],true);?>'><i class='fa fa-location-arrow'></i> &nbsp; Request To New Activation Link</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>