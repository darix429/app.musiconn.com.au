<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Url;
use app\models\Instrument;
use app\models\StudentTuitionBooking;
$step_data = StudentTuitionBooking::getSession(Yii::$app->user->identity->student_uuid);

?>

<table class="table table-striped table-hover">
    <?php
    if (!empty($onlineTutorialList)) {
        foreach ($onlineTutorialList as $key => $value) {
            
            $displayClass = (isset($step_data['step_2']['onlinetutorial_uuid']) && $step_data['step_2']['onlinetutorial_uuid'] == $value->uuid) ? "" : "d-none";
            $checked = (isset($step_data['step_2']['onlinetutorial_uuid']) && $step_data['step_2']['onlinetutorial_uuid'] == $value->uuid) ? "checked" : "";
            ?>
            <tr class="unread onlinetutorial_tr" >
                <td class="" style="width: 10%">
                   
                    <input tabindex="5" type="radio"  class="skin-square-red float-left onlinetutorial_checkbox" name="StudentTuitionBooking[onlinetutorialcheckbox]" value="<?= $value->uuid;?>" <?= $checked;?>>
                </td>
                <td class="open-view" style="width: 50%"><?= $value->name ?></td>
                <td class="" style="width: 10%">
                   $<?= $value->total_price; ?>
                </td>

                <td class="open-view">
                    <?= Html::submitButton('Select', ['class' => "btn btn-orange btn-md onlinetutorial_select_btn $displayClass"]) ?>
                </td>
            </tr>
            <?php
        }
    } else {
        echo "No plan available.";
    }
    ?>
</table>
