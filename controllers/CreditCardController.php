<?php

namespace app\controllers;

use Yii;
use app\models\StudentCreditCard;
use app\models\StudentCreditCardSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * StudentCreditCardController implements the CRUD actions for StudentCreditCard model.
 */
class CreditCardController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all StudentCreditCard models.
     * @return mixed
     */
    public function actionIndex() { 
        $model = new StudentCreditCard();
        $student_uuid = Yii::$app->user->identity->student_uuid;
        $creditCardList = StudentCreditCard::find()->where(['student_uuid' => $student_uuid])->asArray()->orderBy('updated_at DESC')->one();
        
        if (Yii::$app->request->isAjax) { 
            return $this->renderAjax('_credit_card_list', [
                        'creditCardList' => $creditCardList,
                        'model' => $model,
            ]);
        } else { 
            return $this->render('index', [
                        'creditCardList' => $creditCardList,
                       'model' => $model,
            ]);
        }
    }

    /**
     * Displays a single StudentCreditCard model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new StudentCreditCard model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() { 
        
        $credit_card = StudentCreditCard::find()->where(['number' => $_POST['StudentCreditCard']['number']])->one();
        $student_uuid = Yii::$app->user->identity->student_uuid;
        //index view
        $creditCardList = StudentCreditCard::find()->where(['student_uuid' => $student_uuid])->asArray()->orderBy('updated_at DESC')->one();
        
        if (empty($credit_card)) { 
            $model = new StudentCreditCard();
            $model->attributes = Yii::$app->request->post('StudentCreditCard');
            
            Yii::$app->session['creditCard_uuid'] = $model->attributes['uuid'];
            
            if ($model->load(Yii::$app->request->post())) { //echo "123"; exit;
                //$model->validate()
                
                $model->student_uuid = $student_uuid;
                $model->status = 'ENABLED';
                $model->expire_date = date('Y-m-t', strtotime($model->expire_date));
                $model->created_at = $model->updated_at = date('Y-m-d H:i:s', time());
                if ($model->save()) { 
                    return $this->redirect(['payment-transaction/create', 'model' => $model]);
                }
            }
          
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = 'json';
                return \yii\widgets\ActiveForm::validate($model);
            } else { 
                $model->expire_date = date('Y-m');
                return $this->render('index', [
                        'model' => $model,
                        'creditCardList' => $creditCardList,
                ]);
            }
        
        }else{ 
            $id = $_POST['StudentCreditCard']['uuid'];
            $model = $this->findModel($id);

            $model->attributes = Yii::$app->request->post('StudentCreditCard');
            
            Yii::$app->session['monthlyPlan_uuid'] = $model->attributes['uuid'];
            
            if ($model->load(Yii::$app->request->post()) && $model->validate()) { 
                
                $model->cvv = $model->attributes['cvv'];
                
                if ($model->save()) {
                    //return $this->redirect('https://stripe.com/docs/testing#cards');
                }
                else{
                    return $this->render('index', [
                        'model' => $model,
                        'creditCardList' => $creditCardList,
                    ]);
                }
            }
            
            
            /*if($credit_card['cvv'] !== $model->attributes['cvv']){
                Yii::$app->session->setFlash('success', Yii::t('app', 'CVV not match'));
		return $this->redirect(['index']);
            }
            else{
                return $this->redirect('https://stripe.com/docs/testing#cards');
            }*/
            
        }


        

        /*$model = new StudentCreditCard();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->uuid]);
        }

        return $this->render('create', [
                    'model' => $model,
        ]);*/
    }

    /**
     * Updates an existing StudentCreditCard model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->uuid]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing StudentCreditCard model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete() { 
        $id = Yii::$app->getRequest()->post('id');
        
        if ((($model = StudentCreditCard::findOne($id)) !== null)) {
            $model->delete();
            $return = ['code' => 200, 'message' => 'Card deleted successfully.'];
        } else {
            $return = ['code' => 404, 'message' => 'The requested page does not exist.'];
        }
        Yii::$app->response->format = 'json';
        return $return;
        
        //$this->findModel($id)->delete();

        //return $this->redirect(['index']);
    }

    /**
     * Finds the StudentCreditCard model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return StudentCreditCard the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = StudentCreditCard::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

}
