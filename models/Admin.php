<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "admin".
 *
 * @property string $admin_uuid
 * @property string $admin_type
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $phone
 * @property string $company_position
 * @property int $updated_at
 * @property int $created_at
 * @property string $status
 */
class Admin extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'admin';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //html tag filter
            [['first_name', 'last_name', 'phone', 'company_position'], 'match', 'pattern' => \app\libraries\General::validTextPattern(), 'message' => 'Invalid charecters used.'], 
            
            [['admin_type', 'first_name', 'last_name', 'email', 'phone', 'company_position', 'updated_at', 'created_at'], 'required'],
            [['admin_uuid', 'first_name', 'last_name', 'email', 'company_position', 'status'], 'string'],
            [['updated_at', 'created_at'], 'default', 'value' => null],
            [['updated_at', 'created_at'], 'integer'],
            [['admin_type'], 'string', 'max' => 50],
            //[['phone'], 'string', 'max' => 10],
            [['phone'], 'match','pattern'=> '/^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$/','message' => 'Enter a valid Phone.'],
            //[['phone'], 'match','pattern'=> '/^\(ddd) dddd dddd$/','message' => 'Enter a 10 digit valid Phone Number.'],
            [['admin_uuid'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'admin_uuid' => Yii::t('app', 'Admin Uuid'),
            'admin_type' => Yii::t('app', 'User Type'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'company_position' => Yii::t('app', 'Company Position'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_at' => Yii::t('app', 'Created At'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
    
    public function getUser()
    {
        return $this->hasOne(User::className(), ['admin_uuid' => 'admin_uuid']);
    }
}
