<?php

use yii\helpers\Html;

?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['student_name']; ?>,</p>
            
            <p>You have successfully booked new Credited Lesson in <?= Yii::$app->name; ?>.</p>
            
            <p><strong>Tutor Name :</strong> <?= $content['tutor_name']; ?></p>
            <p><strong>Instrument :</strong> <?= $content['instrument']; ?></p>
            <p><strong>Lesson Duration :</strong> <?= $content['tution_duration']; ?> Minutes</p>
            <p><strong>Lesson Date :</strong> <?php echo (is_array($content['session_dates'])) ? implode(', ', $content['session_dates']) : ''; ?></p>
            <p><strong>Lesson Time :</strong> <?php echo $content['time']; ?></p>
                        
            <p> If any query for your account then please contact to administrator.</p>

            <p>Sincerely,</p>
            <p>System Administrators</p>
        </td>
    </tr>
</table>

