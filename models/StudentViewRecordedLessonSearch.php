<?php

namespace app\models;
use Yii;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\StudentViewRecordedLesson;
use yii\db\Expression;

/**
 * StudentViewRecordedLessonSearch represents the model behind the search form of `app\models\StudentViewRecordedLesson`.
 */
class StudentViewRecordedLessonSearch extends StudentViewRecordedLesson
{
    public $tutorName;
     public $instrumentName;
     public $studentName;
     public $studentId;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'booked_session_uuid', 'student_uuid', 'tutor_uuid', 'instrument_uuid', 'datetime','tutorName','instrumentName','studentName','studentId'], 'safe'],
            [['part'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $modeDatatable = false, $statusnot = '') {

        //$query = StudentViewRecordedLesson::find()->select('student_view_recorded_lesson.booked_session_uuid,tutor.*,instrument.*')->joinWith([ 'tutorUu', 'instrumentUu']);

        $sql = 'SELECT "student_view_recorded_lesson"."booked_session_uuid", "tutor".*, "instrument".* FROM "student_view_recorded_lesson" LEFT JOIN "tutor" ON "student_view_recorded_lesson"."tutor_uuid" = "tutor"."uuid" LEFT JOIN "instrument" ON "student_view_recorded_lesson"."instrument_uuid" = "instrument"."uuid" ';
// add conditions that should always apply here

        /*$dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        
       */
        
        $this->load($params);

        
        
        $where = '';
         if (!empty($this->tutorName)) {
             $where .= " where concat(tutor.first_name,' ',tutor.last_name) like '%".$this->tutorName."%' ";
            //$query->andFilterWhere(['ilike', "concat(tutor.first_name,'',tutor.last_name)", $this->tutorName]);
        }
        
        
        if (!empty($this->instrumentName)) {
            if($where == '')
             {
               $where .= " where instrument.name like '%".$this->instrumentName."%' ";

             } else {
                 $where .= " and instrument.name like '%".$this->instrumentName."%' ";
             } 
            //$query->andFilterWhere(['ilike', "instrument.name", $this->instrumentName]);
        }
        
         if (!is_null($this->datetime) && 
            strpos($this->datetime, ' - ') !== false ) {
            list($start_date, $end_date) = explode(' - ', $this->datetime);
            if($where == '')
             {
            $where .= " where student_view_recorded_lesson.datetime >= '".date("Y-m-d", strtotime($start_date))." 00:00:00'  and student_view_recorded_lesson.datetime  <= '".date("Y-m-d", strtotime($end_date))." 23:59:59' ";
            } else {
            $where .= " and student_view_recorded_lesson.datetime >= '".date("Y-m-d", strtotime($start_date))." 00:00:00'  and student_view_recorded_lesson.datetime  <= '".date("Y-m-d", strtotime($end_date))." 23:59:59' ";
             } 
                    }  
 
        
        $sql .= $where . 'GROUP BY "student_view_recorded_lesson"."booked_session_uuid", "tutor"."uuid", "instrument"."uuid"';        

        if ($modeDatatable) {
            //echo $query->groupBy('student_view_recorded_lesson.booked_session_uuid,tutor.uuid,instrument.uuid')->createCommand()->getRawSql();exit;
            //$result = $query->groupBy('student_view_recorded_lesson.booked_session_uuid,tutor.uuid,instrument.uuid')->asArray()->all();
            $result = Yii::$app->db->createCommand($sql)->queryall();
            //echo "<pre>"; print_r($result);exit; 
            return $result;
        }
        return $dataProvider;
    }

    
        public function searchDetail($params, $modeDatatable = false, $statusnot = '') {

        $query = StudentViewRecordedLesson::find()->joinWith(['studentUu', 'tutorUu', 'instrumentUu']);


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        if (!empty($this->tutor_uuid)) {
            $query->andFilterWhere(['=', '"student_view_recorded_lesson"."tutor_uuid"', $this->tutor_uuid]);
        } else {
            $null = new Expression('NULL');

                        $query->andFilterWhere(['is not', '"student_view_recorded_lesson"."tutor_uuid"', $null]);

        }
         if (!empty($this->studentName)) {
            $query->andFilterWhere(['ilike', "concat(student.first_name,'',student.last_name)", $this->studentName]);
        }
        
        if (!empty($this->studentId)) {
            $query->andFilterWhere(['ilike', "student.enrollment_id", $this->studentId]);
        }
        
        if (!empty($this->instrumentName)) {
            $query->andFilterWhere(['ilike', "instrument.name", $this->instrumentName]);
        }
        
 
        
         if (!is_null($this->datetime) && 
            strpos($this->datetime, ' - ') !== false ) {
            list($start_date, $end_date) = explode(' - ', $this->datetime);
            

            $query->andFilterWhere(['>=', '"student_view_recorded_lesson"."datetime"', date("Y-m-d", strtotime($start_date))." 00:00:00"]);
            $query->andFilterWhere(['<=', '"student_view_recorded_lesson"."datetime"', date("Y-m-d", strtotime($end_date))." 23:59:59"]);
        }

        

       

        $query->andFilterWhere(['=', 'booked_session_uuid', $this->booked_session_uuid]);
        if ($modeDatatable) {
            $result = $query->asArray()->orderBy('datetime DESC')->all();

            //echo "<pre>"; print_r($result); 
            return $result;
        }
        return $dataProvider;
    }

}
