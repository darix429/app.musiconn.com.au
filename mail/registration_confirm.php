<?php

use yii\helpers\Html;
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['name'] ?>,</p>
            
            <p>Thank you for registering with us, and welcome to the Musiconn family.</p>
            
            <p>Whatever your musical genre, I hope that we can play a role in nurturing your interest and help you attain your goals.</p>

            <p>To book lessons with one of our tutors, or subscribe to our range of quality online educational contents, please <a target="_blank" href="https://www.musiconn.com.au/">visit our website</a>.</p>
            
            <p>Regards,<br>
            Julian Fung <br>
            CEO – Musiconn Pty Ltd</p>
        </td>
    </tr>
</table>
