<?php
use Yii;
use yii\helpers\Html;
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['student_name']; ?>,</p>

            <p>Your request to cancel your monthly subscription to Musiconn online contents will be processed once it's been reviewed by the Musiconn Admin team.</p>

            <p>If you wish to re-subscribe to our video library in future, simply log into our <a target="_blank" href="<?= Yii::$app->params['base_url']; ?>">booking system</a>, and from your profile page, select "MANAGE SUBSCRIPTION" and proceed to payment.</p>
            
            <p> Please do not reply to this unattended mailbox.  Any queries or feedback can be directed to admin@musiconn.com.au.</p>

            <p>Regards,</p>
            <p>Musiconn Booking System</p>
        </td>
    </tr>
</table>

