<?php

namespace app\libraries;

use Yii;
use app\models\Admin;
use app\models\Timezone;
use app\models\BookedSession;
use app\libraries\Browser;

class General {

    public function __construct() {
        date_default_timezone_set('Australia/Sydney');
    }

    public static function encrypt_str($in_string) {
        $myPattern = array("0" => "012345",
            "1" => "6789bQ",
            "2" => "WERTYU",
            "3" => "IOPASD",
            "4" => "FGHJKL",
            "5" => "ZXCVBN",
            "6" => "Mqwert",
            "7" => "yuiopa",
            "8" => "sdfghj",
            "9" => "klzxcv");
        //Create a numeric string
        $num_str = "";
        for ($i = 0; $i < strlen($in_string); $i++) {
            $num_str .= substr("000" . ord(substr($in_string, $i, 1)), -3);
        }
        //Convert each number to letter
        $ltr_str = "";
        for ($i = 0; $i < strlen($num_str); $i++) {
            $ltr_str .= substr($myPattern[substr($num_str, $i, 1)], rand(0, 5), 1);
        }
        //Return encrypted string
        return $ltr_str;
    }

    public static function decrypt_str($in_string) {

        $myPattern = array("0" => "0", "1" => "0", "2" => "0", "3" => "0", "4" => "0", "5" => "0",
            "6" => "1", "7" => "1", "8" => "1", "9" => "1", "b" => "1", "Q" => "1",
            "W" => "2", "E" => "2", "R" => "2", "T" => "2", "Y" => "2", "U" => "2",
            "I" => "3", "O" => "3", "P" => "3", "A" => "3", "S" => "3", "D" => "3",
            "F" => "4", "G" => "4", "H" => "4", "J" => "4", "K" => "4", "L" => "4",
            "Z" => "5", "X" => "5", "C" => "5", "V" => "5", "B" => "5", "N" => "5",
            "M" => "6", "q" => "6", "w" => "6", "e" => "6", "r" => "6", "t" => "6",
            "y" => "7", "u" => "7", "i" => "7", "o" => "7", "p" => "7", "a" => "7",
            "s" => "8", "d" => "8", "f" => "8", "g" => "8", "h" => "8", "j" => "8",
            "k" => "9", "l" => "9", "z" => "9", "x" => "9", "c" => "9", "v" => "9");
        //Create a numeric string
        $num_str = "";
        for ($i = 0; $i < strlen($in_string); $i++) {
            $ch = substr($in_string, $i, 1);
            if(isset($myPattern[$ch])){
                $num_str .= $myPattern[substr($in_string, $i, 1)];
            }
        }
        //Convert each 3 digits to letter
        $ltr_str = "";
        for ($i = 0; $i < strlen($num_str); $i = $i + 3) {
            $ltr_str .= chr(substr($num_str, $i, 3));
        }
        //Return encrypted string
        return $ltr_str;
    }

    public static function generateEnrollmentID() {
        $model = \app\models\Student::find()->select('enrollment_id')
                ->orderBy('enrollment_id DESC')
                ->one();
        if (isset($model->enrollment_id) && !empty($model->enrollment_id)) {
            $oldEnrollmentID = ltrim($model->enrollment_id, "C");
            $EnrollmentID = $oldEnrollmentID + 1;
            $newEnrollmentID = 'C' . str_pad($EnrollmentID, 4, '0', STR_PAD_LEFT);
        } else {
            $newEnrollmentID = "C0001";
        }
        return $newEnrollmentID;
    }

    public static function generateBookingID() {
        $model = \app\models\StudentTuitionBooking::find()->select('booking_id')
                ->orderBy('booking_id DESC')
                ->one();
        if (isset($model->booking_id) && !empty($model->booking_id)) {
            $oldEnrollmentID = ltrim($model->booking_id, "B");
            $EnrollmentID = $oldEnrollmentID + 1;
            $newEnrollmentID = 'B' . str_pad($EnrollmentID, 4, '0', STR_PAD_LEFT);
        } else {
            $newEnrollmentID = "B0001";
        }
        return $newEnrollmentID;
    }

    public static function generateOrderID() {
        $model = \app\models\Order::find()->select('order_id')
                ->orderBy('order_id DESC')
                ->one();
        if (isset($model->order_id) && !empty($model->order_id)) {
            $oldEnrollmentID = ltrim($model->order_id, "ORD");
            $EnrollmentID = $oldEnrollmentID + 1;
            $newEnrollmentID = 'ORD' . str_pad($EnrollmentID, 4, '0', STR_PAD_LEFT);
        } else {
            $newEnrollmentID = "ORD0001";
        }
        return $newEnrollmentID;
    }

    public static function generateTransactionID() {
        $model = \app\models\PaymentTransaction::find()->select('transaction_number')
                ->orderBy('transaction_number DESC')
                ->one();
        if (isset($model->transaction_number) && !empty($model->transaction_number)) {
            $oldEnrollmentID = ltrim($model->transaction_number, "TRN");
            $EnrollmentID = $oldEnrollmentID + 1;
            $newEnrollmentID = 'TRN' . str_pad($EnrollmentID, 4, '0', STR_PAD_LEFT);
        } else {
            $newEnrollmentID = "TRN0001";
        }
        return $newEnrollmentID;
    }

    public static function generateRescheduleID() {
        $model = \app\models\RescheduleSession::find()->select('referance_number')
                ->orderBy('referance_number DESC')
                ->one();
        if (isset($model->referance_number) && !empty($model->referance_number)) {
            $oldEnrollmentID = ltrim($model->referance_number, "R");
            $EnrollmentID = $oldEnrollmentID + 1;
            $newEnrollmentID = 'R' . str_pad($EnrollmentID, 4, '0', STR_PAD_LEFT);
        } else {
            $newEnrollmentID = "R0001";
        }
        return $newEnrollmentID;
    }

    public static function generateStrongPassword($length = 9, $add_dashes = false, $available_sets = 'luds') {

        $sets = array();
        if (strpos($available_sets, 'l') !== false)
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        if (strpos($available_sets, 'u') !== false)
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        if (strpos($available_sets, 'd') !== false)
            $sets[] = '23456789';
        if (strpos($available_sets, 's') !== false)
            $sets[] = '!@#$%&*?';
        $all = '';
        $password = '';
        foreach ($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        $all = str_split($all);
        for ($i = 0; $i < $length - count($sets); $i++)
            $password .= $all[array_rand($all)];
        $password = str_shuffle($password);
        if (!$add_dashes)
            return $password;
        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while (strlen($password) > $dash_len) {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;
        return $dash_str;
    }

    public static function GeneratePassword() {
        /* $password = "";

          $length = 6;
          $chars = array_merge(range(0,9), range('a','z'), range('A','Z'),array('@','$','%','&','*','+'));

          shuffle($chars);
          $password = implode(array_slice($chars, 0, $length));

          return $password; */
        return self::generateStrongPassword(6, false, 'luds');
    }

    public static function CreateDir($path = '') {
        $return = true;
        try {
            if ($path != '') {
                if (!is_dir($path)) {
                    $return = mkdir($path, 0755, true);
                }
            }
        } catch (\Exception $exception) {
            $return = false;
            $exception->getMessage();
        }
        return $return;
    }

    public static function ccMasking($number, $maskingCharacter = 'X') {
        if ($number != '') {
            return substr($number, 0, 4) . str_repeat($maskingCharacter, strlen($number) - 8) . substr($number, -4);
        } else {
            return '';
        }
    }

    public static function displayDate($date = '') {
        if ($date != '') {
            return self::convertSystemToUserTimezone($date, 'd-m-Y');
        } else {
            return '';
        }
    }

    public static function displayTime($date = '') {
        if ($date != '') {
            return self::convertSystemToUserTimezone($date, 'h:i A');
        } else {
            return '';
        }
    }

    public static function displayTimeFormat($date = '', $formate = 'h:i A') {
        if ($date != '') {
            return date($formate, strtotime($date));
        } else {
            return '';
        }
    }

    public static function displayDateTime($date = '', $istimestamp = false, $withTimezone = false) {
        $format = 'd-m-Y h:i A';
        if ($date != '') {
            $timestamp = ($istimestamp) ?: strtotime($date);
            if ($withTimezone) {
                $format = 'd-m-Y h:i A T';
            }
            return self::convertSystemToUserTimezone($date, $format);
        } else {
            return '';
        }
    }

    public static function displayDateTimeCalender($date = '', $istimestamp = false, $withTimezone = false) {
        $format = 'Y-m-d h:i A';
        if ($date != '') {
            $timestamp = ($istimestamp) ?: strtotime($date);
            if ($withTimezone) {
                $format = 'Y-m-d h:i A T';
            }
            return self::convertSystemToUserTimezone($date, $format);
        } else {
            return '';
        }
    }

    public static function convertSystemToUserTimezone($date, $format = 'd-m-Y h:i A') {
        $user_timezone = (!Yii::$app->getUser()->isGuest) ? Yii::$app->user->identity->timezone : Yii::$app->params['timezone'];
        $date = new \DateTime($date, new \DateTimeZone(Yii::$app->params['timezone']));
        $date->setTimezone(new \DateTimeZone($user_timezone));
        $date = $date->format($format);
        return $date;
    }

    public static function convertUserToSystemTimezone($date, $format = 'Y-m-d H:i') {
        $user_timezone = (!Yii::$app->getUser()->isGuest) ? Yii::$app->user->identity->timezone : Yii::$app->params['timezone'];
        $date = new \DateTime($date, new \DateTimeZone($user_timezone));
        $date->setTimezone(new \DateTimeZone(Yii::$app->params['timezone']));
        $date = $date->format($format);
        return $date;
    }

    public static function convertTimezone($date, $from, $to, $format = 'Y-m-d H:i:s') {
        $date = new \DateTime($date, new \DateTimeZone($from));
        $date->setTimezone(new \DateTimeZone($to));
        $date = $date->format($format);
        return $date;
    }

    public static function convertTime($time, $from, $to, $format = 'h:i A') {
        $date = new \DateTime($time, new \DateTimeZone($from));
        $date->setTimezone(new \DateTimeZone($to));
        $time = $date->format($format);
        return $time;
    }

    public static function getOwnersEmails($role = ['OWNER']) {

        $return = [];
        if (($model = Admin::find()->joinWith(['user'])->where(['in', 'user.role', $role])->andWhere(['admin.status' => 'ENABLED'])->asArray()->all() ) !== null) {
            $return = array_column($model, 'email');
        }
        return $return;
    }

    public static function getAllTimeZones() {

        $return = [];
        if (($model = Timezone::find()->where(['status' => 'ACTIVE'])->asArray()->all()) !== null) {
            $return = array_column($model, 'name', 'name');
        }
        return $return;
    }

    public static function getLessonVideoListUsingUuid($uuid) {
        try {
            $result = [];
            $bsmodel = BookedSession::find()->where(['uuid' => $uuid])->one();
            if (!empty($bsmodel)) {
                $path = Yii::$app->params['media']['session_recorded_video']['path'] . $bsmodel->uuid . '/';
                if (is_dir($path)) {
                    @chmod($path, 0775);
                    $return = @scandir($path);
                    if ($return) {
                        foreach ($return as $key => $value) {
                            if (!in_array($value, array(".", ".."))) {
                                if (is_dir($path . $value)) {
                                    $result[$value] = dirToArray($path . $value);
                                } else {
                                    $result[] = $value;
                                }
                            }
                        }
                    }
                }
            }
            array_unshift($result, "0");
            unset($result[0]);
            return $result;
        } catch (\Exception $exception) {
            throw new \yii\base\Exception($exception->getMessage());
        }
    }

    public static function lessonVideoThumbnailGenerate($filename = '', $conference_uuid = '') {

        $video_file_path = Yii::$app->params['media']['session_recorded_video']['path'] . $conference_uuid . '/' . $filename;
        if (!is_file($video_file_path)) {
            return false;
        }
        // Location where video thumbnail to store
        $ffmpeg_installation_path = Yii::$app->params['ffmpeg_installation_path'];
        $thumbnail_path = Yii::$app->params['media']['session_recorded_video']['thumbnail']['path'];
        $second = 1;
        $thumbSize = '550x550';


        $thumbnail_path_full = $thumbnail_path . $conference_uuid . '.jpg';

        // FFmpeg Command to generate video thumbnail

        $cmd = "{$ffmpeg_installation_path} -i {$video_file_path} -deinterlace -an -ss {$second} -t 00:00:01  -s {$thumbSize} -r 1 -y -vcodec mjpeg -f mjpeg {$thumbnail_path_full} 2>&1";

        exec($cmd, $output, $retval);

        return (!$retval) ? true : false;

        /* echo "<pre>";
          print_r($cmd);
          print_r($output);
          print_r($retval);exit;

          if ($retval)
          {
          echo 'error in generating video thumbnail';
          }
          else
          {
          echo 'Thumbnail generated successfully';
          echo $thumb_path = $thumbnail_path . $videoname . '.jpg';
          } */
    }

    public static function getVideoTitle($title = '') {

        $postfix = '...';
        $return = $postfix;
        if (!empty($title)) {
            $return = (strlen($title) > 18 ) ? substr($title, 0, 18) . $postfix : $title;
        }
        return $return;
    }

    public static function getSmallVideoTitle($title = '') {

        $postfix = '...';
        $return = $postfix;
        if (!empty($title)) {
            $return = (strlen($title) > 17 ) ? substr($title, 0, 17) . $postfix : $title;
        }
        return $return;
    }

    public static function getBrowser() {
        
        $u_agent = $_SERVER['HTTP_USER_AGENT'];
        $bname = 'Unknown';
        $platform = 'Unknown';
        $version = "";

        //First get the platform?
        if (preg_match('/linux/i', $u_agent)) {
            $platform = 'linux';
        } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
            $platform = 'mac';
        } elseif (preg_match('/windows|win32/i', $u_agent)) {
            $platform = 'windows';
        }
	$ub='';
        // Next get the name of the useragent yes seperately and for good reason
        if (preg_match('/MSIE/i', $u_agent) && !preg_match('/Opera/i', $u_agent)) {
            $bname = 'Internet Explorer';
            $ub = "MSIE";
        } elseif (preg_match('/Firefox/i', $u_agent)) {
            $bname = 'Mozilla Firefox';
            $ub = "Firefox";
        } elseif (preg_match('/OPR/i', $u_agent)) {
            $bname = 'Opera';
            $ub = "Opera";
        } elseif (preg_match('/Chrome/i', $u_agent) && !preg_match('/Edge/i', $u_agent)) {
            $bname = 'Google Chrome';
            $ub = "Chrome";
        }elseif (preg_match('/CriOS/i', $u_agent) && !preg_match('/Edge/i', $u_agent)) {
            $bname = 'Google Chrome';
            $ub = "CriOS";
        }  elseif (preg_match('/Safari/i', $u_agent) && !preg_match('/Edge/i', $u_agent)) {
            $bname = 'Apple Safari';
            $ub = "Safari";
        } elseif (preg_match('/Netscape/i', $u_agent)) {
            $bname = 'Netscape';
            $ub = "Netscape";
        } elseif (preg_match('/Edge/i', $u_agent)) {
            $bname = 'Edge';
            $ub = "Edge";
        } elseif (preg_match('/Trident/i', $u_agent)) {
            $bname = 'Internet Explorer';
            $ub = "MSIE";
        } 

        // finally get the correct version number
        $known = array('Version', $ub, 'other');
        $pattern = '#(?<browser>' . join('|', $known) .
                ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        if (!preg_match_all($pattern, $u_agent, $matches)) {
            // we have no matching number just continue
        }
        
        //echo $ub;exit;
        // see how many we have
        $i = count($matches['browser']);
        if ($i != 1) {
            //we will have two since we are not using 'other' argument yet
            //see if version is before or after the name
            if (strripos($u_agent, "Version") < strripos($u_agent, $ub)) {
                $version = $matches['version'][0];
            } else {
                $version = $matches['version'][1];
            }
        } else {
            $version = $matches['version'][0];
        }

        // check if we have a number
        if ($version == null || $version == "") {
            $version = "?";
        }

//        $browser = new Browser();
//        $u_agent = $_SERVER['HTTP_USER_AGENT'];
//        $bname = $browser->getBrowser();
//        $version = $browser->getVersion();
//        $platform = $browser->getPlatform();
//        $pattern = '';
        return array(
            'userAgent' => $u_agent,
            'name' => $bname,
            'version' => $version,
            'platform' => $platform,
            'pattern' => $pattern
        );
    }

    /*function CheckDevice() {
        $tablet_browser = 0;
        $mobile_browser = 0;
        $ipad_browser = 0;
        $device = '';

        //echo "<pre>";
        //print_r($_SERVER['HTTP_USER_AGENT']);
        
        if (preg_match('/(tablet)|(android)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
            $tablet_browser++;
        }
         //if (preg_match('/(ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
        if (preg_match('/(ipad|playbook)|(mobi|opera mini)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
   
        $ipad_browser++;
        }

        if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
            $mobile_browser++;
        }

        if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']), 'application/vnd.wap.xhtml+xml') > 0) or ( (isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
            $mobile_browser++;
        }
       
        $mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
        $mobile_agents = array(
            'w3c ', 'acs-', 'alav', 'alca', 'amoi', 'audi', 'avan', 'benq', 'bird', 'blac',
            'blaz', 'brew', 'cell', 'cldc', 'cmd-', 'dang', 'doco', 'eric', 'hipt', 'inno',
            'ipaq', 'java', 'jigs', 'kddi', 'keji', 'leno', 'lg-c', 'lg-d', 'lg-g', 'lge-',
            'maui', 'maxo', 'midp', 'mits', 'mmef', 'mobi', 'mot-', 'moto', 'mwbp', 'nec-',
            'newt', 'noki', 'palm', 'pana', 'pant', 'phil', 'play', 'port', 'prox',
            'qwap', 'sage', 'sams', 'sany', 'sch-', 'sec-', 'send', 'seri', 'sgh-', 'shar',
            'sie-', 'siem', 'smal', 'smar', 'sony', 'sph-', 'symb', 't-mo', 'teli', 'tim-',
            'tosh', 'tsm-', 'upg1', 'upsi', 'vk-v', 'voda', 'wap-', 'wapa', 'wapi', 'wapp',
            'wapr', 'webc', 'winw', 'winw', 'xda ', 'xda-');

        if (in_array($mobile_ua, $mobile_agents)) {
            $mobile_browser++;
        }

        if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'opera mini') > 0) {
            $mobile_browser++;
            //Check for tablets on opera mini alternative headers
            $stock_ua = strtolower(isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA']) ? $_SERVER['HTTP_X_OPERAMINI_PHONE_UA'] : (isset($_SERVER['HTTP_DEVICE_STOCK_UA']) ? $_SERVER['HTTP_DEVICE_STOCK_UA'] : ''));
            if (preg_match('/(tablet)|(android(?!.*mobile))/i', $stock_ua)) {
                $tablet_browser++;
            }
            if (preg_match('/(ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
                $ipad_browser++;
            }
        }

        if ($ipad_browser > 0) {
            
            // do something for tablet devices
            //print 'is tablet';
            $device = 'Ipad';
            $ua = General::getBrowser();
            $browser = $ua['name'];
            $browser_version = $ua['version'];
            $webrtc_supported_version = '12.1';
            if ($browser != 'Apple Safari') {

                return array(
                    'success' => 'false',
                    'browser' => $browser,
                    'browser_version' => $browser_version,
                    'device' => $device,
                    'msg' => 'This product was built for use in Safari only for Ipad.'
                );
                
            } elseif ($browser == 'Apple Safari' && $browser_version < $webrtc_supported_version) {

                return array(
                    'success' => 'false',
                    'browser' => $browser,
                    'browser_version' => $browser_version,
                    'device' => $device,
                    'msg' => 'Please update your safari.'
                );
                
            }

            return array(
                'success' => 'true',
                'browser' => $browser,
                'browser_version' => $browser_version,
                'device' => $device,
                'msg' => 'System capabilities checked successfully.'
            );
            
        } elseif ($tablet_browser > 0) {
            
            // do something for tablet devices
            //print 'is tablet';
            $device = 'Tablet';
            $ua = General::getBrowser();
            $browser = $ua['name'];
            $browser_version = $ua['version'];
            $webrtc_supported_version = '28';

            if ($browser != 'Google Chrome') {
                
                return array(
                    'success' => 'false',
                    'browser' => $browser,
                    'browser_version' => $browser_version,
                    'device' => $device,
                    'msg' => 'This product was built for use in Chrome browser only.'
                );
                
            } elseif ($browser == 'Google Chrome' && $browser_version <= $webrtc_supported_version) {

                return array(
                    'success' => 'false',
                     'browser' => $browser,
                     'browser_version' => $browser_version,
                    'device' => $device,
                    'msg' => 'Please update your google chrome.'
                );
            }

            return array(
                'success' => 'true',
                'browser' => $browser,
                'browser_version' => $browser_version,
                'device' => $device,
                'msg' => 'System capabilities checked successfully.'
            );
            
        } else if ($mobile_browser > 0) {

            $device = 'Mobile';
            $ua = General::getBrowser();
            $browser = $ua['name'];
            $browser_version = $ua['version'];
            // do something for mobile devices
            return array(
                'success' => 'false',
                'browser' => $browser,
                'browser_version' => $browser_version,
                'device' => $device,
                'msg' => 'Mobile device is not supported.'
            );
            
        } else {
            // do something for everything else
            //print 'is desktop';
            $device = 'Desktop';
            $ua = General::getBrowser();
            $browser = $ua['name'];
            $browser_version = $ua['version'];
            $webrtc_supported_version = '28';

            if ($browser != 'Google Chrome') {
                
                return array(
                    'success' => 'false',
                     'browser' => $browser,
                     'browser_version' => $browser_version,
                    'device' => $device,
                    'msg' => 'This product was built for use in Chrome browser only.'
                );
                
            } elseif ($browser == 'Google Chrome' && $browser_version <= $webrtc_supported_version) {

                return array(
                    'success' => 'false',
                     'browser' => $browser,
                     'browser_version' => $browser_version,
                    'device' => $device,
                    'msg' => 'Please update your google chrome.'
                );
            }

            return array(
                'success' => 'true',
                'browser' => $browser,
                'browser_version' => $browser_version,
                'device' => $device,
                'msg' => 'System capabilities checked successfully.'
            );
        }
    }*/
    
 public static function CheckDevice() {

        $tablet_browser = 0;
        $mobile_browser = 0;
        $ipad_browser = 0;
        $device = '';
        $MY_SERVER = $_SERVER;
        $GET_BROWSER = General::getBrowser();
        $GET_OS = General::getOS();
        $IS_MOBILE = General::isMobile();

        //for debuging start
        /* $json_array = \app\controllers\TestController::server_agent();
          $MY_SERVER = $json_array['server_data'];
          $GET_BROWSER = $json_array['get_browser'];
         */
        //for debuging end

        $return = array(
            'success' => '',
            'browser' => '',
            'browser_version' => '',
            'device' => '',
            'msg' => 'Sorry! No data found.'
        );

        //echo "<pre>";
        //print_r($MY_SERVER['HTTP_USER_AGENT']);

        if (preg_match('/(tablet)|(android)/i', strtolower($MY_SERVER['HTTP_USER_AGENT']))) {
            $tablet_browser++;
        }
        //if (preg_match('/(ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($MY_SERVER['HTTP_USER_AGENT']))) {
        if (preg_match('/(ipad|playbook)|(mobi|opera mini)/i', strtolower($MY_SERVER['HTTP_USER_AGENT']))) {

            $ipad_browser++;
        }

        if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($MY_SERVER['HTTP_USER_AGENT']))) {
            $mobile_browser++;
        }

        if ((strpos(strtolower($MY_SERVER['HTTP_ACCEPT']), 'application/vnd.wap.xhtml+xml') > 0) or ( (isset($MY_SERVER['HTTP_X_WAP_PROFILE']) or isset($MY_SERVER['HTTP_PROFILE'])))) {
            $mobile_browser++;
        }

        $mobile_ua = strtolower(substr($MY_SERVER['HTTP_USER_AGENT'], 0, 4));
        $mobile_agents = array(
            'w3c ', 'acs-', 'alav', 'alca', 'amoi', 'audi', 'avan', 'benq', 'bird', 'blac',
            'blaz', 'brew', 'cell', 'cldc', 'cmd-', 'dang', 'doco', 'eric', 'hipt', 'inno',
            'ipaq', 'java', 'jigs', 'kddi', 'keji', 'leno', 'lg-c', 'lg-d', 'lg-g', 'lge-',
            'maui', 'maxo', 'midp', 'mits', 'mmef', 'mobi', 'mot-', 'moto', 'mwbp', 'nec-',
            'newt', 'noki', 'palm', 'pana', 'pant', 'phil', 'play', 'port', 'prox',
            'qwap', 'sage', 'sams', 'sany', 'sch-', 'sec-', 'send', 'seri', 'sgh-', 'shar',
            'sie-', 'siem', 'smal', 'smar', 'sony', 'sph-', 'symb', 't-mo', 'teli', 'tim-',
            'tosh', 'tsm-', 'upg1', 'upsi', 'vk-v', 'voda', 'wap-', 'wapa', 'wapi', 'wapp',
            'wapr', 'webc', 'winw', 'winw', 'xda ', 'xda-');

        if (in_array($mobile_ua, $mobile_agents)) {
            $mobile_browser++;
        }

        if (strpos(strtolower($MY_SERVER['HTTP_USER_AGENT']), 'opera mini') > 0) {
            $mobile_browser++;
            //Check for tablets on opera mini alternative headers
            $stock_ua = strtolower(isset($MY_SERVER['HTTP_X_OPERAMINI_PHONE_UA']) ? $MY_SERVER['HTTP_X_OPERAMINI_PHONE_UA'] : (isset($MY_SERVER['HTTP_DEVICE_STOCK_UA']) ? $MY_SERVER['HTTP_DEVICE_STOCK_UA'] : ''));
            if (preg_match('/(tablet)|(android(?!.*mobile))/i', $stock_ua)) {
                $tablet_browser++;
            }
            if (preg_match('/(ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
                $ipad_browser++;
            }
        }

        if ($ipad_browser > 0  && in_array($GET_OS, array('iPad','iPhone'))) {

            // do something for tablet devices
            //print 'is tablet';
            $device = 'Ipad';
            $ua = $GET_BROWSER;
            $browser = $ua['name'];
            $browser_version = $ua['version'];
            $webrtc_supported_version = '12.1';
            if ($GET_OS == 'iPhone') {
                
                $device = 'iPhone';
                $return = array(
                    'success' => 'false',
                    'browser' => $browser,
                    'browser_version' => $browser_version,
                    'device' => $device,
                    'msg' => 'Settings in your mobile device is not compatible for lessons.'
                );
            }elseif ($browser != 'Apple Safari') {

                $return = array(
                    'success' => 'false',
                    'browser' => $browser,
                    'browser_version' => $browser_version,
                    'device' => $device,
                    'msg' => 'This product was built for use in the Safari browser.  Please log into Musiconn using Safari.'
                );
            } elseif ($browser == 'Apple Safari' && $browser_version < $webrtc_supported_version) {

                $return = array(
                    'success' => 'false',
                    'browser' => $browser,
                    'browser_version' => $browser_version,
                    'device' => $device,
                    'msg' => 'Please update your Safari browser for the lessons to run smoothly.'
                );
            } else {

                $return = array(
                    'success' => 'true',
                    'browser' => $browser,
                    'browser_version' => $browser_version,
                    'device' => $device,
                    'msg' => "Systems compatibility testing completed successfully. You are good to go!"
                );
            }
        } elseif ($tablet_browser > 0 && $IS_MOBILE == false) {

            // do something for tablet devices
            //print 'is tablet';
            $device = 'Tablet';
            $ua = $GET_BROWSER;
            $browser = $ua['name'];
            $browser_version = $ua['version'];
            $webrtc_supported_version = '28';

            if ($browser != 'Google Chrome') {

                $return = array(
                    'success' => 'false',
                    'browser' => $browser,
                    'browser_version' => $browser_version,
                    'device' => $device,
//                    'msg' => 'This product was built for use in Chrome browser only.'
                    'msg' => 'This product was built for use in <font style="color:red;"><b>Chrome browser</b></font> for your device.  Please log in again using Chrome.  If you don’t have Chrome installed, follow this <a target="_blank" href="https://www.google.com.au/chrome/?brand=CHBD&gclid=EAIaIQobChMI2caQ7Prw5QIV2AorCh2zNgGgEAAYASABEgIhOPD_BwE&gclsrc=aw.ds" style="text-decoration: underline;">link</a> to download.'
                );
            } elseif ($browser == 'Google Chrome' && $browser_version <= $webrtc_supported_version) {

                $return = array(
                    'success' => 'false',
                    'browser' => $browser,
                    'browser_version' => $browser_version,
                    'device' => $device,
                    'msg' => 'Please update your Google Chrome browser for the lessons to run smoothly.'
                );
            } else {

                $return = array(
                    'success' => 'true',
                    'browser' => $browser,
                    'browser_version' => $browser_version,
                    'device' => $device,
                    'msg' => "Systems compatibility testing completed successfully. You are good to go!"
                );
            }
        } else if ($mobile_browser > 0 && $IS_MOBILE == true) {

            $device = 'Mobile';
            $ua = $GET_BROWSER;
            $browser = $ua['name'];
            $browser_version = $ua['version'];
            
            /*if ($GET_OS == 'Android' && $browser == 'Google Chrome' ) {
                if($browser_version >= '50'){
                    $return = array(
                        'success' => 'true',
                        'browser' => $browser,
                        'browser_version' => $browser_version,
                        'device' => $device,
                        'msg' => "Systems compatibility testing completed successfully. You are good to go!"
                    );
                }else{
                    $return = array(
                        'success' => 'true',
                        'browser' => $browser,
                        'browser_version' => $browser_version,
                        'device' => $device,
                        'msg' => "Please install latest version of Chrome."
                    );
                }
            } else {
                // do something for mobile devices
                $return = array(
                    'success' => 'false',
                    'browser' => $browser,
                    'browser_version' => $browser_version,
                    'device' => $device,
                    'msg' => 'Settings in your mobile device is not compatible for lessons.'
                );
            }*/
            $return = array(
                'success' => 'false',
                'browser' => $browser,
                'browser_version' => $browser_version,
                'device' => $device,
                'msg' => 'Settings in your mobile device is not compatible for lessons.'
            );
        } else {
            // do something for everything else
            //print 'is desktop';
            $device = 'Desktop';
            $ua = $GET_BROWSER;
            $browser = $ua['name'];
            $browser_version = $ua['version'];
            $webrtc_supported_version = '28';

            $mac_browser_version = $ua['version'];
            $mac_webrtc_supported_version = '12.1';
            
            
            if ($ua['platform'] == 'mac') {
                //condition for MAC system - start
                if ($browser == 'Apple Safari') {
                    if($mac_browser_version < $mac_webrtc_supported_version){
                        
                        $return = array(
                            'success' => 'false',
                            'browser' => $browser,
                            'browser_version' => $browser_version,
                            'device' => $device,
                            'msg' => 'Please update your Safari browser for the lessons to run smoothly.'
                        );
                    } else{
                        $return = array(
                            'success' => 'true',
                            'browser' => $browser,
                            'browser_version' => $browser_version,
                            'device' => $device,
                            'msg' => "Systems compatibility testing completed successfully. You are good to go!"
                        );
                    }
                } else{
                    $return = array(
                        'success' => 'false',
                        'browser' => $browser,
                        'browser_version' => $browser_version,
                        'device' => $device,
                        'msg' => 'This product was built for use in the Safari browser.  Please log into Musiconn using Safari.'
                    );
                }
                //condition for MAC system - end
            } elseif ($browser != 'Google Chrome') {

                $return = array(
                    'success' => 'false',
                    'browser' => $browser,
                    'browser_version' => $browser_version,
                    'device' => $device,
                    //'msg' => 'This product was built for use in Chrome browser only.'
                    'msg' => 'This product was built for use in <font style="color:red;"><b>Chrome browser</b></font> for your device.  Please log in again using Chrome.  If you don’t have Chrome installed, follow this <a target="_blank" href="https://www.google.com.au/chrome/?brand=CHBD&gclid=EAIaIQobChMI2caQ7Prw5QIV2AorCh2zNgGgEAAYASABEgIhOPD_BwE&gclsrc=aw.ds" style="text-decoration: underline;">link</a> to download.'
                );
            } elseif ($browser == 'Google Chrome' && $browser_version <= $webrtc_supported_version) {

                $return = array(
                    'success' => 'false',
                    'browser' => $browser,
                    'browser_version' => $browser_version,
                    'device' => $device,
                    'msg' => 'Please update your Google Chrome browser for the lessons to run smoothly.'
                );
            } else {

                $return = array(
                    'success' => 'true',
                    'browser' => $browser,
                    'browser_version' => $browser_version,
                    'device' => $device,
                    'msg' => "Systems compatibility testing completed successfully. You are good to go!"
                );
            }
        }
        
        return $return;
    }
    
    public static function JitsiCheckDevice() {

        $tablet_browser = 0;
        $mobile_browser = 0;
        $ipad_browser = 0;
        $device = '';
        $MY_SERVER = $_SERVER;
        $GET_BROWSER = General::getBrowser();
        $GET_OS = General::getOS();
        $IS_MOBILE = General::isMobile();
        $allow_browsers = ['Mozilla Firefox', 'Google Chrome'];
        $min_allow_chrome = '60';
        $min_allow_firefox = '71';

        //for debuging start
        /* $json_array = \app\controllers\TestController::server_agent();
          $MY_SERVER = $json_array['server_data'];
          $GET_BROWSER = $json_array['get_browser'];
         */
        //for debuging end

        $return = array(
            'success' => '',
            'browser' => '',
            'browser_version' => '',
            'device' => '',
            'msg' => 'Sorry! No data found.'
        );

        //echo "<pre>";
        //print_r($MY_SERVER['HTTP_USER_AGENT']);

        if (preg_match('/(tablet)|(android)/i', strtolower($MY_SERVER['HTTP_USER_AGENT']))) {
            $tablet_browser++;
        }
        //if (preg_match('/(ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($MY_SERVER['HTTP_USER_AGENT']))) {
        if (preg_match('/(ipad|playbook)|(mobi|opera mini)/i', strtolower($MY_SERVER['HTTP_USER_AGENT']))) {

            $ipad_browser++;
        }

        if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($MY_SERVER['HTTP_USER_AGENT']))) {
            $mobile_browser++;
        }

        if ((isset( $_SERVER['HTTP_ACCEPT'] ) && strpos(strtolower($MY_SERVER['HTTP_ACCEPT']), 'application/vnd.wap.xhtml+xml') > 0) or ( (isset($MY_SERVER['HTTP_X_WAP_PROFILE']) or isset($MY_SERVER['HTTP_PROFILE'])))) {
            $mobile_browser++;
        }

        $mobile_ua = strtolower(substr($MY_SERVER['HTTP_USER_AGENT'], 0, 4));
        $mobile_agents = array(
            'w3c ', 'acs-', 'alav', 'alca', 'amoi', 'audi', 'avan', 'benq', 'bird', 'blac',
            'blaz', 'brew', 'cell', 'cldc', 'cmd-', 'dang', 'doco', 'eric', 'hipt', 'inno',
            'ipaq', 'java', 'jigs', 'kddi', 'keji', 'leno', 'lg-c', 'lg-d', 'lg-g', 'lge-',
            'maui', 'maxo', 'midp', 'mits', 'mmef', 'mobi', 'mot-', 'moto', 'mwbp', 'nec-',
            'newt', 'noki', 'palm', 'pana', 'pant', 'phil', 'play', 'port', 'prox',
            'qwap', 'sage', 'sams', 'sany', 'sch-', 'sec-', 'send', 'seri', 'sgh-', 'shar',
            'sie-', 'siem', 'smal', 'smar', 'sony', 'sph-', 'symb', 't-mo', 'teli', 'tim-',
            'tosh', 'tsm-', 'upg1', 'upsi', 'vk-v', 'voda', 'wap-', 'wapa', 'wapi', 'wapp',
            'wapr', 'webc', 'winw', 'winw', 'xda ', 'xda-');

        if (in_array($mobile_ua, $mobile_agents)) {
            $mobile_browser++;
        }

        if (strpos(strtolower($MY_SERVER['HTTP_USER_AGENT']), 'opera mini') > 0) {
            $mobile_browser++;
            //Check for tablets on opera mini alternative headers
            $stock_ua = strtolower(isset($MY_SERVER['HTTP_X_OPERAMINI_PHONE_UA']) ? $MY_SERVER['HTTP_X_OPERAMINI_PHONE_UA'] : (isset($MY_SERVER['HTTP_DEVICE_STOCK_UA']) ? $MY_SERVER['HTTP_DEVICE_STOCK_UA'] : ''));
            if (preg_match('/(tablet)|(android(?!.*mobile))/i', $stock_ua)) {
                $tablet_browser++;
            }
            if (preg_match('/(ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
                $ipad_browser++;
            }
        }

        if ($ipad_browser > 0  && in_array($GET_OS, array('iPad','iPhone'))) {

            // do something for tablet devices
            //print 'is tablet';
            $device = 'Ipad';
            $ua = $GET_BROWSER;
            $browser = $ua['name'];
            $browser_version = $ua['version'];
            $webrtc_supported_version = '12.1';
            
            if (in_array($browser, $allow_browsers)) {
                
                if ($browser == 'Google Chrome' && $browser_version < $min_allow_chrome) {

                    $return = array(
                        'success' => 'false',
                        'browser' => $browser,
                        'browser_version' => $browser_version,
                        'device' => $device,
                        'msg' => 'Please update your Google Chrome browser for the lessons to run smoothly.'
                    );
                }elseif ($browser == 'Mozilla Firefox' && $browser_version < $min_allow_firefox) {

                    $return = array(
                        'success' => 'false',
                        'browser' => $browser,
                        'browser_version' => $browser_version,
                        'device' => $device,
                        'msg' => 'Please update your Mozilla Firefox browser for the lessons to run smoothly.'
                    );
                }else{
                        $return = array(
                        'success' => 'true',
                        'browser' => $browser,
                        'browser_version' => $browser_version,
                        'device' => $device,
                        'msg' => "Systems compatibility testing completed successfully. You are good to go!"
                    );
                }
            }else{
                $return = array(
                    'success' => 'false',
                    'browser' => $browser,
                    'browser_version' => $browser_version,
                    'device' => $device,
                    //'msg' => 'This product was built for use in Chrome browser only.'
                    'msg' => 'This product was built for use in <font style="color:red;"><b>Chrome browser</b></font> for your device.  Please log in again using Chrome.  If you don’t have Chrome installed, follow this <a target="_blank" href="https://www.google.com.au/chrome/?brand=CHBD&gclid=EAIaIQobChMI2caQ7Prw5QIV2AorCh2zNgGgEAAYASABEgIhOPD_BwE&gclsrc=aw.ds" style="text-decoration: underline;">link</a> to download.'
                );
            }
            
        } elseif ($tablet_browser > 0 && $IS_MOBILE == false) {

            // do something for tablet devices
            //print 'is tablet';
            $device = 'Tablet';
            $ua = $GET_BROWSER;
            $browser = $ua['name'];
            $browser_version = $ua['version'];
            $webrtc_supported_version = '28';

            if (in_array($browser, $allow_browsers)) {
                
                if ($browser == 'Google Chrome' && $browser_version < $min_allow_chrome) {

                    $return = array(
                        'success' => 'false',
                        'browser' => $browser,
                        'browser_version' => $browser_version,
                        'device' => $device,
                        'msg' => 'Please update your Google Chrome browser for the lessons to run smoothly.'
                    );
                }elseif ($browser == 'Mozilla Firefox' && $browser_version < $min_allow_firefox) {

                    $return = array(
                        'success' => 'false',
                        'browser' => $browser,
                        'browser_version' => $browser_version,
                        'device' => $device,
                        'msg' => 'Please update your Mozilla Firefox browser for the lessons to run smoothly.'
                    );
                }else{
                        $return = array(
                        'success' => 'true',
                        'browser' => $browser,
                        'browser_version' => $browser_version,
                        'device' => $device,
                        'msg' => "Systems compatibility testing completed successfully. You are good to go!"
                    );
                }
            }else{
                $return = array(
                    'success' => 'false',
                    'browser' => $browser,
                    'browser_version' => $browser_version,
                    'device' => $device,
                    //'msg' => 'This product was built for use in Chrome browser only.'
                    'msg' => 'This product was built for use in <font style="color:red;"><b>Chrome browser</b></font> for your device.  Please log in again using Chrome.  If you don’t have Chrome installed, follow this <a target="_blank" href="https://www.google.com.au/chrome/?brand=CHBD&gclid=EAIaIQobChMI2caQ7Prw5QIV2AorCh2zNgGgEAAYASABEgIhOPD_BwE&gclsrc=aw.ds" style="text-decoration: underline;">link</a> to download.'
                );
            }
        } else if ($mobile_browser > 0 && $IS_MOBILE == true) {

            $device = 'Mobile';
            $ua = $GET_BROWSER;
            $browser = $ua['name'];
            $browser_version = $ua['version'];
            
            if (in_array($browser, $allow_browsers)) {
                
                if ($browser == 'Google Chrome' && $browser_version < $min_allow_chrome) {

                    $return = array(
                        'success' => 'false',
                        'browser' => $browser,
                        'browser_version' => $browser_version,
                        'device' => $device,
                        'msg' => 'Please update your Google Chrome browser for the lessons to run smoothly.'
                    );
                }elseif ($browser == 'Mozilla Firefox' && $browser_version < $min_allow_firefox) {

                    $return = array(
                        'success' => 'false',
                        'browser' => $browser,
                        'browser_version' => $browser_version,
                        'device' => $device,
                        'msg' => 'Please update your Mozilla Firefox browser for the lessons to run smoothly.'
                    );
                }else{
                        $return = array(
                        'success' => 'true',
                        'browser' => $browser,
                        'browser_version' => $browser_version,
                        'device' => $device,
                        'msg' => "Systems compatibility testing completed successfully. You are good to go!"
                    );
                }
            }else{
                $return = array(
                    'success' => 'false',
                    'browser' => $browser,
                    'browser_version' => $browser_version,
                    'device' => $device,
                    //'msg' => 'This product was built for use in Chrome browser only.'
                    'msg' => 'This product was built for use in <font style="color:red;"><b>Chrome browser</b></font> for your device.  Please log in again using Chrome.  If you don’t have Chrome installed, follow this <a target="_blank" href="https://www.google.com.au/chrome/?brand=CHBD&gclid=EAIaIQobChMI2caQ7Prw5QIV2AorCh2zNgGgEAAYASABEgIhOPD_BwE&gclsrc=aw.ds" style="text-decoration: underline;">link</a> to download.'
                );
            }
            
        } else {
            // do something for everything else
            //print 'is desktop';
            $device = 'Desktop';
            $ua = $GET_BROWSER;
            $browser = $ua['name'];
            $browser_version = $ua['version'];
            $webrtc_supported_version = '28';

            $mac_browser_version = $ua['version'];
            $mac_webrtc_supported_version = '12.1';
            
            if (in_array($browser, $allow_browsers)) {
                
                if ($browser == 'Google Chrome' && $browser_version < $min_allow_chrome) {

                    $return = array(
                        'success' => 'false',
                        'browser' => $browser,
                        'browser_version' => $browser_version,
                        'device' => $device,
                        'msg' => 'Please update your Google Chrome browser for the lessons to run smoothly.'
                    );
                }elseif ($browser == 'Mozilla Firefox' && $browser_version < $min_allow_firefox) {

                    $return = array(
                        'success' => 'false',
                        'browser' => $browser,
                        'browser_version' => $browser_version,
                        'device' => $device,
                        'msg' => 'Please update your Mozilla Firefox browser for the lessons to run smoothly.'
                    );
                }else{
                        $return = array(
                        'success' => 'true',
                        'browser' => $browser,
                        'browser_version' => $browser_version,
                        'device' => $device,
                        'msg' => "Systems compatibility testing completed successfully. You are good to go!"
                    );
                }

                
            }else{
                $return = array(
                    'success' => 'false',
                    'browser' => $browser,
                    'browser_version' => $browser_version,
                    'device' => $device,
                    //'msg' => 'This product was built for use in Chrome browser only.'
                    'msg' => 'This product was built for use in <font style="color:red;"><b>Chrome browser</b></font> for your device.  Please log in again using Chrome.  If you don’t have Chrome installed, follow this <a target="_blank" href="https://www.google.com.au/chrome/?brand=CHBD&gclid=EAIaIQobChMI2caQ7Prw5QIV2AorCh2zNgGgEAAYASABEgIhOPD_BwE&gclsrc=aw.ds" style="text-decoration: underline;">link</a> to download.'
                );
            }
        }
        
        return $return;
    }

    public static function GetMAC($salt = ""){
       if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {  
          
        $temp = sys_get_temp_dir().DIRECTORY_SEPARATOR."diskpartscript.txt";  
        if(!file_exists($temp) && !is_file($temp)) file_put_contents($temp, "select disk 0\ndetail disk");  
        $output = shell_exec("diskpart /s ".$temp);  
        $lines = explode("\n",$output);  
        $result = array_filter($lines,function($line) {  
            return stripos($line,"ID:")!==false;  
        });  
          
          
        if(count($result)>0) {  
            $result = array_shift(array_values($result));  
            $result = explode(":",$result);  
            $result = trim(end($result));         
        } else $result = $output;         
    } else {  
        $result = shell_exec("blkid -o value -s UUID");    
        if(stripos($result,"blkid")!==false) {  
            $result = $_SERVER['HTTP_HOST'];  
        }  
    }     
    return  md5($salt.md5($result));  
    
}

    public static function getOS() {

        $user_agent = $_SERVER['HTTP_USER_AGENT'];

        $os_platform = "Unknown OS Platform";

        $os_array = array(
            '/windows nt 10/i' => 'Windows 10',
            '/windows nt 6.3/i' => 'Windows 8.1',
            '/windows nt 6.2/i' => 'Windows 8',
            '/windows nt 6.1/i' => 'Windows 7',
            '/windows nt 6.0/i' => 'Windows Vista',
            '/windows nt 5.2/i' => 'Windows Server 2003/XP x64',
            '/windows nt 5.1/i' => 'Windows XP',
            '/windows xp/i' => 'Windows XP',
            '/windows nt 5.0/i' => 'Windows 2000',
            '/windows me/i' => 'Windows ME',
            '/win98/i' => 'Windows 98',
            '/win95/i' => 'Windows 95',
            '/win16/i' => 'Windows 3.11',
            '/macintosh|mac os x/i' => 'Mac OS X',
            '/mac_powerpc/i' => 'Mac OS 9',
            '/linux/i' => 'Linux',
            '/ubuntu/i' => 'Ubuntu',
            '/iphone/i' => 'iPhone',
            '/ipod/i' => 'iPod',
            '/ipad/i' => 'iPad',
            '/android/i' => 'Android',
            '/blackberry/i' => 'BlackBerry',
            '/webos/i' => 'Mobile'
        );

        foreach ($os_array as $regex => $value)
            if (preg_match($regex, $user_agent))
                $os_platform = $value;

        return $os_platform;
    }
    
    public static function isMobile() {
        $useragent=$_SERVER['HTTP_USER_AGENT'];

        return (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)));

    }
    
    public static function priceToMusicoins($price = 0) {
        
        $musicoins = 0;
        $doller_value = Yii::$app->params['musicoin']['doller_value'];
        if($price > 0){
            
            $musicoins = ((1 / $doller_value) * $price);
            //$musicoins = ceil(number_format((float)$musicoins, 2, '.', ''));
            $musicoins = (int) ceil($musicoins);
        }
        return $musicoins;
    }
    
    public static function getStudentTimezone($studentUuid = '') {
        
        $return = '';
        if (($model = \app\models\User::find()->where(['student_uuid' => $studentUuid])->one()) !== null) {
            $return = $model->timezone;
        }
        
        return (!empty($return)) ? $return : Yii::$app->params['timezone'];
    }
    
    public static function getTutorTimezone($tutorUuid = '') {
        
        $return = '';
        if (($model = \app\models\User::find()->where(['tutor_uuid' => $tutorUuid])->one()) !== null) {
            $return = $model->timezone;
        }
        
        return (!empty($return)) ? $return : Yii::$app->params['timezone'];
    }
    
    public static function getStudentMusicoin($studentUuid = '') {
        
        return (($model = \app\models\Student::find()->where(['student_uuid' => $studentUuid])->one()) !== null) ? $model->musicoin_balance : 0;
    }
    
    public static function validTextPattern($array=[]) {
        
	    //$pt = 'a-zA-Z 0-9_\-';
	    $pt = 'a-zA-Z 0-9_.$&%:,=!\-';
        if(!empty($array)){
            $np = implode('', $array);
            $pt = $pt.$np;
        }
        //return '/^[a-zA-Z 0-9_\-.]+$/';
        return '/^['.$pt.']+$/';
    }
    
    public static function getInstrumentImage($uuid = '') {
        
        $return = '';
        if( !empty($uuid) && ($model = \app\models\Instrument::find()->where(['uuid' => $uuid])->one()) !== null){
            if(!empty($model->image)){
                ob_start();
                fpassthru($model->image);
                $contents = ob_get_contents();
                ob_end_clean();
                $return = "data:image/jpg;base64," . base64_encode($contents);
            }
        }
        if(empty($return)){
            $return = Yii::$app->request->baseUrl . '/theme_assets/new_book/img/music-1.png';
        }
        return $return;
    }
    
    public static function getTutorImage($uuid = '') {
        
        $return = '';
        if( !empty($uuid) && ($model = \app\models\Tutor::find()->where(['uuid' => $uuid])->one()) !== null){
            if(!empty($model->profile_image)){
                ob_start();
                fpassthru($model->profile_image);
                $contents = ob_get_contents();
                ob_end_clean();
                $return = "data:image/jpg;base64," . base64_encode($contents);
            }
        }
        if(empty($return)){
            $return = Yii::$app->request->baseUrl . '/theme_assets/new_book/img/default_profile.png';
        }
        return $return;
    }
    
    public static function is_uuid($uuid) {
        $regex = '/^[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}$/i';
        return preg_match($regex, $uuid);
    }

}

?>
