<?php

use yii\helpers\Html;
//echo $content['name']; exit;
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['tutor_name'] ?>,</p>

	    <p>Your session video has been Published successfully in <?= Yii::$app->name; ?>.</p>

	    <p><strong>Session :</strong> <?= $content['title']; ?></p>
            <p><strong>Video Link :</strong> <?= $content['link'] ?></p> 

            <p> If any query for your account then please contact to administrator.</p>
            
            <p>Sincerely,</p>
            <p>System Administrators</p>
        </td>
    </tr>
</table>

