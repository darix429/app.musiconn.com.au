<?php

namespace app\models;

use Yii;
use app\models\ECCValidator;
use app\libraries\General;

/**
 * This is the model class for table "{{%student_credit_card}}".
 *
 * @property string $uuid
 * @property string $student_uuid
 * @property string $number
 * @property string $cvv
 * @property string $expire_date
 * @property string $name
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Student $studentUu
 */
class StudentCreditCard extends \yii\db\ActiveRecord
{
    //public $exp_month;
    //public $exp_year;
    public $save_future;
    public $cc_month_year;
    public $term_condition_checkbox;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%student_credit_card}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //html tag filter
            [['name'], 'match', 'pattern' => \app\libraries\General::validTextPattern(), 'message' => 'Invalid charecters used.'], 
            
            [['uuid', 'student_uuid', 'name', 'status'], 'string'],
            [[ 'number', 'cvv',  'name', 'cc_month_year'], 'required', 'on'=>'step_2'],
            [['cvv'], 'required'],
            [['expire_date', 'created_at', 'updated_at', 'cc_month_year'], 'safe'],
            //[['cvv'], 'number', 'min'=>3],
	    [['cvv'], 'integer', 'min' => 3],
            [['number'], ECCValidator::className()],
            [['cc_month_year'],'date', 'format'=>'php:Y-m', 'on'=>'step_2'],  
            [['student_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['student_uuid' => 'student_uuid']],
            [['term_condition_checkbox'], 'required', 'on' => 'step_2','requiredValue' => 1,'message' => 'Accept Terms and Condition'],
            [['term_condition_checkbox'], 'required', 'on' => 'step_term_condition','requiredValue' => 1,'message' => 'Accept Terms and Condition'],
            [['term_condition_checkbox'], 'required', 'on' => 'step_term_condition_cvv','requiredValue' => 1,'message' => 'Accept Terms and Condition'],
        ];  
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'student_uuid' => Yii::t('app', 'Student Uuid'),
            'number' => Yii::t('app', 'Card Number'),
            'cvv' => Yii::t('app', 'CVV'),
            'expire_date' => Yii::t('app', 'Expire Date'),
            'name' => Yii::t('app', 'Card Holder Name'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'exp_month' => Yii::t('app', 'Expire Month'),
            'exp_year' => Yii::t('app', 'Expire Year'),
            'cc_month_year' => 'Expiry',
            'save_future' => Yii::t('app', 'Save for future use'),
            'term_condition_checkbox' => 'Term and condition',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentUu()
    {
        return $this->hasOne(Student::className(), ['student_uuid' => 'student_uuid']);
    }
    
    public function getcreditCardList() {
        return StudentCreditCard::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid])->one();
    }

    //cron booked session mail
    public function ExpireCreditCardNotification($creditCardDetails) {
        
        $content['student_first_name'] = $creditCardDetails['student_first_name'];
        $content['student_last_name'] = $creditCardDetails['student_last_name'];
        $content['name'] = $creditCardDetails['name'];
        $content['number'] = $creditCardDetails['number'];
        $content['expire_date'] = $creditCardDetails['expire_date'];
        
        $owner = General::getOwnersEmails(['OWNER', 'ADMIN', 'SUBADMIN']);
        
        $send = Yii::$app->mailer->compose('credit_card_expire_notification', ['content' => $content], ['htmlLayout' => 'layouts/html'])
            ->setFrom(Yii::$app->params['supportEmail'])
            ->setTo($creditCardDetails['student_email'])            //student email id
            ->setBcc($owner)
            ->setSubject("Musiconn Notification:  Your Credit Card has Expired")
            ->send();

        return true;
    }
    
    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['step_term_condition'] = ['term_condition_checkbox', 'number', 'cvv',  'name', 'cc_month_year'];
        $scenarios['step_term_condition_cvv'] = ['cvv', 'term_condition_checkbox'];
        return $scenarios;
    }
    
}