<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Url;

$this->title = Yii::t('app', 'MONTHLY SUBSCRIPTION');
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_icon'] = 'my-lesson-tag.png';

//$this->registerJsFile('http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js');
$this->registerJs(
        '
    $("input[name=\"StudentMonthlySubscription[monthlysubscriptionfeecheckbox]\"]").on("ifClicked", function (event) { 
        $("#studentmonthlysubscription-monthlysubscriptionfeecheckbox").val(this.value);
        
        //$(".monthly_sub_fee_select_btn").addClass("d-none"); 
        //$(this).closest("tr.monthly_sub_fee_tr").find(".monthly_sub_fee_select_btn").removeClass("d-none"); 
    });
    
    $( "#frm_step_1" ).submit(function( event ) {
        blockBody();
    });

        ', View::POS_END
);
?>
<section class="content">
    
    <div class="row mar-bottom-10">
        <div class="col-xs-12">
            <div class="box">
                <div class="col-md-12 box-panel-header border-bottom-radious-none">
                    <h4 class="col-md-6">Select Monthly Subscription</h4>
                    <div class="col-md-6 "> </div>
                </div>
                <div class="box-body box-panel-body">
                    <div class="row vertical-center-box vertical-center-box-tablet" >
                        <?php
                    $form = ActiveForm::begin([
                                'action' => ['index'],
                                'id' => 'frm_step_1',
                                'enableClientValidation' => true,
//                              'enableAjaxValidation' => true,
                                'validateOnChange' => true,
                                'options' => ['class' => 'form-horizontal']]);
                    ?>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <div class="row" style="margin-top:10px;">
                                    <input type="hidden" name="step" value="1">
                                    <input type="hidden" name="monthlysubscriptionfee_uuid" value="<?= $subscriptionFeeList->uuid; ?>">
                                </div>
                                
                                <div class="row pakage-list monthly_sub_fee_tr" >
                                    <div class="col-md-2 text-center">
                                        <?php
                                                $model->monthlysubscriptionfeecheckbox = $subscriptionFeeList->uuid;
                                                echo $form->field($model, 'monthlysubscriptionfeecheckbox')->radio([
                                                    'label' => '',
                                                    'template' => '{input}{error} ',
                                                    'class' => 'skin-square-red float-left monthlysubscriptionfee_checkbox', 'value' => $subscriptionFeeList->uuid])->label('');
                                                ?>
                                    </div>
                                    <div class="col-md-8">
                                        <p class="pull-left"><?= $subscriptionFeeList->name ?></p>
                                        <p class="pull-right">$<?= $subscriptionFeeList->total_price; ?></p>
                                    </div>
                                    <div class="col-md-2">
                                        <?= Html::submitButton('Select', ['class' => "btn btn-primary btn-md monthly_sub_fee_select_btn"]) ?>
                                    </div>
                                </div>
                                
                                
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row mar-bottom-10">
        <div class="col-xs-12">
            <div class="box">
                <div class="col-md-12 box-panel-header ">
                    <h4 class="col-md-6">Payment</h4>
                    <div class="col-md-6 "></div>
                </div>
            </div>
        </div>
    </div>
    <a class="btn btn-primary btn-sm black_button_in" href="<?= Yii::$app->getUrlManager()->createUrl('/student/profile'); ?>">RETURN TO PROFILE</a>
</section>

<style>
    .field-studentmonthlysubscription-instrumentcheckbox{
        margin-bottom: 0px !important;
    }
</style>
