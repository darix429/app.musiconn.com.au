<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "stanadard_plan_options".
 *
 * @property string $uuid
 * @property string $option_value
 * @property string $option_name
 */
class StanadardPlanOptions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'stanadard_plan_options';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'option_value', 'option_name'], 'required'],
            [['uuid', 'option_value', 'option_name'], 'string'],
            [['uuid'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'option_value' => Yii::t('app', 'Option Value'),
            'option_name' => Yii::t('app', 'Option Name'),
        ];
    }
}
