<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\PremiumStandardPlanRate;
use app\models\TutorFee;
use app\models\Order;
use app\models\StudentTuitionBooking;
?>

<div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">

    <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Booking ID</th>
                <th>Booking Date</th>
                <th>Transaction ID</th>
                <th>Order ID</th>
                <th>Student ID</th>
                <th>Student Name</th>
                <th>Tutor Name</th>
                <th>Instrument</th>
                <!--<th>Session Pin</th>-->
                <th>Lesson Date</th>
                <th>Time</th>
                <th>Lesson Duration</th>
                <th>Tutor Plan Category</th>
                <th>Tutor Plan Option</th>
                <th>Amount</th>
                <th>GST</th>
                <th>Total Amount</th>
                <th>Discount</th>
                <th>Received Amount</th>
                <th>Tutor Pay Rate(GST-Inclusive)</th>
                <th>NSW Creative Kids Voucher Code</th>
                <th>Voucher Kid's Name</th>
                <th>Voucher Kid's Date of Birth</th>
                <th>Voucher Kid's Postcode</th>
                <th>Tutorial Package Purchased</th>
                <th>Free Credit</th>
                <th>Cancellation Date</th>
                <th>Status</th>
                <th>Created Date</th>
            </tr>
        </thead>

        <tbody>
            <?php
            if (!empty($bookedSessionList)) {
                foreach ($bookedSessionList as $key => $value) {
                    //echo "<pre>";print_r($value);echo "</pre>";
                    $tutor_fee = '0.00';
                    $tutorial_name = (isset($value['creditSessionUu']['credited_by']) && $value['creditSessionUu']['credited_by'] == 'OWNER' || $value['creditSessionUu']['credited_by'] == 'ADMIN' || $value['creditSessionUu']['credited_by'] == 'SUBADMIN') ? "Free Credit" : "No";
                    $student_name = $value['studentUu']['first_name'] . ' ' . $value['studentUu']['last_name'];
                    $tutor_name = $value['tutorUu']['first_name'] . ' ' . $value['tutorUu']['last_name'];
                    //echo "<pre>";print_r($tutorList);echo "</pre>";
                    $plan_model = PremiumStandardPlanRate::find()->joinWith(['onlineTutorialPackageTypeUu'])->where(['premium_standard_plan_rate.uuid' => $value['studentBookingUu']['premium_standard_plan_rate_uuid']])->asArray()->one();
                    $option = $value['studentBookingUu']['plan_rate_option'];
                    $tutor_plan_cat = strtoupper($plan_model['onlineTutorialPackageTypeUu']['display_name']);

                    //if (!empty($value['studentBookingUu']['premium_standard_plan_rate_uuid'])) {
                    if (!empty($value['studentBookingUu']['premium_standard_plan_rate_uuid']) && empty($value['credit_session_uuid'])) {
                        $tutor_fee_model = TutorFee::find()->where(['online_tutorial_package_type_uuid' => $plan_model['onlineTutorialPackageTypeUu']['uuid'], 'option' => $option, 'sesson_length' => $value['session_min']])->one();

                        if (isset($tutor_fee_model->tutor_fee)) {
                            $tutor_fee = $tutor_fee_model->tutor_fee;
                        }
                        $plan_rate_option = ($value['studentBookingUu']['plan_rate_option'] != 0) ? $value['studentBookingUu']['plan_rate_option'] : '';
                    } else {
                        //This lessons rate are defined which lesson booked before option rate module implementation.
                        $before_rate_module_price_array = [30 => 37, 60 => 66];
                        $tutorList = StudentTuitionBooking::getInstrumentWiseTutorListSingle($value['instrumentUu']['uuid'], $value['tutorUu']['uuid']);
                        if (!empty($tutorList)) {
                            //$TutorInstrumentPlanOptions = StudentTuitionBooking::getTutorInstrumentPlanOptions($tutorList[0]['online_tutorial_package_type_uuid'], $tutorList[0]['uuid'], $tutorList[0]['instrument_uuid']);
                            if (isset($tutorList) && $tutorList[0]['standard_plan_rate_option'] != 0) {
                                $TutorInstrumentPlanOptions = StudentTuitionBooking::getTutorInstrumentPlanOptions($tutorList[0]['online_tutorial_package_type_uuid'], $tutorList[0]['uuid'], $tutorList[0]['instrument_uuid']);
                                $InstrumentWiseTutorPriceList = StudentTuitionBooking::getInstrumentWiseTutorPriceList($tutorList[0]['online_tutorial_package_type_uuid'], $TutorInstrumentPlanOptions['standard_plan_rate_option']);
                                //echo "<pre>";print_r($InstrumentWiseTutorPriceList);echo "</pre>";
                                //echo "<pre>";print_r($tutor_fee_model);echo "</pre>";
                                if (date("Y-m-d", strtotime($value['created_at'])) > date('Y-m-d', strtotime('2019-10-25'))) {
                                    $tutor_fee_model = TutorFee::find()->where(['online_tutorial_package_type_uuid' => $tutorList[0]['online_tutorial_package_type_uuid'], 'option' => $TutorInstrumentPlanOptions['standard_plan_rate_option'], 'sesson_length' => $value['session_min']])->one();
                                    //echo "<pre>";print_r($tutor_fee_model);echo "</pre>";
                                    $tutor_fee = $tutor_fee_model['tutor_fee'];
                                    $plan_rate_option = $TutorInstrumentPlanOptions['standard_plan_rate_option'];
                                    $plan_type_model = \app\models\OnlineTutorialPackageType::find()->where(['uuid' => $tutor_fee_model['online_tutorial_package_type_uuid']])->one();
                                    //echo "<pre>";print_r($plan_type_model->display_name);echo "</pre>";	
                                } else {
                                    $tutor_fee = ($value['session_min'] == 30) ? $before_rate_module_price_array[30] : $before_rate_module_price_array[60];
                                    $plan_type_model = \app\models\OnlineTutorialPackageType::find()->where(['name' => 'PREMIUM'])->one();
                                    $plan_rate_option = ($value['studentBookingUu']['plan_rate_option'] != 0) ? $value['studentBookingUu']['plan_rate_option'] : '';
                                }
                            } else {
                                if (date("Y-m-d", strtotime($value['created_at'])) > date('Y-m-d', strtotime('2019-10-25'))) {
                                    if (in_array($value['studentBookingUu']['plan_rate_option'], ['', '0'])) {
                                        $tutor_fee_model = TutorFee::find()->where(['option' => '0', 'sesson_length' => $value['session_min']])->one();
                                        $tutor_fee = $tutor_fee_model['tutor_fee'];
                                    } else {
                                        $tutor_fee_model = TutorFee::find()->where(['option' => $value['studentBookingUu']['plan_rate_option'], 'sesson_length' => $value['session_min']])->one();
                                        $tutor_fee = $tutor_fee_model['tutor_fee'];
                                    }
                                } else {

                                    $tutor_fee = ($value['session_min'] == 30) ? $before_rate_module_price_array[30] : $before_rate_module_price_array[60];
                                }
                                $plan_type_model = \app\models\OnlineTutorialPackageType::find()->where(['name' => 'PREMIUM'])->one();
                                $plan_rate_option = ($value['studentBookingUu']['plan_rate_option'] != 0) ? $value['studentBookingUu']['plan_rate_option'] : '';
                            }
                        } else {
                            //echo '</br>'.$value['studentBookingUu']['plan_rate_option'].'patel'.$value['created_at'];
                            if (date("Y-m-d", strtotime($value['created_at'])) > date('Y-m-d', strtotime('2019-10-25'))) {
                                if (in_array($value['studentBookingUu']['plan_rate_option'], ['', '0'])) {
                                    $tutor_fee_model = TutorFee::find()->where(['option' => '0', 'sesson_length' => $value['session_min']])->one();
                                    $tutor_fee = $tutor_fee_model['tutor_fee'];
                                } else {
                                    $tutor_fee_model = TutorFee::find()->where(['option' => $value['studentBookingUu']['plan_rate_option'], 'sesson_length' => $value['session_min']])->one();
                                    $tutor_fee = $tutor_fee_model['tutor_fee'];
                                }
                            } else {

                                $tutor_fee = ($value['session_min'] == 30) ? $before_rate_module_price_array[30] : $before_rate_module_price_array[60];
                            }
                            $plan_type_model = \app\models\OnlineTutorialPackageType::find()->where(['name' => 'PREMIUM'])->one();
                            $plan_rate_option = ($value['studentBookingUu']['plan_rate_option'] != 0) ? $value['studentBookingUu']['plan_rate_option'] : '';
                        }
                        /* if(date("Y-m-d",strtotime($value['created_at'])) > date('Y-m-d',strtotime('2019-10-25'))){
                          $tutor_fee_model = TutorFee::find()->where(['online_tutorial_package_type_uuid' => $tutorList[0]['online_tutorial_package_type_uuid'], 'option' => $TutorInstrumentPlanOptions['standard_plan_rate_option'], 'sesson_length' => $value['session_min']])->one();
                          //echo "<pre>";print_r($tutor_fee_model);echo "</pre>";
                          $tutor_fee = $tutor_fee_model['tutor_fee'];
                          $plan_rate_option =$TutorInstrumentPlanOptions['standard_plan_rate_option'];
                          $plan_type_model = \app\models\OnlineTutorialPackageType::find()->where(['uuid' => $tutor_fee_model['online_tutorial_package_type_uuid']])->one();
                          //echo "<pre>";print_r($plan_type_model->display_name);echo "</pre>";
                          }else{
                          $tutor_fee =  ($value['session_min'] == 30) ? $before_rate_module_price_array[ 30 ] : $before_rate_module_price_array[ 60 ];
                          $plan_type_model = \app\models\OnlineTutorialPackageType::find()->where(['name' => 'PREMIUM'])->one();
                          $plan_rate_option =($value['studentBookingUu']['plan_rate_option']!=0) ? $value['studentBookingUu']['plan_rate_option']:'';
                          } */
//                        $plan_type_model = \app\models\OnlineTutorialPackageType::find()->where(['name' => 'PREMIUM'])->one();
                        $tutor_plan_cat = (!empty($plan_type_model)) ? strtoupper($plan_type_model->display_name) . "" : "";

                        //if lesson booked with credited lesson options then set tutor rate
                        /* if($tutorial_name == 'Free Credit'){
                          $tutor_plan_cat =  (!empty($plan_type_model))? strtoupper($plan_type_model->display_name) : "";
                          $credited_tutor_fee_model = TutorFee::find()->where(['online_tutorial_package_type_uuid' => $plan_type_model->uuid, 'option' => 0, 'sesson_length' => $value['session_min']])->one();
                          $tutor_fee = (!empty($credited_tutor_fee_model)) ? $credited_tutor_fee_model->tutor_fee : 0;
                          } */
                    }
                    $booking_date = $order_id = $kids_code = $kids_name = $kids_dob = $kids_postal_code = $transaction_id = "";
                    $net_payment = $sub_price = $sub_gst = $sub_total = $price = $grand_payment = $total_payment = $gst = $discount = 0;
                    $instrument_name = $value['instrumentUu']['name'];
                    if (!empty($value['studentBookingUu']['booking_id'])) {

                        $booking_date = \app\libraries\General::displayDate($value['studentBookingUu']['booking_datetime']);
                        $order_model = Order::findOne($value['studentBookingUu']['order_uuid']);
                        $order_id = (!empty($order_model)) ? "#" . $order_model->order_id : "";
                        if(!empty($order_model) && !empty($order_model->paymentTransactionUu->transaction_number)){
                            $transaction_id = "#" . $order_model->paymentTransactionUu->transaction_number;
                        }

                        if (!empty($value['studentBookingUu']['student_monthly_subscription_uuid'])) {
                            $SMSub = \app\models\StudentMonthlySubscription::findOne($value['studentBookingUu']['student_monthly_subscription_uuid']);
                            if (!empty($SMSub)) {
                                $sub_price = $SMSub->price;
                                $sub_gst = $SMSub->tax;
                                $sub_total = $SMSub->total_price;
                            }
                        }

                        $price = $value['studentBookingUu']['price'];
                        $total_payment = $price + $sub_price;
                        $gst = $value['studentBookingUu']['gst'] + $sub_gst;
                        $net_payment = $total_payment + $gst;
                        $discount = $value['studentBookingUu']['discount'];
                        $grand_payment = $value['studentBookingUu']['grand_total'];

                        $kids_code = $value['studentBookingUu']['creative_kids_voucher_code'];
                        $kids_name = $value['studentBookingUu']['kids_name'];
                        $kids_dob = $value['studentBookingUu']['kids_dob'];
                        $kids_postal_code = (!empty($value['studentBookingUu']['kids_postal_code'])) ? $value['studentBookingUu']['kids_postal_code'] : "";
                    }
                    ?>
                    <tr>
                        <td><?= $key + 1; ?></td>
                        <td><?= (!empty($value['studentBookingUu']['booking_id'])) ? "#" . $value['studentBookingUu']['booking_id'] : ''; ?></td>
                        <td><?= $booking_date; ?></td>
                        <td><?= $transaction_id; ?></td>
                        <td><?= $order_id; ?></td>
                        <td><?= $value['studentUu']['enrollment_id']; ?></td>
                        <td><?= (!empty($student_name)) ? $student_name : ''; ?></td>
                        <td><?= (!empty($tutor_name)) ? $tutor_name : ''; ?></td>
                        <td><?= $instrument_name; ?></td>
                        <!--<td><?php //echo $value['session_pin'];  ?></td>-->
                        <td><?= \app\libraries\General::displayDate($value['start_datetime']); ?></td>
                        <td><?= \app\libraries\General::displayTime($value['start_datetime']) . " - " . \app\libraries\General::displayTime($value['end_datetime']) ?></td>
                        <td><?= $value['session_min']; ?> minutes</td>
                        <td><?= $tutor_plan_cat ?></td>
                        <!--td><?= ($value['studentBookingUu']['plan_rate_option'] != 0) ? $value['studentBookingUu']['plan_rate_option'] : '' ?></td-->
                        <td><?= $plan_rate_option; ?></td>
                        <td><?= "$" . $total_payment; ?></td>
                        <td><?= "$" . $gst; ?></td>
                        <td><?= "$" . $net_payment; ?></td>
                        <td><?= "$" . $discount; ?></td>
                        <td><?= "$" . $grand_payment; ?></td>
                        <td><?= "$" . $tutor_fee; ?></td>
                        <td><?= (!empty($kids_code)) ? "#" . trim($kids_code, "#") : ""; ?></td>
                        <td><?= $kids_name; ?></td>
                        <td><?= $kids_dob; ?></td>
                        <td><?= $kids_postal_code; ?></td>
                        <td><?= $value['studentBookingUu']['tutorial_name']; ?></td>
                        <td><?= $tutorial_name == 'Free Credit' ? 'Yes' : ''; ?></td>
                        <td><?= (!empty($value['cancel_datetime'])) ? \app\libraries\General::displayDate($value['cancel_datetime']) : '-'; ?></td>
                        <td>
                            <?php
                            if ($value['status'] == 'SCHEDULE') {
                                $e_class = "badge-info";
                                //$e_link_class = "coupon_disabled_link";
                            } elseif ($value['status'] == 'PUBLISHED') {
                                $e_class = "badge-success";
                                //$e_link_class = "coupon_enabled_link";
                            } elseif ($value['status'] == 'EXPIRED') {
                                $e_class = "badge-secondary";
                                //$e_link_class = "coupon_enabled_link";
                            } elseif ($value['status'] == 'INPROCESS') {
                                $e_class = "badge-warning";
                                //$e_link_class = "coupon_enabled_link";
                            } else {
                                $e_class = "badge-danger";
                                //$e_link_class = "";
                            }
                            ?>
                            <span class="badge badge-pill <?= $e_class ?> "  ><?= $value['status']; ?></span>
                        </td>
                        <td><?php echo date("d-m-Y", strtotime($value['created_at'])); ?> </td>

                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>

</div>