<?php

namespace app\models;
use app\libraries\General;
use Yii;

/**
 * This is the model class for table "{{%student_subscription}}".
 *
 * @property string $uuid
 * @property string $student_uuid
 * @property string $subscription_uuid
 * @property string $next_renewal_date
 *
 * @property Student $studentUu
 */
class StudentSubscription extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%student_subscription}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'student_uuid', 'subscription_uuid'], 'string'],
            [['student_uuid', 'subscription_uuid', 'next_renewal_date'], 'required'],
            [['next_renewal_date'], 'safe'],
            [['student_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['student_uuid' => 'student_uuid']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'student_uuid' => Yii::t('app', 'Student Uuid'),
            'subscription_uuid' => Yii::t('app', 'Subscription Uuid'),
            'next_renewal_date' => Yii::t('app', 'Next Renewal Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentUu()
    {
        return $this->hasOne(Student::className(), ['student_uuid' => 'student_uuid']);
    }
    
    //cron mail
    public function ExpireStudentSubscriptionMail($studentsubDetails) { 
        
        $content['student_name'] = $studentsubDetails['student_name'];
	$content['name'] = $studentsubDetails['name'];
        $content['price'] = $studentsubDetails['price'];
        $content['tax'] = $studentsubDetails['tax'];
        $content['total_price'] = $studentsubDetails['total_price'];
        
        //$owner = General::getOwnersEmails(['OWNER', 'ADMIN', 'SUBADMIN']);
        
        $mail = Yii::$app->mailer->compose('expired_student_subscription', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                ->setFrom(Yii::$app->params['supportEmail'])
                ->setTo($studentsubDetails['email'])
                //->setBcc($owner)
                ->setSubject("Student Video Library Subscription Expired -" . Yii::$app->name);

        $mail->send();

        return true;
    }
}
