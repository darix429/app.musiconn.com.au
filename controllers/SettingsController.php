<?php

namespace app\controllers;

use Yii;
use app\models\Settings;
use app\models\SettingsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
//use app\models\User;
use app\libraries\General;
use yii\helpers\Url;
use yii\filters\AccessControl;
use app\models\ChangePassword;

/**
 * SettingsController implements the CRUD actions for Settings model.
 */
class SettingsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
	    'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                        [
                        'actions' => ['index'],
                        'allow' => Yii::$app->user->can('/settings/index'),
                        'roles' => ['@'],
                    ],
			[
                        'actions' => ['update'],
                        'allow' => Yii::$app->user->can('/settings/update'),
                        'roles' => ['@'],
                    ],
                        
                        
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Settings models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SettingsSearch();
        $settingList = $searchModel->search(Yii::$app->request->queryParams,true);

	if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_setting_list', [
                        'settingList' => $settingList,
            ]);
        } else {
            return $this->render('index', [
                        //'inactiveAdmin' => $inactiveAdmin,
                        'settingList' => $settingList,
                        'searchModel' => $searchModel,
            ]);
        }
       
    }

    /**
     * Displays a single Settings model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    /*public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }*/

    /**
     * Creates a new Settings model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    /*public function actionCreate()
    {
        $model = new Settings();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->uuid]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }*/

    /**
     * Updates an existing Settings model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
	try{
		$model = $this->findModel($id);
		
		if ($model->load(Yii::$app->request->post())) {
			//$model->created_at = $model->updated_at = date('Y-m-d H:i:s', time());
		        if($model->save()){ 
				Yii::$app->session->setFlash('success', Yii::t('app', 'Setting successfully updated.'));
				return $this->redirect(['index']);
			}
		}

		return $this->render('update', [
		    'model' => $model,
		]);
	} catch (\yii\base\Exception $exception) {
            throw new \yii\web\HttpException(505, $exception->getMessage());
        }

    }

    /**
     * Deletes an existing Settings model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    /*public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }*/

    /**
     * Finds the Settings model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Settings the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Settings::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /*public function actionSystemSetting()
    {
        return $this->render('system_setting', [
           // 'model' => $this->findModel($id),
        ]);
    }*/

}
