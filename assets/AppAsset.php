<?php
namespace app\assets;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        
        'theme_assets/plugins/pace/pace-theme-flash.css',        
        'theme_assets/plugins/bootstrap/css/bootstrap.min.css',        
        'theme_assets/fonts/font-awesome/css/font-awesome.css',
        'theme_assets/css/animate.min.css',
        'theme_assets/plugins/perfect-scrollbar/perfect-scrollbar.css',
        'theme_assets/plugins/messenger/css/messenger.css',
        'theme_assets/plugins/messenger/css/messenger-theme-future.css',
        'theme_assets/plugins/messenger/css/messenger-theme-flat.css',
        'theme_assets/plugins/datatables/css/datatables.min.css',
        'theme_assets/plugins/icheck/skins/all.css',
        'theme_assets/css/style.css',
        'theme_assets/css/responsive.css',
        'theme_assets/css/custom.css',
        'theme_assets/plugins/select2/select2_4.css',
        'theme_assets/plugins/multi-select/css/multi-select.css',
        'theme_assets/plugins/ios-switch/css/switch.css',
        'theme_assets/plugins/video-js/video-js.css',
        'theme_assets/plugins/dropzone/css/dropzone.css',
        'theme_assets/plugins/calendar/fullcalendar.css',
        'theme_assets/plugins/datepicker/css/datepicker.css',
        'theme_assets/plugins/timepicker/css/bootstrap-timepicker.css',
        'theme_assets/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css',
        'theme_assets/plugins/daterangepicker/css/daterangepicker-bs3.css',
	
    ];
    public $js = [
        
        'theme_assets/js/popper.min.js',
        'theme_assets/plugins/bootstrap/js/bootstrap.min.js',
        'theme_assets/plugins/pace/pace.js',
        'theme_assets/plugins/pace/blockui.min.js',
        'theme_assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js',        
        'theme_assets/plugins/viewport/viewportchecker.js',
        'theme_assets/plugins/autosize/autosize.min.js',
        'theme_assets/plugins/icheck/icheck.min.js',
        'theme_assets/js/scripts.js',
        'theme_assets/plugins/sparkline-chart/jquery.sparkline.min.js',
        'theme_assets/js/chart-sparkline.js',
        'theme_assets/plugins/datepicker/js/datepicker.js',
        'theme_assets/plugins/daterangepicker/js/moment.min.js',
        'theme_assets/plugins/daterangepicker/js/moment-timezone-with-data.js',
        'theme_assets/plugins/daterangepicker/js/daterangepicker.js',
        'theme_assets/plugins/timepicker/js/bootstrap-timepicker.min.js',
        'theme_assets/plugins/datetimepicker/js/bootstrap-datetimepicker.min.js',
        'theme_assets/plugins/select2/select2_4.js',
        'theme_assets/plugins/multi-select/js/jquery.multi-select.js',
        'theme_assets/plugins/multi-select/js/jquery.quicksearch.js',
        'theme_assets/plugins/video-js/js/video.js',
        'theme_assets/plugins/dropzone/dropzone.js',
        'theme_assets/js/custom.js',
    ];
    public $depends = [
        
    ];
}
