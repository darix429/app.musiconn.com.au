<?php
 
use yii\helpers\Html;
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['name']; ?>,</p>

            <p>Your contract has expired on <?= $content['contract_expiry']; ?> in <?= Yii::$app->name; ?>.</p>

	    <p><strong>Expiry Date :</strong> <?= $content['contract_expiry']; ?></p>

            <p> If any query for your account then please contact to administrator.</p>

            <p>Sincerely,</p>
            <p>System Administrators</p>
        </td>
    </tr>
</table>

