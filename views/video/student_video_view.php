<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\ArrayHelper;
use app\models\InstrumentCategory;
use app\models\Instrument;
use app\models\User;
use app\models\Student;
use app\models\StudentViewPreRecordedLesson;
use yii\helpers\Url;

$this->title = Yii::t('app', 'VIDEO LIBRARY');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Videos'), 'url' => (isset($back_url)) ? $back_url : ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_icon'] = 'VideoLibrary-gradient.png';
/*$this->params['page_title_custom'] = '<div class="col-md-7">
                        <h1>
                            <img src="' . Yii::$app->request->baseUrl . '/theme_assets/new/img/video-icon.png' . '">VIDEO LIBRARY 
                        </h1>
                    </div>
                    <div class="col-md-5">
                        <a href="'.Url::to(['/booked-session/completed']).'" class="btn btn-primary btn-sm black_button_in" >MY RECORDED LESSONS</a>
                        <a href="'.Url::to(['/video/student-video-list/']).'" class="btn btn-primary btn-sm black_button_in" >VIDEO LIBRARY</a>
                    </div>';*/
$this->params['page_title_custom'] = ''
        . '         <div class="col-md-12">
                        <div class="col-md-5">
                            <h3>
                                <img src="'. Yii::$app->request->baseUrl . '/theme_assets/new_book/img/'.$this->params['page_icon'] .'" style="width: 52px;position: relative;">'
            . '                 <strong class="title-hm">'.strtoupper($this->title).'</strong>
                            </h3>
                        </div>
                        <div class="col-md-7 text-right">
                            <a href="'.Url::to(['/booked-session/completed']).'" class="btn btn-primary btn-sm black_button_in" >MY RECORDED LESSONS</a>
                            <a href="'.Url::to(['/video/student-video-list/']).'" class="btn btn-primary btn-sm black_button_in" >VIDEO LIBRARY</a>
                        </div>
                    </div>
                    ';

$this->registerJs(
        '  
            var videocount = true;
var vid = document.getElementById("myVideo");
vid.onplay= function() {

      
 var seccount = setInterval(function(){ 
    //code goes here that will be run every 5 seconds. 
    var current_seconds = vid.currentTime;
    if(current_seconds >= 30 && videocount==true)
    {
    student_view_count(current_seconds);
    videocount = false;
    clearInterval(seccount);

    } 
}, 2000);

};

function student_view_count(video_seconds)
{

$.ajax({
                        type: "GET",
                        url: "' . Url::to(['/video/student_view_pre_recorded_count/', 'id' => $model->uuid]) . '",
                       // data: { id: id,part:p},
                        success:function(result){
                          
                        },
                        error:function(e){
                            showErrorMessage(e.responseText);
                        }
                    });
 
}

$(document).on("click",".instrument_btn", function(){  
    
    $("#page_no").val(0);
    $("#search_input").val("");
    $("#instrument_uuid").val($(this).data("bind"));
    
    $("#search_frm").submit();
    
}); 
$(document).on("click",".search_input_btn", function(){  
    
    $("#page_no").val(0);
    $("#search_input").val($("#search_input_1").val());
    $("#instrument_uuid").val("");
    
    $("#search_frm").submit();
    
}); 


', View::POS_END
);

//$user = User::find()->where(['id' => $model->user_uuid])->one();
if ($model['tutor_uuid'] != NULL) {
    $user = \app\models\Tutor::find()->where(['uuid' => $model['tutor_uuid']])->one();
} else {
    $user = \app\models\Admin::find()->where(['admin_uuid' => $model['admin_uuid']])->one();
}
$instrument = Instrument::find()->where(['uuid' => $model->instrument_uuid])->one();
$instrumentCategory = InstrumentCategory::find()->where(['uuid' => $model->instrument_category_uuid])->one();

$instrumentArray = ArrayHelper::map(Instrument::find()->where(['status' => 'ACTIVE'])->asArray()->all(), 'uuid', 'name');

$file_path = Yii::$app->params['media']['pre_recorded_video']['path'] . $model->file;
$file_url = Yii::$app->params['media']['pre_recorded_video']['url'] . $model->file;
$view_count = StudentViewPreRecordedLesson::find()->where(['pre_recorded_video_uuid'=> $model->uuid])->count();
$poster = (is_file($file_path)) ? '' : Yii::$app->params['theme_assets']['url'] . "images/novideo.png";
?>
<style>
    .instrument_btn{
        cursor: pointer;
    }
    .search_input_btn{
        cursor: pointer;
    }
    #myVideo{
         width: 100%;
        border-radius: 10px;
        max-height: 340px;
        border: 1px solid #000;
    }
</style>
<section class="content video_display">
    <div class="col-md-2">
        <div class="white_bg">
            <div class="text-center categry-bg-white">
                <?php if (!empty($instrumentArray)) { ?>
                    <?php
                    foreach ($instrumentArray as $key => $value) {
                        $instrument_video_count = \app\models\PreRecordedVideo::instrumentPreRecordedVideoCount($key);
                        ?>

                        <div class=""><a  class="instrument_btn" data-bind="<?= $key ?>"><?= $value . ' (' . $instrument_video_count . ')' ?></a></div>
                    <?php } ?>
                <?php } ?>
            </div>
            <div class="form-group video_search_btm">
                <label for="">Search</label>
                <div class="input-group">
                    <input type="text" name="search_input_1" id="search_input_1" class="form-control">
                    <span class="input-group-addon search_input_btn"><i class="fa fa-search"></i></span>
                </div>
                
            </div>

        </div>
    </div>
    <div class="col-md-10">
        <div class="container-fluid video_categry-bg-white">
            <div class="col-md-10 video_content_bar">
                <!--<img class="video_library_img img-reponsive" src="img/video.png">-->
                <div class=" mb-0">
                    <video id="myVideo" class="video-js resize" controls preload="none" controlsList="nodownload"  poster="<?= $poster; ?>">
                        <source src="<?= $file_url ?>" type="video/mp4" > 
                    </video> 
                </div>
                <div class="clearfix"></div>
                <h4><?= $model->title; ?></h4>
                <p><?= $user->first_name . ' ' . $user->last_name; ?></p>
                
                <?php if (!empty($view_count) && ($view_count > 0)) { ?>
                    <p><b><?= $view_count."</b> views"; ?></p>
                    <?php } ?>
                    
                <?php if (!empty($model->instrument_uuid)) { ?>
                    <p id="instrument" class="orange"><?= $instrument->name; ?></p>
                <?php } ?>
                   
                <?php if (!empty($model->instrument_category_uuid)) { ?>
                    <p id="instrumentCategory" class="orange"><?= $instrumentCategory->name; ?></p>
                <?php } ?>
                <p>
                    <?php if (!empty($model->description)) { ?>
                        <?= $model->description; ?>
                    <?php } ?>
                </p>
            </div>
            <div class="col-md-2">
                <h5><b>RECOMMENDED VIDEOS</b></h5>

                <?php
                $student = Student::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid])->one();

                if ($student->isMonthlySubscription == '1') {
                    $recordData = app\models\PreRecordedVideo::find()->joinWith(['instrumentUu'])->where(['<>', 'pre_recorded_video.uuid', $model->uuid])->andWhere("(is_free = 1 and pre_recorded_video.status ='PUBLISHED') OR pre_recorded_video.status ='PUBLISHED' ")->asArray()->limit(3)->orderBy('pre_recorded_video.updated_at')->all();
                } else {
                    $recordData = app\models\PreRecordedVideo::find()->joinWith(['instrumentUu'])->where(['<>', 'pre_recorded_video.uuid', $model->uuid])->andWhere("is_free = 1 and pre_recorded_video.status ='PUBLISHED'")->asArray()->limit(3)->orderBy('pre_recorded_video.updated_at')->all();
                }

                if (!empty($recordData)) {
                    foreach ($recordData as $key => $value) {
                        $thumb_url = is_file(Yii::$app->params['media']['pre_recorded_video']['thumbnail']['path'] . $value['uuid'] . '.jpg') ?
                                Yii::$app->params['media']['pre_recorded_video']['thumbnail']['url'] . $value['uuid'] . '.jpg' :
                                Yii::$app->params['theme_assets']['url'] . "images/thumbnail.jpeg";
                        ?>

                        <a href="<?= Url::to(['/video/review/', 'id' => $value['uuid']]) ?>">
                            <img class="video_library_img img-reponsive" src="<?= $thumb_url; ?>">
                        </a>
                        <p class="orange video_content_side_bar"><?= $value['instrumentUu']['name']; ?></p>
                        <p class="video_content_side_bar_sub"><?= $value['title']; ?></p>
                        <p class="video_name">
                            <?php
                                if ($value['tutor_uuid'] != NULL) {
                                    $username = \app\models\Tutor::find()->where(['uuid' => $value['tutor_uuid']])->one();
                                    echo $username['first_name'] . ' ' . $username['last_name'];
                                } else {
                                    $username = \app\models\Admin::find()->where(['admin_uuid' => $value['admin_uuid']])->one();
                                    echo $username['first_name'] . ' ' . $username['last_name'];
                                }
                            ?>
                        </p>
                        <br>
                        <?php
                    }
                } else {
                    ?>
                    <p>No video found</p>
                <?php }
                ?>
            </div>
        </div>
        
        <form id="search_frm" method="POST" action="<?= Url::to(['/video/student-video-list/']); ?>">
            <input type="hidden" name="page_no" id="page_no" value="0">
            <input type="hidden" name="search_input" id="search_input" value="">
            <input type="hidden" name="instrument_uuid" id="instrument_uuid" value="">
        </form>
        
    </div>
</section>
