<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\BookedSession;
use app\models\BookedSessionSearch;
use app\models\CancelSession;
use app\models\CreditSession;
use app\models\RescheduleSession;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use app\libraries\General;

/**
 * BookedSessionController implements the CRUD actions for BookedSession model.
 */
class BookedSessionController extends Controller {

    /**
     * {@inheritdoc}
     */
    public $enableCsrfValidation = false;
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                        [
                        'actions' => ['index'],
                        'allow' => Yii::$app->user->can('/booked-session/index'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['upcoming'],
                        'allow' => Yii::$app->user->can('/booked-session/upcoming'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['completed'],
                        'allow' => Yii::$app->user->can('/booked-session/completed'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['cancelled'],
                        'allow' => Yii::$app->user->can('/booked-session/cancelled'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['cancel'],
                        'allow' => Yii::$app->user->can('/booked-session/cancel'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['credited'],
                        'allow' => Yii::$app->user->can('/booked-session/credited'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['send-session-completed-mail'],
                        'allow' => true,
                    ],
                        [
                        'actions' => ['test'],
                        'allow' => true,
                    ],
                        [
                        'actions' => ['active-start-button-student'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['cancel-lesson-request'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['cancel-lesson-request-note'],
                        'allow' => Yii::$app->user->can('/booked-session/cancel-lesson-request-note'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['cancel-lesson-approval'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['expired'],
                        'allow' => Yii::$app->user->can('/booked-session/expired'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['student-reschedule-lesson-request-cancel'],
                        'allow' => Yii::$app->user->can('/booked-session/student-reschedule-lesson-request-cancel'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['reschedule-request-action'],
                        'allow' => Yii::$app->user->can('/booked-session/student-reschedule-lesson-request-cancel'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['student-cancel-lesson-request-cancel'],
                        'allow' => Yii::$app->user->can('/booked-session/student-cancel-lesson-request-cancel'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['my-lessons'],
                        'allow' => Yii::$app->user->can('/booked-session/my-lessons'),
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'send-session-completed-mail' => ['POST'],
                //'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {

        if ($action->id == 'send-session-completed-mail') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }


    public function actionUpcoming() {

        $searchModel = new BookedSessionSearch();

        $order_by = "start_datetime asc";
        if(Yii::$app->user->identity->role == 'TUTOR'){
            $view = 'tutor_upcoming';
            $view_ajax = '_upcoming';
            $searchModel->tutor_uuid = Yii::$app->user->identity->tutor_uuid;
        }else{
            
            $this->layout = "student_layout";
            
            $view = 'student_upcoming';
            $view_ajax = '_upcoming';
            $searchModel->student_uuid = Yii::$app->user->identity->student_uuid;
        }

        $statusIn = ['SCHEDULE','INPROCESS'];
        $upcomingList = $searchModel->search(Yii::$app->request->queryParams, true,'',$statusIn,$order_by);

        return $this->render($view, [
                    'searchModel' => $searchModel,
                    'upcomingList' => $upcomingList,
        ]);

        // if(Yii::$app->user->identity->role == 'TUTOR'){
        //     $dateFilter = "start_datetime > '".date("Y-m-d h:i:s")."'";
        //     $collection = BookedSession::find()
        //     ->joinWith(["tutorUu","studentUu"])
        //     ->where([
        //         "booked_session.status"=>["INPROCESS","SCHEDULE"]
        //     ])
        //     ->andWhere([
        //         ">","start_datetime",date("Y-m-d h:i:s")
        //     ])
        //     ->orderBy(["start_datetime"=>SORT_ASC])
        //     ->all();

        //     $view = 'tutor_upcoming';
        //     return $this->render($view, [
        //         'upcomingList' => $collection,
        //     ]);

        // }else{
        //     $searchModel = new BookedSessionSearch();
        //     $order_by = "start_datetime asc";
        //     $this->layout = "student_layout";
        //     $view = 'student_upcoming';
        //     $view_ajax = '_upcoming';
        //     $searchModel->student_uuid = Yii::$app->user->identity->student_uuid;

        //     $statusIn = ['SCHEDULE','INPROCESS'];
        //     $upcomingList = $searchModel->search(Yii::$app->request->queryParams, true,'',$statusIn,$order_by);
    
        //     return $this->render($view, [
        //                 'searchModel' => $searchModel,
        //                 'upcomingList' => $upcomingList,
        //     ]);
        // }
    }

    public function actionCompleted() {

        $searchModel = new BookedSessionSearch();

        $order_by = "start_datetime desc";
        if(Yii::$app->user->identity->role == 'TUTOR'){
            $view = 'tutor_completed';
            //$view_ajax = '_completed';
            $searchModel->tutor_uuid = Yii::$app->user->identity->tutor_uuid;
        }else{
            
            $this->layout = "student_layout";
            
            $view = 'student_completed';
            //$view_ajax = '_completed';
            $searchModel->student_uuid = Yii::$app->user->identity->student_uuid;
        }

        $statusIn = ['COMPLETED','PUBLISHED'];
        $completedList = $searchModel->search(Yii::$app->request->queryParams, true,'',$statusIn,$order_by);

        return $this->render($view, [
                    'searchModel' => $searchModel,
                    'completedList' => $completedList,
        ]);
    }

    public function actionCancelled() {

        $searchModel = new BookedSessionSearch();

        $order_by = "start_datetime desc";
        if(Yii::$app->user->identity->role == 'TUTOR'){
            $view = 'tutor_canceled';
            $searchModel->tutor_uuid = Yii::$app->user->identity->tutor_uuid;
        }else{
            
            $this->layout = "student_layout";
            
            $view = 'student_canceled';
            $searchModel->student_uuid = Yii::$app->user->identity->student_uuid;
        }

        $statusIn = ['CANCELLED'];
        $canceledList = $searchModel->search(Yii::$app->request->queryParams, true,'',$statusIn,$order_by);

        return $this->render($view, [
                    'searchModel' => $searchModel,
                    'canceledList' => $canceledList,
        ]);
    }

    public function actionCredited() {

        $this->layout = "student_layout";
        $searchModel = new \app\models\CreditSessionSearch();

        $order_by = "";
        $searchModel->student_uuid = Yii::$app->user->identity->student_uuid;
        $statusIn = ['PENDING'];
        $group_by = ['credit_session.uuid','tutorial_type_code'];
        $creditedList = $searchModel->search(Yii::$app->request->queryParams, true,'',$statusIn,$order_by,$group_by);

        return $this->render('student_credited', [
                    'searchModel' => $searchModel,
                    'creditedList' => $creditedList,
        ]);
    }

    /**
     * Tutor Cancel an existing BookedSession model.
     * If cancel is successful, the browser will be redirected to the 'upcoming' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionCancel($id) {
        if (($model = BookedSession::find()->where(['booked_session.uuid' => $id, 'booked_session.status' => 'SCHEDULE'])->one()) !== null) {
            if(Yii::$app->user->identity->tutor_uuid == $model->tutor_uuid){

                $response = BookedSession::cancelSession($id);

                if(isset($response['code']) && $response['code'] == 200){
                    Yii::$app->getSession()->setFlash('success', 'You have successfully cancelled Tuition.');
                }else{
                    $message = (isset($response['message'])) ? $response['message'] : 'Sorry, something went wrong.';
                    Yii::$app->getSession()->setFlash('danger', $message);
                }

            } else{
                Yii::$app->getSession()->setFlash('danger', 'You do not permission to access this page.');
            }

        } else{
            Yii::$app->getSession()->setFlash('danger', 'Sorry upcoming sesion not found.');
        }

        return $this->redirect(['upcoming']);
    }

    /**
     * Finds the BookedSession model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return BookedSession the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = BookedSession::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionIndex() {

        $searchModel = new BookedSessionSearch();
	$bookedSessionList = $searchModel->search(Yii::$app->request->queryParams, true, '', '', '');

	if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_booked_session_list', [
                        'bookedSessionList' => $bookedSessionList,
            ]);
        } else {
            return $this->render('index', [
                        //'inactiveAdmin' => $inactiveAdmin,
                        'bookedSessionList' => $bookedSessionList,
                        'searchModel' => $searchModel,
            ]);
        }

    }


    /****
     * When Tutor-Student Tuition completed then this action call and send mail
     * **********/
    public function actionSendSessionCompletedMail() {

        $resp = ['code' => 505, 'error' => 'Sorry something went wrong'];

        $uuid = Yii::$app->request->post('id');

//echo "<pre>";print_r($uuid);exit;
        if (($model = BookedSession::find()->joinWith(['studentUu','tutorUu'])->where(['booked_session.uuid' => $uuid, 'booked_session.status' => 'PUBLISHED'])->one() ) !== null) {

            BookedSession::lessonCompletedMail($uuid," api");

            $resp = ['code' => 200, 'error' => 'Mail sent successfully.'];
        } else {
            $resp = ['code' => 422, 'error' => 'Record not found.'];
        }

        Yii::$app->response->format = 'json';
        return $resp;
    }

    public function actionActiveStartButtonStudent() {

        $return['start']['id'] = [];
        $return['start']['text'] = [];
        $return['all'] = [];
        $now = date('Y-m-d H:i:s');
        $upcomingList = BookedSession::find()
                ->where(['student_uuid' => Yii::$app->user->identity->student_uuid])
                ->andWhere(['in', 'booked_session.status', ['SCHEDULE','INPROCESS']])
                ->asArray()->orderBy('start_datetime asc')->all();
        if(!empty($upcomingList)){
            foreach ($upcomingList as $key => $value) {

                if($now >= $value['start_datetime'] && $now <= $value['end_datetime']){
                    $text = ($now >= $value['start_datetime'] && $now <= $value['end_datetime']) ? "Join" : "Start";
                    $return['start']['id'][] = $value['uuid'];
                    $return['start']['text'][] = $text;
                }
                $return['all'][] = $value['uuid'];
            }
        }

        Yii::$app->response->format = 'json';
        return $return;
    }

    /*public function actionTest() {

        $model = \app\models\CreditSession::find()->all();
        if(!empty($model)){
            foreach ($model as $key => $value) {

                $bs = BookedSession::findOne($value->booked_session_uuid);
                if(!empty($bs)){
                    $value->booking_uuid = $bs->booking_uuid;
                    $value->save(false);
                }
            }
        }
        echo "<pre>";
        $model = \app\models\BookedSession::find()->where('credit_session_uuid IS NOT NULL')->all();
        if(!empty($model)){
            foreach ($model as $key => $value) {
                $bs = \app\models\CreditSession::findOne($value->credit_session_uuid);
                if(!empty($bs)){
                    $value->booking_uuid = $bs->booking_uuid;
                    $value->save(false);
                }
              echo "uuid-> ".$value->uuid ."  cs_uuid-> ". $value->credit_session_uuid." \n";

            }
        } exit;
    }*/

    public function actionCancelLessonRequest($id) {
        if (($modelBS = BookedSession::find()->joinWith(['studentUu','tutorUu'])->where(['booked_session.uuid' => $id])->one()) !== null) {
          if(!$modelBS->is_cancelled_request == 1){

            $model = new CancelSession();
            if(in_array(Yii::$app->user->identity->role,["OWNER","ADMIN","SUBADMIN"])){
              $canceler_uuid = Yii::$app->user->identity->admin_uuid;
            } else if(in_array(Yii::$app->user->identity->role,["TUTOR"])){
              $canceler_uuid = Yii::$app->user->identity->tutor_uuid;
            }else{
              $canceler_uuid = Yii::$app->user->identity->student_uuid;
            }

            $model->booked_session_uuid = $modelBS->uuid;
            $model->student_uuid = $modelBS->student_uuid;
            $model->tutor_uuid = $modelBS->tutor_uuid;
            $model->status = 'AWAITING';
            $model->cancel_by = Yii::$app->user->identity->role;
            $model->canceler_uuid = $canceler_uuid;
            $model->created_at = Yii::$app->formatter->asDate(time(), 'php:Y-m-d H:i:s');
            if($model->save(false)){
              $modelBS->is_cancelled_request = 1;
              $modelBS->save(false);

              //send mail
              $content['student_name'] = $model->studentUu->first_name." ".$model->studentUu->last_name;
              $content['tutor_name'] = $model->tutorUu->first_name." ".$model->tutorUu->last_name;
              $content['from_date'] = General::convertTimezone($modelBS->start_datetime,Yii::$app->params['timezone'],Yii::$app->params['timezone'],'d-m-Y T'); 
                                    //General::displayDate($modelBS->start_datetime);
              $content['from_time'] = General::convertTimezone($modelBS->start_datetime,Yii::$app->params['timezone'],Yii::$app->params['timezone'],'h:i A')." - ".General::convertTimezone($modelBS->end_datetime,Yii::$app->params['timezone'],Yii::$app->params['timezone'],'h:i A');
                                    //General::displayTime($modelBS->start_datetime) . ' - ' . General::displayTime($modelBS->end_datetime);
              $ownersEmail = General::getOwnersEmails(['OWNER', 'ADMIN']);
              Yii::$app->mailer->compose('cancel_lesson_request_admin_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                                  ->setFrom(Yii::$app->params['supportEmail'])
                                  ->setTo($ownersEmail)
                                  ->setSubject('Lesson Cancellation Request-' . Yii::$app->name)
                                  ->send();
              Yii::$app->getSession()->setFlash('success', 'Cancel lesson request successfully saved.');
            } else{
                Yii::$app->getSession()->setFlash('danger', 'Request not save successfully.');
            }
          } else{
            Yii::$app->getSession()->setFlash('danger', 'Cancellation request already exist');
          }

        } else {
            Yii::$app->getSession()->setFlash('danger', 'Sorry lesson not found.');
        }
        
        if(Yii::$app->user->identity->role == "STUDENT"){
            return $this->redirect(['/student/home']);
        }else{
            return $this->redirect(['upcoming']);
        }
    }

    public function actionCancelLessonRequestNote($id) {
        $note = Yii::$app->getRequest()->post('note');
        if (!Yii::$app->request->isAjax) {
            Yii::$app->response->format = 'json';
            return ['code' => 505, 'message' => 'You are not authorised to access this page'];
        }
        if (($modelBS = BookedSession::find()->joinWith(['studentUu','tutorUu'])->where(['booked_session.uuid' => $id])->one()) !== null) {
            if(!$modelBS->is_cancelled_request == 1){

                $model = new CancelSession();
                if(in_array(Yii::$app->user->identity->role,["OWNER","ADMIN","SUBADMIN"])){
                  $canceler_uuid = Yii::$app->user->identity->admin_uuid;
                } else if(in_array(Yii::$app->user->identity->role,["TUTOR"])){
                  $canceler_uuid = Yii::$app->user->identity->tutor_uuid;
                }else{
                  $canceler_uuid = Yii::$app->user->identity->student_uuid;
                }

                $model->booked_session_uuid = $modelBS->uuid;
                $model->student_uuid = $modelBS->student_uuid;
                $model->tutor_uuid = $modelBS->tutor_uuid;
                $model->status = 'AWAITING';
                if(!empty($note)) {
                    $model->note = $note;
                } else{
                    $model->note = Yii::$app->user->identity->role. " Request To Cancelled Lesson.";
                }
                $model->cancel_by = Yii::$app->user->identity->role;
                $model->canceler_uuid = $canceler_uuid;
                $model->created_at = Yii::$app->formatter->asDate(time(), 'php:Y-m-d H:i:s');
                if($model->save(false)){
                    $modelBS->is_cancelled_request = 1;
                    $modelBS->save(false);

                    //send mail
                    $content['student_name'] = $model->studentUu->first_name;
                    $content['tutor_name'] = $model->tutorUu->first_name;
                    $content['from_date'] = General::displayDate($modelBS->start_datetime);
                    $content['from_time'] = General::displayTime($modelBS->start_datetime) . ' - ' . General::displayTime($modelBS->end_datetime);
                    $ownersEmail = General::getOwnersEmails(['OWNER']);
                    Yii::$app->mailer->compose('cancel_lesson_request_admin_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                        ->setFrom(Yii::$app->params['supportEmail'])
                        ->setTo($ownersEmail)
                        ->setSubject('Cancel Lesson Request-' . Yii::$app->name)
                        ->send();
                    $resp = ['code' => 200, 'message' => 'Cancel lesson request successfully saved.'];
                } else{
                    $resp = ['code' => 422, 'message' => 'Request not save successfully.'];
                }
            } else{
                $resp = ['code' => 422, 'message' => 'Cancellation request already exist.'];
            }
        } else {
            $resp = ['code' => 422, 'message' => 'Sorry lesson not found.'];
        }
        Yii::$app->response->format = 'json';
        return $resp;
    }

    public function actionCancelLessonApproval($id, $st) {
        if (!Yii::$app->request->isAjax) {
            Yii::$app->response->format = 'json';
            $resp = ['code' => 505, 'message' => 'You are not authorised to access this page'];
        }
       if (($modelBS = BookedSession::find()->joinWith(['studentUu','tutorUu','creditSessionUu'])->where(['booked_session.uuid' => $id, 'booked_session.status' => 'SCHEDULE','is_cancelled_request' => 1])->one()) !== null &&
           ($modelCS = CancelSession::find()->joinWith(['studentUu','tutorUu'])->where(['cancel_session.booked_session_uuid' => $id, 'cancel_session.status' => 'AWAITING'])->one()) !== null) {
            if($modelBS->is_cancelled_request == 1) {

                if($st == 'yes') {
                    $cancel_by = $approved_by = Yii::$app->user->identity->role;
                    $cancel_reason = $approved_by." APPROVE CANCELLED LESSON";
                    if(in_array(Yii::$app->user->identity->role,["OWNER","ADMIN","SUBADMIN"])){
                    $approver_uuid = Yii::$app->user->identity->admin_uuid;
                    } else if(in_array(Yii::$app->user->identity->role,["TUTOR"])){
                    $approver_uuid = Yii::$app->user->identity->tutor_uuid;
                    }else{
                    $approver_uuid = Yii::$app->user->identity->student_uuid;
                    }

                    $ownersEmail = General::getOwnersEmails(['OWNER']);
                    $student_tz = General::getStudentTimezone($modelBS->student_uuid);

                    $modelCS->approved_by = $approved_by;
                    $modelCS->approver_uuid = $approver_uuid;
                    $modelCS->approve_datetime = Yii::$app->formatter->asDate(time(), 'php:Y-m-d H:i:s');
                    $modelCS->cancel_note = $cancel_reason;
                    $modelCS->status = "APPROVED";
                    if($modelCS->save(false)) {
                        $modelBS->is_cancelled_request = 0;
                        $modelBS->cancel_by = $cancel_by;
                        $modelBS->cancel_reason = $cancel_reason;
                        $modelBS->status = "CANCELLED";
                        $modelBS->cancel_user_uuid = $approver_uuid;
                        $modelBS->cancel_datetime = Yii::$app->formatter->asDate(time(), 'php:Y-m-d H:i:s');
                        if($modelBS->save(false)) {
                            if(isset($modelBS['creditSessionUu']) && in_array($modelBS['creditSessionUu']['credited_by'],["OWNER","ADMIN","SUBADMIN"])) {
                                //send mail
                                $content['credited_musicoins'] = 0;
                                $content['student_name'] = $modelCS->studentUu->first_name;
                                $content['student_last_name'] = $modelCS->studentUu->last_name;
                                $content['tutor_name'] = $modelCS->tutorUu->first_name;
                                $content['tutor_last_name'] = $modelCS->tutorUu->last_name;
                                $content['from_date'] = General::convertTimezone($modelBS->start_datetime,Yii::$app->params['timezone'],$student_tz,'d-m-Y T'); 
                                $content['from_time'] = General::convertTimezone($modelBS->start_datetime,Yii::$app->params['timezone'],$student_tz,'h:i A')." - ".General::convertTimezone($modelBS->end_datetime,Yii::$app->params['timezone'],$student_tz,'h:i A');
                                //$content['from_date'] = General::displayDate($modelBS->start_datetime);
                                //$content['from_time'] = General::displayTime($modelBS->start_datetime) . ' - ' . General::displayTime($modelBS->end_datetime);

                                $ownersEmail = General::getOwnersEmails(['OWNER', 'ADMIN', ]);
                                $ToEmails = [$modelCS->studentUu->email, $modelCS->tutorUu->email];
                                Yii::$app->mailer->compose('cancel_lesson_request_approve_admin_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                                                 ->setFrom(Yii::$app->params['supportEmail'])
                                                 ->setTo($ToEmails)
                                                 ->setBcc($ownersEmail)
                                                 ->setSubject('Lesson Cancellation Request Approved -' . Yii::$app->name)
                                                 ->send();
                                $resp = ['code' => 200, 'message' => 'Cancel lesson request approved successfully.'];
                            } else {

				//credit musicoins
				$cr_coins = bcsub($modelBS->session_musicoins, $modelBS->session_discount_musicoins, 2);
				$cr_coins = ($cr_coins > 0 )?$cr_coins:0;
				$array_coins = [
				    'student_uuid' => $modelBS->student_uuid,
				    'coins' => $cr_coins,
				    'summary' => 'Credit Musicoin for the cancellation of the lesson',
				];
                		$result_credit_coins = \app\models\MusicoinLog::creditMusicoin($array_coins);
				//if($result_credit_coins['code'] == 200){
					//send mail
                                    $content['credited_musicoins'] = $cr_coins;
                                    $content['student_name'] = $modelCS->studentUu->first_name;
                                    $content['student_last_name'] = $modelCS->studentUu->last_name;
                                    $content['tutor_name'] = $modelCS->tutorUu->first_name;
                                    $content['tutor_last_name'] = $modelCS->tutorUu->last_name;
                                    $content['from_date'] = General::convertTimezone($modelBS->start_datetime,Yii::$app->params['timezone'],$student_tz,'d-m-Y T'); 
                                    $content['from_time'] = General::convertTimezone($modelBS->start_datetime,Yii::$app->params['timezone'],$student_tz,'h:i A')." - ".General::convertTimezone($modelBS->end_datetime,Yii::$app->params['timezone'],$student_tz,'h:i A');

                                    $ownersEmail = General::getOwnersEmails(['OWNER', 'ADMIN', ]);
                                    Yii::$app->mailer->compose('cancel_lesson_request_approve_admin_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                                                     ->setFrom(Yii::$app->params['supportEmail'])
                                                     ->setTo($modelCS->studentUu->email)
                                                     ->setBcc($ownersEmail)
                                                     ->setSubject('Lesson Cancellation Request Approved -' . Yii::$app->name)
                                                     ->send();
                                    
                                    Yii::$app->mailer->compose('cancel_lesson_request_approve_admin_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                                                    ->setFrom(Yii::$app->params['supportEmail'])
                                                    ->setTo($modelCS->tutorUu->email)
                                                    ->setBcc($ownersEmail)
                                                    ->setSubject('Lesson Cancellation Request Approved -' . Yii::$app->name)
                                                    ->send();


                                    $resp = ['code' => 200, 'message' => 'Cancel lesson request approved successfully.'];
                            }
                        } else {
                            $resp = ['code' => 422, 'message' => 'Booked session not successfully saved.'];
                        }
                    } else {
                        $resp = ['code' => 422, 'message' => 'Cancel lesson request not successfully saved.'];
                    }
                } else {
                    $rejected_by = Yii::$app->user->identity->role;
                    if(in_array(Yii::$app->user->identity->role,["OWNER","ADMIN","SUBADMIN"])){
                      $rejecter_uuid = Yii::$app->user->identity->admin_uuid;
                    } else if(in_array(Yii::$app->user->identity->role,["TUTOR"])){
                      $rejecter_uuid = Yii::$app->user->identity->tutor_uuid;
                    }else{
                      $rejecter_uuid = Yii::$app->user->identity->student_uuid;
                    }

                    $cancel_reason = $rejected_by." REJECTED CANCELLED LESSON";
                    $student_tz = General::getStudentTimezone($modelBS->student_uuid);

                    $modelCS->rejected_by = $rejected_by;
                    $modelCS->rejecter_uuid = $rejecter_uuid;
                    $modelCS->reject_datetime = Yii::$app->formatter->asDate(time(), 'php:Y-m-d H:i:s');
                    $modelCS->status = "REJECTED";
                    $modelCS->cancel_note = $cancel_reason;

                    if($modelCS->save(false)) {

                        $modelBS->is_cancelled_request = 0;

                        if($modelBS->save(false)) {
                            //send mail
                            $content['student_name'] = $modelCS->studentUu->first_name;
                            $content['student_last_name'] = $modelCS->studentUu->last_name;
                            $content['tutor_name'] = $modelCS->tutorUu->first_name;
                            $content['tutor_last_name'] = $modelCS->tutorUu->last_name;
                            $content['from_date'] = General::convertTimezone($modelBS->start_datetime,Yii::$app->params['timezone'],$student_tz,'d-m-Y T'); 
                            $content['from_time'] = General::convertTimezone($modelBS->start_datetime,Yii::$app->params['timezone'],$student_tz,'h:i A')." - ".General::convertTimezone($modelBS->end_datetime,Yii::$app->params['timezone'],$student_tz,'h:i A');
                            //$content['from_date'] = General::displayDate($modelBS->start_datetime);
                            //$content['from_time'] = General::displayTime($modelBS->start_datetime) . ' - ' . General::displayTime($modelBS->end_datetime);

                            $ToEmails = [$modelCS->studentUu->email, $modelCS->tutorUu->email];
                            Yii::$app->mailer->compose('cancel_lesson_request_reject_admin_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                                               ->setFrom(Yii::$app->params['supportEmail'])
                                               ->setTo($ToEmails)
                                               ->setSubject('Lesson Cancellation Request Rejected -' . Yii::$app->name)
                                               ->send();
                            $resp = ['code' => 200, 'message' => 'Cancel lesson request rejected successfully.'];
                        } else {
                            $resp = ['code' => 422, 'message' => 'Cancel lesson request not successfully saved.'];
                        }
                    } else {
                        $resp = ['code' => 422, 'message' => 'Booked session not successfully saved.'];
                    }
                }
            } else {
                $resp = ['code' => 422, 'message' => 'Cancellation approval request already exist'];
            }
       } else{
           $resp = ['code' => 404, 'message' => 'Sorry upcoming lesson not found.'];
       }
       Yii::$app->response->format = 'json';
       return $resp;
    }

    public function actionExpired() {
        $searchModel = new BookedSessionSearch();

        $order_by = "start_datetime desc";
        $statusIn = ['EXPIRED'];
        $expiredList = $searchModel->searchReport(Yii::$app->request->queryParams, true,'',$statusIn,$order_by);

        if(Yii::$app->user->identity->role == 'TUTOR'){
            $searchModel->tutor_uuid = Yii::$app->user->identity->tutor_uuid;
            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('tutor_expired', [
                    'expiredList' => $expiredList,
                ]);
            } else {
                return $this->render('expired_index', [
                    'expiredList' => $expiredList,
                    'searchModel' => $searchModel,
                ]);
            }
        }else{
            
            $this->layout = "student_layout";
            
            $searchModel->student_uuid = Yii::$app->user->identity->student_uuid;
            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('student_expired', [
                    'expiredList' => $expiredList,
                ]);
            } else {
                return $this->render('expired_index_student', [
                    'expiredList' => $expiredList,
                    'searchModel' => $searchModel,
                ]);
            }
        }
    }
    
    // STUDENT CANCEL > student reschedule request
    public function actionStudentRescheduleLessonRequestCancel($id) {

        if (!Yii::$app->request->isAjax) {
            throw new \yii\web\HttpException(505, 'You are not authorised to access this page');
        }
        if (($modelRS = RescheduleSession::find()->where(['booked_session_uuid' => $id])->andWhere(['status' => 'AWAITING'])->one()) !== null) {
            if(!empty(Yii::$app->getRequest()->post('is_cancel'))){

                $is_cancel = Yii::$app->getRequest()->post('is_cancel');

                $return = ['code' => 421, 'message' => 'Something missing.'];
                $status = ($is_cancel == "yes") ? "CANCELLED" : "AWAITING";

                $canceler_uuid = Yii::$app->user->identity->student_uuid;
                $modelBS = BookedSession::find()->where(['booked_session.uuid' => $modelRS->booked_session_uuid])->one();
                $modelRS->request_cancel_by = Yii::$app->user->identity->role;
                $modelRS->request_canceler_uuid = $canceler_uuid;
                $modelRS->request_cancel_datetime = Yii::$app->formatter->asDate(time(), 'php:Y-m-d H:i:s');
                $modelRS->status = $status;
                if($modelRS->save(false)){
                    $modelBS->is_reschedule_request = 0;
                    $modelBS->save(false);
                    $return = ['code' => 200, 'message' => 'Reschedule Lesson Request Cancel successfully.'];
                } else{
                    $return = ['code' => 421, 'message' => 'Reschedule Lesson Request Cancel not save successfully.'];
                }
            } else{
                $return = ['code' => 421, 'message' => 'Reschedule Lesson Request Cancel already exist.'];
            }
        } else {
            $return = ['code' => 421, 'message' => 'Sorry lesson not found.'];
        }
        Yii::$app->response->format = 'json';
        return $return;
    }

    // STUDENT approve or reject tutor reschedule request
    public function actionRescheduleRequestAction($id) {
        if (!Yii::$app->request->isAjax) {
            throw new \yii\web\HttpException(505, 'You are not authorised to access this page');
        }
        $reqAction = Yii::$app->getRequest()->post('action');

        if (empty($reqAction)) {
            $return = ['code' => 406, 'message' => 'Unknown Request Body'];
            Yii::$app->response->format = 'json';
            return $return;
        }
        $model = RescheduleSession::find()->where(['uuid' => $id, 'status' => 'AWAITING'])->one();
        if(!empty($model)) {
            $modelUpdateBS = BookedSession::find()->joinWith(['studentUu', 'instrumentUu', 'tutorUu'])->where(['booked_session.uuid' => $model->booked_session_uuid])->one();
            $status = ($reqAction == "approve") ? "APPROVED" : "REJECTED";

            $model->approved_by = Yii::$app->user->identity->role;
            $model->approver_uuid = Yii::$app->user->identity->student_uuid;
            $model->status = $status;

            if ($status == "APPROVED") {
                $modelUpdateBS->start_datetime = $model->to_start_datetime;
                $modelUpdateBS->end_datetime = $model->to_end_datetime;
                $modelUpdateBS->is_reschedule_request = 0;
                $modelUpdateBS->save(false);
                $model->approve_datetime = date('Y-m-d H:i:s');
            } else if ($status == "REJECTED") {
                $modelUpdateBS->is_reschedule_request = 0;
                $modelUpdateBS->save(false);
                $model->reject_datetime = date('Y-m-d H:i:s');
            } else {
                $return = ['code' => 406, 'message' => 'Invalid request action'];
                Yii::$app->response->format = 'json';
                return $return;
            }

            if ($model->save(false)) {
                // MAIL
                $from_start_datetime = $modelUpdateBS->start_datetime; 
                $from_end_datetime = $modelUpdateBS->end_datetime; 
                $student_timezone = General::getStudentTimezone($modelUpdateBS->student_uuid);
                $tutor_timezone = General::getTutorTimezone($modelUpdateBS->tutor_uuid);

                $content['student_name'] = $modelUpdateBS->studentUu->first_name;
                $content['student_last_name'] = $modelUpdateBS->studentUu->last_name;
                $content['tutor_name'] = $modelUpdateBS->tutorUu->first_name . ' ' . $modelUpdateBS->tutorUu->last_name;
                $content['tutor_last_name'] = $modelUpdateBS->tutorUu->last_name;
                $content['reschedule_number'] = $model->referance_number;
                $content['status'] = strtolower($status);
                $content['instrument_name'] = $modelUpdateBS->instrumentUu->name;
                $content['from_start_datetime'] = $from_start_datetime;
                $content['from_end_datetime'] = $from_end_datetime;
                $content['to_start_datetime'] = $model->to_start_datetime;
                $content['to_end_datetime'] = $model->to_end_datetime;
                $content['student_timezone'] = $student_timezone;
                $content['tutor_timezone'] = $tutor_timezone;
                
                Yii::$app->mailer->compose('reschedule_lesson_request_approved_rejected_student_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                        ->setFrom(Yii::$app->params['supportEmail'])
                        ->setTo($modelUpdateBS->studentUu->email)
                        ->setSubject(ucfirst(strtolower(Yii::$app->name)) . ' Notification - Your booking change request has been ' . $content['status'] . ' by your tutor: #' . $model->referance_number)
                        ->send();

                Yii::$app->mailer->compose('reschedule_lesson_request_approved_rejected_tutor_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                        ->setFrom(Yii::$app->params['supportEmail'])
                        ->setTo($modelUpdateBS->tutorUu->email)
                        ->setSubject(ucfirst(strtolower(Yii::$app->name)) . ' Notification - Student booking change request has been ' . $content['status'] . ' by you: #' . $model->referance_number)
                        ->send();

                Yii::$app->mailer->compose('reschedule_lesson_request_approved_rejected_admin_mail', ['content' => $content], ['htmlLayout' => 'layouts/html'])
                        ->setFrom(Yii::$app->params['supportEmail'])
                        ->setTo(Yii::$app->params['adminEmail'])
                        ->setSubject(ucfirst(strtolower(Yii::$app->name)) . ' Notification - Student booking change request has been ' . $content['status'] . ' by the tutor: #' . $model->referance_number)
                        ->send();


                $return = ['code' => 200, 'message' => "Reschedule lesson request marked as $status"];
            } else {
                $return = ['code' => 501, 'message' => "Failed to update reschedule lesson request"];
            }
            Yii::$app->response->format = 'json';
            return $return;
        } else {
            $return = ['code' => 406, 'message' => 'Unknown request body'];
            Yii::$app->response->format = 'json';
            return $return;
        }
    }
    public function actionStudentCancelLessonRequestCancel($id) {

        if (!Yii::$app->request->isAjax) {
            throw new \yii\web\HttpException(505, 'You are not authorised to access this page');
        }
        if (($modelCS = CancelSession::find()->where(['booked_session_uuid' => $id])->andWhere(['status' => 'AWAITING'])->one()) !== null) {
            if(!empty(Yii::$app->getRequest()->post('is_cancel'))){

                $is_cancel = Yii::$app->getRequest()->post('is_cancel');

                $return = ['code' => 421, 'message' => 'Something missing.'];
                $status = ($is_cancel == "yes") ? "CANCELLED" : "AWAITING";

                $canceler_uuid = Yii::$app->user->identity->student_uuid;
                $modelBS = BookedSession::find()->where(['booked_session.uuid' => $modelCS->booked_session_uuid])->one();
                $modelCS->request_cancel_by = Yii::$app->user->identity->role;
                $modelCS->request_canceler_uuid = $canceler_uuid;
                $modelCS->request_cancel_datetime = Yii::$app->formatter->asDate(time(), 'php:Y-m-d H:i:s');
                $modelCS->status = $status;
                if($modelCS->save(false)){
                    $modelBS->is_cancelled_request = 0;
                    $modelBS->save(false);
                    $return = ['code' => 200, 'message' => 'Cancel Lesson Request Cancel successfully.'];
                } else{
                    $return = ['code' => 421, 'message' => 'Cancel Lesson Request Cancel not save successfully.'];
                }
            } else{
                $return = ['code' => 421, 'message' => 'Cancel Lesson Request Cancel already exist.'];
            }
        } else {
            $return = ['code' => 421, 'message' => 'Sorry lesson not found.'];
        }
        Yii::$app->response->format = 'json';
        return $return;
    }
    
    public function actionMyLessons() {

        $this->layout = "student_layout";
        
        $statusIn = ['SCHEDULE','INPROCESS'];
        $upcomingList = BookedSession::find()
                ->joinWith(['studentUu','tutorUu','studentBookingUu','creditSessionUu'])
                ->where(['booked_session.student_uuid' => Yii::$app->user->identity->student_uuid])
                ->andWhere(['in', 'booked_session.status', $statusIn])
                ->asArray()->orderBy('booked_session.start_datetime asc')->limit(3)->all();
        
        $completedList = BookedSession::find()
                ->joinWith(['studentUu','tutorUu','studentBookingUu','creditSessionUu'])
                ->where(['booked_session.student_uuid' => Yii::$app->user->identity->student_uuid])
                ->andWhere(['in', 'booked_session.status', ['COMPLETED','PUBLISHED']])
                ->asArray()->orderBy('booked_session.start_datetime desc')->limit(3)->all();
        
        $cancelledList = BookedSession::find()
                ->joinWith(['studentUu','tutorUu','studentBookingUu','creditSessionUu'])
                ->where(['booked_session.student_uuid' => Yii::$app->user->identity->student_uuid])
                ->andWhere(['in', 'booked_session.status', ['CANCELLED']])
                ->asArray()->orderBy('booked_session.start_datetime desc')->limit(3)->all();
        
        
        $group_by = ['credit_session.uuid','tutorial_type_code'];
        $creditedList = CreditSession::find()
                ->where(['student_uuid' => Yii::$app->user->identity->student_uuid])
                ->andWhere(['in', 'status', ['PENDING']])
                ->asArray()
                ->orderBy('')
                ->groupBy($group_by)
                ->limit(3)->all();

//        echo "<pre>";
//        print_r($creditedList);
//        exit;

        return $this->render('student_my_lessons', [                    
                    'upcomingList' => $upcomingList,
                    'completedList' => $completedList,
                    'cancelledList' => $cancelledList,
                    'creditedList' => $creditedList,
        ]);
    }
}
