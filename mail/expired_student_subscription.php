<?php

use yii\helpers\Html;
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['student_name']; ?>,</p>

            <p>Your subscription to Musiconn's online content library has now expired.</p>

	        <p><strong>Package Name:</strong> <?= $content['name']; ?></p>
            <p><strong>Price:</strong> <?= $content['price']; ?></p>
            <p><strong>Tax:</strong> <?= $content['tax']; ?></p>
            <p><strong>Total Price:</strong> <?= $content['total_price']; ?></p>

            <p>Please follow this <a href="https://app.musiconn.com.au/">link</a> to renew your subscription and continue accessing our wide range of quality online content.</p>
            <p>Please do not reply to this unattended mailbox.  Any queries can be directed to admin@musiconn.com.au, or 1300 068 742.</p>

            <p>Regards,<br>
            Admin Team – Musiconn Pty Ltd</p>
        </td>
    </tr>
</table>

