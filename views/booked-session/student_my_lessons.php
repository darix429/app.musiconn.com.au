<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;
use app\models\Tutor;
use app\models\BookedSession;
use app\libraries\General;
use app\models\CreditSession;

$s_c_count = \app\models\CreditSession::getStudentCreditSessionCount(Yii::$app->user->identity->student_uuid);

$this->title = Yii::t('app', 'MY LESSONS');
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_icon'] = 'my-lesson-tag.png';
$this->params['page_title_custom'] = '<div class="col-md-12"><div class="col-md-7">
                        <h1>
                            <img src="' . Yii::$app->request->baseUrl . '/theme_assets/new/img/my-lesson-tag.png' . '">MY LESSONS 
                        </h1>
                    </div>
                    <div class="col-md-2">
                        <span class="lesson-credits pull-left" type="">Lesson Credits = '.$s_c_count.'</span>
                    </div>
                    <div class="col-md-3">
                        <button type="button" id="studentBookingOption_button" class="btn btn-primary btn-sm black_button_in pull-right" data-creditsession="'.$s_c_count.'">SCHEDULE A NEW LESSON</button>
                        
                    </div></div>';


$this->registerJs(
        '
$(document).on("click",".session_cancel_button",function(){
    if(confirm("Are you sure, you want to cancel this Lesson?")){
        window.location.href= $(this).data("url");
        blockBody();
    } else {
        unblockBody();
        return false;
    }
}); 

function cancel_lesson_request_cancel(id){
    if(confirm("Are you sure, you want to cancel this lesson cancelled request?")){
        $.ajax({
            type: "POST",
            url: "' . Url::to(['booked-session/student-cancel-lesson-request-cancel']) . '?id="+id,
            data: {"is_cancel": "yes"},
            beforeSend: function() {
                blockBody();
            },
            success:function(result){
                
                if(result.code == 200){
                    window.location.href= "' . Url::to(['/booked-session/my-lessons/']) . '";
                    showSuccess(result.message);
                }
                unblockBody();
            },
            error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
        blockBody();
    } else {
        unblockBody();
        return false;
    }

}

setInterval(function() {
	array = [];
	value = [];
	var d = new Date();
	var current_timestamp = Math.floor(d / 1000);
	d.setMinutes(d.getMinutes() + 5);
	var after_timestamp = Math.floor(d / 1000);

	$(".tution_tr").each(function(item){
		var uuid = array["uuid"] = $(this).attr("data-uuid");
		array["start_time"] = $(this).attr("data-stime");
		array["end_time"] = $(this).attr("data-etime");
		value.push(array);
		if(after_timestamp >= array["start_time"] && current_timestamp <= array["end_time"]) {
			$("#startbutton_"+uuid).css({display:"inline"});
			$("#reschedulebutton_"+uuid).css({display:"none"});
			$("#cancelbutton_"+uuid).css({display:"none"});
		} else {
			$("#startbutton_"+uuid).css({display:"none"});
			$("#reschedulebutton_"+uuid).css({display:"inline"});
			$("#cancelbutton_"+uuid).css({display:"inline"});
			if(current_timestamp >= array["end_time"]) {
				$(".tr_"+uuid).replaceWith("");
			}
		}
		
	});
}, 500 * 60 * 1);
        
', View::POS_END
);
?>
<style>
    .row_hr_line {
        border-bottom: 2px solid #d2d5de;
        margin: 30px 0px 10px 0px;
        display: inline-block;
        width: 100%;
    }
</style>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <h4>UPCOMING LESSONS</h4>
            <?php if (!empty($upcomingList)) { ?>

                <?php
                foreach ($upcomingList as $key => $value) {
                    $now = date('Y-m-d h:i A');
                    $timestamp = strtotime($now);
                    $timestamp_after_5 = date('Y-m-d h:i A', strtotime('+5 minutes', strtotime($now)));
                    ?>

                    <div class="box box-primary tution_tr tr_<?= $value['uuid']; ?> " data-stime="<?= strtotime($value['start_datetime']); ?>" data-etime="<?= strtotime($value['end_datetime']); ?>" data-uuid="<?= $value['uuid']; ?>">
                        <div class="box-header with-border">
                            <ul class="products-list product-list-in-box">
                                <li class="item">
                                    <div class="product-img">
                                        <img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new/' ?>img/music-1.png">
                                    </div>
                                    <div class="product-info">
                                        <a href="#" class="product-title">
                                            <?= \app\libraries\General::convertSystemToUserTimezone($value['start_datetime'], "l,d F Y"); ?>
                                            <span class="label pull-right">at <?= \app\libraries\General::convertSystemToUserTimezone($value['start_datetime'], "h:ia"); ?> - <?= \app\libraries\General::convertSystemToUserTimezone($value['end_datetime'], "h:ia"); ?></span>
                                        </a>
                                        <span class="product-description">
                                            with <?= $value['tutorUu']['first_name'] . ' ' . $value['tutorUu']['last_name']; ?>
                                            <!--<p class="pull-right">
                                                Pin is <?php //echo $value['session_pin']; ?>
                                            </p>-->
                                        </span>

                                        <div class="clearfix"></div>




                                        <?php
                                        if ($value['status'] == "SCHEDULE" || $value['status'] == "INPROCESS") {

                                            $enabled = (strtotime($timestamp_after_5) >= strtotime($value['start_datetime']) && $timestamp <= strtotime($value['end_datetime'])) ? "" : "disabled='disabled'";
                                            $enabled_class = (strtotime($timestamp_after_5) >= strtotime($value['start_datetime']) && $timestamp <= strtotime($value['end_datetime'])) ? "btn-primary" : "btn-default";
                                            $button_text = ( (strtotime($timestamp_after_5) >= strtotime($value['start_datetime']) && $timestamp <= strtotime($value['end_datetime'])) ) ? "JOIN" : "START";
//                                                                        if ($value['student_login_flag'] == 1 && $value['tutor_login_flag'] == 1 && $value['status'] == "INPROCESS") {
                                            /* if ($value['student_login_flag'] == 1 && $value['status'] == "INPROCESS") {
                                              //echo ucfirst($value['status']);?>
                                              <button class="btn btn-xs <?= $enabled_class?> session_start_button" <?= $enabled ?> data-uuid="<?= $value['uuid'] ?>" id="startbutton_<?= $value['uuid'] ?>" data-url="<?= BookedSession::getStartSessionLink($value['uuid'], 'student'); ?>"><?= $button_text; ?></button>
                                              <?php } else {
                                              ?>
                                              <button class="btn btn-xs <?= $enabled_class?> session_start_button" <?= $enabled ?> data-uuid="<?= $value['uuid'] ?>" id="startbutton_<?= $value['uuid'] ?>" data-url="<?= BookedSession::getStartSessionLink($value['uuid'], 'student'); ?>"><?= $button_text; ?></button>
                                              <?php
                                              } */
                                            ?>
                                            <?php
                                            if (strtotime($timestamp_after_5) >= strtotime($value['start_datetime']) && $timestamp <= strtotime($value['end_datetime'])) {
                                                $start_display = "display:inline";
                                                $reschedule_display = "display:none";
                                                $cancel_display = "display:none";
                                            } else {
                                                $start_display = "display:none";
                                                $reschedule_display = "display:inline";
                                                $cancel_display = "display:inline";
                                            }
                                            ?>
                                            <span class="product-description ">
                                                <div class="pull-right">
                                                    <a class="label label-warning pull-right session_start_button" style="<?= $start_display; ?>" data-uuid="<?= $value['uuid'] ?>" id="startbutton_<?= $value['uuid'] ?>" data-url="<?= BookedSession::getStartSessionLink($value['uuid'], 'student'); ?>"><?= $button_text; ?></a>
                                                    <?php if ($value['start_datetime'] > date('Y-m-d H:i:s')) {  //echo $value['start_datetime'];  ?>
                                                        <?php if ($value['is_cancelled_request'] == 1) { ?>
                                                            <a class="label label-warning cancel_lesson_popup_btn" title="Pending Cancel Approval" data-uuid="<?= $value['uuid'] ?>" onclick="cancel_lesson_request_cancel('<?= $value['uuid'] ?>');">Lesson Cancellation is awaiting tutor's approval</a>
                                                        <?php } else { ?>
                                                            <a class="label label-warning session_cancel_button" style="<?= $cancel_display; ?>" title="Cancel this lesson" data-url="<?= Url::to(['/booked-session/cancel-lesson-request', 'id' => $value['uuid']]) ?>" id="cancelbutton_<?= $value['uuid'] ?>" >CANCEL</a>
                                                        <?php } ?>
                                                        <?php /* <a class="label label-warning <?= ($value['is_reschedule_request'] == 1) ? "btn-info" : "btn-warning" ?> session_reschedule_button" style="<?= $reschedule_display; ?>" title="<?= ($value['is_reschedule_request'] == 1) ? "Pending Reschedule Approval" : "Reschedule this lesson"; ?>" href="<?= Url::to(['/book-music-tution/reschedule-request', 'id' => $value['uuid']]) ?>" id="reschedulebutton_<?= $value['uuid'] ?>" ><?= ($value['is_reschedule_request'] == 1) ? "Request to reschedule lesson is awaiting tutor's approval" : "RESCHEDULE" ?></a> */ ?>
                                                        <a class="label label-warning <?= ($value['is_reschedule_request'] == 1) ? "btn-info" : "btn-warning" ?> session_reschedule_button" style="<?= $reschedule_display; ?>" title="<?= ($value['is_reschedule_request'] == 1) ? "Pending Reschedule Approval" : "Reschedule this lesson"; ?>" href="<?= Url::to(['/book-music-tution/reschedule-request', 'id' => $value['uuid']]) ?>" id="reschedulebutton_<?= $value['uuid'] ?>" ><?= ($value['is_reschedule_request'] == 1) ? "Pending reschedule approval" : "RESCHEDULE" ?></a>
                                                    <?php } ?>
                                                </div>
                                            </span>
                                            <?php
                                        } else {
                                            echo ucfirst($value['status']);
                                        }
                                        ?>

                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                <?php } ?>
            <?php } else { ?>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <ul class="products-list product-list-in-box">
                            <li class="item">
                                <div class="product-img">
                                    <img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new/' ?>img/my-lesson-tag.png">
                                </div>
                                <div class="product-info">
                                    <a href="#" class="product-title">No upcoming lessons.</a>                                        
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            <?php } ?>
            <a href="<?= Url::to(['/booked-session/upcoming/']) ?>" class=" pull-right"><div class="change_pass">SEE MORE</div></a>
        </div>

        <div class="col-md-6">
            <h4>COMPLETED LESSONS</h4>
            <?php if (!empty($completedList)) { ?>

                <?php foreach ($completedList as $key => $value) { ?>

                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <ul class="products-list product-list-in-box">
                                <li class="item">
                                    <div class="product-img">
                                        <img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new/' ?>img/music-1.png">
                                    </div>
                                    <div class="product-info">
                                        <a href="#" class="product-title">
                                            <?= \app\libraries\General::convertSystemToUserTimezone($value['start_datetime'], "l,d F Y"); ?>
                                            <span class="label pull-right">at <?= \app\libraries\General::convertSystemToUserTimezone($value['start_datetime'], "h:ia"); ?> - <?= \app\libraries\General::convertSystemToUserTimezone($value['end_datetime'], "h:ia"); ?></span>
                                        </a>
                                        <span class="product-description">
                                            with <?= $value['tutorUu']['first_name'] . ' ' . $value['tutorUu']['last_name']; ?>

                                            <?php
                                            if ($value['status'] == 'COMPLETED') {
                                                echo "Under Process";
                                            }
                                            if ($value['status'] == 'PUBLISHED' && $value['studentUu']['isMonthlySubscription']) {
                                                ?>
                                                <a href="<?= Url::to(['video/tution-view', 'id' => $value['uuid']], true); ?>" class="label label-warning pull-right">VIEW RECORDING</a>
                                                <?php
                                            } elseif (!$value['studentUu']['isMonthlySubscription']) {
                                                ?>
                                                <a href="<?= Url::to(['monthly-subscription/index', 'id' => $value['uuid']], true); ?>" class="label label-warning pull-right">BUY SUBCRIPTION</a>
                                                <?php
                                            }
                                            ?>
                                        </span>

                                        <span class="product-description">
                                        </span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                <?php } ?>
            <?php } else { ?>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <ul class="products-list product-list-in-box">
                            <li class="item">
                                <div class="product-img">
                                    <img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new/' ?>img/my-lesson-tag.png">
                                </div>
                                <div class="product-info">
                                    <a href="#" class="product-title">No completed lessons.</a>                                        
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            <?php } ?>
            <a href="<?= Url::to(['/booked-session/completed/']) ?>" class="pull-right"><div class="change_pass">SEE MORE</div></a>
        </div>



    </div>
    <div class="row_hr_line"></div>
    <div class="row">
        <div class="col-md-6">
            <h4>CANCELLED LESSONS</h4>
            <?php if (!empty($cancelledList)) { ?>

                <?php
                foreach ($cancelledList as $key => $value) {
                    ?>

                    <div class="box box-primary ">
                        <div class="box-header with-border">
                            <ul class="products-list product-list-in-box">
                                <li class="item">
                                    <div class="product-img">
                                        <img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new/' ?>img/music-1.png">
                                    </div>
                                    <div class="product-info">
                                        <a href="#" class="product-title">
                                            <?= \app\libraries\General::convertSystemToUserTimezone($value['start_datetime'], "l,d F Y"); ?>
                                            <span class="label pull-right">at <?= \app\libraries\General::convertSystemToUserTimezone($value['start_datetime'], "h:ia"); ?> - <?= \app\libraries\General::convertSystemToUserTimezone($value['end_datetime'], "h:ia"); ?></span>
                                        </a>
                                        <span class="product-description">
                                            with <?= $value['tutorUu']['first_name'] . ' ' . $value['tutorUu']['last_name']; ?>
                                        </span>
                                        <span class="label pull-right">
                                            Cancelled on <?= \app\libraries\General::convertSystemToUserTimezone($value['cancel_datetime'], "d F Y"); ?>
                                        </span>
                                        <div class="clearfix"></div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                <?php } ?>
            <?php } else { ?>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <ul class="products-list product-list-in-box">
                            <li class="item">
                                <div class="product-img">
                                    <img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new/' ?>img/my-lesson-tag.png">
                                </div>
                                <div class="product-info">
                                    <a href="#" class="product-title">No cancelled lessons.</a>                                        
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            <?php } ?>
            <a href="<?= Url::to(['/booked-session/cancelled/']) ?>" class="pull-right"><div class="change_pass">SEE MORE</div></a>
        </div>
        <div class="col-md-6">
            <h4>CREDITED LESSONS</h4>
            <?php if (!empty($creditedList)) { 

                $all_type_code_array = array_column($creditedList,'tutorial_type_code');
                $type_code_array = array_count_values($all_type_code_array);
               
                if (!empty($type_code_array)) {
                    $row = 0;
                    foreach ($type_code_array as $key => $value) {
                        
                        $TutorialType = \app\models\TutorialType::find()->where(['code' => $key])->one();
                        ?>
                       

                        <div class="box box-primary ">
                            <div class="box-header with-border">
                                <ul class="products-list product-list-in-box">
                                    <li class="item">
                                        <div class="product-img">
                                            <img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new/' ?>img/music-1.png">
                                        </div>
                                        <div class="product-info">
                                            <a href="#" class="product-title">
                                                <?= (!empty($TutorialType)) ? $TutorialType->description : $key; ?>
                                                <span class="label pull-right">Total <?= $value; ?></span>
                                            </a>
                                           

                                            <div class="clearfix"></div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>

            <?php } else { ?>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <ul class="products-list product-list-in-box">
                            <li class="item">
                                <div class="product-img">
                                    <img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new/' ?>img/my-lesson-tag.png">
                                </div>
                                <div class="product-info">
                                    <a href="#" class="product-title">No credited lessons.</a>                                        
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            <?php } ?>
            <a href="<?= Url::to(['/booked-session/credited/']) ?>" class="pull-right"><div class="change_pass">SEE MORE</div></a>
        </div>
    </div>
</section>