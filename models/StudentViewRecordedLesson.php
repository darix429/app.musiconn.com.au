<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "student_view_recorded_lesson".
 *
 * @property string $uuid
 * @property string $booked_session_uuid
 * @property int $part
 * @property string $student_uuid
 * @property string $tutor_uuid
 * @property string $instrument_uuid
 * @property string $datetime
 */
class StudentViewRecordedLesson extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'student_view_recorded_lesson';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'booked_session_uuid', 'part', 'student_uuid', 'tutor_uuid', 'instrument_uuid'], 'required'],
            [['uuid', 'booked_session_uuid', 'student_uuid', 'tutor_uuid', 'instrument_uuid'], 'string'],
            [['part'], 'default', 'value' => null],
            [['part'], 'integer'],
            [['datetime'], 'safe'],
            [['uuid'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'booked_session_uuid' => Yii::t('app', 'Booked Session Uuid'),
            'part' => Yii::t('app', 'Part'),
            'student_uuid' => Yii::t('app', 'Student Uuid'),
            'tutor_uuid' => Yii::t('app', 'Tutor Uuid'),
            'instrument_uuid' => Yii::t('app', 'Instrument Uuid'),
            'datetime' => Yii::t('app', 'Datetime'),
        ];
    }
    
      
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstrumentUu() {
        return $this->hasOne(Instrument::className(), ['uuid' => 'instrument_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentUu() {
        return $this->hasOne(Student::className(), ['student_uuid' => 'student_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTutorUu() {
        return $this->hasOne(Tutor::className(), ['uuid' => 'tutor_uuid']);
    }
}
