<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\ArrayHelper;
use app\models\StudentMonthlySubscription;
use app\models\StudentTuitionBooking;

$this->title = Yii::t('app', 'All Lessons History');
$this->params['breadcrumbs'][] = $this->title;

//$studentMonthlySubArray = ArrayHelper::map(StudentMonthlySubscription::find()->where(['status' => 'ENABLED'])->all(),'uuid','name');
$studentTuitionBookingArray = ArrayHelper::map(StudentTuitionBooking::find()->where(['status' => 'BOOKED'])->orderBy(['booking_id' => SORT_DESC])->all(), 'uuid', 'booking_id');
//print_r($studentTuitionBookingArray); exit;
$this->registerJs(
        '

$(document).on("click","#search_btn", function(){        
    $.ajax({
        type: "GET",
        url: "' . Url::to(['/booked-session/index/']) . '",
        data: $("#search_frm").serialize(),
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            $("#payment_transaction_table_parent_div").html(result);
            window.ULTRA_SETTINGS.dataTablesInit();
        },
        error:function(e){
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});

$(document).on("click","#search_reset_btn", function(){        
    $.ajax({
        type: "GET",
        url: "' . Url::to(['/booked-session/index/']) . '",
        data: {},
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            $("#payment_transaction_table_parent_div").html(result);
            window.ULTRA_SETTINGS.dataTablesInit();
        },
        error:function(e){
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});


  $(".datefilter").daterangepicker({
      autoUpdateInput: false,
      locale: {
          cancelLabel: "Clear",
          format: "DD-MM-YYYY"
      },ranges : {
                    "Today": [moment(), moment()],
                    "Yesterday": [moment().subtract("days", 1), moment().subtract("days", 1)],
                    "Last 7 Days": [moment().subtract("days", 6), moment()],
                    "Last 30 Days": [moment().subtract("days", 29), moment()],
                    "This Month": [moment().startOf("month"), moment().endOf("month")],
                    "Last Month": [moment().subtract("month", 1).startOf("month"), moment().subtract("month", 1).endOf("month")]
                }
  });

  $(".datefilter").on("apply.daterangepicker", function(ev, picker) {
      $(this).val(picker.startDate.format("DD-MM-YYYY") + " - " + picker.endDate.format("DD-MM-YYYY"));
  });

  $(".datefilter").on("cancel.daterangepicker", function(ev, picker) {
      $(this).val("");
  });

    $.fn.dataTable.moment("DD-MM-YYYY");
    
', View::POS_END
);
?>
<script type="text/javascript">

</script>
<div class="col-xl-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">All Lessons History</h2>
            <div class="actions panel_actions float-right">
                <i class="fa fa-search icon-primary icon-xs icon-orange mail_head_icon custom-toggle" data-togglediv="datatable-search-div" title="Advance Search"></i>

            </div>
        </header>
        <div class="content-body"> 
            <div class="row ">
                <div class="col-lg-12 col-md-12 col-12 ">
                    <div class="datatable-search-div " style="display: none;">

                        <?php
                        $form = ActiveForm::begin([
                                    'method' => 'get',
                                    'options' => [
                                        'id' => 'search_frm',
                                        'class' => 'form-inline'
                                    ],
                        ]);
                        ?>

                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'booking_uuid')->dropDownList($studentTuitionBookingArray, ['prompt' => 'Select Booking ID', 'class' => 'form-control myselect2', 'style' => 'width: 100%'])->label(false) ?>
                        </div>
                        <div class="col-md-3 m-0">
                            <?= $form->field($searchModel, 'cancellation_daterange')->textInput(['value' => '', 'class' => 'form-control datefilter', 'maxlength' => true, 'placeholder' => 'Cancellation Date', 'data-format' => 'DD-MM-YYYY'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'student_name')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Student'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'tutor_name')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Tutor'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'session_pin')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Session Pin', 'type' => 'number'])->label(false) ?>
                        </div><br><br>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'start_daterange')->textInput(['class' => 'form-control datefilter', 'maxlength' => true, 'placeholder' => 'Lesson Date'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'status')->dropDownList(['ALL' => 'ALL', 'SCHEDULE' => 'SCHEDULE', 'PUBLISHED' => 'PUBLISHED', 'INPROCESS' => 'INPROCESS', 'CANCELLED' => 'CANCELLED', 'EXPIRED' => 'EXPIRED'], ['class' => 'form-control', 'style' => 'width:100%'])->label(false) ?>
                        </div>

                        <?= Html::Button(Yii::t('app', 'Search'), ['class' => 'btn btn-primary mb-0', 'id' => 'search_btn', 'style' => 'margin-left: 17px;']) ?>
                        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-purple mb-0', 'id' => 'search_reset_btn']) ?>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
            <br>
            <div class=""></div>
            <div class="clearfix"></div>
            <div class="row" id="payment_transaction_table_parent_div">
                <?php echo Yii::$app->controller->renderPartial('_booked_session_list', ['bookedSessionList' => $bookedSessionList]); ?>
            </div>
        </div>
    </section> 
</div>
