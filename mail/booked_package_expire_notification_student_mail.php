<?php

use yii\helpers\Html;
//echo $content['name']; exit;
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['student_first_name'] ?>,</p>

            <p>Our records show that your current package of lessons with <?= $content['tutor_first_name'] ?> will finish up soon.  To continue your lessons with <?= $content['tutor_first_name'] ?>, please <a href="<?= Yii::$app->params['base_url']; ?>">login</a> to our booking system to book in further lessons!</p>
            
            <p>Please do not reply to this unattended mailbox.  Any queries or feedback can be directed to admin@musiconn.com.au, or call us on 1300 068 742.</p>
                
            <p>Regards,<br>
            Musiconn Booking System</p>
        </td>
    </tr>
</table>

