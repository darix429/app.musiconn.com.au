<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\TutorialType;
use app\models\PaymentTerm;
use app\models\OnlineTutorialPackageType;
?>
<div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">

    <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Name</th>
                <th align="right">Price</th>
                <th align="right">GST</th>
                <th align="right">Total Price</th>
                <th align="right">Coins</th>
                <th align="right">Bonus Coins</th>
                <th align="right">Bonus Coins Price</th>
                <th align="right">Total Coins</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>

        <tbody>
            <?php
            if (!empty($packageList)) {
                foreach ($packageList as $key => $value) {
                    
                    ?>
                    <tr>
                        <td><?= $key + 1; ?></td>
                        <td><?= $value['name']; ?></td>
                        <td align="right"><?= $value['price']; ?></td>
                        <td align="right"><?= $value['gst']; ?></td>
                        <td align="right"><?= $value['total_price']; ?></td>
                        <td align="right"><?= $value['coins']; ?></td>
                        <td align="right"><?= $value['bonus_coins']; ?></td>
                        <td align="right"><?= $value['bonus_coins_amount']; ?></td>
                        <td align="right"><?= $value['total_coins']; ?></td>
                        <td>
                            <?php
                            if ($value['status'] == 'ENABLED') {
                                $e_class = "badge-primary";
                                $pointer = 'style="cursor:pointer;color:#fff"';
                                $e_link_class = "tutorial_package_disabled_link";
                            } elseif ($value['status'] == 'DISABLED') {
                                $e_class = "badge-secondary";
                                $pointer = 'style="cursor:pointer;color:#fff"';
                                $e_link_class = "tutorial_package_enabled_link";
                            } else {
                                $e_class = "badge-danger";
                                $pointer = '';
                                $e_link_class = "";
                            }
                            ?>
                            <span class="badge badge-pill <?= $e_class ?> <?= in_array(Yii::$app->user->identity->role, ['OWNER','ADMIN']) ? $e_link_class : ''; ?>"  data-uuid="<?= $value['uuid']; ?>" <?= in_array(Yii::$app->user->identity->role, ['OWNER','ADMIN']) ? $pointer : ''; ?>><?= $value['status']; ?></span>
                        </td>

                        <td>
                            <?php if (in_array(Yii::$app->user->identity->role, ['OWNER', 'ADMIN'])) { ?>
                                <a href="<?= Url::to(['/musicoin-package/update/', 'id' => $value['uuid']]) ?>" class="badge badge-info badge-md" title="Update"><i class="fa fa-pencil"></i></a>
                            <?php } ?>
                            <?php if (in_array(Yii::$app->user->identity->role, ['OWNER'])) { ?>
                                <a href="javascript:void(0)" data-uuid="<?= $value['uuid']; ?>" class="delete_tutorial_package_link badge badge-danger badge-md" title="Delete"><i class="fa fa-trash"></i></a>
                                <?php } ?>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>

</div>
