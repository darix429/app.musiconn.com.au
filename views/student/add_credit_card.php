<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\helpers\Url;

$this->registerJs('
        $(document).bind("cut copy paste","#studentcreditcard-number",function(e){
            e.preventDefault();
        });  

$(".custom_expire_month_year").datepicker( {
    format: "yyyy-mm",
    viewMode: "months", 
    minViewMode: "months",
    startDate:new Date(),
    startView:2
});

    ');
?>
<script>
    function refreshCCForm() {
        $.ajax({
            type: "GET",
            url: "<?= Url::to(['student/credit-card']); ?>",
            success: function (result) {
                $("#creditcard_div").html(result);
                unblockBody();
            },
            error: function (e) {
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
    }

    //callback to handle the response from stripe
    function stripeResponseHandler(status, response) {
        if (response.error) {

            $("#payBtn").removeAttr("disabled");
            $(".payment-errors").html(response.error.message);
            unblockBody();
            showSuccess(response.error.message);
        } else {
            var form$ = $("#form-create-card");

            var token = response["id"];

            form$.append("<input type=\"hidden\" name=\"stripeToken\" value=\"" + token + "\" />");

            //form$.get(0).submit();

            //form submit start
            var data = $("#form-create-card").serialize();
            $.ajax({
                type: "POST",
                url: "<?= Url::to(['student/credit-card']); ?>",
                data: $("#form-create-card").serialize(),
                success: function (result) {
                    if (result.code == 200) {
                        showSuccess(result.message);
                        refreshCCForm();
                    } else {
                        showErrorMessage(result.message);
                        refreshCCForm();
                        //$("#creditcard_div").html(result);
                        
                    }
                    unblockBody();
                },
                error: function (e) {
                    unblockBody();
                    showErrorMessage(e.responseText);
                }
            });
            //form submit end

        }
    }
    function beforeValidation() {
        $("#form-create-card").find("input").each(//console.log()
                function (index) {
                    idnya = $(this).attr("id");
                    $("#form-create-card").yiiActiveForm("validateAttribute", idnya);
                    err = $(this).parent().has("has-error");
                    msg = $(this).parent().find(".help-block").html();
                    // alert(idnya+"=="+msg);
                    //unblockBody();
                }
        );
    }
    function submitform()
    {
        blockBody();
        beforeValidation();

        $("#payBtn").attr("disabled", "disabled");
        var dateText = $("#studentcreditcard-cc_month_year").val();
        var pieces = dateText.split("-");
        var year = (pieces[0] != undefined && pieces[0] > 0) ? pieces[0] : "1970";
        var month = (pieces[1] != undefined && pieces[1] > 0) ? pieces[1] : "00";

        var token = Stripe.createToken({
            number: $("#studentcreditcard-number").val(),
            cvc: $("#studentcreditcard-cvv").val(),
            exp_month: month,
            exp_year: year,
        }, stripeResponseHandler);
    }
    
    
</script>


<div class="modal-body">
    <div class="row vertical-center-box vertical-center-box-tablet" >
        <?php $form = ActiveForm::begin(['id' => 'form-create-card', 'action' => Url::to(['student/credit-card'])]); ?>

        <label class="cont_title_add">These credit card details will be used for subscription and lesson payments</label>

        <?=
                $form->field($modelCC, 'name', ['template' => '<span>{label}</span>{input}{error}'])
                ->textInput(['class' => 'form-control', 'maxlength' => true,])
                ->label($modelCC->getAttributeLabel('name') . '*')
        ?>

        <?=
                $form->field($modelCC, 'number', ['template' => '<span>{label}</span>{input}{error}', 'inputOptions' => ['autocomplete' => 'new-password']])
                ->textInput(['class' => 'credit-card-number form-control', 'maxlength' => true,])
                ->label($modelCC->getAttributeLabel('number'). '*')
        ?>

        <div class="padd_zero col-md-5">
            <?=
                    $form->field($modelCC, 'cvv', ['template' => '<span>{label}</span>{input}{error}', 'inputOptions' => ['autocomplete' => 'new-password']])
                    ->passwordInput(['class' => 'form-control', 'maxlength' => true,])
                    ->label($modelCC->getAttributeLabel('cvv') . '*')
            ?>
        </div>
        <div class="padd_zero col-md-5 pull-right">
            <?=
                    $form->field($modelCC, 'cc_month_year', ['template' => '<span>{label}</span>{input}{error}'])
                    ->textInput(['class' => 'form-control custom_expire_month_year', 'data-min-view-mode' => 'months', 'data-start-view' => '2', 'data-format' => 'yyyy-mm', 'maxlength' => true,])
                    ->label($modelCC->getAttributeLabel('cc_month_year') . '*')
            ?>
        </div>
        <div class="">
            <div class="col-xs-12">
                <div class="checkbox icheck">
                    <?php
                    echo $form->field($modelCC, 'term_condition_checkbox', ['template' => ' <label>{input} I agree to Musiconns <a data-toggle="modal" data-target="#terms_condition_model"><b>Terms and Conditions</b></a></label>{error} '])->checkbox([
                        'class' => 'skin-square-red float-left '], false);
                    ?>
                </div>
            </div>
        </div>
        <div class="padd_zero col-xs-12">
            <div class="spacer"></div>
            <div class="payment-errors text-danger"></div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<div class="modal-footer">
    <button type="button" id="payBtn" class="btn btn-primary" onclick="submitform()">Save</button>
</div>
