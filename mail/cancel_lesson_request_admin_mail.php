<?php

use yii\helpers\Html;

?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear Musiconn Admin,</p>

            <p>You have received a lesson cancellation request to review. Please approve or reject in the system.</p>

            <p>Details of the lesson cancellation request are as follows:</p>

            <p><strong>Student Name :</strong> <?= $content['student_name']; ?></p>
            <p><strong>Tutor Name :</strong> <?= $content['tutor_name']; ?></p>
            <p><strong>Lesson Date :</strong> <?= $content['from_date']; ?></p>
            <p><strong>Lesson Time :</strong> <?= $content['from_time']; ?></p>

            <p>Regards,<br>
            Admin Team – Musiconn Pty Ltd</p>
        </td>
    </tr>
</table>
