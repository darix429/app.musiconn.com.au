<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\StudentCreditCard;
use app\libraries\General;
use app\models\StudentNotification;

/* @var $this yii\web\View */
/* @var $model app\models\Admin */

$timezone = General::getAllTimeZones();
if($userModel->timezone == '') {
 $userModel->timezone = Yii::$app->params['timezone'];
}
$this->title = Yii::t('app', 'Profile');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile('https://js.stripe.com/v2/');
$this->registerJs('

//set your publishable key
Stripe.setPublishableKey("'. Yii::$app->params['stripe']['publishable_key'] .'");

    function refreshCPForm(){
        $.ajax({
            type: "GET",
            url:  "' . Url::to(['student/change-password']) . '",
            success:function(result){
                $("#changepassword_div").html(result);
            },
            error:function(e){
                showErrorMessage(e.responseText);
            }
        });
    }
    $(document).on("click","#password-change-btn",function(){
        $.ajax({
            type: "POST",
            url:  "' . Url::to(['student/change-password']) . '",
            data: $("#form-change").serialize(),
            success:function(result){
                if(result.code == 200){
                    showSuccess(result.message);
                    refreshCPForm();
                }else{
                    $("#changepassword_div").html(result);
                }
            },
            error:function(e){
                showErrorMessage(e.responseText);
            }
        });
    })
    $(".birthdate").datepicker({
        minViewMode:  0,
        format: "dd-mm-yyyy",
        /* startDate: getValue($this, "startDate", ""),
        endDate: getValue($this, "endDate", ""),*/
        /* daysOfWeekDisabled: getValue($this, "disabledDays", ""),*/
        startView: 2,
        autoclose:true
    });

    //delete card
            $(document).on("click",".delete_credit_card", function(){
                if(confirm("Are you sure, you want to delete this card ?")){
                var element = $(this);
                    $.ajax({
                        type: "POST",
                        url: "' . Url::to(['/monthly-subscription/delete/']) . '",
                        data: { "id": $(this).data("uuid")},
                        beforeSend: function() { blockBody(); },
                        success:function(result){
                            if(result.code == 200){

                                $("#creditcard_div").html(result);
                                refreshCCForm();
                            }else{
                                showErrorMessage(result.message);
                            }
                            unblockBody();
                        },
                        error:function(e){
                            unblockBody();
                            showErrorMessage(e.responseText);
                        }
                    });
                }
            });

    function refreshCCForm(){
        $.ajax({
            type: "GET",
            url:  "' . Url::to(['student/credit-card']) . '",
            success:function(result){
               $("#creditcard_div").html(result);
               ULTRA_SETTINGS.otherScripts();
            },
            error:function(e){
                showErrorMessage(e.responseText);
            }
        });
    }

    //add credit card
    $(document).on("click","#card-create-btn",function(){
    var data = $("#form-create-card").serialize();

        $.ajax({
            type: "POST",
            url:  "' . Url::to(['student/credit-card']) . '",
            data: $("#form-create-card").serialize(),
            success:function(result){
                if(result.code == 200){
                    showSuccess(result.message);
                    refreshCCForm();
                }else{
                    $("#creditcard_div").html(result);
                    ULTRA_SETTINGS.otherScripts();
                }
            },
            error:function(e){
                showErrorMessage(e.responseText);
            }
        });
    })



            //card number validation
            $(document).on("input","#studentcreditcard-number", function(){
                    match = (/(\d{0,16})[^]*((?:\.\d{0,0})?)/g).exec(this.value.replace(/[^\d.]/g, ""));
                    this.value = match[1] + match[2];
            });

            //card cvv validation
            $(document).on("input","#studentcreditcard-cvv", function(){
                    match = (/(\d{0,4})[^]*((?:\.\d{0,0})?)/g).exec(this.value.replace(/[^\d.]/g, ""));
                    this.value = match[1] + match[2];
            });

    $(document).on("click",".termination_account",function(){
        if(confirm("Are you sure, you want to delete your account it will cancelled all your bookings and monthly subscription if any?")){
            var id = $(".termination_account").data("id");
            var element = $(this);
            $.ajax({
                type: "GET",
                url:  "' . Url::to(['student/remove-account']) . '",
                data: { "id": id},
                beforeSend: function() { blockBody(); },
                success:function(result){
                    if(result.code == 200){
                        showSuccess(result.message);
                        location.reload();
                    }else{
                        showErrorMessage(result.message);
                    }
                    unblockBody();
                },
                error:function(e){
                    unblockBody();
                    showErrorMessage(e.responseText);
                }
            });
        }
    });

    $("#student-country").change(function(){
                if($(this).val() == "other"){
                    $(".state_dd").hide();
                    $(".state_text").show();
                }else{
                    $(".state_text").hide();
                    $(".state_dd").show();
                }
            })

    function refreshStudentNotificationForm(){
        $.ajax({
            type: "GET",
            url:  "' . Url::to(['student-notification/index']) . '",
            success:function(result){
                $("#student_notification_div").html(result);
                $(".student_uuid").val("'. Yii::$app->user->identity->student_uuid .'");
            },
            error:function(e){
                showErrorMessage(e.responseText);
            }
        });
    }

    //Get Student Notification
    $("#studentNotification").click(function() {
        refreshStudentNotificationForm();
    });

    //Add Student Notification
    $(document).on("click", "#StudentAddNotification", function() {
        $.ajax({
            type: "POST",
            url:  "' . Url::to(['student-notification/index']) . '",
            beforeSend: function() { blockBody(); },
            data: $("#dynamic-form").serialize(),
            success:function(result){
                unblockBody();
                if(result.code == 200) {
                    showSuccess(result.message);
                    refreshStudentNotificationForm();
                 } else {
                    $.each(result.message, function( key, val ){
                      $("#"+key).next().html(val);
                      $("#"+key).parent().addClass("has-error");
                    });
                 }
            },
            error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
    });

    $(document).on("click","#password-change-btn",function(){
        $.ajax({
            type: "POST",
            url:  "' . Url::to(['student/change-password']) . '",
            data: $("#form-change").serialize(),
            success:function(result){
                if(result.code == 200){
                    showSuccess(result.message);
                    refreshCPForm();
                }else{
                    $("#changepassword_div").html(result);
                }
            },
            error:function(e){
                showErrorMessage(e.responseText);
            }
        });
    })

');
?>
<!--?= Html::errorSummary($model)?-->
<!--?= print_r($model->getErrors());?-->
<div class="col-lg-12">
    <div class="row">
        <div class="col-md-3 col-sm-4 col-xs-12 ">
            <section class="box">
                <div class="content-body">
                    <div class="uprofile-image ">
                        <img src="/data/profile/profile.png" class="img-fluid user-profile-status-active">
                    </div>
                    <div class="uprofile-name">
                        <h3>
                            <a href="#"><?= $model->first_name . ' ' . $model->last_name; ?></a>
                            <!-- Available statuses: online, idle, busy, away and offline -->
                            <span class="uprofile-status online"></span>
                        </h3>
                        <p class="uprofile-title"><?= Yii::$app->user->identity->role; ?></p>
                    </div>
                    <ul class="list-group list-group-unbordered" style="font-size: 13px;">
                        <li class="list-group-item bg-gray-field" style="padding: 4px 10px;">
                            <b>Student ID</b><a class="pull-right"><?= $model->enrollment_id; ?></a>
                        </li>
                    </ul>
                </div>
            </section>
            <section class="box ">
                <header class="panel_header">
                    <h2 class="title float-left">About Me</h2>
                    <div class="actions panel_actions float-right">
                        <i class="box_toggle fa fa-chevron-down"></i>
                    </div>
                </header>
                <div class="content-body">
                    <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>
                    <p class="text-muted">
                        <?= $model->street; ?><br>
                        <?= $model->city; ?><br>
                        <?= $model->state; ?> <?= $model->postal_code; ?><br>
                        <?= $model->country; ?>
                    </p>
                </div>
            </section>
        </div>

        <div class="col-md-9 col-sm-8 col-xs-12">
            <section class="box">
                <!-- Horizontal - start -->
                <div class="content-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="nav nav-tabs primary"  id="myTab1" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" role="tab" href="#profile-1" data-toggle="tab">
                                        <i class="fa fa-user"></i> Profile
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="#change-password" role="tab" data-toggle="tab">
                                        <i class="fa fa-lock"></i> Change Password
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="#credit-card" role="tab" data-toggle="tab">
                                        <i class="fa fa-credit-card"></i> Credit Card
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="#notification" role="tab" data-toggle="tab" id="studentNotification">
                                        <i class="fa fa-bell-o"></i> Notification
                                    </a>
                                </li>
                            </ul>

                            <div class="tab-content primary" id="myTabContent1">
                                <div class="tab-pane fade show active" id="profile-1">
                                    <div>
                                        <?php $form = ActiveForm::begin(); ?>
                                        <div class="form-row">
                                            <div class="col-md-8 mb-0">
                                                <?= $form->field($userModel, 'email')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $userModel->getAttributeLabel('email')]) ?>
                                            </div>
                                            <div class="col-md-4 mb-0">
                                                <div class="form-group ">
                                                    <label class="control-label" ><?= $model->getAttributeLabel('created_at'); ?></label>
                                                    <span  class="form-control bg-gray-field" aria-required="true" aria-invalid="true"><?= \app\libraries\General::displayDate(date("Y-m-d", $model->created_at)); ?></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="col-md-6 mb-0">
                                                <strong>
                                                    <h5 class="title float-left page-title-custom">Personal Information</h5>
                                                </strong>
                                            </div>
                                        </div>

                                        <hr>

                                        <div class="form-row">
                                            <div class="col-md-6 mb-0">
                                                <?= $form->field($model, 'first_name')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('first_name')]) ?>
                                            </div>
                                            <div class="col-md-6 mb-0">
                                                <?= $form->field($model, 'last_name')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('last_name')]) ?>
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="col-md-6 mb-0">
                                                <?php echo $form->field($model, 'phone_number')->widget(\yii\widgets\MaskedInput::className(['plceholder' => '']), ['mask' => '9999 999 999', 'clientOptions' => ['removeMaskOnSubmit' => true],'options' => ['placeholder' => 'XXXX XXX XXX', 'class' => 'form-control']]) ?>
                                                <?php //echo $form->field($model, 'phone_number')->textInput(['maxlength' => 10,'class' => 'form-control', 'placeholder' => $model->getAttributeLabel('phone_number')]) ?>
                                            </div>
                                            <div class="col-md-6 mb-0">
                                                <?php $model->birthdate = \app\libraries\General::displayDate($model->birthdate);
                                                echo $form->field($model, 'birthdate')->textInput(['autofocus' => false, 'class' => 'form-control birthdate', 'placeholder' => "DD-MM-YYYY"]); ?>
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="col-md-6 mb-0">
                                                <strong>
                                                    <h5 class="title float-left page-title-custom">Address</h5>
                                                </strong>
                                            </div>
                                        </div>

                                        <hr>

                                        <div class="form-row">
                                            <div class="col-md-6 mb-0">
                                                <?= $form->field($model, 'street')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('street')]) ?>
                                            </div>
                                            <div class="col-md-6 mb-0">
                                                <?= $form->field($model, 'city')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('city')]) ?>
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <?php if($model->country == 'other'){
                                                $display_state_dd= 'none';
                                                $display_state_text= 'block';
                                            }else{
                                                $display_state_dd= 'block';
                                                $display_state_text= 'none';

                                            } ?>
                                            <div class="col-md-3 mb-0">
                                                <?= $form->field($model, 'country')->dropDownList(['AUS' => 'Australia','other' => 'Other'], ['class' => 'form-control', 'maxlength' => true]) ?>
                                            </div>
                                            <div class="col-md-3 mb-0 state_dd" style="display:<?= $display_state_dd;?>">
                                                <?= $form->field($model, 'state')->dropDownList(['New South Wales' => '	New South Wales', 'Queensland' => 'Queensland', 'South Australia' => 'South Australia', 'Tasmania' => 'Tasmania', 'Victoria' => 'Victoria', 'Western Australia' => 'Western Australia', 'Australian Capital Territory' => 'Australian Capital Territory', 'Northern Territory' => 'Northern Territory'], ['class' => 'form-control', 'maxlength' => true, 'prompt' => "Select " . $model->getAttributeLabel('state')]) ?>
                                            </div>
                                            <div class="col-md-3 mb-0 state_text" style="display:<?= $display_state_text;?>">
                                                <?= $form->field($model, 'state_text')->textInput(['autofocus' => false, 'class' => 'input form-control state_text', 'placeholder' => "State"]); ?>
                                            </div>
                                            <div class="col-md-3 mb-0">
                                                <?php echo $form->field($model, 'postal_code')->widget(\yii\widgets\MaskedInput::className(['plceholder' => '']), ['mask' => '9999', 'clientOptions' => ['removeMaskOnSubmit' => true], 'options' => ['placeholder' => 'xxxx', 'class' => 'form-control']]) ?>
                                            </div>
                                            <div class="col-md-3 mb-0">
                                                <?= $form->field($userModel, 'timezone')->dropDownList($timezone, ['class' => 'form-control myselect2', 'prompt' => 'Select Timezone', 'maxlength' => true]) ?>
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="col-md-12 mb-0">
                                                <?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
                                                <a class="btn btn-danger text-white float-right termination_account" title="Delete Account" data-id="<?php echo $model->student_uuid;?>">Delete Account</a>
                                            </div>
                                        </div>
                                        <?php ActiveForm::end(); ?>
                                    </div>

                                </div>
                                <div class="tab-pane fade" id="change-password">

                                    <div id="changepassword_div">
                                        <?php echo Yii::$app->controller->renderPartial('change-password', ['modelCP' => $modelCP]); ?>
                                    </div>

                                </div>
                                <div class="tab-pane fade" id="credit-card">

                                    <div id="creditcard_div">
                                        <?php
                                        $creditCard = StudentCreditCard::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid])->one();

                                        if (empty($creditCard)) {
                                            echo Yii::$app->controller->renderPartial('add_credit_card', ['modelCC' => $modelCC]);
                                        } else {
                                            echo Yii::$app->controller->renderPartial('update_credit_card', ['modelCC' => $modelCC, 'creditCard' => $creditCard]);
                                        }
                                        ?>
                                    </div>

                                </div>
                                <div class="tab-pane fade" id="notification">

                                    <div id="student_notification_div">
                                        
                                    </div>

                                </div>
                            </div>

                        </div>
                        <!--<br><div class="spacer"></div><div class="spacer"></div>-->
                    </div>
                    <!-- Horizontal - end -->
                </div>
            </section>

        </div>
    </div>
</div>
