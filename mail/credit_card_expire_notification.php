<?php

use yii\helpers\Html;

?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['student_first_name']. ' ' .$content['student_last_name']; ?>,</p>

            <p>According to our records, your registered credit card in our booking system has expired.</p>

	    <p><strong>Card Holder Name:</strong> <?= $content['name']; ?></p>
            <p><strong>Card Number:</strong> <?= $content['number']; ?></p>
            <p><strong>Expire Date:</strong> <?= $content['expire_date']; ?></p>
            
            <p>Please follow this <a href="https://app.musiconn.com.au/">link</a> to update your credit card details.</p>
            <p>Do not reply to this unattended mailbox.  Any queries can be directed to admin@musiconn.com.au, or 1300 068 742.</p>

            <p>Regards,<br>
               Admin Team – Musiconn Pty Ltd</p>
        </td>
    </tr>
</table>

