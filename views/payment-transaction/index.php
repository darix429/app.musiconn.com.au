<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\ArrayHelper;
use app\models\StudentMonthlySubscription;
use app\models\StudentTuitionBooking;


$this->title = Yii::t('app', 'Payment Transactions');
$this->params['breadcrumbs'][] = $this->title;

$studentTuitionBookingArray = ArrayHelper::map(StudentTuitionBooking::find()->where(['status' => 'BOOKED'])->all(),'uuid','booking_id');
$this->registerJs(
        '

$(document).on("click","#search_btn", function(){        
    $.ajax({
        type: "GET",
        url: "' . Url::to(['/payment-transaction/index/']) . '",
        data: $("#search_frm").serialize(),
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            $("#payment_transaction_table_parent_div").html(result);
            window.ULTRA_SETTINGS.dataTablesInit();
        },
        error:function(e){ 
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});

$(document).on("click","#search_reset_btn", function(){        
    $.ajax({
        type: "GET",
        url: "' . Url::to(['/payment-transaction/index/']) . '",
        data: {},
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            $("#payment_transaction_table_parent_div").html(result);
            window.ULTRA_SETTINGS.dataTablesInit();
        },
        error:function(e){ 
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});
    
    
', View::POS_END
);
?>

<div class="col-xl-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Payment Transaction History</h2>
            <div class="actions panel_actions float-right">
                <i class="fa fa-search icon-primary icon-xs icon-orange mail_head_icon custom-toggle" data-togglediv="datatable-search-div" title="Advance Search"></i>

            </div>
        </header>
        <div class="content-body"> 
            <div class="row ">
                <div class="col-lg-12 col-md-12 col-12 ">
                    <div class="datatable-search-div " style="display: none;">

                        <?php
                        $form = ActiveForm::begin([
                                    'method' => 'get',
                                    'options' => [
                                        'id' => 'search_frm',
                                        'class' => 'form-inline'
                                    ],
                        ]);
                        ?>

                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'transaction_number')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Transaction Number'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'order_id')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Order ID'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'book_uuid')->dropDownList($studentTuitionBookingArray, ['prompt' => 'Select Booking ID', 'class' => 'form-control', 'style' => 'width: 100%'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'fullname')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Student'])->label(false) ?>
                        </div><br><br>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'amount')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Amount', 'type' => 'number'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
                            <?= $form->field($searchModel, 'payment_datetime')->textInput(['class' => 'form-control custom_date_picker', 'maxlength' => true, 'placeholder' => 'Payment Date'])->label(false) ?>
                        </div>
                        <div class="col-md-3 mb-0">
		            <?= $form->field($searchModel, 'status')->dropDownList(['ALL' => 'ALL', 'succeeded' => 'SUCCEEDED', 'canceled' => 'CANCELED'], ['class' => 'form-control', 'style' => 'width:100%'])->label(false) ?>
		        </div> 

                        <?= Html::Button(Yii::t('app', 'Search'), ['class' => 'btn btn-primary mb-0', 'id' => 'search_btn', 'style' => 'margin-left: 17px;']) ?>
                        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-purple mb-0', 'id'=>'search_reset_btn']) ?>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
            <br>
            <div class=""></div>
            <div class="clearfix"></div>
            <div class="row" id="payment_transaction_table_parent_div">
                <?php echo Yii::$app->controller->renderPartial('_payment_transaction_list', ['paymentTransactionList' => $paymentTransactionList]); ?>
            </div>
        </div>
    </section> 
</div>
