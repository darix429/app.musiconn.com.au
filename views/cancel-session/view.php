<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CancelSession */

$this->title = $model->uuid;
$this->params['breadcrumbs'][] = ['label' => 'Cancel Sessions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cancel-session-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->uuid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->uuid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'uuid',
            'booked_session_uuid',
            'student_uuid',
            'tutor_uuid',
            'cancel_by:ntext',
            'canceler_uuid',
            'approved_by:ntext',
            'approver_uuid',
            'approve_datetime',
            'rejected_by:ntext',
            'rejecter_uuid',
            'reject_datetime',
            'status:ntext',
            'cancel_note:ntext',
            'created_at',
        ],
    ]) ?>

</div>
