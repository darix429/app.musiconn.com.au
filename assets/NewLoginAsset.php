<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class NewLoginAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'theme_assets/new/css/bootstrap.min.css',
        'theme_assets/new/css/font-awesome.min.css',
        'theme_assets/new/css/ionicons.min.css',
        //'theme_assets/new/css/skins.css',
        'theme_assets/plugins/messenger/css/messenger.css',
        'theme_assets/plugins/messenger/css/messenger-theme-flat.css',
        'theme_assets/new/css/style.css',
        'theme_assets/new/css/dataTables.bootstrap.min.css',
        'theme_assets/plugins/datepicker/css/datepicker.css',
        'theme_assets/plugins/timepicker/css/bootstrap-timepicker.css',
        'theme_assets/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css',
        'theme_assets/plugins/select2/select2.css',
        'theme_assets/plugins/multi-select/css/multi-select.css',
	'theme_assets/plugins/daterangepicker/css/daterangepicker-bs3.css',
        'theme_assets/new/custom.css',
    ];
    public $js = [
        'theme_assets/js/popper.min.js',
        
        'theme_assets/new/js/bootstrap.min.js',
        'theme_assets/plugins/pace/pace.js',
        'theme_assets/new/js/jquery.meanmenu.js',
        'theme_assets/new/js/owl.carousel.min.js',
        'theme_assets/new/js/jquery.sticky.js',
        'theme_assets/new/js/jquery.scrollUp.min.js',
        'theme_assets/new/js/adminlte.min.js',
        'theme_assets/new/js/main.js',
        'theme_assets/plugins/datepicker/js/datepicker.js',
        'theme_assets/plugins/daterangepicker/js/moment.min.js',
        'theme_assets/plugins/daterangepicker/js/daterangepicker.js',
        'theme_assets/plugins/timepicker/js/bootstrap-timepicker.min.js',
        'theme_assets/plugins/datetimepicker/js/bootstrap-datetimepicker.min.js',
        
        'theme_assets/new/new_custom.js',
        'theme_assets/js/custom.js',
        //'theme_assets/plugins/select2/select2.min.js',
        //'theme_assets/plugins/multi-select/js/jquery.multi-select.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
