<?php

use yii\helpers\Html;
use app\libraries\General;
use app\models\Student;

$student_address = '';
$student_name = '';
$modelStudent = Student::findOne($model->student_uuid);
if (!empty($modelStudent)) {
    $student_address = $modelStudent->street . ', ' . $modelStudent->city . ', ' . $modelStudent->state . ', ' . $modelStudent->country . ', ' . $modelStudent->postal_code;
    $student_name = $modelStudent->first_name . ' ' . $modelStudent->last_name;
}
?>

<!--<html class=" ">
    <head>-->
<?php
//$this->registerJsFile(Yii::$app->request->baseUrl . '//theme_assets/plugins/datatables/js/datatables.min.js');
?>


<!-- CORE CSS FRAMEWORK - START -->
<!--<link href="/theme_assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen"/>-->
<!--<link href="/theme_assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>-->
<!--        <link href="/theme_assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="/theme_assets/css/animate.min.css" rel="stylesheet" type="text/css"/>
        <link href="/theme_assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css"/>-->
<!-- CORE CSS FRAMEWORK - END -->

<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


<!-- CORE CSS TEMPLATE - START -->
<!--<link href="/theme_assets/css/style.css" rel="stylesheet" type="text/css"/>-->
<!--<link href="/theme_assets/css/responsive.css" rel="stylesheet" type="text/css"/>-->
<!-- CORE CSS TEMPLATE - END -->
<!--</head>-->
<!--    <body class=" ">
        <div class="page-container row-fluid">
            <section id="" class=" " style="width:100%">
                <section class="wrapper main-wrapper" style=''>

                    <div class="col-xl-12">
                       
                    </div>
                </section>
            </section>
        </div>
    </body>-->
<!--</html>-->
<style>

</style>
<section class="box ">
    <div class="content-body">    
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-12" >
                        <div class="invoice-head"  >
                            <table style="width :100% ; " width="100%"  >
                                <tr >
                                    <td style="width :20%;color:#1fb5ac;background-color: #1fb5ac; text-align: center; "  class="invoice-title">                                        
                                        <h2 class="" style="color: white;text-align: center; font-size: 24px;">Invoice</h2>
                                    </td>
                                    <td width="40%" style="width :40%; padding-left: 10px;">
                                        <?php $company_address = \app\models\Settings::getSettingsByName('company_address'); ?>
                                        <span class='text-muted invoice-head-info' style="line-height: 23px;" >
                                            <?= $company_address; ?>
                                        </span>

                                    </td>
                                    <td width="20%" style="width :20%">
                                        <div class="col-lg-3 col-md-3 col-3 invoice-head-info" >
                                            <span class='text-muted'>Order # <?= $model->order_id; ?><br><?= General::displayDate($model->order_datetime); ?></span>
                                        </div>
                                    </td>
                                    <td style="width :30%; max-height: 60px; background-color: #9972b5; text-align: center;" width="30%" >

                                        <img width="30%" src="<?= Yii::$app->params['theme_assets']['url'] . 'images/logo.png'; ?>" class="" style="width :30%; max-height: 60px;">

                                    </td>
                                </tr>
                            </table>                            
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="spacer"></div>

                    <div class="col-12 invoice-infoblock float-left">
                        <table style="width :100% ; " width="100%" border="0" >
                            <tr>
                                <td style="width:50%">
                                    <div class="col-6 invoice-infoblock float-left">
                                        <h4>Billed To:</h4>
                                        <address>
                                            <br>
                                            <h3><?= $student_name ?></h3>
                                            <br>
                                            <span class='text-muted' style="font-size: 11px;"><?= $student_address ?></span>
                                        </address>
                                    </div>
                                </td>
                                <td style="width:50%; text-align: right">
                                    <div class="col-6 invoice-infoblock float-right">
<!--                                        <h4>Payment Method:</h4>
                                        <address>
                                            <br>
                                            <h3><?= $student_name ?></h3>
                                            <br>
                                            <span class='text-muted'><?= $student_address ?></span>
                                        </address>-->
                                        <div class="invoice-due">
                                            <h3 class="text-muted">Total :&nbsp; <h2 class="text-primary">$<?= $model['grand_total']; ?></h2></h3>                 
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="clearfix"></div><br>
                </div>

                <div class="row">
                    <div class="col-lg-12 col-md-12 col-12">
                        <h3 style="">Order summary</h3>
                        
                            <table class="table table-hover invoice-table">
                                <thead >
                                    <tr class="bg-primary" >
                                        <td style="background-color: #1fb5ac;color:white;"><h4 class="text-white">Item</h4></td>
                                        <td class="text-center" style="background-color: #1fb5ac;color:white;"><h4 class="text-white">Price</h4></td>
                                        <td class="text-center" style="background-color: #1fb5ac;color:white;"><h4 class="text-white">Tax</h4></td>
                                        <td class="text-right" style="background-color: #1fb5ac;color:white;"><h4 class="text-white">Totals</h4></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if(!empty($order_item)){
                                        foreach ($order_item as $key => $item) {
                                            ?>
                                    <tr>
                                        <td style="line-height: 23px;padding: 5px 10px;font-size: 11px"><?= $item['item']; ?></td>
                                        <td style="line-height: 23px;padding: 5px 10px;font-size: 11px" class="text-center">$<?= $item['price']; ?></td>
                                        <td style="line-height: 23px;padding: 5px 10px;font-size: 11px" class="text-center">$<?= $item['tax']; ?></td>
                                        <td style="line-height: 23px;padding: 5px 10px;font-size: 11px" class="text-right">$<?= $item['total']; ?></td>
                                    </tr>
                                    <?php
                                        }
                                    }
                                    ?>
                                    
                                    <tr>
                                        <td class="thick-line" style="border-top: 1px solid #aaaaaa;"></td>
                                        <td class="thick-line" style="border-top: 1px solid #aaaaaa;"></td>
                                        <td class="thick-line text-center" style="border-top: 1px solid #aaaaaa;"><h4>Subtotal</h4></td>
                                        <td class="thick-line text-right" style="border-top: 1px solid #aaaaaa;"><h4>$<?= $model['total']; ?></h4></td>
                                    </tr>
                                    <tr>
                                        <td class="no-line" style="border-top: none;"></td>
                                        <td class="no-line" style="border-top: none;"></td>
                                        <td class="no-line text-center" style="border-top: none;"><h4>Discount</h4></td>
                                        <td class="no-line text-right" style="border-top: none;"><h4>$<?= $model['discount']; ?></h4></td>
                                    </tr>
                                    
                                    <tr>
                                        <td class="no-line" style="border-top: none;"></td>
                                        <td class="no-line" style="border-top: none;"></td>
                                        <td class="no-line text-center" style="border-top: none;"><h4>Total</h4></td>
                                        <td class="no-line text-right" style="border-top: none;"><h3 style='margin:0px;' class="text-primary">$<?= $model['grand_total']; ?></h3></td>
                                    </tr>
                                </tbody>
                            </table>
                       
                    </div>
                </div>
                <div class="clearfix"></div>


            </div>
        </div>
    </div>
</section>
<?php
// echo "come";exit; ?>