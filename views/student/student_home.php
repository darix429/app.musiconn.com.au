<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;
use app\models\Tutor;
use app\models\BookedSession;
use app\libraries\General;
use app\models\CreditSession;

$s_c_count = \app\models\CreditSession::getStudentCreditSessionCount(Yii::$app->user->identity->student_uuid);

$this->title = Yii::t('app', 'HOME');
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_icon'] = '1Home.png';

$this->registerJs(
        '
$(document).on("click",".session_cancel_button",function(){
    if(confirm("Are you sure you want to cancel this lesson?")){
        window.location.href= $(this).data("url");
        blockBody();
    } else {
        unblockBody();
        return false;
    }
}); 

function cancel_lesson_request_cancel(id){
    if(confirm("Are you sure, you want to cancel this lesson cancelled request?")){
        $.ajax({
            type: "POST",
            url: "' . Url::to(['booked-session/student-cancel-lesson-request-cancel']) . '?id="+id,
            data: {"is_cancel": "yes"},
            beforeSend: function() {
                blockBody();
            },
            success:function(result){
                
                if(result.code == 200){
                    window.location.href= "' . Url::to(['/booked-session/my-lessons/']) . '";
                    showSuccess(result.message);
                }
                unblockBody();
            },
            error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
        blockBody();
    } else {
        unblockBody();
        return false;
    }

}

setInterval(function() {
	array = [];
	value = [];
	var d = new Date();
	var current_timestamp = Math.floor(d / 1000);
	d.setMinutes(d.getMinutes() + 5);
	var after_timestamp = Math.floor(d / 1000);

	$(".tution_tr").each(function(item){
		var uuid = array["uuid"] = $(this).attr("data-uuid");
		array["start_time"] = $(this).attr("data-stime");
		array["end_time"] = $(this).attr("data-etime");
		value.push(array);
		if(after_timestamp >= array["start_time"] && current_timestamp <= array["end_time"]) {
			$("#startbutton_"+uuid).attr("disabled", false);
			$("#reschedulebutton_"+uuid).css({display:"none"});
			$("#cancelbutton_"+uuid).css({display:"none"});
		} else {
			$("#startbutton_"+uuid).attr("disabled",true);
			$("#reschedulebutton_"+uuid).css({display:"inline"});
			$("#cancelbutton_"+uuid).css({display:"inline"});
			if(current_timestamp >= array["end_time"]) {
				$(".tr_"+uuid).replaceWith("");
			}
		}
		
	});
}, 500 * 60 * 1);


$(document).on("click",".isCancelMonthlySubscriptionRequest", function(){ 
        if(confirm("Are you sure, you want to opt out from your current monthly subscription plan?")){
        var id = $(".isCancelMonthlySubscriptionRequest").data("id");
        var element = $(this);
            $.ajax({
                type: "GET",
                url: "' . Url::to(['/student/monthly-subscription-cancel/']) . '",
                data: { "id": id},
                //beforeSend:function(){ $(".local-spinner").show(); },
                success:function(result){
                    if(result.code == 200){
                        $(element).html("");
                        //$(element).addClass("disabled");
                        $(element).removeClass("isCancelMonthlySubscriptionRequest");
                        
                        //showSuccess(result.message);

                        window.location="' . Yii::$app->urlManager->createUrl('student/profile') . '";

                    }else{
                        showErrorMessage(result.message);
                    }
                },
                error:function(e){ 
                    showErrorMessage(e.responseText);
                }
            });
        }
    });

        
', View::POS_END
);
?>
<style>
    .row_hr_line {
        border-bottom: 2px solid #d2d5de;
        margin: 30px 0px 10px 0px;
        display: inline-block;
        width: 100%;
    }
    .lesson_cate_header{
        padding: 10px;
    }
    .disabled {
        pointer-events: none;
        background: #aaa !important;
    }
</style>
<section class="content">
    
    <div class="row">
        <div class="col-md-4"><span class="box-header with-border" type=""><button class="hm-mn-btn yellow" onclick="buy_musicoin();">BUY MUSICOINS</button></span></div>
        <div class="col-md-4"><span class="box-header with-border" type=""><button class="hm-mn-btn black" onclick="window.location.href = '<?= Yii::$app->getUrlManager()->createUrl('/book'); ?>'">BOOK A LESSON</button></span></div>
        <div class="col-md-4"><span class="box-header with-border" type=""><button class="hm-mn-btn black" data-toggle="modal" data-target="#manage-subscription-model">VIDEO LIBRARY SUBSCRIPTION</button></span></div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <span class="box-header " type="">My Musicoins <span class="student_musicoin_balance_text"><?= \app\libraries\General::getStudentMusicoin(Yii::$app->user->identity->student_uuid); ?></span></span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="lesson_cate_header">
                <h4>UPCOMING LESSONS</h4>
            </div>
            <?php if (!empty($upcomingList)) { ?>

                <?php
                foreach ($upcomingList as $key => $value) {
                   
                    $now = date('Y-m-d h:i A');
                    $timestamp = strtotime($now);
                    $timestamp_after_5 = date('Y-m-d h:i A', strtotime('+5 minutes', strtotime($now)));
                    ?>

                    <div class="box box-primary tution_tr tr_<?= $value['uuid']; ?> " data-stime="<?= strtotime($value['start_datetime']); ?>" data-etime="<?= strtotime($value['end_datetime']); ?>" data-uuid="<?= $value['uuid']; ?>">
                        <div class="box-header with-border">
                            <ul class="products-list product-list-in-box">
                                <li class="item pad-0">
                                    <div class="product-img">
                                        <?php
                                        $inst_img_dataUri = \app\libraries\General::getInstrumentImage($value['instrument_uuid']);
                                        echo "<img src='$inst_img_dataUri'/>";
                                        ?>
                                        <!--<img src="<?php // echo '/theme_assets/new_book/icons/drums.png'; ?>">-->
                                    </div>
                                    <div class="product-info">
                                        <a  class="product-title">
                                            <?= \app\libraries\General::convertSystemToUserTimezone($value['start_datetime'], "l, d F Y"); ?>
                                            <span class="label pull-right">at <?= \app\libraries\General::convertSystemToUserTimezone($value['start_datetime'], "h:ia"); ?> - <?= \app\libraries\General::convertSystemToUserTimezone($value['end_datetime'], "h:ia"); ?></span>
                                        </a>
                                        <span class="product-description">
                                            <a>with <?= $value['tutorUu']['first_name'] . ' ' . $value['tutorUu']['last_name']; ?></a>
                                            <p class="pull-right " style="color:#8223d1;">
                                                <?php //echo 'Pin is '.$value['session_pin']; ?>
                                            </p>
                                        </span>
                                        <!--<div class="clearfix"></div>-->
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="box-body box-bodys" style="">
                            <div class="pull-right">                                        
                                <?php
                                if ($value['status'] == "SCHEDULE" || $value['status'] == "INPROCESS") {

                                    $enabled = (strtotime($timestamp_after_5) >= strtotime($value['start_datetime']) && $timestamp <= strtotime($value['end_datetime'])) ? "" : "disabled='disabled'";
                                    $enabled_class = (strtotime($timestamp_after_5) >= strtotime($value['start_datetime']) && $timestamp <= strtotime($value['end_datetime'])) ? "btn-primary" : "btn-default";
                                    $button_text = ( (strtotime($timestamp_after_5) >= strtotime($value['start_datetime']) && $timestamp <= strtotime($value['end_datetime'])) ) ? "JOIN" : "START";
                                    ?>
                                    <?php
                                    $isSchedule = false;
                                    if (strtotime($timestamp_after_5) >= strtotime($value['start_datetime']) && $timestamp <= strtotime($value['end_datetime'])) {
                                        $start_display = "display:inline";
                                        $reschedule_display = "display:none";
                                        $cancel_display = "display:none";
                                        $isSchedule = true;
                                    } else {
                                        $start_display = "display:none";
                                        $reschedule_display = "display:inline";
                                        $cancel_display = "display:inline";
                                    }
                                    ?>
                                    <span class="product-description ">
                                        <div class="pull-right">
                                            <a class="label label-warning mr-2 session_start_button <?= $isSchedule?'':'disabled' ?>" 
                                                data-uuid="<?= $value['uuid'] ?>" 
                                                id="startbutton_<?= $value['uuid'] ?>" 
                                                data-url="<?= BookedSession::getStartSessionLink($value['uuid'], 'student'); ?>" 
                                                style="margin-right: 9px;"
                                            >
                                                <?= $button_text; ?>
                                            </a>
                                            

                                            <?php if ($value['start_datetime'] > date('Y-m-d H:i:s')) {  //echo $value['start_datetime'];  ?>
                                                <?php if ($value['is_cancelled_request'] == 1) { ?>
                                                    <?php /*<a class="label label-warning hm-1 mr-2 cancel_lesson_popup_btn" title="Pending Cancel Approval" data-uuid="<?= $value['uuid'] ?>" onclick="cancel_lesson_request_cancel('<?= $value['uuid'] ?>');">Lesson Cancellation is awaiting tutor's approval</a>*/?>
                                                    <a class="label label-warning hm-1 mr-2 cancel_lesson_popup_btn  text-uppercase" title="Cancellation initiated" data-uuid="<?= $value['uuid'] ?>" onclick="cancel_lesson_request_cancel('<?= $value['uuid'] ?>');">Cancellation initiated</a>
                                                <?php } else { ?>
                                                    <a class="label label-warning hm-1 mr-2 session_cancel_button" style="<?= $cancel_display; ?>" title="Cancel this lesson" data-url="<?= Url::to(['/booked-session/cancel-lesson-request', 'id' => $value['uuid']]) ?>" id="cancelbutton_<?= $value['uuid'] ?>" >CANCEL</a>
                                                <?php } ?>
                                                <?php if ($value['is_reschedule_request'] == 2) { ?>
                                                    <a class="label label-warning btn-warning  text-uppercase session_reschedule_button" style="<?= $reschedule_display; ?>" title="Pending Approval" href="<?= Url::to(['/book/reschedule-request', 'id' => $value['uuid']]) ?>" id="reschedulebutton_<?= $value['uuid'] ?>" >PENDING APPROVAL</a>
                                                <?php } else { ?>
                                                    <a class="label label-warning <?= ($value['is_reschedule_request'] == 1) ? "btn-info" : "btn-warning" ?>  text-uppercase session_reschedule_button" style="<?= $reschedule_display; ?>" title="<?= ($value['is_reschedule_request'] == 1) ? "Reschedule initiated" : "Reschedule this lesson"; ?>" href="<?= Url::to(['/book/reschedule-request', 'id' => $value['uuid']]) ?>" id="reschedulebutton_<?= $value['uuid'] ?>" ><?= ($value['is_reschedule_request'] == 1) ? "Reschedule initiated" : "RESCHEDULE" ?></a>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                    </span>
                                    <?php 
                                } else {
                                    echo ucfirst($value['status']);
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            <?php } else { ?>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <ul class="products-list product-list-in-box">
                            <li class="item">
                                <div class="product-img">
                                    <img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new_book/img/my-lesson-tag.png' ?>">
                                </div>
                                <div class="product-info">
                                    <a  class="product-title">No upcoming lessons.</a>                                        
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            <?php } ?>
            <a href="<?= Url::to(['/booked-session/upcoming/']) ?>" class="label label-warning hm-1 mr-2 pull-right">SEE MORE</a>
        </div>

        <div class="col-md-6">
            <div class="lesson_cate_header">
                <h4>COMPLETED LESSONS</h4>
            </div>
            <?php if (!empty($completedList)) { ?>

                <?php foreach ($completedList as $key => $value) { ?>

                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <ul class="products-list product-list-in-box">
                                <li class="item">
                                    <div class="product-img">
                                        <?php
                                        $inst_img_dataUri = \app\libraries\General::getInstrumentImage($value['instrument_uuid']);
                                        echo "<img src='$inst_img_dataUri'/>";
                                        ?>
                                        <!--<img src="<?php //Yii::$app->request->baseUrl . '/theme_assets/new_book/' ?>img/music-1.png">-->
                                    </div>
                                    <div class="product-info">
                                        <a  class="product-title">
                                            <?= \app\libraries\General::convertSystemToUserTimezone($value['start_datetime'], "l, d F Y"); ?>
                                            <span class="label pull-right">at <?= \app\libraries\General::convertSystemToUserTimezone($value['start_datetime'], "h:ia"); ?> - <?= \app\libraries\General::convertSystemToUserTimezone($value['end_datetime'], "h:ia"); ?></span>
                                        </a>
                                        <span class="product-description">
                                            <a>with <?= $value['tutorUu']['first_name'] . ' ' . $value['tutorUu']['last_name']; ?></a>
                                            <?php
                                            if ($value['status'] == 'COMPLETED') {
                                                //echo "<span class='label'>(Video Under Process)</span>";
                                            }
                                            ?>
                                        </span>
                                    </div>
                                    <div class="box-tools pull-right">
                                        <?php
                                        if ($value['status'] == 'PUBLISHED' && $value['studentUu']['isMonthlySubscription']) {
                                            $competed_link_url = Url::to(['video/tution-view', 'id' => $value['uuid']], true);
                                            $competed_link_label = 'VIEW RECORDING';
                                            ?>
                                                                <!--<a href="<?php // Url::to(['video/tution-view', 'id' => $value['uuid']], true);   ?>" class="label label-warning pull-right">VIEW RECORDING</a>-->
                                            <?php
                                        } elseif (!$value['studentUu']['isMonthlySubscription']) {
                                            $competed_link_url = Url::to(['monthly-subscription/index', 'id' => $value['uuid']], true);
                                            $competed_link_label = 'BUY SUBCRIPTION';
                                            ?>
                                                                <!--<a href="<?php // Url::to(['monthly-subscription/index', 'id' => $value['uuid']], true);   ?>" class="label label-warning pull-right">BUY SUBCRIPTION</a>-->
                                            <?php
                                        }
                                        if($value['status'] != "COMPLETED"){
                                        ?>
                                        <button type="button" class="btn btn-box-tool mp-75" data-widget="collapse" onclick="window.location.href = '<?= $competed_link_url; ?>'" title="<?= $competed_link_label; ?>"><i class="fa fa-arrow-circle-o-up" aria-hidden="true"></i></button>
                                        <?php } ?>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                <?php } ?>
            <?php } else { ?>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <ul class="products-list product-list-in-box">
                            <li class="item">
                                <div class="product-img">
                                    <img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new_book/img/my-lesson-tag.png' ?>">
                                </div>
                                <div class="product-info">
                                    <a  class="product-title">No completed lessons.</a>                                        
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            <?php } ?>
            <a href="<?= Url::to(['/booked-session/completed/']) ?>" class="label label-warning hm-1 mr-2 pull-right">SEE MORE</a>
        </div>
    </div>
</section>
<hr>
<section class="content">
    <!--<div class="row_hr_line"></div>-->
    <div class="row">
        <div class="col-md-6">
            <div class="lesson_cate_header">
                <h4>CANCELLED LESSONS</h4>
            </div>
            <?php if (!empty($cancelledList)) { ?>
                <?php foreach ($cancelledList as $key => $value) { ?>
                    <div class="box box-primary ">
                        <div class="box-header with-border">
                            <ul class="products-list product-list-in-box">
                                <li class="item">
                                    <div class="product-img">
                                        <?php
                                        $inst_img_dataUri = \app\libraries\General::getInstrumentImage($value['instrument_uuid']);
                                        echo "<img src='$inst_img_dataUri'/>";
                                        ?>
                                        <!--<img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new_book/' ?>img/music-1.png">-->
                                    </div>
                                    <div class="product-info">
                                        <a  class="product-title">
                                            <?= \app\libraries\General::convertSystemToUserTimezone($value['start_datetime'], "l, d F Y"); ?>
                                            <span class="label pull-right">at <?= \app\libraries\General::convertSystemToUserTimezone($value['start_datetime'], "h:ia"); ?> - <?= \app\libraries\General::convertSystemToUserTimezone($value['end_datetime'], "h:ia"); ?></span>
                                        </a>
                                        <span class="product-description">
                                            <a>with <?= $value['tutorUu']['first_name'] . ' ' . $value['tutorUu']['last_name']; ?></a>
                                        </span>
                                        <span class="label pull-right">
                                            Cancelled on <?= \app\libraries\General::convertSystemToUserTimezone($value['cancel_datetime'], "d F Y"); ?>
                                        </span>
                                        <div class="clearfix"></div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                <?php } ?>
            <?php } else { ?>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <ul class="products-list product-list-in-box">
                            <li class="item">
                                <div class="product-img">
                                    <img src="<?= Yii::$app->request->baseUrl . '/theme_assets/new_book/img/my-lesson-tag.png' ?>">
                                </div>
                                <div class="product-info">
                                    <a  class="product-title">No cancelled lessons.</a>                                        
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            <?php } ?>
            <a href="<?= Url::to(['/booked-session/cancelled/']) ?>" class="label label-warning hm-1 mr-2 pull-right">SEE MORE</a>

        </div>
        <div class="col-md-6">
        </div>
    </div>
</section>

<!-- modal-content-MANAGE SUBSCRIPTION -->
<div class="modal fade" id="manage-subscription-model">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">VIDEO LIBRARY SUBSCRIPTION</h4>
            </div>
            <div class="modal-body">
                <?php
                if ($model->isMonthlySubscription == '0') {
                    $subscriptionFeeList = \app\models\StudentMonthlySubscription::getsubscriptionFeeList(Yii::$app->user->identity->student_uuid);
                    ?>
                    <div class="row vertical-center-box vertical-center-box-tablet">
                        <span class="hr_line">
                            <div class="text-left col-xs-12 mar-bot-15">
                                <label class="cont_title_add">
                                    <h4>MUSICONN ONLINE VIDEO LIBRARY</h4>
                                </label>
                            </div>
                        </span>
                        <span class="hr_line">
                            <div class="text-left col-xs-7 mar-bot-15">
                                <label class="cont_title_add"><b>Subscription</b><br><?= $subscriptionFeeList->name ?> in $<?= $subscriptionFeeList->total_price; ?>.</label>
                            </div>
                            <div class="text-left col-xs-4 mar-bot-15">
                                <a href="<?= Yii::$app->getUrlManager()->createUrl('/monthly-subscription/index'); ?>"><div class="modal_add">BUY</div></a>
                            </div>
                        </span>

                    </div>
                    <?php
                } else {

                    $studentSubDetail = \app\models\StudentSubscription::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid])->one();
                    $studentMonthlySubDetail = \app\models\StudentMonthlySubscription::find()->where(['uuid' => $studentSubDetail->subscription_uuid])->one();
                    ?>

                    <div class="row vertical-center-box vertical-center-box-tablet">
                        <span class="hr_line">
                            <div class="text-left col-xs-12 mar-bot-15">
                                <label class="cont_title_add">
                                    <h4>MUSICONN ONLINE VIDEO LIBRARY</h4>
                                    Subscription Status - Active
                                </label>
                            </div>
                        </span>
                        <span class="hr_line">
                            <div class="text-left col-xs-7 mar-bot-15">
                                <label class="cont_title_add"><b>Subscription </b><br><?= $studentMonthlySubDetail['name']; ?></label>
                            </div>
                            <div class="text-left col-xs-4 mar-bot-15">
                                <?php if ($model->isMonthlySubscription && !$model->isCancelMonthlySubscriptionRequest) { ?>
                                    <a class="isCancelMonthlySubscriptionRequest" data-id="<?= $model->student_uuid; ?>">
                                        <div class="modal_add">Cancel Subscription</div>
                                    </a>
                                <?php } elseif ($model->isMonthlySubscription && $model->isCancelMonthlySubscriptionRequest) { ?>
                                    <a class=" disabled">
                                        <div class="modal_add">Pending approval of cancel subscription</div>
                                    </a>
                                <?php } ?>

                            </div>
                        </span>
                        <span class="hr_line2">
                            <div class="text-left col-xs-7 mar-bot-15">
                                <label class="cont_title_add"><b>Next Billing Date</b><br><?= General::convertSystemToUserTimezone(date('d M Y', strtotime('+1 days', strtotime($studentMonthlySubDetail->end_datetime))), 'd F Y'); ?></label>
                            </div>
                            <?php
                            if (!empty($studentSubDetail)) {
                                $next_renewal_date = strtotime($studentSubDetail['next_renewal_date']);
                                $currentDate = date("Y-m-d");
                                $days_ago = date('Y-m-d', strtotime('-10 days', $next_renewal_date));
                                if ($currentDate >= $days_ago) {
                                    ?>

                                    <div class="text-left col-xs-4 mar-bot-15">
                                        <a href="<?= Yii::$app->getUrlManager()->createUrl('/monthly-subscription/index'); ?>"><div class="modal_add">RENEW</div></a>
                                    </div>
                                    <?php
                                }
                            }
                            ?>

                        </span>
                    </div>
                <?php } ?>
            </div>

        </div>
    </div>
</div>
<!-- /.modal -->
