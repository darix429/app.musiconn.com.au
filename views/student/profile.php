<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\StudentCreditCard;
use app\libraries\General;
use app\models\StudentNotification;

/* @var $this yii\web\View */
/* @var $model app\models\Admin */

$timezone = General::getAllTimeZones();
if ($userModel->timezone == '') {
    $userModel->timezone = Yii::$app->params['timezone'];
}
$this->title = Yii::t('app', 'MY PROFILE');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_icon'] = 'my-profiles.png';
$this->params['page_icon'] = 'MyProfile-gradient.png';

$this->registerJsFile('https://js.stripe.com/v2/');
$this->registerJs('

//set your publishable key
Stripe.setPublishableKey("' . Yii::$app->params['stripe']['publishable_key'] . '");

    $(document).on("click","#load_cp_model_btn",function(){
        document.getElementById("form-change").reset();
    });
    
    function refreshCPForm(){
        $.ajax({
            type: "GET",
            url:  "' . Url::to(['student/change-password']) . '",
            success:function(result){
                $("#changepassword_div").html(result);
            },
            error:function(e){
                showErrorMessage(e.responseText);
            }
        });
    }
    
    $(document).on("click","#password-change-btn",function(){
        $.ajax({
            type: "POST",
            url:  "' . Url::to(['student/change-password']) . '",
            data: $("#form-change").serialize(),
            beforeSend: function() { blockBody(); },
            success:function(result){
                if(result.code == 200){
                    showSuccess(result.message);
                    //refreshCPForm();
                    $("#change_password_model").modal("toggle");
                }else{
                    $("#changepassword_div").html(result);
                }
                unblockBody();
            },
            error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
    })
    

    //delete card
            $(document).on("click",".delete_credit_card", function(){
                if(confirm("Are you sure, you want to delete this card ?")){
                var element = $(this);
                    $.ajax({
                        type: "POST",
                        url: "' . Url::to(['/monthly-subscription/delete/']) . '",
                        data: { "id": $(this).data("uuid")},
                        beforeSend: function() { blockBody(); },
                        success:function(result){
                            if(result.code == 200){

                                $("#creditcard_div").html(result);
                                refreshCCForm();
                            }else{
                                showErrorMessage(result.message);
                            }
                            unblockBody();
                        },
                        error:function(e){
                            unblockBody();
                            showErrorMessage(e.responseText);
                        }
                    });
                }
            });

    function refreshCCForm(){
        $.ajax({
            type: "GET",
            url:  "' . Url::to(['student/credit-card']) . '",
            success:function(result){
               $("#creditcard_div").html(result);
               ULTRA_SETTINGS.otherScripts();
            },
            error:function(e){
                showErrorMessage(e.responseText);
            }
        });
    }

    //add credit card
    $(document).on("click","#card-create-btn",function(){
    var data = $("#form-create-card").serialize();

        $.ajax({
            type: "POST",
            url:  "' . Url::to(['student/credit-card']) . '",
            data: $("#form-create-card").serialize(),
            success:function(result){
                if(result.code == 200){
                    showSuccess(result.message);
                    refreshCCForm();
                }else{
                    $("#creditcard_div").html(result);
                    ULTRA_SETTINGS.otherScripts();
                }
            },
            error:function(e){
                showErrorMessage(e.responseText);
            }
        });
    })



            //card number validation
            $(document).on("input","#studentcreditcard-number", function(){
                    match = (/(\d{0,16})[^]*((?:\.\d{0,0})?)/g).exec(this.value.replace(/[^\d.]/g, ""));
                    this.value = match[1] + match[2];
            });

            //card cvv validation
            $(document).on("input","#studentcreditcard-cvv", function(){
                    match = (/(\d{0,4})[^]*((?:\.\d{0,0})?)/g).exec(this.value.replace(/[^\d.]/g, ""));
                    this.value = match[1] + match[2];
            });

    $(document).on("click",".termination_account",function(){
        if(confirm("Are you sure, you want to delete your account it will cancelled all your bookings and monthly subscription if any?")){
            var id = $(".termination_account").data("id");
            var element = $(this);
            $.ajax({
                type: "GET",
                url:  "' . Url::to(['student/remove-account']) . '",
                data: { "id": id},
                beforeSend: function() { blockBody(); },
                success:function(result){
                    if(result.code == 200){
                        showSuccess(result.message);
                        location.reload();
                    }else{
                        showErrorMessage(result.message);
                    }
                    unblockBody();
                },
                error:function(e){
                    unblockBody();
                    showErrorMessage(e.responseText);
                }
            });
        }
    });

    

    function refreshStudentNotificationForm(){
        $.ajax({
            type: "GET",
            url:  "' . Url::to(['student-notification/index']) . '",
            success:function(result){
                $("#student_notification_div").html(result);
                $(".student_uuid").val("' . Yii::$app->user->identity->student_uuid . '");
            },
            error:function(e){
                showErrorMessage(e.responseText);
            }
        });
    }

    //Get Student Notification
    $("#studentNotification").click(function() {
        refreshStudentNotificationForm();
    });

    //Add Student Notification
    $(document).on("click", "#StudentAddNotification", function() {
        $.ajax({
            type: "POST",
            url:  "' . Url::to(['student-notification/index']) . '",
            beforeSend: function() { blockBody(); },
            data: $("#dynamic-form").serialize(),
            success:function(result){
                unblockBody();
                if(result.code == 200) {
                    showSuccess(result.message);
                    refreshStudentNotificationForm();
                    $("#student-notification-model").modal("hide");
                 } else {
                    $.each(result.message, function( key, val ){
                      $("#"+key).next().html(val);
                      $("#"+key).parent().addClass("has-error");
                    });
                 }
            },
            error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
    });

    $(document).on("click","#open_profile_popup",function(){
        $.ajax({
            type: "GET",
            url:  "' . Url::to(['student/profile-update']) . '",
            data: {},
            beforeSend: function() { blockBody(); },
            success:function(result){
                
                $("#update-personal-details").modal("show");
                $("#profile_detail_div").html(result);
                unblockBody();
            },
            error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
    });
    $(document).on("click","#form-profile-update-btn",function(){
        $.ajax({
            type: "POST",
            url:  "' . Url::to(['student/profile-update']) . '",
            data: $("#form-profile-update").serialize(),
            beforeSend: function() { blockBody(); },
            success:function(result){
                if(result.code == 200){
                    $("#update-personal-details").modal("toggle");
                    //showSuccess(result.message);
		    location.reload();
                }else{
                    $("#profile_detail_div").html(result);
                }
                unblockBody();
            },
            error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
    })
    
$(document).on("click",".isCancelMonthlySubscriptionRequest", function(){ 
        if(confirm("Are you sure, you want to opt out from your current monthly subscription plan?")){
        var id = $(".isCancelMonthlySubscriptionRequest").data("id");
        var element = $(this);
            $.ajax({
                type: "GET",
                url: "' . Url::to(['/student/monthly-subscription-cancel/']) . '",
                data: { "id": id},
                //beforeSend:function(){ $(".local-spinner").show(); },
                success:function(result){
                    if(result.code == 200){
                        $(element).html("");
                        //$(element).addClass("disabled");
                        $(element).removeClass("isCancelMonthlySubscriptionRequest");
                        
                        //showSuccess(result.message);

                        window.location="'.Yii::$app->urlManager->createUrl('student/profile').'";

                    }else{
                        showErrorMessage(result.message);
                    }
                },
                error:function(e){ 
                    showErrorMessage(e.responseText);
                }
            });
        }
    });




');
?>


<div class="section-admin container-fluid">
    <div class="row admin text-center">
        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="profile-dtl">
<!--                        <span class="profile_sec_dp">
                            <img src="/theme_assets/new/img/profile-2.png" alt="" />
                        </span>-->
                        <span class="profile-charecter-profilepage" >
                                <?= \app\models\User::loginUserProfileCharecter(); ?>
                        </span>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="margin-bottom:1px;">
                    <div class="admin-content analysis-progrebar-ctn res-mg-t-30">
                        <h1 class="text-left"><?= Yii::$app->user->identity->first_name ?> <?= Yii::$app->user->identity->last_name ?></h1>
                        <span class="min-dtn-sub-2">Birthday <?= \app\libraries\General::displayTimeFormat($model->birthdate, 'd.m.Y') ?></span>
                        <p class="min-dtn2">STUDENT ID <?= $model->enrollment_id; ?></p>
                        <p class="min-dtn">JOIN DATE <?= \app\libraries\General::convertSystemToUserTimezone(date("Y-m-d", $model->created_at), 'd.m.Y') ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section-admin container-fluid">
    <div class="row admin text-center">
        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="admin-content analysis-progrebar-ctn res-mg-t-30">
                        <div class="row vertical-center-box vertical-center-box-tablet">
                            <?= Html::a('<div class="change_pass" style="font-weight:bold;">TEST SYSTEMS COMPATIBILITY</div>', ['/site/check-device'], []) ?>
                            <a href="#" id="load_cp_model_btn" data-toggle="modal" data-target="#change_password_model"><div class="change_pass">CHANGE PASSWORD</div></a>
                            <a href="#" data-toggle="modal" data-target="#update-credit-card"><div class="change_pass">UPDATE CREDIT CARD</div></a>
                            <a href="#" id="studentNotification" data-toggle="modal" data-target="#student-notification-model"><div class="change_pass">NOTIFICATIONS</div></a>

                        </div>
                    </div>
                    <div class="admin-content analysis-progrebar-ctn res-mg-t-30">
                        <div class="row vertical-center-box vertical-center-box-tablet">
                            <!--<a class="btn btn-danger text-white float-right termination_account" title="Delete Account" data-id="<?php // echo $model->student_uuid;   ?>">Delete Account</a>-->
                            <a class="termination_account" title="Delete Account" data-id="<?php echo $model->student_uuid; ?>">
                                <div class="change_pass " style="color:#fff;background-color: #e03913; font-weight: 700; cursor: pointer;">DELETE ACCOUNT</div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="admin-content analysis-progrebar-ctn res-mg-t-30">
                        <h4 class="text-left text-uppercase">CONTACT INFO</h4>
                        <div class="row vertical-center-box vertical-center-box-tablet">
                            <div class="text-left col-xs-12 mar-bot-15">
                                <label class="cont_title_add">
                                    <b>Address</b>
                                    <br><?= $model->street; ?>
                                    <br><?= $model->city; ?>
                                    <br><?= $model->state; ?>, <?= $model->postal_code; ?>
                                    <br><?= $model->country; ?>
                                </label>
                            </div>
                            <div class="text-left col-xs-12 mar-bot-15">
                                <label class="cont_title_add"><b>Email</b><br><?= $userModel->email; ?></label>
                            </div>
                            <div class="text-left col-xs-12 mar-bot-15">
                                <label class="cont_title_add"><b>Phone Number</b><br>
                                    <?php echo $model->phone_number; ?>
                                </label>
                            </div>
                            <div class="text-left col-xs-12 mar-bot-15">
                                <a href="javascript:void(0)" id="open_profile_popup"><div class="change_pass_add">UPDATE DETAILS</div></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="admin-content analysis-progrebar-ctn res-mg-t-30">
                        <h4 class="text-left text-uppercase">SUBSCRIPTION</h4>
                        <div class="row vertical-center-box vertical-center-box-tablet">
                            <div class="text-left col-xs-12 mar-bot-15">
                                <label class="cont_title_add">
                                    <b>Online Video Library</b>
                                    <?php
                                    $studentSubDetail = \app\models\StudentSubscription::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid])->one();

                                    if ($model->isMonthlySubscription == '0') {
                                        
                                    } else {
                                        $studentMonthlySubDetail = \app\models\StudentMonthlySubscription::find()->where(['uuid' => $studentSubDetail->subscription_uuid])->one();
                                        echo ''
                                        . '<br>$' . $studentMonthlySubDetail['total_price'] . ' billed monthly'
                                        . '<br>' . General::convertSystemToUserTimezone(date('d M Y', strtotime('+1 days', strtotime($studentMonthlySubDetail->end_datetime))), 'd F Y');
                                    }
                                    ?>
                                </label>
                            </div>
                            <div class="text-left col-xs-12 mar-bot-15">
                                <a href="#" data-toggle="modal" data-target="#manage-subscription-model"><div class="change_pass_add">MANAGE SUBSCRIPTION</div></a>
                            </div>
                        </div>
                    </div>
                </div>-->
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="admin-content analysis-progrebar-ctn res-mg-t-30">
                        <h4 class="text-left text-uppercase">REPORTS</h4>
                        <div class="row vertical-center-box vertical-center-box-tablet">
                            <div class="text-left col-xs-12 mar-bot-15 report_list_view">
                                <div><a href="<?= Yii::$app->getUrlManager()->createUrl('/monthly-subscription/history'); ?>">Subscription History <i class="fa fa-arrow-circle-o-right"></i></a></div>
                                <div><a href="<?= Yii::$app->getUrlManager()->createUrl('/book-music-tution/history'); ?>">Lesson History <i class="fa fa-arrow-circle-o-right"></i></a></div>
                                <div><a href="<?= Yii::$app->getUrlManager()->createUrl('/order/history'); ?>">Order History <i class="fa fa-arrow-circle-o-right"></i></a></div>
                                <div><a href="<?= Yii::$app->getUrlManager()->createUrl('/booked-session/expired'); ?>">Missed Lessons <i class="fa fa-arrow-circle-o-right"></i></a></div>
                                <div><a href="<?= Yii::$app->getUrlManager()->createUrl('/musicoin/history'); ?>">Musicoin History <i class="fa fa-arrow-circle-o-right"></i></a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal-content-UPDATE CREDIT CARD-->
<div class="modal fade" id="update-credit-card">
    <div class="modal-dialog">
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">UPDATE CREDIT CARD</h4>
            </div>
            <div  id="creditcard_div">
                <?php
                $creditCard = StudentCreditCard::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid])->one();

                if (empty($creditCard)) {
                    echo Yii::$app->controller->renderPartial('add_credit_card', ['modelCC' => $modelCC]);
                } else {
                    echo Yii::$app->controller->renderPartial('update_credit_card', ['modelCC' => $modelCC, 'creditCard' => $creditCard]);
                }
                ?>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->

<!-- modal-content Notifications-->
<div class="modal fade" id="student-notification-model">
    <div class="modal-dialog">
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">NOTIFICATIONS</h4>
            </div>
            <div  id="student_notification_div">
               
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->



<!-- modal-content-CHANGE PASSWORD-->
<div class="modal fade" id="change_password_model">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">CHANGE PASSWORD</h4>
            </div>
            <div id="changepassword_div">
                <?php echo Yii::$app->controller->renderPartial('change-password', ['modelCP' => $modelCP]); ?>
            </div>

        </div>
    </div>
</div>
<!-- /.modal -->

<!-- modal-content-UPDATE-PERSONAL-DETAILS-->
<div class="modal fade" id="update-personal-details">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">UPDATE PERSONAL DETAILS</h4>
            </div>
            <div id="profile_detail_div">
                <?php echo Yii::$app->controller->renderPartial('_profile_detail', ['userModel' => $userModel, 'model' => $model]); ?>
            </div>

        </div>
    </div>
</div>
<!-- /.modal -->

<!-- modal-content-MANAGE SUBSCRIPTION -->
<div class="modal fade" id="manage-subscription-model">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">MANAGE SUBSCRIPTION</h4>
            </div>
            <div class="modal-body">
                <?php
                if ($model->isMonthlySubscription == '0') {
                    $subscriptionFeeList = \app\models\StudentMonthlySubscription::getsubscriptionFeeList(Yii::$app->user->identity->student_uuid);
                    ?>
                    <div class="row vertical-center-box vertical-center-box-tablet">
                        <span class="hr_line">
                            <div class="text-left col-xs-12 mar-bot-15">
                                <label class="cont_title_add">
                                    <h4>MUSICONN ONLINE VIDEO LIBRARY</h4>
                                </label>
                            </div>
                        </span>
                        <span class="hr_line">
                            <div class="text-left col-xs-7 mar-bot-15">
                                <label class="cont_title_add"><b>Subscription</b><br><?= $subscriptionFeeList->name ?> in $<?= $subscriptionFeeList->total_price; ?>.</label>
                            </div>
                            <div class="text-left col-xs-4 mar-bot-15">
                                <a href="<?= Yii::$app->getUrlManager()->createUrl('/monthly-subscription/index'); ?>"><div class="modal_add">BUY</div></a>
                            </div>
                        </span>

                    </div>
                    <?php
                } else {

                    $studentSubDetail = \app\models\StudentSubscription::find()->where(['student_uuid' => Yii::$app->user->identity->student_uuid])->one();
                    $studentMonthlySubDetail = \app\models\StudentMonthlySubscription::find()->where(['uuid' => $studentSubDetail->subscription_uuid])->one();
                    ?>

                    <div class="row vertical-center-box vertical-center-box-tablet">
                        <span class="hr_line">
                            <div class="text-left col-xs-12 mar-bot-15">
                                <label class="cont_title_add">
                                    <h4>MUSICONN ONLINE VIDEO LIBRARY</h4>
                                    Subscription Status - Active
                                </label>
                            </div>
                        </span>
                        <span class="hr_line">
                            <div class="text-left col-xs-7 mar-bot-15">
                                <label class="cont_title_add"><b>Subscription </b><br><?= $studentMonthlySubDetail['name']; ?></label>
                            </div>
                            <div class="text-left col-xs-4 mar-bot-15">
                                <?php if ($model->isMonthlySubscription && !$model->isCancelMonthlySubscriptionRequest) { ?>
                                    <a class="isCancelMonthlySubscriptionRequest" data-id="<?= $model->student_uuid; ?>">
                                        <div class="modal_add">Cancel Subscription</div>
                                    </a>
                                <?php } elseif ($model->isMonthlySubscription && $model->isCancelMonthlySubscriptionRequest) { ?>
                                    <a class=" disabled">
                                        <div class="modal_add">Pending approval of cancel subscription</div>
                                    </a>
                                <?php } ?>

                            </div>
                        </span>
                        <span class="hr_line2">
                            <div class="text-left col-xs-7 mar-bot-15">
                                <label class="cont_title_add"><b>Next Billing Date</b><br><?= General::convertSystemToUserTimezone(date('d M Y', strtotime('+1 days', strtotime($studentMonthlySubDetail->end_datetime))), 'd F Y'); ?></label>
                            </div>
                            <?php
                            if (!empty($studentSubDetail)) {
                                $next_renewal_date = strtotime($studentSubDetail['next_renewal_date']);
                                $currentDate = date("Y-m-d");
                                $days_ago = date('Y-m-d', strtotime('-10 days', $next_renewal_date));
                                if ($currentDate >= $days_ago) {
                                    ?>

                                    <div class="text-left col-xs-4 mar-bot-15">
                                        <a href="<?= Yii::$app->getUrlManager()->createUrl('/monthly-subscription/index'); ?>"><div class="modal_add">RENEW</div></a>
                                    </div>
                                    <?php
                                }
                            }
                            ?>

                        </span>
                    </div>
                <?php } ?>
            </div>

        </div>
    </div>
</div>
<!-- /.modal -->
