<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;
use app\models\Tutor;
use app\models\BookedSession;
use app\libraries\General;

$this->title = Yii::t('app', 'COMPLETED LESSONS');
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_icon'] = 'my-lesson-tag.png';

$this->registerJs(
        '
$(".my-datatable-student-completed").dataTable({
                responsive: true,
               "pageLength": dt_page_length,
                "dom": "<\"dataTables_wrapper\" <\"row\" <\"col-sm-12 col-md-4\"l><\"col-sm-12 col-md-4\"B><\"col-sm-12 col-md-4\"f>>  <\"row\" <\"col-sm-12\"t>> <\"row\" <\"col-sm-12 col-md-5\"i> <\"col-sm-12 col-md-7\"p>> >",
                buttons: [
                    "copy", {
			   extend: "csv",
			   footer: false,
			   exportOptions: {
                		columns: [0,1,2,3]
            		   }			  
		       }, 
		       {
			   extend: "excel",
			   footer: false,
			   exportOptions: {
                		columns: [0,1,2,3]
            		   }			  
		       },
			{
			   extend: "pdf",
			   footer: false,
			   exportOptions: {
                		columns: [0,1,2,3]
            		   }			  
		       },
			{
			   extend: "print",
			   footer: false,
			   exportOptions: {
                		columns: [0,1,2,3]
            		   }			  
		       }
                ],
                aLengthMenu: [
                    [10, 25, 50, 100, -1],
                    [10, 25, 50, 100, "All"]
                ]
            });

', View::POS_END
);
?>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <?php /*<div class="row center-block box-search-area">
                        <div class="col-lg-12 col-md-12 col-12 ">
                            <div class="datatable-search-div " style="display: block; margin-bottom: 10px;">
                                <?php
                                $s_c_count = \app\models\CreditSession::getStudentCreditSessionCount(Yii::$app->user->identity->student_uuid);
                                ?>
                                <?= Html::Button(Yii::t('app', 'SCHEDULE A NEW LESSON'), ['class' => 'btn btn-primary mb-0', 'data-creditsession' => $s_c_count, 'id' => 'studentBookingOption_button',]) ?>

                                <label class="pull-right" style="cursor: default;" style="cursor: default;">Lesson Credits = <?= $s_c_count ?> </label>

                            </div>
                        </div>
                    </div> */ ?>
                    <div class="clearfix"></div>
                    <div class="row" >
                        <!---start---->
                        <div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">
                            <table  class="table table-striped dt-responsive display my-datatable-student-completed" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Tutor</th>
                                        <th>Date</th>
                                        <th>Time</th>
                                        <th>Recording</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($completedList)) {
                                        foreach ($completedList as $key => $value) {

                                            $now = date('Y-m-d H:i:s');
                                            ?>
                                            <tr>
                                                <td><?= $key + 1; ?></td>
                                                <td><?= $value['tutorUu']['first_name'] . ' ' . $value['tutorUu']['last_name']; ?></td>
                                                <td><?= General::displayDate($value['start_datetime']); ?></td>
                                                <td><?= General::displayTime($value['start_datetime']) . " - " . General::displayTime($value['end_datetime']); ?></td>
                                                <td>
                                                    <?php
                                                    if ($value['status'] == 'COMPLETED') {
                                                        echo "Under Process";
                                                    }
                                                    if ($value['status'] == 'PUBLISHED' && $value['studentUu']['isMonthlySubscription']) {
                                                        ?>

                                                        <a href="<?= Url::to(['video/tution-view', 'id' => $value['uuid']], true); ?>" class="btn btn-xs btn-primary "><i class="fa fa-video-camera"></i> View Recording</a>
                                                        <?php 
                                                        if($value['publish_date']!='' && $value['publish_date']!=NULL)
                                                        {
                                                                                                                
                                                        $publish_date = strtotime($value['publish_date']);
                                                        $current_date = strtotime(date('Y-m-d H:i:s'));
                                                       

                                                    $months = 0;

                                                    while (strtotime('+1 MONTH', $publish_date) < $current_date) {
                                                        $months++;
                                                        $publish_date = strtotime('+1 MONTH', $publish_date);
                                                    }

                                                       if($months <= 6 ) {?>
                                                    <a href="<?= Url::to(['video/download-recording', 'id' => $value['uuid']], true); ?>" class="btn btn-xs" title="Download Recording"><i class="fa fa-cloud-download" style="font-size:20px;"></i></a>
                                                       <?php }
                                                       }
                                                    ?>
                                                        
                                                        <?php
                                                    } elseif (!$value['studentUu']['isMonthlySubscription']) {
                                                        ?>
                                                        <a href="<?= Url::to(['monthly-subscription/index', 'id' => $value['uuid']], true); ?>" class="btn btn-xs btn-purple">Buy Subscription</a>
                                                        <?php
                                                    }
                                                    ?>
                                                </td>
                                                
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>

                        </div>
                        <!---end--->
                    </div>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <a class="btn btn-primary btn-sm black_button_in" href="<?= Yii::$app->getUrlManager()->createUrl('/student/home'); ?>">RETURN TO HOME</a>

</section>
