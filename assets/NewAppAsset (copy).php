<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class NewAppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        
        'theme_assets/new/css/bootstrap.min.css',
        'http://fonts.googleapis.com/css?family=Lato:400,700',
        'https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome-font-awesome.min.css',
        'theme_assets/plugins/messenger/css/messenger.css',
        'theme_assets/plugins/messenger/css/messenger-theme-flat.css',
        'theme_assets/new/css/style.css',
        'theme_assets/new/css/custom.css',
	
    ];
    public $js = [
        'theme_assets/js/popper.min.js',
        'theme_assets/plugins/bootstrap/js/bootstrap.min.js',
        'theme_assets/plugins/pace/pace.js',
        //'theme_assets/new/js/bootstrap.min.js',
        'theme_assets/new/js/jquery.meanmenu.js',
        'theme_assets/new/js/owl.carousel.min.js',
        'theme_assets/new/js/jquery.sticky.js',
        'theme_assets/new/js/jquery.scrollUp.min.js',
        'theme_assets/new/js/main.js',
        'theme_assets/js/custom.js',
    ];
    public $depends = [
        
    ];
}
