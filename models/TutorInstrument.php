<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tutor_instrument}}".
 *
 * @property string $uuid
 * @property string $tutor_uuid
 * @property string $instrument_uuid
 */
class TutorInstrument extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return '{{%tutor_instrument}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
                [['uuid', 'tutor_uuid'], 'string'],
                [['instrument_uuid'], 'required', 'message' => 'Instrument cannot be blank.'],
                [['instrument_uuid'], 'required', 'on' => 'instrument_selection'],
                // [['instrument_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Instrument::className(), 'targetAttribute' => ['instrument_uuid' => 'uuid']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'tutor_uuid' => Yii::t('app', 'Tutor Uuid'),
            'instrument_uuid' => Yii::t('app', 'Instrument Uuid'),
        ];
    }

    public function getInstrument() {
        return $this->hasOne(Instrument::className(), ['uuid' => 'instrument_uuid'])->from(['instrument' => Instrument::tableName()]);
        ;
    }
    public function getTutor() {
        return $this->hasOne(Tutor::className(), ['uuid' => 'tutor_uuid'])->from(['tutor' => Tutor::tableName()]);
        ;
    }

}
