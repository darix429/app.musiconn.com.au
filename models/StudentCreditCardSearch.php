<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\StudentCreditCard;

/**
 * StudentCreditCardSearch represents the model behind the search form of `app\models\StudentCreditCard`.
 */
class StudentCreditCardSearch extends StudentCreditCard
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'student_uuid', 'number', 'cvv', 'expire_date', 'name', 'status', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $modeDatatable = false, $statusnot = '')
    {
        $query = StudentCreditCard::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        if (!empty($statusnot)) {
            if (is_array($statusnot)) {

                $query->andWhere(['not in', 'status', $statusnot]);
            } else {

                $query->andWhere(['<>', 'status', $statusnot]);
            }
        }
	$query->andFilterWhere(['status' => $this->status]);

        // grid filtering conditions
        $query->andFilterWhere([
            'expire_date' => $this->expire_date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'uuid', $this->uuid])
            ->andFilterWhere(['ilike', 'student_uuid', $this->student_uuid])
            ->andFilterWhere(['ilike', 'number', $this->number])
            ->andFilterWhere(['ilike', 'cvv', $this->cvv])
            ->andFilterWhere(['ilike', 'name', $this->name]);
            //->andFilterWhere(['ilike', 'status', $this->status]);

        if ($modeDatatable) {
            $result = $query->asArray()->orderBy('name')->all();
            return $result;
        }
        return $dataProvider;
    }
}
