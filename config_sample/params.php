<?php
$base_path = realpath(dirname(__FILE__).'/../');
$base_url = 'https://app.musiconn.com.au';

$media_url = 'https://app.musiconn.com.au:9001/';
$media_path = '/var/www/document/';

return [
    'adminEmail' => 'admin@musiconn.com.au',
    'supportEmail' => 'support@musiconn.com',
    'user.passwordResetTokenExpire' => 86400,
    'companyName' => 'Musiconn',
    'timezone' => 'Australia/Sydney',
    'copyright' => "&copy; MUSICONN ".date('Y'),
    'datatable' => [
        'page_length' => 50,
    ],
    'media' => [
        'url' => $media_url,
	'path' => $media_path,
        'pre_recorded_video' => [
            'url' => $media_url . 'pre_recorded_video/',
            'path' => $media_path . 'pre_recorded_video/',
            'thumbnail' => [
                'url' => $media_url . 'pre_recorded_video/thumbnail/',
                'path' => $media_path . 'pre_recorded_video/thumbnail/',
            ],
        ],
        'session_recorded_video' => [
            'url' => $media_url . 'tution_video/',
            'path' => $media_path . 'tution_video/',
            'thumbnail' => [
                'url' => $media_url . 'tution_video/thumbnail/',
                'path' => $media_path . 'tution_video/thumbnail/',
            ],
        ],
        'invoice' => [
            'url' => $media_url . 'invoice/',
            'path' => $media_path . 'invoice/',
        ],
    ],
    'theme_assets' => [
        'url' => $base_url.'/theme_assets/',
        'path' => $base_path.'/web/theme_assets/',
    ],
    'base_path' => $base_path."/",
    'base_url' => $base_url."/index.php/",
    
    'availabilities_type_fixed' => "fixed",
    'book_advance_timeout' => 10,//book lession before xx minutes
    'interval_after_lesson' => 0,//minutes
    'stripe' => [
        'publishable_key' => 'public key',
        'secret_key' => 'secret key',
	'currency' => 'aud',
    ],
    'verto' => [
        //'url' => 'https://192.168.1.195:9002/',//freeswitch
        'url' => $base_url.'/index.php/lesson',
        'lesson_frame_base_url' => 'https://192.168.1.195:9000',
    ],
    'ffmpeg_installation_path' => '/usr/local/bin/ffmpeg',
    'musiconn_admin_mail' => 'admin@musiconn.com.au',
    'musiconn_company_name' => 'Musiconn Pty Ltd',
    'musicoin' => [
        'cent_value' => 1,
        'doller_value' => 0.5, // $get_total_musicoins = ( (1 / $doller_value) * $total_doller_price );
        'coin_cent_value' => 50, //1 coin = 50 cents //this is only display purpose
    ],
    'jitsi' => [
        'video_path' => '/usr/local/recordings/lesson/',
    ],
    'mailchimp' => [
        'url' => 'https://us19.api.mailchimp.com/3.0/',
        'api_key' => 'API-KEY',
    ]
];
