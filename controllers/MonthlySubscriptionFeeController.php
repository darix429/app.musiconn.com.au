<?php

namespace app\controllers;

use Yii;
use app\models\MonthlySubscriptionFee;
use app\models\MonthlySubscriptionFeeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
//rupal
use app\libraries\General;
use yii\helpers\Url;
use yii\filters\AccessControl;

/**
 * MonthlySubscriptionFeeController implements the CRUD actions for MonthlySubscriptionFee model.
 */
class MonthlySubscriptionFeeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
	    'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                        [
                        'actions' => ['index'],
                        'allow' => Yii::$app->user->can('/monthly-subscription-fee/index'),
                        'roles' => ['@'],
                    ],
			[
                        'actions' => ['fee-enabled'],
                        'allow' => Yii::$app->user->can('/monthly-subscription-fee/fee-enabled'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['fee-disabled'],
                        'allow' => Yii::$app->user->can('/monthly-subscription-fee/fee-disabled'),
                        'roles' => ['@'],
                    ],
			[
                        'actions' => ['create'],
                        'allow' => Yii::$app->user->can('/monthly-subscription-fee/create'),
                        'roles' => ['@'],
                    ],
			[
                        'actions' => ['update'],
                        'allow' => Yii::$app->user->can('/monthly-subscription-fee/update'),
                        'roles' => ['@'],
                    ],
                        [
                        'actions' => ['delete'],
                        'allow' => Yii::$app->user->can('/monthly-subscription-fee/delete'),
                        'roles' => ['@'],
                    ],
                        
                ],
            ], 
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    
    public function actionIndex()
    {
        $searchModel = new MonthlySubscriptionFeeSearch();
        $monthlyFeeList = $searchModel->search(Yii::$app->request->queryParams,true);

	if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_monthly_subscription_fees_list', [
                        'monthlyFeeList' => $monthlyFeeList,
            ]);
        } else {
            return $this->render('index', [
                        //'inactiveAdmin' => $inactiveAdmin,
                        'monthlyFeeList' => $monthlyFeeList,
                        'searchModel' => $searchModel,
            ]);
        }
    
    }

    
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    
    public function actionCreate()
    { 
        $model = new MonthlySubscriptionFee();

	$price = Yii::$app->request->post('MonthlySubscriptionFee')['price'];	
	$tax = Yii::$app->request->post('MonthlySubscriptionFee')['tax'];	

        if ($model->load(Yii::$app->request->post())) {
		$model->total_price = $price + $tax;
		$model->created_at = $model->updated_at = date('Y-m-d H:i:s', time());
		if($model->save()){ 
			Yii::$app->session->setFlash('success', Yii::t('app', 'Monthly Subscription Fee successfully created.'));
			return $this->redirect(['index']);
		}
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        
	try{
		$model = $this->findModel($id);

		$price = Yii::$app->request->post('MonthlySubscriptionFee')['price'];	
		$tax = Yii::$app->request->post('MonthlySubscriptionFee')['tax'];		

		if ($model->load(Yii::$app->request->post())) {
			$model->total_price = $price + $tax;
			$model->created_at = $model->updated_at = date('Y-m-d H:i:s', time());
		        if($model->save()){ 
				Yii::$app->session->setFlash('success', Yii::t('app', 'Monthly Subscription Fee successfully updated.'));
				return $this->redirect(['index']);
			}
		}

		return $this->render('update', [
		    'model' => $model,
		]);
	} catch (\yii\base\Exception $exception) {
            throw new \yii\web\HttpException(505, $exception->getMessage());
        }
    }

    
    public function actionDelete()
    {
	
        $id = Yii::$app->getRequest()->post('id');
	if ((($model = MonthlySubscriptionFee::findOne($id)) !== null))
	{
		$model->delete();
		$return = ['code' => 200, 'message' => 'Monthly Subscription Fee deleted successfully.'];
        } else {

            	$return = ['code' => 404, 'message' => 'The requested page does not exist.'];
        }
	Yii::$app->response->format = 'json';
        return $return;
    }

    protected function findModel($id)
    {
        if (($model = MonthlySubscriptionFee::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionFeeDisabled($id) { 

        if (($monthlyfee = MonthlySubscriptionFee::findOne($id)) !== null) {

            if ($monthlyfee->status == "ENABLED") { 

                
                    $monthlyfee->status = 'DISABLED';
                    $monthlyfee->save(false);

                    $return = ['code' => 200, 'message' => 'Monthly Subscription Fee disabled successfully.'];
            } else {
                $return = ['code' => 421, 'message' => 'Monthly Subscription Fee not enabled.'];
            }
        } else {
            $return = ['code' => 404, 'message' => 'Record does not exist.'];
        }

        Yii::$app->response->format = 'json';
        return $return;
    }

    public function actionFeeEnabled($id) {  

        if (($monthlyfee = MonthlySubscriptionFee::findOne($id)) !== null) { 

            if ($monthlyfee->status == "DISABLED") {

               
                    $monthlyfee->status = 'ENABLED';
                    $monthlyfee->save(false);

                    $return = ['code' => 200, 'message' => 'Monthly Subscription Fee enabled successfully.'];
            } else {
                $return = ['code' => 421, 'message' => 'Monthly Subscription Fee not disabled.'];
            }
        } else {
            $return = ['code' => 404, 'message' => 'Record does not exist.'];
        }

        Yii::$app->response->format = 'json';
        return $return;
    } 
    

}
