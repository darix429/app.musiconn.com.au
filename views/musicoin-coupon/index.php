<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;

$this->title = Yii::t('app', 'Musicoin Promo Codes');
$this->params['breadcrumbs'][] = $this->title;


$this->registerJs(
   '
    
    $(document).on("click",".coupon_disabled_link", function(){
        if(confirm("Are you sure, you want to disabled this promo code?")){
        var element = $(this); 
            $.ajax({
                type: "GET",
                url: "'.Url::to(['/musicoin-coupon/coupon-disabled/']).'",
                data: { "id": $(this).data("uuid")},
                //beforeSend:function(){ $(".local-spinner").show(); },
                success:function(result){ 
                    if(result.code == 200){ 
                        $(element).removeClass("badge-primary");
                        $(element).addClass("badge-secondary");
                        
                        $(element).removeClass("coupon_disabled_link");
                        $(element).addClass("coupon_enabled_link");
                        $(element).text("DISABLED");
                        showSuccess(result.message);
                    }else{ 
                        showErrorMessage(result.message);
                    }
                },
                error:function(e){ 
                    showErrorMessage(e.responseText);
                }
            });
        }
    });
    
    $(document).on("click",".coupon_enabled_link", function(){
        if(confirm("Are you sure, you want to enabled this promo code?")){
        var element = $(this);
            $.ajax({
                type: "GET",
                url: "'.Url::to(['/musicoin-coupon/coupon-enabled/']).'",
                data: { "id": $(this).data("uuid")},
                //beforeSend:function(){ $(".local-spinner").show(); },
                success:function(result){
                    if(result.code == 200){
                        $(element).removeClass("badge-secondary");
                        $(element).addClass("badge-primary");
                        
                        $(element).removeClass("coupon_enabled_link");
                        $(element).addClass("coupon_disabled_link");
                        
                        $(element).text("ENABLED");
                        showSuccess(result.message);
                    }else{
                        showErrorMessage(result.message);
                    }
                },
                error:function(e){ 
                    showErrorMessage(e.responseText);
                }
            });
        }
    });
    
//delete code 
    $(document).on("click",".delete_coupon_link", function(){
        if(confirm("Are you sure, you want to delete this promo code?")){
        var element = $(this);
            $.ajax({
                type: "POST",
                url: "'.Url::to(['/musicoin-coupon/delete/']).'",
                data: { "id": $(this).data("uuid")},
                success:function(result){
                    if(result.code == 200){
                    
                        $(element).closest("tr").remove();
                        
                        showSuccess(result.message);
                    }else{
                        showErrorMessage(result.message);
                    }
                },
                error:function(e){ 
                    showErrorMessage(e.responseText);
                }
            });
        }
    });
    
$(document).on("click","#search_btn", function(){        
    $.ajax({
        type: "GET",
        url: "'.Url::to(['/musicoin-coupon/index/']).'",
        data: $("#search_frm").serialize(),
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            $("#coupon_table_parent_div").html(result);
            window.ULTRA_SETTINGS.dataTablesInit();
        },
        error:function(e){ 
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});
    
$(document).on("click","#search_reset_btn", function(){        
    $.ajax({
        type: "GET",
        url: "'.Url::to(['/musicoin-coupon/index/']).'",
        data: {},
        beforeSend:function(){ blockBody(); },
        success:function(result){
            unblockBody();
            $("#coupon_table_parent_div").html(result);
            window.ULTRA_SETTINGS.dataTablesInit();
        },
        error:function(e){ 
            unblockBody();
            showErrorMessage(e.responseText);
        }
    });
});
    
    
',View::POS_END
);
?>

<div class="col-xl-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Promo Code List</h2>
            <div class="actions panel_actions float-right">
                <i class="fa fa-search icon-primary icon-xs icon-orange mail_head_icon custom-toggle" data-togglediv="datatable-search-div" title="Advance Search"></i>
                <a href="<?= Yii::$app->getUrlManager()->createUrl(['/musicoin-coupon/create/'])?>" class="btn btn-primary "><i class="fa fa-plus text-white"></i></a>
            </div>
        </header>
        <div class="content-body"> 
            <div class="row ">
                <div class="col-lg-12 col-md-12 col-12 ">
                    <div class="datatable-search-div " style="display: none;">
                    
                    <?php $form = ActiveForm::begin([
                                'method' => 'get',
                                'options' => [
                                    'id' => 'search_frm',
                                    'class' => 'form-inline'
                                ],
                            ]); ?>
                   	    
		                <div class="col-md-3 mb-0">
		                    <?= $form->field($searchModel, 'code')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Code'])->label(false) ?>
		                </div>
				<div class="col-md-3 mb-0">
		                    <?= $form->field($searchModel, 'coins')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Coins', ])->label(false) ?>
		                </div> 
<!--				<div class="col-md-3 mb-0">
		                    <?php // $form->field($searchModel, 'min_amount')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Min. Amount', 'type' => 'number'])->label(false) ?>
		                </div>-->
                                <div class="col-md-3 mb-0">
		                    <?= $form->field($searchModel, 'start_date')->textInput(['class' => 'form-control custom_date_picker', 'maxlength' => true, 'placeholder' => 'Start Date'])->label(false) ?>
		                </div>
				<div class="col-md-3 mb-0">
		                    <?= $form->field($searchModel, 'end_date')->textInput(['class' => 'form-control custom_date_picker', 'maxlength' => true, 'placeholder' => 'End Date'])->label(false) ?>
		                </div><br><br>
		               <div class="col-md-3 mb-0">
		                    <?= $form->field($searchModel, 'status')->dropDownList(['ENABLED' => 'ENABLED', 'DISABLED' => 'DISABLED'], ['prompt' => 'Select','class' => 'form-control', 'style' => 'width:100%'])->label(false) ?>
		                </div>  
			                       
                        <?= Html::Button(Yii::t('app', 'Search'), ['class' => 'btn btn-primary mb-0', 'id'=>'search_btn','style'=>'margin-left: 17px;']) ?>
                                <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-purple mb-0', 'id'=>'search_reset_btn']) ?>
                    
                    <?php ActiveForm::end(); ?>
                </div>
                </div>
            </div>
            <br>
            <div class=""></div>
            <div class="clearfix"></div>
            <div class="row" id="coupon_table_parent_div">
                <?php echo Yii::$app->controller->renderPartial('_coupon_list', ['couponList' => $couponList]); ?>
            </div>
        </div>
    </section> 
</div>

