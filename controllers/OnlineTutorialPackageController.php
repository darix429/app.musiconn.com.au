<?php

namespace app\controllers;

use Yii;
use app\models\OnlineTutorialPackage;
use app\models\OnlineTutorialPackageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
//rupal
use app\libraries\General;
use yii\helpers\Url;
use yii\filters\AccessControl;
use app\models\PaymentTerm;
use app\models\TutorialType;
use app\models\PremiumStandardPlanRate;
use app\models\StandardPlanOptionRate;
use app\models\OnlineTutorialPackageType;
use app\models\TutorStandardRateHistorySearch;

/**
 * OnlineTutorialPackageController implements the CRUD actions for OnlineTutorialPackage model.
 */
class OnlineTutorialPackageController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => Yii::$app->user->can('/online-tutorial-package/index'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['package-enabled'],
                        'allow' => Yii::$app->user->can('/online-tutorial-package/package-enabled'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['package-disabled'],
                        'allow' => Yii::$app->user->can('/online-tutorial-package/package-disabled'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['create'],
                        'allow' => Yii::$app->user->can('/online-tutorial-package/create'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['get-tutorial-type'],
                        'allow' => true,
                    //'allow' => Yii::$app->user->can('/online-tutorial-package/get-tutorial-type'),
                    //'roles' => ['@'],
                    ],
                    [
                        'actions' => ['update'],
                        'allow' => Yii::$app->user->can('/online-tutorial-package/update'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => Yii::$app->user->can('/online-tutorial-package/delete'),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['get-rate-option-history'],
                        'allow' => Yii::$app->user->can('/online-tutorial-package/get-rate-option-history'),
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OnlineTutorialPackage models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new OnlineTutorialPackageSearch();
        $packegetypeModel = \app\models\OnlineTutorialPackageType::find()->all();

        $searchModel->status = 'ENABLED';
        $tutorialPackageList = $searchModel->search(Yii::$app->request->queryParams, true);

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_online_tutorial_packages_list', [
                        'tutorialPackageList' => $tutorialPackageList,
                        'packegetypeModel' => $packegetypeModel
            ]);
        } else {
            return $this->render('index', [
                        //'inactiveAdmin' => $inactiveAdmin,
                        'tutorialPackageList' => $tutorialPackageList,
                        'searchModel' => $searchModel,
                        'packegetypeModel' => $packegetypeModel
            ]);
        }
        /* return $this->render('index', [
          'searchModel' => $searchModel,
          'dataProvider' => $dataProvider,
          ]); */
    }

    /**
     * Displays a single OnlineTutorialPackage model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new OnlineTutorialPackage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new OnlineTutorialPackage();
        $packegetypeModel = \app\models\OnlineTutorialPackageType::find()->all();
        $premiumstandardplanmodel = new PremiumStandardPlanRate();

        $model->attributes = Yii::$app->getRequest()->post('OnlineTutorialPackage');

        if ($model->load(Yii::$app->request->post()) && $premiumstandardplanmodel->load(Yii::$app->request->post()) && $model->validate()) {

            //echo "<pre>";
            //print_r(Yii::$app->request->post());
            $model->payment_term_uuid = (!empty($model->payment_term_uuid)) ? $model->payment_term_uuid : NULL;

            $premiumstandardplanmodel->name = $model->name;
            $premiumstandardplanmodel->description = $model->description;
            $premiumstandardplanmodel->payment_term_uuid = $model->payment_term_uuid;
            $premiumstandardplanmodel->tutorial_type_uuid = $model->tutorial_type_uuid;

            $currentupdatepackgetype = OnlineTutorialPackageType::find()
                            ->where(['uuid' => $premiumstandardplanmodel->online_tutorial_package_type_uuid])->one();
            $premiumstandardplanmodel->plantext = $currentupdatepackgetype->name;

            if ($premiumstandardplanmodel->plantext == "STANDARD") {

                $option = $_POST['PremiumStandardPlanRate']['standard_plan_options_option_' . $premiumstandardplanmodel->default_option];
                $price = $_POST['PremiumStandardPlanRate']['standard_plan_options_price_' . $premiumstandardplanmodel->default_option];
                $gst = $_POST['PremiumStandardPlanRate']['standard_plan_options_gst_' . $premiumstandardplanmodel->default_option];
                $gst_description =  $_POST['PremiumStandardPlanRate']['standard_plan_options_gst_description_' . $premiumstandardplanmodel->default_option];
                
                $premiumstandardplanmodel->price = $price;
                $premiumstandardplanmodel->gst = $gst;
                $premiumstandardplanmodel->total_price = $price + $gst;
                $premiumstandardplanmodel->gst_description = $gst_description;

                $model->price = $price;
                $model->gst = $gst;
                $model->total_price = $price + $gst;
                $model->gst_description  = $gst_description;
            } else {

                $model->price = $premiumstandardplanmodel->price;
                $model->gst = $premiumstandardplanmodel->gst;
                $model->total_price = $premiumstandardplanmodel->price + $premiumstandardplanmodel->gst;
                $model->gst_description  = $premiumstandardplanmodel->gst_description;
                $premiumstandardplanmodel->default_option = '0';
            }


            if ($model->validate() && $premiumstandardplanmodel->validate()) {
                
                $PaymentTermModel = PaymentTerm::findOne($model->payment_term_uuid);
                
                $TutorialTypeModel = TutorialType::find()->where(['uuid' => $model->tutorial_type_uuid])->one();
                $pt_code = (!empty($PaymentTermModel)) ? $PaymentTermModel->code : '';
                $tt_code = (!empty($TutorialTypeModel)) ? $TutorialTypeModel->type : '';

                $model->total_price = $model->price + $model->gst;
                $model->duration = (!empty($PaymentTermModel)) ? $PaymentTermModel->unit : 1;
                $premiumstandardplanmodel->duration = $model->duration;
                //$model->duration = PaymentTerm::getPaymentTermWiseTotalSessonCount($pt_code, $tt_code);
                //$premiumstandardplanmodel->duration = PaymentTerm::getPaymentTermWiseTotalSessonCount($pt_code, $tt_code);
                $model->created_at = $model->updated_at = date('Y-m-d H:i:s', time());
                if ($model->save(false)) {
                    $premiumstandardplanmodel->online_tutorial_package_uuid = $model->getPrimaryKey();

                    if ($premiumstandardplanmodel->save(false)) {

                        if ($premiumstandardplanmodel->plantext == "STANDARD") {

                            for ($x = 1; $x <= 6; $x++) {
                                //foreach ($premiumstandardplanmodel->standard_plan_options as $key => $value) {
                                $option = $_POST['PremiumStandardPlanRate']['standard_plan_options_option_' . $x];
                                $price = $_POST['PremiumStandardPlanRate']['standard_plan_options_price_' . $x];
                                $gst = $_POST['PremiumStandardPlanRate']['standard_plan_options_gst_' . $x];
                                $gst_description  = $_POST['PremiumStandardPlanRate']['standard_plan_options_gst_description_' . $x];


                                $StandardPlanOptionRate_update = new StandardPlanOptionRate();
                                $StandardPlanOptionRate_update->premium_standard_plan_rate_uuid = $premiumstandardplanmodel->getPrimaryKey();
                                $StandardPlanOptionRate_update->price = $price;
                                $StandardPlanOptionRate_update->option = $x;
                                $StandardPlanOptionRate_update->gst = $gst;
                                $StandardPlanOptionRate_update->total_price = $price + $gst;
                                $StandardPlanOptionRate_update->gst_description = $gst_description;
                                $StandardPlanOptionRate_update->save(false);
                            }
                        }
                        
                        $data = [
                            'premium_standard_plan_rate_uuid' => $premiumstandardplanmodel->getPrimaryKey(),
                        ];
                        \app\models\OnlineTutorialPackageTutorwise::tutorWiseNewOnlinePlanEntry_assigntoTutor($data);

                        Yii::$app->session->setFlash('success', Yii::t('app', 'Online Tutorial Package successfully created.'));
                        return $this->redirect(['index']);
                    } 
                }
            } else {
//                 echo "<pre>";
//                 print_r($model->getErrors());
//                 print_r($premiumstandardplanmodel->getErrors());
//                 exit;
            }
        }

        return $this->render('create', [
                    'model' => $model,
                    'packegetypeModel' => $packegetypeModel,
                    'premiumstandardplanmodel' => $premiumstandardplanmodel,
        ]);
    }

    /**
     * Updates an existing OnlineTutorialPackage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        try {
            $premiumstandardplanmodel = PremiumStandardPlanRate::findOne($id);
            $currentpackgetype = OnlineTutorialPackageType::find()
                            ->where(['uuid' => $premiumstandardplanmodel->online_tutorial_package_type_uuid])->one();
            $premiumstandardplanmodel->plantext = $currentpackgetype->name;
            $model = $this->findModel($premiumstandardplanmodel->online_tutorial_package_uuid);
            $packegetypeModel = \app\models\OnlineTutorialPackageType::find()->all();
            $StandardPlanOptionRateList = StandardPlanOptionRate::find()
                            ->where(['premium_standard_plan_rate_uuid' => $id])
                            ->orderBy([
                                'option' => SORT_ASC
                            ])->asArray()->all();


            if (in_array(Yii::$app->user->identity->role, ['ADMIN'])) {
                $model->scenario = 'admin_update_tutorial';
            }
            $model->attributes = Yii::$app->getRequest()->post('OnlineTutorialPackage');

            if ($model->load(Yii::$app->request->post()) && $premiumstandardplanmodel->load(Yii::$app->request->post()) && in_array(Yii::$app->user->identity->role, ['OWNER'])) {
                
                $model->payment_term_uuid = (!empty($model->payment_term_uuid)) ? $model->payment_term_uuid : NULL;
                
                $premiumstandardplanmodel->name = $model->name;
                    $premiumstandardplanmodel->description = $model->description;
                    $premiumstandardplanmodel->payment_term_uuid = $model->payment_term_uuid;
                    $premiumstandardplanmodel->status = $model->status;
                    $premiumstandardplanmodel->tutorial_type_uuid = $model->tutorial_type_uuid;
                if ($model->validate() && $premiumstandardplanmodel->validate()) {


                    $currentupdatepackgetype = OnlineTutorialPackageType::find()
                                    ->where(['uuid' => $premiumstandardplanmodel->online_tutorial_package_type_uuid])->one();
                    $plantype = $currentupdatepackgetype->name;

                    if ($plantype == "STANDARD") {

                        for ($x = 1; $x <= 6; $x++) {
                            //foreach ($premiumstandardplanmodel->standard_plan_options as $key => $value) {
                            $option = $_POST['PremiumStandardPlanRate']['standard_plan_options_option_' . $x];
                            $price = $_POST['PremiumStandardPlanRate']['standard_plan_options_price_' . $x];
                            $gst = $_POST['PremiumStandardPlanRate']['standard_plan_options_gst_' . $x];
                            $gst_description  = $_POST['PremiumStandardPlanRate']['standard_plan_options_gst_description_' . $x];

                            $StandardPlanOptionRate_update = StandardPlanOptionRate::find()
                                            ->where(['premium_standard_plan_rate_uuid' => $id, 'option' => $option])->one();

                            $StandardPlanOptionRate_update->price = $price;
                            $StandardPlanOptionRate_update->gst = $gst;
                            $StandardPlanOptionRate_update->total_price = $price + $gst;
                            $StandardPlanOptionRate_update->gst_description = $gst_description;
                            $StandardPlanOptionRate_update->save(false);
                            if ($premiumstandardplanmodel->default_option == $option) {
                                $premiumstandardplanmodel->price = $price;
                                $premiumstandardplanmodel->gst = $gst;
                                $premiumstandardplanmodel->total_price = $price + $gst;
                                $premiumstandardplanmodel->gst_description = $gst_description;

                                $model->price = $price;
                                $model->gst = $gst;
                                $model->total_price = $price + $gst;
                                $model->gst_description = $gst_description;
                            }
                        }
                    } else {


                        $model->price = $premiumstandardplanmodel->price;
                        $model->gst = $premiumstandardplanmodel->gst;
                        $model->total_price = $premiumstandardplanmodel->price + $premiumstandardplanmodel->gst;
                        $premiumstandardplanmodel->default_option = '0';
                        $model->gst_description = $premiumstandardplanmodel->gst_description;
                    }

                    $data = [
                            'premium_standard_plan_rate_uuid' => $premiumstandardplanmodel->getPrimaryKey(),
                        ];
                        \app\models\OnlineTutorialPackageTutorwise::tutorWiseNewOnlinePlanEntry_assigntoTutor($data);

                    $PaymentTermModel = PaymentTerm::find()->where(['uuid' => $model->payment_term_uuid])->one();
                    $TutorialTypeModel = TutorialType::find()->where(['uuid' => $model->tutorial_type_uuid])->one();
                    $pt_code = (!empty($PaymentTermModel)) ? $PaymentTermModel->code : '';
                    $tt_code = (!empty($TutorialTypeModel)) ? $TutorialTypeModel->type : '';

                    $model->duration = (!empty($PaymentTermModel)) ? $PaymentTermModel->unit : 1;
                    $premiumstandardplanmodel->duration = $model->duration;
                    //$model->duration = PaymentTerm::getPaymentTermWiseTotalSessonCount($pt_code, $tt_code);
                    //$premiumstandardplanmodel->duration = PaymentTerm::getPaymentTermWiseTotalSessonCount($pt_code, $tt_code);
                    
                    $premiumstandardplanmodel->created_at = $model->updated_at = date('Y-m-d H:i:s', time());
                    $model->created_at = $model->updated_at = date('Y-m-d H:i:s', time());
                    if ($model->save() && $premiumstandardplanmodel->save()) {
                        Yii::$app->session->setFlash('success', Yii::t('app', 'Online Tutorial Package successfully updated.'));
                        return $this->redirect(['index']);
                    }
                } else {
                    //echo "<pre>";
                    //print_r($premiumstandardplanmodel->getErrors());exit;
                }
            } elseif ($model->load(Yii::$app->request->post()) && in_array(Yii::$app->user->identity->role, ['ADMIN'])) {
                $premiumstandardplanmodel->name = $model->name;
                    $premiumstandardplanmodel->description = $model->description;
                    $premiumstandardplanmodel->payment_term_uuid = $model->payment_term_uuid;
                    $premiumstandardplanmodel->status = $model->status;
                    $premiumstandardplanmodel->tutorial_type_uuid = $model->tutorial_type_uuid;
                if ($model->validate() && $premiumstandardplanmodel->validate()) {


                    $currentupdatepackgetype = OnlineTutorialPackageType::find()
                                    ->where(['uuid' => $premiumstandardplanmodel->online_tutorial_package_type_uuid])->one();
                    $plantype = $currentpackgetype->name;

                    if ($plantype == "STANDARD") {

                        for ($x = 1; $x <= 6; $x++) {
                            //foreach ($premiumstandardplanmodel->standard_plan_options as $key => $value) {
                            $option = $_POST['PremiumStandardPlanRate']['standard_plan_options_option_' . $x];
                            $price = $_POST['PremiumStandardPlanRate']['standard_plan_options_price_' . $x];
                            $gst = $_POST['PremiumStandardPlanRate']['standard_plan_options_gst_' . $x];
                            $gst_description  = $_POST['PremiumStandardPlanRate']['standard_plan_options_gst_description_' . $x];


                            $StandardPlanOptionRate_update = StandardPlanOptionRate::find()
                                            ->where(['premium_standard_plan_rate_uuid' => $id, 'option' => $option])->one();

                            $StandardPlanOptionRate_update->price = $price;
                            $StandardPlanOptionRate_update->gst = $gst;
                            $StandardPlanOptionRate_update->total_price = $price + $gst;
                            $StandardPlanOptionRate_update->gst_description = $gst_description;
                            $StandardPlanOptionRate_update->save(false);
                            if ($premiumstandardplanmodel->default_option == $option) {
                                $premiumstandardplanmodel->price = $price;
                                $premiumstandardplanmodel->gst = $gst;
                                $premiumstandardplanmodel->total_price = $price + $gst;
                                $premiumstandardplanmodel->gst_description = $gst_description;

                                $model->price = $price;
                                $model->gst = $gst;
                                $model->total_price = $price + $gst;
                                $model->gst_description = $gst_description;
                            }
                        }
                    } else {


                        $model->price = $premiumstandardplanmodel->price;
                        $model->gst = $premiumstandardplanmodel->gst;
                        $model->total_price = $premiumstandardplanmodel->price + $premiumstandardplanmodel->gst;
                        $premiumstandardplanmodel->default_option = '0';
                        $model->gst_description = $premiumstandardplanmodel->gst_description;
                    }

                    $data = [
                            'premium_standard_plan_rate_uuid' => $premiumstandardplanmodel->getPrimaryKey(),
                        ];
                        \app\models\OnlineTutorialPackageTutorwise::tutorWiseNewOnlinePlanEntry_assigntoTutor($data);

                    $PaymentTermModel = PaymentTerm::find()->where(['uuid' => $model->payment_term_uuid])->one();
                    $TutorialTypeModel = TutorialType::find()->where(['uuid' => $model->tutorial_type_uuid])->one();
                    $pt_code = (!empty($PaymentTermModel)) ? $PaymentTermModel->code : '';
                    $tt_code = (!empty($TutorialTypeModel)) ? $TutorialTypeModel->type : '';

                    $model->duration = (!empty($PaymentTermModel)) ? $PaymentTermModel->unit : 1;
                    $premiumstandardplanmodel->duration = $model->duration;
                    //$model->duration = PaymentTerm::getPaymentTermWiseTotalSessonCount($pt_code, $tt_code);
                    //$premiumstandardplanmodel->duration = PaymentTerm::getPaymentTermWiseTotalSessonCount($pt_code, $tt_code);
                    
                    $premiumstandardplanmodel->created_at = $model->updated_at = date('Y-m-d H:i:s', time());
                    $model->created_at = $model->updated_at = date('Y-m-d H:i:s', time());
                    if ($model->save() && $premiumstandardplanmodel->save()) {
                        Yii::$app->session->setFlash('success', Yii::t('app', 'Online Tutorial Package successfully updated.'));
                        return $this->redirect(['index']);
                    }
                } else {
                    //echo "<pre>";
                    //print_r($model->getErrors());
                    //print_r($premiumstandardplanmodel->getErrors());exit;
                }
            }

            if (in_array(Yii::$app->user->identity->role, ['ADMIN'])) {
                return $this->render('update_admin', [
                            'model' => $model,
                            'premiumstandardplanmodel' => $premiumstandardplanmodel,
                            'packegetypeModel' => $packegetypeModel,
                            'StandardPlanOptionRateList' => $StandardPlanOptionRateList
                ]);
            } else {
                return $this->render('update', [
                            'model' => $model,
                            'premiumstandardplanmodel' => $premiumstandardplanmodel,
                            'packegetypeModel' => $packegetypeModel,
                            'StandardPlanOptionRateList' => $StandardPlanOptionRateList
                ]);
            }
        } catch (\yii\base\Exception $exception) {
            throw new \yii\web\HttpException(505, $exception->getMessage());
        }
    }

    /**
     * Deletes an existing OnlineTutorialPackage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete() {
        $id = Yii::$app->getRequest()->post('id');

        $premiumstandardplanmodel = PremiumStandardPlanRate::findOne($id);



        if ((($premiumstandardplanmodel = PremiumStandardPlanRate::findOne($id)) !== null)) {
            OnlineTutorialPackageTutorwise::deleteAll(['premium_standard_plan_rate_uuid' => $id]);
            StandardPlanOptionRate::deleteAll(['premium_standard_plan_rate_uuid' => $id]);
            OnlineTutorialPackage::deleteAll(['uuid' => $premiumstandardplanmodel->online_tutorial_package_uuid]);
            $return = ['code' => 200, 'message' => 'Tutorial Package deleted successfully.'];
        } else {
            $return = ['code' => 404, 'message' => 'The requested page does not exist.'];
        }

        Yii::$app->response->format = 'json';
        return $return;
    }

    /**
     * Finds the OnlineTutorialPackage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return OnlineTutorialPackage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = OnlineTutorialPackage::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionPackageDisabled($id) {

        if (($tutorialpackage = PremiumStandardPlanRate::findOne($id)) !== null) {   
            

            if ($tutorialpackage->status == "ENABLED") {
                $model = OnlineTutorialPackage::findOne($tutorialpackage->online_tutorial_package_uuid);
                $model->status =  'DISABLED';
                $model->save(false);

                $tutorialpackage->status = 'DISABLED';
                $tutorialpackage->save(false);

                $return = ['code' => 200, 'message' => 'Online Tutorial Package disabled successfully.'];
            } else {
                $return = ['code' => 421, 'message' => 'Online Tutorial Package not enabled.'];
            }
        } else {
            $return = ['code' => 404, 'message' => 'Record does not exist.'];
        }

        Yii::$app->response->format = 'json';
        return $return;
    }

    public function actionPackageEnabled($id) {

        if (($tutorialpackage = PremiumStandardPlanRate::findOne($id)) !== null) {

            if ($tutorialpackage->status == "DISABLED") {
                $model = OnlineTutorialPackage::findOne($tutorialpackage->online_tutorial_package_uuid);
                $model->status =  'DISABLED';
                $model->save(false);

                $tutorialpackage->status = 'ENABLED';
                $tutorialpackage->save(false);

                $return = ['code' => 200, 'message' => 'Online Tutorial Package enabled successfully.'];
            } else {
                $return = ['code' => 421, 'message' => 'Online Tutorial Package not disabled.'];
            }
        } else {
            $return = ['code' => 404, 'message' => 'Record does not exist.'];
        }

        Yii::$app->response->format = 'json';
        return $return;
    }

    public function actionGetTutorialType($id) {

        $data = "<option value=''>Select Tutorial Type</option>";
        $return = ['code' => 404, 'message' => 'Data not found'];

        $res = self::GetTutorialType($id);
        if (!empty($res)) {


            if (!empty($res)) {
                foreach ($res as $key => $value) {
                    $data .= "<option value='" . $key . "'>" . $value . "</option>";
                }
            }
            $return = ['code' => 200, 'message' => 'Record found', 'data' => $data];
        }

        Yii::$app->response->format = 'json';
        return $return;
    }

    public function GetTutorialType($id) {

        $return = [];
        if (($query = PaymentTerm::find()->where(['uuid' => $id])->one()) !== null) {

            $where = "1=1";
            if ($query->term == "Fortnightly") {
                $where = ['type' => 'F', 'status' => 'ENABLED'];
            }
            /* if($query->term == "Monthly" || $query->term == "Quarterly") {
              $where = ['type' => 'W', 'status'=>'ENABLED'];
              }elseif($query->term == "Fortnightly") {
              $where = ['type' => 'F', 'status'=>'ENABLED'];
              }else{
              $where = "1=1";
              } */
            $return = \yii\helpers\ArrayHelper::map(TutorialType::find()->where($where)->asArray()->all(), 'uuid', 'description');
        }
        return $return;
    }
    
    
    public function actionGetRateOptionHistory()
    {
        $searchModel = new TutorStandardRateHistorySearch();
        $searchModel->datetime = date('01-m-Y').' - '.date('t-m-Y');
        
        $historyList = $searchModel->search(Yii::$app->request->queryParams, true);

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_tutor_stanadard_rate_history_list', [
                        'historyList' => $historyList,
            ]);
        } else {
            return $this->render('tutor_stanadard_rate_history_report', [
                        'historyList' => $historyList,
                        'searchModel' => $searchModel,

            ]);
        }
    }
}
