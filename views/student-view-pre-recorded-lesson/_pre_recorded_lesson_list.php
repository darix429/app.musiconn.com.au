<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use app\libraries\General;
use app\models\OnlineTutorialPackageType;
use app\models\StudentViewPreRecordedLesson;

?>
<div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">
    <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Title</th>
                <th>Instrument</th>
                <th>Tutor</th>
                <th>Number of Views</th>
                <th>Action</th>
            </tr>
        </thead>

        <tbody>
            <?php
            if (!empty($lessonList)) {
                foreach ($lessonList as $key => $value) {
                                $total_view_query = StudentViewPreRecordedLesson::find()->where(["pre_recorded_video_uuid" => $value['pre_recorded_video_uuid']]);

                                        
                    if (!is_null($searchModel->datetime) && 
            strpos($searchModel->datetime, ' - ') !== false ) {
            list($start_date, $end_date) = explode(' - ', $searchModel->datetime);
            

            $total_view_query->andWhere(['>=', 'datetime', date("Y-m-d", strtotime($start_date))." 00:00:00"]);
            $total_view_query->andWhere(['<=', 'datetime', date("Y-m-d", strtotime($end_date))." 23:59:59"]);
            

            
            } 
        
        $total_view = $total_view_query->count();
                   
                        ?>
                    <tr>
                        <td><?= $key + 1; ?></td>

                        <td> <?= $value['title']; ?>
                        </td>
                        <td><?= $value['instrumentUu']['name']; ?> </td>
                        <td><?= $value['tutorUu']['first_name'].' '.$value['tutorUu']['last_name']; ?></td>
                        <td><?= $total_view; ?></td>                       
                        <td><a href="<?= Url::to(['/student-view-pre-recorded-lesson/view/', 'id' => $value['pre_recorded_video_uuid']]) ?>" class="badge badge-info badge-md" title="View"><i class="fa fa-eye"></i></a></td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>

</div>
