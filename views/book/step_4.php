<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Url;
use app\libraries\General;
use app\models\StudentTuitionBooking;
use app\models\Instrument;
use app\models\Tutor;

$this->title = Yii::t('app', 'BOOK A MUSIC LESSON');
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_icon'] = 'my-lesson-tag.png';

if (!empty($step_data['step_1']['instrument_uuid'])) {
    $instrumentDetail = Instrument::find()->where(['uuid' => $step_data['step_1']['instrument_uuid']])->one();
} else {
    return \yii\web\Controller::redirect(['index']);
}
//print_r($step_data['step_2']);
if (isset($step_data['step_2']['tutor_uuid']) && $step_data['step_2']['tutor_uuid'] != "") {
    $tutorDetail = Tutor::findOne($step_data['step_2']['tutor_uuid']);
    $model->tutor_uuid = $step_data['step_2']['tutor_uuid'];
    $model->tutorcheckbox = $model->tutor_uuid;
} else {
    return \yii\web\Controller::redirect(['index']);
}

if (empty($step_data['step_3']['lesson_length']) || empty($step_data['step_3']['lesson_total'])) {
    return \yii\web\Controller::redirect(['index']);
}
if (empty($_POST['Book'])) {
   //$model->session_when_start = $step_data['step_4']['session_when_start'];
   //$model->start_hour = $step_data['step_4']['start_hour'];
   $selecteddate = date("Y-m-d",strtotime($step_data['step_4']['session_when_start']));

}else{
 $selecteddate = date("Y-m-d");
}
if(empty($model->session_when_start)){ $model->session_when_start = $selecteddate;}
$student_uuid = $model->student_uuid;
$tutor_half_hour_price = $step_data['step_2']['tutor_half_hour_price'];
$tutor_hour_price = $step_data['step_2']['tutor_hour_price'];

$model_start_hour = $model->start_hour;
$start_hour_json = (isset($step_data['step_4']['start_hour_array'])) ? $step_data['step_4']['start_hour_array'] : '{}';
$this->registerJs(
        '
    
$( "#frm_step_4" ).submit(function( event ) {
    blockBody();
});

//Define datepicker and onchange datepicker
//var todaydate = new Date();
var todaydate = "' . date("Y-m-d") . '";
var selecteddate = "' . $selecteddate . '";
//alert(todaydate)
$("#new_when_startdate_div").datepicker({
    inline: true,
    //todayBtn: true,
    format: "yyyy-mm-dd",
    viewSelect: "Default",
    startDate:todaydate,
    todayHighlight:false
}).on("changeDate", function(e) {
    $("#book-session_when_start").val( e.format() );
    clear_selected_data();
    get_timeslots();
});

$("#new_when_startdate_div").datepicker("update", selecteddate);
get_timeslots();
function get_timeslots(){
    var whendate = $("#book-session_when_start").val();
    if(whendate !== ""){
        $.ajax({
            type: "GET",
            url: "' . Url::to(['book/get-timeslots']) . '",
            data: {"selected_when_date":whendate,"student_uuid":"' . $student_uuid . '"},
            beforeSend: function() {
                blockBody();
            },
            success:function(result){
                $("#book-start_hour").html(result.html);
                $("#start_hour_array").val(JSON.stringify(result.result));
                NEW_CUSTOM.iCheck();
                unblockBody();
            },error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
    }
}

function select_time(tm){
    if(tm !== ""){
        $.ajax({
            type: "POST",
            url: "' . Url::to(['book/select-time']) . '",
            data: {"selected_time":tm,"student_uuid":"' . $student_uuid . '"},
            beforeSend: function() {
                blockBody();
            },
            success:function(result){

                $("#selected_date_data_parent").html(result.html);
                $(".selected_sesson_html").val(result.html);
                $("#book-all_lesson_data").val(JSON.stringify(result.all_lesson_data));
                $("#btn_next_btn").show();
                unblockBody();
            },error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
    }
}

function clear_selected_data(){
    $("#selected_date_data_parent").html("<div><strong>No Lesson Available</strong></div>");
    $(".selected_sesson_html").val("<div><strong>No Lesson Available</strong></div>");
    $("#book-all_lesson_data").val("");
    $("#btn_next_btn").hide();
}

//Open change tutor for perticuler date
$(document).on("click",".rebook_btn",function(){
    open_change_booking_model($(this).data("time"));
})

function open_change_booking_model(cdate = ""){

    if(cdate !== ""){
        all_lesson_data = $("#book-all_lesson_data").val();
        $.ajax({
            type: "GET",
            url: "' . Url::to(['book/change-booking']) . '",
            data: {"current_datetime":cdate,"student_uuid":"' . $student_uuid . '","tutor_hour_price":"' . $tutor_hour_price . '","tutor_half_hour_price":"' . $tutor_half_hour_price . '","all_lesson_data":all_lesson_data},
            beforeSend: function() {
                blockBody();
            },
            success:function(result){
                $(".change-booking-model-body").html(result);
                NEW_CUSTOM.iCheck();
                $("#change-booking-model").modal({show: true});
                unblockBody();
            },error:function(e){
                unblockBody();
                showErrorMessage(e.responseText);
            }
        });
    }
}
    

 
', View::POS_END
);
?>
<style>
    .rebook_btn{
        margin: 1px;
        padding: 5px 10px 4px;
        border: 1px solid #000;
    }
    .text-red{
        color: #e90c07;
    }
    #new_when_startdate_div .datepicker{
        min-width: unset !important;
        max-width: unset !important;
    }
    .active.day.book_sesson{
        background-image: linear-gradient(to bottom, #989898, #000000);
    }
    .active.day.book_sesson:hover{
        background-image: linear-gradient(to bottom, #000000, #989898);
    }
</style>
<?= Yii::$app->controller->renderPartial('step_prev_booking', ['model' => $model]); ?>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-12"><strong>BOOK A NEW MUSIC LESSON</strong></div><br>
            <div class="panel-group box-header with-border" id="accordion" role="tablist" aria-multiselectable="true">

                <!-- SELECT INSTRUMENT -->
                <div class="panel panel-default">

                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseInstrument" aria-expanded="false" aria-controls="collapseTwo">
                                SELECT INSTRUMENT 
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </a>
                            <span class="pull-right"><?= $instrumentDetail->name; ?> <a href="javascript:$('#gostep1').submit();"><div class="label label-warning hm-1 mr-2 siba">CHANGE</div></a></span>
                        </h4>
                    </div>
                </div>
                <!-- END SELECT INSTRUMENT -->

                <!-- SELECT TUTOR -->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseLength" aria-expanded="false" aria-controls="collapseThree">
                                SELECT TUTOR
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </a>
                            <span class="pull-right"><?= $tutorDetail->first_name . ' ' . $tutorDetail->last_name; ?> <a href="javascript:$('#gostep2').submit();"><div class="label label-warning hm-1 mr-2 siba">CHANGE</div></a></span>
                        </h4>
                    </div>

                </div>
                <!-- END SELECT TUTOR -->

                <!-- SELECT LENGTH OF LESSON -->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingfour">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseLength" aria-expanded="false" aria-controls="collapsefour">
                                SELECT LENGTH OF LESSON
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </a>
                            <span class="pull-right"><?= $step_data['step_3']['lesson_length'] . ' Mins - ' . $step_data['step_3']['lesson_total'] . ($step_data['step_3']['lesson_total'] > 1? ' Lessons' : ' Lesson'); ?> <a href="javascript:$('#gostep3').submit();"><div class="label label-warning hm-1 mr-2 siba">CHANGE</div></a></span>
                        </h4>
                    </div>
                </div>
                <!--END SELECT LENGTH OF LESSON -->

                <!-- SELECT DATE AND TIME -->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingfive">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseDate" aria-expanded="false" aria-controls="collapsefive">
                                SELECT DATE AND TIME
                            </a>
                        </h4>
                    </div>
                    <div class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingFour">
                        <?php
                        $form = ActiveForm::begin([
                                    'action' => ['index'],
                                    'id' => 'frm_step_4',
                                    'enableClientValidation' => true,
                                    'validateOnChange' => true,
                                    'options' => ['class' => 'form-horizontal']]);
                        ?>
                        <div class="panel-body">
                            <!--                            <div class="container-fluid sst-ff">
                                                            <div class="col-lg-2 col-sm-4 col-xs-12">
                                                                <ul class="products-list product-list-in-box">
                                                                    <li class="st-p st-p2">
                                                                        <label class="contai_r">
                                                                            <span class="checkmark"></span></label><span class="">Selected Day</span>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-lg-3 col-sm-4 col-xs-12">
                                                                <ul class="products-list product-list-in-box">
                                                                    <li class="st-p st-p2">
                                                                        <label class="contai_r">
                                                                            <span class="checkmark green_d"></span></label> <span class="">Selected tutor availability</span>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-lg-2 col-sm-4 col-xs-12">
                                                                <ul class="products-list product-list-in-box">
                                                                    <li class="st-p st-p2">
                                                                        <label class="contai_r">
                                                                            <span class="checkmark yellow_d"></span></label> <span class="">Other available tutor</span>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-lg-5 col-sm-4 col-xs-12">
                                                                <ul class="products-list product-list-in-box">
                                                                    <li class="st-p st-p2">
                                                                        <label class="contai_r">
                                                                            <span class="checkmark gray_d"></span></label> <span class="">Sorry no tutors available at this time offering the same rate</span>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>-->


                            <div class="col-md-4">
                                <ul class="products-list product-list-in-box">
                                    <li>
                                        <div id="new_when_startdate_div"></div>
                                        <?php echo $form->field($model, 'session_when_start')->hiddenInput(['class' => 'form-control session_when_start', 'maxlength' => true, ])->label(false) ?>

                                    </li>
                                </ul>
                            </div>

                            <div class="col-md-4" style="border: 1px solid #ccc;padding: 7px 0px; border-radius: 30px;">
                                <div class="col-md-12"><div class="sst_btn">Select Start Time</div><br></div>


                                <?php
//                                $model_start_hour = $model->start_hour;
//                                $start_hour_json = (isset($step_data['step_4']['start_hour_array'])) ? $step_data['step_4']['start_hour_array'] : '{}';
                                $start_hour_array = json_decode($start_hour_json, true);
                                $start_hour_array = (!empty($start_hour_array)) ? $start_hour_array : [0 => 'dummy'];
                                echo $form->field($model, 'start_hour', ['template' => '{input}<div class="col-md-12">{error}</div>', 'options' => ['tag' => false]], ['class' => 'dasdasd'])
                                        ->radioList(
                                                $start_hour_array, [
                                            'item' => function($index, $label, $name, $checked, $value) use($model_start_hour, $start_hour_array) {

                                                if (count($start_hour_array) == 1 && $value == 'dummy') {
                                                    return '<div class="col-md-12 text-center">Please select the date.</div>';
                                                }
                                                $checked_val = ($model_start_hour == $value) ? 'checked="checked"' : "";

                                                $html = '<div class="col-md-6 ">';
                                                $html .= '<ul class="products-list product-list-in-box">';
                                                $html .= '<li class="st-p3">';
                                                $html .= '<label class="contai_r">';
                                                $html .= '<input type="radio" onclick="select_time(\'' . $value . '\');" value="' . $value . '" name="' . $name . '" ' . $checked_val . '>';
                                                $html .= '<span class="checkmark"></span>';
                                                $html .= '</label>';
                                                $html .= '<span class="mn_3">' . $label . '</span>';
                                                $html .= '</li>';
                                                $html .= '</ul>';
                                                $html .= '</div>';
                                                return $html;
                                            }
                                                ]
                                        )
                                        ->label(false);
                                ?>


                            </div>
                            <div class="col-md-4">
                                <ul class="products-list product-list-in-box">
                                    <li>
                                        <div class="col-md-12">
                                            <?php 
                                            $model->selected_sesson_html = (empty($model->selected_sesson_html)) ? "<div><strong>No Lesson Available</strong></div>" : $model->selected_sesson_html; 
                                            echo $form->field($model, 'selected_sesson_html')->hiddenInput(['class' => 'form-control selected_sesson_html', 'maxlength' => true, ])->label(false);
                                            $displayNextBtn =  (!empty($model->start_hour))? "block":"none";
                                            ?>
                                            <div class="sst_btn sst_btn2" id="selected_date_data_parent">
                                                <?= $model->selected_sesson_html; ?>
                                            </div>
<!--                                            <div class="sst_btn sst_btn2" id="selected_date_data_parent">
                                                <div><strong>No Lesson Available</strong></div>
                                            </div>-->
                                            <br>
					    <button id="btn_next_btn" style="cursor: pointer; padding-top: 4px; display:<?= $displayNextBtn ?>;" class="label label-warning custom-hm-1">NEXT</button>
                                            <!--button id="btn_next_btn" style="display:none;" class="label label-warning hm-1 mr-2 black1">NEXT</button -->
                                        </div>
                                    </li>
                                </ul>
                                <?php echo $form->field($model, 'all_lesson_data')->hiddenInput(['class' => 'form-control all_lesson_data', 'maxlength' => true,])->label(false) ?>
                            </div>
                            <div class="col-md-12" style="">
                                <input type="hidden" name="step" value="4">
                                <input type="hidden" name="start_hour_array" id="start_hour_array" value="<?= $start_hour_json; ?>">
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
                <!--END SELECT DATE AND TIME -->

                <!-- CONFIRM BOOKING AND PAYMENT -->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingsix">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsePayment" aria-expanded="false" aria-controls="collapsesix">
                                CONFIRM BOOKING AND PAYMENT
                            </a>
                        </h4>
                    </div>
                </div>
                <!--END CONFIRM BOOKING AND PAYMENT -->

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <a class="btn btn-primary btn-sm black_button_in" href="<?= Yii::$app->getUrlManager()->createUrl('/student/home'); ?>">RETURN TO HOME</a>
        </div>
    </div>
</section>

<div class="other_forms">
    <?php echo Html::beginForm(['index'], 'post', ['id' => 'gostep1', 'class' => 'form-inline d-block']) ?>
    <input type="hidden" name="step" value="1">
    <input type="hidden" name="prev" value="1">
    <?php echo Html::endForm() ?>
    <?php echo Html::beginForm(['index'], 'post', ['id' => 'gostep2', 'class' => 'form-inline d-block']) ?>
    <input type="hidden" name="step" value="2">
    <input type="hidden" name="prev" value="2">
    <?php echo Html::endForm() ?>
    <?php echo Html::beginForm(['index'], 'post', ['id' => 'gostep3', 'class' => 'form-inline d-block']) ?>
    <input type="hidden" name="step" value="3">
    <input type="hidden" name="prev" value="3">
    <?php echo Html::endForm() ?>
</div>

<div class="modal big-width-model fade" id="change-booking-model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content change-booking-model-body">

        </div>
    </div>
</div>
