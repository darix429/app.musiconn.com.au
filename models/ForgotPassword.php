<?php
namespace app\models;

use Yii;
use mdm\admin\models\User;
use yii\base\Model;

/**
 * Password reset request form
 */
class ForgotPassword extends Model
{
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => 'mdm\admin\models\User',
                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => 'There is no user with such email.'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'email' => $this->email,
        ]);

        if ($user) {
            if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
                $user->generatePasswordResetToken();
            }

            if ($user->save()) {
               return Yii::$app->mailer->compose('@app/mail/forgot_password',['user'=>$user],['htmlLayout'=>'layouts/html'])
                                    ->setFrom(Yii::$app->params['supportEmail'])
                                    ->setTo($this->email)
                                    ->setSubject('Musiconn Booking System:  Password Reset')
                                    ->send();
                

            }
        }

        return false;
    }
}
