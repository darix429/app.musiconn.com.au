<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use app\libraries\General;
?>
<div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">
    <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Title</th>                
                <th>Tutor</th>
                <th>StudentID</th>
                <th>Student</th>
                <th>Instrument</th>
                <th>View Date</th>
                <th>View Time</th>

            </tr>
        </thead>
        <tbody>
            <?php
            if (!empty($lessonList)) {
                foreach ($lessonList as $key => $value) {
                    ?>
                    <tr>
                        <td><?= $key + 1; ?></td>
                        <td> <?= $value['title'];?></td>
                         <td><?= $value['tutorUu']['first_name'].' '.$value['tutorUu']['last_name'];?></td>
                         <td><?= $value['studentUu']['enrollment_id'];    ?> </td>                                               
                        <td><?= $value['studentUu']['first_name'] . ' ' . $value['studentUu']['last_name']; ?></td>
                        <td><?= $value['instrumentUu']['name'];?></td>
                        <td ><?= \app\libraries\General::displayDate($value['datetime']); ?></td>
                        <td ><?= \app\libraries\General::displayTime($value['datetime']); ?></td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
</div>
