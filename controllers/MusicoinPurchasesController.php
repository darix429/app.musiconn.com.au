<?php

namespace app\controllers;

use Yii;
use app\models\PaymentTransaction;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class MusicoinPurchasesController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['OWNER','ADMIN']
                    ]
                ],
            ]
        ];
    }

    public function actionIndex() {
        $collection = PaymentTransaction::find()
        ->joinWith(["studentMusicoinPackageUu","studentUu"])
        ->where(["not",["student_musicoin_package_uuid"=>null]])
        ->all();

        return $this->render('index', [
            "collection"=>$collection
        ]);
    }
}