<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "premium_standard_plan_rate".
 *
 * @property string $uuid
 * @property string $online_tutorial_package_uuid
 * @property string $name
 * @property string $payment_term_uuid
 * @property string $tutorial_type_uuid
 * @property string $price
 * @property string $gst
 * @property string $gst_description
 * @property string $total_price
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $duration
 * @property string $option
 * @property string $online_tutorial_package_type_uuid
 *
 * @property OnlineTutorialPackage $onlineTutorialPackageUu
 */
class PremiumStandardPlanRate extends \yii\db\ActiveRecord {

    public $standard_plan_options = [];
    public $plantext;
    public $standard_plan_options_price_1;
    public $standard_plan_options_gst_1;
    public $standard_plan_options_total_price_1;
    public $standard_plan_options_price_2;
    public $standard_plan_options_gst_2;
    public $standard_plan_options_total_price_2;
    public $standard_plan_options_price_3;
    public $standard_plan_options_gst_3;
    public $standard_plan_options_total_price_3;
    public $standard_plan_options_price_4;
    public $standard_plan_options_gst_4;
    public $standard_plan_options_total_price_4;
    public $standard_plan_options_price_5;
    public $standard_plan_options_gst_5;
    public $standard_plan_options_total_price_5;
    public $standard_plan_options_price_6;
    public $standard_plan_options_gst_6;
    public $standard_plan_options_total_price_6;
    public $standard_plan_options_gst_description_1;
    public $standard_plan_options_gst_description_2;
    public $standard_plan_options_gst_description_3;
    public $standard_plan_options_gst_description_4;
    public $standard_plan_options_gst_description_5;
    public $standard_plan_options_gst_description_6;

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'premium_standard_plan_rate';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            //[['name', 'payment_term_uuid', 'tutorial_type_uuid', 'default_option', 'online_tutorial_package_type_uuid'], 'required'],
            [['name', 'tutorial_type_uuid', 'default_option', 'online_tutorial_package_type_uuid'], 'required'],
            [['uuid', 'online_tutorial_package_uuid', 'name','description', 'payment_term_uuid', 'tutorial_type_uuid', 'gst_description', 'status', 'default_option', 'online_tutorial_package_type_uuid'], 'string'],
            [
                ['price', 'gst'],
                'required',
                'when' => function ($premiumstandardplanmodel) {
                    return $premiumstandardplanmodel->plantext == "PREMIUM";
                },
                'whenClient' => "function (attribute, value) { 
              return $('#id').find('option:selected').text() == 'PREMIUM'; 
          }"
            ],
            [
                ['standard_plan_options_price_1', 'standard_plan_options_gst_1', 'standard_plan_options_price_2', 'standard_plan_options_gst_2',  'standard_plan_options_price_3',
                    'standard_plan_options_gst_3', 
                    'standard_plan_options_price_4', 'standard_plan_options_gst_4',
                    'standard_plan_options_price_5', 'standard_plan_options_gst_5','standard_plan_options_price_6', 'standard_plan_options_gst_6'],
                'required',
                'when' => function ($premiumstandardplanmodel) {
                    return $premiumstandardplanmodel->plantext == "STANDARD";
                },
                'whenClient' => "function (attribute, value) { 
              return $('#id').find('option:selected').text() == 'STANDARD'; 
          }"
            ],
            [['price', 'gst', 'total_price'], 'number'],
            [['standard_plan_options_price_1', 'standard_plan_options_gst_1', 'standard_plan_options_total_price_1', 'standard_plan_options_price_2', 'standard_plan_options_gst_2', 'standard_plan_options_total_price_2', 'standard_plan_options_price_3',
            'standard_plan_options_gst_3', 'standard_plan_options_total_price_3',
            'standard_plan_options_price_4', 'standard_plan_options_gst_4', 'standard_plan_options_total_price_4',
            'standard_plan_options_price_5', 'standard_plan_options_gst_5', 'standard_plan_options_total_price_5',
                'standard_plan_options_price_6', 'standard_plan_options_gst_6', 'standard_plan_options_total_price_6'], 'number'],
            [['created_at', 'updated_at', 'standard_plan_options', 'plantext','standard_plan_options_gst_description_1',
                'standard_plan_options_gst_description_2',
                'standard_plan_options_gst_description_3',
                'standard_plan_options_gst_description_4',
                'standard_plan_options_gst_description_5',
                'standard_plan_options_gst_description_6'], 'safe'],
            [['standard_plan_options_price_1', 'standard_plan_options_gst_1', 'standard_plan_options_price_2', 'standard_plan_options_gst_2', 'standard_plan_options_price_3',
            'standard_plan_options_gst_3',
            'standard_plan_options_price_4', 'standard_plan_options_gst_4',
            'standard_plan_options_price_5', 'standard_plan_options_gst_5',], 'match', 'pattern' => '/^\d{0,10}(\.\d{0,5})?$/', 'message' => 'Only allow decimal 5 digit.'],
            [['price', 'gst'], 'match', 'pattern' => '/^\d{0,10}(\.\d{0,5})?$/', 'message' => 'Only allow decimal 5 digit.'],
            ['gst', 'compare', 'compareAttribute' => 'price', 'operator' => '<', 'enableClientValidation' => false],
            ['price', 'compare', 'compareAttribute' => 'gst', 'operator' => '>', 'enableClientValidation' => false],
            ['standard_plan_options_gst_1', 'compare', 'compareAttribute' => 'standard_plan_options_price_1', 'operator' => '<', 'enableClientValidation' => false, 'message' => ' GST 1 must be less than  Price 1.'],
            ['standard_plan_options_price_1', 'compare', 'compareAttribute' => 'standard_plan_options_gst_1', 'operator' => '>', 'enableClientValidation' => false, 'message' => ' Price 1 must be greater than  Gst 1.'],
            ['standard_plan_options_gst_2', 'compare', 'compareAttribute' => 'standard_plan_options_price_2', 'operator' => '<', 'enableClientValidation' => false, 'message' => ' GST 2 must be less than  Price 2.'],
            ['standard_plan_options_price_2', 'compare', 'compareAttribute' => 'standard_plan_options_gst_2', 'operator' => '>', 'enableClientValidation' => false, 'message' => ' Price 2 must be greater than  Gst 2.'],
            ['standard_plan_options_gst_3', 'compare', 'compareAttribute' => 'standard_plan_options_price_3', 'operator' => '<', 'enableClientValidation' => false, 'message' => ' GST 3 must be less than  Price 3.'],
            ['standard_plan_options_price_3', 'compare', 'compareAttribute' => 'standard_plan_options_gst_3', 'operator' => '>', 'enableClientValidation' => false, 'message' => ' Price 3 must be greater than  Gst 3.'],
            ['standard_plan_options_gst_4', 'compare', 'compareAttribute' => 'standard_plan_options_price_4', 'operator' => '<', 'enableClientValidation' => false, 'message' => ' GST 4 must be less than  Price 4.'],
            ['standard_plan_options_price_4', 'compare', 'compareAttribute' => 'standard_plan_options_gst_4', 'operator' => '>', 'enableClientValidation' => false, 'message' => ' Price 4 must be greater than  Gst 4.'],
            ['standard_plan_options_gst_5', 'compare', 'compareAttribute' => 'standard_plan_options_price_5', 'operator' => '<', 'enableClientValidation' => false, 'message' => ' GST 5 must be less than  Price 5.'],
            ['standard_plan_options_price_5', 'compare', 'compareAttribute' => 'standard_plan_options_gst_5', 'operator' => '>', 'enableClientValidation' => false, 'message' => ' Price 5 must be greater than  Gst 5.'],
                        
            ['standard_plan_options_gst_6', 'compare', 'compareAttribute' => 'standard_plan_options_price_6', 'operator' => '<', 'enableClientValidation' => false, 'message' => ' GST 6 must be less than  Price 6.'],
            ['standard_plan_options_price_6', 'compare', 'compareAttribute' => 'standard_plan_options_gst_6', 'operator' => '>', 'enableClientValidation' => false, 'message' => ' Price 6 must be greater than  Gst 6.'],

            [['price', 'gst'], 'required', 'on' => ['admin_update_tutorial']],
            [['uuid'], 'unique'],
            [['online_tutorial_package_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => OnlineTutorialPackage::className(),
                'targetAttribute' => ['online_tutorial_package_uuid' => 'uuid']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'online_tutorial_package_uuid' => Yii::t('app', 'Online Tutorial Package Uuid'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'payment_term_uuid' => Yii::t('app', 'Payment Term Uuid'),
            'tutorial_type_uuid' => Yii::t('app', 'Tutorial Type Uuid'),
            'price' => Yii::t('app', 'Price'),
            'gst' => Yii::t('app', 'GST'),
            'gst_description' => Yii::t('app', 'GST Description'),
            'total_price' => Yii::t('app', 'Total Price'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'duration' => Yii::t('app', 'Duration'),
            'default_option' => Yii::t('app', 'Option'),
            'online_tutorial_package_type_uuid' => Yii::t('app', 'Plan Type'),
            'standard_plan_options_price_1' => Yii::t('app', 'Price 1'),
            'standard_plan_options_gst_1' => Yii::t('app', 'GST 1'),
            'standard_plan_options_price_2' => Yii::t('app', 'Price 2'),
            'standard_plan_options_gst_2' => Yii::t('app', 'GST 2'),
            'standard_plan_options_price_3' => Yii::t('app', 'Price 3'),
            'standard_plan_options_gst_3' => Yii::t('app', 'GST 3'),
            'standard_plan_options_price_4' => Yii::t('app', 'Price 4'),
            'standard_plan_options_gst_4' => Yii::t('app', 'GST 4'),
            'standard_plan_options_price_5' => Yii::t('app', 'Price 5'),
            'standard_plan_options_gst_5' => Yii::t('app', 'GST 5'),
            'standard_plan_options_price_6' => Yii::t('app', 'Price 6'),
            'standard_plan_options_gst_6' => Yii::t('app', 'GST 6'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOnlineTutorialPackageUu() {
        return $this->hasOne(OnlineTutorialPackage::className(), ['uuid' => 'online_tutorial_package_uuid']);
    }

    public function loadstandard_plan_optionsData($standard_plan_optionsData) {
        $this->standard_plan_options = [];
        foreach ($standard_plan_optionsData as $standard_plan_optionsData) {
            $model1 = new PremiumStandardPlanRate();
            $model1->setAttributes($standard_plan_optionsData);
            $this->standard_plan_options[] = $model1;
        }

        return !empty($this->passengers);
    }

    public function validatestandard_plan_options() {
        foreach ($this->standard_plan_options as $standard_plan_option) {
            if (!$standard_plan_option->validate()) {
                $this->addErrors($standard_plan_option->getErrors());
                return false;
            }
        }

        return true;
    }
    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentTermUu() {
        return $this->hasOne(PaymentTerm::className(), ['uuid' => 'payment_term_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTutorialTypeUu() {
        return $this->hasOne(TutorialType::className(), ['uuid' => 'tutorial_type_uuid']);
    }
    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOnlineTutorialPackageTypeUu() {
        return $this->hasOne(OnlineTutorialPackageType::className(), ['uuid' => 'online_tutorial_package_type_uuid']);
    }
    
    


}
