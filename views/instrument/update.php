<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Instrument */

$this->title = Yii::t('app', 'Update Instrument: ' . $model->name, [
            'nameAttribute' => '' . $model->name,
        ]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Instruments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->uuid]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<?=

$this->render('_form', [
    'model' => $model,
    'new' => false,
])
?>


