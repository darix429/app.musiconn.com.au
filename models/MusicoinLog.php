<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "musicoin_log".
 *
 * @property string $uuid
 * @property string $student_uuid
 * @property string $summary
 * @property string $debit
 * @property string $credit
 * @property string $balance
 * @property string $referenceno
 * @property string $created_at
 *
 * @property Student $studentUu
 */
class MusicoinLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $type;
    public $coins;
    public static function tableName()
    {
        return 'musicoin_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //html tag filter
            [['summary',], 'match', 'pattern' => \app\libraries\General::validTextPattern(), 'message' => 'Invalid charecters used.'], 
            
            [['type', 'student_uuid', 'summary', 'coins'], 'required', 'on' => 'credit_debit'],
            [['coins'], 'number', 'min' => 1, 'on' => 'credit_debit'],
            [['coins'], 'checkCoinValidation', 'on' => 'credit_debit'],
            [['uuid', 'student_uuid', 'summary', 'referenceno'], 'string'],
            [['debit', 'credit', 'balance'], 'number'],
            [['coins', 'credit','debit', 'balance'], 'match', 'pattern' => '/^\d{0,10}?$/', 'message' => 'Only allow digit not allow decimal digit.'],
            [['created_at'], 'safe'],
            [['uuid'], 'unique'],
            [['student_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['student_uuid' => 'student_uuid']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uuid' => Yii::t('app', 'Uuid'),
            'student_uuid' => Yii::t('app', 'Student'),
            'summary' => Yii::t('app', 'Description'),
            'debit' => Yii::t('app', 'Debit'),
            'credit' => Yii::t('app', 'Credit'),
            'balance' => Yii::t('app', 'Balance'),
            'referenceno' => Yii::t('app', 'Referenceno'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentUu()
    {
        return $this->hasOne(Student::className(), ['student_uuid' => 'student_uuid']);
    }
    
    public function checkCoinValidation($attribute) {

        if (!empty($this->student_uuid)) {
            $student = Student::findOne($this->student_uuid);
            if (!empty($student)) {
                if ($this->type == 'DEBIT') {
                    if ($this->coins > $student->musicoin_balance) {
                        $this->addError($attribute, 'Debit coins should not be greater than available coins. Available coins (' . $student->musicoin_balance . ').');
                    }
                }
            }
        }
    }

    /* Add Musicoins into student account*/
    public function creditMusicoin($param) {

        $student_uuid = (isset($param['student_uuid'])) ? $param['student_uuid'] : NULL;
        $coins = (isset($param['coins'])) ? $param['coins'] : 0;
        $summary = (isset($param['summary'])) ? $param['summary'] : "Credited from portal.";
        $referanceno = (isset($param['referanceno'])) ? $param['referanceno'] : NULL;


        if ($coins <= 0) {
            $return = ['code' => 421, 'message' => 'Coin must be greater than 0.'];
        }
        $student = Student::findOne($student_uuid);
        if (!empty($student)) {

            $new_balance = bcadd($student->musicoin_balance, $coins, 2);

            $model = new MusicoinLog();
            $model->student_uuid = $student_uuid;
            $model->credit = $coins;
            $model->debit = 0;
            $model->balance = $new_balance;
            $model->summary = $summary;
            $model->referenceno = $referanceno;
            $model->created_at = date('Y-m-d H:i:s');
            if ($model->save(false)) {
                $student->musicoin_balance = $new_balance;
                $student->save(false);
                $return = ['code' => 200, 'message' => 'Coins credited successfully.', 'log_uuid' => $model->getPrimaryKey()];
            } else {
                $return = ['code' => 421, 'message' => 'Sorry! something wrong into saved data.'];
            }
        } else {
            $return = ['code' => 404, 'message' => 'Sorry! Account not found.'];
        }
        return $return;
    }
    
    /* Debit Musicoins into student account*/
    public function debitMusicoin($param) {

        $student_uuid = (isset($param['student_uuid'])) ? $param['student_uuid'] : NULL;
        $coins = (isset($param['coins'])) ? $param['coins'] : 0;
        $summary = (isset($param['summary'])) ? $param['summary'] : "Debited from portal.";
        $referanceno = (isset($param['referanceno'])) ? $param['referanceno'] : NULL;

        if ($coins <= 0) {
            $return = ['code' => 421, 'message' => 'Coin must be greater than 0.'];
        }
        $student = Student::findOne($student_uuid);
        if (!empty($student)) {

            $new_balance = bcsub($student->musicoin_balance, $coins, 2);

            $model = new MusicoinLog();
            $model->student_uuid = $student_uuid;
            $model->credit = 0;
            $model->debit = $coins;
            $model->balance = $new_balance;
            $model->summary = $summary;
            $model->referenceno = $referanceno;
            $model->created_at = date('Y-m-d H:i:s');
            if ($model->save(false)) {
                $student->musicoin_balance = $new_balance;
                $student->save(false);
                $return = ['code' => 200, 'message' => 'Coins debited successfully.', 'log_uuid' => $model->getPrimaryKey()];
            } else {
                $return = ['code' => 421, 'message' => 'Sorry! something wrong into saved data.'];
            }
        } else {
            $return = ['code' => 404, 'message' => 'Sorry! Account not found.'];
        }
        return $return;
    }

}
