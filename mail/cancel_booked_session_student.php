<?php

use yii\helpers\Html;

?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['student_name']; ?>,</p>
            
            <p><?= ucfirst($content['tutor_name']); ?> have cancelled your booked Lesson in <?= Yii::$app->name; ?>. </p>
            
            <p><strong>Tutor Name :</strong> <?= $content['tutor_name']; ?></p>
            <p><strong>Instrument :</strong> <?= $content['instrument']; ?></p>
            <p><strong>Lesson Duration :</strong> <?= $content['tution_duration']; ?> Minutes</p>
            <p><strong>Lesson Date :</strong> <?= $content['session_date']; ?></p>
            <p><strong>Lesson Time :</strong> <?php echo $content['session_time']; ?></p>
               
            <p>You have credit this cancelled Lesson in your account so you can able to booked new Lesson.</p>
            <p> If any query for your account then please contact to administrator.</p>

            <p>Sincerely,</p>
            <p>System Administrators</p>
        </td>
    </tr>
</table>