<?php

use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />

        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>
        <table border="0" cellpadding="0" cellspacing="0" class="body" align="center">
            <tr>
                <td>&nbsp;</td>
                <td class="container">
                    <div class="content">

                        <!-- START CENTERED WHITE CONTAINER -->
                        <span class="preheader"></span>
                        <table class="main">
<!--                            <div class="top-header"><?= Yii::$app->name; ?>abc</div>-->
                            <!-- START MAIN CONTENT AREA -->
                            <tr>
                                <td class="wrapper" >
                                    <?= $content ?>

                                </td>
                            </tr>

                            <!-- END MAIN CONTENT AREA -->
                        </table>

                        <!-- START FOOTER -->
                        <div class="footer">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                <!--                  <td class="content-block">
                                    <span class="apple-link"></span>
                                    <br> Don't like these emails? <a href="">Unsubscribe</a>.
                                  </td>-->
                                </tr>
                                <tr>
                                    <td class="content-block powered-by">
                                        <a href=""><?= Yii::$app->params['copyright']; ?></a>.
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <!-- END FOOTER -->

                        <!-- END CENTERED WHITE CONTAINER -->
                    </div>
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>

        <?php $this->endBody() ?>
    </body>

</html>
<?php  ?>
<?php $this->endPage() ?>
