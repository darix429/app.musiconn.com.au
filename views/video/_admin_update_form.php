<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\ArrayHelper;
use app\models\InstrumentCategory;
use app\models\Instrument;
use yii\helpers\Url;

$categoryArray = ArrayHelper::map(InstrumentCategory::find()->asArray()->all(), 'uuid', 'name');
//$instrumentArray = ArrayHelper::map(Instrument::find()->where(['status' => 'ACTIVE'])->asArray()->all(), 'uuid', 'name');

if($model['tutor_uuid'] != NULL){ 
    $tutorInstrumentModel = \app\models\TutorInstrument::find()->where(['tutor_uuid' => $model['tutor_uuid']])->asArray()->all();
    $old_t_ins = array_column($tutorInstrumentModel, 'instrument_uuid');

    $instrumentArray = ArrayHelper::map(Instrument::find()->where(['in', 'uuid', $old_t_ins])->asArray()->all(), 'uuid', 'name');
}else{ 
    $instrumentArray = ArrayHelper::map(Instrument::find()->where(['status' => 'ACTIVE'])->asArray()->all(), 'uuid', 'name');
}

?>

<?php
//echo "<pre>";
//print_r($model->getErrors());
//echo "</pre>";
?>
<?php
$form = ActiveForm::begin(['options' => ['id' => 'upload_form', 'enctype' => 'multipart/form-data', 'enableClientValidation' => true,
                'enableAjaxValidation' => true, 'validateOnChange' => true,]]);
?>
<div class="form-row">
    <div class="col-md-4 mb-0">
        <?= $form->field($model, 'title')->textInput(['class' => 'form-control code', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('title')]) ?>
    </div>

    <div class="col-md-4 mb-0">
        <?= $form->field($model, 'instrument_uuid')->dropDownList($instrumentArray, ['class' => 'form-control', 'prompt' => 'Select Instrument',]) ?>
    </div>
    <div class="col-md-4 mb-0">
        <?= $form->field($model, 'instrument_category_uuid')->dropDownList($categoryArray, ['class' => 'form-control', 'prompt' => 'Select Category',]) ?>
    </div>
</div>

<div class="form-row">
    <div class="col-md-9 mb-0">
        <?= $form->field($model, 'update_file')->fileInput(['class' => 'form-control']) ?>
    </div> 
    
    <div class="col-md-3 mb-0">
        <label class='control-label' for='is_free'> Is Free </label>
        <?= $form->field($model, 'is_free')->checkbox(['class' => 'form-control iswitch iswitch-lg iswitch-primary','label' => null]) ?>
    </div>

    
    <div class="col-md-6 mb-0 expire_date_parent" style="<?= ($model->status == 'PUBLISHED') ? 'display:block;' : 'display:none;' ?>">
        <?php $model->expire_date = (empty($model->expire_date)) ? date('Y-m-d') : $model->expire_date; ?>
        <?php $model->expire_date = date('Y-m-d', strtotime($model->expire_date)); ?>
        <?= $form->field($model, 'expire_date')->textInput(['readonly' => true, 'class' => 'form-control custom_date_picker', 'placeholder' => 'YYYY-MM-DD']) ?>        
    </div>                     
    
</div>
<div class="form-row">
    <div class="col-md-12 mb-0">
        <?= $form->field($model, 'description')->textarea(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('description')]) ?>
    </div>

</div>


<div class="progress progress-md">
    <div id="progress-bar" class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
        <span class="sr-only" id="progress-status">0%</span>
    </div>
</div>
<div class="msg"></div>
<div class="spacer"></div>


<?= Html::Button(Yii::t('app', 'Update'), ['id' => 'savebutton', 'class' => 'btn btn-primary', 'style' => 'float: right;']) ?>

<?php ActiveForm::end(); ?>
