<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Tutor */

$this->title = Yii::t('app', 'Create Tutor');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tutors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


    <?= $this->render('_form', [
        'model' => $model,
        'userModel' => $userModel,
        'instrumentModel'=>$instrumentModel,
        'tutorInstrumentModel'=>$tutorInstrumentModel,
        'packegetypeModel' => $packegetypeModel,/** Added by pooja : Tutor wise plan rate **/
        'new'=>$new,
    ]) ?>


