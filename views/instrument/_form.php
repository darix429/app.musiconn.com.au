<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Admin */
/* @var $form yii\widgets\ActiveForm */
//echo "<pre>";print_r(Yii::$app->controller);
$this->registerCssFile(Yii::$app->request->baseUrl . '/theme_assets/plugins/ios-switch/css/switch.css');

?>

<div class="col-xl-12 col-lg-12 col-12 col-md-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Please Fill Information</h2>
            <div class="actions panel_actions float-right">
                <!--<i class="box_toggle fa fa-chevron-down"></i>-->
                <a href="<?= Yii::$app->getUrlManager()->createUrl(['/instrument/index/']) ?>" class="btn btn-info btn-icon "><i class="fa fa-arrow-left text-white"></i> <span>Back</span></a>
            </div>
        </header>
        <div class="content-body">
            <div class="row">
                <div class="<?php echo (!$new) ? "col-lg-4 col-md-12 col-12" : ''; ?>">
                    <div class="form-row">
                        <div class="col-md-12 mb-0">
                            <?php if (!$new) { ?>
                                <!--label class="form-label" for="field-1">Instrument Image</label-->
                                <span class="desc"></span>
                                <?php
                                //print_r($model->image);
                                ob_start();
                                fpassthru($model->image);
                                $contents = ob_get_contents();
                                ob_end_clean();
                                //print_r(base64_encode($contents));
                                $dataUri = "data:image/jpg;base64," . base64_encode($contents);
                                echo "<img class='img-fluid' style='' src='$dataUri'/>";
                                ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="<?php echo (!$new) ? "col-lg-8" : 'col-lg-12'; ?>  col-md-12 col-12">
                    <?php
//                    echo "<pre>";
//                    print_r($model->getErrors());
//                    print_r($userModel->getErrors());
//                    echo "</pre>";
                    ?>
                    <!--<div class="form-block"><input type="checkbox" checked="" class="iswitch iswitch-md iswitch-success"></div>-->
                    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'id' => 'instrument_form']]); ?>
                    <div class="form-row">
                        <div class="col-md-6 mb-0">
                            <?= $form->field($model, 'name')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('name')]) ?>
                        </div>
                        <div class="col-md-6 mb-0">
                            <?= $form->field($model, 'status')->dropDownList(['ACTIVE' => 'ACTIVE', 'INACTIVE' => 'INACTIVE'], ['class' => 'form-control']) ?>
                        </div> 
                    </div>
                    <div class="form-row">
                        <div class="col-md-12 mb-0">
                            <?= $form->field($model, 'description')->textarea(['rows' => 2]) ?>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="col-md-12 mb-0">							
                            <div class="controls">
                                <?= $form->field($model, 'image')->fileInput(['class' => 'form-control']) ?>
                            </div>
                        </div>                                                       
                    </div>
                    <?php if ($new) { ?>
                        <?= Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn btn-primary', 'style' => 'float: right;']) ?>
                    <?php } else { ?>
                        <?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-primary', 'style' => 'float: right;']) ?>
                    <?php } ?>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </section>
</div>
