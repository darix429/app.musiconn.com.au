<?php

use yii\helpers\Html;
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>Dear <?= $content['name'] ?>,</p>
            
            <p>This mail has been sent to you to create new account in to <?= Yii::$app->name; ?>. Please check below detail of <?= Yii::$app->name; ?> account.</p>
            

            <p><strong>Username / Email :</strong> <?= $content['username']; ?></p>
            <p><strong>Password :</strong> <?= $content['password']; ?></p>
            <p><strong>Type of Role :</strong> <?= $content['admin_type']; ?></p>
            
            
            <p> If any query for your account then please contact to administrator.</p>

            <p>Sincerely,</p>
            <p>System Administrators</p>
        </td>
    </tr>
</table>

