<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<?php $form = ActiveForm::begin(['id' => 'form-change']); ?>
<div class="form-row">
    <div class="col-md-6 mb-0">
        <div class="form-group ">
            <label class="control-label" >Username</label>
            <span class="form-control bg-gray-field" aria-required="true" aria-invalid="true"><?= Yii::$app->user->identity->username; ?></span>
        </div>
    </div>
    <div class="col-md-6 mb-0">
        <?= $form->field($modelCP, 'oldPassword')->passwordInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $modelCP->getAttributeLabel('oldPassword')]) ?>
    </div>                                                       
</div>
<div class="form-row">
    <div class="col-md-6 mb-0">
        <?= $form->field($modelCP, 'newPassword')->passwordInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $modelCP->getAttributeLabel('newPassword')]) ?>
    </div>
    <div class="col-md-6 mb-0">
        <?= $form->field($modelCP, 'retypePassword')->passwordInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $modelCP->getAttributeLabel('retypePassword')]) ?>
    </div>                                                       
</div>

<?= Html::Button('Change', ['class' => 'btn btn-primary', 'name' => 'change-button', 'id' => 'password-change-btn']) ?>

<?php ActiveForm::end(); ?>