<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="col-lg-12 col-md-12 col-12 padding-0 theme-datatable">

    <table  class="table table-striped dt-responsive display my-datatable" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Order ID</th>
                <th>Payment number</th>
                <th>Total</th>
                <th>Discount</th>
                <th>Grand Total</th>
                <th>Order Date</th>
                <th>Invoice</th>
            </tr>
        </thead>

        <tbody>
            <?php
            if (!empty($orderList)) {
                foreach ($orderList as $key => $value) { 
                    
                    ?>
                    <tr>
                        <td><?= $key + 1; ?></td>
                        <td><?= "#".$value['order_id']; ?></td>
                        <td><?= (!empty($value['paymentTransactionUu']['transaction_number'])) ? "#".$value['paymentTransactionUu']['transaction_number'] : ''; ?></td>
                        <td><?= $value['total']; ?></td>
			<td><?= $value['discount']; ?></td>
			<td><?= $value['grand_total']; ?></td>
                        <td><?= \app\libraries\General::displayDateTime($value['order_datetime']); ?></td>
                        <td>
                            <?php 
                            $invoicePath = Yii::$app->params['media']['invoice']['path'].$value['invoice_file'];
                            
                            if(is_file($invoicePath)){  ?>
                                <a href="<?php echo Yii::$app->params['media']['invoice']['url'] . $value['invoice_file']; ?>" download>
                                <button class="btn btn-purple  btn-icon bottom15 right15 download">
                                    <i class="fa fa-download"></i> &nbsp; <span>Download</span>
                                </button>
                            </a>
                            <?php } else { ?>
                                <button class="btn btn-purple  btn-icon bottom15 right15 invoice_check" data-uuid="<?= $value['uuid']; ?>">
                                        <i class="fa fa-download"></i> &nbsp; <span>Download</span>
                                </button>
                            <?php } ?>
                            
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>

</div>
