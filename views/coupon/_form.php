<?php

use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Admin */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs(
    '

    $(function() {
      $(".common").on("input", function() {
        match = (/(\d{0,10})[^.]*((?:\.\d{0,2})?)/g).exec(this.value.replace(/[^\d.]/g, ""));
        this.value = match[1] + match[2];
          });
    });

    document.getElementById("coupon-type").addEventListener("change", function () {
        if(this.value == "Percentage")
        {
            document.getElementById("flat_amount_div").style.display = "none";
        document.getElementById("per_amount_div").style.display = "block";
        } else {
            document.getElementById("per_amount_div").style.display = "none";
            document.getElementById("flat_amount_div").style.display = "block";
        }

});


', View::POS_END
);
?>
<style>
    .input-group .form-control{
        width: 285px;
    }
    /*#per_amount_div {
    display: none;
}*/
</style>
<div class="col-xl-12 col-lg-12 col-12 col-md-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title float-left">Please Fill Information</h2>
            <div class="actions panel_actions float-right">
                <a href="<?=Yii::$app->getUrlManager()->createUrl(['/coupon/index/'])?>" class="btn btn-info btn-icon "><i class="fa fa-arrow-left text-white"></i> <span>Back</span></a>
            </div>
        </header>
        <div class="content-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12">
                    <?php
//                    echo "<pre>";
//                    print_r($model->getErrors());
//                    echo "</pre>";
?>
                    <?php
$form = ActiveForm::begin([
    'action'                 => (!$model->isNewRecord) ? Yii::$app->getUrlManager()->createUrl(['/coupon/update/', 'id' => $model->uuid]) : ['create'],
    'enableClientValidation' => true,
    //'enableAjaxValidation' => true,
    'validateOnChange'       => true,
]);
?>
                    <div class="form-row">
                        <div class="col-md-4 mb-0">
                            <?=$form->field($model, 'code')->textInput(['class' => 'form-control code', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('code')])?>
                        </div>
                        <div class="col-md-4 mb-0">
                            <?=$form->field($model, 'type')->dropDownList(['Flat' => 'Flat', 'Percentage' => 'Percentage'], ['class' => 'form-control'])?>
                        </div>

                        <?php if(isset($model->type) && ($model->type =='Percentage'))
                        {
                        $display = 'none'; 
                        $display2 = 'block';
                    } else {
                        $display = 'block'; 
                        $display2 = 'none';
                    }
                        ?>



                        <div class="col-md-4 mb-0" id="flat_amount_div" style="display:<?= $display;?>">

                            <?=$form->field($model, 'amount', ['template' => '{label}<div class="input-group"><div class="input-group-prepend" ><span class="input-group-text">$</span></div>{input}{error}</div>'])->textInput(['class' => 'form-control common', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('amount'), 'aria-label' => 'Amount (to the nearest dollar)', 'style' => 'text-align:right; width: auto;'])?>
                        </div>

                         <div class="col-md-4 mb-0" id="per_amount_div"  style="display:<?= $display2;?>">

                            <?=$form->field($model, 'percentage', ['template' => '{label}<div class="input-group"><div class="input-group-prepend" ><span class="input-group-text">%</span></div>{input}{error}</div>'])->textInput(['class' => 'form-control common', 'maxlength' => true, 'placeholder' => 'Percentage', 'aria-label' => 'Amount (to the nearest dollar)', 'style' => 'text-align:right; width: auto;'])->label('Percentage')?>
                        </div>


                    </div>
                    <div class="form-row">
                        <div class="col-md-6 mb-0">
                            <?=$form->field($model, 'code_type')->dropDownList(['Sale Representative' => 'Sale Representative', 'Music Shop' => 'Music Shop'], ['class' => 'form-control'])?>
                        </div>
                        <div class="col-md-6 mb-0">
                            <?=$form->field($model, 'status')->dropDownList(['ENABLED' => 'ENABLED', 'DISABLED' => 'DISABLED'], ['class' => 'form-control'])?>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6 mb-0">
                            <?php $model->start_date = \app\libraries\General::displayDate($model->start_date);
echo $form->field($model, 'start_date')->textInput(['readonly' => true, 'class' => 'form-control custom_date_picker', 'maxlength' => true, 'placeholder' => 'DD-MM-YYYY'])?>
                        </div>
                        <div class="col-md-6 mb-0">
                            <?php $model->end_date = \app\libraries\General::displayDate($model->end_date);
echo $form->field($model, 'end_date')->textInput(['readonly' => true, 'class' => 'form-control custom_date_picker', 'maxlength' => true, 'placeholder' => 'DD-MM-YYYY'])?>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-12 mb-0">
                            <?=$form->field($model, 'description')->textarea(['class' => 'form-control', 'maxlength' => true, 'placeholder' => $model->getAttributeLabel('description')])?>
                        </div>

                    </div>

                    <?=Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary', 'style' => 'float: right;'])?>

                    <?php ActiveForm::end();?>

                </div>
            </div>

        </div>
    </section>
</div>
